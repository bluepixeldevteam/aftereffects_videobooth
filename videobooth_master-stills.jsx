﻿//TODO update this to use typescript?

//********************************************
//
// videobooth_master.jsx
//
// adapted from Ink Time Capsule video (2014) developed for Blue Pixel by Dan Ebberts
// debberts@comcast.net
// 
// updated by Ben Johnson and evolved over many years
// bjohnson@bpcreates.com
// Mar. 19, 2023
//***********************************************


/*
	Revision History

 	Ver. 	Description
	----	---------------------------------------------------------------

	v1          Initial version to support switching based on codes in waiver

*/

// DEVELOPER VARIABLES
var devMode = true;  //if true will use alternate computer name in paths, will use additinal logging, will NOT RUN IN LOOP
var devModeComputerUserName = "bluepixel";

// GLOBAL PROJECT VARIABLES
var loopDelay = 5000; //miliseconds to wait between runs when no waivers are found
var renderSettingsName = "Best Settings";
var outputModuleName = "High Quality";

var mainCompName = "PROJECT_MASTER"; //override this in activation specific function
var customerFootageCompName = "PROJECT_MASTER"; //override this in activation specific function
var customerFootageLayerName = "customer_footage"; //override this in activation specific function

var maxCustomerNameChars = 16; //might be legacy cruft, to be moved into activation specific function

var computerUserName = "bluepixel"; // set this to bluepixel in production!!!!!!!

var outputFolderPath = "";
var outputFileName = "";

//	if (templateFile == null){
//		alert("Template project file not open -- exiting.");
//		return;
//	}


// APPLICATION STARTS HERE
// APPLICATION STARTS HERE
// APPLICATION STARTS HERE

if (devMode){
    computerUserName = devModeComputerUserName; // set this to bluepixel in production!!!!!!!
    // app.bp_watchFolder = Folder.selectDialog("Please select XML watch folder");
    app.bp_watchFolder = Folder(("/Users/" + computerUserName + "/Documents/waivers_pending/"));
    outputFolderPath = ('/Users/' + computerUserName + '/Documents/ae_output');
    imageOutputFolderPath = ('/Users/' + computerUserName + '/Documents/ae_image_output');
 }else {
         app.bp_watchFolder = Folder(("/Users/" + computerUserName + "/Documents/waivers_pending/"));
         outputFolderPath = ('/Users/' + computerUserName + '/Documents/ae_output');
         imageOutputFolderPath = ('/Users/' + computerUserName + '/Documents/ae_image_output');
     }

//EXIT IF NO WATCH FOLDER IS FOUND, ELSE SLEEP FOR 200MS
if (app.bp_watchFolder == null){
	alert ("No watch folder selected -- exiting.");
}else{
	app.bp_oneBigLoop = oneBigLoop;
	app.scheduleTask("app.bp_oneBigLoop();",5000,false); // initial delay (.2 sec)  - IMPORTANT this will cause script to not run in ExtendScript IDE because it is referencing the AE app
}
    

//CRITICAL!!!!!!!!!!!!!!!!!!!!!!!!!!!!
//for scheduleTask to work properly, ALL of the fuctions to be called must be contained within the schedule task.
//for this purpose, we have oneBigLoop
function oneBigLoop(){
if(devMode){
    logMessage("Starting oneBigLoop");
    }
CheckForXML();

// SUPPORTING FUNCTIONS
// SUPPORTING FUNCTIONS
// SUPPORTING FUNCTIONS
// SUPPORTING FUNCTIONS

/**
 * 
 * @param {string} theCompName name of the composition
 * @returns {object} the compostiion object
 */
	function findComp(theCompName){
		for (var i = 1; i <= app.project.numItems; i++){
			if ((app.project.item(i).name == theCompName) && (app.project.item(i) instanceof CompItem)){
				return app.project.item(i);
			}
		}
		return null; // can't find comp--return bad news
	}

    /**
     * 
     * @returns {Date} the current timestamp
     */
	function timeStamp(){ 
		function dateString(d) {
    			function pad(n) { return n<10 ? '0'+n : n }
   			return      d.getFullYear()
			+ '-' + pad(d.getMonth()+1)
			+ '-' + pad(d.getDate())
			+ ' ' + pad(d.getHours())
			+ ':' + pad(d.getMinutes())
			+ ':' + pad(d.getSeconds());
		}
		var myDate = new Date();
		return dateString(myDate);
	}


   function addCommas ( s ){
    return s.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    }


function removeNonDigits ( s ) {
        return s.toString().replace(/\D/g,'');
    }

function replaceExtension ( original, targetText, replacement ) {
        return original.toString().replace(targetText, replacement);
    }

    function toTitleCase(str){
    return str.replace(/\w\S*/g, function(txt){return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();});
    }

	function strip(stringToTrim) { // get rid of leading and trailing white space
	return stringToTrim.replace(/^\s+|\s+$/g,"");
	}

    /**
     * Utility to write a message to the log file
     * @param {string} theMessage 
     */
	function logMessage(theMessage){
		// append messages to log file in Log folder -- create folder and/or file if they don't exist
		var myLogFolderPath = app.bp_watchFolder.path + "/" + app.bp_watchFolder.name + "/" + "log";
		var myLogFolder = new Folder(myLogFolderPath);
		if (! myLogFolder.exists){
			myLogFolder.create();
		}
		var myLogFileName = "log.txt";
		var myLogFilePath = myLogFolderPath + "/" + escape(myLogFileName);
		var myLogFile = new File(myLogFilePath);
		var logFileOK = myLogFile.open("e");
		myLogFile.seek(0,2);
		myLogFile.writeln(timeStamp() + "  " + theMessage);
		myLogFile.close();
	}

    /**
     * Replace text content of a text layer
     * @param {object} theLayer the text layer to modify
     * @param {string} theNewText the string to update the layer with
     * @param {boolean} resize resize the text to fit the layer width
     */
    function replaceText(theLayer,theNewText,resize){
			var maxWidth = theLayer.sourceRectAtTime(theLayer.inPoint,false).width; // get max width
			var mySourceText = theLayer.property("ADBE Text Properties").property("ADBE Text Document");
			var myTextDoc = mySourceText.value;
			myTextDoc.text = theNewText;
			mySourceText.setValue(myTextDoc);
			var newWidth = theLayer.sourceRectAtTime(theLayer.inPoint,false).width;
			if (newWidth > maxWidth && resize){
				var newFontSize = myTextDoc.fontSize*(maxWidth/newWidth);
				myTextDoc.fontSize = newFontSize;
				mySourceText.setValue(myTextDoc);
			}
		}
        
        /**  method to autoKey a layer 
         * selects a region and averages the color, color value is passed in to KeyLight keyer
         * @param myVideoLayer - a layer object
         * @param {Array} sampleCenter - [x,y] center point of the selected region
         * @param sampleHeight - height of the region
         * @param sampleWidth - width of the region
        */
    function autoKeyer(myVideoLayer,sampleCenter,sampleHeight,sampleWidth){
        // UPDATE KEY COLOR
        logMessage("start autoKeyer");
        if(!sampleCenter){
            sampleCenter = [1800,250];  // offset from upper left corner
        }
        if(!sampleHeight){
            sampleHeight = 40;
        }
        if(!sampleWidth){
            sampleWidth = 40;
        }
        logMessage("sampleRadius = " + sampleWidth + "," + sampleHeight); 
        logMessage("sampleCenter = " + sampleCenter.toString()); 
        try{
        // add temporary color control to layer
        var myColorControl = myVideoLayer.property("Effects").addProperty("ADBE Color Control");
        var myColorProp = myColorControl.property("ADBE Color Control-0001");
        myColorProp.expression = "sampleImage(" + sampleCenter.toSource() + ",[" +  sampleWidth /2 + "," + sampleHeight /2 + "],false);";
	
        // harvest the result
        var myColor = myColorProp.value;
	
        // remove the color control
        myColorControl.remove();
        logMessage("color set to " + myColor.toString());
	
        // apply the color to the Keylight efect
        myVideoLayer.property("Effects").property("Keylight (1.2)").property("Screen Colour").setValue(myColor);
        logMessage("autoKeyer successful");
        return true;

        } catch (e) {
            logMessage("AutoKeyer Failed: " + e);
            return false;
        }
    }
    
    /**
     * Output a PNG file of the frame at 2 sec
     * @param {*} mainComp comp object
     * @param {string} outputFileName 
     * @returns 
     */
    function outputPngImage(mainComp, outputFileName){
        // export PNG of a single frame
        // var a = comp.saveFrameToPng(compTimestamp, outputFileName);
        logMessage("Begin export PNG");

        var myOutputFilePath;
        var myFilePath;
        var mySplitName;
        var compTimestamp = 2;
       
        //set preview resolution to FULL
        //app.project.item(index).resolutionFactor
        //FULL is 1,1 HALF is 2,2 QUARTER is 4,4
        mainComp.resolutionFactor = [1,1];

        myOutputFilePath = escape(imageOutputFolderPath + "/" + outputFileName)
        logMessage("Output path: " + myOutputFilePath + "");
        
        // create file object
        var myTempFile = new File(myOutputFilePath);

        try{
            app.beginSuppressDialogs();
            //EXPORT THE FILE
            mainComp.saveFrameToPng(compTimestamp, myTempFile);
            app.endSuppressDialogs(false);
            } catch (e) {
                logMessage("I broke the PNG export" + e);
                return("Export PNG Failed");
            }

        //wait 1 second for the comp to catch up to the script
        $.sleep(1000)
        logMessage("PNG export success" );
        return ""; // success
    
    }

    /**
     * utility to render a comp, requires globally set output path
     * @param {object} mainComp the composition to render
     * @param {string} outputFileName output filename
     * @returns {string} status message, will return empty string for success
     */
    function renderVideo(mainComp, outputFileName){
    	// add to render queue and render
		var myRQItem;
		var myOM;
		var myOutputFilePath;
		var myFilePath;
		var mySplitName;

		myOutputFilePath = escape(outputFolderPath + "/" + outputFileName)
        logMessage("Output path: " + myOutputFilePath + "");

		// clean out render queue to avoid errors
		for (var i = app.project.renderQueue.numItems; i > 0; i--){
			app.project.renderQueue.item(i).remove();
		}

		myRQItem = app.project.renderQueue.items.add(mainComp);
		myRQItem.applyTemplate(renderSettingsName);
        
        //TODO set work area programmatically, or set it based on an empty later because it's easy to move the work area handles accidentially
		myRQItem.timeSpanStart = mainComp.workAreaStart;
		myRQItem.timeSpanDuration = mainComp.workAreaDuration;

		myOM = myRQItem.outputModule(1);

		if (myOM.file == null){
			try{
				myOM.file = File(myOutputFilePath);
			}catch (e){
                         logMessage("Illegal output file name specified at 209");
				return("Illegal output file name specified ('" + myOutputFilePath + "') in XML file: '" + unescape(myXmlFile.name) + "'");
			}
		}

		myOM.applyTemplate(outputModuleName);

		try{
			myOM.file = File(myOutputFilePath);
		}catch (e){
              logMessage("Illegal output file name specified at 219");
			return("Illegal output file name specified ('" + myOutputFilePath + "') in XML file: '" + unescape(myXmlFile.name) + "'");
		}
		
        try{
		app.beginSuppressDialogs();
		app.project.renderQueue.render();
		app.endSuppressDialogs(false);
         } catch (e) {
             logMessage("I broke it");
             return("Render Failed");
             }
        
        return ""; // success
    
    }



// START ACTIVATION SPECIFIC PROCESSES
// START ACTIVATION SPECIFIC PROCESSES
// START ACTIVATION SPECIFIC PROCESSES
// START ACTIVATION SPECIFIC PROCESSES

/**
 * GEICO Nascar Poster 2023
 * @param {*} theXmlString 
 * @param {*} customerVideoPath 
 * @returns {string} status message, will be empty string on success
 */
function geico_nascar_poster(theXmlString, customerVideoPath){
    logMessage("using geico_nascar_poster process");
    var thisCustomer = new XML (theXmlString);

    /**
     * name of the main composition
     */
    var mainCompName = "GEICO_NASCAR_MASTER"; 
    var customerFootageCompName = "GEICO_NASCAR_CUSTOMER"; 
    var customerFootageLayerName = "CUSTOMER-RAW-LAYER"; 
    var topLayerCompName = "GEICO_NASCAR_TOP"; 
    var topLayerGenericLayerName = "layer-top-race-GENERIC";
          
    var myVideoLayer;
    var myImportOptions = new ImportOptions();
    var myFootageFile = new File();
    var myFootage;

    var topLayerFileName  = "generic";
    tempXML = thisCustomer.eventEndDate;
    if (tempXML != null && tempXML !=""){
        topLayerFileName = tempXML.toString();
        } else {
            topLayerFileName = "GENERIC";
        }
        topLayerFileName = "layer-top-" + topLayerFileName.replace("2023","race");
        logMessage("topLayerFileName = " + topLayerFileName);

    //MAKE SURE THE COMPS EXIST BEFORE PROCEEDING
    // find the template comps

    mainComp = findComp(mainCompName);
    customerFootageComp = findComp(customerFootageCompName);
    topLayerComp = findComp(topLayerCompName);
      
    if (mainComp == null | customerFootageComp == null | topLayerComp == null ){
        alert("Can't find comp in: '" + mainCompName + "' -- exiting.");
        logMessage("Can't find comp in: '" + mainCompName + "' -- exiting.");
        return("Can't find comp in: '" + mainCompName + "' -- exiting.");
        //TODO - BAIL OUT OF THIS WAIVER AND MOVE ON TO ANOTHER ONE
    }
          
    // make sure customer layer is on
    try{
        myVideoLayer = customerFootageComp.layer(customerFootageLayerName);
        myVideoLayer.enabled = true;
    }catch (e){
        logMessage("Cannot find customer layer: " + myVideoLayer + "");
        return("Cannot find customer layer: " + myVideoLayer + "");
    }
          
    // Replace the customer footage
    // myVideoLayer = mainComp.layer(customerFootageLayerName);
    //logMessage("customer footage: '" + customerVideoPath+ "'");

    try{
        myFootageFile = File(escape(customerVideoPath));
    }catch (e){
        logMessage("377 error replacing customer footage: '" + e + "'");
        return("Illegal footage file path ('" + customerVideoPath + "') in XML file");
    }
    if (! myFootageFile.exists){
        logMessage("381 error replacing customer footage: '" + e + "'");
        return("Footage file not found ('" + customerVideoPath + "') in XML file");
    }
    try{
        myImportOptions.file = myFootageFile;
        myFootage = app.project.importFile(myImportOptions);
    }catch (e){
        logMessage("388 error replacing customer footage: '" + e + "'");
        return("Footage file import failed ('" + customerVideoPath + "') in XML file");
    }
  
    try{
        myVideoLayer = customerFootageComp.layer(customerFootageLayerName);
        myVideoLayer.replaceSource(myFootage,false);
        logMessage("Replaced Customer Footage");
    }catch (e){
        logMessage("397 error replacing customer footage: '" + e + "'");
        return("error replacing customer footage: '" + e + "'");
    }
    
   //key out background
    var sampleCenter = [150,200];  // x,y offset from upper left corner
    var sampleHeight = 60;
    var sampleWidth = 200;
    keyResult = autoKeyer(myVideoLayer,sampleCenter,sampleHeight,sampleWidth);

    //update race name/location layer
    //each race has it's own PNG layer
    // loop all layers of comp and set correct layer
    logMessage("looking for layer " + topLayerFileName.toLowerCase());
    var foundLayer = false;

    for(var i = 1; i < topLayerComp.layers.length + 1; i++) {
        // logMessage("evaluating layer " + i + " : " + topLayerComp.layers[i].name);
        try{
            //turn it off first
            topLayerComp.layers[i].enabled = false;
            
            // check name, turn on if match
            if(topLayerComp.layers[i].name.toLowerCase() == topLayerFileName.toLowerCase()) {
                topLayerComp.layers[i].enabled = true;
                foundLayer = true;
                logMessage("found matching layer - using " + topLayerFileName);
            }
        }catch (e) {
            logMessage("error setting top layer: " + e);
        }
    }
    if(!foundLayer){
        //no matching layer found, use GENERIC
        topLayerComp.layer(topLayerGenericLayerName).enabled = true;
        logMessage("no matching layer - using " + topLayerGenericLayerName);
    }

    //pause a bit
    $.sleep(1000)

    logMessage("RENDER geico_nascar_poster process OUTPUT");
    // RENDER THE FILE
    var renderError = outputPngImage(mainComp, outputFileName);
    logMessage("still image output = " + renderError);
    return renderError; //success
  
}

function fsxp_2023(theXmlString, customerVideoPath){
    logMessage("using FSXP process");
    var thisCustomer = new XML (theXmlString);

    var mainCompName = "FSXP_2023_MASTER"; 
    var customerFootageCompName = "FSXP_2023_CUSTOMER_CONTENT"; 
    var customerFootageLayerName1 = "CUSTOMER_VIDEO"; 
    var variableTextCompName = "FSXP_2023_TAIL_TEXT"; 
    var variableLogoCompName = "FSXP_2023_TAIL_LOGOS"; 
          
    var myVideoLayer;
    var myImportOptions = new ImportOptions();
    var myFootageFile = new File();
    var myFootage;

    var raceLocationText  = "default";
    tempXML = thisCustomer.custom_event_text_1;
    if (tempXML != null && tempXML !=""){
        raceLocationText = tempXML.toString();
        } else {
            raceLocationText = "Firestone Experience Tour";
        }

    var raceDateText  = "2023";
    tempXML = thisCustomer.custom_event_text_2;
    if (tempXML != null && tempXML !=""){
        raceDateText = tempXML.toString();
        } else {
            raceDateText = "2023";
        }

    var logoCode  = "default";
    tempXML = thisCustomer.custom_event_text_3;
    if (tempXML != null && tempXML !=""){
        logoCode = tempXML.toString();
        } else {
            logoCode = "default";
        }

    // get event
    var event_code  = "";
    tempXML = thisCustomer.event;
    if (tempXML != null && tempXML !=""){
        event_code = tempXML.toString();
        } else {
            event_code = null;
            }

    if (event_code == "BLM5I" || event_code == "PLMZ1") {
        raceLocationText = "Firestone Grand Prix of St. Petersburg";
        raceDateText = "March 3 - 5, 2023";
        logoCode = "stp";
        }
    else if (event_code == "PLMZ1") {
        raceLocationText = "Firestone Grand Prix of St. Petersburg";
        raceDateText = "March 3 - 5, 2023";
        logoCode = "stp";
        }
          
    //MAKE SURE THE COMPS EXIST BEFORE PROCEEDING
    // find the template comps
    logMessage("using FSXP process 289");

    mainComp = findComp(mainCompName);
    customerFootageComp = findComp(customerFootageCompName);
    logoComp = findComp(variableLogoCompName);
    textComp = findComp(variableTextCompName);
      
    if (mainComp == null | customerFootageComp == null | logoComp == null | textComp == null ){
        alert("Can't find comp: '" + mainCompName + "' -- exiting.");
        return("Can't find comp: '" + mainCompName + "' -- exiting.");
        //TODO - BAIL OUT OF THIS WAIVER AND MOVE ON TO ANOTHER ONE
    }
          
    // make sure customer layer is on
    try{
        myVideoLayer = customerFootageComp.layer(customerFootageLayerName1);
        myVideoLayer.enabled = true;
    }catch (e){
        return ("Cannot find customer layer: " + myVideoLayer + "");
    }
          
    // Replace the customer footage
    // myVideoLayer = mainComp.layer(customerFootageLayerName);
    //logMessage("customer footage: '" + customerVideoPath+ "'");
    logMessage("using FSXP process 313");

    try{
        myFootageFile = File(escape(customerVideoPath));
        logMessage("using FSXP process 317");
    }catch (e){
        logMessage("319 error replacing customer footage: '" + e + "'");
        return("Illegal footage file path ('" + customerVideoPath + "') in XML file");
    }
    if (! myFootageFile.exists){
        logMessage("323 error replacing customer footage: '" + e + "'");
        return("Footage file not found ('" + customerVideoPath + "') in XML file");
    }
    try{
        myImportOptions.file = myFootageFile;
        myFootage = app.project.importFile(myImportOptions);
        logMessage("using FSXP process 329");
    }catch (e){
        logMessage("331 error replacing customer footage: '" + e + "'");
        return("Footage file import failed ('" + customerVideoPath + "') in XML file");
    }
  
    try{
        myVideoLayer = customerFootageComp.layer(customerFootageLayerName1);
        myVideoLayer.replaceSource(myFootage,false);
        logMessage("Replaced Customer Footage");
    }catch (e){
        logMessage("340 error replacing customer footage: '" + e + "'");
        return("error replacing customer footage: '" + e + "'");
    }
    
    logMessage("using FSXP process 342");
    //set correct logo

    logMessage("logoCode = " + logoCode.toLowerCase());
    try{
        myVideoLayer = logoComp.layer("LOGO_GENERIC");
        if(logoCode.toLowerCase() == "default"){
        myVideoLayer.enabled = true;
        }else {
        myVideoLayer.enabled = false; 
        }
    }catch (e){
        logMessage("356 cannot swap logo: '" + e + "'");
        return ("Cannot find customer layer: " + myVideoLayer + "");
    }
    try{
        myVideoLayer = logoComp.layer("LOGO_ST-PETE");
        if(logoCode.toLowerCase() == "stp"){
            myVideoLayer.enabled = true;
            }else {
            myVideoLayer.enabled = false; 
            }
    }catch (e){
        logMessage("367 cannot swap logo: '" + e + "'");
        return ("Cannot find customer layer: " + myVideoLayer + "");
    }
    try{
        myVideoLayer = logoComp.layer("LOGO_MONTEREY");
        if(logoCode.toLowerCase() == "monterey"){
            myVideoLayer.enabled = true;
            }else {
            myVideoLayer.enabled = false; 
            }
    }catch (e){
        logMessage("378 cannot swap logo: '" + e + "'");
        return ("Cannot find customer layer: " + myVideoLayer + "");
    }

    //update race name/location layer
    logMessage("using FSXP process 383");

    logMessage("Set location text layer to " + raceLocationText);
    myVideoLayer = textComp.layer("RACE_NAME");
    replaceText(myVideoLayer,raceLocationText.toUpperCase(),true); 
    myVideoLayer.enabled = true;

    //update race date layer
    logMessage("Set date text layer to " + raceDateText);
    myVideoLayer = textComp.layer("RACE_DATES");
    replaceText(myVideoLayer,raceDateText.toUpperCase(),true); 
    myVideoLayer.enabled = true;


    var stillExport = outputPngImage(mainComp, outputFileName);

    logMessage("RENDER FSXP process OUTPUT");
    // RENDER THE FILE
    var renderError = renderVideo(mainComp,outputFileName);
    return renderError; //success
  
}

function reliantRodeo(theXmlString, customerVideoPath){
    logMessage("using RODEO process");
    var thisCustomer = new XML (theXmlString);

    var creativeChoice  = "";
    tempCustomerXML = thisCustomer.uniqueOverlayCode;
    if (tempCustomerXML != null && tempCustomerXML !=""){
        creativeChoice = tempCustomerXML.toString();
    } else {
        creativeChoice = "";
    }

    if(creativeChoice == "concert"){
        var mainCompName = "RODEO_CONCERT_MASTER"; 
        var customerFootageCompName = "RODEO_CONCERT_CUSTOMER"; 
        var customerFootageLayerName1 = "customer_layer"; 
        logMessage("using concert creative");
    } else {
        var mainCompName = "RODEO_BULL_MASTER"; 
        var customerFootageCompName = "RODEO_BULL_MASTER"; 
        var customerFootageLayerName1 = "customer_layer"; 
        logMessage("using bull creative");
    }

    var myOverlaylayerName = "side_posts";
          
    var myVideoLayer;
    var myImportOptions = new ImportOptions();
    var myFootageFile = new File();
    var myFootage;
          
    //MAKE SURE THE COMPS EXIST BEFORE PROCEEDING
    // find the template comps
    mainComp = findComp(mainCompName);
    customerFootageComp = findComp(customerFootageCompName);
      
    if (mainComp == null | customerFootageComp == null ){
        alert("Can't find comp: '" + mainCompName + "' -- exiting.");
        return("Can't find comp: '" + mainCompName + "' -- exiting.");
        //TODO - BAIL OUT OF THIS WAIVER AND MOVE ON TO ANOTHER ONE
    }
          
    // first iterate through and turn off all the layers
    try{
        myVideoLayer = customerFootageComp.layer(customerFootageLayerName1);
        myVideoLayer.enabled = true;
    }catch (e){
        return ("Cannot find customer layer: " + myVideoLayer + "");
    }
          
    // Replace the customer footage
    // myVideoLayer = mainComp.layer(customerFootageLayerName);
    //logMessage("customer footage: '" + customerVideoPath+ "'");
  
    try{
        myFootageFile = File(escape(customerVideoPath));
    }catch (e){
        return("Illegal footage file path ('" + customerVideoPath + "') in XML file");
    }
    if (! myFootageFile.exists){
            return("Footage file not found ('" + customerVideoPath + "') in XML file");
    }
    try{
        myImportOptions.file = myFootageFile;
        myFootage = app.project.importFile(myImportOptions);
    }catch (e){
        return("Footage file import failed ('" + customerVideoPath + "') in XML file");
    }
  
    myVideoLayer = customerFootageComp.layer(customerFootageLayerName1);
    try{
        myVideoLayer.replaceSource(myFootage,false);
        logMessage("Replaced Customer Footage");
    }catch (e){
        return("error replacing customer footage: '" + e + "'");
    }
    
    logMessage("using RELIANT RODEO process 3");
    //turn posts on/off
    if(creativeChoice == "bullNoPost"){
        try{
            myVideoLayer = mainComp.layer(myOverlaylayerName);
            myVideoLayer.enabled = false;
        }catch (e){
            return ("Cannot find customer layer: " + myVideoLayer + "");
        }
    } else if(creativeChoice == "bullWithPost"){
        try{
            myVideoLayer = mainComp.layer(myOverlaylayerName);
            myVideoLayer.enabled = true;
        }catch (e){
            return ("Cannot find customer layer: " + myVideoLayer + "");
        }
    }

    logMessage("using RELIANT RODEO process 4");
    // RENDER THE FILE
    var renderError = renderVideo(mainComp,outputFileName);
    return renderError; //success
  
}

function nbcOial(theXmlString, customerVideoPath){
  logMessage("using NBC OIAL process");
var mainCompName = "NBC_OIAL_MASTER_FLAT"; //override this in activation specific function
var customerFootageCompName = "NBC_OIAL_CUSTOMER"; //override this in activation specific function
var customerFootageLayerName1 = "customer_footage_1"; //override this in activation specific function
var customerFootageLayerName2 = "customer_footage_2"; //override this in activation specific function        

        
		var myVideoLayer;
		var myImportOptions = new ImportOptions();
		var myFootageFile = new File();
		var myFootage;
        
        //MAKE SURE THE COMPS EXIST BEFORE PROCEEDING
        	// find the template comps
	mainComp = findComp(mainCompName);
    customerFootageComp = findComp(customerFootageCompName);
    
        if (mainComp == null | customerFootageComp == null ){
		alert("Can't find comp: '" + mainCompName + "' -- exiting.");
		return("Can't find comp: '" + mainCompName + "' -- exiting.");
        //TODO - BAIL OUT OF THIS WAIVER AND MOVE ON TO ANOTHER ONE
	}
        
         // first iterate throuhg and turn off all the layers
         try{
    	myVideoLayer = customerFootageComp.layer(customerFootageLayerName1);
    	myVideoLayer.enabled = true;
        }catch (e){
            return ("Cannot find customer layer: " + myVideoLayer + "");
            }
          // first iterate throuhg and turn off all the layers
         try{
    	myVideoLayer = customerFootageComp.layer(customerFootageLayerName2);
    	myVideoLayer.enabled = true;
        }catch (e){
            return ("Cannot find customer layer: " + myVideoLayer + "");
            }
        
         // Replace the customer footage
		// myVideoLayer = mainComp.layer(customerFootageLayerName);
         //logMessage("customer footage: '" + customerVideoPath+ "'");

		try{
			myFootageFile = File(escape(customerVideoPath));
		}catch (e){
			return("Illegal footage file path ('" + customerVideoPath + "') in XML file");
		}
		if (! myFootageFile.exists){
                return("Footage file not found ('" + customerVideoPath + "') in XML file");
		}
		try{
			myImportOptions.file = myFootageFile;
			myFootage = app.project.importFile(myImportOptions);
		}catch (e){
			return("Footage file import failed ('" + customerVideoPath + "') in XML file");
		}

        myVideoLayer = customerFootageComp.layer(customerFootageLayerName1);
        try{
		myVideoLayer.replaceSource(myFootage,false);
        logMessage("Replaced Customer Footage");
        }catch (e){
           return("error replacing customer footage: '" + e + "'");
            }
        
        myVideoLayer = customerFootageComp.layer(customerFootageLayerName2);
        try{
		myVideoLayer.replaceSource(myFootage,false);
        logMessage("Replaced Customer Footage");
        }catch (e){
           return("error replacing customer footage: '" + e + "'");
            }
        
        // RENDER THE FILE
        var renderError = renderVideo(mainComp,outputFileName);

        return renderError; //success

    }


function geico(theXmlString, customerVideoPath){
  logMessage("using geico comicon process");
var mainCompName = "GEICO_MARVEL_MASTER"; //override this in activation specific function
var customerFootageCompName = "GEICO_MARVEL_CUSTOMER"; //override this in activation specific function
var customerFootageLayerName = "customer_footage"; //override this in activation specific function        
        
        
        
		var myVideoLayer;
		var myImportOptions = new ImportOptions();
		var myFootageFile = new File();
		var myFootage;
        
        //MAKE SURE THE COMPS EXIST BEFORE PROCEEDING
        	// find the template comps
	mainComp = findComp(mainCompName);
    customerFootageComp = findComp(customerFootageCompName);
    
        if (mainComp == null | customerFootageComp == null ){
		alert("Can't find comp: '" + mainCompName + "' -- exiting.");
		return("Can't find comp: '" + mainCompName + "' -- exiting.");
        //TODO - BAIL OUT OF THIS WAIVER AND MOVE ON TO ANOTHER ONE
	}
        
         // first iterate throuhg and turn off all the layers
         try{
    	myVideoLayer = customerFootageComp.layer(customerFootageLayerName);
    	myVideoLayer.enabled = true;
        }catch (e){
            return ("Cannot find customer layer: " + myVideoLayer + "");
            }
        
         // Replace the customer footage
		// myVideoLayer = mainComp.layer(customerFootageLayerName);
         //logMessage("customer footage: '" + customerVideoPath+ "'");

		try{
			myFootageFile = File(escape(customerVideoPath));
		}catch (e){
			return("Illegal footage file path ('" + customerVideoPath + "') in XML file");
		}
		if (! myFootageFile.exists){
                return("Footage file not found ('" + customerVideoPath + "') in XML file");
		}
		try{
			myImportOptions.file = myFootageFile;
			myFootage = app.project.importFile(myImportOptions);
		}catch (e){
			return("Footage file import failed ('" + customerVideoPath + "') in XML file");
		}

        myVideoLayer = customerFootageComp.layer(customerFootageLayerName);
        try{
		myVideoLayer.replaceSource(myFootage,false);
        logMessage("Replaced Customer Footage");
        }catch (e){
           return("error replacing customer footage: '" + e + "'");
            }
        
        // RENDER THE FILE
        var renderError = renderVideo(mainComp,outputFileName);

        return renderError; //success

    }



function snfPlayerIntro(theXmlString, customerVideoPath){
  logMessage("using SNF Player Intro process");
var mainCompName = "SNF_PLAYER_INTRO"; //override this in activation specific function
var customerFootageCompName = "SNF_PLAYER_INTRO"; //override this in activation specific function
var customerFootageLayerName = "customer"; //override this in activation specific function       
var customerNameLayerName = "name_box"; //override this in activation specific function        

        
		var myVideoLayer;
		var myImportOptions = new ImportOptions();
		var myFootageFile = new File();
		var myFootage;
         var thisCustomer = new XML (theXmlString);

        
        //MAKE SURE THE COMPS EXIST BEFORE PROCEEDING
        	// find the template comps
	mainComp = findComp(mainCompName);
    customerFootageComp = findComp(customerFootageCompName);
    
        if (mainComp == null | customerFootageComp == null ){
		alert("Can't find comp: '" + mainCompName + "' -- exiting.");
		return("Can't find comp: '" + mainCompName + "' -- exiting.");
        //TODO - BAIL OUT OF THIS WAIVER AND MOVE ON TO ANOTHER ONE
	}

   var customerFirstNameString  = "";
    tempCustomerXML = thisCustomer.firstName;
    if (tempCustomerXML != null && tempCustomerXML !=""){
        customerFirstNameString = tempCustomerXML.toString();
        } else {
            customerFirstNameString = "";
            }
        
           var customerLastNameString  = "";
    tempCustomerXML = thisCustomer.lastName;
    if (tempCustomerXML != null && tempCustomerXML !=""){
        customerLastNameString = tempCustomerXML.toString();
        } else {
            customerLastNameString = "";
            }
        
        var customerFullName = customerFirstNameString + " " + customerLastNameString;

    
    // Replace customer name & to UPPER CASE
    		myVideoLayer = mainComp.layer(customerNameLayerName);
              logMessage("Set custoer_name layer to " + customerFullName);
    		myVideoLayer.enabled = true;
			replaceText(myVideoLayer,customerFullName.toUpperCase(),true);  
        
        
         // first iterate throuhg and turn off all the layers
         try{
    	myVideoLayer = customerFootageComp.layer(customerFootageLayerName);
    	myVideoLayer.enabled = true;
        }catch (e){
            return ("Cannot find customer layer: " + myVideoLayer + "");
            }
        
         // Replace the customer footage
		// myVideoLayer = mainComp.layer(customerFootageLayerName);
         //logMessage("customer footage: '" + customerVideoPath+ "'");

		try{
			myFootageFile = File(escape(customerVideoPath));
		}catch (e){
			return("Illegal footage file path ('" + customerVideoPath + "') in XML file");
		}
		if (! myFootageFile.exists){
                return("Footage file not found ('" + customerVideoPath + "') in XML file");
		}
		try{
			myImportOptions.file = myFootageFile;
			myFootage = app.project.importFile(myImportOptions);
		}catch (e){
			return("Footage file import failed ('" + customerVideoPath + "') in XML file");
		}

        myVideoLayer = customerFootageComp.layer(customerFootageLayerName);
        try{
		myVideoLayer.replaceSource(myFootage,false);
        logMessage("Replaced Customer Footage");
        }catch (e){
           return("error replacing customer footage: '" + e + "'");
            }
        
        // UPDATE KEY COLOR
        //TODO make a method for the autokeyer
        
        var samplePos = [1800,250];  // offset from upper left corner
        var sampleRadius = 20;
        
        // add temporary color control to layer
        var myColorControl = myVideoLayer.property("Effects").addProperty("ADBE Color Control");
        var myColorProp = myColorControl.property("ADBE Color Control-0001");
        myColorProp.expression = "sampleImage(" + samplePos.toSource() + ",[" +  sampleRadius + "," + sampleRadius + "],false);";
	
        // harvest the result
        var myColor = myColorProp.value;
	
        // remove the color control
        myColorControl.remove();
	
        // apply the color to the Keylight efect
        myVideoLayer.property("Effects").property("Keylight (1.2)").property("Screen Colour").setValue(myColor);
        
        // RENDER THE FILE
        var renderError = renderVideo(mainComp,outputFileName);

        return renderError; //success

    }

function netflixGCC(theXmlString, customerVideoPath){
  logMessage("using netflixGCC process");
var mainCompName = "netflix_master_overlay"; //override this in activation specific function
var customerFootageCompName = "netflix_master_clean"; //override this in activation specific function
var customerFootageLayerName = "customer"; //override this in activation specific function       
        
		var myVideoLayer;
		var myImportOptions = new ImportOptions();
		var myFootageFile = new File();
		var myFootage;
         var thisCustomer = new XML (theXmlString);

         

        
        //MAKE SURE THE COMPS EXIST BEFORE PROCEEDING
        	// find the template comps
	mainComp = findComp(mainCompName);
    customerFootageComp = findComp(customerFootageCompName);
    
        if (mainComp == null | customerFootageComp == null ){
		alert("Can't find comp: '" + mainCompName + "' -- exiting.");
          logMessage("Can't find comp: '" + mainCompName + "' -- exiting.");

		return("Can't find comp: '" + mainCompName + "' -- exiting.");
        //TODO - BAIL OUT OF THIS WAIVER AND MOVE ON TO ANOTHER ONE
	}

   var selectedBackground  = "";
    tempCustomerXML = thisCustomer.selectedBackground;
    if (tempCustomerXML != null && tempCustomerXML !=""){
        selectedBackground = tempCustomerXML.toString();
        } else {
            selectedBackground = "netflix_background_1";
        }
    
var skipKeyer = "false"; //override this in activation specific function     
    tempCustomerXML = thisCustomer.skipKeyer;
    if (tempCustomerXML != null && tempCustomerXML !=""){
        skipKeyer = tempCustomerXML.toString();
        } else {
            skipKeyer = "false";
        }
    
    //make sure it's a valid BG name
    if(selectedBackground == "netflix_background_1" ||
        selectedBackground == "netflix_background_2" ||
        selectedBackground == "netflix_background_3" ||
        selectedBackground == "netflix_background_4" ||
        selectedBackground == "netflix_background_5"
    ){
        //do nothing, valid background
        }else {
            selectedBackground = "netflix_background_1";
            }
        

        //set correct BG layer
         var bgLayer1 = customerFootageComp.layer("netflix_background_1");
         var bgLayer2 = customerFootageComp.layer("netflix_background_2");         
         var bgLayer3 = customerFootageComp.layer("netflix_background_3");
         var bgLayer4 = customerFootageComp.layer("netflix_background_4");
         var bgLayer5 = customerFootageComp.layer("netflix_background_5");
         var selectedBackgroundLayer = customerFootageComp.layer(selectedBackground);
        
         // first iterate throuhg and turn off all the layers
         try{
    	bgLayer1.enabled = false;
        bgLayer2.enabled = false;
        bgLayer3.enabled = false;
        bgLayer4.enabled = false;
        bgLayer5.enabled = false;
        
        //TODO need to check that layer exists first before trying to turn on
        //in catch set to default BG
        customerFootageComp.layer(selectedBackground).enabled = true;
        }catch (e){
           // return ("Cannot set BG layer: " + myVideoLayer + "" + e);
           logMessage("Cannot set BG layer: " + myVideoLayer + "" + e);
           bgLayer1.enabled = true;

            }
        
         // Replace the customer footage
		// myVideoLayer = mainComp.layer(customerFootageLayerName);
         //logMessage("customer footage: '" + customerVideoPath+ "'");

		try{
			myFootageFile = File(escape(customerVideoPath));
		}catch (e){
			return("Illegal footage file path ('" + customerVideoPath + "') in XML file");
		}
		if (! myFootageFile.exists){
                return("Footage file not found ('" + customerVideoPath + "') in XML file");
		}
		try{
			myImportOptions.file = myFootageFile;
			myFootage = app.project.importFile(myImportOptions);
		}catch (e){
			return("Footage file import failed ('" + customerVideoPath + "') in XML file");
		}

        myVideoLayer = customerFootageComp.layer(customerFootageLayerName);
        try{
		myVideoLayer.replaceSource(myFootage,false);
        logMessage("Replaced Customer Footage");
        }catch (e){
           return("error replacing customer footage: '" + e + "'");
            }
        
        
        // UPDATE KEY COLOR
        //TODO make a method for the autokeyer
        logMessage("skipKeyer = " + skipKeyer);
        if(skipKeyer != "true"){
        var samplePos = [250,50];  // offset from upper left corner
        var sampleRadius = 20;
        
        // add temporary color control to layer
        var myColorControl = myVideoLayer.property("Effects").addProperty("ADBE Color Control");
        var myColorProp = myColorControl.property("ADBE Color Control-0001");
        myColorProp.expression = "sampleImage(" + samplePos.toSource() + ",[" +  sampleRadius + "," + sampleRadius + "],false);";
	
        // harvest the result
        var myColor = myColorProp.value;
	
        // remove the color control
        myColorControl.remove();
	
        // apply the color to the Keylight efect
        myVideoLayer.property("Effects").property("Keylight (1.2)").property("Screen Colour").setValue(myColor);
        } else{
       logMessage("Skipping Keyer");
       myVideoLayer.property("Effects").property("Keylight (1.2)").remove();
       myVideoLayer.property("Effects").property("Advanced Spill Suppressor").remove();

            }
        //END KEYER
        
        //TODO set comp length to source footage duration
        
        //Get duration of heads & tails - we will add customer duration to them to geet total duration
        //for Netflix, there are no heads or tails, so we can just set them to zero
       //myHeadsDurationLayer = customerFootageComp.layer("heads_duration");
        //myTailsLayer = customerFootageComp.layer("dish_tails");
           //var heads_duration =  (myHeadsDurationLayer.outPoint - myHeadsDurationLayer.inPoint);
          // var tails_duration = (myTailsLayer.outPoint - myTailsLayer.inPoint);
           var heads_duration = 0;
            var tails_duration = -.3;
            
        //get source duration
          var d =  (myVideoLayer.outPoint - myVideoLayer.inPoint);
      
          logMessage("Heads Duration: '" + heads_duration + "'");
          logMessage("Source Duration: '" + d + "'");
          logMessage("Tails Duration: '" + tails_duration + "'");
          
          var totalWorkAreaDuration = (heads_duration + d + tails_duration);
           logMessage("Work Area Duration: '" + totalWorkAreaDuration + "'");
           
           if(totalWorkAreaDuration >=60){
                   //total cannot be longer than comp so may need to do something here
                   totalWorkAreaDuration = 60
                   }
           
        // adjust main comp workarea duration
              mainComp.workAreaStart = 0;
              mainComp.workAreaDuration = totalWorkAreaDuration;
              customerFootageComp.workAreaStart = 0;
              customerFootageComp.workAreaDuration = totalWorkAreaDuration;
              
        //move tails to correct position
        //no tails on Netflix, so no layer to move
             //myTailsLayer.startTime = (heads_duration + d);        
        
        
        
        
        //TODO render BOTH versions (with and without overlay)
        var renderError = renderError = renderVideo(customerFootageComp, outputFileName.toString().replace('.mp4', '_clean.mov'));
        if(renderError != "") return renderError; //failed on first export
            
        // renderError = renderVideo(customerFootageComp, outputFileName.toString().replace('.mp4', '_clean.mov'));
       renderError = renderVideo(mainComp,outputFileName);

        return renderError; //success

    }

// END ACTIVATION SPECIFIC PROCESSES
// END ACTIVATION SPECIFIC PROCESSES
// END ACTIVATION SPECIFIC PROCESSES
// END ACTIVATION SPECIFIC PROCESSES
// END ACTIVATION SPECIFIC PROCESSES
// END ACTIVATION SPECIFIC PROCESSES


// START processFile()
// START processFile()
// START processFile()

	function processFile(theXmlString){
        
      // make sure template file is open
      //TODO this would move into activation specific script in future version where we open different projects
       logMessage("XML: " + theXmlString);

        var myJob = new XML (theXmlString);
        var tempXML;
    
    //get PATE - program, activation, team, event
    // get program
    var program_code = "";
    tempXML = myJob.program;
    if (tempXML != null && tempXML !=""){
        program_code = tempXML.toString();
        } else {
            program_code = null;
            }
    // get activation
    var activation_code  = "";
    tempXML = myJob.activation;
    if (tempXML != null && tempXML !=""){
        activation_code = tempXML.toString();
        } else {
            activation_code = null;
            }
    // get team
    var team_code  = "";
    tempXML = myJob.team;
    if (tempXML != null && tempXML !=""){
        team_code = tempXML.toString();
        } else {
            team_code = null;
            }
    // get event
    var event_code  = "";
    tempXML = myJob.event;
    if (tempXML != null && tempXML !=""){
        event_code = tempXML.toString();
        } else {
            event_code = null;
            }
    
    // get output file name
	tempXML = myJob.OutputFileName;
	if (tempXML != null){
		outputFileName = tempXML.toString();
		}
    
    // get customer video path
	tempXML = myJob.customer_footage;
    if (tempXML != null && tempXML !=""){
	customerVideoPath = ('/Users/' + computerUserName + '/Documents/media_pool/' + (tempXML.toString()));
     } else {
         customerVideoPath = ('/Users/' + computerUserName + '/Documents/media_pool/' + outputFileName + ".mp4");
         }
     
     //change path to .mov to handle transcoded files from AME to get around VFR issue in After Effects
     customerVideoPathMOV = customerVideoPath.substr(0, customerVideoPath.lastIndexOf(".")) + ".mov";
    //use when project takes JPG file as source material
     customerVideoPathJPG = customerVideoPath.substr(0, customerVideoPath.lastIndexOf(".")) + ".jpg";

//PROCESS FILE BASED ON ACTIVATION SPECIFIC FUNCTION

//Delay a bit to make sure raw file is moved into place
 $.sleep(250)

if(activation_code == "seresto_2018" || activation_code == "seresto_2019") {
var processFileError =  seresto(theXmlString, customerVideoPath);
} 

else if (team_code == "geico_events_1Q17_nascar") {
    var processFileError =  geico_nascar_poster(theXmlString, customerVideoPath);
    }
else if (activation_code == "geico_events_1Q17") {
    var processFileError =  geico_nascar_poster(theXmlString, customerVideoPath);
    }

else if (activation_code == "penguins_2018-2019") {
    var processFileError =  penguins(theXmlString, customerVideoPath);
    }
else if (activation_code == "uscc_packers_2019") {
    var processFileError =  packersFowling(theXmlString, customerVideoPath);
    }
else if (activation_code == "snf_2019") {
    var processFileError =  snfPlayerIntro(theXmlString, customerVideoPath);
    }
else if (activation_code == "netflix_gcc_2021" ) {
    var processFileError =  netflixGCC(theXmlString, customerVideoPathMOV);
    }

else if (activation_code == "nbc_oial_2021" ) {
    var processFileError =  nbcOial(theXmlString, customerVideoPathMOV);
    }
else if (activation_code == "reliant_rodeo_2022" ) {
    var processFileError =  reliantRodeo(theXmlString, customerVideoPathJPG);
    }
else if (activation_code == "fsxp_indycar_2023" ) {
    var processFileError =  fsxp_2023(theXmlString, customerVideoPath);
    }

else {
    return ("error - no process found for activation " + activation_code + "");
    }
    
	return processFileError; // success
	
    //end of processFile
    }

    // end processFile() function block
    
// START CheckForXML()
// START CheckForXML()
// START CheckForXML()

function CheckForXML(){
    
    if(devMode) logMessage("start CheckForXML");
    var templateFile = app.project.file; //TODO in future version we need to open/close specific projects based on activation

 // make sure output folder is not null
		if (outputFolderPath == ""){
			return ("No output folder defined: '" +  "'");
		}

  // make sure output folder is not null
		var myOutputFolder = new Folder(escape(outputFolderPath));
		if (! myOutputFolder.exists){
            logMessage("Output folder does not exist: '" + outputFolderPath);
		    return ("Output folder does not exist: '" + outputFolderPath);
		}

    // make sure output folder is not null
    if (imageOutputFolderPath == ""){
        logMessage ("No output folder defined: '" +  "'");
        return ("No output folder defined: '" +  "'");
    }
    // make sure output folder is not null
    var myImageOutputFolder = new Folder(escape(imageOutputFolderPath));
    if (! myImageOutputFolder.exists){
        logMessage ("Output folder does not exist: '" + imageOutputFolderPath);
         return ("Output folder does not exist: '" + imageOutputFolderPath);
    }

    var myFiles = app.bp_watchFolder.getFiles();
    var myXmlFile = new File();

	if (myFiles.length > 0){
		var mySplit;
		myXmlFile = null;
		for (var myIdx = 0; myIdx < myFiles.length; myIdx++){
			if (myFiles[myIdx] instanceof File){
				mySplit = unescape(myFiles[myIdx].name).split(".");
				if (mySplit[mySplit.length-1].toLowerCase() == "xml"){
					if (myXmlFile == null){
						myXmlFile = myFiles[myIdx];
					}else if (mySplit[0] < unescape(myXmlFile.name).split(".")[0]){
						myXmlFile = myFiles[myIdx];
					}
				}
			}
		}
	}
    
    //IF XML IS VALID, PROCEED TO PROCESSING
    if (myXmlFile != null && myXmlFile.exists){
		// found an xml file--read it
		var myError = false;

		var fileOK = myXmlFile.open("r");
		if (! fileOK){
			logMessage("Error opening XML file: '" + unescape(myXmlFile.name) + "'");
			myError = true;
		}
		try{
			var myXmlString = myXmlFile.read();
		}catch (err){
			logMessage("Error reading XML file: '" + unescape(myXmlFile.name) + "'");
			myError = true;
		}finally{
			myXmlFile.close();
		}

		if (! myError){
            //WAIVER IS VALID, PROCESS IT
			var myErrorMsg = processFile(myXmlString);
			if (myErrorMsg != ""){
				logMessage(myErrorMsg);
				myError = true;
			}
		}

		// SUCCESS move waiver file to completed folder
		if (! myError){
			// move xml file to "complete" folder (create one if it doesn't exist)
			var myProcessedFolderPath = app.bp_watchFolder.path + "/" + app.bp_watchFolder.name + "/" + "complete";
			var myProcessedFolder = new Folder(myProcessedFolderPath);
			if (! myProcessedFolder.exists){
				myProcessedFolder.create();
			}
			myXmlFile.copy(myProcessedFolderPath + "/" + myXmlFile.name);
			logMessage("Successfully processed XML file: '" + unescape(myXmlFile.name) + "'");
		}else{

		// ERROR move waiver file to error folder
			var myErrorFolderPath = app.bp_watchFolder.path + "/" + app.bp_watchFolder.name + "/" + "error";
			var myErrorFolder = new Folder(myErrorFolderPath);
			if (! myErrorFolder.exists){
				myErrorFolder.create();
			}
			myXmlFile.copy(myErrorFolderPath + "/" + myXmlFile.name);
            //TODO - can we send an http command here to alert the error?
		}

		// remove xml file in either case
		myXmlFile.remove();

		// close and reload the template
        //TODO - this is what we would move around to open project based on waiver details, instead of monolithic project
         logMessage("closing project");
		app.project.close(CloseOptions.DO_NOT_SAVE_CHANGES);
		
        
                //desperate attempt to prevent AE from randomly freezing
        logMessage("Sleep 250ms before opening project");
        $.sleep(250)
        logMessage("opening project");
		app.open(templateFile);
        
        //experimental - clear the cache
        logMessage("purging caches");
        app.purge(PurgeTarget.ALL_CACHES);
        app.purge(PurgeTarget.UNDO_CACHES);
        app.purge(PurgeTarget.SNAPSHOT_CACHES);
        app.purge(PurgeTarget.IMAGE_CACHES);

		// take short break, then look for another job
         logMessage("wait 250ms for oneBigLoop");
		app.scheduleTask("app.bp_oneBigLoop();",250,false);


	}
    //NO VALID WAIVERS FOUND
    else{  
        // no xml files found -- go to sleep for a while
        if (!devMode){
          logMessage("End of oneBigLoop. User is:" + computerUserName + " | no waivers found - waiting " + loopDelay + "ms");
        app.scheduleTask("app.bp_oneBigLoop();",loopDelay,false);
        }else {
          logMessage("DEV MODE!!!!!! user is:" + computerUserName + " | no waivers found - exiting loop");
            }
	}

}

// END OF CheckForXML()

}

// END OF oneBigLoop()
