FasdUAS 1.101.10   ��   ��    k             l     ��  ��    H B multi-processor oriented Applescript for Pre-Ingestion processing     � 	 	 �   m u l t i - p r o c e s s o r   o r i e n t e d   A p p l e s c r i p t   f o r   P r e - I n g e s t i o n   p r o c e s s i n g   
  
 l     ��������  ��  ��        j     �� �� 0 theplistpath thePListPath  m        �   � / U s e r s / b l u e p i x e l / D o c u m e n t s / p r o c e s s _ c o n f i g / o r c a v i e w _ 3 6 0 _ p r o c e s s _ c o n f i g . p l i s t      j    
�� �� 00 incomingfileextensions incomingFileExtensions  J    	       m       �    . j p g      m       �    . p n g      m       �      . m p 4   !�� ! m     " " � # #  . m o v��     $ % $ j    �� &�� 80 rawtranscodefileextensions rawTranscodeFileExtensions & J     ' '  ( ) ( m     * * � + +  . m p 4 )  ,�� , m     - - � . .  . m o v��   %  / 0 / j    �� 1�� D0  bypassrawtranscodefileextensions  bypassRawTranscodeFileExtensions 1 J     2 2  3 4 3 m     5 5 � 6 6  . j p g 4  7�� 7 m     8 8 � 9 9  . p n g��   0  : ; : j    �� <�� 60 statsintervalcheckminutes statsIntervalCheckMinutes < m    ����  ;  = > = j    �� ?�� $0 processdelaysecs processDelaySecs ? m    ����  >  @ A @ j    !�� B�� 0 loopdelaysecs loopDelaySecs B m     ���� 
 A  C D C j   " $�� E�� 0 	automated   E m   " #��
�� boovfals D  F G F j   % '�� H�� 0 devendpoints devEndpoints H m   % &��
�� boovfals G  I J I j   ( ,�� K�� $0 computerusername computerUserName K m   ( + L L � M M  b l u e p i x e l J  N O N j   - 1�� P�� $0 waivergrepstring waiverGrepString P m   - 0 Q Q � R R  - e   . x m l O  S T S j   2 6�� U�� >0 filesizecheck_minaeoutputsize filesizeCheck_minAEOutputSize U m   2 5���� ��@ T  V W V j   7 ;�� X�� (0 filesizecheckdelay filesizeCheckDelay X m   7 :���� 
 W  Y Z Y j   < >�� [�� (0 performfilecleanup performFileCleanup [ m   < =��
�� boovtrue Z  \ ] \ j   ? A�� ^�� 0 dev_mode   ^ m   ? @��
�� boovfals ]  _ ` _ l      a b c a j   B F�� d�� 00 oldestfileagethreshold oldestFileAgeThreshold d m   B E����  b  seconds    c � e e  s e c o n d s `  f g f l      h i j h j   G M�� k�� 00 expectedmaxprocesstime expectedMaxProcessTime k m   G J���� x i  seconds    j � l l  s e c o n d s g  m n m j   N T�� o�� "0 applicationname applicationName o m   N Q p p � q q   O r c a v i e w   P r o c e s s n  r s r l     ��������  ��  ��   s  t u t p   U U v v ������ 00 pendingaewaiverlistold pendingAEWaiverListOLD��   u  w x w p   U U y y ������ .0 pendingamefilelistold pendingAMEFileListOLD��   x  z { z p   U U | | ������ :0 pendingdropboxwaiverlistold pendingDropboxWaiverListOLD��   {  } ~ } l     ��������  ��  ��   ~   �  p   U U � � �� ��� "0 dropboxrootpath dropboxRootPath � �� ��� .0 incomingrawxmlwaivers incomingRawXmlWaivers � ������ :0 incomingrawmediafolderposix incomingRawMediaFolderPosix��   �  � � � p   U U � � ������ 0 stationtype stationType��   �  � � � p   U U � � �� ��� :0 transcodependingfolderposix transcodePendingFolderPosix � ������ >0 transcodecompletedfolderposix transcodeCompletedFolderPosix��   �  � � � p   U U � � �� ��� @0 pendingwaiversforaefolderposix pendingWaiversForAEFolderPosix � ������ 0 
media_pool  ��   �  � � � p   U U � � �� ��� 0 computer_id   � ������ 0 ishappy isHappy��   �  � � � p   U U � � �� ��� 0 processed_media   � �� ��� 0 	ae_output   � �� ��� 0 	ame_watch   � ������ 0 spock_ingestor  ��   �  � � � p   U U � � ������ 80 laststatsgatheredtimestamp lastStatsGatheredTimestamp��   �  � � � p   U U � � ������ :0 laststalefilechecktimestamp lastStaleFileCheckTimestamp��   �  � � � l     ��������  ��  ��   �  � � � l     ��������  ��  ��   �  � � � i   U X � � � I     ������
�� .aevtoappnull  �   � ****��  ��   � k     � � �  � � � I    �� ���
�� .ascrcmnt****      � **** � b      � � � l     ����� � I    ������
�� .misccurdldt    ��� null��  ��  ��  ��   � m     � � � � �  L a u n c h i n g��   �  � � � I    �� ����� $0 shownotification showNotification �  � � � m     � � � � �  L a u n c h i n g �  ��� � m     � � � � � , c h e c k i n g   s y s t e m   s t a t u s��  ��   �  � � � l   ��������  ��  ��   �  � � � Z    8 � ����� � =     � � � I    �� ����� 0 
fileexists 
fileExists �  ��� � o    ���� 0 theplistpath thePListPath��  ��   � m    ��
�� boovfals � k   # 4 � �  � � � I  # .�� ���
�� .sysodisAaleR        TEXT � b   # * � � � m   # $ � � � � � P A b o r t i n g   b e c a u s e   c o n f i g   f i l e   n o t   f o u n d :   � o   $ )���� 0 theplistpath thePListPath��   �  ��� � I  / 4������
�� .aevtquitnull��� ��� null��  ��  ��  ��  ��   �  � � � l  9 9��������  ��  ��   �  � � � r   9 < � � � m   9 :��
�� boovtrue � o      ���� 0 ishappy isHappy �  � � � r   = D � � � I   = B�������� 0 setvars setVars��  ��   � o      ���� 0 isready1 isReady1 �  � � � r   E L � � � I   E J�������� 0 setpaths setPaths��  ��   � o      ���� 0 isready2 isReady2 �  � � � Z   M z � ��� � � F   M X � � � =  M P � � � o   M N���� 0 isready1 isReady1 � m   N O��
�� boovtrue � =  S V � � � o   S T���� 0 isready2 isReady2 � m   T U��
�� boovtrue � k   [ h � �  � � � I   [ a�� ���� 0 gatherstats gatherStats �  ��~ � m   \ ]�}�}  �~  �   �  ��| � I   b h�{ ��z�{ "0 processonecycle processOneCycle �  ��y � m   c d�x
�x boovtrue�y  �z  �|  ��   � I  k z�w ��v
�w .sysodisAaleR        TEXT � b   k v � � � b   k t � � � b   k p � � � m   k n � � � � � & A b o r t i n g .   s e t V a r   =   � o   n o�u�u 0 isready1 isReady1 � m   p s � � � � �    s e t P a t h s   =   � o   t u�t�t 0 isready2 isReady2�v   �  � � � I  { ��s ��r
�s .ascrcmnt****      � **** � b   { � � � � l  { � ��q�p � I  { ��o�n�m
�o .misccurdldt    ��� null�n  �m  �q  �p   � m   � � � � �   t D o n e   w i t h   i n i t i a l   c y c l e   -   w a i t i n g   3 0 s e c   f o r   o n I d l e   h a n d l e r�r   �  l  � ��l�k�j�l  �k  �j    T   � � k   � �  I   � ��i	�h�i "0 processonecycle processOneCycle	 
�g
 m   � ��f
�f boovfals�g  �h   �e I  � ��d�c
�d .sysodelanull��� ��� nmbr o   � ��b�b 0 loopdelaysecs loopDelaySecs�c  �e   �a l  � ��`�_�^�`  �_  �^  �a   �  l     �]�\�[�]  �\  �[    l     �Z�Y�X�Z  �Y  �X    l     �W�V�U�W  �V  �U    i   Y \ I      �T�S�T $0 shownotification showNotification  o      �R�R  0 subtitlestring subtitleString �Q o      �P�P 0 messagestring messageString�Q  �S   k     +  Z     # �O! >    "#" o     �N�N 0 messagestring messageString# m    �M
�M 
null  I   �L$%
�L .sysonotfnull��� ��� TEXT$ o    �K�K 0 messagestring messageString% �J&'
�J 
appr& o    �I�I "0 applicationname applicationName' �H(�G
�H 
subt( o    �F�F  0 subtitlestring subtitleString�G  �O  ! I   #�E�D)
�E .sysonotfnull��� ��� TEXT�D  ) �C*+
�C 
subt* o    �B�B  0 subtitlestring subtitleString+ �A,�@
�A 
appr, o    �?�? "0 applicationname applicationName�@   -.- l  $ )/01/ I  $ )�>2�=
�> .sysodelanull��� ��� nmbr2 m   $ %�<�< �=  0 ) #allow time for notification to fire   1 �33 F a l l o w   t i m e   f o r   n o t i f i c a t i o n   t o   f i r e. 4�;4 l  * *�:�9�8�:  �9  �8  �;   565 l     �7�6�5�7  �6  �5  6 787 l     �4�3�2�4  �3  �2  8 9:9 i   ] `;<; I      �1=�0�1 "0 processonecycle processOneCycle= >�/> o      �.�.  0 isinitialcycle isInitialCycle�/  �0  < k    
?? @A@ I    �-B�,
�- .ascrcmnt****      � ****B b     CDC l    E�+�*E I    �)�(�'
�) .misccurdldt    ��� null�(  �'  �+  �*  D m    FF �GG  S t a r t i n g   C y c l e�,  A HIH I    �&J�%�& $0 shownotification showNotificationJ KLK m    MM �NN  S t a r t i n g   C y c l eL O�$O m    �#
�# 
null�$  �%  I PQP l   �"�!� �"  �!  �   Q RSR I    ���� 0 handlewaivers handleWaivers�  �  S TUT I   +�V�
� .ascrcmnt****      � ****V b    'WXW b    !YZY l   [��[ I   ���
� .misccurdldt    ��� null�  �  �  �  Z m     \\ �]] > D o n e   c h e c k i n g   w a i v e r s .   w a i t i n g  X o   ! &�� $0 processdelaysecs processDelaySecs�  U ^_^ I  , 5�`�
� .sysodelanull��� ��� nmbr` o   , 1�� $0 processdelaysecs processDelaySecs�  _ aba l  6 6����  �  �  b cdc I   6 ;���� :0 transcodeaftereffectsoutput transcodeAfterEffectsOutput�  �  d efe I  < M�g�

� .ascrcmnt****      � ****g b   < Ihih b   < Cjkj l  < Al�	�l I  < A���
� .misccurdldt    ��� null�  �  �	  �  k m   A Bmm �nn B d o n e   c h e c k i n g   A E   o u t p u t .   w a i t i n g  i o   C H�� $0 processdelaysecs processDelaySecs�
  f opo I  N W�q�
� .sysodelanull��� ��� nmbrq o   N S�� $0 processdelaysecs processDelaySecs�  p rsr l  X X� �����   ��  ��  s tut I   X ]�������� ,0 movefilestoingestion moveFilesToIngestion��  ��  u vwv I  ^ o��x��
�� .ascrcmnt****      � ****x b   ^ kyzy b   ^ e{|{ l  ^ c}����} I  ^ c������
�� .misccurdldt    ��� null��  ��  ��  ��  | m   c d~~ � L d o n e   c h e c k i n g   e n c o d e r   o u t p u t .   w a i t i n g  z o   e j���� $0 processdelaysecs processDelaySecs��  w ��� I  p y�����
�� .sysodelanull��� ��� nmbr� o   p u���� $0 processdelaysecs processDelaySecs��  � ��� l  z z��������  ��  ��  � ��� l  z z������  � 0 * clean out the raw .mov from encoder watch   � ��� T   c l e a n   o u t   t h e   r a w   . m o v   f r o m   e n c o d e r   w a t c h� ��� I   z �������� 0 filecleanup fileCleanup��  ��  � ��� I  � ������
�� .ascrcmnt****      � ****� b   � ���� b   � ���� l  � ������� I  � �������
�� .misccurdldt    ��� null��  ��  ��  ��  � m   � ��� ��� R d o n e   c l e a n i n g   u p   e n c o d e r   s o u r c e .   w a i t i n g  � o   � ����� $0 processdelaysecs processDelaySecs��  � ��� I  � ������
�� .sysodelanull��� ��� nmbr� o   � ����� $0 processdelaysecs processDelaySecs��  � ��� l  � ���������  ��  ��  � ��� I   � �������� 0 gatherstats gatherStats� ���� m   � ����� ��  ��  � ��� I  � ������
�� .ascrcmnt****      � ****� b   � ���� l  � ������� I  � �������
�� .misccurdldt    ��� null��  ��  ��  ��  � m   � ��� ��� $ d o n e   s e n d i n g   s t a t s��  � ��� l  � ���������  ��  ��  � ��� Z   � ������� o   � �����  0 isinitialcycle isInitialCycle� I   � �������� $0 shownotification showNotification� ��� m   � ��� ��� , F i n i s h e d   I n i t i a l   C y c l e� ���� m   � ��� ��� B w a i t i n g   ~ 3 0 s e c   f o r   o n I d l e   h a n d l e r��  ��  ��  � I   � �������� $0 shownotification showNotification� ��� m   � ��� ���  F i n i s h e d   C y c l e� ���� l  � ������� b   � ���� b   � ���� m   � ��� ���  w a i t i n g  � o   � ����� 0 loopdelaysecs loopDelaySecs� m   � ��� ��� $ s e c   f o r   n e x t   c y c l e��  ��  ��  ��  � ��� l  � ���������  ��  ��  � ��� Z   �������� =  � ���� o   � ����� 0 	automated  � m   � ���
�� boovfals� k   ��� ��� I  � ������
�� .ascrcmnt****      � ****� b   � ���� l  � ������� I  � �������
�� .misccurdldt    ��� null��  ��  ��  ��  � m   � ��� ��� j D o n e   w i t h   c y c l e   -   e x i t i n g   b e c a u s e   a u t o m a t i o n   i s   f a l s e��  � ��� I  � �����
�� .sysodisAaleR        TEXT� m   � ��� ��� j D o n e   w i t h   c y c l e   -   e x i t i n g   b e c a u s e   a u t o m a t i o n   i s   f a l s e� �����
�� 
givu� m   � ����� ��  � ���� I  �������
�� .aevtquitnull��� ��� null��  ��  ��  ��  ��  � ���� l 		��������  ��  ��  ��  : ��� l     ��������  ��  ��  � ��� l     ������  � C =#############################################################   � ��� z # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #� ��� l     ������  � ) ### --DO NOT EDIT BELOW THIS LIINE--   � ��� F # #   - - D O   N O T   E D I T   B E L O W   T H I S   L I I N E - -� ��� l     ������  � > 8## --THESE ARE ALL THE HANDLERS USED IN THE LOOP ABOVE--   � ��� p # #   - - T H E S E   A R E   A L L   T H E   H A N D L E R S   U S E D   I N   T H E   L O O P   A B O V E - -� ��� l     ������  � C =#############################################################   � ��� z # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #� ��� l     ��������  ��  ��  � ��� i   a d��� I      ������� &0 checkpendingfiles checkPendingFiles� ��� o      ���� 0 oldlist oldList� ���� o      ���� 0 newlist newList��  ��  � k     ���    r      c      J     ����   m    ��
�� 
list o      ���� 0 
filesadded 
filesAdded  r    	 c    

 J    	����   m   	 
��
�� 
list	 o      ���� 0 filesremoved filesRemoved  r     m    ��
�� boovfals o      ���� 0 isprocessing isProcessing  Z    ��� F    # l   ���� @     n     1    ��
�� 
leng o    ���� 0 newlist newList n     1    ��
�� 
leng o    ���� 0 oldlist oldList��  ��   l   !���� ?    !  n    !"! 1    ��
�� 
leng" o    ���� 0 oldlist oldList  m     ����  ��  ��   k   & �## $%$ l  & &��������  ��  ��  % &'& l  & &��()��  (   new/old is same and > 0   ) �** 0   n e w / o l d   i s   s a m e   a n d   >   0' +,+ Z   & 8-.���- =   & )/0/ o   & '�~�~ 0 newlist newList0 o   ' (�}�} 0 oldlist oldList. k   , 411 232 l  , ,�|45�|  4 &   error condition, not processing   5 �66 @   e r r o r   c o n d i t i o n ,   n o t   p r o c e s s i n g3 787 I  , 1�{9�z
�{ .ascrcmnt****      � ****9 m   , -:: �;;  n o t   p r o c e s s i n g�z  8 <�y< L   2 4== m   2 3�x
�x boovfals�y  ��  �  , >?> l  9 9�w�v�u�w  �v  �u  ? @A@ Y   9 dB�tCD�sB k   I _EE FGF r   I OHIH n   I MJKJ 4   J M�rL
�r 
cobjL o   K L�q�q 0 i  K o   I J�p�p 0 oldlist oldListI o      �o�o 0 n  G M�nM Z  P _NO�m�lN H   P TPP E  P SQRQ o   P Q�k�k 0 newlist newListR o   Q R�j�j 0 n  O r   W [STS o   W X�i�i 0 n  T n      UVU  ;   Y ZV o   X Y�h�h 0 filesremoved filesRemoved�m  �l  �n  �t 0 i  C m   < =�g�g D I  = D�fW�e
�f .corecnte****       ****W n   = @XYX 2  > @�d
�d 
cobjY o   = >�c�c 0 oldlist oldList�e  �s  A Z[Z l  e e�b�a�`�b  �a  �`  [ \]\ Y   e �^�__`�^^ k   u �aa bcb r   u {ded n   u yfgf 4   v y�]h
�] 
cobjh o   w x�\�\ 0 i  g o   u v�[�[ 0 newlist newListe o      �Z�Z 0 n  c i�Yi Z  | �jk�X�Wj H   | �ll E  | mnm o   | }�V�V 0 oldlist oldListn o   } ~�U�U 0 n  k r   � �opo o   � ��T�T 0 n  p n      qrq  ;   � �r o   � ��S�S 0 
filesadded 
filesAdded�X  �W  �Y  �_ 0 i  _ m   h i�R�R ` I  i p�Qs�P
�Q .corecnte****       ****s n   i ltut 2  j l�O
�O 
cobju o   i j�N�N 0 newlist newList�P  �^  ] vwv l  � ��M�L�K�M  �L  �K  w xyx I  � ��Jz�I
�J .ascrcmnt****      � ****z c   � �{|{ b   � �}~} m   � � ���  f i l e s A d d e d :  ~ o   � ��H�H 0 
filesadded 
filesAdded| m   � ��G
�G 
TEXT�I  y ��� I  � ��F��E
�F .ascrcmnt****      � ****� b   � ���� m   � ��� ���  f i l e s R e m o v e d :  � o   � ��D�D 0 filesremoved filesRemoved�E  � ��� l  � ��C�B�A�C  �B  �A  � ��� l  � ��@���@  � ( " adding files but not removing any   � ��� D   a d d i n g   f i l e s   b u t   n o t   r e m o v i n g   a n y� ��� Z   � ����?�>� F   � ���� l  � ���=�<� ?   � ���� n   � ���� 1   � ��;
�; 
leng� o   � ��:�: 0 
filesadded 
filesAdded� m   � ��9�9  �=  �<  � l  � ���8�7� =   � ���� n   � ���� 1   � ��6
�6 
leng� o   � ��5�5 0 filesremoved filesRemoved� m   � ��4�4  �8  �7  � k   � ��� ��� l  � ��3���3  � &   error condition, not processing   � ��� @   e r r o r   c o n d i t i o n ,   n o t   p r o c e s s i n g� ��2� L   � ��� m   � ��1
�1 boovfals�2  �?  �>  � ��� l  � ��0�/�.�0  �/  �.  � ��� l  � ��-���-  � %  adding files AND removing them   � ��� >   a d d i n g   f i l e s   A N D   r e m o v i n g   t h e m� ��� Z   � ����,�+� F   � ���� l  � ���*�)� @   � ���� n   � ���� 1   � ��(
�( 
leng� o   � ��'�' 0 
filesadded 
filesAdded� m   � ��&�&  �*  �)  � l  � ���%�$� ?   � ���� n   � ���� 1   � ��#
�# 
leng� o   � ��"�" 0 filesremoved filesRemoved� m   � ��!�!  �%  �$  � L   � ��� m   � �� 
�  boovtrue�,  �+  � ��� l  � �����  �  �  �  ��   k   � ��� ��� l  � �����  � %  new/old are both 0 OR old is 0   � ��� >   n e w / o l d   a r e   b o t h   0   O R   o l d   i s   0� ��� l  � �����  � A ; this is where we are when new files are added after a lull   � ��� v   t h i s   i s   w h e r e   w e   a r e   w h e n   n e w   f i l e s   a r e   a d d e d   a f t e r   a   l u l l� ��� I  � ����
� .ascrcmnt****      � ****� m   � ��� ��� 0 s k i p p i n g   l i s t   c o m p a r i s o n�  � ��� r   � ���� m   � ��
� boovtrue� o      �� 0 isprocessing isProcessing�   ��� l  � �����  �  �  � ��� L   � ��� o   � ��� 0 isprocessing isProcessing� ��� l  � �����  �  �  �  � ��� l     ���
�  �  �
  � ��� i   e h��� I      �	���	 0 filecleanup fileCleanup�  �  � k     '�� ��� Z     %����� =    ��� o     �� (0 performfilecleanup performFileCleanup� m    �
� boovtrue� k   
 !�� ��� r   
 ��� m   
 �� ��� h r m   - r f   / U s e r s / b l u e p i x e l / D o c u m e n t s / a m e _ w a t c h / S o u r c e / *� o      �� 0 shscript  � ��� Q    ���� I   ��� 
� .sysoexecTEXT���     TEXT� o    ���� 0 shscript  �   � R      �����
�� .ascrerr ****      � ****� o      ���� 
0 errmsg  ��  �  � ��� l     ��������  ��  ��  � ���� l     ������  �   DO NOTHING   � ���    D O   N O T H I N G��  �  � l  $ $������  �  
do nothing   � ���  d o   n o t h i n g� ��� l  & &��������  ��  ��  � ���� l  & &��������  ��  ��  ��  �    l     ��������  ��  ��    l     ��������  ��  ��    i   i l I      �������� ,0 movefilestoingestion moveFilesToIngestion��  ��   k    � 	
	 r      o     ���� 0 processed_media   o      ���� 0 myfolder  
  r    	 n     1    ��
�� 
psxp o    ���� 0 processed_media   o      ���� 0 	pmyfolder    l  
 
��������  ��  ��    I  
 ����
�� .ascrcmnt****      � **** b   
  l  
 ���� I  
 ������
�� .misccurdldt    ��� null��  ��  ��  ��   m     �   s t a r t i n g   u p l o a d s��    r      b    !"! b    #$# m    %% �&&  l s  $ n    '(' 1    ��
�� 
strq( o    ���� 0 	pmyfolder  " m    )) �**    |   g r e p   ' . m p 4 '  o      ���� 0 shscript   +,+ l     ��������  ��  ��  , -.- Q     P/01/ r   # .232 c   # ,454 n   # *676 2  ( *��
�� 
cpar7 l  # (8����8 I  # (��9��
�� .sysoexecTEXT���     TEXT9 o   # $���� 0 shscript  ��  ��  ��  5 m   * +��
�� 
list3 o      ���� 0 filelist  0 R      ��:��
�� .ascrerr ****      � ****: o      ���� 
0 errmsg  ��  1 k   6 P;; <=< I  6 C��>��
�� .ascrcmnt****      � ****> b   6 ??@? b   6 =ABA l  6 ;C����C I  6 ;������
�� .misccurdldt    ��� null��  ��  ��  ��  B m   ; <DD �EE  u p l o a d   e r r o r :  @ o   = >���� 
0 errmsg  ��  = FGF I  D I��H��
�� .ascrcmnt****      � ****H m   D EII �JJ z i f   e r r o r   1   i t   p r o b a b l y   m e a n s   t h e r e   a r e   n o   f i l e s   t o   u p l o a d   y e t��  G K��K r   J PLML c   J NNON J   J L����  O m   L M��
�� 
listM o      ���� 0 filelist  ��  . PQP l  Q Q��������  ��  ��  Q RSR X   Q�T��UT k   c�VV WXW l  c c��������  ��  ��  X YZY r   c l[\[ c   c j]^] b   c f_`_ o   c d���� 0 myfolder  ` o   d e���� 0 i  ^ m   f i��
�� 
TEXT\ o      ���� 
0 myfile  Z aba r   m pcdc o   m n���� 0 i  d o      ���� 0 newfile  b efe l  q q��������  ��  ��  f ghg l  q q��������  ��  ��  h iji Z   q�kl����k =  q mnm I   q }��o���� ,0 checkfilewritestatus checkFileWriteStatuso pqp o   r s���� 
0 myfile  q rsr m   s v����  B@s t��t m   v y���� ��  ��  n m   } ~��
�� boovtruel k   ��uu vwv l  � ���xy��  x , &upload to Dropbox folder for ingestion   y �zz L u p l o a d   t o   D r o p b o x   f o l d e r   f o r   i n g e s t i o nw {|{ r   � �}~} b   � �� b   � ���� b   � ���� b   � ���� m   � ��� ���  m v  � n   � ���� 1   � ���
�� 
strq� o   � ����� 
0 myfile  � m   � ��� ���   � n   � ���� 1   � ���
�� 
strq� o   � ����� 0 spock_ingestor  � o   � ����� 0 newfile  ~ o      ���� 0 shscript  | ��� l  � ���������  ��  ��  � ��� Q   � ����� k   � ��� ��� I  � ������
�� .ascrcmnt****      � ****� b   � ���� l  � ������� I  � �������
�� .misccurdldt    ��� null��  ��  ��  ��  � m   � ��� ���  s t a r t i n g   u p l o a d��  � ���� I  � ������
�� .sysoexecTEXT���     TEXT� o   � ����� 0 shscript  ��  ��  � R      �����
�� .ascrerr ****      � ****� o      ���� 
0 errmsg  ��  � k   � ��� ��� I  � ������
�� .ascrcmnt****      � ****� b   � ���� b   � ���� l  � ������� I  � ���~�}
� .misccurdldt    ��� null�~  �}  ��  ��  � m   � ��� ��� ( e r r o r   i n   u p l o a d i n g :  � o   � ��|�| 
0 errmsg  ��  � ��� I  � ��{�z�y
�{ .sysobeepnull��� ��� long�z  �y  � ��� I  � ��x�w�v
�x .sysobeepnull��� ��� long�w  �v  � ��� I  � ��u�t�s
�u .sysobeepnull��� ��� long�t  �s  � ��� I  � ��r��
�r .sysodisAaleR        TEXT� b   � ���� m   � ��� ��� > A n   e r r o r   o c c u r e d   i n   u p l o a d i n g :  � o   � ��q�q 
0 errmsg  � �p��o
�p 
givu� m   � ��n�n �o  � ��m�  S   � ��m  � ��� l  � ��l�k�j�l  �k  �j  � ��� l  � ��i���i  � 6 0cleanup intermediate raw files to save HDD space   � ��� ` c l e a n u p   i n t e r m e d i a t e   r a w   f i l e s   t o   s a v e   H D D   s p a c e� ��� r   � ���� I   � ��h��g�h 0 	changeext 	changeExt� ��� c   � ���� o   � ��f�f 0 i  � m   � ��e
�e 
TEXT� ��d� m   � ��� ���  �d  �g  � o      �c�c 0 basefilename baseFilename� ��� r   ���� b   ���� b   �	��� b   ���� b   ���� m   � ��� ���  l s  � n   ���� 1   �b
�b 
strq� o   � �a�a 0 
media_pool  � m  �� ���    |   g r e p   - e  � o  �`�` 0 basefilename baseFilename� m  	�� ���  *� o      �_�_ 0 shscript  � ��� Q  D���� r  ��� c  ��� n  ��� 2 �^
�^ 
cpar� l ��]�\� I �[��Z
�[ .sysoexecTEXT���     TEXT� o  �Y�Y 0 shscript  �Z  �]  �\  � m  �X
�X 
list� o      �W�W $0 intermediatelist intermediateList� R      �V��U
�V .ascrerr ****      � ****� o      �T�T 
0 errmsg  �U  � k  &D�� ��� I &5�S��R
�S .ascrcmnt****      � ****� b  &1��� b  &/��� l &+��Q�P� I &+�O�N�M
�O .misccurdldt    ��� null�N  �M  �Q  �P  � m  +.�� ���  u p l o a d   e r r o r :  � o  /0�L�L 
0 errmsg  �R  � ��� I 6=�K��J
�K .ascrcmnt****      � ****� m  69   � z i f   e r r o r   1   i t   p r o b a b l y   m e a n s   t h e r e   a r e   n o   f i l e s   t o   u p l o a d   y e t�J  � �I r  >D c  >B J  >@�H�H   m  @A�G
�G 
list o      �F�F $0 intermediatelist intermediateList�I  �  l EE�E�D�C�E  �D  �C   	
	 X  E��B k  W�  r  Wl c  Wj b  Wf b  Wd b  W` m  WZ �  r m   n  Z_ 1  ]_�A
�A 
strq o  Z]�@�@ 0 
media_pool   m  `c �  / o  de�?�? 0 i   m  fi�>
�> 
TEXT o      �=�= 0 shscript    !  Q  m�"#$" I pu�<%�;
�< .sysoexecTEXT���     TEXT% o  pq�:�: 0 shscript  �;  # R      �9&�8
�9 .ascrerr ****      � ****& o      �7�7 
0 errmsg  �8  $ I }��6'�5
�6 .ascrcmnt****      � ****' b  }�()( b  }�*+* l }�,�4�3, I }��2�1�0
�2 .misccurdldt    ��� null�1  �0  �4  �3  + m  ��-- �.. 8 i n t e r m e d i a t e   c l e a n u p   e r r o r :  ) o  ���/�/ 
0 errmsg  �5  ! /�./ l ���-�,�+�-  �,  �+  �.  �B 0 i   o  HI�*�* $0 intermediatelist intermediateList
 010 l ���)�(�'�)  �(  �'  1 232 l ���&�%�$�&  �%  �$  3 4�#4 l ���"56�"  5  fileReady end if   6 �77   f i l e R e a d y   e n d   i f�#  ��  ��  j 8�!8 l ��� ���   �  �  �!  �� 0 i  U o   T U�� 0 filelist  S 9:9 I ���;�
� .ascrcmnt****      � ****; b  ��<=< l ��>��> I �����
� .misccurdldt    ��� null�  �  �  �  = m  ��?? �@@ V d o n e   w i t h   u p l o a d i n g .   W a i t i n g   f o r   n e x t   c y c l e�  : A�A l ������  �  �  �   BCB l     ����  �  �  C DED l     ����  �  �  E FGF i   m pHIH I      ��
�	� :0 transcodeaftereffectsoutput transcodeAfterEffectsOutput�
  �	  I k    *JJ KLK l     �MN�  M l f TODO improve ExtendScript to move file when done rendering, or to write a proxy file that we look for   N �OO �   T O D O   i m p r o v e   E x t e n d S c r i p t   t o   m o v e   f i l e   w h e n   d o n e   r e n d e r i n g ,   o r   t o   w r i t e   a   p r o x y   f i l e   t h a t   w e   l o o k   f o rL PQP l     �RS�  R k e currently, AE writes output in VERY slow bursts, so sometimes we think a file is done when it is not   S �TT �   c u r r e n t l y ,   A E   w r i t e s   o u t p u t   i n   V E R Y   s l o w   b u r s t s ,   s o   s o m e t i m e s   w e   t h i n k   a   f i l e   i s   d o n e   w h e n   i t   i s   n o tQ UVU l     �WX�  W  y other times, like when receiving test files that are blank, the output will be very small which confuses our file checks   X �YY �   o t h e r   t i m e s ,   l i k e   w h e n   r e c e i v i n g   t e s t   f i l e s   t h a t   a r e   b l a n k ,   t h e   o u t p u t   w i l l   b e   v e r y   s m a l l   w h i c h   c o n f u s e s   o u r   f i l e   c h e c k sV Z[Z l     ����  �  �  [ \]\ r     ^_^ o     �� 0 	ae_output  _ o      �� 0 myfolder  ] `a` r    	bcb n    ded 1    � 
�  
psxpe o    ���� 0 	ae_output  c o      ���� 0 	pmyfolder  a fgf l  
 
��������  ��  ��  g hih I  
 ��j��
�� .ascrcmnt****      � ****j b   
 klk l  
 m����m I  
 ������
�� .misccurdldt    ��� null��  ��  ��  ��  l m    nn �oo & s t a r t i n g   t r a n s c o d e s��  i pqp r    rsr b    tut b    vwv m    xx �yy  l s  w n    z{z 1    ��
�� 
strq{ o    ���� 0 	pmyfolder  u m    || �}}    |   g r e p   - e   . m o vs o      ���� 0 shscript  q ~~ l     ��������  ��  ��   ��� Q     N���� r   # .��� c   # ,��� n   # *��� 2  ( *��
�� 
cpar� l  # (������ I  # (�����
�� .sysoexecTEXT���     TEXT� o   # $���� 0 shscript  ��  ��  ��  � m   * +��
�� 
list� o      ���� 0 filelist  � R      �����
�� .ascrerr ****      � ****� o      ���� 
0 errmsg  ��  � k   6 N�� ��� I  6 A�����
�� .ascrcmnt****      � ****� b   6 =��� l  6 ;������ I  6 ;������
�� .misccurdldt    ��� null��  ��  ��  ��  � o   ; <���� 
0 errmsg  ��  � ��� I  B G�����
�� .ascrcmnt****      � ****� m   B C�� ��� > p r o b a b l y   b e c a u s e   n o   w a i v e r s   y e t��  � ���� r   H N��� c   H L��� J   H J����  � m   J K��
�� 
list� o      ���� 0 filelist  ��  � ��� l  O O��������  ��  ��  � ��� X   O����� k   _�� ��� l  _ _������  � H B checkFileWriteStatus(pmyfile, expectedMinSize, checkDelaySeconds)   � ��� �   c h e c k F i l e W r i t e S t a t u s ( p m y f i l e ,   e x p e c t e d M i n S i z e ,   c h e c k D e l a y S e c o n d s )� ��� l  _ _��������  ��  ��  � ��� I  _ l�����
�� .ascrcmnt****      � ****� b   _ h��� l  _ d������ I  _ d������
�� .misccurdldt    ��� null��  ��  ��  ��  � m   d g�� ��� $ c h e c k i n g   f i l e   s i z e��  � ��� r   m v��� c   m t��� b   m p��� o   m n���� 0 myfolder  � o   n o���� 0 i  � m   p s��
�� 
TEXT� o      ���� 
0 myfile  � ��� l  w w������  � # move file to AME watch folder   � ��� : m o v e   f i l e   t o   A M E   w a t c h   f o l d e r� ��� l  w w������  � k e set shscript to "cd " & quoted form of pmyfolder & "; mv " & i & " /" & quoted form of ame_watch & i   � ��� �   s e t   s h s c r i p t   t o   " c d   "   &   q u o t e d   f o r m   o f   p m y f o l d e r   &   " ;   m v   "   &   i   &   "   / "   &   q u o t e d   f o r m   o f   a m e _ w a t c h   &   i� ��� Z   w������� =  w ���� I   w �������� ,0 checkfilewritestatus checkFileWriteStatus� ��� o   x y���� 
0 myfile  � ��� o   y ~���� >0 filesizecheck_minaeoutputsize filesizeCheck_minAEOutputSize� ���� o   ~ ����� (0 filesizecheckdelay filesizeCheckDelay��  ��  � m   � ���
�� boovtrue� k   ��� ��� r   � ���� b   � ���� b   � ���� b   � ���� b   � ���� b   � ���� b   � ���� m   � ��� ���  m v  � n   � ���� 1   � ���
�� 
strq� o   � ����� 0 	pmyfolder  � m   � ��� ���  � o   � ����� 0 i  � m   � ��� ���    /� n   � ���� 1   � ���
�� 
strq� o   � ����� 0 	ame_watch  � o   � ����� 0 i  � o      ���� 0 shscript  � ��� Z   � �������� =  � ���� o   � ����� 0 dev_mode  � m   � ���
�� boovtrue� I  � ������
�� .sysodlogaskr        TEXT� o   � ����� 0 shscript  ��  ��  ��  � ��� Q   ����� k   � ��� ��� I  � ������
�� .ascrcmnt****      � ****� b   � ���� l  � � ����  I  � �������
�� .misccurdldt    ��� null��  ��  ��  ��  � m   � � � : m o v i n g   i   t o   A M E   w a t c h   f o l d e r  ��  � �� I  � �����
�� .sysoexecTEXT���     TEXT o   � ����� 0 shscript  ��  ��  � R      ����
�� .ascrerr ****      � **** o      ���� 
0 errmsg  ��  � k   �  I  � ���	��
�� .ascrcmnt****      � ****	 b   � �

 b   � � l  � ����� I  � �������
�� .misccurdldt    ��� null��  ��  ��  ��   m   � � � ( e r r o r   i n   u p l o a d i n g :   o   � ����� 
0 errmsg  ��    I  � �������
�� .sysobeepnull��� ��� long��  ��    I  � �������
�� .sysobeepnull��� ��� long��  ��    I  � �������
�� .sysobeepnull��� ��� long��  ��    I  ���
�� .sysodisAaleR        TEXT b   � m   � � � > A n   e r r o r   o c c u r e d   i n   u p l o a d i n g :   o   � ���� 
0 errmsg   ����
�� 
givu m  ���� ��    ��   S  ��  � !"! l ��������  ��  ��  " #��# l �$%�  $  fileReady end if   % �&&   f i l e R e a d y   e n d   i f��  ��  ��  � '�~' l �}�|�{�}  �|  �{  �~  �� 0 i  � o   R S�z�z 0 filelist  � ()( I (�y*�x
�y .ascrcmnt****      � ***** b  $+,+ l  -�w�v- I  �u�t�s
�u .misccurdldt    ��� null�t  �s  �w  �v  , m   #.. �// * d o n e   w i t h   t r a n s c o d i n g�x  ) 0�r0 l ))�q�p�o�q  �p  �o  �r  G 121 l     �n�m�l�n  �m  �l  2 343 l     �k�j�i�k  �j  �i  4 565 l     �h�g�f�h  �g  �f  6 787 l     �e�d�c�e  �d  �c  8 9:9 i   q t;<; I      �b�a�`�b 0 handlewaivers handleWaivers�a  �`  < k    === >?> I    �_@�^
�_ .ascrcmnt****      � ****@ b     ABA l    C�]�\C I    �[�Z�Y
�[ .misccurdldt    ��� null�Z  �Y  �]  �\  B m    DD �EE   c h e c k i n g   w a i v e r s�^  ? FGF l   �X�W�V�X  �W  �V  G HIH r    JKJ o    �U�U .0 incomingrawxmlwaivers incomingRawXmlWaiversK o      �T�T 60 incomingwaiverfolderposix incomingWaiverFolderPosixI LML l   �S�R�Q�S  �R  �Q  M NON l   �PPQ�P  P  assemble greps	   Q �RR  a s s e m b l e   g r e p s 	O STS r    UVU b    WXW b    YZY b    [\[ m    ]] �^^  l s  \ n    _`_ 1    �O
�O 
strq` o    �N�N 60 incomingwaiverfolderposix incomingWaiverFolderPosixZ m    aa �bb    |   g r e p  X o    �M�M $0 waivergrepstring waiverGrepStringV o      �L�L 0 shscript  T cdc l     �K�J�I�K  �J  �I  d efe Z     3gh�H�Gg =    'iji o     %�F�F 0 dev_mode  j m   % &�E
�E boovtrueh I  * /�Dk�C
�D .sysodlogaskr        TEXTk o   * +�B�B 0 shscript  �C  �H  �G  f lml l  4 4�A�@�?�A  �@  �?  m non l  4 4�>pq�>  p  get list of waivers   q �rr & g e t   l i s t   o f   w a i v e r so sts l  4 4�=uv�=  u 2 ,grep -e Added -e Changed -e Fixed -e Deleted   v �ww X g r e p   - e   A d d e d   - e   C h a n g e d   - e   F i x e d   - e   D e l e t e dt xyx Q   4 bz{|z r   7 B}~} c   7 @� n   7 >��� 2  < >�<
�< 
cpar� l  7 <��;�:� I  7 <�9��8
�9 .sysoexecTEXT���     TEXT� o   7 8�7�7 0 shscript  �8  �;  �:  � m   > ?�6
�6 
list~ o      �5�5  0 waiverfilelist waiverFileList{ R      �4��3
�4 .ascrerr ****      � ****� o      �2�2 
0 errmsg  �3  | k   J b�� ��� I  J U�1��0
�1 .ascrcmnt****      � ****� b   J Q��� l  J O��/�.� I  J O�-�,�+
�- .misccurdldt    ��� null�,  �+  �/  �.  � o   O P�*�* 
0 errmsg  �0  � ��� I  V [�)��(
�) .ascrcmnt****      � ****� m   V W�� ��� > p r o b a b l y   b e c a u s e   n o   w a i v e r s   y e t�(  � ��'� r   \ b��� c   \ `��� J   \ ^�&�&  � m   ^ _�%
�% 
list� o      �$�$  0 waiverfilelist waiverFileList�'  y ��� l  c c�#�"�!�#  �"  �!  � ��� l  c c� ���   � N H look for incoming source files and move them and waiver into production   � ��� �   l o o k   f o r   i n c o m i n g   s o u r c e   f i l e s   a n d   m o v e   t h e m   a n d   w a i v e r   i n t o   p r o d u c t i o n� ��� X   c;���� k   s6�� ��� l  s s����  � G ANeed to read the waiver and see if it contains multiple raw files   � ��� � N e e d   t o   r e a d   t h e   w a i v e r   a n d   s e e   i f   i t   c o n t a i n s   m u l t i p l e   r a w   f i l e s� ��� l  s s����  � - ' we also have access here to PATE info    � ��� N   w e   a l s o   h a v e   a c c e s s   h e r e   t o   P A T E   i n f o  � ��� r   s x��� b   s v��� o   s t�� 60 incomingwaiverfolderposix incomingWaiverFolderPosix� o   t u�� 0 outteri outterI� o      �� 0 
waiverfile 
waiverFile� ��� r   y ���� I   y ���� 0 read_waiver  � ��� o   z {�� 0 
waiverfile 
waiverFile�  �  � o      �� 0 mywaiver myWaiver� ��� l  � �����  �  �  � ��� r   � ���� J   � ���  � o      �� 0 rawfileslist rawFilesList� ��� r   � ���� J   � ���  � o      �� 0 movefileslist moveFilesList� ��� l  � �����  �  �  � ��� Z   � ����
�� F   � ���� l  � ���	�� >  � ���� n   � ���� o   � ��� 0 waiver_total_raw_files  � o   � ��� 0 mywaiver myWaiver� m   � ��
� 
null�	  �  � l  � ����� ?   � ���� n   � ���� o   � ��� 0 waiver_total_raw_files  � o   � ��� 0 mywaiver myWaiver� m   � �� �   �  �  � k   � ��� ��� r   � ���� m   � �����  � o      ���� 0 myincrementer myIncrementer� ���� U   � ���� k   � ��� ��� r   � ���� [   � ���� o   � ����� 0 myincrementer myIncrementer� m   � ����� � o      ���� 0 myincrementer myIncrementer� ��� l  � �������  �    does this need try/catch?   � ��� 4   d o e s   t h i s   n e e d   t r y / c a t c h ?� ��� r   � ���� I   � �������� 0 get_element  � ��� o   � ����� 0 
waiverfile 
waiverFile� ��� m   � ��� ���  J o b� ���� l  � ������� b   � ���� m   � ��� ���  r a w _ v i d e o _� o   � ����� 0 myincrementer myIncrementer��  ��  ��  ��  � o      ���� 0 	myrawfile 	myRawFile� ���� s   � ���� o   � ����� 0 	myrawfile 	myRawFile� l     ������ n      � �  ;   � �  o   � ����� 0 rawfileslist rawFilesList��  ��  ��  � l  � ����� n   � � o   � ����� 0 waiver_total_raw_files   o   � ����� 0 mywaiver myWaiver��  ��  ��  �
  � k   � �  l  � �����   O I no raw files defined in waiver, infer raw filename from waiver file name    �		 �   n o   r a w   f i l e s   d e f i n e d   i n   w a i v e r ,   i n f e r   r a w   f i l e n a m e   f r o m   w a i v e r   f i l e   n a m e 

 l  � �����   O I just look for single raw file, we will strip .xml extension in next step    � �   j u s t   l o o k   f o r   s i n g l e   r a w   f i l e ,   w e   w i l l   s t r i p   . x m l   e x t e n s i o n   i n   n e x t   s t e p �� s   � � o   � ����� 0 outteri outterI l     ���� n        ;   � � o   � ����� 0 rawfileslist rawFilesList��  ��  ��  �  l  � ���������  ��  ��    l  � ���������  ��  ��    l  � �����   2 , see if all media files are ready to process    � X   s e e   i f   a l l   m e d i a   f i l e s   a r e   r e a d y   t o   p r o c e s s  Z   �4 !��"  I   � ���#���� (0 checkrawfilestatus checkRawFileStatus# $��$ o   � ����� 0 rawfileslist rawFilesList��  ��  ! k   �0%% &'& l  � ���()��  ( 3 - files are ready, move waiver into production   ) �** Z   f i l e s   a r e   r e a d y ,   m o v e   w a i v e r   i n t o   p r o d u c t i o n' +,+ r   � �-.- b   � �/0/ b   � �121 b   � �343 b   � �565 m   � �77 �88  m v  6 n   � �9:9 1   � ���
�� 
strq: o   � ����� 0 
waiverfile 
waiverFile4 m   � �;; �<<   2 o   � ����� @0 pendingwaiversforaefolderposix pendingWaiversForAEFolderPosix0 l  � �=����= c   � �>?> o   � ����� 0 outteri outterI? m   � ���
�� 
TEXT��  ��  . o      ���� 0 shscript  , @A@ l  � ���BC��  B  display dialog shscript   C �DD . d i s p l a y   d i a l o g   s h s c r i p tA EFE l  � ���������  ��  ��  F GHG r   �IJI m   � ��
�� boovfalsJ o      ���� 0 success  H K��K Q  0LMNL k  OO PQP I ��R��
�� .sysoexecTEXT���     TEXTR o  ���� 0 shscript  ��  Q S��S r  TUT m  ��
�� boovtrueU o      ���� 0 success  ��  M R      ��V��
�� .ascrerr ****      � ****V o      ���� 
0 errmsg  ��  N k  0WW XYX I &��Z��
�� .ascrcmnt****      � ****Z b  "[\[ b   ]^] l _����_ I ������
�� .misccurdldt    ��� null��  ��  ��  ��  ^ m  `` �aa ` e r r o r   i n   m o v i n g   w a i v e r   f i l e   t o   w a i v e r s _ p e n d i n g :  \ o   !���� 
0 errmsg  ��  Y b��b I '0��c��
�� .sysodisAaleR        TEXTc b  ',ded m  '*ff �gg ` e r r o r   i n   m o v i n g   w a i v e r   f i l e   t o   w a i v e r s _ p e n d i n g :  e o  *+���� 
0 errmsg  ��  ��  ��  ��  " l 33��hi��  h 8 2 waiver not ready, we'll check again on next cycle   i �jj d   w a i v e r   n o t   r e a d y ,   w e ' l l   c h e c k   a g a i n   o n   n e x t   c y c l e klk l 55��������  ��  ��  l mnm l 55��������  ��  ��  n o��o l 55��pq��  p  outterI   q �rr  o u t t e r I��  � 0 outteri outterI� o   f g����  0 waiverfilelist waiverFileList� sts l <<��������  ��  ��  t uvu l <<��������  ��  ��  v wxw l <<��������  ��  ��  x y��y l <<��������  ��  ��  ��  : z{z l     ��������  ��  ��  { |}| l     ��������  ��  ��  } ~~ i   u x��� I      ������� (0 checkrawfilestatus checkRawFileStatus� ���� o      ���� 0 rawfileslist rawFilesList��  ��  � k    )�� ��� l     ������  � * $ check if files are done transcoding   � ��� H   c h e c k   i f   f i l e s   a r e   d o n e   t r a n s c o d i n g� ��� l     ������  � E ? if they are not found, check for them in incoming media folder   � ��� ~   i f   t h e y   a r e   n o t   f o u n d ,   c h e c k   f o r   t h e m   i n   i n c o m i n g   m e d i a   f o l d e r� ��� l     ������  � 5 / returns true if all files are done transcoding   � ��� ^   r e t u r n s   t r u e   i f   a l l   f i l e s   a r e   d o n e   t r a n s c o d i n g� ��� l     ��������  ��  ��  � ��� l     ������  � b \ if first file is not found in transcoded, just move on to checking raw path for all of them   � ��� �   i f   f i r s t   f i l e   i s   n o t   f o u n d   i n   t r a n s c o d e d ,   j u s t   m o v e   o n   t o   c h e c k i n g   r a w   p a t h   f o r   a l l   o f   t h e m� ��� Z     ������� =    ��� o     ���� 0 dev_mode  � m    ��
�� boovtrue� I  
 �����
�� .sysodlogaskr        TEXT� b   
 ��� m   
 �� ���  r a w F i l e s L i s t   =  � o    ���� 0 rawfileslist rawFilesList��  ��  ��  � ��� r    ��� m    ��
�� boovfals� o      �� "0 filereadystatus fileReadyStatus� ��� r    ��� m    �~
�~ boovfals� o      �}�}  0 skiptranscoded skipTranscoded� ��� r    "��� J     �|�|  � o      �{�{ 0 movefileslist moveFilesList� ��� l  # #�z�y�x�z  �y  �x  � ��� l  # #�w�v�u�w  �v  �u  � ��� X   #=��t�� k   38�� ��� r   3 >��� I  3 <�s��r
�s .corecnte****       ****� n   3 8��� 2  6 8�q
�q 
cha � l  3 6��p�o� c   3 6��� o   3 4�n�n 0 i  � m   4 5�m
�m 
TEXT�p  �o  �r  � o      �l�l 0 
namelength  � ��� r   ? P��� c   ? N��� l  ? L��k�j� n   ? L��� 7 @ L�i��
�i 
cha � m   D F�h�h � l  G K��g�f� \   G K��� o   H I�e�e 0 
namelength  � m   I J�d�d �g  �f  � o   ? @�c�c 0 i  �k  �j  � m   L M�b
�b 
TEXT� o      �a�a 
0 myname  � ��� l  Q Q�`�_�^�`  �_  �^  � ��� l  Q Q�]�\�[�]  �\  �[  � ��� l  Q Q�Z���Z  � 8 2check for transcoded media for any valid extension   � ��� d c h e c k   f o r   t r a n s c o d e d   m e d i a   f o r   a n y   v a l i d   e x t e n s i o n� ��� X   Q6��Y�� k   e1�� ��� r   e p��� c   e n��� b   e l��� b   e j��� b   e h��� m   e f�� ���  /� o   f g�X�X >0 transcodecompletedfolderposix transcodeCompletedFolderPosix� o   h i�W�W 
0 myname  � o   j k�V�V 0 fileextension fileExtension� m   l m�U
�U 
TEXT� o      �T�T $0 mytranscodedfile myTranscodedFile� ��� r   q |��� c   q z��� b   q x��� b   q v��� b   q t��� m   q r�� ���  /� o   r s�S�S :0 incomingrawmediafolderposix incomingRawMediaFolderPosix� o   t u�R�R 
0 myname  � o   v w�Q�Q 0 fileextension fileExtension� m   x y�P
�P 
TEXT� o      �O�O 0 	myrawfile 	myRawFile� ��� r   } �� � o   } ~�N�N 0 fileextension fileExtension  o      �M�M  0 foundextension foundExtension�  l  � ��L�K�J�L  �K  �J    Z   � ��I�H I   � ��G�F�G ,0 checkfilewritestatus checkFileWriteStatus 	 o   � ��E�E $0 mytranscodedfile myTranscodedFile	 

 m   � ��D�D� �C m   � ��B�B �C  �F   k   � �  r   � � m   � ��A
�A boovtrue o      �@�@ "0 filereadystatus fileReadyStatus  s   � � c   � � l  � ��?�> b   � � o   � ��=�= 
0 myname   o   � ��<�<  0 foundextension foundExtension�?  �>   m   � ��;
�; 
TEXT l     �:�9 n        ;   � � o   � ��8�8 0 movefileslist moveFilesList�:  �9   �7  S   � ��7  �I  �H     l  � ��6�5�4�6  �5  �4    !"! l  � ��3#$�3  # 4 . file not found in transcoded, check it in raw   $ �%% \   f i l e   n o t   f o u n d   i n   t r a n s c o d e d ,   c h e c k   i t   i n   r a w" &'& Z   �/()�2�1( I   � ��0*�/�0 ,0 checkfilewritestatus checkFileWriteStatus* +,+ o   � ��.�. 0 	myrawfile 	myRawFile, -.- m   � ��-�-  . /�,/ m   � ��+�+ �,  �/  ) k   �+00 121 r   � �343 m   � ��*
�* boovfals4 o      �)�) "0 filereadystatus fileReadyStatus2 565 l  � ��(78�(  7 ( "move raw file to Transcoding chain   8 �99 D m o v e   r a w   f i l e   t o   T r a n s c o d i n g   c h a i n6 :;: Z   � �<=>�'< E   � �?@? o   � ��&�& 80 rawtranscodefileextensions rawTranscodeFileExtensions@ o   � ��%�%  0 foundextension foundExtension= r   � �ABA b   � �CDC b   � �EFE b   � �GHG b   � �IJI m   � �KK �LL  m v  J n   � �MNM 1   � ��$
�$ 
strqN o   � ��#�# 0 	myrawfile 	myRawFileH m   � �OO �PP   F o   � ��"�" :0 transcodependingfolderposix transcodePendingFolderPosixD l  � �Q�!� Q b   � �RSR o   � ��� 
0 myname  S o   � ���  0 foundextension foundExtension�!  �   B o      �� 0 shscript  > TUT E   � �VWV o   � ��� D0  bypassrawtranscodefileextensions  bypassRawTranscodeFileExtensionsW o   � ���  0 foundextension foundExtensionU X�X r   � �YZY b   � �[\[ b   � �]^] b   � �_`_ b   � �aba m   � �cc �dd  m v  b n   � �efe 1   � ��
� 
strqf o   � ��� 0 	myrawfile 	myRawFile` m   � �gg �hh   ^ o   � ��� >0 transcodecompletedfolderposix transcodeCompletedFolderPosix\ l  � �i��i b   � �jkj o   � ��� 
0 myname  k o   � ���  0 foundextension foundExtension�  �  Z o      �� 0 shscript  �  �'  ; lml r   � �non m   � ��
� boovfalso o      �� 0 success  m pqp Q   �)rstr k   �uu vwv I  � ��x�
� .sysoexecTEXT���     TEXTx o   � ��� 0 shscript  �  w yzy r   � {|{ m   � ��
� boovtrue| o      �� $0 filemove_success fileMove_successz }~}  S  ~ �
 l �	���	  �  �  �
  s R      ���
� .ascrerr ****      � ****� o      �� 
0 errmsg  �  t k  )�� ��� I ���
� .ascrcmnt****      � ****� b  ��� b  ��� l ��� � I ������
�� .misccurdldt    ��� null��  ��  �  �   � m  �� ��� ` e r r o r   i n   m o v i n g   m e d i a   f i l e   t o   m e d i a _ t r a n s c o d e d :  � o  ���� 
0 errmsg  �  � ��� I %�����
�� .sysodisAaleR        TEXT� b  !��� m  �� ��� ` e r r o r   i n   m o v i n g   m e d i a   f i l e   t o   m e d i a _ t r a n s c o d e d :  � o   ���� 
0 errmsg  ��  � ���� r  &)��� m  &'��
�� boovfals� o      ���� $0 filemove_success fileMove_success��  q ��� l **��������  ��  ��  � ��� l **��������  ��  ��  � ���� l **������  �  exists rawFile end if   � ��� * e x i s t s   r a w F i l e   e n d   i f��  �2  �1  ' ���� l 00������  �  file extensions repeat   � ��� , f i l e   e x t e n s i o n s   r e p e a t��  �Y 0 fileextension fileExtension� o   T Y���� 00 incomingfileextensions incomingFileExtensions� ��� l 77��������  ��  ��  � ��� l 77��������  ��  ��  � ���� l 77������  �  outter repeat in filelist   � ��� 2 o u t t e r   r e p e a t   i n   f i l e l i s t��  �t 0 i  � o   & '���� 0 rawfileslist rawFilesList� ��� l >>��������  ��  ��  � ��� l >>������  � U Oif all files are found in transcoded, check their status and move to media_pool   � ��� � i f   a l l   f i l e s   a r e   f o u n d   i n   t r a n s c o d e d ,   c h e c k   t h e i r   s t a t u s   a n d   m o v e   t o   m e d i a _ p o o l� ��� Z  >$������ F  >S��� = >A��� o  >?���� "0 filereadystatus fileReadyStatus� m  ?@��
�� boovtrue� l DO������ =  DO��� n  DI��� 1  EI��
�� 
leng� o  DE���� 0 movefileslist moveFilesList� n  IN��� 1  JN��
�� 
leng� o  IJ���� 0 rawfileslist rawFilesList��  ��  � k  V�� ��� X  V����� k  f��� ��� Z  f������� = fm��� o  fk���� 0 dev_mode  � m  kl��
�� boovtrue� I p{�����
�� .sysodlogaskr        TEXT� c  pw��� b  pu��� m  ps�� ���   m o v e F i l e s L i s t   =  � o  st���� 0 movefileslist moveFilesList� m  uv��
�� 
TEXT��  ��  ��  � ��� r  ����� c  ����� b  ����� b  ����� m  ���� ���  /� o  ������ >0 transcodecompletedfolderposix transcodeCompletedFolderPosix� o  ������ 0 rawfilei rawFileI� m  ����
�� 
TEXT� o      ���� 
0 myfile  � ��� l ����������  ��  ��  � ��� r  ����� n  ����� 1  ����
�� 
psxp� o  ������ 
0 myfile  � o      ���� 0 pmyfile  � ��� l ��������  � F @TODO refactor this to use incoming_media_ext from config file!!!   � ��� � T O D O   r e f a c t o r   t h i s   t o   u s e   i n c o m i n g _ m e d i a _ e x t   f r o m   c o n f i g   f i l e ! ! !� ��� r  ����� b  ����� b  ����� b  ����� b  ����� m  ���� ���  m v  � n  ����� 1  ����
�� 
strq� o  ������ 0 pmyfile  � m  ���� ���   � o  ������ 0 
media_pool  � l �������� o  ������ 0 rawfilei rawFileI��  ��  � o      ���� 0 shscript  �    r  �� m  ����
�� boovfals o      ���� 0 success    Z  ������ = ��	 o  ������ 0 dev_mode  	 m  ����
�� boovtrue I ����
��
�� .sysodlogaskr        TEXT
 o  ������ 0 shscript  ��  ��  ��    Q  �� k  ��  I ������
�� .sysoexecTEXT���     TEXT o  ������ 0 shscript  ��   �� r  �� m  ����
�� boovtrue o      ���� "0 filereadystatus fileReadyStatus��   R      ����
�� .ascrerr ****      � **** o      ���� 
0 errmsg  ��   k  ��  I ������
�� .ascrcmnt****      � **** b  �� b  �� l �� ����  I ��������
�� .misccurdldt    ��� null��  ��  ��  ��   m  ��!! �"" T e r r o r   i n   m o v i n g   m e d i a   f i l e   t o   m e d i a _ p o o l :   o  ������ 
0 errmsg  ��   #$# I ����%��
�� .sysodisAaleR        TEXT% b  ��&'& m  ��(( �)) T e r r o r   i n   m o v i n g   m e d i a   f i l e   t o   m e d i a _ p o o l :  ' o  ������ 
0 errmsg  ��  $ *+* r  ��,-, m  ����
�� boovfals- o      ���� "0 filereadystatus fileReadyStatus+ .��.  S  ����   /��/ l ����������  ��  ��  ��  �� 0 rawfilei rawFileI� o  YZ���� 0 movefileslist moveFilesList� 010 L  22 o  ���� "0 filereadystatus fileReadyStatus1 3��3 l ��������  ��  ��  ��  ��  � k  
$44 565 l 

��78��  7 3 - files not ready or move to media_pool failed   8 �99 Z   f i l e s   n o t   r e a d y   o r   m o v e   t o   m e d i a _ p o o l   f a i l e d6 :;: Z  
<=����< = 
>?> o  
���� 0 dev_mode  ? m  ��
�� boovtrue= I ��@��
�� .sysodlogaskr        TEXT@ m  AA �BB . N o t   a l l   f i l e s   a r e   r e a d y��  ��  ��  ; CDC L   "EE m   !��
�� boovfalsD F��F l ##��GH��  G  fileReadyStatus   H �II  f i l e R e a d y S t a t u s��  � JKJ l %%��������  ��  ��  K LML l %%��������  ��  ��  M NON l %%��~�}�  �~  �}  O PQP L  %'RR o  %&�|�| "0 filereadystatus fileReadyStatusQ STS l ((�{�z�y�{  �z  �y  T UVU l ((�x�w�v�x  �w  �v  V W�uW l ((�t�s�r�t  �s  �r  �u   XYX l     �q�p�o�q  �p  �o  Y Z[Z l     �n�m�l�n  �m  �l  [ \]\ l     �k�j�i�k  �j  �i  ] ^_^ i   y |`a` I      �hb�g�h ,0 checkfilewritestatus checkFileWriteStatusb cdc o      �f�f 0 pmyfile  d efe o      �e�e "0 expectedminsize expectedMinSizef g�dg o      �c�c &0 checkdelayseconds checkDelaySeconds�d  �g  a k     khh iji r     klk m     �b
�b boovfalsl o      �a�a  0 thisfilestatus thisFileStatusj mnm l   �`�_�^�`  �_  �^  n opo r    qrq m    �]�]  r o      �\�\ 0 	filesize1  p sts r    uvu m    	�[�[ v o      �Z�Z 0 	filesize2  t wxw Z   yz�Y�Xy =   {|{ o    �W�W "0 expectedminsize expectedMinSize| m    �V
�V 
nullz r    }~} m    �U�U ~ o      �T�T "0 expectedminsize expectedMinSize�Y  �X  x � l   �S�R�Q�S  �R  �Q  � ��� Z    h���P�� I     �O��N�O 0 
fileexists 
fileExists� ��M� o    �L�L 0 pmyfile  �M  �N  � k   # c�� ��� r   # -��� l  # +��K�J� I  # +�I��H
�I .rdwrgeofcomp       ****� 4   # '�G�
�G 
psxf� o   % &�F�F 0 pmyfile  �H  �K  �J  � o      �E�E 0 	filesize1  � ��� Z  . B���D�C� l  . 9��B�A� G   . 9��� l  . 1��@�?� A   . 1��� o   . /�>�> 0 	filesize1  � m   / 0�=�= �@  �?  � l  4 7��<�;� A   4 7��� o   4 5�:�: 0 	filesize1  � o   5 6�9�9 "0 expectedminsize expectedMinSize�<  �;  �B  �A  � L   < >�� m   < =�8
�8 boovfals�D  �C  � ��� l  C C�7�6�5�7  �6  �5  � ��� I  C H�4��3
�4 .sysodelanull��� ��� nmbr� o   C D�2�2 &0 checkdelayseconds checkDelaySeconds�3  � ��� r   I S��� l  I Q��1�0� I  I Q�/��.
�/ .rdwrgeofcomp       ****� 4   I M�-�
�- 
psxf� o   K L�,�, 0 pmyfile  �.  �1  �0  � o      �+�+ 0 	filesize2  � ��� Z   T a���*�� =   T W��� o   T U�)�) 0 	filesize1  � o   U V�(�( 0 	filesize2  � L   Z \�� m   Z [�'
�' boovtrue�*  � L   _ a�� m   _ `�&
�& boovfals� ��%� l  b b�$�#�"�$  �#  �"  �%  �P  � k   f h�� ��� l  f f�!���!  �   file does not exist   � ��� (   f i l e   d o e s   n o t   e x i s t� �� � L   f h�� m   f g�
� boovfals�   � ��� l  i i����  �  �  � ��� l  i i����  �  �  � ��� L   i k�� o   i j��  0 thisfilestatus thisFileStatus�  _ ��� l     ����  �  �  � ��� i   } ���� I      ���� 0 gatherstats gatherStats� ��� o      �� 0 intervalmins intervalMins�  �  � k    ��� ��� l     ����  � &   only send stats every X minutes   � ��� @   o n l y   s e n d   s t a t s   e v e r y   X   m i n u t e s� ��� r     ��� m     �
� boovfals� o      �� 0 
initialrun 
initialRun� ��� Z    C����� ?    ��� o    �� 0 intervalmins intervalMins� m    �
�
  � k   
 -�� ��� r   
 ��� I   
 �	���	 0 getepochtime getEpochTime�  �  � o      �� 0 timenow timeNow� ��� r     ��� I    ���� 0 number_to_string  � ��� [    ��� o    �� 80 laststatsgatheredtimestamp lastStatsGatheredTimestamp� l   ��� � ]    ��� ]    ��� o    ���� 0 intervalmins intervalMins� m    ���� <� m    ����  ���  �   �  �  � o      ���� 0 	nextcheck 	nextCheck� ���� Z   ! -������� ?   ! $��� o   ! "���� 0 	nextcheck 	nextCheck� o   " #���� 0 timenow timeNow� L   ' )����  ��  ��  ��  �  � k   0 C�� ��� l  0 0������  � A ;making initial run, need to init lastStatsGatheredTimestamp   � ��� v m a k i n g   i n i t i a l   r u n ,   n e e d   t o   i n i t   l a s t S t a t s G a t h e r e d T i m e s t a m p� ��� r   0 7��� I   0 5�������� 0 getepochtime getEpochTime��  ��  � o      ���� 80 laststatsgatheredtimestamp lastStatsGatheredTimestamp� ��� r   8 ?��� I   8 =�������� 0 getepochtime getEpochTime��  ��  � o      ���� :0 laststalefilechecktimestamp lastStaleFileCheckTimestamp� 	 ��	  r   @ C			 m   @ A��
�� boovtrue	 o      ���� 0 
initialrun 
initialRun��  � 			 l  D D��������  ��  ��  	 			 l  D D��		��  	 %  set var for our overall health   	 �				 >   s e t   v a r   f o r   o u r   o v e r a l l   h e a l t h	 	
		
 r   D G			 m   D E��
�� boovtrue	 o      ���� 0 ishappy isHappy	 			 r   H K			 m   H I��
�� boovfals	 o      ���� 40 updatestalefiletimestamp updateStaleFileTimestamp	 			 r   L O			 m   L M����  	 o      ���� 0 oldestfileage oldestFileAge	 			 r   P S			 m   P Q		 �		  	 o      ���� 0 	dataitems 	dataItems	 			 r   T W			 m   T U	 	  �	!	!  	 o      ���� 0 failedtests failedTests	 	"	#	" l  X X��������  ��  ��  	# 	$	%	$ r   X [	&	'	& m   X Y	(	( �	)	) @ { " n a m e " : " t e s t i n g " , " v a l u e " : " f o o " }	' o      ���� 0 	firstitem 	firstItem	% 	*	+	* l  \ \��������  ��  ��  	+ 	,	-	, l  \ \��	.	/��  	.   check if AE is running   	/ �	0	0 .   c h e c k   i f   A E   i s   r u n n i n g	- 	1	2	1 r   \ d	3	4	3 I   \ b��	5���� 0 
is_running  	5 	6��	6 m   ] ^	7	7 �	8	8  A f t e r   E f f e c t s��  ��  	4 o      ���� 0 ae_test  	2 	9	:	9 I   e q��	;���� 0 
writeplist 
writePlist	; 	<	=	< o   f k���� 0 theplistpath thePListPath	= 	>	?	> m   k l	@	@ �	A	A  a e _ t e s t	? 	B��	B o   l m���� 0 ae_test  ��  ��  	: 	C	D	C Z   r �	E	F��	G	E =  r u	H	I	H o   r s���� 0 ae_test  	I m   s t��
�� boovfals	F k   x �	J	J 	K	L	K r   x {	M	N	M m   x y��
�� boovfals	N o      ���� 0 ishappy isHappy	L 	O	P	O r   | �	Q	R	Q b   | 	S	T	S o   | }���� 0 failedtests failedTests	T m   } ~	U	U �	V	V  a e _ s t a t u s ,  	R o      ���� 0 failedtests failedTests	P 	W	X	W Z   � �	Y	Z����	Y =  � �	[	\	[ o   � ����� 0 dev_mode  	\ m   � ���
�� boovtrue	Z I  � ���	]��
�� .sysodlogaskr        TEXT	] m   � �	^	^ �	_	_ & N O T   H A P P Y   a e _ s t a t u s��  ��  ��  	X 	`��	` r   � �	a	b	a m   � �	c	c �	d	d F { " n a m e " : " a e _ s t a t u s " , " v a l u e " : " D O W N " }	b o      ���� 0 	firstitem 	firstItem��  ��  	G r   � �	e	f	e m   � �	g	g �	h	h B { " n a m e " : " a e _ s t a t u s " , " v a l u e " : " U P " }	f o      ���� 0 	firstitem 	firstItem	D 	i	j	i r   � �	k	l	k b   � �	m	n	m o   � ����� 0 	dataitems 	dataItems	n o   � ����� 0 	firstitem 	firstItem	l o      ���� 0 	dataitems 	dataItems	j 	o	p	o l  � ���������  ��  ��  	p 	q	r	q l  � ���������  ��  ��  	r 	s	t	s l  � ���	u	v��  	u ( " check if Media Encoder is running   	v �	w	w D   c h e c k   i f   M e d i a   E n c o d e r   i s   r u n n i n g	t 	x	y	x r   � �	z	{	z I   � ���	|���� 0 
is_running  	| 	}��	} m   � �	~	~ �		 & A d o b e   M e d i a   E n c o d e r��  ��  	{ o      ���� 0 ame_test  	y 	�	�	� I   � ���	����� 0 
writeplist 
writePlist	� 	�	�	� o   � ����� 0 theplistpath thePListPath	� 	�	�	� m   � �	�	� �	�	�  a m e _ t e s t	� 	���	� o   � ����� 0 ame_test  ��  ��  	� 	�	�	� Z   � �	�	���	�	� =  � �	�	�	� o   � ����� 0 ame_test  	� m   � ���
�� boovfals	� k   � �	�	� 	�	�	� r   � �	�	�	� m   � ���
�� boovfals	� o      ���� 0 ishappy isHappy	� 	�	�	� r   � �	�	�	� b   � �	�	�	� o   � ����� 0 failedtests failedTests	� m   � �	�	� �	�	�  a m e _ s t a t u s ,  	� o      ���� 0 failedtests failedTests	� 	�	�	� Z   � �	�	�����	� =  � �	�	�	� o   � ����� 0 dev_mode  	� m   � ���
�� boovtrue	� I  � ���	���
�� .sysodlogaskr        TEXT	� m   � �	�	� �	�	� ( N O T   H A P P Y   a m e _ s t a t u s��  ��  ��  	� 	���	� r   � �	�	�	� m   � �	�	� �	�	� H { " n a m e " : " a m e _ s t a t u s " , " v a l u e " : " D O W N " }	� o      ���� 0 nextitem nextItem��  ��  	� r   � �	�	�	� m   � �	�	� �	�	� D { " n a m e " : " a m e _ s t a t u s " , " v a l u e " : " U P " }	� o      ���� 0 nextitem nextItem	� 	�	�	� r   �	�	�	� b   �	�	�	� b   � �	�	�	� o   � ����� 0 	dataitems 	dataItems	� m   � �	�	� �	�	�  ,	� o   � ���� 0 nextitem nextItem	� o      ���� 0 	dataitems 	dataItems	� 	�	�	� l ��������  ��  ��  	� 	�	�	� l ��	�	���  	� "  check if Dropbox is running   	� �	�	� 8   c h e c k   i f   D r o p b o x   i s   r u n n i n g	� 	�	�	� r  	�	�	� I  ��	����� 0 
is_running  	� 	���	� m  	�	� �	�	�  D r o p b o x��  ��  	� o      ���� 0 dropbox_test  	� 	�	�	� I  ��	����� 0 
writeplist 
writePlist	� 	�	�	� o  ���� 0 theplistpath thePListPath	� 	�	�	� m  	�	� �	�	�  d r o p b o x _ t e s t	� 	���	� o  ���� 0 dropbox_test  ��  ��  	� 	�	�	� Z  S	�	���	�	� = !	�	�	� o  ���� 0 dropbox_test  	� m   ��
�� boovfals	� k  $K	�	� 	�	�	� r  $'	�	�	� m  $%��
�� boovfals	� o      ���� 0 ishappy isHappy	� 	�	�	� r  (/	�	�	� b  (-	�	�	� o  ()���� 0 failedtests failedTests	� m  ),	�	� �	�	�   d r o p b o x _ s t a t u s ,  	� o      ���� 0 failedtests failedTests	� 	�	�	� Z  0E	�	�����	� = 07	�	�	� o  05���� 0 dev_mode  	� m  56�
� boovtrue	� I :A�~	��}
�~ .sysodlogaskr        TEXT	� m  :=	�	� �	�	� 0 N O T   H A P P Y   d r o p b o x _ s t a t u s�}  ��  ��  	� 	��|	� r  FK	�	�	� m  FI	�	� �	�	� P { " n a m e " : " d r o p b o x _ s t a t u s " , " v a l u e " : " D O W N " }	� o      �{�{ 0 nextitem nextItem�|  ��  	� r  NS	�	�	� m  NQ	�	� �	�	� L { " n a m e " : " d r o p b o x _ s t a t u s " , " v a l u e " : " U P " }	� o      �z�z 0 nextitem nextItem	� 	�	�	� r  T]	�	�	� b  T[	�	�	� b  TY	�
 	� o  TU�y�y 0 	dataitems 	dataItems
  m  UX

 �

  ,	� o  YZ�x�x 0 nextitem nextItem	� o      �w�w 0 	dataitems 	dataItems	� 


 l ^^�v�u�t�v  �u  �t  
 


 l ^^�s�r�q�s  �r  �q  
 


 l ^^�p
	

�p  
	 &  check pending waivers (AE queue)   

 �

 @ c h e c k   p e n d i n g   w a i v e r s   ( A E   q u e u e )
 


 r  ^o


 b  ^m


 b  ^i


 m  ^a

 �

  l s  
 n  ah


 1  dh�o
�o 
strq
 o  ad�n�n @0 pendingwaiversforaefolderposix pendingWaiversForAEFolderPosix
 m  il

 �

    |   g r e p   ' . x m l '
 o      �m�m 0 shscript  
 


 Q  p�



 r  s�

 
 c  s�
!
"
! n  s|
#
$
# 2 x|�l
�l 
cpar
$ l sx
%�k�j
% I sx�i
&�h
�i .sysoexecTEXT���     TEXT
& o  st�g�g 0 shscript  �h  �k  �j  
" m  |�f
�f 
list
  o      �e�e 0 mylist myList
 R      �d
'�c
�d .ascrerr ****      � ****
' o      �b�b 
0 errmsg  �c  
 k  ��
(
( 
)
*
) I ���a
+�`
�a .ascrcmnt****      � ****
+ b  ��
,
-
, b  ��
.
/
. l ��
0�_�^
0 I ���]�\�[
�] .misccurdldt    ��� null�\  �[  �_  �^  
/ m  ��
1
1 �
2
2 2 p r o b a b l y   n o   w a i v e r s   y e t :  
- o  ���Z�Z 
0 errmsg  �`  
* 
3�Y
3 r  ��
4
5
4 J  ���X�X  
5 o      �W�W 0 mylist myList�Y  
 
6
7
6 l ���V�U�T�V  �U  �T  
7 
8
9
8 r  ��
:
;
: n  ��
<
=
< 1  ���S
�S 
leng
= o  ���R�R 0 mylist myList
; o      �Q�Q 0 mycount myCount
9 
>
?
> r  ��
@
A
@ I  ���P
B�O�P 0 	readplist 	readPlist
B 
C
D
C o  ���N�N 0 theplistpath thePListPath
D 
E
F
E m  ��
G
G �
H
H   a e _ w a i v e r _ c o u n t 1
F 
I�M
I m  ���L
�L 
null�M  �O  
A o      �K�K 0 oldcount oldCount
? 
J
K
J r  ��
L
M
L \  ��
N
O
N o  ���J�J 0 mycount myCount
O o  ���I�I 0 oldcount oldCount
M o      �H�H 0 ae_waiver_delta  
K 
P
Q
P I  ���G
R�F�G 0 
writeplist 
writePlist
R 
S
T
S o  ���E�E 0 theplistpath thePListPath
T 
U
V
U m  ��
W
W �
X
X  a e _ w a i v e r _ d e l t a
V 
Y�D
Y o  ���C�C 0 ae_waiver_delta  �D  �F  
Q 
Z
[
Z I  ���B
\�A�B 0 
writeplist 
writePlist
\ 
]
^
] o  ���@�@ 0 theplistpath thePListPath
^ 
_
`
_ m  ��
a
a �
b
b   a e _ w a i v e r _ c o u n t 1
` 
c�?
c o  ���>�> 0 mycount myCount�?  �A  
[ 
d
e
d l ���=�<�;�=  �<  �;  
e 
f
g
f l ���:
h
i�:  
h   see if files are moving	   
i �
j
j 2   s e e   i f   f i l e s   a r e   m o v i n g 	
g 
k
l
k r  ��
m
n
m I  ���9�8�7�9 0 getepochtime getEpochTime�8  �7  
n o      �6�6 0 timenow timeNow
l 
o
p
o r  �
q
r
q I  ��5
s�4�5 0 number_to_string  
s 
t�3
t [  ��
u
v
u o  ���2�2 :0 laststalefilechecktimestamp lastStaleFileCheckTimestamp
v l ��
w�1�0
w ]  ��
x
y
x o  ���/�/ 00 expectedmaxprocesstime expectedMaxProcessTime
y m  ���.�.  ���1  �0  �3  �4  
r o      �-�- 0 	nextcheck 	nextCheck
p 
z
{
z Z  `
|
}�,
~
| G  

�
 l 
��+�*
� A  
�
�
� o  �)�) 0 	nextcheck 	nextCheck
� o  �(�( 0 timenow timeNow�+  �*  
� l 
��'�&
� = 
�
�
� o  �%�% 0 
initialrun 
initialRun
� m  �$
�$ boovtrue�'  �&  
} k  \
�
� 
�
�
� l �#
�
��#  
�  check files   
� �
�
�  c h e c k   f i l e s
� 
�
�
� r  "
�
�
� I  �"
��!�" &0 checkpendingfiles checkPendingFiles
� 
�
�
� o  � �  00 pendingaewaiverlistold pendingAEWaiverListOLD
� 
��
� o  �� 0 mylist myList�  �!  
� o      �� 0 isprocessing isProcessing
� 
�
�
� r  #(
�
�
� o  #$�� 0 mylist myList
� o      �� 00 pendingaewaiverlistold pendingAEWaiverListOLD
� 
�
�
� Z  )V
�
��
�
� = ).
�
�
� o  ),�� 0 isprocessing isProcessing
� m  ,-�
� boovfals
� k  1R
�
� 
�
�
� r  14
�
�
� m  12�
� boovfals
� o      �� 0 ishappy isHappy
� 
�
�
� r  5<
�
�
� b  5:
�
�
� o  56�� 0 failedtests failedTests
� m  69
�
� �
�
� ( a e _ p e n d i n g _ w a i v e r s ,  
� o      �� 0 failedtests failedTests
� 
��
� Z  =R
�
���
� = =D
�
�
� o  =B�� 0 dev_mode  
� m  BC�
� boovtrue
� I GN�
��
� .sysodlogaskr        TEXT
� m  GJ
�
� �
�
� 8 N O T   H A P P Y   a e _ p e n d i n g _ w a i v e r s�  �  �  �  �  
� l UU�
�
��  
�  isProcessing is true			   
� �
�
� . i s P r o c e s s i n g   i s   t r u e 	 	 	
� 
�
�
� r  WZ
�
�
� m  WX�
� boovtrue
� o      �
�
 40 updatestalefiletimestamp updateStaleFileTimestamp
� 
��	
� l [[����  �  �  �	  �,  
~ l __�
�
��  
�   not time to check yet   
� �
�
� ,   n o t   t i m e   t o   c h e c k   y e t
{ 
�
�
� l aa����  �  �  
� 
�
�
� l aa�� ���  �   ��  
� 
�
�
� r  ah
�
�
� o  ad���� 0 mycount myCount
� o      ���� 0 mydatavalue myDataValue
� 
�
�
� r  iv
�
�
� b  it
�
�
� b  ip
�
�
� m  il
�
� �
�
� L { " n a m e " : " a e _ p e n d i n g _ w a i v e r s " , " v a l u e " : "
� o  lo���� 0 mydatavalue myDataValue
� m  ps
�
� �
�
�  " }
� o      ���� 0 nextitem nextItem
� 
�
�
� r  w�
�
�
� b  w~
�
�
� b  w|
�
�
� o  wx���� 0 	dataitems 	dataItems
� m  x{
�
� �
�
�  ,
� o  |}���� 0 nextitem nextItem
� o      ���� 0 	dataitems 	dataItems
� 
�
�
� l ����������  ��  ��  
� 
�
�
� r  ��
�
�
� o  ������ 0 ae_waiver_delta  
� o      ���� 0 mydatavalue myDataValue
� 
�
�
� r  ��
�
�
� b  ��
�
�
� b  ��
�
�
� m  ��
�
� �
�
� F { " n a m e " : " a e _ w a i v e r _ d e l t a " , " v a l u e " : "
� o  ������ 0 mydatavalue myDataValue
� m  ��
�
� �
�
�  " }
� o      ���� 0 nextitem nextItem
� 
�
�
� r  ��
�
�
� b  ��
�
�
� b  ��
�
�
� o  ������ 0 	dataitems 	dataItems
� m  ��
�
� �
�
�  ,
� o  ������ 0 nextitem nextItem
� o      ���� 0 	dataitems 	dataItems
� 
�
�
� l ����������  ��  ��  
� 
�
�
� l ����
�
���  
�   get age of oldest file   
� �   .   g e t   a g e   o f   o l d e s t   f i l e
�  r  �� I  �������� (0 getageofoldestfile getAgeOfOldestFile  o  ������ @0 pendingwaiversforaefolderposix pendingWaiversForAEFolderPosix �� m  ��		 �

  . x m l��  ��   o      ���� 0 mydatavalue myDataValue  Z ������ ?  �� o  ������ 0 mydatavalue myDataValue o  ������ 0 oldestfileage oldestFileAge r  �� o  ������ 0 mydatavalue myDataValue o      ���� 0 oldestfileage oldestFileAge��  ��    r  �� b  �� b  �� m  �� � B { " n a m e " : " a e _ w a i v e r _ a g e " , " v a l u e " : " o  ������ 0 mydatavalue myDataValue m  �� �  " } o      ���� 0 nextitem nextItem   r  ��!"! b  ��#$# b  ��%&% o  ������ 0 	dataitems 	dataItems& m  ��'' �((  ,$ o  ������ 0 nextitem nextItem" o      ���� 0 	dataitems 	dataItems  )*) l ����������  ��  ��  * +,+ l ����������  ��  ��  , -.- l ����/0��  /  check for errored waivers   0 �11 2 c h e c k   f o r   e r r o r e d   w a i v e r s. 232 r  ��454 m  ������  5 o      ���� 0 mythreshhold myThreshhold3 676 r  ��898 b  ��:;: b  ��<=< m  ��>> �??  l s  = n  ��@A@ 1  ����
�� 
strqA o  ������ @0 pendingwaiversforaefolderposix pendingWaiversForAEFolderPosix; m  ��BB �CC ( e r r o r /   |   g r e p   ' . x m l '9 o      ���� 0 shscript  7 DED l ����������  ��  ��  E FGF Q  �;HIJH k  �KK LML r  �NON I ���P��
�� .corecnte****       ****P c  �QRQ n  ��STS 2 ����
�� 
cparT l ��U����U I ����V��
�� .sysoexecTEXT���     TEXTV o  ������ 0 shscript  ��  ��  ��  R m  ���
�� 
list��  O o      ���� 0 mycount myCountM WXW I  ��Y���� 0 
writeplist 
writePlistY Z[Z o  ���� 0 theplistpath thePListPath[ \]\ m  ^^ �__ $ a e _ e r r o r e d _ w a i v e r s] `��` o  ���� 0 mycount myCount��  ��  X a��a l ��������  ��  ��  ��  I R      ��b��
�� .ascrerr ****      � ****b o      ���� 
0 errmsg  ��  J k  &;cc ded I &5��f��
�� .ascrcmnt****      � ****f b  &1ghg b  &/iji l &+k����k I &+������
�� .misccurdldt    ��� null��  ��  ��  ��  j m  +.ll �mm 2 p r o b a b l y   n o   w a i v e r s   y e t :  h o  /0���� 
0 errmsg  ��  e n��n r  6;opo m  67����  p o      ���� 0 mycount myCount��  G qrq l <<��������  ��  ��  r sts Z  <kuv����u ?  <Cwxw o  <?���� 0 mycount myCountx o  ?B���� 0 mythreshhold myThreshholdv k  Fgyy z{z r  FI|}| m  FG��
�� boovfals} o      ���� 0 ishappy isHappy{ ~~ r  JQ��� b  JO��� o  JK���� 0 failedtests failedTests� m  KN�� ��� " a e _ e r r o r _ w a i v e r ,  � o      ���� 0 failedtests failedTests ��� l RR��������  ��  ��  � ���� Z  Rg������� = RY��� o  RW���� 0 dev_mode  � m  WX��
�� boovtrue� I \c�����
�� .sysodlogaskr        TEXT� m  \_�� ��� 2 N O T   H A P P Y   a e _ e r r o r _ w a i v e r��  ��  ��  ��  ��  ��  t ��� r  ls��� o  lo���� 0 mycount myCount� o      ���� 0 mydatavalue myDataValue� ��� r  t���� b  t��� b  t{��� m  tw�� ��� F { " n a m e " : " a e _ e r r o r _ w a i v e r " , " v a l u e " : "� o  wz���� 0 mydatavalue myDataValue� m  {~�� ���  " }� o      ���� 0 nextitem nextItem� ��� r  ����� b  ����� b  ����� o  ������ 0 	dataitems 	dataItems� m  ���� ���  ,� o  ������ 0 nextitem nextItem� o      ���� 0 	dataitems 	dataItems� ��� l ����������  ��  ��  � ��� l ����������  ��  ��  � ��� l ��������  � ) #check pending AME files (AME queue)   � ��� F c h e c k   p e n d i n g   A M E   f i l e s   ( A M E   q u e u e )� ��� r  ����� b  ����� b  ����� m  ���� ���  l s  � n  ����� 1  ����
�� 
strq� o  ������ 0 	ame_watch  � m  ���� ���    |   g r e p   ' . m o v '� o      ���� 0 shscript  � ��� Q  ������ r  ����� c  ����� n  ����� 2 ����
�� 
cpar� l �������� I ������
�� .sysoexecTEXT���     TEXT� o  ���~�~ 0 shscript  �  ��  ��  � m  ���}
�} 
list� o      �|�| 0 mylist myList� R      �{��z
�{ .ascrerr ****      � ****� o      �y�y 
0 errmsg  �z  � k  ���� ��� I ���x��w
�x .ascrcmnt****      � ****� b  ����� b  ����� l ����v�u� I ���t�s�r
�t .misccurdldt    ��� null�s  �r  �v  �u  � m  ���� ��� 2 p r o b a b l y   n o   w a i v e r s   y e t :  � o  ���q�q 
0 errmsg  �w  � ��p� r  ����� J  ���o�o  � o      �n�n 0 mylist myList�p  � ��� l ���m�l�k�m  �l  �k  � ��� r  ����� n  ����� 1  ���j
�j 
leng� o  ���i�i 0 mylist myList� o      �h�h 0 mycount myCount� ��� r  ����� I  ���g��f�g 0 	readplist 	readPlist� ��� o  ���e�e 0 theplistpath thePListPath� ��� m  ���� ���  a m e _ c o u n t _ 1� ��d� m  ���c
�c 
null�d  �f  � o      �b�b 0 oldcount oldCount� ��� r  ����� \  ����� o  ���a�a 0 mycount myCount� o  ���`�` 0 oldcount oldCount� o      �_�_ 0 ame_file_delta  � ��� I  ��^��]�^ 0 
writeplist 
writePlist� ��� o  ���\�\ 0 theplistpath thePListPath� ��� m  ��� ���  a m e _ q u e u e _ d e l t a�  �[  o  �Z�Z 0 ame_file_delta  �[  �]  �  I  	�Y�X�Y 0 
writeplist 
writePlist  o  
�W�W 0 theplistpath thePListPath  m   �		  a m e _ c o u n t _ 1 
�V
 o  �U�U 0 mycount myCount�V  �X    l �T�S�R�T  �S  �R    l �Q�Q     see if files are moving	    � 2   s e e   i f   f i l e s   a r e   m o v i n g 	  r  ! I  �P�O�N�P 0 getepochtime getEpochTime�O  �N   o      �M�M 0 timenow timeNow  r  "2 I  "0�L�K�L 0 number_to_string   �J [  #, o  #$�I�I :0 laststalefilechecktimestamp lastStaleFileCheckTimestamp l $+�H�G ]  $+  o  $)�F�F 00 expectedmaxprocesstime expectedMaxProcessTime  m  )*�E�E  ���H  �G  �J  �K   o      �D�D 0 	nextcheck 	nextCheck !"! Z  3�#$�C%# G  3@&'& l 36(�B�A( A  36)*) o  34�@�@ 0 	nextcheck 	nextCheck* o  45�?�? 0 timenow timeNow�B  �A  ' l 9<+�>�=+ = 9<,-, o  9:�<�< 0 
initialrun 
initialRun- m  :;�;
�; boovtrue�>  �=  $ k  C�.. /0/ l CC�:12�:  1  check files   2 �33  c h e c k   f i l e s0 454 r  CP676 I  CL�98�8�9 &0 checkpendingfiles checkPendingFiles8 9:9 o  DG�7�7 .0 pendingamefilelistold pendingAMEFileListOLD: ;�6; o  GH�5�5 0 mylist myList�6  �8  7 o      �4�4 0 isprocessing isProcessing5 <=< r  QV>?> o  QR�3�3 0 mylist myList? o      �2�2 .0 pendingamefilelistold pendingAMEFileListOLD= @A@ Z  W�BC�1�0B = W\DED o  WZ�/�/ 0 isprocessing isProcessingE m  Z[�.
�. boovfalsC k  _�FF GHG r  _bIJI m  _`�-
�- boovfalsJ o      �,�, 0 ishappy isHappyH KLK r  cjMNM b  chOPO o  cd�+�+ 0 failedtests failedTestsP m  dgQQ �RR & a m e _ p e n d i n g _ f i l e s ,  N o      �*�* 0 failedtests failedTestsL S�)S Z  k�TU�(�'T = krVWV o  kp�&�& 0 dev_mode  W m  pq�%
�% boovtrueU I u|�$X�#
�$ .sysodlogaskr        TEXTX m  uxYY �ZZ 6 N O T   H A P P Y   a m e _ p e n d i n g _ f i l e s�#  �(  �'  �)  �1  �0  A [�"[ r  ��\]\ m  ���!
�! boovtrue] o      � �  40 updatestalefiletimestamp updateStaleFileTimestamp�"  �C  % l ���^_�  ^   not time to check   _ �`` $   n o t   t i m e   t o   c h e c k" aba l ������  �  �  b cdc r  ��efe o  ���� 0 mycount myCountf o      �� 0 mydatavalue myDataValued ghg r  ��iji b  ��klk b  ��mnm m  ��oo �pp J { " n a m e " : " a m e _ p e n d i n g _ f i l e s " , " v a l u e " : "n o  ���� 0 mydatavalue myDataValuel m  ��qq �rr  " }j o      �� 0 nextitem nextItemh sts r  ��uvu b  ��wxw b  ��yzy o  ���� 0 	dataitems 	dataItemsz m  ��{{ �||  ,x o  ���� 0 nextitem nextItemv o      �� 0 	dataitems 	dataItemst }~} l ������  �  �  ~ � r  ����� o  ���� 0 ame_file_delta  � o      �� 0 mydatavalue myDataValue� ��� r  ����� b  ����� b  ����� m  ���� ��� J { " n a m e " : " a m e _ p e n d i n g _ d e l t a " , " v a l u e " : "� o  ���� 0 mydatavalue myDataValue� m  ���� ���  " }� o      �� 0 nextitem nextItem� ��� r  ����� b  ����� b  ����� o  ���� 0 	dataitems 	dataItems� m  ���� ���  ,� o  ���� 0 nextitem nextItem� o      �� 0 	dataitems 	dataItems� ��� l ���
�	��
  �	  �  � ��� l ������  �   get age of oldest file   � ��� .   g e t   a g e   o f   o l d e s t   f i l e� ��� r  ����� I  ������ (0 getageofoldestfile getAgeOfOldestFile� ��� o  ���� 0 	ame_watch  � ��� m  ���� ���  . m o v�  �  � o      �� 0 mydatavalue myDataValue� ��� Z ������ � ?  ����� o  ������ 0 mydatavalue myDataValue� o  ������ 0 oldestfileage oldestFileAge� r  ����� o  ������ 0 mydatavalue myDataValue� o      ���� 0 oldestfileage oldestFileAge�  �   � ��� r  ����� b  ����� b  ����� m  ���� ��� @ { " n a m e " : " a m e _ f i l e _ a g e " , " v a l u e " : "� o  ������ 0 mydatavalue myDataValue� m  ���� ���  " }� o      ���� 0 nextitem nextItem� ��� r  ���� b  ���� b  ���� o  ������ 0 	dataitems 	dataItems� m  ��� ���  ,� o  ���� 0 nextitem nextItem� o      ���� 0 	dataitems 	dataItems� ��� l ��������  ��  ��  � ��� l ��������  ��  ��  � ��� l ������  � $ check pending incoming waivers   � ��� < c h e c k   p e n d i n g   i n c o m i n g   w a i v e r s� ��� r  ��� b  ��� b  ��� m  
�� ���  l s  � n  
��� 1  ��
�� 
strq� o  
���� .0 incomingrawxmlwaivers incomingRawXmlWaivers� m  �� ���    |   g r e p   ' . x m l '� o      ���� 0 shscript  � ��� Q  G���� r  +��� c  )��� n  %��� 2 !%��
�� 
cpar� l !������ I !�����
�� .sysoexecTEXT���     TEXT� o  ���� 0 shscript  ��  ��  ��  � m  %(��
�� 
list� o      ���� 0 mylist myList� R      �����
�� .ascrerr ****      � ****� o      ���� 
0 errmsg  ��  � k  3G�� ��� I 3B�����
�� .ascrcmnt****      � ****� b  3>��� b  3<��� l 38������ I 38������
�� .misccurdldt    ��� null��  ��  ��  ��  � m  8;�� ��� 2 p r o b a b l y   n o   w a i v e r s   y e t :  � o  <=���� 
0 errmsg  ��  � ���� r  CG��� J  CE����  � o      ���� 0 mylist myList��  � ��� l HH��������  ��  ��  � ��� r  HQ� � n  HM 1  IM��
�� 
leng o  HI���� 0 mylist myList  o      ���� 0 mycount myCount�  r  Rf I  Rb������ 0 	readplist 	readPlist 	 o  SX���� 0 theplistpath thePListPath	 

 m  X[ � ( d r o p b o x _ w a i v e r _ c o u n t �� m  [^��
�� 
null��  ��   o      ���� 0 oldcount oldCount  r  gr \  gn o  gj���� 0 mycount myCount o  jm���� 0 oldcount oldCount o      ���� 0 dropbox_waiver_delta    I  s������� 0 
writeplist 
writePlist  o  ty���� 0 theplistpath thePListPath  m  y| � ( d r o p b o x _ w a i v e r _ d e l t a �� o  |���� 0 dropbox_waiver_delta  ��  ��     I  ����!���� 0 
writeplist 
writePlist! "#" o  ������ 0 theplistpath thePListPath# $%$ m  ��&& �'' ( d r o p b o x _ w a i v e r _ c o u n t% (��( o  ������ 0 mycount myCount��  ��    )*) l ����������  ��  ��  * +,+ l ����-.��  -   see if files are moving	   . �// 2   s e e   i f   f i l e s   a r e   m o v i n g 	, 010 r  ��232 I  ���������� 0 getepochtime getEpochTime��  ��  3 o      ���� 0 timenow timeNow1 454 r  ��676 I  ����8���� 0 number_to_string  8 9��9 [  ��:;: o  ������ :0 laststalefilechecktimestamp lastStaleFileCheckTimestamp; l ��<����< ]  ��=>= o  ������ 00 expectedmaxprocesstime expectedMaxProcessTime> m  ������  ����  ��  ��  ��  7 o      ���� 0 	nextcheck 	nextCheck5 ?@? Z  �AB��CA G  ��DED l ��F����F A  ��GHG o  ������ 0 	nextcheck 	nextCheckH o  ������ 0 timenow timeNow��  ��  E l ��I����I = ��JKJ o  ������ 0 
initialrun 
initialRunK m  ����
�� boovtrue��  ��  B k  �LL MNM l ����OP��  O  check files   P �QQ  c h e c k   f i l e sN RSR r  ��TUT I  ����V���� &0 checkpendingfiles checkPendingFilesV WXW o  ������ :0 pendingdropboxwaiverlistold pendingDropboxWaiverListOLDX Y��Y o  ������ 0 mylist myList��  ��  U o      ���� 0 isprocessing isProcessingS Z[Z r  ��\]\ o  ������ 0 mylist myList] o      ���� :0 pendingdropboxwaiverlistold pendingDropboxWaiverListOLD[ ^_^ Z  ��`a����` = ��bcb o  ������ 0 isprocessing isProcessingc m  ����
�� boovfalsa k  ��dd efe r  ��ghg m  ����
�� boovfalsh o      ���� 0 ishappy isHappyf iji r  ��klk b  ��mnm o  ������ 0 failedtests failedTestsn m  ��oo �pp " d r o p b o x _ w a i v e r s ,  l o      ���� 0 failedtests failedTestsj q��q Z  ��rs����r = ��tut o  ������ 0 dev_mode  u m  ����
�� boovtrues I ����v��
�� .sysodlogaskr        TEXTv m  ��ww �xx 2 N O T   H A P P Y   d r o p b o x _ w a i v e r s��  ��  ��  ��  ��  ��  _ y��y r   z{z m   ��
�� boovtrue{ o      ���� 40 updatestalefiletimestamp updateStaleFileTimestamp��  ��  C l ��|}��  |   not time to check   } �~~ $   n o t   t i m e   t o   c h e c k@ � l ��������  ��  ��  � ��� r  ��� o  ���� 0 mycount myCount� o      ���� 0 mydatavalue myDataValue� ��� r  ��� b  ��� b  ��� m  �� ��� F { " n a m e " : " d r o p b o x _ w a i v e r s " , " v a l u e " : "� o  ���� 0 mydatavalue myDataValue� m  �� ���  " }� o      ���� 0 nextitem nextItem� ��� r  '��� b  %��� b  #��� o  ���� 0 	dataitems 	dataItems� m  "�� ���  ,� o  #$�� 0 nextitem nextItem� o      �~�~ 0 	dataitems 	dataItems� ��� l ((�}�|�{�}  �|  �{  � ��� r  (/��� o  (+�z�z 0 dropbox_waiver_delta  � o      �y�y 0 mydatavalue myDataValue� ��� r  0=��� b  0;��� b  07��� m  03�� ��� P { " n a m e " : " d r o p b o x _ w a i v e r _ d e l t a " , " v a l u e " : "� o  36�x�x 0 mydatavalue myDataValue� m  7:�� ���  " }� o      �w�w 0 nextitem nextItem� ��� r  >G��� b  >E��� b  >C��� o  >?�v�v 0 	dataitems 	dataItems� m  ?B�� ���  ,� o  CD�u�u 0 nextitem nextItem� o      �t�t 0 	dataitems 	dataItems� ��� l HH�s�r�q�s  �r  �q  � ��� l HH�p���p  �   get age of oldest file   � ��� .   g e t   a g e   o f   o l d e s t   f i l e� ��� r  HW��� I  HS�o��n�o (0 getageofoldestfile getAgeOfOldestFile� ��� o  IL�m�m .0 incomingrawxmlwaivers incomingRawXmlWaivers� ��l� m  LO�� ���  . x m l�l  �n  � o      �k�k 0 mydatavalue myDataValue� ��� Z Xi���j�i� ?  X]��� o  X[�h�h 0 mydatavalue myDataValue� o  [\�g�g 0 oldestfileage oldestFileAge� r  `e��� o  `c�f�f 0 mydatavalue myDataValue� o      �e�e 0 oldestfileage oldestFileAge�j  �i  � ��� r  jw��� b  ju��� b  jq��� m  jm�� ��� L { " n a m e " : " d r o p b o x _ w a i v e r _ a g e " , " v a l u e " : "� o  mp�d�d 0 mydatavalue myDataValue� m  qt�� ���  " }� o      �c�c 0 nextitem nextItem� ��� r  x���� b  x��� b  x}��� o  xy�b�b 0 	dataitems 	dataItems� m  y|�� ���  ,� o  }~�a�a 0 nextitem nextItem� o      �`�` 0 	dataitems 	dataItems� ��� l ���_�^�]�_  �^  �]  � ��� l ���\���\  �  get memory stats   � ���   g e t   m e m o r y   s t a t s� ��� Q  ������ r  ����� I ���[��Z
�[ .sysoexecTEXT���     TEXT� m  ���� ��� l t o p   - l   1   |   h e a d   - n   1 0   |   g r e p   P h y s M e m   |   s e d   ' s / ,   / n   / g '�Z  � o      �Y�Y 0 memory_stats  � R      �X��W
�X .ascrerr ****      � ****� o      �V�V 
0 errmsg  �W  � r  ����� b  ����� m  ���� ��� . E R R O R   R E A D I N G   M E M O R Y   -  � o  ���U�U 
0 errmsg  � o      �T�T 0 memory_stats  � ��� l ���S�R�Q�S  �R  �Q  �    r  �� o  ���P�P 0 memory_stats   o      �O�O 0 mydatavalue myDataValue  r  �� b  ��	 b  ��

 m  �� � B { " n a m e " : " m e m o r y _ s t a t u s " , " v a l u e " : " o  ���N�N 0 mydatavalue myDataValue	 m  �� �  " } o      �M�M 0 nextitem nextItem  r  �� b  �� b  �� o  ���L�L 0 	dataitems 	dataItems m  �� �  , o  ���K�K 0 nextitem nextItem o      �J�J 0 	dataitems 	dataItems  l ���I�H�G�I  �H  �G    l ���F�F    get free disk stats    �   & g e t   f r e e   d i s k   s t a t s !"! r  ��#$# c  ��%&% I  ���E�D�C�E  0 getfreediskpct getFreeDiskPct�D  �C  & m  ���B
�B 
long$ o      �A�A 0 freediskpct freeDiskPct" '(' r  ��)*) c  ��+,+ I  ���@�?�>�@ (0 getfreediskspacegb getFreeDiskSpaceGB�?  �>  , m  ���=
�= 
long* o      �<�< 0 
freediskgb 
freeDiskGB( -.- Z  �/0�;�:/ G  ��121 l ��3�9�83 A  ��454 o  ���7�7 0 
freediskgb 
freeDiskGB5 m  ���6�6 �9  �8  2 l ��6�5�46 A  ��787 o  ���3�3 0 freediskpct freeDiskPct8 m  ���2�2 
�5  �4  0 k  �99 :;: r  ��<=< m  ���1
�1 boovfals= o      �0�0 0 ishappy isHappy; >?> r  �@A@ b  ��BCB o  ���/�/ 0 failedtests failedTestsC m  ��DD �EE  d i s k _ f r e e _ g b ,  A o      �.�. 0 failedtests failedTests? F�-F Z  GH�,�+G = 	IJI o  �*�* 0 dev_mode  J m  �)
�) boovtrueH I �(K�'
�( .sysodisAaleR        TEXTK m  LL �MM , N O T   H A P P Y   d i s k _ f r e e _ g b�'  �,  �+  �-  �;  �:  . NON l �&�%�$�&  �%  �$  O PQP r  #RSR o  �#�# 0 
freediskgb 
freeDiskGBS o      �"�" 0 mydatavalue myDataValueQ TUT r  $1VWV b  $/XYX b  $+Z[Z m  $'\\ �]] @ { " n a m e " : " d i s k _ f r e e _ g b " , " v a l u e " : "[ o  '*�!�! 0 mydatavalue myDataValueY m  +.^^ �__  " }W o      � �  0 nextitem nextItemU `a` r  2;bcb b  29ded b  27fgf o  23�� 0 	dataitems 	dataItemsg m  36hh �ii  ,e o  78�� 0 nextitem nextItemc o      �� 0 	dataitems 	dataItemsa jkj l <<����  �  �  k lml r  <Cnon o  <?�� 0 freediskpct freeDiskPcto o      �� 0 mydatavalue myDataValuem pqp r  DQrsr b  DOtut b  DKvwv m  DGxx �yy J { " n a m e " : " d i s k _ f r e e _ p e r c e n t " , " v a l u e " : "w o  GJ�� 0 mydatavalue myDataValueu m  KNzz �{{  " }s o      �� 0 nextitem nextItemq |}| r  R[~~ b  RY��� b  RW��� o  RS�� 0 	dataitems 	dataItems� m  SV�� ���  ,� o  WX�� 0 nextitem nextItem o      �� 0 	dataitems 	dataItems} ��� l \\����  �  �  � ��� l \\����  � !  get computer ID from pList   � ��� 6   g e t   c o m p u t e r   I D   f r o m   p L i s t� ��� Q  \����� r  _s��� I  _o���� 0 	readplist 	readPlist� ��� o  `e�� 0 theplistpath thePListPath� ��� m  eh�� ���  c o m p u t e r _ i d� ��� m  hk�� ���  m a c S t a d i u m�  �  � o      �
�
 0 mydatavalue myDataValue� R      �	��
�	 .ascrerr ****      � ****� o      �� 
0 errmsg  �  � r  {���� b  {���� m  {~�� ��� 0 C o m p u t e r   I D   n o t   f o u n d   -  � o  ~�� 
0 errmsg  � o      �� 0 mydatavalue myDataValue� ��� r  ����� b  ����� b  ����� m  ���� ��� > { " n a m e " : " c o m p u t e r _ i d " , " v a l u e " : "� o  ���� 0 mydatavalue myDataValue� m  ���� ���  " }� o      �� 0 nextitem nextItem� ��� r  ����� b  ����� b  ����� o  ���� 0 	dataitems 	dataItems� m  ���� ���  ,� o  ���� 0 nextitem nextItem� o      � �  0 	dataitems 	dataItems� ��� l ����������  ��  ��  � ��� l ��������  �  get IP address   � ���  g e t   I P   a d d r e s s� ��� r  ����� n  ����� 1  ����
�� 
siip� l �������� e  ���� I ��������
�� .sysosigtsirr   ��� null��  ��  ��  ��  � o      ���� 0 mydatavalue myDataValue� ��� r  ����� b  ����� b  ����� m  ���� ��� R { " n a m e " : " c o m p u t e r _ I P v 4 _ a d d r e s s " , " v a l u e " : "� o  ������ 0 mydatavalue myDataValue� m  ���� ���  " }� o      ���� 0 nextitem nextItem� ��� r  ����� b  ����� b  ����� o  ������ 0 	dataitems 	dataItems� m  ���� ���  ,� o  ������ 0 nextitem nextItem� o      ���� 0 	dataitems 	dataItems� ��� l ����������  ��  ��  � ��� l ����������  ��  ��  � ��� l ����������  ��  ��  � ��� r  ����� o  ������ 0 oldestfileage oldestFileAge� o      ���� 0 mydatavalue myDataValue� ��� r  ����� b  ����� b  ����� m  ���� ��� B { " n a m e " : " o l d e s t F i l e A g e " , " v a l u e " : "� o  ������ 0 mydatavalue myDataValue� m  ���� ���  " }� o      ���� 0 nextitem nextItem� ��� r  ����� b  ����� b  ����� o  ������ 0 	dataitems 	dataItems� m  ���� ���  ,� o  ������ 0 nextitem nextItem� o      ���� 0 	dataitems 	dataItems�    l ����������  ��  ��    Z  ������ ?  �� o  ������ 0 oldestfileage oldestFileAge o  ������ 00 oldestfileagethreshold oldestFileAgeThreshold k  �� 	
	 r  �� m  ����
�� boovfals o      ���� 0 ishappy isHappy
 �� r  �� b  �� o  ������ 0 failedtests failedTests m  �� �  o l d e s t F i l e A g e ,   o      ���� 0 failedtests failedTests��  ��  ��    l ����������  ��  ��    Z  �(�� = �� o  ������ 0 ishappy isHappy m  ����
�� boovtrue r   m   �    U P o      ���� 0 mydatavalue myDataValue��   k  (!! "#" r  $%$ m  && �''  D O W N% o      ���� 0 mydatavalue myDataValue# (��( Z  ()*����) = +,+ o  ���� 0 dev_mode  , m  ��
�� boovtrue* I $��-��
�� .sysodisAaleR        TEXT- m   .. �// " N O T   H A P P Y   O V E R A L L��  ��  ��  ��   010 r  )6232 b  )4454 b  )0676 m  ),88 �99 D { " n a m e " : " o v e r a l l _ s t a t u s " , " v a l u e " : "7 o  ,/���� 0 mydatavalue myDataValue5 m  03:: �;;  " }3 o      ���� 0 nextitem nextItem1 <=< r  7@>?> b  7>@A@ b  7<BCB o  78���� 0 	dataitems 	dataItemsC m  8;DD �EE  ,A o  <=���� 0 nextitem nextItem? o      ���� 0 	dataitems 	dataItems= FGF l AA��������  ��  ��  G HIH r  AFJKJ o  AB���� 0 failedtests failedTestsK o      ���� 0 mydatavalue myDataValueI LML r  GTNON b  GRPQP b  GNRSR m  GJTT �UU > { " n a m e " : " f a i l e d T e s t s " , " v a l u e " : "S o  JM���� 0 mydatavalue myDataValueQ m  NQVV �WW  " }O o      ���� 0 nextitem nextItemM XYX r  U^Z[Z b  U\\]\ b  UZ^_^ o  UV���� 0 	dataitems 	dataItems_ m  VY`` �aa  ,] o  Z[���� 0 nextitem nextItem[ o      ���� 0 	dataitems 	dataItemsY bcb l __��������  ��  ��  c ded r  _hfgf I  _d�������� 0 getepochtime getEpochTime��  ��  g o      ���� 0 mydatavalue myDataValuee hih r  ivjkj b  itlml b  ipnon m  ilpp �qq L { " n a m e " : " c u r r e n t _ e p o c h _ t i m e " , " v a l u e " : "o o  lo���� 0 mydatavalue myDataValuem m  psrr �ss  " }k o      ���� 0 nextitem nextItemi tut r  w�vwv b  w~xyx b  w|z{z o  wx���� 0 	dataitems 	dataItems{ m  x{|| �}}  ,y o  |}���� 0 nextitem nextItemw o      ���� 0 	dataitems 	dataItemsu ~~ l ����������  ��  ��   ��� Z  ��������� = ����� o  ������ 0 dev_mode  � m  ����
�� boovtrue� I �������
�� .sysodlogaskr        TEXT� o  ������ 0 	dataitems 	dataItems��  ��  ��  � ��� l ����������  ��  ��  � ��� I  ��������� "0 sendstatustobpc sendStatusToBPC� ���� o  ������ 0 	dataitems 	dataItems��  ��  � ��� r  ����� I  ���������� 0 getepochtime getEpochTime��  ��  � o      ���� 80 laststatsgatheredtimestamp lastStatsGatheredTimestamp� ��� l ����������  ��  ��  � ��� Z  ��������� = ����� o  ������ 40 updatestalefiletimestamp updateStaleFileTimestamp� m  ����
�� boovtrue� r  ����� I  ���������� 0 getepochtime getEpochTime��  ��  � o      ���� :0 laststalefilechecktimestamp lastStaleFileCheckTimestamp��  ��  � ��� l ����������  ��  ��  � ���� l ����������  ��  ��  ��  � ��� l     ��������  ��  ��  � ��� i   � ���� I      ���~� "0 sendstatustobpc sendStatusToBPC� ��}� o      �|�| 0 	dataitems 	dataItems�}  �~  � k     ��� ��� l     �{���{  � [ U https://dev-api.bpcreates.com/stats/userDataCount?e=1000&type=FIRST+NAME&value=stacy   � ��� �   h t t p s : / / d e v - a p i . b p c r e a t e s . c o m / s t a t s / u s e r D a t a C o u n t ? e = 1 0 0 0 & t y p e = F I R S T + N A M E & v a l u e = s t a c y� ��� l     �z�y�x�z  �y  �x  � ��� r     	��� n     ��� 1    �w
�w 
siip� l    ��v�u� e     �� I    �t�s�r
�t .sysosigtsirr   ��� null�s  �r  �v  �u  � o      �q�q 0 myip myIP� ��� l  
 
�p�o�n�p  �o  �n  � ��� r   
 ��� m   
 �� ��� 2 a p i . b p c r e a t e s . c o m / a p i / c u i� o      �m�m 0 
theurlbase 
theURLbase� ��� r    $��� I    �l��k�l 0 	readplist 	readPlist� ��� o    �j�j 0 theplistpath thePListPath� ��� m    �� ���  d e v _ e n d p o i n t s� ��i� o    �h�h 0 devendpoints devEndpoints�i  �k  � o      �g�g 0 devendpoints devEndpoints� ��� r   % 2��� c   % ,��� o   % *�f�f 0 devendpoints devEndpoints� m   * +�e
�e 
bool� o      �d�d 0 devendpoints devEndpoints� ��� Z   3 J���c�� =  3 :��� o   3 8�b�b 0 devendpoints devEndpoints� m   8 9�a
�a boovtrue� r   = B��� b   = @��� m   = >�� ���  h t t p s : / / d e v -� o   > ?�`�` 0 
theurlbase 
theURLbase� o      �_�_ 0 
theurlbase 
theURLbase�c  � r   E J��� b   E H��� m   E F�� ���  h t t p s : / /� o   F G�^�^ 0 
theurlbase 
theURLbase� o      �]�] 0 
theurlbase 
theURLbase� ��� l  K K�\�[�Z�\  �[  �Z  � ��� r   K V��� b   K T��� b   K R��� b   K P��� b   K N��� m   K L�� ���0 { " a u t h " :   { " a g e n c y " :   " M a c S t a d i u m _ M o n i t o r i n g " , " a p i K e y " :   " 4 b 4 9 5 6 a d c 6 6 0 0 7 f f 9 9 0 f a 6 4 5 1 8 d 7 7 2 b 9 " } , " s u b m i s s i o n T y p e " :   " a d d " , " u i d L i s t " :   [ { " n a m e " :   " R F I D " , " v a l u e " :   "� o   L M�Y�Y 0 myip myIP� m   N O�� ��� x " } ] , " e v e n t C o d e " :   " 3 Q J V D " , " d i P a r a m F i l t e r " :   " r " , " d a t a I t e m s " :   [� o   P Q�X�X 0 	dataitems 	dataItems� m   R S�� ���  ] }� o      �W�W 0 
thepayload 
thePayload� ��� l  W W�V�U�T�V  �U  �T  � ��� l  W W�S�R�Q�S  �R  �Q  � ��� r   W b��� b   W `� � b   W ^ b   W \ m   W X � � c u r l   - v   - H   " A c c e p t :   a p p l i c a t i o n / j s o n "   - H   " C o n t e n t - t y p e :   a p p l i c a t i o n / j s o n "   - X   P O S T   - d   n   X [ 1   Y [�P
�P 
strq o   X Y�O�O 0 
thepayload 
thePayload m   \ ]		 �

     o   ^ _�N�N 0 
theurlbase 
theURLbase� o      �M�M 0 shscript  �  l  c c�L�K�J�L  �K  �J    Z   c v�I�H =  c j o   c h�G�G 0 dev_mode   m   h i�F
�F boovtrue I  m r�E�D
�E .sysodlogaskr        TEXT o   m n�C�C 0 shscript  �D  �I  �H    l  w w�B�A�@�B  �A  �@    Q   w � k   z �  r   z � I  z �? �>
�? .sysoexecTEXT���     TEXT  o   z {�=�= 0 shscript  �>   o      �<�< 0 	myoutcome 	myOutcome !�;! L   � �"" o   � ��:�: 0 	myoutcome 	myOutcome�;   R      �9#�8
�9 .ascrerr ****      � ****# o      �7�7 
0 errmsg  �8   L   � �$$ o   � ��6�6 
0 errmsg   %�5% l  � ��4�3�2�4  �3  �2  �5  � &'& l     �1�0�/�1  �0  �/  ' ()( l     �.�-�,�.  �-  �,  ) *+* l     �+�*�)�+  �*  �)  + ,-, i   � �./. I      �(�'�&�( 0 setvars setVars�'  �&  / k    J00 121 Q    H3453 k   >66 787 r    	9:9 c    ;<; J    �%�%  < m    �$
�$ 
list: o      �#�# 00 pendingaewaiverlistold pendingAEWaiverListOLD8 =>= r   
 ?@? c   
 ABA J   
 �"�"  B m    �!
�! 
list@ o      � �  .0 pendingamefilelistold pendingAMEFileListOLD> CDC r    EFE c    GHG J    ��  H m    �
� 
listF o      �� :0 pendingdropboxwaiverlistold pendingDropboxWaiverListOLDD IJI l   ����  �  �  J KLK r    &MNM I    $�O�� 0 	readplist 	readPlistO PQP o    �� 0 theplistpath thePListPathQ RSR m    TT �UU  c o m p u t e r _ i dS V�V m     �
� 
null�  �  N o      �� 0 computer_id  L WXW l  ' '����  �  �  X YZY r   ' =[\[ I   ' 7�]�� 0 	readplist 	readPlist] ^_^ o   ( -�� 0 theplistpath thePListPath_ `a` m   - .bb �cc   c o m p u t e r U s e r N a m ea d�d o   . 3�� $0 computerusername computerUserName�  �  \ o      �� $0 computerusername computerUserNameZ efe l  > >�
�	��
  �	  �  f ghg r   > Viji c   > Pklk I   > N�m�� 0 	readplist 	readPlistm non o   ? D�� 0 theplistpath thePListPatho pqp m   D Err �ss  d e v _ m o d eq t�t o   E J�� 0 dev_mode  �  �  l m   N O�
� 
boolj o      �� 0 dev_mode  h uvu r   W owxw c   W iyzy I   W g� {���  0 	readplist 	readPlist{ |}| o   X ]���� 0 theplistpath thePListPath} ~~ m   ] ^�� ���  d e v _ e n d p o i n t s ���� o   ^ c���� 0 devendpoints devEndpoints��  ��  z m   g h��
�� 
boolx o      ���� 0 devendpoints devEndpointsv ��� l  p p��������  ��  ��  � ��� r   p ���� c   p ~��� I   p |������� 0 	readplist 	readPlist� ��� o   q v���� 0 theplistpath thePListPath� ��� m   v w�� ���  c l e a n u p� ���� m   w x��
�� boovtrue��  ��  � m   | }��
�� 
bool� o      ���� (0 performfilecleanup performFileCleanup� ��� l  � ���������  ��  ��  � ��� r   � ���� I   � �������� 0 	readplist 	readPlist� ��� o   � ����� 0 theplistpath thePListPath� ��� m   � ��� ���  s t a t i o n _ t y p e� ���� m   � ���
�� 
null��  ��  � o      ���� 0 stationtype stationType� ��� l  � ���������  ��  ��  � ��� l  � �������  � # set this to false for testing   � ��� : s e t   t h i s   t o   f a l s e   f o r   t e s t i n g� ��� l  � �������  �  set automated to true   � ��� * s e t   a u t o m a t e d   t o   t r u e� ��� r   � ���� c   � ���� I   � �������� 0 	readplist 	readPlist� ��� o   � ����� 0 theplistpath thePListPath� ��� m   � ��� ���  a u t o m a t e d� ���� o   � ����� 0 	automated  ��  ��  � m   � ���
�� 
bool� o      ���� 0 	automated  � ��� l  � ���������  ��  ��  � ��� l  � �������  �  set loopDelaySecs to 10   � ��� . s e t   l o o p D e l a y S e c s   t o   1 0� ��� r   � ���� c   � ���� I   � �������� 0 	readplist 	readPlist� ��� o   � ����� 0 theplistpath thePListPath� ��� m   � ��� ���  l o o p _ d e l a y� ���� o   � ����� 0 loopdelaysecs loopDelaySecs��  ��  � m   � ���
�� 
long� o      ���� 0 loopdelaysecs loopDelaySecs� ��� l  � ���������  ��  ��  � ��� l  � �������  �  set processDelaySecs to 5   � ��� 2 s e t   p r o c e s s D e l a y S e c s   t o   5� ��� r   � ���� c   � ���� I   � �������� 0 	readplist 	readPlist� ��� o   � ����� 0 theplistpath thePListPath� ��� m   � ��� ���  p r o c e s s _ d e l a y� ���� o   � ����� $0 processdelaysecs processDelaySecs��  ��  � m   � ���
�� 
long� o      ���� $0 processdelaysecs processDelaySecs� ��� l  � ���������  ��  ��  � ��� l  � �������  � I Cset waiver sequence, the interger to look for at end of waiver name   � ��� � s e t   w a i v e r   s e q u e n c e ,   t h e   i n t e r g e r   t o   l o o k   f o r   a t   e n d   o f   w a i v e r   n a m e� ��� r   ���� c   � ���� I   � �������� 0 	readplist 	readPlist� ��� o   � ����� 0 theplistpath thePListPath� ��� m   � ��� ���  w a i v e r _ s e q u e n c e� ���� o   � ����� $0 waivergrepstring waiverGrepString��  ��  � m   � ���
�� 
list� o      ���� $0 waivergrepstring waiverGrepString� ��� l ��������  ��  ��  � ��� l ������  � 1 + set delay when checking AE output filesize   � �   V   s e t   d e l a y   w h e n   c h e c k i n g   A E   o u t p u t   f i l e s i z e�  r   c   I  ������ 0 	readplist 	readPlist 	 o  ���� 0 theplistpath thePListPath	 

 m   � $ f i l e s i z e C h e c k D e l a y �� o  ���� (0 filesizecheckdelay filesizeCheckDelay��  ��   m  ��
�� 
long o      ���� (0 filesizecheckdelay filesizeCheckDelay  l ��������  ��  ��    l ����   G A set minimum file size expected from AE output (before transcode)    � �   s e t   m i n i m u m   f i l e   s i z e   e x p e c t e d   f r o m   A E   o u t p u t   ( b e f o r e   t r a n s c o d e )  r  ; c  5 I  1������ 0 	readplist 	readPlist  o   %���� 0 theplistpath thePListPath   m  %(!! �"" : f i l e s i z e C h e c k _ m i n A E O u t p u t S i z e  #��# o  (-���� >0 filesizecheck_minaeoutputsize filesizeCheck_minAEOutputSize��  ��   m  14��
�� 
long o      ���� >0 filesizecheck_minaeoutputsize filesizeCheck_minAEOutputSize $%$ l <<��������  ��  ��  % &'& l <<��������  ��  ��  ' (��( L  <>)) m  <=��
�� boovtrue��  4 R      ��*��
�� .ascrerr ****      � ***** o      ���� 
0 errmsg  ��  5 L  FH++ o  FG���� 
0 errmsg  2 ,��, l II��������  ��  ��  ��  - -.- l     ��������  ��  ��  . /0/ l     ��������  ��  ��  0 121 l     ��������  ��  ��  2 343 i   � �565 I      �������� 0 setpaths setPaths��  ��  6 k    \77 898 Q    Z:;<: k   P== >?> r    @A@ I    ��B���� 0 	readplist 	readPlistB CDC o    	���� 0 theplistpath thePListPathD EFE m   	 
GG �HH  d r o p b o x _ p a t hF I��I m   
 JJ �KK 2 / U s e r s / b l u e p i x e l / D r o p b o x /��  ��  A o      ���� "0 dropboxrootpath dropboxRootPath? LML Z    +NO��PN =   QRQ o    ���� 0 devendpoints devEndpointsR m    ��
�� boovtrueO r    #STS b    !UVU b    WXW o    �� "0 dropboxrootpath dropboxRootPathX m    YY �ZZ  d e v _V o     �~�~ 0 stationtype stationTypeT o      �}�} "0 dropboxrootpath dropboxRootPath��  P r   & +[\[ b   & )]^] o   & '�|�| "0 dropboxrootpath dropboxRootPath^ o   ' (�{�{ 0 stationtype stationType\ o      �z�z "0 dropboxrootpath dropboxRootPathM _`_ l  , ,�y�x�w�y  �x  �w  ` aba r   , 1cdc b   , /efe o   , -�v�v "0 dropboxrootpath dropboxRootPathf m   - .gg �hh  / w a i v e r s /d o      �u�u .0 incomingrawxmlwaivers incomingRawXmlWaiversb iji r   2 7klk b   2 5mnm o   2 3�t�t "0 dropboxrootpath dropboxRootPathn m   3 4oo �pp  / m e d i a /l o      �s�s :0 incomingrawmediafolderposix incomingRawMediaFolderPosixj qrq l  8 8�r�q�p�r  �q  �p  r sts r   8 Cuvu b   8 Awxw b   8 ?yzy m   8 9{{ �||  / U s e r s /z o   9 >�o�o $0 computerusername computerUserNamex m   ? @}} �~~ : / D o c u m e n t s / p e n d i n g _ t r a n s c o d e /v o      �n�n :0 transcodependingfolderposix transcodePendingFolderPosixt � Q   D Z���� r   G O��� I   G M�m��l�m 0 folderexists folderExists� ��k� o   H I�j�j :0 transcodependingfolderposix transcodePendingFolderPosix�k  �l  � o      �i�i 0 filetest fileTest� R      �h��g
�h .ascrerr ****      � ****� o      �f�f 
0 errmsg  �g  � r   W Z��� m   W X�e
�e boovtrue� o      �d�d 0 filetest fileTest� ��� Z   [ r���c�b� =  [ ^��� o   [ \�a�a 0 filetest fileTest� m   \ ]�`
�` boovfals� I  a n�_��^
�_ .sysoexecTEXT���     TEXT� b   a j��� m   a d�� ���  m k d i r  � n   d i��� 1   e i�]
�] 
strq� o   d e�\�\ :0 transcodependingfolderposix transcodePendingFolderPosix�^  �c  �b  � ��� l  s s�[�Z�Y�[  �Z  �Y  � ��� r   s ���� b   s ���� b   s |��� m   s v�� ���  / U s e r s /� o   v {�X�X $0 computerusername computerUserName� m   | �� ��� 8 / D o c u m e n t s / m e d i a _ t r a n s c o d e d /� o      �W�W >0 transcodecompletedfolderposix transcodeCompletedFolderPosix� ��� Q   � ����� r   � ���� I   � ��V��U�V 0 folderexists folderExists� ��T� o   � ��S�S >0 transcodecompletedfolderposix transcodeCompletedFolderPosix�T  �U  � o      �R�R 0 filetest fileTest� R      �Q��P
�Q .ascrerr ****      � ****� o      �O�O 
0 errmsg  �P  � r   � ���� m   � ��N
�N boovtrue� o      �M�M 0 filetest fileTest� ��� Z   � ����L�K� =  � ���� o   � ��J�J 0 filetest fileTest� m   � ��I
�I boovfals� I  � ��H��G
�H .sysoexecTEXT���     TEXT� b   � ���� m   � ��� ���  m k d i r  � n   � ���� 1   � ��F
�F 
strq� o   � ��E�E >0 transcodecompletedfolderposix transcodeCompletedFolderPosix�G  �L  �K  � ��� l  � ��D�C�B�D  �C  �B  � ��� r   � ���� b   � ���� b   � ���� m   � ��� ���  / U s e r s /� o   � ��A�A $0 computerusername computerUserName� m   � ��� ��� 6 / D o c u m e n t s / w a i v e r s _ p e n d i n g /� o      �@�@ @0 pendingwaiversforaefolderposix pendingWaiversForAEFolderPosix� ��� Q   � ����� r   � ���� I   � ��?��>�? 0 folderexists folderExists� ��=� o   � ��<�< @0 pendingwaiversforaefolderposix pendingWaiversForAEFolderPosix�=  �>  � o      �;�; 0 filetest fileTest� R      �:��9
�: .ascrerr ****      � ****� o      �8�8 
0 errmsg  �9  � r   � ���� m   � ��7
�7 boovtrue� o      �6�6 0 filetest fileTest� ��� Z   � ����5�4� =  � ���� o   � ��3�3 0 filetest fileTest� m   � ��2
�2 boovfals� I  � ��1��0
�1 .sysoexecTEXT���     TEXT� b   � ���� m   � ��� ���  m k d i r  � n   � ���� 1   � ��/
�/ 
strq� o   � ��.�. @0 pendingwaiversforaefolderposix pendingWaiversForAEFolderPosix�0  �5  �4  � ��� l  � ��-�,�+�-  �,  �+  � ��� l  � ��*�)�(�*  �)  �(  � ��� r   ���� b   �
��� b   ���� m   � �� ���  / U s e r s /� o   �'�' $0 computerusername computerUserName� m  	�� ��� , / D o c u m e n t s / m e d i a _ p o o l /� o      �&�& 0 
media_pool  � ��� Q  '���� r  ��� I  �%��$�% 0 folderexists folderExists� ��#� o  �"�" 0 
media_pool  �#  �$  � o      �!�! 0 filetest fileTest� R      � ��
�  .ascrerr ****      � ****�  �  � r  $'� � m  $%�
� boovtrue  o      �� 0 filetest fileTest�  Z  (A�� = (+ o  ()�� 0 filetest fileTest m  )*�
� boovfals I .=��
� .sysoexecTEXT���     TEXT b  .9	 m  .1

 �  m k d i r  	 n  18 1  48�
� 
strq o  14�� 0 
media_pool  �  �  �    l BB����  �  �    r  BS b  BO b  BK m  BE �  / U s e r s / o  EJ�� $0 computerusername computerUserName m  KN � 8 / D o c u m e n t s / a m e _ w a t c h / O u t p u t / o      �� 0 processed_media    Q  Tl  r  Wa!"! I  W_�#�� 0 folderexists folderExists# $�$ o  X[�� 0 processed_media  �  �  " o      �
�
 0 filetest fileTest R      �	��
�	 .ascrerr ****      � ****�  �    r  il%&% m  ij�
� boovtrue& o      �� 0 filetest fileTest '(' Z  m�)*��) = mp+,+ o  mn�� 0 filetest fileTest, m  no�
� boovfals* I s�� -��
�  .sysoexecTEXT���     TEXT- b  s~./. m  sv00 �11  m k d i r  / n  v}232 1  y}��
�� 
strq3 o  vy���� 0 processed_media  ��  �  �  ( 454 l ����������  ��  ��  5 676 r  ��898 b  ��:;: b  ��<=< m  ��>> �??  / U s e r s /= o  ������ $0 computerusername computerUserName; m  ��@@ �AA * / D o c u m e n t s / a e _ o u t p u t /9 o      ���� 0 	ae_output  7 BCB Q  ��DEFD r  ��GHG I  ����I���� 0 folderexists folderExistsI J��J o  ������ 0 	ae_output  ��  ��  H o      ���� 0 filetest fileTestE R      ������
�� .ascrerr ****      � ****��  ��  F r  ��KLK m  ����
�� boovtrueL o      ���� 0 filetest fileTestC MNM Z  ��OP����O = ��QRQ o  ������ 0 filetest fileTestR m  ����
�� boovfalsP I ����S��
�� .sysoexecTEXT���     TEXTS b  ��TUT m  ��VV �WW  m k d i r  U n  ��XYX 1  ����
�� 
strqY o  ������ 0 	ae_output  ��  ��  ��  N Z[Z l ����������  ��  ��  [ \]\ r  ��^_^ b  ��`a` b  ��bcb m  ��dd �ee  / U s e r s /c o  ������ $0 computerusername computerUserNamea m  ��ff �gg * / D o c u m e n t s / a m e _ w a t c h /_ o      ���� 0 	ame_watch  ] hih Q  ��jklj r  ��mnm I  ����o���� 0 folderexists folderExistso p��p o  ������ 0 	ame_watch  ��  ��  n o      ���� 0 filetest fileTestk R      ������
�� .ascrerr ****      � ****��  ��  l r  ��qrq m  ����
�� boovtruer o      ���� 0 filetest fileTesti sts Z  �uv����u = ��wxw o  ������ 0 filetest fileTestx m  ����
�� boovfalsv I ���y��
�� .sysoexecTEXT���     TEXTy b  �z{z m  � || �}}  m k d i r  { n   ~~ 1  ��
�� 
strq o   ���� 0 	ame_watch  ��  ��  ��  t ��� l ��������  ��  ��  � ��� r  ��� b  ��� o  ���� "0 dropboxrootpath dropboxRootPath� m  �� ���  / i n g e s t o r /� o      ���� 0 spock_ingestor  � ��� Q  3���� r  (��� I  &������� 0 folderexists folderExists� ���� o  "���� 0 spock_ingestor  ��  ��  � o      ���� 0 filetest fileTest� R      ������
�� .ascrerr ****      � ****��  ��  � r  03��� m  01��
�� boovtrue� o      ���� 0 filetest fileTest� ��� Z  4M������� = 47��� o  45���� 0 filetest fileTest� m  56��
�� boovfals� I :I�����
�� .sysoexecTEXT���     TEXT� b  :E��� m  :=�� ���  m k d i r  � n  =D��� 1  @D��
�� 
strq� o  =@���� 0 spock_ingestor  ��  ��  ��  � ��� l NN��������  ��  ��  � ���� L  NP�� m  NO��
�� boovtrue��  ; R      �����
�� .ascrerr ****      � ****� o      ���� 
0 errmsg  ��  < L  XZ�� o  XY���� 
0 errmsg  9 ���� l [[��������  ��  ��  ��  4 ��� l     ��������  ��  ��  � ��� l     ��������  ��  ��  � ��� l     ������  � C =#############################################################   � ��� z # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #� ��� l     ������  � % ## --HELPER & UTILITY METHODS--   � ��� > # #   - - H E L P E R   &   U T I L I T Y   M E T H O D S - -� ��� l     ������  � % ## --HELPER & UTILITY METHODS--   � ��� > # #   - - H E L P E R   &   U T I L I T Y   M E T H O D S - -� ��� l     ������  � C =#############################################################   � ��� z # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #� ��� l     ��������  ��  ��  � ��� i   � ���� I      ������� 0 read_waiver  � ���� o      ���� 0 	thewaiver  ��  ��  � k     r�� ��� l     ������  �  get waiver details   � ��� $ g e t   w a i v e r   d e t a i l s� ��� r     
��� I     ������� 0 get_element  � ��� o    ���� 0 	thewaiver  � ��� m    �� ���  J o b� ���� m    �� ���  i d��  ��  � o      ���� 0 	waiver_id  � ��� r    ��� I    ������� 0 get_element  � ��� o    ���� 0 	thewaiver  � ��� m    �� ���  J o b� ���� m    �� ���  c r e a t e d��  ��  � o      �� 0 waiver_created  � ��� l   ����  �  �  � ��� r     ��� I    ���� 0 get_element  � ��� o    �� 0 	thewaiver  � ��� m    �� ���  J o b� ��� m    �� ���  p r o g r a m�  �  � o      �� 0 waiver_program  � ��� r   ! +��� I   ! )���� 0 get_element  �    o   " #�� 0 	thewaiver    m   # $ �  J o b � m   $ % �  a c t i v a t i o n�  �  � o      �� 0 waiver_activation  � 	
	 r   , 6 I   , 4��� 0 get_element    o   - .�� 0 	thewaiver    m   . / �  J o b �~ m   / 0 �  t e a m�~  �   o      �}�} 0 waiver_team  
  r   7 A I   7 ?�|�{�| 0 get_element    o   8 9�z�z 0 	thewaiver    m   9 :   �!!  J o b "�y" m   : ;## �$$ 
 e v e n t�y  �{   o      �x�x 0 waiver_event   %&% r   B L'(' I   B J�w)�v�w 0 get_element  ) *+* o   C D�u�u 0 	thewaiver  + ,-, m   D E.. �//  J o b- 0�t0 m   E F11 �22  t o t a l _ r a w _ f i l e s�t  �v  ( o      �s�s 0 waiver_total_raw_files  & 343 l  M M�r�q�p�r  �q  �p  4 565 l  M M�o�n�m�o  �n  �m  6 787 r   M m9:9 K   M k;; �l<=�l 0 	waiver_id  < o   N O�k�k 0 	waiver_id  = �j>?�j 0 waiver_created  > o   R S�i�i 0 waiver_created  ? �h@A�h 0 waiver_program  @ o   V W�g�g 0 waiver_program  A �fBC�f 0 waiver_activation  B o   Z [�e�e 0 waiver_activation  C �dDE�d 0 waiver_team  D o   ^ _�c�c 0 waiver_team  E �bFG�b 0 waiver_event  F o   b c�a�a 0 waiver_event  G �`H�_�` 0 waiver_total_raw_files  H o   f g�^�^ 0 waiver_total_raw_files  �_  : o      �]�] 0 waiver_data  8 IJI l  n n�\�[�Z�\  �[  �Z  J KLK l  n n�Y�X�W�Y  �X  �W  L MNM L   n pOO o   n o�V�V 0 waiver_data  N P�UP l  q q�T�S�R�T  �S  �R  �U  � QRQ l     �Q�P�O�Q  �P  �O  R STS i   � �UVU I      �NW�M�N 0 get_element  W XYX o      �L�L 0 	thewaiver  Y Z[Z o      �K�K 0 node  [ \�J\ o      �I�I 0 element_name  �J  �M  V Q     =]^_] O    `a` k    bb cdc l   �Hef�H  e  get top level   f �gg  g e t   t o p   l e v e ld hih r    jkj n    lml 5    �Gn�F
�G 
xmlen o    �E�E 0 node  
�F kfrmnamem n    opo 1    �D
�D 
pcntp 4    �Cq
�C 
xmlfq o   	 
�B�B 0 	thewaiver  k o      �A�A 0 xmldata  i r�@r r    sts n    uvu 1    �?
�? 
valLv n    wxw 5    �>y�=
�> 
xmley o    �<�< 0 element_name  
�= kfrmnamex o    �;�; 0 xmldata  t o      �:
�: 
ret �@  a m    zz�                                                                                  sevs  alis    �  Fusion                     �X��H+  ��System Events.app                                              ܢ����        ����  	                CoreServices    �X�      ���    ���2��2�  7Fusion:System: Library: CoreServices: System Events.app   $  S y s t e m   E v e n t s . a p p    F u s i o n  -System/Library/CoreServices/System Events.app   / ��  ^ R      �9{�8
�9 .ascrerr ****      � ****{ o      �7�7 
0 errmsg  �8  _ k   & =|| }~} Z   & 9��6�5 =  & -��� o   & +�4�4 0 dev_mode  � m   + ,�3
�3 boovtrue� I  0 5�2��1
�2 .sysodisAaleR        TEXT� o   0 1�0�0 
0 errmsg  �1  �6  �5  ~ ��/� r   : =��� m   : ;�� ���  � o      �.
�. 
ret �/  T ��� l     �-�,�+�-  �,  �+  � ��� l     �*�)�(�*  �)  �(  � ��� i   � ���� I      �'��&�' 0 
fileexists 
fileExists� ��%� o      �$�$ 0 thefile theFile�%  �&  � l    ���� O     ��� Z    ���#�� I   �"��!
�" .coredoexnull���     ****� 4    � �
�  
file� o    �� 0 thefile theFile�!  � L    �� m    �
� boovtrue�#  � L    �� m    �
� boovfals� m     ���                                                                                  sevs  alis    �  Fusion                     �X��H+  ��System Events.app                                              ܢ����        ����  	                CoreServices    �X�      ���    ���2��2�  7Fusion:System: Library: CoreServices: System Events.app   $  S y s t e m   E v e n t s . a p p    F u s i o n  -System/Library/CoreServices/System Events.app   / ��  �   (String) as Boolean   � ��� (   ( S t r i n g )   a s   B o o l e a n� ��� l     ����  �  �  � ��� i   � ���� I      ���� 0 folderexists folderExists� ��� o      �� 0 thefile theFile�  �  � l    ���� O     ��� Z    ����� I   ���
� .coredoexnull���     ****� 4    ��
� 
cfol� o    �� 0 thefile theFile�  � L    �� m    �
� boovtrue�  � L    �� m    �
� boovfals� m     ���                                                                                  sevs  alis    �  Fusion                     �X��H+  ��System Events.app                                              ܢ����        ����  	                CoreServices    �X�      ���    ���2��2�  7Fusion:System: Library: CoreServices: System Events.app   $  S y s t e m   E v e n t s . a p p    F u s i o n  -System/Library/CoreServices/System Events.app   / ��  �   (String) as Boolean   � ��� (   ( S t r i n g )   a s   B o o l e a n� ��� l     ����  �  �  � ��� i   � ���� I      ���
� 0 	changeext 	changeExt� ��� o      �	�	 0 filename  � ��� o      �� 0 new_ext  �  �
  � k      �� ��� r     ��� \     	��� l    ���� I    ���
� .corecnte****       ****� n     ��� 2   �
� 
cha � o     �� 0 filename  �  �  �  � m    � �  � o      ���� 0 mylength  � ��� r    ��� b    ��� l   ������ c    ��� l   ������ n    ��� 7  ����
�� 
cha � m    ���� � o    ���� 0 mylength  � o    ���� 0 filename  ��  ��  � m    ��
�� 
TEXT��  ��  � o    ���� 0 new_ext  � o      ���� 0 	newstring  � ���� L     �� o    ���� 0 	newstring  ��  � ��� l     ��������  ��  ��  � ��� i   � ���� I      ��������  0 getfreediskpct getFreeDiskPct��  ��  � k     �� ��� r     ��� I     ������� 0 trimline trimLine� ��� n    ��� o    ���� 0 capacity  � I    �������� 0 getdiskstats getDiskStats��  ��  � ��� m    	�� ���  %� ���� m   	 
���� ��  ��  � o      ���� 0 rawpct rawPct� ��� l   ������  � . ( returns % used, so invert it for % free   � ��� P   r e t u r n s   %   u s e d ,   s o   i n v e r t   i t   f o r   %   f r e e� ���� L    �� \    ��� m    ���� d� o    ���� 0 rawpct rawPct��  � ��� l     ��������  ��  ��  � ��� i   � ���� I      �������� (0 getfreediskspacegb getFreeDiskSpaceGB��  ��  � L     	   n      o    ���� 0 	available   I     �������� 0 getdiskstats getDiskStats��  ��  �  l     ��������  ��  ��    i   � � I      �������� 0 getdiskstats getDiskStats��  ��   k     �		 

 r      n     1    ��
�� 
txdl 1     ��
�� 
ascr o      ���� 0 old_delimts    Q    ��� k   	 �  l  	 	��������  ��  ��    r   	  I  	 ����
�� .sysoexecTEXT���     TEXT m   	 
 �  d f   - h l g��   o      ���� 0 memory_stats    r     !  n    "#" 4    ��$
�� 
cpar$ m    ���� # o    ���� 0 memory_stats  ! o      ���� 0 	rawstring   %&% l   ��'(��  ' ( " set freeDisk to item 1 of theList   ( �)) D   s e t   f r e e D i s k   t o   i t e m   1   o f   t h e L i s t& *+* l   �,-�  , , & set diskCapacity to item 1 of theList   - �.. L   s e t   d i s k C a p a c i t y   t o   i t e m   1   o f   t h e L i s t+ /0/ l   �12�  1 2 , set diskFreePercentage to item 2 of theList   2 �33 X   s e t   d i s k F r e e P e r c e n t a g e   t o   i t e m   2   o f   t h e L i s t0 454 r    676 m    88 �99   7 n     :;: 1    �
� 
txdl; 1    �
� 
ascr5 <=< r    #>?> n    !@A@ 2   !�
� 
citmA o    �� 0 	rawstring  ? o      �� 0 thelist theList= BCB r   $ )DED o   $ %�� 0 old_delimts  E n     FGF 1   & (�
� 
txdlG 1   % &�
� 
ascrC HIH l  * *����  �  �  I JKJ r   * .LML J   * ,��  M o      �� 0 newlist newListK NON X   / XP�QP k   ? SRR STS l  ? ?�UV�  U  display dialog i   V �WW   d i s p l a y   d i a l o g   iT XYX Z   ? QZ[��Z >  ? D\]\ l  ? B^��^ n   ? B_`_ m   @ B�
� 
ctxt` o   ? @�� 0 i  �  �  ] m   B Caa �bb  [ s   G Mcdc l  G Je��e n   G Jfgf m   H J�
� 
ctxtg o   G H�� 0 i  �  �  d l     h��h n      iji  ;   K Lj o   J K�� 0 newlist newList�  �  �  �  Y k�k l  R R����  �  �  �  � 0 i  Q o   2 3�� 0 thelist theListO lml l  Y Y����  �  �  m n�n r   Y �opo K   Y �qq �rs� 0 diskname  r l  Z `t��t n   Z `uvu m   ^ `�
� 
ctxtv n   Z ^wxw 4   [ ^�y
� 
cobjy m   \ ]�� x o   Z [�� 0 newlist newList�  �  s �z{� 0 
onegblocks 
OneGblocksz l  a g|��| n   a g}~} m   e g�
� 
ctxt~ n   a e� 4   b e��
� 
cobj� m   c d�� � o   a b�� 0 newlist newList�  �  { ���� 0 used  � l  h n���� n   h n��� m   l n�
� 
ctxt� n   h l��� 4   i l��
� 
cobj� m   j k�� � o   h i�� 0 newlist newList�  �  � ���� 0 	available  � l  o w���� n   o w��� m   u w�
� 
ctxt� n   o u��� 4   p u��
� 
cobj� m   q t�~�~ � o   o p�}�} 0 newlist newList�  �  � �|��{�| 0 capacity  � l  z ���z�y� n   z ���� m   � ��x
�x 
ctxt� n   z ���� 4   { ��w�
�w 
cobj� m   | �v�v � o   z {�u�u 0 newlist newList�z  �y  �{  p o      �t�t 0 	finallist 	finalList�   R      �s��r
�s .ascrerr ****      � ****� o      �q�q 
0 errmsg  �r  ��   ��� l  � ��p�o�n�p  �o  �n  � ��� r   � ���� o   � ��m�m 0 old_delimts  � n     ��� 1   � ��l
�l 
txdl� 1   � ��k
�k 
ascr� ��� l  � ��j�i�h�j  �i  �h  � ��� L   � ��� o   � ��g�g 0 	finallist 	finalList� ��� l  � ��f�e�d�f  �e  �d  � ��c� l  � ��b�a�`�b  �a  �`  �c   ��� l     �_�^�]�_  �^  �]  � ��� i   � ���� I      �\��[�\ 0 
is_running  � ��Z� o      �Y�Y 0 appname appName�Z  �[  � k     :�� ��� O     ��� r    ��� l   	��X�W� n    	��� 1    	�V
�V 
pnam� 2    �U
�U 
prcs�X  �W  � o      �T�T "0 listofprocesses listOfProcesses� m     ���                                                                                  sevs  alis    �  Fusion                     �X��H+  ��System Events.app                                              ܢ����        ����  	                CoreServices    �X�      ���    ���2��2�  7Fusion:System: Library: CoreServices: System Events.app   $  S y s t e m   E v e n t s . a p p    F u s i o n  -System/Library/CoreServices/System Events.app   / ��  � ��� l   �S�R�Q�S  �R  �Q  � ��� r    ��� m    �P
�P boovfals� o      �O�O 0 appisrunning appIsRunning� ��� X    5��N�� Z   ! 0���M�L� E   ! $��� o   ! "�K�K 0 thisitem thisItem� o   " #�J�J 0 appname appName� k   ' ,�� ��� r   ' *��� m   ' (�I
�I boovtrue� o      �H�H 0 appisrunning appIsRunning� ��G�  S   + ,�G  �M  �L  �N 0 thisitem thisItem� o    �F�F "0 listofprocesses listOfProcesses� ��� L   6 8�� o   6 7�E�E 0 appisrunning appIsRunning� ��D� l  9 9�C�B�A�C  �B  �A  �D  � ��� l     �@�?�>�@  �?  �>  � ��� i   � ���� I      �=��<�= 0 	readplist 	readPlist� ��� o      �;�; 0 theplistpath thePListPath� ��� o      �:�: 0 plistvar plistVar� ��9� o      �8�8 0 defaultvalue defaultValue�9  �<  � Q     (���� l   ��7�6� I   �5��4
�5 .sysoexecTEXT���     TEXT� b    
��� b    ��� b    ��� m    �� ���  d e f a u l t s   r e a d  � o    �3�3 0 theplistpath thePListPath� m    �� ���   � o    	�2�2 0 plistvar plistVar�4  �7  �6  � R      �1��0
�1 .ascrerr ****      � ****� o      �/�/ 
0 errmsg  �0  � k    (�� ��� Z    %���.�-� =   ��� o    �,�, 0 defaultvalue defaultValue� m    �+
�+ 
null� I   !�*��)
�* .sysodisAaleR        TEXT� o    �(�( 
0 errmsg  �)  �.  �-  � ��'� L   & (�� o   & '�&�& 0 defaultvalue defaultValue�'  � ��� l     �%�$�#�%  �$  �#  � ��� l     �"�!� �"  �!  �   � � � l     ����  �  �     l     ����  �  �    i   � � I      ��� 0 
writeplist 
writePlist 	 o      �� 0 theplistpath thePListPath	 

 o      �� 0 plistvar plistVar � o      �� 0 thevalue theValue�  �   Q      I   ��
� .sysoexecTEXT���     TEXT b     b     b    
 b     b     m     �  d e f a u l t s   w r i t e   o    �� 0 theplistpath thePListPath m     �    o    	�� 0 plistvar plistVar m   
  �      o    �� 0 thevalue theValue�   R      �!�
� .ascrerr ****      � ****! o      �� 
0 errmsg  �   I   �"�

� .sysodisAaleR        TEXT" o    �	�	 
0 errmsg  �
   #$# l     ����  �  �  $ %&% l     ����  �  �  & '(' i   � �)*) I      ��� � 0 getepochtime getEpochTime�  �   * k     ++ ,-, r     	./. c     010 l    2����2 I    ��3��
�� .sysoexecTEXT���     TEXT3 m     44 �55 \ p e r l   - e   ' u s e   T i m e : : H i R e s   q w ( t i m e ) ;   p r i n t   t i m e '��  ��  ��  1 m    ��
�� 
TEXT/ o      ���� 
0 mytime  - 676 r   
 898 ]   
 :;: o   
 ���� 
0 mytime  ; m    ����  ��9 o      ���� 
0 mytime  7 <��< L    == I    ��>���� 0 number_to_string  > ?��? o    ���� 
0 mytime  ��  ��  ��  ( @A@ l     ��������  ��  ��  A BCB i   � �DED I      ��F���� 0 number_to_string  F G��G o      ���� 0 this_number  ��  ��  E k     �HH IJI r     KLK c     MNM o     ���� 0 this_number  N m    ��
�� 
TEXTL o      ���� 0 this_number  J O��O Z    �PQ��RP E    	STS o    ���� 0 this_number  T m    UU �VV  E +Q k    �WW XYX r    Z[Z l   \����\ I   ����]
�� .sysooffslong    ��� null��  ] ��^_
�� 
psof^ m    `` �aa  ._ ��b��
�� 
psinb o    ���� 0 this_number  ��  ��  ��  [ o      ���� 0 x  Y cdc r    #efe l   !g����g I   !����h
�� .sysooffslong    ��� null��  h ��ij
�� 
psofi m    kk �ll  +j ��m��
�� 
psinm o    ���� 0 this_number  ��  ��  ��  f o      ���� 0 y  d non r   $ /pqp l  $ -r����r I  $ -����s
�� .sysooffslong    ��� null��  s ��tu
�� 
psoft m   & 'vv �ww  Eu ��x��
�� 
psinx o   ( )���� 0 this_number  ��  ��  ��  q o      ���� 0 z  o yzy r   0 E{|{ c   0 C}~} c   0 A� n   0 ?��� 7  1 ?����
�� 
cha � l  5 ;������ \   5 ;��� o   6 7���� 0 y  � l  7 :������ n   7 :��� 1   8 :��
�� 
leng� o   7 8���� 0 this_number  ��  ��  ��  ��  � l 	 < >������ m   < >������  ��  � o   0 1�� 0 this_number  � m   ? @�
� 
TEXT~ m   A B�
� 
nmbr| l     ���� o      �� 0 decimal_adjust  �  �  z ��� Z   F c����� >  F I��� o   F G�� 0 x  � m   G H��  � r   L ]��� c   L [��� n   L Y��� 7  M Y���
� 
cha � m   Q S�� � l  T X���� \   T X��� o   U V�� 0 x  � m   V W�� �  �  � o   L M�� 0 this_number  � m   Y Z�
� 
TEXT� l     ���� o      �� 0 
first_part  �  �  �  � r   ` c��� m   ` a�� ���  � l     ���� o      �� 0 
first_part  �  �  � ��� r   d w��� c   d u��� n   d s��� 7  e s���
� 
cha � l  i m���� [   i m��� o   j k�� 0 x  � m   k l�� �  �  � l  n r���� \   n r��� o   o p�� 0 z  � m   p q�� �  �  � o   d e�� 0 this_number  � m   s t�
� 
TEXT� l     ���� o      �� 0 second_part  �  �  � ��� r   x {��� l  x y���� o   x y�� 0 
first_part  �  �  � l     ���� o      �� 0 converted_number  �  �  � ��� Y   | ������� Q   � ����� r   � ���� b   � ���� l 	 � ����� l  � ����� o   � ��� 0 converted_number  �  �  �  �  � n   � ���� 4   � ���
� 
cha � o   � ��� 0 i  � l  � ����� o   � ��� 0 second_part  �  �  � l     ���� o      �� 0 converted_number  �  �  � R      ���
� .ascrerr ****      � ****�  �  � r   � ���� b   � ���� l  � ����� o   � ��� 0 converted_number  �  �  � m   � ��� ���  0� l     ��~�}� o      �|�| 0 converted_number  �~  �}  � 0 i  � m    ��{�{ � l  � ���z�y� o   � ��x�x 0 decimal_adjust  �z  �y  �  � ��w� L   � ��� l  � ���v�u� o   � ��t�t 0 converted_number  �v  �u  �w  ��  R L   � ��� o   � ��s�s 0 this_number  ��  C ��� l     �r�q�p�r  �q  �p  � ��� i   � ���� I      �o��n�o 0 add_leading_zeros  � ��� o      �m�m 0 this_number  � ��l� o      �k�k 0 max_leading_zeros  �l  �n  � k     G�� ��� r     ��� c     ��� l    ��j�i� a     ��� m     �h�h 
� o    �g�g 0 max_leading_zeros  �j  �i  � m    �f
�f 
long� l     ��e�d� o      �c�c 0 threshold_number  �e  �d  � ��b� Z    G���a�� A   ��� o    	�`�` 0 this_number  � l  	 
��_�^� o   	 
�]�] 0 threshold_number  �_  �^  � k    @�� ��� r    ��� m    �� ���  � l     ��\�[� o      �Z�Z 0 leading_zeros  �\  �[  �    r     l   �Y�X n     1    �W
�W 
leng l   �V�U c    	 l   
�T�S
 _     o    �R�R 0 this_number   m    �Q�Q �T  �S  	 m    �P
�P 
TEXT�V  �U  �Y  �X   l     �O�N o      �M�M 0 digit_count  �O  �N    r    # \    ! l   �L�K [     o    �J�J 0 max_leading_zeros   m    �I�I �L  �K   o     �H�H 0 digit_count   l     �G�F o      �E�E 0 character_count  �G  �F    U   $ 7 r   + 2 c   + 0 l  + . �D�C  b   + .!"! l  + ,#�B�A# o   + ,�@�@ 0 leading_zeros  �B  �A  " m   , -$$ �%%  0�D  �C   m   . /�?
�? 
TEXT l     &�>�=& o      �<�< 0 leading_zeros  �>  �=   o   ' (�;�; 0 character_count   '�:' L   8 @(( c   8 ?)*) l  8 =+�9�8+ b   8 =,-, o   8 9�7�7 0 leading_zeros  - l  9 <.�6�5. c   9 </0/ o   9 :�4�4 0 this_number  0 m   : ;�3
�3 
ctxt�6  �5  �9  �8  * m   = >�2
�2 
TEXT�:  �a  � L   C G11 c   C F232 o   C D�1�1 0 this_number  3 m   D E�0
�0 
ctxt�b  � 454 l     �/�.�-�/  �.  �-  5 676 l     �,�+�*�,  �+  �*  7 898 i   � �:;: I      �)<�(�) 00 roundandtruncatenumber roundAndTruncateNumber< =>= o      �'�' 0 	thenumber 	theNumber> ?�&? o      �%�% .0 numberofdecimalplaces numberOfDecimalPlaces�&  �(  ; k     �@@ ABA Z     CD�$�#C =    EFE o     �"�" .0 numberofdecimalplaces numberOfDecimalPlacesF m    �!�!  D k    GG HIH r    JKJ [    	LML o    � �  0 	thenumber 	theNumberM m    NN ?�      K o      �� 0 	thenumber 	theNumberI O�O L    PP I    �Q�� 0 number_to_string  Q R�R _    STS o    �� 0 	thenumber 	theNumberT m    �� �  �  �  �$  �#  B UVU l   ����  �  �  V WXW r    YZY m    [[ �\\  5Z o      �� $0 theroundingvalue theRoundingValueX ]^] U    /_`_ r   % *aba b   % (cdc m   % &ee �ff  0d o   & '�� $0 theroundingvalue theRoundingValueb o      �� $0 theroundingvalue theRoundingValue` o   ! "�� .0 numberofdecimalplaces numberOfDecimalPlaces^ ghg r   0 7iji c   0 5klk l  0 3m��m b   0 3non m   0 1pp �qq  .o o   1 2�� $0 theroundingvalue theRoundingValue�  �  l m   3 4�
� 
nmbrj o      �� $0 theroundingvalue theRoundingValueh rsr l  8 8���
�  �  �
  s tut r   8 =vwv [   8 ;xyx o   8 9�	�	 0 	thenumber 	theNumbery o   9 :�� $0 theroundingvalue theRoundingValuew o      �� 0 	thenumber 	theNumberu z{z l  > >����  �  �  { |}| r   > A~~ m   > ?�� ���  1 o      �� 0 themodvalue theModValue} ��� U   B U��� r   K P��� b   K N��� m   K L�� ���  0� o   L M�� 0 themodvalue theModValue� o      �� 0 themodvalue theModValue� \   E H��� o   E F� �  .0 numberofdecimalplaces numberOfDecimalPlaces� m   F G���� � ��� r   V ]��� c   V [��� l  V Y������ b   V Y��� m   V W�� ���  .� o   W X���� 0 themodvalue theModValue��  ��  � m   Y Z��
�� 
nmbr� o      ���� 0 themodvalue theModValue� ��� l  ^ ^��������  ��  ��  � ��� r   ^ e��� _   ^ c��� l  ^ a������ `   ^ a��� o   ^ _���� 0 	thenumber 	theNumber� m   _ `���� ��  ��  � o   a b���� 0 themodvalue theModValue� o      ���� 0 thesecondpart theSecondPart� ��� Z   f �������� A  f m��� n   f k��� 1   i k��
�� 
leng� l  f i������ c   f i��� o   f g���� 0 thesecondpart theSecondPart� m   g h��
�� 
ctxt��  ��  � o   k l���� .0 numberofdecimalplaces numberOfDecimalPlaces� U   p ���� r   } ���� c   } ���� l  } ������� b   } ���� m   } ~�� ���  0� o   ~ ���� 0 thesecondpart theSecondPart��  ��  � m   � ���
�� 
TEXT� o      ���� 0 thesecondpart theSecondPart� \   s z��� o   s t���� .0 numberofdecimalplaces numberOfDecimalPlaces� l  t y������ l  t y������ n   t y��� 1   w y��
�� 
leng� l  t w������ c   t w��� o   t u���� 0 thesecondpart theSecondPart� m   u v��
�� 
ctxt��  ��  ��  ��  ��  ��  ��  ��  � ��� l  � ���������  ��  ��  � ��� r   � ���� _   � ���� o   � ����� 0 	thenumber 	theNumber� m   � ����� � o      ���� 0 thefirstpart theFirstPart� ��� r   � ���� I   � �������� 0 number_to_string  � ���� o   � ����� 0 thefirstpart theFirstPart��  ��  � o      ���� 0 thefirstpart theFirstPart� ��� r   � ���� l  � ������� b   � ���� b   � ���� o   � ����� 0 thefirstpart theFirstPart� m   � ��� ���  .� o   � ����� 0 thesecondpart theSecondPart��  ��  � o      ���� 0 	thenumber 	theNumber� ��� l  � ���������  ��  ��  � ���� L   � ��� o   � ����� 0 	thenumber 	theNumber��  9 ��� l     ��������  ��  ��  � ��� l     �������  ��  �  � ��� i   � ���� I      ���� 0 trimline trimLine� ��� o      �� 0 	this_text  � ��� o      �� 0 
trim_chars  � ��� o      �� 0 trim_indicator  �  �  � k     {�� ��� l     ����  � ' ! 0 = beginning, 1 = end, 2 = both   � ��� B   0   =   b e g i n n i n g ,   1   =   e n d ,   2   =   b o t h� ��� r     ��� l    ���� n     ��� 1    �
� 
leng� l    ���� o     �� 0 
trim_chars  �  �  �  �  � o      �� 0 x  � ��� l   ����  �   TRIM BEGINNING   � �      T R I M   B E G I N N I N G�  Z    >�� E    J    
 	 m    ��  	 
�
 m    �� �   l  
 �� o   
 �� 0 trim_indicator  �  �   V    : Q    5 r    + c    ) n    ' 7   '�
� 
cha  l   #�� [    # o     !�� 0 x   m   ! "�� �  �   m   $ &���� o    �� 0 	this_text   m   ' (�
� 
TEXT o      �� 0 	this_text   R      ���
� .ascrerr ****      � ****�  �   k   3 5  l  3 3� �   8 2 the text contains nothing but the trim characters     �!! d   t h e   t e x t   c o n t a i n s   n o t h i n g   b u t   t h e   t r i m   c h a r a c t e r s "�" L   3 5## m   3 4$$ �%%  �   C   &'& o    �� 0 	this_text  ' l   (��( o    �� 0 
trim_chars  �  �  �  �   )*) l  ? ?�+,�  +   TRIM ENDING   , �--    T R I M   E N D I N G* ./. Z   ? x01��0 E  ? E232 J   ? C44 565 m   ? @�� 6 7�7 m   @ A�� �  3 l  C D8��8 o   C D�� 0 trim_indicator  �  �  1 V   H t9:9 Q   P o;<=; r   S e>?> c   S c@A@ n   S aBCB 7  T a�DE
� 
cha D m   X Z�� E d   [ `FF l  \ _G��G [   \ _HIH o   \ ]�� 0 x  I m   ] ^�� �  �  C o   S T�� 0 	this_text  A m   a b�
� 
TEXT? o      �� 0 	this_text  < R      ���
� .ascrerr ****      � ****�  �  = k   m oJJ KLK l  m m�MN�  M 8 2 the text contains nothing but the trim characters   N �OO d   t h e   t e x t   c o n t a i n s   n o t h i n g   b u t   t h e   t r i m   c h a r a c t e r sL P�P L   m oQQ m   m nRR �SS  �  : D   L OTUT o   L M�~�~ 0 	this_text  U l  M NV�}�|V o   M N�{�{ 0 
trim_chars  �}  �|  �  �  / W�zW L   y {XX o   y z�y�y 0 	this_text  �z  � YZY l     �x�w�v�x  �w  �v  Z [\[ l     �u�t�s�u  �t  �s  \ ]^] i   � �_`_ I      �ra�q�r (0 getageofoldestfile getAgeOfOldestFilea bcb o      �p�p 0 filepath filePathc d�od o      �n�n 0 fileextension fileExtension�o  �q  ` k     ee fgf r     	hih l    j�m�lj I     �kk�j�k 0 getoldestfile getOldestFilek lml o    �i�i 0 filepath filePathm n�hn o    �g�g 0 fileextension fileExtension�h  �j  �m  �l  i o      �f�f 
0 myfile  g o�eo Z   
 pq�drp >  
 sts o   
 �c�c 
0 myfile  t m    uu �vv  q L    ww I    �bx�a�b 0 getageoffile getAgeOfFilex y�`y b    z{z o    �_�_ 0 filepath filePath{ o    �^�^ 
0 myfile  �`  �a  �d  r L    || m    �]�]  �e  ^ }~} l     �\�[�Z�\  �[  �Z  ~ � i   � ���� I      �Y��X�Y 0 getageoffile getAgeOfFile� ��W� o      �V�V 
0 myfile  �W  �X  � k     "�� ��� l     �U���U  � 8 2 returns age based on modification time in seconds   � ��� d   r e t u r n s   a g e   b a s e d   o n   m o d i f i c a t i o n   t i m e   i n   s e c o n d s� ��� l     �T���T  � � } set shscript2 to "echo $(($(date +%s) - $(stat -t %s -f %m -- /Users/Ben/documents/waivers_pending/" & getOldestFile & ")))"   � ��� �   s e t   s h s c r i p t 2   t o   " e c h o   $ ( ( $ ( d a t e   + % s )   -   $ ( s t a t   - t   % s   - f   % m   - -   / U s e r s / B e n / d o c u m e n t s / w a i v e r s _ p e n d i n g / "   &   g e t O l d e s t F i l e   &   " ) ) ) "� ��� r     ��� b     ��� b     ��� m     �� ��� X e c h o   $ ( ( $ ( d a t e   + % s )   -   $ ( s t a t   - t   % s   - f   % m   - -  � o    �S�S 
0 myfile  � m    �� ���  ) ) )� o      �R�R 0 shscript  � ��� Q    ���� r    ��� c    ��� l   ��Q�P� I   �O��N
�O .sysoexecTEXT���     TEXT� o    �M�M 0 shscript  �N  �Q  �P  � m    �L
�L 
long� o      �K�K  0 fileageseconds fileAgeSeconds� R      �J��I
�J .ascrerr ****      � ****� o      �H�H 
0 errmsg  �I  � r    ��� m    �G
�G 
null� o      �F�F  0 fileageseconds fileAgeSeconds� ��E� L     "�� o     !�D�D  0 fileageseconds fileAgeSeconds�E  � ��� l     �C�B�A�C  �B  �A  � ��@� i   � ���� I      �?��>�? 0 getoldestfile getOldestFile� ��� o      �=�= 0 filepath filePath� ��<� o      �;�; 0 fileextension fileExtension�<  �>  � k     &�� ��� l     �:���:  � c ] set shscript to "ls -tr /Users/Ben/documents/waivers_pending/ | grep '-e *.xml' | head -n 1"   � ��� �   s e t   s h s c r i p t   t o   " l s   - t r   / U s e r s / B e n / d o c u m e n t s / w a i v e r s _ p e n d i n g /   |   g r e p   ' - e   * . x m l '   |   h e a d   - n   1 "� ��� r     ��� b     ��� b     	��� b     ��� b     ��� m     �� ���  l s   - t r  � n    ��� 1    �9
�9 
strq� o    �8�8 0 filepath filePath� m    �� ���    |   g r e p   ' - e   *� o    �7�7 0 fileextension fileExtension� m   	 
�� ���  '   |   h e a d   - n   1� o      �6�6 0 shscript  � ��� Q    #���� r    ��� I   �5��4
�5 .sysoexecTEXT���     TEXT� o    �3�3 0 shscript  �4  � o      �2�2 0 
oldestfile 
oldestFile� R      �1��0
�1 .ascrerr ****      � ****� o      �/�/ 
0 errmsg  �0  � r     #��� m     !�� ���  � o      �.�. 0 
oldestfile 
oldestFile� ��-� L   $ &�� o   $ %�,�, 0 
oldestfile 
oldestFile�-  �@       5�+� ����*�)�(�'�& L Q�%�$�#�"�!�  p������������������������������ �+  � 3����������������������
�	��������� ��������������������������������������� 0 theplistpath thePListPath� 00 incomingfileextensions incomingFileExtensions� 80 rawtranscodefileextensions rawTranscodeFileExtensions� D0  bypassrawtranscodefileextensions  bypassRawTranscodeFileExtensions� 60 statsintervalcheckminutes statsIntervalCheckMinutes� $0 processdelaysecs processDelaySecs� 0 loopdelaysecs loopDelaySecs� 0 	automated  � 0 devendpoints devEndpoints� $0 computerusername computerUserName� $0 waivergrepstring waiverGrepString� >0 filesizecheck_minaeoutputsize filesizeCheck_minAEOutputSize� (0 filesizecheckdelay filesizeCheckDelay� (0 performfilecleanup performFileCleanup� 0 dev_mode  � 00 oldestfileagethreshold oldestFileAgeThreshold� 00 expectedmaxprocesstime expectedMaxProcessTime� "0 applicationname applicationName
� .aevtoappnull  �   � ****� $0 shownotification showNotification� "0 processonecycle processOneCycle�
 &0 checkpendingfiles checkPendingFiles�	 0 filecleanup fileCleanup� ,0 movefilestoingestion moveFilesToIngestion� :0 transcodeaftereffectsoutput transcodeAfterEffectsOutput� 0 handlewaivers handleWaivers� (0 checkrawfilestatus checkRawFileStatus� ,0 checkfilewritestatus checkFileWriteStatus� 0 gatherstats gatherStats� "0 sendstatustobpc sendStatusToBPC� 0 setvars setVars�  0 setpaths setPaths�� 0 read_waiver  �� 0 get_element  �� 0 
fileexists 
fileExists�� 0 folderexists folderExists�� 0 	changeext 	changeExt��  0 getfreediskpct getFreeDiskPct�� (0 getfreediskspacegb getFreeDiskSpaceGB�� 0 getdiskstats getDiskStats�� 0 
is_running  �� 0 	readplist 	readPlist�� 0 
writeplist 
writePlist�� 0 getepochtime getEpochTime�� 0 number_to_string  �� 0 add_leading_zeros  �� 00 roundandtruncatenumber roundAndTruncateNumber�� 0 trimline trimLine�� (0 getageofoldestfile getAgeOfOldestFile�� 0 getageoffile getAgeOfFile�� 0 getoldestfile getOldestFile� ����       "� ����    * -� ����    5 8�* �) �( 

�' boovfals
�& boovfals�% ��@�$ 

�# boovtrue
�" boovfals�! �  x� �� �������
�� .aevtoappnull  �   � ****��  ��     �� ��� � ����� ��������������������� � � ���
�� .misccurdldt    ��� null
�� .ascrcmnt****      � ****�� $0 shownotification showNotification�� 0 
fileexists 
fileExists
�� .sysodisAaleR        TEXT
�� .aevtquitnull��� ��� null�� 0 ishappy isHappy�� 0 setvars setVars�� 0 isready1 isReady1�� 0 setpaths setPaths�� 0 isready2 isReady2
�� 
bool�� 0 gatherstats gatherStats�� "0 processonecycle processOneCycle
�� .sysodelanull��� ��� nmbr�� �*j  �%j O*��l+ O*b   k+ f  �b   %j O*j 	Y hOeE�O*j+ E�O*j+ E�O�e 	 �e �& *jk+ O*ek+ Y a �%a %�%j O*j  a %j O hZ*fk+ Ob  j [OY��OP� ������	���� $0 shownotification showNotification�� ��
�� 
  ������  0 subtitlestring subtitleString�� 0 messagestring messageString��   ������  0 subtitlestring subtitleString�� 0 messagestring messageString	 ������������
�� 
null
�� 
appr
�� 
subt�� 
�� .sysonotfnull��� ��� TEXT
�� .sysodelanull��� ��� nmbr�� ,�� ��b  �� Y *��b  � Okj OP� ��<�������� "0 processonecycle processOneCycle�� ����   ����  0 isinitialcycle isInitialCycle��   ����  0 isinitialcycle isInitialCycle ��F�M���\��m�~���������������
�� .misccurdldt    ��� null
� .ascrcmnt****      � ****
� 
null� $0 shownotification showNotification� 0 handlewaivers handleWaivers
� .sysodelanull��� ��� nmbr� :0 transcodeaftereffectsoutput transcodeAfterEffectsOutput� ,0 movefilestoingestion moveFilesToIngestion� 0 filecleanup fileCleanup� 0 gatherstats gatherStats
� 
givu� 
� .sysodisAaleR        TEXT
� .aevtquitnull��� ��� null��*j  �%j O*��l+ O*j+ O*j  �%b  %j Ob  j O*j+ 	O*j  �%b  %j Ob  j O*j+ O*j  �%b  %j Ob  j O*j+ O*j  �%b  %j Ob  j O*kk+ O*j  a %j O� *a a l+ Y *a a b  %a %l+ Ob  f  &*j  a %j Oa a a l O*j Y hOP� ������ &0 checkpendingfiles checkPendingFiles� ��   ��� 0 oldlist oldList� 0 newlist newList�   �������� 0 oldlist oldList� 0 newlist newList� 0 
filesadded 
filesAdded� 0 filesremoved filesRemoved� 0 isprocessing isProcessing� 0 i  � 0 n   ���:������
� 
list
� 
leng
� 
bool
� .ascrcmnt****      � ****
� 
cobj
� .corecnte****       ****
� 
TEXT� �jv�&E�Ojv�&E�OfE�O��,��,	 	��,j�& ���  �j OfY hO *k��-j kh ��/E�O�� 	��6FY h[OY��O *k��-j kh ��/E�O�� 	��6FY h[OY��O�%�&j O�%j O��,j	 	��,j �& fY hO��,j	 	��,j�& eY hOPY �j OeE�O�OP� ������ 0 filecleanup fileCleanup�  �   ��� 0 shscript  � 
0 errmsg   ����
� .sysoexecTEXT���     TEXT� 
0 errmsg  �  � (b  e  �E�O 
�j W X  hOPY hOP� ����� ,0 movefilestoingestion moveFilesToIngestion�  �   
�������������������� 0 myfolder  �� 0 	pmyfolder  �� 0 shscript  �� 0 filelist  �� 
0 errmsg  �� 0 i  �� 
0 myfile  �� 0 newfile  �� 0 basefilename baseFilename�� $0 intermediatelist intermediateList ,��������%��)������~�}DI�|�{�z�y�x�w�v���u���t��s�r�q��p��o��� -?�� 0 processed_media  
�� 
psxp
�� .misccurdldt    ��� null
�� .ascrcmnt****      � ****
�� 
strq
�� .sysoexecTEXT���     TEXT
�� 
cpar
� 
list�~ 
0 errmsg  �}  
�| 
kocl
�{ 
cobj
�z .corecnte****       ****
�y 
TEXT�x  B@�w �v ,0 checkfilewritestatus checkFileWriteStatus�u 0 spock_ingestor  
�t .sysobeepnull��� ��� long
�s 
givu�r 
�q .sysodisAaleR        TEXT�p 0 	changeext 	changeExt�o 0 
media_pool  ���E�O��,E�O*j �%j O��,%�%E�O �j �-�&E�W !X  *j �%�%j O�j Ojv�&E�ON�[�a l kh ��%a &E�O�E�O*�a a m+ e a ��,%a %_ �,%�%E�O *j a %j O�j W :X  *j a %�%j O*j O*j O*j Oa �%a a l OO*�a &a  l+ !E�Oa "_ #�,%a $%�%a %%E�O �j �-�&E�W %X  *j a &%�%j Oa 'j Ojv�&E�O M�[�a l kh a (_ #�,%a )%�%a &E�O 
�j W X  *j a *%�%j OP[OY��OPY hOP[OY��O*j a +%j OP� �nI�m�l�k�n :0 transcodeaftereffectsoutput transcodeAfterEffectsOutput�m  �l   �j�i�h�g�f�e�d�j 0 myfolder  �i 0 	pmyfolder  �h 0 shscript  �g 0 filelist  �f 
0 errmsg  �e 0 i  �d 
0 myfile   !�c�b�an�`x�_|�^�]�\�[�Z��Y�X�W��V�U����T�S�R�Q�P�O.�c 0 	ae_output  
�b 
psxp
�a .misccurdldt    ��� null
�` .ascrcmnt****      � ****
�_ 
strq
�^ .sysoexecTEXT���     TEXT
�] 
cpar
�\ 
list�[ 
0 errmsg  �Z  
�Y 
kocl
�X 
cobj
�W .corecnte****       ****
�V 
TEXT�U ,0 checkfilewritestatus checkFileWriteStatus�T 0 	ame_watch  
�S .sysodlogaskr        TEXT
�R .sysobeepnull��� ��� long
�Q 
givu�P 
�O .sysodisAaleR        TEXT�k+�E�O��,E�O*j �%j O��,%�%E�O �j �-�&E�W X  *j �%j O�j Ojv�&E�O ʣ[��l kh *j a %j O��%a &E�O*�b  b  m+ e  �a ��,%a %�%a %_ �,%�%E�Ob  e  
�j Y hO *j a %j O�j W :X  *j a %�%j O*j O*j O*j Oa �%a a l OOPY hOP[OY�DO*j a  %j OP� �N<�M�L�K�N 0 handlewaivers handleWaivers�M  �L   �J�I�H�G�F�E�D�C�B�A�@�?�J 60 incomingwaiverfolderposix incomingWaiverFolderPosix�I 0 shscript  �H  0 waiverfilelist waiverFileList�G 
0 errmsg  �F 0 outteri outterI�E 0 
waiverfile 
waiverFile�D 0 mywaiver myWaiver�C 0 rawfileslist rawFilesList�B 0 movefileslist moveFilesList�A 0 myincrementer myIncrementer�@ 0 	myrawfile 	myRawFile�? 0 success    �>D�=�<]�;a�:�9�8�7�6�5��4�3�2�1�0�/�.���-�,7;�+�*`f�)
�> .misccurdldt    ��� null
�= .ascrcmnt****      � ****�< .0 incomingrawxmlwaivers incomingRawXmlWaivers
�; 
strq
�: .sysodlogaskr        TEXT
�9 .sysoexecTEXT���     TEXT
�8 
cpar
�7 
list�6 
0 errmsg  �5  
�4 
kocl
�3 
cobj
�2 .corecnte****       ****�1 0 read_waiver  �0 0 waiver_total_raw_files  
�/ 
null
�. 
bool�- 0 get_element  �, (0 checkrawfilestatus checkRawFileStatus�+ @0 pendingwaiversforaefolderposix pendingWaiversForAEFolderPosix
�* 
TEXT
�) .sysodisAaleR        TEXT�K>*j  �%j O�E�O��,%�%b  
%E�Ob  e  
�j Y hO �j �-�&E�W X  *j  �%j O�j Ojv�&E�O ע[��l kh ��%E�O*�k+ E�OjvE�OjvE�O�a ,a 	 �a ,ja & 5jE�O +�a ,Ekh�kE�O*�a a �%m+ E�O��6G[OY��Y ��6GO*�k+  Na ��,%a %_ %�a &%E�OfE�O �j OeE�W  X  *j  a %�%j Oa �%j Y hOP[OY�7OP� �(��'�&�%�( (0 checkrawfilestatus checkRawFileStatus�' �$�$   �#�# 0 rawfileslist rawFilesList�&   �"�!� ����������������" 0 rawfileslist rawFilesList�! "0 filereadystatus fileReadyStatus�   0 skiptranscoded skipTranscoded� 0 movefileslist moveFilesList� 0 i  � 0 
namelength  � 
0 myname  � 0 fileextension fileExtension� $0 mytranscodedfile myTranscodedFile� 0 	myrawfile 	myRawFile�  0 foundextension foundExtension� 0 shscript  � 0 success  � $0 filemove_success fileMove_success� 
0 errmsg  � 0 rawfilei rawFileI� 
0 myfile  � 0 pmyfile   '��������
��	����K�O�cg���� ������������������!(A
� .sysodlogaskr        TEXT
� 
kocl
� 
cobj
� .corecnte****       ****
� 
TEXT
� 
cha �
 �	 >0 transcodecompletedfolderposix transcodeCompletedFolderPosix� :0 incomingrawmediafolderposix incomingRawMediaFolderPosix��� ,0 checkfilewritestatus checkFileWriteStatus
� 
strq� :0 transcodependingfolderposix transcodePendingFolderPosix
� .sysoexecTEXT���     TEXT� 
0 errmsg  �  
�  .misccurdldt    ��� null
�� .ascrcmnt****      � ****
�� .sysodisAaleR        TEXT
�� 
leng
�� 
bool
�� 
psxp�� 0 
media_pool  �%*b  e  �%j Y hOfE�OfE�OjvE�O�[��l kh ��&�-j E�O�[�\[Zk\Z��2�&E�O �b  [��l kh ��%�%�%�&E�O��%�%�%�&E�O�E�O*��km+  eE�O��%�&�6GOY hO*�jkm+  �fE�Ob  � ��,%a %_ %��%%E�Y #b  � a ��,%a %�%��%%E�Y hOfE�O �j OeE�OOPW $X  *j a %�%j Oa �%j OfE�OPY hOP[OY�.OP[OY��O�e 	 �a ,�a , a & � ��[��l kh b  e  a �%�&j Y hOa �%�%�&E^ O] a  ,E^ Oa !] �,%a "%_ #%�%E�OfE�Ob  e  
�j Y hO �j OeE�W &X  *j a $%�%j Oa %�%j OfE�OOP[OY�cO�OPY b  e  a &j Y hOfOPO�OP� ��a�������� ,0 checkfilewritestatus checkFileWriteStatus�� ����   �������� 0 pmyfile  �� "0 expectedminsize expectedMinSize�� &0 checkdelayseconds checkDelaySeconds��   �������������� 0 pmyfile  �� "0 expectedminsize expectedMinSize�� &0 checkdelayseconds checkDelaySeconds��  0 thisfilestatus thisFileStatus�� 0 	filesize1  �� 0 	filesize2   ������������
�� 
null�� 0 
fileexists 
fileExists
�� 
psxf
�� .rdwrgeofcomp       ****
�� 
bool
�� .sysodelanull��� ��� nmbr�� lfE�OjE�OkE�O��  kE�Y hO*�k+  E*�/j E�O�k
 ���& fY hO�j O*�/j E�O��  eY fOPY fO�� ������� ���� 0 gatherstats gatherStats�� ��!�� !  ���� 0 intervalmins intervalMins��   �������������������������������������������������������� 0 intervalmins intervalMins�� 0 
initialrun 
initialRun�� 0 timenow timeNow�� 0 	nextcheck 	nextCheck�� 40 updatestalefiletimestamp updateStaleFileTimestamp�� 0 oldestfileage oldestFileAge�� 0 	dataitems 	dataItems�� 0 failedtests failedTests�� 0 	firstitem 	firstItem�� 0 ae_test  �� 0 ame_test  �� 0 nextitem nextItem�� 0 dropbox_test  �� 0 shscript  �� 0 mylist myList�� 
0 errmsg  �� 0 mycount myCount�� 0 oldcount oldCount�� 0 ae_waiver_delta  �� 0 isprocessing isProcessing�� 0 mydatavalue myDataValue�� 0 mythreshhold myThreshhold�� 0 ame_file_delta  �� 0 dropbox_waiver_delta  �� 0 memory_stats  �� 0 freediskpct freeDiskPct�� 0 
freediskgb 
freeDiskGB  �������������		 	(	7�	@�	U	^�	c	g	~	�	�	�	�	�	�	�	�	�	�	�	�

��
������
1��
G��
W
a���
�
�
�
�
�
�
�
�	�'>B�^l������������QYoq{�����������&�ow�����������������DL�\^hxz���������������&.8:DTV`pr|��� 0 getepochtime getEpochTime�� 80 laststatsgatheredtimestamp lastStatsGatheredTimestamp�� <��  ���� 0 number_to_string  � :0 laststalefilechecktimestamp lastStaleFileCheckTimestamp� 0 ishappy isHappy� 0 
is_running  � 0 
writeplist 
writePlist
� .sysodlogaskr        TEXT� @0 pendingwaiversforaefolderposix pendingWaiversForAEFolderPosix
� 
strq
� .sysoexecTEXT���     TEXT
� 
cpar
� 
list� 
0 errmsg  �  
� .misccurdldt    ��� null
� .ascrcmnt****      � ****
� 
leng
� 
null� 0 	readplist 	readPlist
� 
bool� 00 pendingaewaiverlistold pendingAEWaiverListOLD� &0 checkpendingfiles checkPendingFiles� (0 getageofoldestfile getAgeOfOldestFile
� .corecnte****       ****� 0 	ame_watch  � .0 pendingamefilelistold pendingAMEFileListOLD� .0 incomingrawxmlwaivers incomingRawXmlWaivers� :0 pendingdropboxwaiverlistold pendingDropboxWaiverListOLD�  0 getfreediskpct getFreeDiskPct
� 
long� (0 getfreediskspacegb getFreeDiskSpaceGB� � 

� .sysodisAaleR        TEXT
� .sysosigtsirr   ��� null
� 
siip� "0 sendstatustobpc sendStatusToBPC���fE�O�j (*j+  E�O*��� � k+ E�O�� hY hY *j+  E�O*j+  E�OeE�OeE�OfE�OjE�O�E�O�E�O�E�O*�k+ E�O*b   �m+ O�f  (fE�O��%E�Ob  e  
�j Y hOa E�Y a E�O��%E�O*a k+ E�O*b   a �m+ O�f  ,fE�O�a %E�Ob  e  a j Y hOa E�Y a E�O�a %�%E�O*a k+ E�O*b   a �m+ O�f  ,fE�O�a %E�Ob  e  a j Y hOa E�Y a E�O�a  %�%E�Oa !_ "a #,%a $%E�O �j %a &-a '&E�W X ( )*j *a +%�%j ,OjvE�O�a -,E^ O*b   a .a /m+ 0E^ O] ] E^ O*b   a 1] m+ O*b   a 2] m+ O*j+  E�O*�b  � k+ E�O��
 	�e a 3& L*_ 4�l+ 5E^ O�E` 4O] f  &fE�O�a 6%E�Ob  e  a 7j Y hY hOeE�OPY hO] E^ Oa 8] %a 9%E�O�a :%�%E�O] E^ Oa ;] %a <%E�O�a =%�%E�O*_ "a >l+ ?E^ O] � 
] E�Y hOa @] %a A%E�O�a B%�%E�OjE^ Oa C_ "a #,%a D%E�O -�j %a &-a '&j EE^ O*b   a F] m+ OPW X ( )*j *a G%�%j ,OjE^ O] ]  &fE�O�a H%E�Ob  e  a Ij Y hY hO] E^ Oa J] %a K%E�O�a L%�%E�Oa M_ Na #,%a O%E�O �j %a &-a '&E�W X ( )*j *a P%�%j ,OjvE�O�a -,E^ O*b   a Qa /m+ 0E^ O] ] E^ O*b   a R] m+ O*b   a S] m+ O*j+  E�O*�b  � k+ E�O��
 	�e a 3& J*_ T�l+ 5E^ O�E` TO] f  &fE�O�a U%E�Ob  e  a Vj Y hY hOeE�Y hO] E^ Oa W] %a X%E�O�a Y%�%E�O] E^ Oa Z] %a [%E�O�a \%�%E�O*_ Na ]l+ ?E^ O] � 
] E�Y hOa ^] %a _%E�O�a `%�%E�Oa a_ ba #,%a c%E�O �j %a &-a '&E�W X ( )*j *a d%�%j ,OjvE�O�a -,E^ O*b   a ea /m+ 0E^ O] ] E^ O*b   a f] m+ O*b   a g] m+ O*j+  E�O*�b  � k+ E�O��
 	�e a 3& J*_ h�l+ 5E^ O�E` hO] f  &fE�O�a i%E�Ob  e  a jj Y hY hOeE�Y hO] E^ Oa k] %a l%E�O�a m%�%E�O] E^ Oa n] %a o%E�O�a p%�%E�O*_ ba ql+ ?E^ O] � 
] E�Y hOa r] %a s%E�O�a t%�%E�O a uj %E^ W X ( )a v�%E^ O] E^ Oa w] %a x%E�O�a y%�%E�O*j+ za {&E^ O*j+ |a {&E^ O] a }
 ] a ~a 3& &fE�O�a %E�Ob  e  a �j �Y hY hO] E^ Oa �] %a �%E�O�a �%�%E�O] E^ Oa �] %a �%E�O�a �%�%E�O *b   a �a �m+ 0E^ W X ( )a ��%E^ Oa �] %a �%E�O�a �%�%E�O*j �a �,E^ Oa �] %a �%E�O�a �%�%E�O�E^ Oa �] %a �%E�O�a �%�%E�O�b   fE�O�a �%E�Y hO�e  a �E^ Y a �E^ Ob  e  a �j �Y hOa �] %a �%E�O�a �%�%E�O�E^ Oa �] %a �%E�O�a �%�%E�O*j+  E^ Oa �] %a �%E�O�a �%�%E�Ob  e  
�j Y hO*�k+ �O*j+  E�O�e  *j+  E�Y hOP� ����"#�� "0 sendstatustobpc sendStatusToBPC� �$� $  �� 0 	dataitems 	dataItems�  " �������� 0 	dataitems 	dataItems� 0 myip myIP� 0 
theurlbase 
theURLbase� 0 
thepayload 
thePayload� 0 shscript  � 0 	myoutcome 	myOutcome� 
0 errmsg  # ������������	����
� .sysosigtsirr   ��� null
� 
siip� 0 	readplist 	readPlist
� 
bool
� 
strq
� .sysodlogaskr        TEXT
� .sysoexecTEXT���     TEXT� 
0 errmsg  �  � �*j  �,E�O�E�O*b   �b  m+ Ec  Ob  �&Ec  Ob  e  
�%E�Y �%E�O�%�%�%�%E�O��,%�%�%E�Ob  e  
�j Y hO �j E�O�W 	X  �OP� �/��%&�� 0 setvars setVars�  �  % �� 
0 errmsg  & ����~T�}�|�{br�z����y���x��!�w�v
� 
list� 00 pendingaewaiverlistold pendingAEWaiverListOLD� .0 pendingamefilelistold pendingAMEFileListOLD�~ :0 pendingdropboxwaiverlistold pendingDropboxWaiverListOLD
�} 
null�| 0 	readplist 	readPlist�{ 0 computer_id  
�z 
bool�y 0 stationtype stationType
�x 
long�w 
0 errmsg  �v  �K@jv�&E�Ojv�&E�Ojv�&E�O*b   ��m+ E�O*b   �b  	m+ Ec  	O*b   �b  m+ �&Ec  O*b   �b  m+ �&Ec  O*b   �em+ �&Ec  O*b   ��m+ E�O*b   �b  m+ �&Ec  O*b   a b  m+ a &Ec  O*b   a b  m+ a &Ec  O*b   a b  
m+ �&Ec  
O*b   a b  m+ a &Ec  O*b   a b  m+ a &Ec  OeW 	X  �OP� �u6�t�s'(�r�u 0 setpaths setPaths�t  �s  ' �q�p�q 0 filetest fileTest�p 
0 errmsg  ( /GJ�o�nY�mg�lo�k{}�j�i�h�g��f�e���d����c����b�a
�`0>@�_Vdf�^|��]��o 0 	readplist 	readPlist�n "0 dropboxrootpath dropboxRootPath�m 0 stationtype stationType�l .0 incomingrawxmlwaivers incomingRawXmlWaivers�k :0 incomingrawmediafolderposix incomingRawMediaFolderPosix�j :0 transcodependingfolderposix transcodePendingFolderPosix�i 0 folderexists folderExists�h 
0 errmsg  �g  
�f 
strq
�e .sysoexecTEXT���     TEXT�d >0 transcodecompletedfolderposix transcodeCompletedFolderPosix�c @0 pendingwaiversforaefolderposix pendingWaiversForAEFolderPosix�b 0 
media_pool  �a  �` 0 processed_media  �_ 0 	ae_output  �^ 0 	ame_watch  �] 0 spock_ingestor  �r]R*b   ��m+ E�Ob  e  ��%�%E�Y ��%E�O��%E�O��%E�O�b  	%�%E�O *�k+ E�W 
X  eE�O�f  a �a ,%j Y hOa b  	%a %E` O *_ k+ E�W 
X  eE�O�f  a _ a ,%j Y hOa b  	%a %E` O *_ k+ E�W 
X  eE�O�f  a _ a ,%j Y hOa b  	%a %E` O *_ k+ E�W 
X  eE�O�f  a _ a ,%j Y hOa  b  	%a !%E` "O *_ "k+ E�W 
X  eE�O�f  a #_ "a ,%j Y hOa $b  	%a %%E` &O *_ &k+ E�W 
X  eE�O�f  a '_ &a ,%j Y hOa (b  	%a )%E` *O *_ *k+ E�W 
X  eE�O�f  a +_ *a ,%j Y hO�a ,%E` -O *_ -k+ E�W 
X  eE�O�f  a ._ -a ,%j Y hOeW 	X  �OP� �\��[�Z)*�Y�\ 0 read_waiver  �[ �X+�X +  �W�W 0 	thewaiver  �Z  ) 	�V�U�T�S�R�Q�P�O�N�V 0 	thewaiver  �U 0 	waiver_id  �T 0 waiver_created  �S 0 waiver_program  �R 0 waiver_activation  �Q 0 waiver_team  �P 0 waiver_event  �O 0 waiver_total_raw_files  �N 0 waiver_data  * ���M���� #.1�L�K�J�I�H�G�F�E�M 0 get_element  �L 0 	waiver_id  �K 0 waiver_created  �J 0 waiver_program  �I 0 waiver_activation  �H 0 waiver_team  �G 0 waiver_event  �F 0 waiver_total_raw_files  �E �Y s*���m+ E�O*���m+ E�O*���m+ E�O*���m+ E�O*���m+ E�O*���m+ E�O*���m+ E�O�a �a �a �a �a �a �a E�O�OP� �DV�C�B,-�A�D 0 get_element  �C �@.�@ .  �?�>�=�? 0 	thewaiver  �> 0 node  �= 0 element_name  �B  , �<�;�:�9�8�< 0 	thewaiver  �; 0 node  �: 0 element_name  �9 0 xmldata  �8 
0 errmsg  - z�7�6�5�4�3�2�1�0�/�
�7 
xmlf
�6 
pcnt
�5 
xmle
�4 kfrmname
�3 
valL
�2 
ret �1 
0 errmsg  �0  
�/ .sysodisAaleR        TEXT�A >  � *�/�,��0E�O���0�,E�UW X  b  e  
�j 	Y hO�E�� �.��-�,/0�+�. 0 
fileexists 
fileExists�- �*1�* 1  �)�) 0 thefile theFile�,  / �(�( 0 thefile theFile0 ��'�&
�' 
file
�& .coredoexnull���     ****�+ � *�/j  eY fU� �%��$�#23�"�% 0 folderexists folderExists�$ �!4�! 4  � �  0 thefile theFile�#  2 �� 0 thefile theFile3 ���
� 
cfol
� .coredoexnull���     ****�" � *�/j  eY fU� ����56�� 0 	changeext 	changeExt� �7� 7  ��� 0 filename  � 0 new_ext  �  5 ����� 0 filename  � 0 new_ext  � 0 mylength  � 0 	newstring  6 ����
� 
cha 
� .corecnte****       ****� 
� 
TEXT� !��-j �E�O�[�\[Zk\Z�2�&�%E�O�� ����89�
�  0 getfreediskpct getFreeDiskPct�  �  8 �	�	 0 rawpct rawPct9 ������ 0 getdiskstats getDiskStats� 0 capacity  � 0 trimline trimLine� d�
 **j+  �,�km+ E�O�� ����:;�� (0 getfreediskspacegb getFreeDiskSpaceGB�  �  :  ; � ���  0 getdiskstats getDiskStats�� 0 	available  � 
*j+  �,E� ������<=���� 0 getdiskstats getDiskStats��  ��  < ������������������ 0 old_delimts  �� 0 memory_stats  �� 0 	rawstring  �� 0 thelist theList�� 0 newlist newList�� 0 i  �� 0 	finallist 	finalList�� 
0 errmsg  = ��������8����������a��������������������
�� 
ascr
�� 
txdl
�� .sysoexecTEXT���     TEXT
�� 
cpar
�� 
citm
�� 
kocl
�� 
cobj
�� .corecnte****       ****
�� 
ctxt�� 0 diskname  �� 0 
onegblocks 
OneGblocks�� 0 used  �� 0 	available  �� �� 0 capacity  �� �� 
�� 
0 errmsg  ��  �� ���,E�O ��j E�O��l/E�O���,FO��-E�O���,FOjvE�O (�[��l 	kh ��-� ��-�6GY hOP[OY��O��k/�-���l/�-��m/�-��a /�-a ��a /�-a E�W X  hO���,FO�OP� �������>?���� 0 
is_running  �� ��@�� @  ���� 0 appname appName��  > ���������� 0 appname appName�� "0 listofprocesses listOfProcesses�� 0 appisrunning appIsRunning�� 0 thisitem thisItem? �����������
�� 
prcs
�� 
pnam
�� 
kocl
�� 
cobj
�� .corecnte****       ****�� ;� 	*�-�,E�UOfE�O #�[��l kh �� 
eE�OY h[OY��O�OP� �������AB���� 0 	readplist 	readPlist�� ��C�� C  �������� 0 theplistpath thePListPath�� 0 plistvar plistVar�� 0 defaultvalue defaultValue��  A ���������� 0 theplistpath thePListPath�� 0 plistvar plistVar�� 0 defaultvalue defaultValue�� 
0 errmsg  B ������������
�� .sysoexecTEXT���     TEXT�� 
0 errmsg  ��  
�� 
null
�� .sysodisAaleR        TEXT�� ) �%�%�%j W X  ��  
�j Y hO�� ���DE�� 0 
writeplist 
writePlist� �F� F  ���� 0 theplistpath thePListPath� 0 plistvar plistVar� 0 thevalue theValue�  D ����� 0 theplistpath thePListPath� 0 plistvar plistVar� 0 thevalue theValue� 
0 errmsg  E ����
� .sysoexecTEXT���     TEXT� 
0 errmsg  �  
� .sysodisAaleR        TEXT�   �%�%�%�%�%j W X  �j � �*��GH�� 0 getepochtime getEpochTime�  �  G �� 
0 mytime  H 4����
� .sysoexecTEXT���     TEXT
� 
TEXT�  ��� 0 number_to_string  � �j �&E�O�� E�O*�k+ � �E��IJ�� 0 number_to_string  � �K� K  �� 0 this_number  �  I 	���������� 0 this_number  � 0 x  � 0 y  � 0 z  � 0 decimal_adjust  � 0 
first_part  � 0 second_part  � 0 converted_number  � 0 i  J �U�`���kv�������
� 
TEXT
� 
psof
� 
psin� 
� .sysooffslong    ��� null
� 
cha 
� 
leng
� 
nmbr�  �  � ���&E�O�� �*���� E�O*���� E�O*���� E�O�[�\[Z���,\Zi2�&�&E�O�j �[�\[Zk\Z�k2�&E�Y �E�O�[�\[Z�k\Z�k2�&E�O�E�O &k�kh  ���/%E�W X  ��%E�[OY��O�Y �� ����LM�� 0 add_leading_zeros  � �N� N  ��� 0 this_number  � 0 max_leading_zeros  �  L ������� 0 this_number  � 0 max_leading_zeros  � 0 threshold_number  � 0 leading_zeros  � 0 digit_count  � 0 character_count  M ����~�}$�|� 

� 
long
�~ 
TEXT
�} 
leng
�| 
ctxt� H�$�&E�O�� 7�E�O�k"�&�,E�O�k�E�O �kh��%�&E�[OY��O���&%�&Y ��&� �{;�z�yOP�x�{ 00 roundandtruncatenumber roundAndTruncateNumber�z �wQ�w Q  �v�u�v 0 	thenumber 	theNumber�u .0 numberofdecimalplaces numberOfDecimalPlaces�y  O �t�s�r�q�p�o�t 0 	thenumber 	theNumber�s .0 numberofdecimalplaces numberOfDecimalPlaces�r $0 theroundingvalue theRoundingValue�q 0 themodvalue theModValue�p 0 thesecondpart theSecondPart�o 0 thefirstpart theFirstPartP N�n[ep�m����l�k��j��n 0 number_to_string  
�m 
nmbr
�l 
ctxt
�k 
leng
�j 
TEXT�x ��j  ��E�O*�k"k+ Y hO�E�O �kh�%E�[OY��O�%�&E�O��E�O�E�O �kkh�%E�[OY��O�%�&E�O�k#�"E�O��&�,�  ���&�,kh�%�&E�[OY��Y hO�k"E�O*�k+ E�O��%�%E�O�� �i��h�gRS�f�i 0 trimline trimLine�h �eT�e T  �d�c�b�d 0 	this_text  �c 0 
trim_chars  �b 0 trim_indicator  �g  R �a�`�_�^�a 0 	this_text  �` 0 
trim_chars  �_ 0 trim_indicator  �^ 0 x  S �]�\�[�Z�Y$R
�] 
leng
�\ 
cha 
�[ 
TEXT�Z  �Y  �f |��,E�Ojllv� 0 *h�� �[�\[Z�k\Zi2�&E�W 	X  �[OY��Y hOkllv� 1 +h�� �[�\[Zk\Z�k'2�&E�W 	X  �[OY��Y hO�  �X`�W�VUV�U�X (0 getageofoldestfile getAgeOfOldestFile�W �TW�T W  �S�R�S 0 filepath filePath�R 0 fileextension fileExtension�V  U �Q�P�O�Q 0 filepath filePath�P 0 fileextension fileExtension�O 
0 myfile  V �Nu�M�N 0 getoldestfile getOldestFile�M 0 getageoffile getAgeOfFile�U *��l+  E�O�� *��%k+ Y j �L��K�JXY�I�L 0 getageoffile getAgeOfFile�K �HZ�H Z  �G�G 
0 myfile  �J  X �F�E�D�C�F 
0 myfile  �E 0 shscript  �D  0 fileageseconds fileAgeSeconds�C 
0 errmsg  Y ���B�A�@�?�>
�B .sysoexecTEXT���     TEXT
�A 
long�@ 
0 errmsg  �?  
�> 
null�I #�%�%E�O �j �&E�W 
X  �E�O� �=��<�;[\�:�= 0 getoldestfile getOldestFile�< �9]�9 ]  �8�7�8 0 filepath filePath�7 0 fileextension fileExtension�;  [ �6�5�4�3�2�6 0 filepath filePath�5 0 fileextension fileExtension�4 0 shscript  �3 0 
oldestfile 
oldestFile�2 
0 errmsg  \ ��1���0�/�.�
�1 
strq
�0 .sysoexecTEXT���     TEXT�/ 
0 errmsg  �.  �: '��,%�%�%�%E�O �j E�W 
X  �E�O�ascr  ��ޭ