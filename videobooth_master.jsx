//TODO update this to use typescript?

//********************************************
//
// videobooth_master.jsx
//
// adapted from Ink Time Capsule video (2014) developed for Blue Pixel by Dan Ebberts
// debberts@comcast.net
// 
// updated by Ben Johnson and evolved over many years
// bjohnson@bpcreates.com
// Mar. 19, 2023
//***********************************************


/*
	Revision History

 	Ver. 	Description
	----	---------------------------------------------------------------

	v1          Initial version to support switching based on codes in waiver

*/

// DEVELOPER VARIABLES
var devMode = false;  //if true will use alternate computer name in paths, will use additinal logging, will NOT RUN IN LOOP
var devModeComputerUserName = "bluepixel";

// GLOBAL PROJECT VARIABLES
var loopDelay = 5000; //miliseconds to wait between runs when no waivers are found
var processDelay = 750; //miliseconds to pause at different points in process
var renderSettingsName = "Best Settings";
var outputModuleName = "High Quality";

var mainCompName = "PROJECT_MASTER"; //override this in activation specific function
var customerFootageCompName = "PROJECT_MASTER"; //override this in activation specific function
var customerFootageLayerName = "customer_footage"; //override this in activation specific function

var maxCustomerNameChars = 16; //might be legacy cruft, to be moved into activation specific function

var computerUserName = "bluepixel"; // set this to bluepixel in production!!!!!!!

var outputFolderPath = "";
var outputFileName = "";

//	if (templateFile == null){
//		alert("Template project file not open -- exiting.");
//		return;
//	}


// APPLICATION STARTS HERE
// APPLICATION STARTS HERE
// APPLICATION STARTS HERE

if (devMode){
    computerUserName = devModeComputerUserName; // set this to bluepixel in production!!!!!!!
    // app.bp_watchFolder = Folder.selectDialog("Please select XML watch folder");
    app.bp_watchFolder = Folder(("/Users/" + computerUserName + "/Documents/waivers_pending/"));
    outputFolderPath = ('/Users/' + computerUserName + '/Documents/ae_output');
    imageOutputFolderPath = ('/Users/' + computerUserName + '/Documents/ae_image_output');
 }else {
         app.bp_watchFolder = Folder(("/Users/" + computerUserName + "/Documents/waivers_pending/"));
         outputFolderPath = ('/Users/' + computerUserName + '/Documents/ae_output');
         imageOutputFolderPath = ('/Users/' + computerUserName + '/Documents/ae_image_output');
     }

//EXIT IF NO WATCH FOLDER IS FOUND, ELSE SLEEP FOR 200MS
if (app.bp_watchFolder == null){
	alert ("No watch folder selected -- exiting.");
}else{
	app.bp_oneBigLoop = oneBigLoop;
	app.scheduleTask("app.bp_oneBigLoop();",5000,false); // initial delay (.2 sec)  - IMPORTANT this will cause script to not run in ExtendScript IDE because it is referencing the AE app
}
    

//CRITICAL!!!!!!!!!!!!!!!!!!!!!!!!!!!!
//for scheduleTask to work properly, ALL of the fuctions to be called must be contained within the schedule task.
//for this purpose, we have oneBigLoop
function oneBigLoop(){
if(devMode){
    logMessage("Starting oneBigLoop");
    }
CheckForXML();

// SUPPORTING FUNCTIONS
// SUPPORTING FUNCTIONS
// SUPPORTING FUNCTIONS
// SUPPORTING FUNCTIONS

/**
 * 
 * @param {string} theCompName name of the composition
 * @returns {object} the compostiion object
 */
	function findComp(theCompName){
		for (var i = 1; i <= app.project.numItems; i++){
			if ((app.project.item(i).name == theCompName) && (app.project.item(i) instanceof CompItem)){
				return app.project.item(i);
			}
		}
		return null; // can't find comp--return bad news
	}

    /**
     * 
     * @returns {Date} the current timestamp
     */
	function timeStamp(){ 
		function dateString(d) {
    			function pad(n) { return n<10 ? '0'+n : n }
   			return      d.getFullYear()
			+ '-' + pad(d.getMonth()+1)
			+ '-' + pad(d.getDate())
			+ ' ' + pad(d.getHours())
			+ ':' + pad(d.getMinutes())
			+ ':' + pad(d.getSeconds());
		}
		var myDate = new Date();
		return dateString(myDate);
	}


   function addCommas ( s ){
    return s.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    }


function removeNonDigits ( s ) {
        return s.toString().replace(/\D/g,'');
    }

function replaceExtension ( original, targetText, replacement ) {
        return original.toString().replace(targetText, replacement);
    }

    function toTitleCase(str){
    return str.replace(/\w\S*/g, function(txt){return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();});
    }

	function strip(stringToTrim) { // get rid of leading and trailing white space
	return stringToTrim.replace(/^\s+|\s+$/g,"");
	}

    /**
     * Utility to write a message to the log file
     * @param {string} theMessage 
     */
	function logMessage(theMessage){
		// append messages to log file in Log folder -- create folder and/or file if they don't exist
		var myLogFolderPath = app.bp_watchFolder.path + "/" + app.bp_watchFolder.name + "/" + "log";
		var myLogFolder = new Folder(myLogFolderPath);
		if (! myLogFolder.exists){
			myLogFolder.create();
		}
		var myLogFileName = "log.txt";
		var myLogFilePath = myLogFolderPath + "/" + escape(myLogFileName);
		var myLogFile = new File(myLogFilePath);
		var logFileOK = myLogFile.open("e");
		myLogFile.seek(0,2);
		myLogFile.writeln(timeStamp() + "  " + theMessage);
		myLogFile.close();
	}

    /**
     * Replace text content of a text layer
     * @param {object} theLayer the text layer to modify
     * @param {string} theNewText the string to update the layer with
     * @param {boolean} resize resize the text to fit the layer width
     */
    function replaceText(theLayer,theNewText,resize){
			var maxWidth = theLayer.sourceRectAtTime(theLayer.inPoint,false).width; // get max width
			var mySourceText = theLayer.property("ADBE Text Properties").property("ADBE Text Document");
			var myTextDoc = mySourceText.value;
			myTextDoc.text = theNewText;
			mySourceText.setValue(myTextDoc);
			var newWidth = theLayer.sourceRectAtTime(theLayer.inPoint,false).width;
			if (newWidth > maxWidth && resize){
				var newFontSize = myTextDoc.fontSize*(maxWidth/newWidth);
				myTextDoc.fontSize = newFontSize;
				mySourceText.setValue(myTextDoc);
			}
		}
        
        /**  method to autoKey a layer 
         * selects a region and averages the color, color value is passed in to KeyLight keyer
         * @param myVideoLayer - a layer object
         * @param {Array} sampleCenter - [x,y] center point of the selected region
         * @param sampleHeight - height of the region
         * @param sampleWidth - width of the region
        */
    function autoKeyer(myVideoLayer,sampleCenter,sampleHeight,sampleWidth){
        // UPDATE KEY COLOR
        logMessage("start autoKeyer");
        if(!sampleCenter){
            sampleCenter = [1800,250];  // offset from upper left corner
        }
        if(!sampleHeight){
            sampleHeight = 40;
        }
        if(!sampleWidth){
            sampleWidth = 40;
        }
        logMessage("sampleRadius = " + sampleWidth + "," + sampleHeight); 
        logMessage("sampleCenter = " + sampleCenter.toString()); 
        try{
        // add temporary color control to layer
        var myColorControl = myVideoLayer.property("Effects").addProperty("ADBE Color Control");
        var myColorProp = myColorControl.property("ADBE Color Control-0001");
        myColorProp.expression = "sampleImage(" + sampleCenter.toSource() + ",[" +  sampleWidth /2 + "," + sampleHeight /2 + "],false);";
	
        // harvest the result
        var myColor = myColorProp.value;
	
        // remove the color control
        myColorControl.remove();
        logMessage("color set to " + myColor.toString());
	
        // apply the color to the Keylight efect
        myVideoLayer.property("Effects").property("Keylight (1.2)").property("Screen Colour").setValue(myColor);
        logMessage("autoKeyer successful");
        return true;

        } catch (e) {
            logMessage("AutoKeyer Failed: " + e);
            return false;
        }
    }
    
    /**
     * Output a PNG file of the frame at 2 sec
     * @param {*} mainComp comp object
     * @param {string} outputFileName 
     * @returns 
     */
    function outputPngImage(mainComp, outputFileName){
        // export PNG of a single frame
        // var a = comp.saveFrameToPng(compTimestamp, outputFileName);
        logMessage("Begin export PNG");

        var myOutputFilePath;
        var myFilePath;
        var mySplitName;
        var compTimestamp = 2;
       
        //set preview resolution to FULL
        //app.project.item(index).resolutionFactor
        //FULL is 1,1 HALF is 2,2 QUARTER is 4,4
        mainComp.resolutionFactor = [1,1];

        myOutputFilePath = escape(imageOutputFolderPath + "/" + outputFileName)
        logMessage("Output path: " + myOutputFilePath + "");
        
        // create file object
        var myTempFile = new File(myOutputFilePath);

        try{
            app.beginSuppressDialogs();
            //EXPORT THE FILE
            mainComp.saveFrameToPng(compTimestamp, myTempFile);
            app.endSuppressDialogs(false);
            } catch (e) {
                logMessage("I broke the PNG export" + e);
                return("Export PNG Failed");
            }

        //wait 1 second for the comp to catch up to the script
        $.sleep(1000)
        logMessage("PNG export success" );
        return ""; // success
    
    }

    /**
     * utility to render a comp, requires globally set output path
     * @param {object} mainComp the composition to render
     * @param {string} outputFileName output filename
     * @returns {string} status message, will return empty string for success
     */
    function renderVideo(mainComp, outputFileName){
    	// add to render queue and render
		var myRQItem;
		var myOM;
		var myOutputFilePath;
		var myFilePath;
		var mySplitName;

		myOutputFilePath = escape(outputFolderPath + "/" + outputFileName)
        logMessage("Output path: " + myOutputFilePath + "");

		// clean out render queue to avoid errors
		for (var i = app.project.renderQueue.numItems; i > 0; i--){
			app.project.renderQueue.item(i).remove();
		}

		myRQItem = app.project.renderQueue.items.add(mainComp);
		myRQItem.applyTemplate(renderSettingsName);
        
        //TODO set work area programmatically, or set it based on an empty later because it's easy to move the work area handles accidentially
		myRQItem.timeSpanStart = mainComp.workAreaStart;
		myRQItem.timeSpanDuration = mainComp.workAreaDuration;

		myOM = myRQItem.outputModule(1);

		if (myOM.file == null){
			try{
				myOM.file = File(myOutputFilePath);
			}catch (e){
                         logMessage("Illegal output file name specified at 209");
				return("Illegal output file name specified ('" + myOutputFilePath + "') in XML file: '" + unescape(myXmlFile.name) + "'");
			}
		}

		myOM.applyTemplate(outputModuleName);

		try{
			myOM.file = File(myOutputFilePath);
		}catch (e){
              logMessage("Illegal output file name specified at 219");
			return("Illegal output file name specified ('" + myOutputFilePath + "') in XML file: '" + unescape(myXmlFile.name) + "'");
		}
		
        try{
		app.beginSuppressDialogs();
		app.project.renderQueue.render();
		app.endSuppressDialogs(false);
         } catch (e) {
             logMessage("I broke it");
             return("Render Failed");
             }
        
        return ""; // success
    
    }



// START ACTIVATION SPECIFIC PROCESSES
// START ACTIVATION SPECIFIC PROCESSES
// START ACTIVATION SPECIFIC PROCESSES
// START ACTIVATION SPECIFIC PROCESSES


/**
 * Sony Kando Gran Turismo Poster 2023
 * @param {*} theXmlString 
 * @param {*} customerVideoPath 
 * @returns {string} status message, will be empty string on success
 */
function sony_kando_2023(theXmlString, customerVideoPath){
    logMessage("using sony_kando_2023 process");
    var thisCustomer = new XML (theXmlString);

    /**
     * name of the main composition
     */
    var mainCompName = "SONY_KANDO_MASTER"; 
    var customerFootageCompName = "SONY_KANDO_CUSTOMER"; 
    var customerFootageLayerName = "CUSTOMER-RAW-LAYER"; 
    var bottomLayerCompName = "SONY_KANDO_BOTTOM"; 
          
    var myVideoLayer;
    var myImportOptions = new ImportOptions();
    var myFootageFile = new File();
    var myFootage;

    //MAKE SURE THE COMPS EXIST BEFORE PROCEEDING
    // find the template comps

    mainComp = findComp(mainCompName);
    customerFootageComp = findComp(customerFootageCompName);
    bottomLayerComp = findComp(bottomLayerCompName);
      
    if (mainComp == null | customerFootageComp == null | bottomLayerComp == null ){
        alert("Can't find comp in: '" + mainCompName + "' -- exiting.");
        logMessage("Can't find comp in: '" + mainCompName + "' -- exiting.");
        return("Can't find comp in: '" + mainCompName + "' -- exiting.");
        //TODO - BAIL OUT OF THIS WAIVER AND MOVE ON TO ANOTHER ONE
    }
          
    // make sure customer layer is on
    try{
        myVideoLayer = customerFootageComp.layer(customerFootageLayerName);
        myVideoLayer.enabled = true;
    }catch (e){
        logMessage("Cannot find customer layer: " + myVideoLayer + "");
        return("Cannot find customer layer: " + myVideoLayer + "");
    }
          
    // Replace the customer footage
    // myVideoLayer = mainComp.layer(customerFootageLayerName);
    //logMessage("customer footage: '" + customerVideoPath+ "'");

    try{
        myFootageFile = File(escape(customerVideoPath));
    }catch (e){
        logMessage("377 error replacing customer footage: '" + e + "'");
        return("Illegal footage file path ('" + customerVideoPath + "') in XML file");
    }
    if (! myFootageFile.exists){
        logMessage("381 error replacing customer footage: '" + e + "'");
        return("Footage file not found ('" + customerVideoPath + "') in XML file");
    }
    try{
        myImportOptions.file = myFootageFile;
        myFootage = app.project.importFile(myImportOptions);
    }catch (e){
        logMessage("388 error replacing customer footage: '" + e + "'");
        return("Footage file import failed ('" + customerVideoPath + "') in XML file");
    }
  
    try{
        myVideoLayer = customerFootageComp.layer(customerFootageLayerName);
        myVideoLayer.replaceSource(myFootage,false);
        logMessage("Replaced Customer Footage");
    }catch (e){
        logMessage("397 error replacing customer footage: '" + e + "'");
        return("error replacing customer footage: '" + e + "'");
    }
    
   //key out background
    var sampleCenter = [100,400];  // x,y offset from upper left corner
    var sampleHeight = 60;
    var sampleWidth = 200;
    keyResult = autoKeyer(myVideoLayer,sampleCenter,sampleHeight,sampleWidth);

    //TODO Update Customer Name
    // <customerFullName><![CDATA[__UDI:display_name__]]></customerFullName>
    
    /*
    //remove name customization 2023-08-25 BJJ
    
    var maxLineLength = 20;
    var tagline  = "FROM CREATOR TO RACER";
    var textLine1string = "";
    var textLine2string = "";
    var textLine3string = "";
    var customerDisplayName  = "";
    var nameArray = [];
    var firstName = "";
    var lastName = "";
    tempXML = thisCustomer.customerFullName;
    logMessage("kando 440");
    if (tempXML != null && tempXML !=""){
        customerDisplayName = tempXML.toString();
        customerDisplayName = customerDisplayName.toUpperCase();
        logMessage("kando 444: " + customerDisplayName);
        if (customerDisplayName.length > maxLineLength){
        //test for split
        logMessage("kando 447");
        nameArray = customerDisplayName.split(" ");
        firstName = nameArray[0];
        lastName = nameArray[1];
            if(firstName.length > maxLineLength || lastName.length > maxLineLength){
                //too long for either line
                logMessage("kando 452");
                textLine1string = "";
                textLine2string = "";
                textLine3string = tagline;
            } else {
                logMessage("kando 457");
                textLine1string = firstName.toString();
                logMessage("split 0 = " + textLine1string);
                textLine2string = lastName.toString();
                logMessage("split 1 = " + textLine2string);
                textLine3string = tagline;
            }
        } else {
            //name is one line
            logMessage("kando 464");
            textLine1string = "";
            textLine2string = customerDisplayName;
            textLine3string = tagline;
        }
    } else {
        //no name string
        logMessage("kando 471");
        textLine1string = "";
        textLine2string = "";
        textLine3string = tagline;
    }

    
    logMessage("kando 476");
    logMessage("Set textLine1 to " + textLine1string);
    myVideoLayer = bottomLayerComp.layer("TEXT_LINE_1");
    replaceText(myVideoLayer,textLine1string,true); 
    myVideoLayer.enabled = true;

    logMessage("kando 485");
    logMessage("Set textLine2 to " + textLine2string);
    myVideoLayer = bottomLayerComp.layer("TEXT_LINE_2");
    replaceText(myVideoLayer,textLine2string,true); 
    myVideoLayer.enabled = true;

    logMessage("Set textLine3 to " + textLine3string);
    myVideoLayer = bottomLayerComp.layer("TEXT_LINE_3");
    replaceText(myVideoLayer,textLine3string,true); 
    myVideoLayer.enabled = true;

    */

    //pause a bit
    $.sleep(1000)

    logMessage("RENDER sony_kando_2023 process OUTPUT");
    // RENDER THE FILE
    var renderError = outputPngImage(mainComp, outputFileName);
    logMessage("still image output = " + renderError);
    return renderError; //success
  
}

function TRESemme_nyfw_2023(theXmlString, customerVideoPath){
    logMessage("using TRESemme_nyfw_2023 process");
    var thisCustomer = new XML (theXmlString);

    var mainCompName = "TRESSEME_MASTER"; 
    var customerFootageCompName = "TRESSEME_MASTER"; 
    var customerFootageLayerName1 = "CUSTOMER_VIDEO"; 
          
    var myVideoLayer;
    var myImportOptions = new ImportOptions();
    var myFootageFile = new File();
    var myFootage;

    
          
    //MAKE SURE THE COMPS EXIST BEFORE PROCEEDING
    // find the template comps
    logMessage("using TRESemme_nyfw_2023 process 289");

    mainComp = findComp(mainCompName);
    customerFootageComp = findComp(customerFootageCompName);
      
    if (mainComp == null | customerFootageComp == null){
        alert("Can't find comp: '" + mainCompName + "' -- exiting.");
        return("Can't find comp: '" + mainCompName + "' -- exiting.");
        //TODO - BAIL OUT OF THIS WAIVER AND MOVE ON TO ANOTHER ONE
    }
          
    // make sure customer layer is on
    try{
        myVideoLayer = customerFootageComp.layer(customerFootageLayerName1);
        myVideoLayer.enabled = true;
    }catch (e){
        return ("Cannot find customer layer: " + myVideoLayer + "");
    }
          
    // Replace the customer footage
    // myVideoLayer = mainComp.layer(customerFootageLayerName);
    //logMessage("customer footage: '" + customerVideoPath+ "'");
    logMessage("using NYFW process 542");

    try{
        myFootageFile = File(escape(customerVideoPath));
        logMessage("using NYFW process 317");
    }catch (e){
        logMessage("319 error replacing customer footage: '" + e + "'");
        return("Illegal footage file path ('" + customerVideoPath + "') in XML file");
    }
    if (! myFootageFile.exists){
        logMessage("323 error replacing customer footage: '" + e + "'");
        return("Footage file not found ('" + customerVideoPath + "') in XML file");
    }
    try{
        myImportOptions.file = myFootageFile;
        myFootage = app.project.importFile(myImportOptions);
        logMessage("using NYFW process 329");
    }catch (e){
        logMessage("331 error replacing customer footage: '" + e + "'");
        return("Footage file import failed ('" + customerVideoPath + "') in XML file");
    }
  
    try{
        myVideoLayer = customerFootageComp.layer(customerFootageLayerName1);
        myVideoLayer.replaceSource(myFootage,false);
        logMessage("Replaced Customer Footage");
    }catch (e){
        logMessage("340 error replacing customer footage: '" + e + "'");
        return("error replacing customer footage: '" + e + "'");
    }
    
    logMessage("RENDER TRESemme_nyfw_2023 process OUTPUT");
    // RENDER THE FILE
    var renderError = renderVideo(mainComp,outputFileName);
    return renderError; //success
}

/**
 * GEICO Nascar Poster 2023
 * @param {*} theXmlString 
 * @param {*} customerVideoPath 
 * @returns {string} status message, will be empty string on success
 */
function geico_nascar_poster(theXmlString, customerVideoPath){
    logMessage("using geico_nascar_poster process");
    var thisCustomer = new XML (theXmlString);

    /**
     * name of the main composition
     */
    var mainCompName = "GEICO_NASCAR_MASTER"; 
    var customerFootageCompName = "GEICO_NASCAR_CUSTOMER"; 
    var customerFootageLayerName = "CUSTOMER-RAW-LAYER"; 
    var bottomLayerCompName = "GEICO_NASCAR_BOTTOM"; 
    var bottomLayerGenericLayerName = "layer-background-race-GENERIC";
          
    var myVideoLayer;
    var myImportOptions = new ImportOptions();
    var myFootageFile = new File();
    var myFootage;

    var bottomLayerFileName  = bottomLayerGenericLayerName;
    tempXML = thisCustomer.eventEndDate;
    if (tempXML != null && tempXML !=""){
        bottomLayerFileName = tempXML.toString();
        } else {
            bottomLayerFileName = bottomLayerGenericLayerName;
        }
        bottomLayerFileName = "layer-background-" + bottomLayerFileName.replace("2023","race");
        logMessage("bottomLayerFileName = " + bottomLayerFileName);

    //MAKE SURE THE COMPS EXIST BEFORE PROCEEDING
    // find the template comps

    mainComp = findComp(mainCompName);
    customerFootageComp = findComp(customerFootageCompName);
    bottomLayerComp = findComp(bottomLayerCompName);
      
    if (mainComp == null | customerFootageComp == null | bottomLayerComp == null ){
        alert("Can't find comp in: '" + mainCompName + "' -- exiting.");
        logMessage("Can't find comp in: '" + mainCompName + "' -- exiting.");
        return("Can't find comp in: '" + mainCompName + "' -- exiting.");
        //TODO - BAIL OUT OF THIS WAIVER AND MOVE ON TO ANOTHER ONE
    }
          
    // make sure customer layer is on
    try{
        myVideoLayer = customerFootageComp.layer(customerFootageLayerName);
        myVideoLayer.enabled = true;
    }catch (e){
        logMessage("Cannot find customer layer: " + myVideoLayer + "");
        return("Cannot find customer layer: " + myVideoLayer + "");
    }
          
    // Replace the customer footage
    // myVideoLayer = mainComp.layer(customerFootageLayerName);
    //logMessage("customer footage: '" + customerVideoPath+ "'");

    try{
        myFootageFile = File(escape(customerVideoPath));
    }catch (e){
        logMessage("377 error replacing customer footage: '" + e + "'");
        return("Illegal footage file path ('" + customerVideoPath + "') in XML file");
    }
    if (! myFootageFile.exists){
        logMessage("381 error replacing customer footage: '" + e + "'");
        return("Footage file not found ('" + customerVideoPath + "') in XML file");
    }
    try{
        myImportOptions.file = myFootageFile;
        myFootage = app.project.importFile(myImportOptions);
    }catch (e){
        logMessage("388 error replacing customer footage: '" + e + "'");
        return("Footage file import failed ('" + customerVideoPath + "') in XML file");
    }
  
    try{
        myVideoLayer = customerFootageComp.layer(customerFootageLayerName);
        myVideoLayer.replaceSource(myFootage,false);
        logMessage("Replaced Customer Footage");
    }catch (e){
        logMessage("397 error replacing customer footage: '" + e + "'");
        return("error replacing customer footage: '" + e + "'");
    }
    
   //key out background
    var sampleCenter = [100,400];  // x,y offset from upper left corner
    var sampleHeight = 60;
    var sampleWidth = 200;
    keyResult = autoKeyer(myVideoLayer,sampleCenter,sampleHeight,sampleWidth);

    //update race name/location layer
    //each race has it's own PNG layer
    // loop all layers of comp and set correct layer
    logMessage("looking for layer " + bottomLayerFileName.toLowerCase());
    var foundLayer = false;

    for(var i = 1; i < bottomLayerComp.layers.length + 1; i++) {
        // logMessage("evaluating layer " + i + " : " + topLayerComp.layers[i].name);
        try{
            //turn it off first
            bottomLayerComp.layers[i].enabled = false;
            
            // check name, turn on if match
            if(bottomLayerComp.layers[i].name.toLowerCase() == bottomLayerFileName.toLowerCase()) {
                bottomLayerComp.layers[i].enabled = true;
                foundLayer = true;
                logMessage("found matching layer - using " + bottomLayerFileName);
            }
        }catch (e) {
            logMessage("error setting top layer: " + e);
        }
    }
    if(!foundLayer){
        //no matching layer found, use GENERIC
        bottomLayerComp.layer(bottomLayerGenericLayerName).enabled = true;
        logMessage("no matching layer - using " + bottomLayerGenericLayerName);
    }

    //pause a bit
    $.sleep(1000)

    logMessage("RENDER geico_nascar_poster process OUTPUT");
    // RENDER THE FILE
    var renderError = outputPngImage(mainComp, outputFileName);
    logMessage("still image output = " + renderError);
    return renderError; //success
  
}


function fsxp_2024(theXmlString, customerVideoPath){
    logMessage("using FSXP 2024 process");
    var thisCustomer = new XML (theXmlString);

    var mainCompName = "FSXP_2024_MASTER"; 
    var customerFootageCompName = "FSXP_2024_CUSTOMER_CONTENT"; 
    var customerFootageLayerName1 = "CUSTOMER_VIDEO"; 
    var variableTextCompName = "FSXP_2024_TAIL_TEXT"; 
    var variableLogoCompName = "FSXP_2024_TAIL_LOGOS"; 
          
    var myVideoLayer;
    var myImportOptions = new ImportOptions();
    var myFootageFile = new File();
    var myFootage;

    var raceLocationText  = "default";
    tempXML = thisCustomer.custom_event_text_1;
    if (tempXML != null && tempXML !=""){
        raceLocationText = tempXML.toString();
        } else {
            raceLocationText = "Firestone Experience Tour";
        }

    var raceDateText  = "2024";
    tempXML = thisCustomer.custom_event_text_2;
    if (tempXML != null && tempXML !="" && tempXML !="2023" && tempXML.length > 4){
        raceDateText = tempXML.toString();
        } else {
            raceDateText = "2024";
        }

    var logoCode  = "default";
    tempXML = thisCustomer.custom_event_text_3;
    if (tempXML != null && tempXML !=""){
        logoCode = tempXML.toString();
        } else {
            logoCode = "default";
        }

    // get event
    var event_code  = "";
    tempXML = thisCustomer.event;
    if (tempXML != null && tempXML !=""){
        event_code = tempXML.toString();
        } else {
            event_code = null;
            }

    if (event_code == "BLM5I" || event_code == "PLMZ1") {
        raceLocationText = "Firestone Grand Prix of St. Petersburg";
        raceDateText = "March 3 - 5, 2023";
        logoCode = "stp";
        }
    else if (event_code == "4JLMW9") {
        raceLocationText = "Firestone Grand Prix of Monterey";
        raceDateText = "June 21 - 23, 2024";
        logoCode = "monterey";
        }
    else if (event_code == "4QLMWE") {
        raceLocationText = "Honda INDY 200 at Mid-Ohio";
        raceDateText = "July 5 - 7, 2024";
        logoCode = "default";
        }
    else if (event_code == "42LMWW") {
        raceLocationText = "HyVee INDYCAR Race Weekend";
        raceDateText = "July 12 - 14, 2024";
        logoCode = "default";
        }
    else if (event_code == "4BLTWX") {
        raceLocationText = "Milwaukee Mile 250";
        raceDateText = "Aug. 30 - Sep. 1, 2024";
        logoCode = "default";
        }
    else if (event_code == "4FLMWU") {
        raceLocationText = "Big Machine Music City Grand Prix";
        raceDateText = "September 13 - 15, 2024";
        logoCode = "default";
        }
          
    //MAKE SURE THE COMPS EXIST BEFORE PROCEEDING
    // find the template comps
    logMessage("using FSXP process 960");

    mainComp = findComp(mainCompName);
    customerFootageComp = findComp(customerFootageCompName);
    logoComp = findComp(variableLogoCompName);
    textComp = findComp(variableTextCompName);
      
    if (mainComp == null | customerFootageComp == null | logoComp == null | textComp == null ){
        alert("Can't find comp: '" + mainCompName + "' -- exiting.");
        return("Can't find comp: '" + mainCompName + "' -- exiting.");
        //TODO - BAIL OUT OF THIS WAIVER AND MOVE ON TO ANOTHER ONE
    }
          
    // make sure customer layer is on
    try{
        myVideoLayer = customerFootageComp.layer(customerFootageLayerName1);
        myVideoLayer.enabled = true;
    }catch (e){
        return ("Cannot find customer layer: " + myVideoLayer + "");
    }
          
    // Replace the customer footage
    // myVideoLayer = mainComp.layer(customerFootageLayerName);
    //logMessage("customer footage: '" + customerVideoPath+ "'");
    logMessage("using FSXP process 984");

    try{
        myFootageFile = File(escape(customerVideoPath));
        logMessage("using FSXP process 987");
    }catch (e){
        logMessage("990 error replacing customer footage: '" + e + "'");
        return("Illegal footage file path ('" + customerVideoPath + "') in XML file");
    }
    if (! myFootageFile.exists){
        logMessage("994 error replacing customer footage:'" + "'");
        return("Footage file not found ('" + customerVideoPath + "') in XML file");
    }
    try{
        myImportOptions.file = myFootageFile;
        myFootage = app.project.importFile(myImportOptions);
        logMessage("using FSXP process 1000");
    }catch (e){
        logMessage("1002 error replacing customer footage: '" + e + "'");
        return("Footage file import failed ('" + customerVideoPath + "') in XML file");
    }
  
    try{
        myVideoLayer = customerFootageComp.layer(customerFootageLayerName1);
        myVideoLayer.replaceSource(myFootage,false);
        logMessage("Replaced Customer Footage");
    }catch (e){
        logMessage("1011 error replacing customer footage: '" + e + "'");
        return("error replacing customer footage: '" + e + "'");
    }
    
    logMessage("using FSXP process 342");
    //set correct logo

    logMessage("logoCode = " + logoCode.toLowerCase());
    try{
        myVideoLayer = logoComp.layer("LOGO_GENERIC");
        if(logoCode.toLowerCase() == "default"){
        myVideoLayer.enabled = true;
        }else {
        myVideoLayer.enabled = false; 
        }
    }catch (e){
        logMessage("1027 cannot swap logo: '" + e + "'");
        return ("Cannot find customer layer: " + myVideoLayer + "");
    }
    try{
        myVideoLayer = logoComp.layer("LOGO_ST-PETE");
        if(logoCode.toLowerCase() == "stp"){
            myVideoLayer.enabled = true;
            }else {
            myVideoLayer.enabled = false; 
            }
    }catch (e){
        logMessage("1038 cannot swap logo: '" + e + "'");
        return ("Cannot find customer layer: " + myVideoLayer + "");
    }
    try{
        myVideoLayer = logoComp.layer("LOGO_MONTEREY");
        if(logoCode.toLowerCase() == "monterey"){
            myVideoLayer.enabled = true;
            }else {
            myVideoLayer.enabled = false; 
            }
    }catch (e){
        logMessage("1049 cannot swap logo: '" + e + "'");
        return ("Cannot find customer layer: " + myVideoLayer + "");
    }
    try{
        myVideoLayer = logoComp.layer("LOGO_INDY500");
        if(logoCode.toLowerCase() == "indy500"){
            myVideoLayer.enabled = true;
            }else {
            myVideoLayer.enabled = false; 
            }
    }catch (e){
        logMessage("1060 cannot swap logo: '" + e + "'");
        return ("Cannot find customer layer: " + myVideoLayer + "");
    }

    //update race name/location layer
    logMessage("using FSXP process 383");

    logMessage("Set location text layer to " + raceLocationText);
    myVideoLayer = textComp.layer("RACE_NAME");
    replaceText(myVideoLayer,raceLocationText.toUpperCase(),true); 
    myVideoLayer.enabled = true;

    //update race date layer
    logMessage("Set date text layer to " + raceDateText);
    myVideoLayer = textComp.layer("RACE_DATES");
    replaceText(myVideoLayer,raceDateText.toUpperCase(),true); 
    myVideoLayer.enabled = true;

    logMessage("RENDER FSXP 2024 process OUTPUT");
    // RENDER THE FILE
    var renderError = renderVideo(mainComp,outputFileName);
    return renderError; //success
  
}


// END ACTIVATION SPECIFIC PROCESSES
// END ACTIVATION SPECIFIC PROCESSES
// END ACTIVATION SPECIFIC PROCESSES
// END ACTIVATION SPECIFIC PROCESSES
// END ACTIVATION SPECIFIC PROCESSES
// END ACTIVATION SPECIFIC PROCESSES


// START processFile()
// START processFile()
// START processFile()

	function processFile(theXmlString){
        
      // make sure template file is open
      //TODO this would move into activation specific script in future version where we open different projects
       logMessage("XML: " + theXmlString);

        var myJob = new XML (theXmlString);
        var tempXML;
    
    //get PATE - program, activation, team, event
    // get program
    var program_code = "";
    tempXML = myJob.program;
    if (tempXML != null && tempXML !=""){
        program_code = tempXML.toString();
        } else {
            program_code = null;
            }
    // get activation
    var activation_code  = "";
    tempXML = myJob.activation;
    if (tempXML != null && tempXML !=""){
        activation_code = tempXML.toString();
        } else {
            activation_code = null;
            }
    // get team
    var team_code  = "";
    tempXML = myJob.team;
    if (tempXML != null && tempXML !=""){
        team_code = tempXML.toString();
        } else {
            team_code = null;
            }
    // get event
    var event_code  = "";
    tempXML = myJob.event;
    if (tempXML != null && tempXML !=""){
        event_code = tempXML.toString();
        } else {
            event_code = null;
            }
    
    // get output file name
	tempXML = myJob.OutputFileName;
	if (tempXML != null){
		outputFileName = tempXML.toString();
		}
    
    // get customer video path
	tempXML = myJob.customer_footage;
    if (tempXML != null && tempXML !=""){
	customerVideoPath = ('/Users/' + computerUserName + '/Documents/media_pool/' + (tempXML.toString()));
     } else {
         customerVideoPath = ('/Users/' + computerUserName + '/Documents/media_pool/' + outputFileName + ".mp4");
         }
     
     //change path to .mov to handle transcoded files from AME to get around VFR issue in After Effects
     customerVideoPathMOV = customerVideoPath.substr(0, customerVideoPath.lastIndexOf(".")) + ".mov";
    //use when project takes JPG file as source material
     customerVideoPathJPG = customerVideoPath.substr(0, customerVideoPath.lastIndexOf(".")) + ".jpg";

//PROCESS FILE BASED ON ACTIVATION SPECIFIC FUNCTION

//Delay a bit to make sure raw file is moved into place
 $.sleep(250)

if (team_code == "geico_events_1Q17_nascar") {
    var processFileError =  geico_nascar_poster(theXmlString, customerVideoPath);
    }
else if (activation_code == "geico_events_1Q17") {
    var processFileError =  geico_nascar_poster(theXmlString, customerVideoPath);
    }
else if (activation_code == "fsxp_indycar_2024" ) {
    var processFileError =  fsxp_2024(theXmlString, customerVideoPath);
    }
else if (activation_code == "sony_kando_2023" ) {
    var processFileError =  sony_kando_2023(theXmlString, customerVideoPath);
    }
else if (activation_code == "TRESemme_nyfw_2023" ) {
    var processFileError =  TRESemme_nyfw_2023(theXmlString, customerVideoPath);
    }
    
else {
    return ("error - no process found for activation " + activation_code + "");
    }
    
	return processFileError; // success
	
    //end of processFile
    }

    // end processFile() function block
    
// START CheckForXML()
// START CheckForXML()
// START CheckForXML()

function CheckForXML(){
    
    if(devMode) logMessage("start CheckForXML");
    var templateFile = app.project.file; //TODO in future version we need to open/close specific projects based on activation

 // make sure output folder is not null
		if (outputFolderPath == ""){
			return ("No output folder defined: '" +  "'");
		}

  // make sure output folder is not null
		var myOutputFolder = new Folder(escape(outputFolderPath));
		if (! myOutputFolder.exists){
            logMessage("Output folder does not exist: '" + outputFolderPath);
		    return ("Output folder does not exist: '" + outputFolderPath);
		}

    // make sure output folder is not null
    if (imageOutputFolderPath == ""){
        logMessage ("No output folder defined: '" +  "'");
        return ("No output folder defined: '" +  "'");
    }
    // make sure output folder is not null
    var myImageOutputFolder = new Folder(escape(imageOutputFolderPath));
    if (! myImageOutputFolder.exists){
        logMessage ("Output folder does not exist: '" + imageOutputFolderPath);
         return ("Output folder does not exist: '" + imageOutputFolderPath);
    }

    var myFiles = app.bp_watchFolder.getFiles();
    var myXmlFile = new File();

	if (myFiles.length > 0){
		var mySplit;
		myXmlFile = null;
		for (var myIdx = 0; myIdx < myFiles.length; myIdx++){
			if (myFiles[myIdx] instanceof File){
				mySplit = unescape(myFiles[myIdx].name).split(".");
				if (mySplit[mySplit.length-1].toLowerCase() == "xml"){
					if (myXmlFile == null){
						myXmlFile = myFiles[myIdx];
					}else if (mySplit[0] < unescape(myXmlFile.name).split(".")[0]){
						myXmlFile = myFiles[myIdx];
					}
				}
			}
		}
	}
    
    //IF XML IS VALID, PROCEED TO PROCESSING
    if (myXmlFile != null && myXmlFile.exists){
		// found an xml file--read it
		var myError = false;

		var fileOK = myXmlFile.open("r");
		if (! fileOK){
			logMessage("Error opening XML file: '" + unescape(myXmlFile.name) + "'");
			myError = true;
		}
		try{
			var myXmlString = myXmlFile.read();
		}catch (err){
			logMessage("Error reading XML file: '" + unescape(myXmlFile.name) + "'");
			myError = true;
		}finally{
			myXmlFile.close();
		}

		if (! myError){
            //WAIVER IS VALID, PROCESS IT
			var myErrorMsg = processFile(myXmlString);
			if (myErrorMsg != ""){
				logMessage(myErrorMsg);
				myError = true;
			}
		}

		// SUCCESS move waiver file to completed folder
		if (! myError){
			// move xml file to "complete" folder (create one if it doesn't exist)
			var myProcessedFolderPath = app.bp_watchFolder.path + "/" + app.bp_watchFolder.name + "/" + "complete";
			var myProcessedFolder = new Folder(myProcessedFolderPath);
			if (! myProcessedFolder.exists){
				myProcessedFolder.create();
			}
			myXmlFile.copy(myProcessedFolderPath + "/" + myXmlFile.name);
			logMessage("Successfully processed XML file: '" + unescape(myXmlFile.name) + "'");
		}else{

		// ERROR move waiver file to error folder
			var myErrorFolderPath = app.bp_watchFolder.path + "/" + app.bp_watchFolder.name + "/" + "error";
			var myErrorFolder = new Folder(myErrorFolderPath);
			if (! myErrorFolder.exists){
				myErrorFolder.create();
			}
			myXmlFile.copy(myErrorFolderPath + "/" + myXmlFile.name);
            //TODO - can we send an http command here to alert the error?
		}

        $.sleep(processDelay);
		// remove xml file in either case
		myXmlFile.remove();

		// close and reload the template
        //TODO - this is what we would move around to open project based on waiver details, instead of monolithic project
         logMessage("closing project");
		app.project.close(CloseOptions.DO_NOT_SAVE_CHANGES);
		
        
                //desperate attempt to prevent AE from randomly freezing
        logMessage("Sleep 250ms before opening project");
        $.sleep(processDelay);
        logMessage("opening project");
		app.open(templateFile);
        
        //experimental - clear the cache
        logMessage("purging caches");
        app.purge(PurgeTarget.ALL_CACHES);
        app.purge(PurgeTarget.UNDO_CACHES);
        app.purge(PurgeTarget.SNAPSHOT_CACHES);
        app.purge(PurgeTarget.IMAGE_CACHES);

		// take short break, then look for another job
         logMessage("wait 250ms for oneBigLoop");
		app.scheduleTask("app.bp_oneBigLoop();",processDelay,false);


	}
    //NO VALID WAIVERS FOUND
    else{  
        // no xml files found -- go to sleep for a while
        if (!devMode){
          logMessage("End of oneBigLoop. User is:" + computerUserName + " | no waivers found - waiting " + loopDelay + "ms");
        app.scheduleTask("app.bp_oneBigLoop();",loopDelay,false);
        }else {
          logMessage("DEV MODE!!!!!! user is:" + computerUserName + " | no waivers found - exiting loop");
            }
	}

}

// END OF CheckForXML()

}

// END OF oneBigLoop()
