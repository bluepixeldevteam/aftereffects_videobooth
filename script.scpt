FasdUAS 1.101.10   ��   ��    k             l     ��  ��    H B multi-processor oriented Applescript for Pre-Ingestion processing     � 	 	 �   m u l t i - p r o c e s s o r   o r i e n t e d   A p p l e s c r i p t   f o r   P r e - I n g e s t i o n   p r o c e s s i n g   
  
 l     ��������  ��  ��        j     �� �� 0 theplistpath thePListPath  m        �   � / U s e r s / b l u e p i x e l / D o c u m e n t s / p r o c e s s _ c o n f i g / v i d e o b o o t h _ m a s t e r _ c o n f i g . p l i s t      j    �� �� *0 aftereffectslogfile afterEffectsLogFile  m       �   l / U s e r s / b l u e p i x e l / D o c u m e n t s / w a i v e r s _ p e n d i n g / l o g / l o g . t x t      j    �� �� 20 aftereffectsprojectfile afterEffectsProjectFile  m       �   � / U s e r s / b l u e p i x e l / g i t / a f t e r e f f e c t s _ v i d e o b o o t h / v i d e o b o o t h _ m a s t e r . a e p      j   	 �� �� 00 incomingfileextensions incomingFileExtensions  J   	         m   	 
 ! ! � " "  . j p g    # $ # m   
  % % � & &  . p n g $  ' ( ' m     ) ) � * *  . m p 4 (  +�� + m     , , � - -  . m o v��     . / . j    �� 0�� 80 rawtranscodefileextensions rawTranscodeFileExtensions 0 J     1 1  2 3 2 m     4 4 � 5 5  . m p 4 3  6�� 6 m     7 7 � 8 8  . m o v��   /  9 : 9 j    �� ;�� D0  bypassrawtranscodefileextensions  bypassRawTranscodeFileExtensions ; J     < <  = > = m     ? ? � @ @  . j p g >  A�� A m     B B � C C  . p n g��   :  D E D j    !�� F�� 60 statsintervalcheckminutes statsIntervalCheckMinutes F m     ����  E  G H G j   " $�� I�� $0 processdelaysecs processDelaySecs I m   " #����  H  J K J j   % )�� L�� 0 loopdelaysecs loopDelaySecs L m   % (���� 
 K  M N M j   * ,�� O�� 0 	automated   O m   * +��
�� boovfals N  P Q P j   - /�� R�� 0 devendpoints devEndpoints R m   - .��
�� boovfals Q  S T S j   0 4�� U�� $0 computerusername computerUserName U m   0 3 V V � W W  b l u e p i x e l T  X Y X j   5 9�� Z�� $0 waivergrepstring waiverGrepString Z m   5 8 [ [ � \ \  - e   . x m l Y  ] ^ ] j   : >�� _�� >0 filesizecheck_minaeoutputsize filesizeCheck_minAEOutputSize _ m   : =���� ��@ ^  ` a ` j   ? A�� b�� (0 filesizecheckdelay filesizeCheckDelay b m   ? @����  a  c d c j   B D�� e�� (0 performfilecleanup performFileCleanup e m   B C��
�� boovtrue d  f g f j   E I�� h�� 0 dev_mode   h m   E F��
�� boovfals g  i j i l      k l m k j   J P�� n�� 00 oldestfileagethreshold oldestFileAgeThreshold n m   J M����  l  seconds    m � o o  s e c o n d s j  p q p l      r s t r j   Q W�� u�� 00 expectedmaxprocesstime expectedMaxProcessTime u m   Q T���� � s  seconds    t � v v  s e c o n d s q  w x w j   X ^�� y�� "0 applicationname applicationName y m   X [ z z � { { $ V i d e o b o o t h   P r o c e s s x  | } | l     ��������  ��  ��   }  ~  ~ j   _ e�� ��� $0 maxfilespercycle maxFilesPerCycle � m   _ b���� 
   � � � l     ��������  ��  ��   �  � � � l     ��������  ��  ��   �  � � � p   f f � � ������ 00 pendingaewaiverlistold pendingAEWaiverListOLD��   �  � � � p   f f � � ������ .0 pendingamefilelistold pendingAMEFileListOLD��   �  � � � p   f f � � ������ :0 pendingdropboxwaiverlistold pendingDropboxWaiverListOLD��   �  � � � l     ��������  ��  ��   �  � � � p   f f � � �� ��� "0 dropboxrootpath dropboxRootPath � �� ��� .0 incomingrawxmlwaivers incomingRawXmlWaivers � �� ��� :0 incomingrawmediafolderposix incomingRawMediaFolderPosix � ������ %0 !waivers_pending_completed_archive  ��   �  � � � p   f f � � ������ 0 stationtype stationType��   �  � � � p   f f � � �� ��� :0 transcodependingfolderposix transcodePendingFolderPosix � ������ >0 transcodecompletedfolderposix transcodeCompletedFolderPosix��   �  � � � p   f f � � �� ��� @0 pendingwaiversforaefolderposix pendingWaiversForAEFolderPosix � ������ 0 
media_pool  ��   �  � � � p   f f � � �� ��� 0 computer_id   � ������ 0 ishappy isHappy��   �  � � � p   f f � � �� ��� 0 processed_media   � �� ��� 0 	ae_output   � �� ��� 0 	ame_watch   � ������ 0 spock_ingestor  ��   �  � � � p   f f � � ������ 80 laststatsgatheredtimestamp lastStatsGatheredTimestamp��   �  � � � p   f f � � �� ��� :0 laststalefilechecktimestamp lastStaleFileCheckTimestamp � ������ *0 forcestalefilecheck forceStaleFileCheck��   �  � � � l     ��������  ��  ��   �  � � � l     ��������  ��  ��   �  � � � i   f i � � � I     ������
�� .aevtoappnull  �   � ****��  ��   � k     � � �  � � � I    �� ���
�� .ascrcmnt****      � **** � b      � � � l     ����� � I    ������
�� .misccurdldt    ��� null��  ��  ��  ��   � m     � � � � �  L a u n c h i n g��   �  � � � I    �� ����� $0 shownotification showNotification �  � � � m     � � � � �  L a u n c h i n g �  ��� � m     � � � � � , c h e c k i n g   s y s t e m   s t a t u s��  ��   �  � � � l   ��������  ��  ��   �  � � � Z    8 � ����� � =     � � � I    �� ����� 0 
fileexists 
fileExists �  ��� � o    ���� 0 theplistpath thePListPath��  ��   � m    ��
�� boovfals � k   # 4 � �  � � � I  # .�� ���
�� .sysodisAaleR        TEXT � b   # * � � � m   # $ � � � � � P A b o r t i n g   b e c a u s e   c o n f i g   f i l e   n o t   f o u n d :   � o   $ )���� 0 theplistpath thePListPath��   �  ��� � I  / 4������
�� .aevtquitnull��� ��� null��  ��  ��  ��  ��   �  � � � l  9 9��������  ��  ��   �  � � � r   9 < � � � m   9 :��
�� boovtrue � o      ���� 0 ishappy isHappy �  � � � r   = D � � � I   = B������� 0 setvars setVars��  �   � o      �~�~ 0 isready1 isReady1 �  � � � r   E L � � � I   E J�}�|�{�} 0 setpaths setPaths�|  �{   � o      �z�z 0 isready2 isReady2 �  � � � Z   M z � ��y � � F   M X � � � =  M P � � � o   M N�x�x 0 isready1 isReady1 � m   N O�w
�w boovtrue � =  S V � � � o   S T�v�v 0 isready2 isReady2 � m   T U�u
�u boovtrue � k   [ h � �  � � � I   [ a�t ��s�t 0 gatherstats gatherStats �  ��r � m   \ ]�q�q  �r  �s   �  ��p � I   b h�o ��n�o "0 processonecycle processOneCycle �  �m  m   c d�l
�l boovtrue�m  �n  �p  �y   � I  k z�k�j
�k .sysodisAaleR        TEXT b   k v b   k t b   k p m   k n �		 & A b o r t i n g .   s e t V a r   =   o   n o�i�i 0 isready1 isReady1 m   p s

 �    s e t P a t h s   =   o   t u�h�h 0 isready2 isReady2�j   �  I  { ��g�f
�g .ascrcmnt****      � **** b   { � l  { ��e�d I  { ��c�b�a
�c .misccurdldt    ��� null�b  �a  �e  �d   m   � � � t D o n e   w i t h   i n i t i a l   c y c l e   -   w a i t i n g   3 0 s e c   f o r   o n I d l e   h a n d l e r�f   �` l  � ��_�^�]�_  �^  �]  �`   �  l     �\�[�Z�\  �[  �Z    i   j m I     �Y�X�W
�Y .miscidlenmbr    ��� null�X  �W   k     !  I     �V�U�V "0 processonecycle processOneCycle �T m    �S
�S boovfals�T  �U    !  I   �R"�Q
�R .ascrcmnt****      � ****" b    #$# b    %&% b    '(' l   )�P�O) I   �N�M�L
�N .misccurdldt    ��� null�M  �L  �P  �O  ( m    ** �++ 2 D o n e   w i t h   c y c l e   -   w a i t i n g& o    �K�K 0 loopdelaysecs loopDelaySecs$ m    ,, �-- 
   s e c s�Q  ! .�J. L    !// o     �I�I 0 loopdelaysecs loopDelaySecs�J   010 l     �H�G�F�H  �G  �F  1 232 i   n q454 I     �E�D�C
�E .aevtquitnull��� ��� null�D  �C  5 k     66 787 I    �B9�A
�B .ascrcmnt****      � ****9 b     :;: l    <�@�?< I    �>�=�<
�> .misccurdldt    ��� null�=  �<  �@  �?  ; m    == �>>  E x i t i n g   A p p�A  8 ?@? I    �;A�:�; 0 gatherstats gatherStatsA B�9B m    �8�8  �9  �:  @ C�7C M    DD I     �6�5�4
�6 .aevtquitnull��� ��� null�5  �4  �7  3 EFE l     �3�2�1�3  �2  �1  F GHG i   r uIJI I      �0K�/�0 $0 shownotification showNotificationK LML o      �.�.  0 subtitlestring subtitleStringM N�-N o      �,�, 0 messagestring messageString�-  �/  J k     +OO PQP Z     #RS�+TR >    UVU o     �*�* 0 messagestring messageStringV m    �)
�) 
nullS I   �(WX
�( .sysonotfnull��� ��� TEXTW o    �'�' 0 messagestring messageStringX �&YZ
�& 
apprY o    �%�% "0 applicationname applicationNameZ �$[�#
�$ 
subt[ o    �"�"  0 subtitlestring subtitleString�#  �+  T I   #�!� \
�! .sysonotfnull��� ��� TEXT�   \ �]^
� 
subt] o    ��  0 subtitlestring subtitleString^ �_�
� 
appr_ o    �� "0 applicationname applicationName�  Q `a` l  $ )bcdb I  $ )�e�
� .sysodelanull��� ��� nmbre m   $ %�� �  c ) #allow time for notification to fire   d �ff F a l l o w   t i m e   f o r   n o t i f i c a t i o n   t o   f i r ea g�g l  * *����  �  �  �  H hih l     ����  �  �  i jkj l     ����  �  �  k lml i   v ynon I      �p�� "0 processonecycle processOneCyclep q�q o      �
�
  0 isinitialcycle isInitialCycle�  �  o k    rr sts I    �	u�
�	 .ascrcmnt****      � ****u b     vwv l    x��x I    ���
� .misccurdldt    ��� null�  �  �  �  w m    yy �zz  S t a r t i n g   C y c l e�  t {|{ I    �}�� $0 shownotification showNotification} ~~ m    �� ���  S t a r t i n g   C y c l e �� � m    ��
�� 
null�   �  | ��� l   ��������  ��  ��  � ��� I    ������� 0 gatherstats gatherStats� ���� m    ���� ��  ��  � ��� I     �������� 0 handlewaivers handleWaivers��  ��  � ��� I  ! 2�����
�� .ascrcmnt****      � ****� b   ! .��� b   ! (��� l  ! &������ I  ! &������
�� .misccurdldt    ��� null��  ��  ��  ��  � m   & '�� ��� > D o n e   c h e c k i n g   w a i v e r s .   w a i t i n g  � o   ( -���� $0 processdelaysecs processDelaySecs��  � ��� I  3 <�����
�� .sysodelanull��� ��� nmbr� o   3 8���� $0 processdelaysecs processDelaySecs��  � ��� l  = =��������  ��  ��  � ��� I   = C������� 0 gatherstats gatherStats� ���� m   > ?���� ��  ��  � ��� I   D I�������� :0 transcodeaftereffectsoutput transcodeAfterEffectsOutput��  ��  � ��� l  J J������  � : 4 TODO improve to have variable for clean file output   � ��� h   T O D O   i m p r o v e   t o   h a v e   v a r i a b l e   f o r   c l e a n   f i l e   o u t p u t� ��� I  J [�����
�� .ascrcmnt****      � ****� b   J W��� b   J Q��� l  J O������ I  J O������
�� .misccurdldt    ��� null��  ��  ��  ��  � m   O P�� ��� B d o n e   c h e c k i n g   A E   o u t p u t .   w a i t i n g  � o   Q V���� $0 processdelaysecs processDelaySecs��  � ��� I  \ e�����
�� .sysodelanull��� ��� nmbr� o   \ a���� $0 processdelaysecs processDelaySecs��  � ��� l  f f��������  ��  ��  � ��� I   f l������� 0 gatherstats gatherStats� ���� m   g h���� ��  ��  � ��� I   m r�������� ,0 movefilestoingestion moveFilesToIngestion��  ��  � ��� I  s ������
�� .ascrcmnt****      � ****� b   s ���� b   s z��� l  s x������ I  s x������
�� .misccurdldt    ��� null��  ��  ��  ��  � m   x y�� ��� L d o n e   c h e c k i n g   e n c o d e r   o u t p u t .   w a i t i n g  � o   z ���� $0 processdelaysecs processDelaySecs��  � ��� I  � ������
�� .sysodelanull��� ��� nmbr� o   � ����� $0 processdelaysecs processDelaySecs��  � ��� l  � ���������  ��  ��  � ��� l  � �������  � 0 * clean out the raw .mov from encoder watch   � ��� T   c l e a n   o u t   t h e   r a w   . m o v   f r o m   e n c o d e r   w a t c h� ��� I   � ��������� 0 filecleanup fileCleanup��  ��  � ��� I  � ������
�� .ascrcmnt****      � ****� b   � ���� b   � ���� l  � ������� I  � �������
�� .misccurdldt    ��� null��  ��  ��  ��  � m   � ��� ��� R d o n e   c l e a n i n g   u p   e n c o d e r   s o u r c e .   w a i t i n g  � o   � ����� $0 processdelaysecs processDelaySecs��  � ��� I  � ������
�� .sysodelanull��� ��� nmbr� o   � ����� $0 processdelaysecs processDelaySecs��  � ��� l  � ���������  ��  ��  � ��� I   � �������� 0 gatherstats gatherStats� ���� m   � ����� ��  ��  � ��� I  � ������
�� .ascrcmnt****      � ****� b   � ���� l  � ������� I  � �������
�� .misccurdldt    ��� null��  ��  ��  ��  � m   � ��� ��� $ d o n e   s e n d i n g   s t a t s��  � ��� l  � ���������  ��  ��  � ��� Z   � ������� o   � �����  0 isinitialcycle isInitialCycle� I   � �������� $0 shownotification showNotification� ��� m   � ��� ��� , F i n i s h e d   I n i t i a l   C y c l e� ���� m   � ��� ��� B w a i t i n g   ~ 3 0 s e c   f o r   o n I d l e   h a n d l e r��  ��  ��  � I   � �������� $0 shownotification showNotification� ��� m   � ��� ���  F i n i s h e d   C y c l e� ���� l  � � ����  b   � � b   � � m   � � �  w a i t i n g   o   � ����� 0 loopdelaysecs loopDelaySecs m   � � � $ s e c   f o r   n e x t   c y c l e��  ��  ��  ��  � 	
	 l  � ���������  ��  ��  
  Z   ����� =  � � o   � ����� 0 	automated   m   � ���
�� boovfals k   �  I  �����
�� .ascrcmnt****      � **** b   � l  � ����� I  � �������
�� .misccurdldt    ��� null��  ��  ��  ��   m   �  � j D o n e   w i t h   c y c l e   -   e x i t i n g   b e c a u s e   a u t o m a t i o n   i s   f a l s e��    I ��
�� .sysodisAaleR        TEXT m  	 � j D o n e   w i t h   c y c l e   -   e x i t i n g   b e c a u s e   a u t o m a t i o n   i s   f a l s e �� ��
�� 
givu  m  �� ��   !�~! I �}�|�{
�} .aevtquitnull��� ��� null�|  �{  �~  ��  ��   "�z" l �y�x�w�y  �x  �w  �z  m #$# l     �v�u�t�v  �u  �t  $ %&% l     �s'(�s  ' C =#############################################################   ( �)) z # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #& *+* l     �r,-�r  , ) ### --DO NOT EDIT BELOW THIS LIINE--   - �.. F # #   - - D O   N O T   E D I T   B E L O W   T H I S   L I I N E - -+ /0/ l     �q12�q  1 > 8## --THESE ARE ALL THE HANDLERS USED IN THE LOOP ABOVE--   2 �33 p # #   - - T H E S E   A R E   A L L   T H E   H A N D L E R S   U S E D   I N   T H E   L O O P   A B O V E - -0 454 l     �p67�p  6 C =#############################################################   7 �88 z # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #5 9:9 l     �o�n�m�o  �n  �m  : ;<; i   z }=>= I      �l?�k�l &0 checkpendingfiles checkPendingFiles? @A@ o      �j�j 0 oldlist oldListA B�iB o      �h�h 0 newlist newList�i  �k  > k     �CC DED r     FGF c     HIH J     �g�g  I m    �f
�f 
listG o      �e�e 0 
filesadded 
filesAddedE JKJ r    LML c    NON J    	�d�d  O m   	 
�c
�c 
listM o      �b�b 0 filesremoved filesRemovedK PQP r    RSR m    �a
�a boovfalsS o      �`�` 0 isprocessing isProcessingQ TUT Z    �VW�_XV F    #YZY l   [�^�][ @    \]\ n    ^_^ 1    �\
�\ 
leng_ o    �[�[ 0 newlist newList] n    `a` 1    �Z
�Z 
lenga o    �Y�Y 0 oldlist oldList�^  �]  Z l   !b�X�Wb ?    !cdc n    efe 1    �V
�V 
lengf o    �U�U 0 oldlist oldListd m     �T�T  �X  �W  W k   & �gg hih l  & &�S�R�Q�S  �R  �Q  i jkj l  & &�Plm�P  l   new/old is same and > 0   m �nn 0   n e w / o l d   i s   s a m e   a n d   >   0k opo Z   & 8qr�O�Nq =   & )sts o   & '�M�M 0 newlist newListt o   ' (�L�L 0 oldlist oldListr k   , 4uu vwv l  , ,�Kxy�K  x &   error condition, not processing   y �zz @   e r r o r   c o n d i t i o n ,   n o t   p r o c e s s i n gw {|{ I  , 1�J}�I
�J .ascrcmnt****      � ****} m   , -~~ �  n o t   p r o c e s s i n g�I  | ��H� L   2 4�� m   2 3�G
�G boovfals�H  �O  �N  p ��� l  9 9�F�E�D�F  �E  �D  � ��� Y   9 d��C���B� k   I _�� ��� r   I O��� n   I M��� 4   J M�A�
�A 
cobj� o   K L�@�@ 0 i  � o   I J�?�? 0 oldlist oldList� o      �>�> 0 n  � ��=� Z  P _���<�;� H   P T�� E  P S��� o   P Q�:�: 0 newlist newList� o   Q R�9�9 0 n  � r   W [��� o   W X�8�8 0 n  � n      ���  ;   Y Z� o   X Y�7�7 0 filesremoved filesRemoved�<  �;  �=  �C 0 i  � m   < =�6�6 � I  = D�5��4
�5 .corecnte****       ****� n   = @��� 2  > @�3
�3 
cobj� o   = >�2�2 0 oldlist oldList�4  �B  � ��� l  e e�1�0�/�1  �0  �/  � ��� Y   e ���.���-� k   u ��� ��� r   u {��� n   u y��� 4   v y�,�
�, 
cobj� o   w x�+�+ 0 i  � o   u v�*�* 0 newlist newList� o      �)�) 0 n  � ��(� Z  | ����'�&� H   | ��� E  | ��� o   | }�%�% 0 oldlist oldList� o   } ~�$�$ 0 n  � r   � ���� o   � ��#�# 0 n  � n      ���  ;   � �� o   � ��"�" 0 
filesadded 
filesAdded�'  �&  �(  �. 0 i  � m   h i�!�! � I  i p� ��
�  .corecnte****       ****� n   i l��� 2  j l�
� 
cobj� o   i j�� 0 newlist newList�  �-  � ��� l  � �����  �  �  � ��� I  � ����
� .ascrcmnt****      � ****� c   � ���� b   � ���� m   � ��� ���  f i l e s A d d e d :  � o   � ��� 0 
filesadded 
filesAdded� m   � ��
� 
TEXT�  � ��� I  � ����
� .ascrcmnt****      � ****� b   � ���� m   � ��� ���  f i l e s R e m o v e d :  � o   � ��� 0 filesremoved filesRemoved�  � ��� l  � �����  �  �  � ��� l  � �����  � ( " adding files but not removing any   � ��� D   a d d i n g   f i l e s   b u t   n o t   r e m o v i n g   a n y� ��� Z   � ������ F   � ���� l  � ����� ?   � ���� n   � ���� 1   � ��

�
 
leng� o   � ��	�	 0 
filesadded 
filesAdded� m   � ���  �  �  � l  � ����� =   � ���� n   � ���� 1   � ��
� 
leng� o   � ��� 0 filesremoved filesRemoved� m   � ���  �  �  � k   � ��� ��� l  � �����  � &   error condition, not processing   � ��� @   e r r o r   c o n d i t i o n ,   n o t   p r o c e s s i n g� ��� L   � ��� m   � �� 
�  boovfals�  �  �  � ��� l  � ���������  ��  ��  � ��� l  � �������  � %  adding files AND removing them   � ��� >   a d d i n g   f i l e s   A N D   r e m o v i n g   t h e m� ��� Z   � �������� F   � ���� l  � ������� @   � ���� n   � ���� 1   � ���
�� 
leng� o   � ����� 0 
filesadded 
filesAdded� m   � �����  ��  ��  � l  � ������� ?   � ���� n   � �   1   � ���
�� 
leng o   � ����� 0 filesremoved filesRemoved� m   � �����  ��  ��  � L   � � m   � ���
�� boovtrue��  ��  � �� l  � ���������  ��  ��  ��  �_  X k   � �  l  � �����   %  new/old are both 0 OR old is 0    �		 >   n e w / o l d   a r e   b o t h   0   O R   o l d   i s   0 

 l  � �����   A ; this is where we are when new files are added after a lull    � v   t h i s   i s   w h e r e   w e   a r e   w h e n   n e w   f i l e s   a r e   a d d e d   a f t e r   a   l u l l  I  � �����
�� .ascrcmnt****      � **** m   � � � 0 s k i p p i n g   l i s t   c o m p a r i s o n��   �� r   � � m   � ���
�� boovtrue o      ���� 0 isprocessing isProcessing��  U  l  � ���������  ��  ��    L   � � o   � ����� 0 isprocessing isProcessing �� l  � ���������  ��  ��  ��  <  l     ��������  ��  ��     i   ~ �!"! I      �������� 0 filecleanup fileCleanup��  ��  " k     '## $%$ Z     %&'��(& =    )*) o     ���� (0 performfilecleanup performFileCleanup* m    ��
�� boovtrue' k   
 !++ ,-, r   
 ./. m   
 00 �11 h r m   - r f   / U s e r s / b l u e p i x e l / D o c u m e n t s / a m e _ w a t c h / S o u r c e / */ o      ���� 0 shscript  - 232 Q    45��4 I   ��6��
�� .sysoexecTEXT���     TEXT6 o    ���� 0 shscript  ��  5 R      ��7��
�� .ascrerr ****      � ****7 o      ���� 
0 errmsg  ��  ��  3 898 l     ��������  ��  ��  9 :��: l     ��;<��  ;   DO NOTHING   < �==    D O   N O T H I N G��  ��  ( l  $ $��>?��  >  
do nothing   ? �@@  d o   n o t h i n g% ABA l  & &��������  ��  ��  B C��C l  & &��������  ��  ��  ��    DED l     ��������  ��  ��  E FGF l     ��������  ��  ��  G HIH i   � �JKJ I      �������� ,0 movefilestoingestion moveFilesToIngestion��  ��  K k    �LL MNM r     OPO o     ���� 0 processed_media  P o      ���� 0 myfolder  N QRQ r    	STS n    UVU 1    ��
�� 
psxpV o    ���� 0 processed_media  T o      ���� 0 	pmyfolder  R WXW l  
 
��������  ��  ��  X YZY I  
 ��[��
�� .ascrcmnt****      � ****[ b   
 \]\ l  
 ^����^ I  
 ������
�� .misccurdldt    ��� null��  ��  ��  ��  ] m    __ �``   s t a r t i n g   u p l o a d s��  Z aba r    cdc b    efe b    ghg m    ii �jj  l s  h n    klk 1    ��
�� 
strql o    ���� 0 	pmyfolder  f m    mm �nn    |   g r e p   ' . m p 4 'd o      ���� 0 shscript  b opo l     ��������  ��  ��  p qrq Q     Pstus r   # .vwv c   # ,xyx n   # *z{z 2  ( *��
�� 
cpar{ l  # (|����| I  # (��}��
�� .sysoexecTEXT���     TEXT} o   # $���� 0 shscript  ��  ��  ��  y m   * +��
�� 
listw o      ���� 0 filelist  t R      ��~��
�� .ascrerr ****      � ****~ o      ���� 
0 errmsg  ��  u k   6 P ��� I  6 C�����
�� .ascrcmnt****      � ****� b   6 ?��� b   6 =��� l  6 ;������ I  6 ;������
�� .misccurdldt    ��� null��  ��  ��  ��  � m   ; <�� ���  u p l o a d   e r r o r :  � o   = >���� 
0 errmsg  ��  � ��� I  D I�����
�� .ascrcmnt****      � ****� m   D E�� ��� z i f   e r r o r   1   i t   p r o b a b l y   m e a n s   t h e r e   a r e   n o   f i l e s   t o   u p l o a d   y e t��  � ���� r   J P��� c   J N��� J   J L����  � m   L M��
�� 
list� o      ���� 0 filelist  ��  r ��� l  Q Q��������  ��  ��  � ��� l  Q T���� r   Q T��� m   Q R����  � o      ���� 0 
cyclecount 
cycleCount� . (limit how many files we do on each cycle   � ��� P l i m i t   h o w   m a n y   f i l e s   w e   d o   o n   e a c h   c y c l e� ��� X   U������ k   g��� ��� Z   g v������ ?   g n��� o   g h�~�~ 0 
cyclecount 
cycleCount� o   h m�}�} $0 maxfilespercycle maxFilesPerCycle�  S   q r��  �  � ��� l  w w�|�{�z�|  �{  �z  � ��� r   w ���� c   w ~��� b   w z��� o   w x�y�y 0 myfolder  � o   x y�x�x 0 i  � m   z }�w
�w 
TEXT� o      �v�v 
0 myfile  � ��� r   � ���� o   � ��u�u 0 i  � o      �t�t 0 newfile  � ��� l  � ��s�r�q�s  �r  �q  � ��� l  � ��p�o�n�p  �o  �n  � ��� Z   �����m�l� =  � ���� I   � ��k��j�k ,0 checkfilewritestatus checkFileWriteStatus� ��� o   � ��i�i 
0 myfile  � ��� m   � ��h�h  B@� ��g� m   � ��f�f �g  �j  � m   � ��e
�e boovtrue� k   ���� ��� l  � ��d���d  � , &upload to Dropbox folder for ingestion   � ��� L u p l o a d   t o   D r o p b o x   f o l d e r   f o r   i n g e s t i o n� ��� r   � ���� b   � ���� b   � ���� b   � ���� b   � ���� m   � ��� ���  m v  � n   � ���� 1   � ��c
�c 
strq� o   � ��b�b 
0 myfile  � m   � ��� ���   � n   � ���� 1   � ��a
�a 
strq� o   � ��`�` 0 spock_ingestor  � o   � ��_�_ 0 newfile  � o      �^�^ 0 shscript  � ��� l  � ��]�\�[�]  �\  �[  � ��� Q   � ����� k   � ��� ��� I  � ��Z��Y
�Z .ascrcmnt****      � ****� b   � ���� l  � ���X�W� I  � ��V�U�T
�V .misccurdldt    ��� null�U  �T  �X  �W  � m   � ��� ���  s t a r t i n g   u p l o a d�Y  � ��S� I  � ��R��Q
�R .sysoexecTEXT���     TEXT� o   � ��P�P 0 shscript  �Q  �S  � R      �O��N
�O .ascrerr ****      � ****� o      �M�M 
0 errmsg  �N  � k   � ��� ��� I  � ��L��K
�L .ascrcmnt****      � ****� b   � ���� b   � ���� l  � ���J�I� I  � ��H�G�F
�H .misccurdldt    ��� null�G  �F  �J  �I  � m   � ��� ��� ( e r r o r   i n   u p l o a d i n g :  � o   � ��E�E 
0 errmsg  �K  � � � I  � ��D�C�B
�D .sysobeepnull��� ��� long�C  �B     I  � ��A�@�?
�A .sysobeepnull��� ��� long�@  �?    I  � ��>�=�<
�> .sysobeepnull��� ��� long�=  �<    I  � ��;
�; .sysodisAaleR        TEXT b   � �	
	 m   � � � > A n   e r r o r   o c c u r e d   i n   u p l o a d i n g :  
 o   � ��:�: 
0 errmsg   �9�8
�9 
givu m   � ��7�7 �8   �6  S   � ��6  �  l  � ��5�4�3�5  �4  �3    l  � ��2�2   6 0cleanup intermediate raw files to save HDD space    � ` c l e a n u p   i n t e r m e d i a t e   r a w   f i l e s   t o   s a v e   H D D   s p a c e  r   � I   �	�1�0�1 0 	changeext 	changeExt  c   � o   � ��/�/ 0 i   m   ��.
�. 
TEXT �- m     �!!  �-  �0   o      �,�, 0 basefilename baseFilename "#" r  !$%$ b  &'& b  ()( b  *+* b  ,-, m  .. �//  l s  - n  010 1  �+
�+ 
strq1 o  �*�* 0 
media_pool  + m  22 �33    |   g r e p   - e  ) o  �)�) 0 basefilename baseFilename' m  44 �55  *% o      �(�( 0 shscript  # 676 Q  "V89:8 r  %0;<; c  %.=>= n  %,?@? 2 *,�'
�' 
cpar@ l %*A�&�%A I %*�$B�#
�$ .sysoexecTEXT���     TEXTB o  %&�"�" 0 shscript  �#  �&  �%  > m  ,-�!
�! 
list< o      � �  $0 intermediatelist intermediateList9 R      �C�
� .ascrerr ****      � ****C o      �� 
0 errmsg  �  : k  8VDD EFE I 8G�G�
� .ascrcmnt****      � ****G b  8CHIH b  8AJKJ l 8=L��L I 8=���
� .misccurdldt    ��� null�  �  �  �  K m  =@MM �NN  u p l o a d   e r r o r :  I o  AB�� 
0 errmsg  �  F OPO I HO�Q�
� .ascrcmnt****      � ****Q m  HKRR �SS z i f   e r r o r   1   i t   p r o b a b l y   m e a n s   t h e r e   a r e   n o   f i l e s   t o   u p l o a d   y e t�  P T�T r  PVUVU c  PTWXW J  PR��  X m  RS�
� 
listV o      �� $0 intermediatelist intermediateList�  7 YZY l WW����  �  �  Z [\[ X  W�]�^] k  i�__ `a` r  i~bcb c  i|ded b  ixfgf b  ivhih b  irjkj m  illl �mm  r m  k n  lqnon 1  oq�

�
 
strqo o  lo�	�	 0 
media_pool  i m  rupp �qq  /g o  vw�� 0 i  e m  x{�
� 
TEXTc o      �� 0 shscript  a rsr Q  �tuvt I ���w�
� .sysoexecTEXT���     TEXTw o  ���� 0 shscript  �  u R      �x�
� .ascrerr ****      � ****x o      � �  
0 errmsg  �  v I ����y��
�� .ascrcmnt****      � ****y b  ��z{z b  ��|}| l ��~����~ I ��������
�� .misccurdldt    ��� null��  ��  ��  ��  } m  �� ��� 8 i n t e r m e d i a t e   c l e a n u p   e r r o r :  { o  ������ 
0 errmsg  ��  s ���� l ����������  ��  ��  ��  � 0 i  ^ o  Z[���� $0 intermediatelist intermediateList\ ��� l ����������  ��  ��  � ��� r  ����� [  ����� o  ������ 0 
cyclecount 
cycleCount� m  ������ � o      ���� 0 
cyclecount 
cycleCount� ��� l ����������  ��  ��  � ���� l ��������  �  fileReady end if   � ���   f i l e R e a d y   e n d   i f��  �m  �l  � ���� l ����������  ��  ��  ��  �� 0 i  � o   X Y���� 0 filelist  � ��� I �������
�� .ascrcmnt****      � ****� b  ����� l �������� I ��������
�� .misccurdldt    ��� null��  ��  ��  ��  � m  ���� ��� V d o n e   w i t h   u p l o a d i n g .   W a i t i n g   f o r   n e x t   c y c l e��  � ���� l ����������  ��  ��  ��  I ��� l     ��������  ��  ��  � ��� l     ��������  ��  ��  � ��� i   � ���� I      �������� :0 transcodeaftereffectsoutput transcodeAfterEffectsOutput��  ��  � k    ��� ��� l      ������  � 7 1 
TODO improve to take varaible for clean files 
   � ��� b   
 T O D O   i m p r o v e   t o   t a k e   v a r a i b l e   f o r   c l e a n   f i l e s   
� ��� r     ��� o     ���� 0 	ae_output  � o      ���� 0 myfolder  � ��� r    ��� c    ��� n    	��� 1    	��
�� 
psxp� l   ������ b    ��� o    ���� @0 pendingwaiversforaefolderposix pendingWaiversForAEFolderPosix� m    �� ���  c o m p l e t e /��  ��  � m   	 
��
�� 
TEXT� o      ���� 0 completed_waivers  � ��� l   ��������  ��  ��  � ��� I   �����
�� .ascrcmnt****      � ****� b    ��� l   ������ I   ������
�� .misccurdldt    ��� null��  ��  ��  ��  � m    �� ��� & s t a r t i n g   t r a n s c o d e s��  � ��� r    #��� b    !��� b    ��� m    �� ���  l s  � n    ��� 1    ��
�� 
strq� o    ���� 0 completed_waivers  � m     �� ���    |   g r e p   - e   . x m l� o      ���� 0 shscript  � ��� l  $ $��������  ��  ��  � ��� Q   $ T���� r   ' 2��� c   ' 0��� n   ' .��� 2  , .��
�� 
cpar� l  ' ,������ I  ' ,�����
�� .sysoexecTEXT���     TEXT� o   ' (���� 0 shscript  ��  ��  ��  � m   . /��
�� 
list� o      ���� 0 filelist  � R      �����
�� .ascrerr ****      � ****� o      ���� 
0 errmsg  ��  � k   : T�� ��� I  : E�����
�� .ascrcmnt****      � ****� b   : A��� l  : ?������ I  : ?������
�� .misccurdldt    ��� null��  ��  ��  ��  � o   ? @���� 
0 errmsg  ��  � ��� I  F M�����
�� .ascrcmnt****      � ****� m   F I�� ��� > p r o b a b l y   b e c a u s e   n o   w a i v e r s   y e t��  � ���� r   N T��� c   N R��� J   N P����  � m   P Q��
�� 
list� o      ���� 0 filelist  ��  � ��� l  U U��������  ��  ��  � ��� l  U X���� r   U X��� m   U V����  � o      ���� 0 
cyclecount 
cycleCount� . (limit how many files we do on each cycle   � ��� P l i m i t   h o w   m a n y   f i l e s   w e   d o   o n   e a c h   c y c l e� ��� X   Y������ k   m�    l  m m��������  ��  ��    r   m z I   m x������ 0 	changeext 	changeExt 	 l  n q
����
 c   n q o   n o���� 0 i   m   o p��
�� 
TEXT��  ��  	 �� m   q t �  . m o v��  ��   o      ����  0 outputfilename outputFilename  r   { � I   { ������� 0 	changeext 	changeExt  l  | ���� c   |  o   | }���� 0 i   m   } ~��
�� 
TEXT��  ��   �� m    � �  _ c l e a n . m o v��  ��   o      ���� *0 outputfilenameclean outputFilenameClean  l  � ���������  ��  ��     Z   � �!"���! ?   � �#$# o   � ��~�~ 0 
cyclecount 
cycleCount$ o   � ��}�} $0 maxfilespercycle maxFilesPerCycle"  S   � ���  �    %&% l  � ��|�{�z�|  �{  �z  & '(' l  � ��y)*�y  )  check for finished file   * �++ . c h e c k   f o r   f i n i s h e d   f i l e( ,-, r   � �./. m   � ��x�x  / o      �w�w 0 	filesize1  - 010 r   � �232 m   � ��v�v 3 o      �u�u 0 	filesize2  1 454 r   � �676 m   � ��t�t  7 o      �s�s  0 filesize1clean filesize1Clean5 898 r   � �:;: m   � ��r�r ; o      �q�q  0 filesize2clean filesize2Clean9 <=< l  � ��p�o�n�p  �o  �n  = >?> r   � �@A@ m   � ��m
�m boovfalsA o      �l�l 0 skipthisfile skipThisFile? BCB r   � �DED m   � ��k
�k boovfalsE o      �j�j &0 skipthisfileclean skipThisFileCleanC FGF l  � ��i�h�g�i  �h  �g  G HIH T   ��JJ k   ��KK LML I  � ��fN�e
�f .ascrcmnt****      � ****N b   � �OPO l  � �Q�d�cQ I  � ��b�a�`
�b .misccurdldt    ��� null�a  �`  �d  �c  P m   � �RR �SS $ c h e c k i n g   f i l e   s i z e�e  M TUT r   � �VWV c   � �XYX b   � �Z[Z o   � ��_�_ 0 myfolder  [ o   � ��^�^  0 outputfilename outputFilenameY m   � ��]
�] 
TEXTW o      �\�\ 
0 myfile  U \]\ r   � �^_^ l  � �`�[�Z` I  � ��Ya�X
�Y .rdwrgeofcomp       ****a 4   � ��Wb
�W 
psxfb o   � ��V�V 
0 myfile  �X  �[  �Z  _ o      �U�U 0 	filesize1  ] cdc I  � ��Te�S
�T .ascrcmnt****      � ****e b   � �fgf b   � �hih l  � �j�R�Qj I  � ��P�O�N
�P .misccurdldt    ��� null�O  �N  �R  �Q  i m   � �kk �ll  f i l e   s i z e   =  g o   � ��M�M 0 	filesize1  �S  d mnm Z   �op�L�Ko ?   � �qrq o   � ��J�J 0 	filesize1  r o   � ��I�I >0 filesizecheck_minaeoutputsize filesizeCheck_minAEOutputSizep k   � ss tut I  � ��Hv�G
�H .ascrcmnt****      � ****v m   � �ww �xx * f i l e s i z e 1   >   c h e c k s i z e�G  u y�Fy I  � �Ez�D
�E .sysodelanull��� ��� nmbrz m   � ��C�C �D  �F  �L  �K  n {|{ r  }~} l �B�A I �@��?
�@ .rdwrgeofcomp       ****� 4  �>�
�> 
psxf� o  	
�=�= 
0 myfile  �?  �B  �A  ~ o      �<�< 0 	filesize2  | ��� I !�;��:
�; .ascrcmnt****      � ****� b  ��� b  ��� l ��9�8� I �7�6�5
�7 .misccurdldt    ��� null�6  �5  �9  �8  � m  �� ���  f i l e   s i z e   =  � o  �4�4 0 	filesize2  �:  � ��� l ""�3�2�1�3  �2  �1  � ��� l ""�0���0  � % , filesizeCheck_minAEOutputSize   � ��� > ,   f i l e s i z e C h e c k _ m i n A E O u t p u t S i z e� ��� Z  "I���/�.� F  "3��� l "%��-�,� =  "%��� o  "#�+�+ 0 	filesize1  � o  #$�*�* 0 	filesize2  �-  �,  � l (/��)�(� ?  (/��� o  ()�'�' 0 	filesize2  � o  ).�&�& >0 filesizecheck_minaeoutputsize filesizeCheck_minAEOutputSize�)  �(  � k  6E�� ��� I 6C�%��$
�% .ascrcmnt****      � ****� b  6?��� l 6;��#�"� I 6;�!� �
�! .misccurdldt    ��� null�   �  �#  �"  � m  ;>�� ��� B f i l e   s i z e s   m a t c h   -   e x i t i n g   r e p e a t�$  � ���  S  DE�  �/  �.  � ��� I J]���
� .ascrcmnt****      � ****� b  JY��� b  JS��� l JO���� I JO���
� .misccurdldt    ��� null�  �  �  �  � m  OR�� ��� 4 f i l e   n o t   d o n e   -   d i f f e r e n c e� l SX���� c  SX��� l SV���� \  SV��� o  ST�� 0 	filesize2  � o  TU�� 0 	filesize1  �  �  � m  VW�
� 
TEXT�  �  �  � ��� l ^^����  �  �  � ��� Z  ^�����
� G  ^o��� A  ^e��� o  ^_�	�	 0 	filesize1  � o  _d�� >0 filesizecheck_minaeoutputsize filesizeCheck_minAEOutputSize� l hk���� A  hk��� o  hi�� 0 	filesize1  � o  ij�� 0 	filesize2  �  �  � k  r�� ��� I ry���
� .ascrcmnt****      � ****� m  ru�� ��� \ f i l e s i z e 1   <   c h e c k s i z e   o r   f i l e s i z e 1   <   f i l e s i z e 2�  � ��� l zz����  � %  still rendering, skip this one   � ��� >   s t i l l   r e n d e r i n g ,   s k i p   t h i s   o n e� ��� r  z}��� m  z{� 
�  boovtrue� o      ���� 0 skipthisfile skipThisFile� ����  S  ~��  �  �
  �  I ��� l ����������  ��  ��  � ��� l ��������  � X ROLD this was used on Netflix where 2 outputs where needed, one keyed and one clean   � ��� � O L D   t h i s   w a s   u s e d   o n   N e t f l i x   w h e r e   2   o u t p u t s   w h e r e   n e e d e d ,   o n e   k e y e d   a n d   o n e   c l e a n� ��� l  ��������  �60
		repeat			log (current date) & "checking clean file size"			set myfile to myfolder & outputFilenameClean as string			set filesize1Clean to (get eof of POSIX file myfile)			log (current date) & "Clean file size = " & filesize1Clean			if filesize1Clean > filesizeCheck_minAEOutputSize then				log "filesize1Clean > checksize"				delay 1			end if			set filesize2Clean to (get eof of POSIX file myfile)			log (current date) & "Clean file size = " & filesize2Clean						--, filesizeCheck_minAEOutputSize			if (filesize1Clean = filesize2Clean) and (filesize2 > filesizeCheck_minAEOutputSize) then				log (current date) & "file sizes match - exiting repeat"				exit repeat			end if			log (current date) & "Clean file not done - difference" & ((filesize2Clean - filesize1Clean) as string)						if filesize1Clean < filesizeCheck_minAEOutputSize or (filesize1Clean < filesize2Clean) then				log "filesize1Clean < checksize or filesize1Clean < filesize2Clean"				-- still rendering, skip this one				set skipThisFileClean to true				exit repeat			end if		end repeat						if skipThisFile is false and skipThisFileClean is false then			--move AE output file to AME watch folder			-- move XML waiver to archive						set shscript1 to "mv " & quoted form of myfolder & outputFilename & " /" & quoted form of ame_watch & outputFilename			set shscript2 to "mv " & quoted form of myfolder & outputFilenameClean & " /" & quoted form of ame_watch & outputFilenameClean			set shscript3 to "mv " & quoted form of completed_waivers & i & " " & quoted form of waivers_pending_completed_archive & i			if dev_mode is true then				display dialog shscript			end if			try				log (current date) & "moving i to AME watch folder "				do shell script shscript1				do shell script shscript2				do shell script shscript3				set cycleCount to cycleCount + 1							on error errmsg				log (current date) & "error in uploading: " & errmsg				beep				beep				beep				display alert "An error occured in uploading: " & errmsg giving up after 30				exit repeat			end try					end if		   � ���` 
 	 	 r e p e a t  	 	 	 l o g   ( c u r r e n t   d a t e )   &   " c h e c k i n g   c l e a n   f i l e   s i z e "  	 	 	 s e t   m y f i l e   t o   m y f o l d e r   &   o u t p u t F i l e n a m e C l e a n   a s   s t r i n g  	 	 	 s e t   f i l e s i z e 1 C l e a n   t o   ( g e t   e o f   o f   P O S I X   f i l e   m y f i l e )  	 	 	 l o g   ( c u r r e n t   d a t e )   &   " C l e a n   f i l e   s i z e   =   "   &   f i l e s i z e 1 C l e a n  	 	 	 i f   f i l e s i z e 1 C l e a n   >   f i l e s i z e C h e c k _ m i n A E O u t p u t S i z e   t h e n  	 	 	 	 l o g   " f i l e s i z e 1 C l e a n   >   c h e c k s i z e "  	 	 	 	 d e l a y   1  	 	 	 e n d   i f  	 	 	 s e t   f i l e s i z e 2 C l e a n   t o   ( g e t   e o f   o f   P O S I X   f i l e   m y f i l e )  	 	 	 l o g   ( c u r r e n t   d a t e )   &   " C l e a n   f i l e   s i z e   =   "   &   f i l e s i z e 2 C l e a n  	 	 	  	 	 	 - - ,   f i l e s i z e C h e c k _ m i n A E O u t p u t S i z e  	 	 	 i f   ( f i l e s i z e 1 C l e a n   =   f i l e s i z e 2 C l e a n )   a n d   ( f i l e s i z e 2   >   f i l e s i z e C h e c k _ m i n A E O u t p u t S i z e )   t h e n  	 	 	 	 l o g   ( c u r r e n t   d a t e )   &   " f i l e   s i z e s   m a t c h   -   e x i t i n g   r e p e a t "  	 	 	 	 e x i t   r e p e a t  	 	 	 e n d   i f  	 	 	 l o g   ( c u r r e n t   d a t e )   &   " C l e a n   f i l e   n o t   d o n e   -   d i f f e r e n c e "   &   ( ( f i l e s i z e 2 C l e a n   -   f i l e s i z e 1 C l e a n )   a s   s t r i n g )  	 	 	  	 	 	 i f   f i l e s i z e 1 C l e a n   <   f i l e s i z e C h e c k _ m i n A E O u t p u t S i z e   o r   ( f i l e s i z e 1 C l e a n   <   f i l e s i z e 2 C l e a n )   t h e n  	 	 	 	 l o g   " f i l e s i z e 1 C l e a n   <   c h e c k s i z e   o r   f i l e s i z e 1 C l e a n   <   f i l e s i z e 2 C l e a n "  	 	 	 	 - -   s t i l l   r e n d e r i n g ,   s k i p   t h i s   o n e  	 	 	 	 s e t   s k i p T h i s F i l e C l e a n   t o   t r u e  	 	 	 	 e x i t   r e p e a t  	 	 	 e n d   i f  	 	 e n d   r e p e a t  	 	  	 	  	 	 i f   s k i p T h i s F i l e   i s   f a l s e   a n d   s k i p T h i s F i l e C l e a n   i s   f a l s e   t h e n  	 	 	 - - m o v e   A E   o u t p u t   f i l e   t o   A M E   w a t c h   f o l d e r  	 	 	 - -   m o v e   X M L   w a i v e r   t o   a r c h i v e  	 	 	  	 	 	 s e t   s h s c r i p t 1   t o   " m v   "   &   q u o t e d   f o r m   o f   m y f o l d e r   &   o u t p u t F i l e n a m e   &   "   / "   &   q u o t e d   f o r m   o f   a m e _ w a t c h   &   o u t p u t F i l e n a m e  	 	 	 s e t   s h s c r i p t 2   t o   " m v   "   &   q u o t e d   f o r m   o f   m y f o l d e r   &   o u t p u t F i l e n a m e C l e a n   &   "   / "   &   q u o t e d   f o r m   o f   a m e _ w a t c h   &   o u t p u t F i l e n a m e C l e a n  	 	 	 s e t   s h s c r i p t 3   t o   " m v   "   &   q u o t e d   f o r m   o f   c o m p l e t e d _ w a i v e r s   &   i   &   "   "   &   q u o t e d   f o r m   o f   w a i v e r s _ p e n d i n g _ c o m p l e t e d _ a r c h i v e   &   i  	 	 	 i f   d e v _ m o d e   i s   t r u e   t h e n  	 	 	 	 d i s p l a y   d i a l o g   s h s c r i p t  	 	 	 e n d   i f  	 	 	 t r y  	 	 	 	 l o g   ( c u r r e n t   d a t e )   &   " m o v i n g   i   t o   A M E   w a t c h   f o l d e r   "  	 	 	 	 d o   s h e l l   s c r i p t   s h s c r i p t 1  	 	 	 	 d o   s h e l l   s c r i p t   s h s c r i p t 2  	 	 	 	 d o   s h e l l   s c r i p t   s h s c r i p t 3  	 	 	 	 s e t   c y c l e C o u n t   t o   c y c l e C o u n t   +   1  	 	 	 	  	 	 	 o n   e r r o r   e r r m s g  	 	 	 	 l o g   ( c u r r e n t   d a t e )   &   " e r r o r   i n   u p l o a d i n g :   "   &   e r r m s g  	 	 	 	 b e e p  	 	 	 	 b e e p  	 	 	 	 b e e p  	 	 	 	 d i s p l a y   a l e r t   " A n   e r r o r   o c c u r e d   i n   u p l o a d i n g :   "   &   e r r m s g   g i v i n g   u p   a f t e r   3 0  	 	 	 	 e x i t   r e p e a t  	 	 	 e n d   t r y  	 	 	  	 	 e n d   i f  	 	� ���� l ����������  ��  ��  ��  �� 0 i  � o   \ ]���� 0 filelist  � ��� I �������
�� .ascrcmnt****      � ****� b  ����� l �������� I ��������
�� .misccurdldt    ��� null��  ��  ��  ��  � m  ���� ��� * d o n e   w i t h   t r a n s c o d i n g��  � ���� l ����������  ��  ��  ��  � ��� l     ��������  ��  ��  � ��� l     ��������  ��  ��  � ��� l     ��������  ��  ��  � ��� l     ��������  ��  ��  � ��� i   � ���� I      �������� 0 handlewaivers handleWaivers��  ��  � k    W�� ��� I    �����
�� .ascrcmnt****      � ****� b     ��� l    ������ I    ������
�� .misccurdldt    ��� null��  ��  ��  ��  � m    �� ���   c h e c k i n g   w a i v e r s��  �    l   ��������  ��  ��    r     o    ���� .0 incomingrawxmlwaivers incomingRawXmlWaivers o      ���� 60 incomingwaiverfolderposix incomingWaiverFolderPosix  l   ��������  ��  ��   	 l   ��
��  
  assemble greps	    �  a s s e m b l e   g r e p s 		  r     b     b     b     m     �  l s   n     1    ��
�� 
strq o    ���� 60 incomingwaiverfolderposix incomingWaiverFolderPosix m     �    |   g r e p   o    ���� $0 waivergrepstring waiverGrepString o      ���� 0 shscript    l     ��������  ��  ��     Z     3!"����! =    '#$# o     %���� 0 dev_mode  $ m   % &��
�� boovtrue" I  * /��%��
�� .sysodlogaskr        TEXT% o   * +���� 0 shscript  ��  ��  ��    &'& l  4 4��������  ��  ��  ' ()( l  4 4��*+��  *  get list of waivers   + �,, & g e t   l i s t   o f   w a i v e r s) -.- l  4 4��/0��  / 2 ,grep -e Added -e Changed -e Fixed -e Deleted   0 �11 X g r e p   - e   A d d e d   - e   C h a n g e d   - e   F i x e d   - e   D e l e t e d. 232 Q   4 b4564 r   7 B787 c   7 @9:9 n   7 >;<; 2  < >��
�� 
cpar< l  7 <=����= I  7 <��>��
�� .sysoexecTEXT���     TEXT> o   7 8���� 0 shscript  ��  ��  ��  : m   > ?��
�� 
list8 o      ����  0 waiverfilelist waiverFileList5 R      ��?��
�� .ascrerr ****      � ****? o      ���� 
0 errmsg  ��  6 k   J b@@ ABA I  J U��C��
�� .ascrcmnt****      � ****C b   J QDED l  J OF����F I  J O������
�� .misccurdldt    ��� null��  ��  ��  ��  E o   O P���� 
0 errmsg  ��  B GHG I  V [��I��
�� .ascrcmnt****      � ****I m   V WJJ �KK > p r o b a b l y   b e c a u s e   n o   w a i v e r s   y e t��  H L��L r   \ bMNM c   \ `OPO J   \ ^����  P m   ^ _��
�� 
listN o      ����  0 waiverfilelist waiverFileList��  3 QRQ l  c c��������  ��  ��  R STS l  c c��UV��  U N H look for incoming source files and move them and waiver into production   V �WW �   l o o k   f o r   i n c o m i n g   s o u r c e   f i l e s   a n d   m o v e   t h e m   a n d   w a i v e r   i n t o   p r o d u c t i o nT XYX l  c fZ[\Z r   c f]^] m   c d����  ^ o      ���� 0 
cyclecount 
cycleCount[ . (limit how many files we do on each cycle   \ �__ P l i m i t   h o w   m a n y   f i l e s   w e   d o   o n   e a c h   c y c l eY `a` l  g g��������  ��  ��  a bcb X   gUd��ed k   wPff ghg Z   w �ij����i ?   w ~klk o   w x���� 0 
cyclecount 
cycleCountl o   x }���� $0 maxfilespercycle maxFilesPerCyclej  S   � ���  ��  h mnm l  � ���������  ��  ��  n opo l  � ���qr��  q G ANeed to read the waiver and see if it contains multiple raw files   r �ss � N e e d   t o   r e a d   t h e   w a i v e r   a n d   s e e   i f   i t   c o n t a i n s   m u l t i p l e   r a w   f i l e sp tut l  � ���vw��  v - ' we also have access here to PATE info    w �xx N   w e   a l s o   h a v e   a c c e s s   h e r e   t o   P A T E   i n f o  u yzy r   � �{|{ b   � �}~} o   � ����� 60 incomingwaiverfolderposix incomingWaiverFolderPosix~ o   � ����� 0 outteri outterI| o      ���� 0 
waiverfile 
waiverFilez � r   � ���� I   � �������� 0 read_waiver  � ���� o   � ����� 0 
waiverfile 
waiverFile��  ��  � o      ���� 0 mywaiver myWaiver� ��� l  � ���������  ��  ��  � ��� r   � ���� J   � ���  � o      �~�~ 0 rawfileslist rawFilesList� ��� r   � ���� J   � ��}�}  � o      �|�| 0 movefileslist moveFilesList� ��� l  � ��{�z�y�{  �z  �y  � ��� Z   � ����x�� F   � ���� l  � ���w�v� >  � ���� n   � ���� o   � ��u�u 0 waiver_total_raw_files  � o   � ��t�t 0 mywaiver myWaiver� m   � ��s
�s 
null�w  �v  � l  � ���r�q� ?   � ���� n   � ���� o   � ��p�p 0 waiver_total_raw_files  � o   � ��o�o 0 mywaiver myWaiver� m   � ��n�n  �r  �q  � k   � ��� ��� r   � ���� m   � ��m�m  � o      �l�l 0 myincrementer myIncrementer� ��k� U   � ���� k   � ��� ��� r   � ���� [   � ���� o   � ��j�j 0 myincrementer myIncrementer� m   � ��i�i � o      �h�h 0 myincrementer myIncrementer� ��� l  � ��g���g  �    does this need try/catch?   � ��� 4   d o e s   t h i s   n e e d   t r y / c a t c h ?� ��� r   � ���� I   � ��f��e�f 0 get_element  � ��� o   � ��d�d 0 
waiverfile 
waiverFile� ��� m   � ��� ���  J o b� ��c� l  � ���b�a� b   � ���� m   � ��� ���  r a w _ v i d e o _� o   � ��`�` 0 myincrementer myIncrementer�b  �a  �c  �e  � o      �_�_ 0 	myrawfile 	myRawFile� ��^� s   � ���� o   � ��]�] 0 	myrawfile 	myRawFile� l     ��\�[� n      ���  ;   � �� o   � ��Z�Z 0 rawfileslist rawFilesList�\  �[  �^  � l  � ���Y�X� n   � ���� o   � ��W�W 0 waiver_total_raw_files  � o   � ��V�V 0 mywaiver myWaiver�Y  �X  �k  �x  � k   � ��� ��� l  � ��U���U  � O I no raw files defined in waiver, infer raw filename from waiver file name   � ��� �   n o   r a w   f i l e s   d e f i n e d   i n   w a i v e r ,   i n f e r   r a w   f i l e n a m e   f r o m   w a i v e r   f i l e   n a m e� ��� l  � ��T���T  � O I just look for single raw file, we will strip .xml extension in next step   � ��� �   j u s t   l o o k   f o r   s i n g l e   r a w   f i l e ,   w e   w i l l   s t r i p   . x m l   e x t e n s i o n   i n   n e x t   s t e p� ��S� s   � ���� o   � ��R�R 0 outteri outterI� l     ��Q�P� n      ���  ;   � �� o   � ��O�O 0 rawfileslist rawFilesList�Q  �P  �S  � ��� l  � ��N�M�L�N  �M  �L  � ��� l  � ��K�J�I�K  �J  �I  � ��� l  � ��H���H  � 2 , see if all media files are ready to process   � ��� X   s e e   i f   a l l   m e d i a   f i l e s   a r e   r e a d y   t o   p r o c e s s� ��� Z   �N���G�� I   � ��F��E�F (0 checkrawfilestatus checkRawFileStatus� ��D� o   � ��C�C 0 rawfileslist rawFilesList�D  �E  � k   �J�� ��� l  � ��B���B  � 3 - files are ready, move waiver into production   � ��� Z   f i l e s   a r e   r e a d y ,   m o v e   w a i v e r   i n t o   p r o d u c t i o n� ��� r   ���� b   ���� b   �
��� b   �� � b   � m   � � �  m v   n   � 1   ��A
�A 
strq o   � ��@�@ 0 
waiverfile 
waiverFile  m   �   � o  	�?�? @0 pendingwaiversforaefolderposix pendingWaiversForAEFolderPosix� l 
	�>�=	 c  


 o  
�<�< 0 outteri outterI m  �;
�; 
TEXT�>  �=  � o      �:�: 0 shscript  �  l �9�9    display dialog shscript    � . d i s p l a y   d i a l o g   s h s c r i p t  l �8�7�6�8  �7  �6    r   m  �5
�5 boovfals o      �4�4 0 success   �3 Q  J k  )  I �2�1
�2 .sysoexecTEXT���     TEXT o  �0�0 0 shscript  �1     r   %!"! [   ##$# o   !�/�/ 0 
cyclecount 
cycleCount$ m  !"�.�. " o      �-�- 0 
cyclecount 
cycleCount  %&% l &&�,�+�*�,  �+  �*  & '�)' r  &)()( m  &'�(
�( boovtrue) o      �'�' 0 success  �)   R      �&*�%
�& .ascrerr ****      � ***** o      �$�$ 
0 errmsg  �%   k  1J++ ,-, I 1@�#.�"
�# .ascrcmnt****      � ****. b  1</0/ b  1:121 l 163�!� 3 I 16���
� .misccurdldt    ��� null�  �  �!  �   2 m  6944 �55 ` e r r o r   i n   m o v i n g   w a i v e r   f i l e   t o   w a i v e r s _ p e n d i n g :  0 o  :;�� 
0 errmsg  �"  - 6�6 I AJ�7�
� .sysodisAaleR        TEXT7 b  AF898 m  AD:: �;; ` e r r o r   i n   m o v i n g   w a i v e r   f i l e   t o   w a i v e r s _ p e n d i n g :  9 o  DE�� 
0 errmsg  �  �  �3  �G  � l MM�<=�  < 8 2 waiver not ready, we'll check again on next cycle   = �>> d   w a i v e r   n o t   r e a d y ,   w e ' l l   c h e c k   a g a i n   o n   n e x t   c y c l e� ?@? l OO����  �  �  @ ABA l OO����  �  �  B C�C l OO�DE�  D  outterI   E �FF  o u t t e r I�  �� 0 outteri outterIe o   j k��  0 waiverfilelist waiverFileListc GHG l VV����  �  �  H IJI l VV�
�	��
  �	  �  J KLK l VV����  �  �  L M�M l VV����  �  �  �  � NON l     � �����   ��  ��  O PQP l     ��������  ��  ��  Q RSR i   � �TUT I      ��V���� (0 checkrawfilestatus checkRawFileStatusV W��W o      ���� 0 rawfileslist rawFilesList��  ��  U k    )XX YZY l     ��[\��  [ * $ check if files are done transcoding   \ �]] H   c h e c k   i f   f i l e s   a r e   d o n e   t r a n s c o d i n gZ ^_^ l     ��`a��  ` E ? if they are not found, check for them in incoming media folder   a �bb ~   i f   t h e y   a r e   n o t   f o u n d ,   c h e c k   f o r   t h e m   i n   i n c o m i n g   m e d i a   f o l d e r_ cdc l     ��ef��  e 5 / returns true if all files are done transcoding   f �gg ^   r e t u r n s   t r u e   i f   a l l   f i l e s   a r e   d o n e   t r a n s c o d i n gd hih l     ��������  ��  ��  i jkj l     ��lm��  l b \ if first file is not found in transcoded, just move on to checking raw path for all of them   m �nn �   i f   f i r s t   f i l e   i s   n o t   f o u n d   i n   t r a n s c o d e d ,   j u s t   m o v e   o n   t o   c h e c k i n g   r a w   p a t h   f o r   a l l   o f   t h e mk opo Z     qr����q =    sts o     ���� 0 dev_mode  t m    ��
�� boovtruer I  
 ��u��
�� .sysodlogaskr        TEXTu b   
 vwv m   
 xx �yy  r a w F i l e s L i s t   =  w o    ���� 0 rawfileslist rawFilesList��  ��  ��  p z{z r    |}| m    ��
�� boovfals} o      ���� "0 filereadystatus fileReadyStatus{ ~~ r    ��� m    ��
�� boovfals� o      ����  0 skiptranscoded skipTranscoded ��� r    "��� J     ����  � o      ���� 0 movefileslist moveFilesList� ��� l  # #��������  ��  ��  � ��� l  # #��������  ��  ��  � ��� X   #=����� k   38�� ��� r   3 >��� I  3 <�����
�� .corecnte****       ****� n   3 8��� 2  6 8��
�� 
cha � l  3 6������ c   3 6��� o   3 4���� 0 i  � m   4 5��
�� 
TEXT��  ��  ��  � o      ���� 0 
namelength  � ��� r   ? P��� c   ? N��� l  ? L������ n   ? L��� 7 @ L����
�� 
cha � m   D F���� � l  G K������ \   G K��� o   H I���� 0 
namelength  � m   I J���� ��  ��  � o   ? @���� 0 i  ��  ��  � m   L M��
�� 
TEXT� o      ���� 
0 myname  � ��� l  Q Q��������  ��  ��  � ��� l  Q Q��������  ��  ��  � ��� l  Q Q������  � 8 2check for transcoded media for any valid extension   � ��� d c h e c k   f o r   t r a n s c o d e d   m e d i a   f o r   a n y   v a l i d   e x t e n s i o n� ��� X   Q6����� k   e1�� ��� r   e p��� c   e n��� b   e l��� b   e j��� b   e h��� m   e f�� ���  /� o   f g���� >0 transcodecompletedfolderposix transcodeCompletedFolderPosix� o   h i���� 
0 myname  � o   j k���� 0 fileextension fileExtension� m   l m��
�� 
TEXT� o      ���� $0 mytranscodedfile myTranscodedFile� ��� r   q |��� c   q z��� b   q x��� b   q v��� b   q t��� m   q r�� ���  /� o   r s���� :0 incomingrawmediafolderposix incomingRawMediaFolderPosix� o   t u���� 
0 myname  � o   v w���� 0 fileextension fileExtension� m   x y��
�� 
TEXT� o      ���� 0 	myrawfile 	myRawFile� ��� r   } ���� o   } ~���� 0 fileextension fileExtension� o      ����  0 foundextension foundExtension� ��� l  � ���������  ��  ��  � ��� Z   � �������� I   � �������� ,0 checkfilewritestatus checkFileWriteStatus� ��� o   � ����� $0 mytranscodedfile myTranscodedFile� ��� m   � ������� ���� m   � ����� ��  ��  � k   � ��� ��� r   � ���� m   � ���
�� boovtrue� o      ���� "0 filereadystatus fileReadyStatus� ��� s   � ���� c   � ���� l  � ������� b   � ���� o   � ����� 
0 myname  � o   � �����  0 foundextension foundExtension��  ��  � m   � ���
�� 
TEXT� l     ������ n      ���  ;   � �� o   � ����� 0 movefileslist moveFilesList��  ��  � ����  S   � ���  ��  ��  � ��� l  � ���������  ��  ��  � ��� l  � �������  � 4 . file not found in transcoded, check it in raw   � ��� \   f i l e   n o t   f o u n d   i n   t r a n s c o d e d ,   c h e c k   i t   i n   r a w� ��� Z   �/������� I   � �������� ,0 checkfilewritestatus checkFileWriteStatus� � � o   � ����� 0 	myrawfile 	myRawFile   m   � �����   �� m   � ����� ��  ��  � k   �+  r   � � m   � ���
�� boovfals o      ���� "0 filereadystatus fileReadyStatus 	
	 l  � �����   ( "move raw file to Transcoding chain    � D m o v e   r a w   f i l e   t o   T r a n s c o d i n g   c h a i n
  Z   � ��� E   � � o   � ����� 80 rawtranscodefileextensions rawTranscodeFileExtensions o   � �����  0 foundextension foundExtension r   � � b   � � b   � � b   � � b   � � m   � � �    m v   n   � �!"! 1   � ���
�� 
strq" o   � ����� 0 	myrawfile 	myRawFile m   � �## �$$    o   � ����� :0 transcodependingfolderposix transcodePendingFolderPosix l  � �%����% b   � �&'& o   � ����� 
0 myname  ' o   � �����  0 foundextension foundExtension��  ��   o      ���� 0 shscript   ()( E   � �*+* o   � ����� D0  bypassrawtranscodefileextensions  bypassRawTranscodeFileExtensions+ o   � �����  0 foundextension foundExtension) ,��, r   � �-.- b   � �/0/ b   � �121 b   � �343 b   � �565 m   � �77 �88  m v  6 n   � �9:9 1   � ���
�� 
strq: o   � ����� 0 	myrawfile 	myRawFile4 m   � �;; �<<   2 o   � ��� >0 transcodecompletedfolderposix transcodeCompletedFolderPosix0 l  � �=�~�}= b   � �>?> o   � ��|�| 
0 myname  ? o   � ��{�{  0 foundextension foundExtension�~  �}  . o      �z�z 0 shscript  ��  ��   @A@ r   � �BCB m   � ��y
�y boovfalsC o      �x�x 0 success  A DED Q   �)FGHF k   �II JKJ I  � ��wL�v
�w .sysoexecTEXT���     TEXTL o   � ��u�u 0 shscript  �v  K MNM r   � OPO m   � ��t
�t boovtrueP o      �s�s $0 filemove_success fileMove_successN QRQ  S  R S�rS l �q�p�o�q  �p  �o  �r  G R      �nT�m
�n .ascrerr ****      � ****T o      �l�l 
0 errmsg  �m  H k  )UU VWV I �kX�j
�k .ascrcmnt****      � ****X b  YZY b  [\[ l ]�i�h] I �g�f�e
�g .misccurdldt    ��� null�f  �e  �i  �h  \ m  ^^ �__ ` e r r o r   i n   m o v i n g   m e d i a   f i l e   t o   m e d i a _ t r a n s c o d e d :  Z o  �d�d 
0 errmsg  �j  W `a` I %�cb�b
�c .sysodisAaleR        TEXTb b  !cdc m  ee �ff ` e r r o r   i n   m o v i n g   m e d i a   f i l e   t o   m e d i a _ t r a n s c o d e d :  d o   �a�a 
0 errmsg  �b  a g�`g r  &)hih m  &'�_
�_ boovfalsi o      �^�^ $0 filemove_success fileMove_success�`  E jkj l **�]�\�[�]  �\  �[  k lml l **�Z�Y�X�Z  �Y  �X  m n�Wn l **�Vop�V  o  exists rawFile end if   p �qq * e x i s t s   r a w F i l e   e n d   i f�W  ��  ��  � r�Ur l 00�Tst�T  s  file extensions repeat   t �uu , f i l e   e x t e n s i o n s   r e p e a t�U  �� 0 fileextension fileExtension� o   T Y�S�S 00 incomingfileextensions incomingFileExtensions� vwv l 77�R�Q�P�R  �Q  �P  w xyx l 77�O�N�M�O  �N  �M  y z�Lz l 77�K{|�K  {  outter repeat in filelist   | �}} 2 o u t t e r   r e p e a t   i n   f i l e l i s t�L  �� 0 i  � o   & '�J�J 0 rawfileslist rawFilesList� ~~ l >>�I�H�G�I  �H  �G   ��� l >>�F���F  � U Oif all files are found in transcoded, check their status and move to media_pool   � ��� � i f   a l l   f i l e s   a r e   f o u n d   i n   t r a n s c o d e d ,   c h e c k   t h e i r   s t a t u s   a n d   m o v e   t o   m e d i a _ p o o l� ��� Z  >$���E�� F  >S��� = >A��� o  >?�D�D "0 filereadystatus fileReadyStatus� m  ?@�C
�C boovtrue� l DO��B�A� =  DO��� n  DI��� 1  EI�@
�@ 
leng� o  DE�?�? 0 movefileslist moveFilesList� n  IN��� 1  JN�>
�> 
leng� o  IJ�=�= 0 rawfileslist rawFilesList�B  �A  � k  V�� ��� X  V��<�� k  f��� ��� Z  f���;�:� = fm��� o  fk�9�9 0 dev_mode  � m  kl�8
�8 boovtrue� I p{�7��6
�7 .sysodlogaskr        TEXT� c  pw��� b  pu��� m  ps�� ���   m o v e F i l e s L i s t   =  � o  st�5�5 0 movefileslist moveFilesList� m  uv�4
�4 
TEXT�6  �;  �:  � ��� r  ����� c  ����� b  ����� b  ����� m  ���� ���  /� o  ���3�3 >0 transcodecompletedfolderposix transcodeCompletedFolderPosix� o  ���2�2 0 rawfilei rawFileI� m  ���1
�1 
TEXT� o      �0�0 
0 myfile  � ��� l ���/�.�-�/  �.  �-  � ��� r  ����� n  ����� 1  ���,
�, 
psxp� o  ���+�+ 
0 myfile  � o      �*�* 0 pmyfile  � ��� l ���)���)  � F @TODO refactor this to use incoming_media_ext from config file!!!   � ��� � T O D O   r e f a c t o r   t h i s   t o   u s e   i n c o m i n g _ m e d i a _ e x t   f r o m   c o n f i g   f i l e ! ! !� ��� r  ����� b  ����� b  ����� b  ����� b  ����� m  ���� ���  m v  � n  ����� 1  ���(
�( 
strq� o  ���'�' 0 pmyfile  � m  ���� ���   � o  ���&�& 0 
media_pool  � l ����%�$� o  ���#�# 0 rawfilei rawFileI�%  �$  � o      �"�" 0 shscript  � ��� r  ����� m  ���!
�! boovfals� o      � �  0 success  � ��� Z  ������� = ����� o  ���� 0 dev_mode  � m  ���
� boovtrue� I �����
� .sysodlogaskr        TEXT� o  ���� 0 shscript  �  �  �  � ��� Q  ������ k  ���� ��� I �����
� .sysoexecTEXT���     TEXT� o  ���� 0 shscript  �  � ��� r  ����� m  ���
� boovtrue� o      �� "0 filereadystatus fileReadyStatus�  � R      ���
� .ascrerr ****      � ****� o      �� 
0 errmsg  �  � k  ���� ��� I �����
� .ascrcmnt****      � ****� b  ����� b  ����� l ������ I ����
�	
� .misccurdldt    ��� null�
  �	  �  �  � m  ���� ��� T e r r o r   i n   m o v i n g   m e d i a   f i l e   t o   m e d i a _ p o o l :  � o  ���� 
0 errmsg  �  � ��� I �����
� .sysodisAaleR        TEXT� b  ����� m  ���� ��� T e r r o r   i n   m o v i n g   m e d i a   f i l e   t o   m e d i a _ p o o l :  � o  ���� 
0 errmsg  �  � ��� r  ��	 		  m  ���
� boovfals	 o      �� "0 filereadystatus fileReadyStatus� 	�	  S  ���  � 	�	 l ��� �����   ��  ��  �  �< 0 rawfilei rawFileI� o  YZ���� 0 movefileslist moveFilesList� 			 L  		 o  ���� "0 filereadystatus fileReadyStatus	 	��	 l ��������  ��  ��  ��  �E  � k  
$		 			
		 l 

��		��  	 3 - files not ready or move to media_pool failed   	 �		 Z   f i l e s   n o t   r e a d y   o r   m o v e   t o   m e d i a _ p o o l   f a i l e d	
 			 Z  
		����	 = 
			 o  
���� 0 dev_mode  	 m  ��
�� boovtrue	 I ��	��
�� .sysodlogaskr        TEXT	 m  		 �		 . N o t   a l l   f i l e s   a r e   r e a d y��  ��  ��  	 			 L   "		 m   !��
�� boovfals	 	��	 l ##��		��  	  fileReadyStatus   	 �		  f i l e R e a d y S t a t u s��  � 			 l %%��������  ��  ��  	 	 	!	  l %%��������  ��  ��  	! 	"	#	" l %%��������  ��  ��  	# 	$	%	$ L  %'	&	& o  %&���� "0 filereadystatus fileReadyStatus	% 	'	(	' l ((��������  ��  ��  	( 	)	*	) l ((��������  ��  ��  	* 	+��	+ l ((��������  ��  ��  ��  S 	,	-	, l     ��������  ��  ��  	- 	.	/	. l     ��������  ��  ��  	/ 	0	1	0 l     ��������  ��  ��  	1 	2	3	2 i   � �	4	5	4 I      ��	6���� ,0 checkfilewritestatus checkFileWriteStatus	6 	7	8	7 o      ���� 0 pmyfile  	8 	9	:	9 o      ���� "0 expectedminsize expectedMinSize	: 	;��	; o      ���� &0 checkdelayseconds checkDelaySeconds��  ��  	5 k     k	<	< 	=	>	= r     	?	@	? m     ��
�� boovfals	@ o      ����  0 thisfilestatus thisFileStatus	> 	A	B	A l   ��������  ��  ��  	B 	C	D	C r    	E	F	E m    ����  	F o      ���� 0 	filesize1  	D 	G	H	G r    	I	J	I m    	���� 	J o      ���� 0 	filesize2  	H 	K	L	K Z   	M	N����	M =   	O	P	O o    ���� "0 expectedminsize expectedMinSize	P m    ��
�� 
null	N r    	Q	R	Q m    ���� 	R o      ���� "0 expectedminsize expectedMinSize��  ��  	L 	S	T	S l   ��������  ��  ��  	T 	U	V	U Z    h	W	X��	Y	W I     ��	Z���� 0 
fileexists 
fileExists	Z 	[��	[ o    ���� 0 pmyfile  ��  ��  	X k   # c	\	\ 	]	^	] r   # -	_	`	_ l  # +	a����	a I  # +��	b��
�� .rdwrgeofcomp       ****	b 4   # '��	c
�� 
psxf	c o   % &���� 0 pmyfile  ��  ��  ��  	` o      ���� 0 	filesize1  	^ 	d	e	d Z  . B	f	g����	f l  . 9	h����	h G   . 9	i	j	i l  . 1	k����	k A   . 1	l	m	l o   . /���� 0 	filesize1  	m m   / 0���� ��  ��  	j l  4 7	n����	n A   4 7	o	p	o o   4 5���� 0 	filesize1  	p o   5 6���� "0 expectedminsize expectedMinSize��  ��  ��  ��  	g L   < >	q	q m   < =��
�� boovfals��  ��  	e 	r	s	r l  C C��������  ��  ��  	s 	t	u	t I  C H��	v��
�� .sysodelanull��� ��� nmbr	v o   C D���� &0 checkdelayseconds checkDelaySeconds��  	u 	w	x	w r   I S	y	z	y l  I Q	{����	{ I  I Q��	|��
�� .rdwrgeofcomp       ****	| 4   I M��	}
�� 
psxf	} o   K L���� 0 pmyfile  ��  ��  ��  	z o      ���� 0 	filesize2  	x 	~		~ Z   T a	�	���	�	� =   T W	�	�	� o   T U���� 0 	filesize1  	� o   U V���� 0 	filesize2  	� L   Z \	�	� m   Z [��
�� boovtrue��  	� L   _ a	�	� m   _ `��
�� boovfals	 	���	� l  b b��������  ��  ��  ��  ��  	Y k   f h	�	� 	�	�	� l  f f��	�	���  	�   file does not exist   	� �	�	� (   f i l e   d o e s   n o t   e x i s t	� 	���	� L   f h	�	� m   f g��
�� boovfals��  	V 	�	�	� l  i i��������  ��  ��  	� 	�	�	� l  i i��������  ��  ��  	� 	���	� L   i k	�	� o   i j��  0 thisfilestatus thisFileStatus��  	3 	�	�	� l     �~�}�|�~  �}  �|  	� 	�	�	� i   � �	�	�	� I      �{	��z�{ 0 gatherstats gatherStats	� 	��y	� o      �x�x 0 intervalmins intervalMins�y  �z  	� k    	�	�	� 	�	�	� l     �w	�	��w  	� &   only send stats every X minutes   	� �	�	� @   o n l y   s e n d   s t a t s   e v e r y   X   m i n u t e s	� 	�	�	� r     	�	�	� m     �v
�v boovfals	� o      �u�u 0 
initialrun 
initialRun	� 	�	�	� Z    G	�	��t	�	� ?    	�	�	� o    �s�s 0 intervalmins intervalMins	� m    �r�r  	� k   
 -	�	� 	�	�	� r   
 	�	�	� I   
 �q�p�o�q 0 getepochtime getEpochTime�p  �o  	� o      �n�n 0 timenow timeNow	� 	�	�	� r     	�	�	� I    �m	��l�m 0 number_to_string  	� 	��k	� [    	�	�	� o    �j�j 80 laststatsgatheredtimestamp lastStatsGatheredTimestamp	� l   	��i�h	� ]    	�	�	� ]    	�	�	� o    �g�g 0 intervalmins intervalMins	� m    �f�f <	� m    �e�e  ���i  �h  �k  �l  	� o      �d�d 0 	nextcheck 	nextCheck	� 	��c	� Z   ! -	�	��b�a	� ?   ! $	�	�	� o   ! "�`�` 0 	nextcheck 	nextCheck	� o   " #�_�_ 0 timenow timeNow	� L   ' )�^�^  �b  �a  �c  �t  	� k   0 G	�	� 	�	�	� l  0 0�]	�	��]  	� A ;making initial run, need to init lastStatsGatheredTimestamp   	� �	�	� v m a k i n g   i n i t i a l   r u n ,   n e e d   t o   i n i t   l a s t S t a t s G a t h e r e d T i m e s t a m p	� 	�	�	� r   0 7	�	�	� I   0 5�\�[�Z�\ 0 getepochtime getEpochTime�[  �Z  	� o      �Y�Y 80 laststatsgatheredtimestamp lastStatsGatheredTimestamp	� 	�	�	� r   8 ?	�	�	� I   8 =�X�W�V�X 0 getepochtime getEpochTime�W  �V  	� o      �U�U :0 laststalefilechecktimestamp lastStaleFileCheckTimestamp	� 	�	�	� r   @ C	�	�	� m   @ A�T
�T boovfals	� o      �S�S *0 forcestalefilecheck forceStaleFileCheck	� 	��R	� r   D G	�	�	� m   D E�Q
�Q boovtrue	� o      �P�P 0 
initialrun 
initialRun�R  	� 	�	�	� l  H H�O�N�M�O  �N  �M  	� 	�	�	� l  H H�L	�	��L  	� %  set var for our overall health   	� �	�	� >   s e t   v a r   f o r   o u r   o v e r a l l   h e a l t h	� 	�	�	� r   H K	�	�	� m   H I�K
�K boovtrue	� o      �J�J 0 ishappy isHappy	� 	�	�	� r   L O	�	�	� m   L M�I
�I boovfals	� o      �H�H 40 updatestalefiletimestamp updateStaleFileTimestamp	� 	�	�	� r   P S	�	�	� m   P Q�G�G  	� o      �F�F 0 oldestfileage oldestFileAge	� 	�	�	� r   T W	�	�	� m   T U	�	� �	�	�  	� o      �E�E 0 	dataitems 	dataItems	� 	�	�	� r   X [	�	�	� m   X Y	�	� �	�	�  	� o      �D�D 0 failedtests failedTests	� 	�	�	� r   \ _	�	�	� m   \ ]�C
�C boovfals	� o      �B�B *0 recycleaftereffects recycleAfterEffects	� 	�	�	� l  ` `�A�@�?�A  �@  �?  	� 
 

  l  ` `�>�=�<�>  �=  �<  
 


 r   ` c


 m   ` a

 �

 @ { " n a m e " : " t e s t i n g " , " v a l u e " : " f o o " }
 o      �;�; 0 	firstitem 	firstItem
 

	
 l  d d�:�9�8�:  �9  �8  
	 




 l  d d�7

�7  
   check if AE is running   
 �

 .   c h e c k   i f   A E   i s   r u n n i n g
 


 r   d l


 I   d j�6
�5�6 0 
is_running  
 
�4
 m   e f

 �

  A f t e r   E f f e c t s�4  �5  
 o      �3�3 0 ae_test  
 


 I   m y�2
�1�2 0 
writeplist 
writePlist
 


 o   n s�0�0 0 theplistpath thePListPath
 


 m   s t

 �

  a e _ t e s t
 
 �/
  o   t u�.�. 0 ae_test  �/  �1  
 
!
"
! Z   z �
#
$�-
%
# =  z }
&
'
& o   z {�,�, 0 ae_test  
' m   { |�+
�+ boovfals
$ k   � �
(
( 
)
*
) r   � �
+
,
+ m   � ��*
�* boovfals
, o      �)�) 0 ishappy isHappy
* 
-
.
- r   � �
/
0
/ b   � �
1
2
1 o   � ��(�( 0 failedtests failedTests
2 m   � �
3
3 �
4
4  a e _ s t a t u s ,  
0 o      �'�' 0 failedtests failedTests
. 
5
6
5 Z   � �
7
8�&�%
7 =  � �
9
:
9 o   � ��$�$ 0 dev_mode  
: m   � ��#
�# boovtrue
8 I  � ��"
;�!
�" .sysodlogaskr        TEXT
; m   � �
<
< �
=
= & N O T   H A P P Y   a e _ s t a t u s�!  �&  �%  
6 
>
?
> r   � �
@
A
@ m   � �
B
B �
C
C F { " n a m e " : " a e _ s t a t u s " , " v a l u e " : " D O W N " }
A o      � �  0 	firstitem 	firstItem
? 
D
E
D r   � �
F
G
F m   � ��
� boovtrue
G o      �� *0 recycleaftereffects recycleAfterEffects
E 
H�
H l  � �����  �  �  �  �-  
% r   � �
I
J
I m   � �
K
K �
L
L B { " n a m e " : " a e _ s t a t u s " , " v a l u e " : " U P " }
J o      �� 0 	firstitem 	firstItem
" 
M
N
M r   � �
O
P
O b   � �
Q
R
Q o   � ��� 0 	dataitems 	dataItems
R o   � ��� 0 	firstitem 	firstItem
P o      �� 0 	dataitems 	dataItems
N 
S
T
S l  � �����  �  �  
T 
U
V
U l  � ��
W
X�  
W   check if AE is hung up   
X �
Y
Y .   c h e c k   i f   A E   i s   h u n g   u p
V 
Z
[
Z r   � �
\
]
\ c   � �
^
_
^ I   � ����� 20 checkaftereffectsstatus checkAfterEffectsStatus�  �  
_ m   � ��
� 
list
] o      �� 00 aftereffectsresponsive afterEffectsResponsive
[ 
`
a
` Z   � �
b
c��
b =  � �
d
e
d n   � �
f
g
f 4   � ��

h
�
 
cobj
h m   � ��	�	 
g o   � ��� 00 aftereffectsresponsive afterEffectsResponsive
e m   � ��
� boovfals
c k   � �
i
i 
j
k
j l  � ��
l
m�  
l 1 + recycle it after we send the status report   
m �
n
n V   r e c y c l e   i t   a f t e r   w e   s e n d   t h e   s t a t u s   r e p o r t
k 
o
p
o r   � �
q
r
q m   � ��
� boovtrue
r o      �� *0 recycleaftereffects recycleAfterEffects
p 
s�
s r   � �
t
u
t b   � �
v
w
v o   � ��� 0 failedtests failedTests
w m   � �
x
x �
y
y 4 r e s t a r t i n g _ a f t e r _ e f f e c t s ,  
u o      �� 0 failedtests failedTests�  �  �  
a 
z
{
z l  � �� �����   ��  ��  
{ 
|
}
| l  � ���������  ��  ��  
} 
~

~ l  � ���
�
���  
� ( " check if Media Encoder is running   
� �
�
� D   c h e c k   i f   M e d i a   E n c o d e r   i s   r u n n i n g
 
�
�
� r   � �
�
�
� I   � ���
����� 0 
is_running  
� 
���
� m   � �
�
� �
�
� & A d o b e   M e d i a   E n c o d e r��  ��  
� o      ���� 0 ame_test  
� 
�
�
� I   � ���
����� 0 
writeplist 
writePlist
� 
�
�
� o   � ����� 0 theplistpath thePListPath
� 
�
�
� m   � �
�
� �
�
�  a m e _ t e s t
� 
���
� o   � ����� 0 ame_test  ��  ��  
� 
�
�
� Z   �0
�
���
�
� =  � �
�
�
� o   � ����� 0 ame_test  
� m   � ���
�� boovfals
� k  (
�
� 
�
�
� r  
�
�
� m  ��
�� boovfals
� o      ���� 0 ishappy isHappy
� 
�
�
� r  
�
�
� b  

�
�
� o  ���� 0 failedtests failedTests
� m  	
�
� �
�
�  a m e _ s t a t u s ,  
� o      ���� 0 failedtests failedTests
� 
�
�
� Z  "
�
�����
� = 
�
�
� o  ���� 0 dev_mode  
� m  ��
�� boovtrue
� I ��
���
�� .sysodlogaskr        TEXT
� m  
�
� �
�
� ( N O T   H A P P Y   a m e _ s t a t u s��  ��  ��  
� 
���
� r  #(
�
�
� m  #&
�
� �
�
� H { " n a m e " : " a m e _ s t a t u s " , " v a l u e " : " D O W N " }
� o      ���� 0 nextitem nextItem��  ��  
� r  +0
�
�
� m  +.
�
� �
�
� D { " n a m e " : " a m e _ s t a t u s " , " v a l u e " : " U P " }
� o      ���� 0 nextitem nextItem
� 
�
�
� r  1:
�
�
� b  18
�
�
� b  16
�
�
� o  12���� 0 	dataitems 	dataItems
� m  25
�
� �
�
�  ,
� o  67���� 0 nextitem nextItem
� o      ���� 0 	dataitems 	dataItems
� 
�
�
� l ;;��������  ��  ��  
� 
�
�
� l ;;��
�
���  
� "  check if Dropbox is running   
� �
�
� 8   c h e c k   i f   D r o p b o x   i s   r u n n i n g
� 
�
�
� r  ;E
�
�
� I  ;C��
����� 0 
is_running  
� 
���
� m  <?
�
� �
�
�  D r o p b o x��  ��  
� o      ���� 0 dropbox_test  
� 
�
�
� I  FT��
����� 0 
writeplist 
writePlist
� 
�
�
� o  GL���� 0 theplistpath thePListPath
� 
�
�
� m  LO
�
� �
�
�  d r o p b o x _ t e s t
� 
���
� o  OP���� 0 dropbox_test  ��  ��  
� 
�
�
� Z  U�
�
���
�
� = UX
�
�
� o  UV���� 0 dropbox_test  
� m  VW��
�� boovfals
� k  [�
�
� 
�
�
� r  [^
�
�
� m  [\��
�� boovfals
� o      ���� 0 ishappy isHappy
� 
�
�
� r  _f
�
�
� b  _d
�
�
� o  _`���� 0 failedtests failedTests
� m  `c
�
� �
�
�   d r o p b o x _ s t a t u s ,  
� o      ���� 0 failedtests failedTests
� 
�
�
� Z  g|
�
�����
� = gn
�
�
� o  gl���� 0 dev_mode  
� m  lm��
�� boovtrue
� I qx��
���
�� .sysodlogaskr        TEXT
� m  qt
�
� �
�
� 0 N O T   H A P P Y   d r o p b o x _ s t a t u s��  ��  ��  
� 
���
� r  }�
�
�
� m  }�
�
� �
�
� P { " n a m e " : " d r o p b o x _ s t a t u s " , " v a l u e " : " D O W N " }
� o      ���� 0 nextitem nextItem��  ��  
� r  ��   m  �� � L { " n a m e " : " d r o p b o x _ s t a t u s " , " v a l u e " : " U P " } o      ���� 0 nextitem nextItem
�  r  �� b  ��	 b  ��

 o  ������ 0 	dataitems 	dataItems m  �� �  ,	 o  ������ 0 nextitem nextItem o      ���� 0 	dataitems 	dataItems  l ����������  ��  ��    l ����������  ��  ��    l ������   &  check pending waivers (AE queue)    � @ c h e c k   p e n d i n g   w a i v e r s   ( A E   q u e u e )  r  �� b  �� b  �� m  �� �    l s   n  ��!"! 1  ����
�� 
strq" o  ������ @0 pendingwaiversforaefolderposix pendingWaiversForAEFolderPosix m  ��## �$$    |   g r e p   ' . x m l ' o      ���� 0 shscript   %&% Q  ��'()' r  ��*+* c  ��,-, n  ��./. 2 ����
�� 
cpar/ l ��0����0 I ����1��
�� .sysoexecTEXT���     TEXT1 o  ������ 0 shscript  ��  ��  ��  - m  ����
�� 
list+ o      ���� 0 mylist myList( R      ��2��
�� .ascrerr ****      � ****2 o      ���� 
0 errmsg  ��  ) k  ��33 454 I ����6��
�� .ascrcmnt****      � ****6 b  ��787 b  ��9:9 l ��;����; I ��������
�� .misccurdldt    ��� null��  ��  ��  ��  : m  ��<< �== 2 p r o b a b l y   n o   w a i v e r s   y e t :  8 o  ������ 
0 errmsg  ��  5 >��> r  ��?@? J  ������  @ o      ���� 0 mylist myList��  & ABA l ����������  ��  ��  B CDC r  ��EFE n  ��GHG 1  ����
�� 
lengH o  ������ 0 mylist myListF o      ���� 0 mycount myCountD IJI r  ��KLK I  ����M���� 0 	readplist 	readPlistM NON o  ������ 0 theplistpath thePListPathO PQP m  ��RR �SS   a e _ w a i v e r _ c o u n t 1Q T��T m  ����
�� 
null��  ��  L o      ���� 0 oldcount oldCountJ UVU r  �WXW \  �YZY o  � ���� 0 mycount myCountZ o   ���� 0 oldcount oldCountX o      ���� 0 ae_waiver_delta  V [\[ I  	��]���� 0 
writeplist 
writePlist] ^_^ o  
���� 0 theplistpath thePListPath_ `a` m  bb �cc  a e _ w a i v e r _ d e l t aa d��d o  ���� 0 ae_waiver_delta  ��  ��  \ efe I  *��g���� 0 
writeplist 
writePlistg hih o   ���� 0 theplistpath thePListPathi jkj m   #ll �mm   a e _ w a i v e r _ c o u n t 1k n��n o  #&���� 0 mycount myCount��  ��  f opo l ++��������  ��  ��  p qrq l ++��st��  s   see if files are moving	   t �uu 2   s e e   i f   f i l e s   a r e   m o v i n g 	r vwv r  +2xyx I  +0����~�� 0 getepochtime getEpochTime�  �~  y o      �}�} 0 timenow timeNoww z{z r  3C|}| I  3A�|~�{�| 0 number_to_string  ~ �z [  4=��� o  45�y�y :0 laststalefilechecktimestamp lastStaleFileCheckTimestamp� l 5<��x�w� ]  5<��� o  5:�v�v 00 expectedmaxprocesstime expectedMaxProcessTime� m  :;�u�u  ���x  �w  �z  �{  } o      �t�t 0 	nextcheck 	nextCheck{ ��� Z  D����s�� G  D[��� G  DQ��� l DG��r�q� A  DG��� o  DE�p�p 0 	nextcheck 	nextCheck� o  EF�o�o 0 timenow timeNow�r  �q  � l JM��n�m� = JM��� o  JK�l�l 0 
initialrun 
initialRun� m  KL�k
�k boovtrue�n  �m  � l TW��j�i� = TW��� o  TU�h�h *0 forcestalefilecheck forceStaleFileCheck� m  UV�g
�g boovtrue�j  �i  � k  ^��� ��� l ^^�f���f  �  check files   � ���  c h e c k   f i l e s� ��� r  ^m��� I  ^i�e��d�e &0 checkpendingfiles checkPendingFiles� ��� o  _b�c�c 00 pendingaewaiverlistold pendingAEWaiverListOLD� ��b� o  be�a�a 0 mylist myList�b  �d  � o      �`�` 0 isprocessing isProcessing� ��� r  nu��� o  nq�_�_ 0 mylist myList� o      �^�^ 00 pendingaewaiverlistold pendingAEWaiverListOLD� ��� Z  v����]�� = v{��� o  vy�\�\ 0 isprocessing isProcessing� m  yz�[
�[ boovfals� k  ~��� ��� r  ~���� m  ~�Z
�Z boovtrue� o      �Y�Y *0 forcestalefilecheck forceStaleFileCheck� ��� r  ����� m  ���X
�X boovfals� o      �W�W 0 ishappy isHappy� ��� r  ����� b  ����� o  ���V�V 0 failedtests failedTests� m  ���� ��� ( a e _ p e n d i n g _ w a i v e r s ,  � o      �U�U 0 failedtests failedTests� ��T� Z  �����S�R� = ����� o  ���Q�Q 0 dev_mode  � m  ���P
�P boovtrue� I ���O��N
�O .sysodlogaskr        TEXT� m  ���� ��� 8 N O T   H A P P Y   a e _ p e n d i n g _ w a i v e r s�N  �S  �R  �T  �]  � l ���M���M  �  isProcessing is true			   � ��� . i s P r o c e s s i n g   i s   t r u e 	 	 	� ��� r  ����� m  ���L
�L boovtrue� o      �K�K 40 updatestalefiletimestamp updateStaleFileTimestamp� ��J� l ���I�H�G�I  �H  �G  �J  �s  � l ���F���F  �   not time to check yet   � ��� ,   n o t   t i m e   t o   c h e c k   y e t� ��� l ���E�D�C�E  �D  �C  � ��� l ���B�A�@�B  �A  �@  � ��� r  ����� o  ���?�? 0 mycount myCount� o      �>�> 0 mydatavalue myDataValue� ��� r  ����� b  ����� b  ����� m  ���� ��� L { " n a m e " : " a e _ p e n d i n g _ w a i v e r s " , " v a l u e " : "� o  ���=�= 0 mydatavalue myDataValue� m  ���� ���  " }� o      �<�< 0 nextitem nextItem� ��� r  ����� b  ����� b  ����� o  ���;�; 0 	dataitems 	dataItems� m  ���� ���  ,� o  ���:�: 0 nextitem nextItem� o      �9�9 0 	dataitems 	dataItems� ��� l ���8�7�6�8  �7  �6  � ��� r  ����� o  ���5�5 0 ae_waiver_delta  � o      �4�4 0 mydatavalue myDataValue� ��� r  ����� b  ����� b  ����� m  ��   � F { " n a m e " : " a e _ w a i v e r _ d e l t a " , " v a l u e " : "� o  ���3�3 0 mydatavalue myDataValue� m  �� �  " }� o      �2�2 0 nextitem nextItem�  r  �� b  ��	 b  ��

 o  ���1�1 0 	dataitems 	dataItems m  �� �  ,	 o  ���0�0 0 nextitem nextItem o      �/�/ 0 	dataitems 	dataItems  l ���.�-�,�.  �-  �,    l ���+�+     get age of oldest file    � .   g e t   a g e   o f   o l d e s t   f i l e  r  � I  ���*�)�* (0 getageofoldestfile getAgeOfOldestFile  o  ���(�( @0 pendingwaiversforaefolderposix pendingWaiversForAEFolderPosix �' m  �� �  . x m l�'  �)   o      �&�& 0 mydatavalue myDataValue   Z !"�%�$! ?  #$# o  �#�# 0 mydatavalue myDataValue$ o  �"�" 0 oldestfileage oldestFileAge" r  
%&% o  
�!�! 0 mydatavalue myDataValue& o      � �  0 oldestfileage oldestFileAge�%  �$    '(' r  !)*) b  +,+ b  -.- m  // �00 B { " n a m e " : " a e _ w a i v e r _ a g e " , " v a l u e " : ". o  �� 0 mydatavalue myDataValue, m  11 �22  " }* o      �� 0 nextitem nextItem( 343 r  "+565 b  ")787 b  "'9:9 o  "#�� 0 	dataitems 	dataItems: m  #&;; �<<  ,8 o  '(�� 0 nextitem nextItem6 o      �� 0 	dataitems 	dataItems4 =>= l ,,����  �  �  > ?@? l ,,����  �  �  @ ABA l ,,�CD�  C  check for errored waivers   D �EE 2 c h e c k   f o r   e r r o r e d   w a i v e r sB FGF r  ,1HIH m  ,-��  I o      �� 0 mythreshhold myThreshholdG JKJ r  2CLML b  2ANON b  2=PQP m  25RR �SS  l s  Q n  5<TUT 1  8<�
� 
strqU o  58�� @0 pendingwaiversforaefolderposix pendingWaiversForAEFolderPosixO m  =@VV �WW ( e r r o r /   |   g r e p   ' . x m l 'M o      �� 0 shscript  K XYX l DD����  �  �  Y Z[Z Q  D�\]^\ k  Go__ `a` r  G\bcb I GX�d�

� .corecnte****       ****d c  GTefe n  GPghg 2 LP�	
�	 
cparh l GLi��i I GL�j�
� .sysoexecTEXT���     TEXTj o  GH�� 0 shscript  �  �  �  f m  PS�
� 
list�
  c o      �� 0 mycount myCounta klk I  ]m�m� � 0 
writeplist 
writePlistm non o  ^c���� 0 theplistpath thePListPatho pqp m  cfrr �ss $ a e _ e r r o r e d _ w a i v e r sq t��t o  fi���� 0 mycount myCount��  �   l u��u l nn��������  ��  ��  ��  ] R      ��v��
�� .ascrerr ****      � ****v o      ���� 
0 errmsg  ��  ^ k  w�ww xyx I w���z��
�� .ascrcmnt****      � ****z b  w�{|{ b  w�}~} l w|���� I w|������
�� .misccurdldt    ��� null��  ��  ��  ��  ~ m  |�� ��� 2 p r o b a b l y   n o   w a i v e r s   y e t :  | o  ������ 
0 errmsg  ��  y ���� r  ����� m  ������  � o      ���� 0 mycount myCount��  [ ��� l ����������  ��  ��  � ��� Z  ��������� ?  ����� o  ������ 0 mycount myCount� o  ������ 0 mythreshhold myThreshhold� k  ���� ��� r  ����� m  ����
�� boovfals� o      ���� 0 ishappy isHappy� ��� r  ����� b  ����� o  ������ 0 failedtests failedTests� m  ���� ��� " a e _ e r r o r _ w a i v e r ,  � o      ���� 0 failedtests failedTests� ��� l ����������  ��  ��  � ���� Z  ��������� = ����� o  ������ 0 dev_mode  � m  ����
�� boovtrue� I �������
�� .sysodlogaskr        TEXT� m  ���� ��� 2 N O T   H A P P Y   a e _ e r r o r _ w a i v e r��  ��  ��  ��  ��  ��  � ��� r  ����� o  ������ 0 mycount myCount� o      ���� 0 mydatavalue myDataValue� ��� r  ����� b  ����� b  ����� m  ���� ��� F { " n a m e " : " a e _ e r r o r _ w a i v e r " , " v a l u e " : "� o  ������ 0 mydatavalue myDataValue� m  ���� ���  " }� o      ���� 0 nextitem nextItem� ��� r  ����� b  ����� b  ����� o  ������ 0 	dataitems 	dataItems� m  ���� ���  ,� o  ������ 0 nextitem nextItem� o      ���� 0 	dataitems 	dataItems� ��� l ����������  ��  ��  � ��� l ����������  ��  ��  � ��� l ��������  � ) #check pending AME files (AME queue)   � ��� F c h e c k   p e n d i n g   A M E   f i l e s   ( A M E   q u e u e )� ��� r  ����� b  ����� b  ����� m  ���� ���  l s  � n  ����� 1  ����
�� 
strq� o  ������ 0 	ame_watch  � m  ���� ���    |   g r e p   ' . m o v '� o      ���� 0 shscript  � ��� Q  �%���� r  ���� c  ���� n  ����� 2 ����
�� 
cpar� l �������� I �������
�� .sysoexecTEXT���     TEXT� o  ������ 0 shscript  ��  ��  ��  � m  � ��
�� 
list� o      ���� 0 mylist myList� R      �����
�� .ascrerr ****      � ****� o      ���� 
0 errmsg  ��  � k  %�� ��� I �����
�� .ascrcmnt****      � ****� b  ��� b  ��� l ������ I ������
�� .misccurdldt    ��� null��  ��  ��  ��  � m  �� ��� 2 p r o b a b l y   n o   w a i v e r s   y e t :  � o  ���� 
0 errmsg  ��  � ���� r  %��� J  !����  � o      ���� 0 mylist myList��  � ��� l &&��������  ��  ��  � ��� r  &1��� n  &-��� 1  )-��
�� 
leng� o  &)���� 0 mylist myList� o      ���� 0 mycount myCount� ��� r  2F��� I  2B������� 0 	readplist 	readPlist� ��� o  38���� 0 theplistpath thePListPath�    m  8; �  a m e _ c o u n t _ 1 �� m  ;>��
�� 
null��  ��  � o      ���� 0 oldcount oldCount�  r  GR \  GN	
	 o  GJ���� 0 mycount myCount
 o  JM���� 0 oldcount oldCount o      ���� 0 ame_file_delta    I  Sc������ 0 
writeplist 
writePlist  o  TY���� 0 theplistpath thePListPath  m  Y\ �  a m e _ q u e u e _ d e l t a �� o  \_���� 0 ame_file_delta  ��  ��    I  dt������ 0 
writeplist 
writePlist  o  ej���� 0 theplistpath thePListPath  m  jm �  a m e _ c o u n t _ 1 �� o  mp���� 0 mycount myCount��  ��     l uu��������  ��  ��    !"! l uu��#$��  #   see if files are moving	   $ �%% 2   s e e   i f   f i l e s   a r e   m o v i n g 	" &'& r  u|()( I  uz�������� 0 getepochtime getEpochTime��  ��  ) o      ���� 0 timenow timeNow' *+* r  }�,-, I  }���.���� 0 number_to_string  . /��/ [  ~�010 o  ~���� :0 laststalefilechecktimestamp lastStaleFileCheckTimestamp1 l �2����2 ]  �343 o  ����� 00 expectedmaxprocesstime expectedMaxProcessTime4 m  ������  ����  ��  ��  ��  - o      ���� 0 	nextcheck 	nextCheck+ 565 Z  ��78��97 G  ��:;: G  ��<=< l ��>����> A  ��?@? o  ������ 0 	nextcheck 	nextCheck@ o  ������ 0 timenow timeNow��  ��  = l ��A��~A = ��BCB o  ���}�} 0 
initialrun 
initialRunC m  ���|
�| boovtrue�  �~  ; l ��D�{�zD = ��EFE o  ���y�y *0 forcestalefilecheck forceStaleFileCheckF m  ���x
�x boovtrue�{  �z  8 k  ��GG HIH l ���wJK�w  J  check files   K �LL  c h e c k   f i l e sI MNM r  ��OPO I  ���vQ�u�v &0 checkpendingfiles checkPendingFilesQ RSR o  ���t�t .0 pendingamefilelistold pendingAMEFileListOLDS T�sT o  ���r�r 0 mylist myList�s  �u  P o      �q�q 0 isprocessing isProcessingN UVU r  ��WXW o  ���p�p 0 mylist myListX o      �o�o .0 pendingamefilelistold pendingAMEFileListOLDV YZY Z  ��[\�n�m[ = ��]^] o  ���l�l 0 isprocessing isProcessing^ m  ���k
�k boovfals\ k  ��__ `a` r  ��bcb m  ���j
�j boovfalsc o      �i�i 0 ishappy isHappya ded r  ��fgf m  ���h
�h boovtrueg o      �g�g *0 forcestalefilecheck forceStaleFileChecke hih r  ��jkj b  ��lml o  ���f�f 0 failedtests failedTestsm m  ��nn �oo & a m e _ p e n d i n g _ f i l e s ,  k o      �e�e 0 failedtests failedTestsi p�dp Z  ��qr�c�bq = ��sts o  ���a�a 0 dev_mode  t m  ���`
�` boovtruer I ���_u�^
�_ .sysodlogaskr        TEXTu m  ��vv �ww 6 N O T   H A P P Y   a m e _ p e n d i n g _ f i l e s�^  �c  �b  �d  �n  �m  Z x�]x r  ��yzy m  ���\
�\ boovtruez o      �[�[ 40 updatestalefiletimestamp updateStaleFileTimestamp�]  ��  9 l ���Z{|�Z  {   not time to check   | �}} $   n o t   t i m e   t o   c h e c k6 ~~ l ���Y�X�W�Y  �X  �W   ��� r  ���� o  ���V�V 0 mycount myCount� o      �U�U 0 mydatavalue myDataValue� ��� r  ��� b  ��� b  	��� m  �� ��� J { " n a m e " : " a m e _ p e n d i n g _ f i l e s " , " v a l u e " : "� o  �T�T 0 mydatavalue myDataValue� m  	�� ���  " }� o      �S�S 0 nextitem nextItem� ��� r  ��� b  ��� b  ��� o  �R�R 0 	dataitems 	dataItems� m  �� ���  ,� o  �Q�Q 0 nextitem nextItem� o      �P�P 0 	dataitems 	dataItems� ��� l �O�N�M�O  �N  �M  � ��� r  !��� o  �L�L 0 ame_file_delta  � o      �K�K 0 mydatavalue myDataValue� ��� r  "/��� b  "-��� b  ")��� m  "%�� ��� J { " n a m e " : " a m e _ p e n d i n g _ d e l t a " , " v a l u e " : "� o  %(�J�J 0 mydatavalue myDataValue� m  ),�� ���  " }� o      �I�I 0 nextitem nextItem� ��� r  09��� b  07��� b  05��� o  01�H�H 0 	dataitems 	dataItems� m  14�� ���  ,� o  56�G�G 0 nextitem nextItem� o      �F�F 0 	dataitems 	dataItems� ��� l ::�E�D�C�E  �D  �C  � ��� l ::�B���B  �   get age of oldest file   � ��� .   g e t   a g e   o f   o l d e s t   f i l e� ��� r  :I��� I  :E�A��@�A (0 getageofoldestfile getAgeOfOldestFile� ��� o  ;>�?�? 0 	ame_watch  � ��>� m  >A�� ���  . m o v�>  �@  � o      �=�= 0 mydatavalue myDataValue� ��� Z J[���<�;� ?  JO��� o  JM�:�: 0 mydatavalue myDataValue� o  MN�9�9 0 oldestfileage oldestFileAge� r  RW��� o  RU�8�8 0 mydatavalue myDataValue� o      �7�7 0 oldestfileage oldestFileAge�<  �;  � ��� r  \i��� b  \g��� b  \c��� m  \_�� ��� @ { " n a m e " : " a m e _ f i l e _ a g e " , " v a l u e " : "� o  _b�6�6 0 mydatavalue myDataValue� m  cf�� ���  " }� o      �5�5 0 nextitem nextItem� ��� r  js��� b  jq��� b  jo��� o  jk�4�4 0 	dataitems 	dataItems� m  kn�� ���  ,� o  op�3�3 0 nextitem nextItem� o      �2�2 0 	dataitems 	dataItems� ��� l tt�1�0�/�1  �0  �/  � ��� l tt�.�-�,�.  �-  �,  � ��� l tt�+���+  � $ check pending incoming waivers   � ��� < c h e c k   p e n d i n g   i n c o m i n g   w a i v e r s� ��� r  t���� b  t���� b  t��� m  tw�� ���  l s  � n  w~��� 1  z~�*
�* 
strq� o  wz�)�) .0 incomingrawxmlwaivers incomingRawXmlWaivers� m  ��� ���    |   g r e p   ' . x m l '� o      �(�( 0 shscript  � ��� Q  ���� � r  �� c  �� n  �� 2 ���'
�' 
cpar l ���&�% I ���$�#
�$ .sysoexecTEXT���     TEXT o  ���"�" 0 shscript  �#  �&  �%   m  ���!
�! 
list o      � �  0 mylist myList� R      �	�
� .ascrerr ****      � ****	 o      �� 
0 errmsg  �    k  ��

  I ����
� .ascrcmnt****      � **** b  �� b  �� l ���� I �����
� .misccurdldt    ��� null�  �  �  �   m  �� � 2 p r o b a b l y   n o   w a i v e r s   y e t :   o  ���� 
0 errmsg  �   � r  �� J  ����   o      �� 0 mylist myList�  �  l ������  �  �    r  �� n  �� 1  ���
� 
leng o  ���� 0 mylist myList o      �� 0 mycount myCount  !  r  ��"#" I  ���$�
� 0 	readplist 	readPlist$ %&% o  ���	�	 0 theplistpath thePListPath& '(' m  ��)) �** ( d r o p b o x _ w a i v e r _ c o u n t( +�+ m  ���
� 
null�  �
  # o      �� 0 oldcount oldCount! ,-, r  ��./. \  ��010 o  ���� 0 mycount myCount1 o  ���� 0 oldcount oldCount/ o      �� 0 dropbox_waiver_delta  - 232 I  ���4�� 0 
writeplist 
writePlist4 565 o  ��� �  0 theplistpath thePListPath6 787 m  ��99 �:: ( d r o p b o x _ w a i v e r _ d e l t a8 ;��; o  ������ 0 dropbox_waiver_delta  ��  �  3 <=< I  �	��>���� 0 
writeplist 
writePlist> ?@? o  ������ 0 theplistpath thePListPath@ ABA m  �CC �DD ( d r o p b o x _ w a i v e r _ c o u n tB E��E o  ���� 0 mycount myCount��  ��  = FGF l 

��������  ��  ��  G HIH l 

��JK��  J   see if files are moving	   K �LL 2   s e e   i f   f i l e s   a r e   m o v i n g 	I MNM r  
OPO I  
�������� 0 getepochtime getEpochTime��  ��  P o      ���� 0 timenow timeNowN QRQ r  "STS I   ��U���� 0 number_to_string  U V��V [  WXW o  ���� :0 laststalefilechecktimestamp lastStaleFileCheckTimestampX l Y����Y ]  Z[Z o  ���� 00 expectedmaxprocesstime expectedMaxProcessTime[ m  ����  ����  ��  ��  ��  T o      ���� 0 	nextcheck 	nextCheckR \]\ Z  #�^_��`^ G  #:aba G  #0cdc l #&e����e A  #&fgf o  #$���� 0 	nextcheck 	nextCheckg o  $%���� 0 timenow timeNow��  ��  d l ),h����h = ),iji o  )*���� 0 
initialrun 
initialRunj m  *+��
�� boovtrue��  ��  b l 36k����k = 36lml o  34���� *0 forcestalefilecheck forceStaleFileCheckm m  45��
�� boovtrue��  ��  _ k  =�nn opo l ==��qr��  q  check files   r �ss  c h e c k   f i l e sp tut r  =Lvwv I  =H��x���� &0 checkpendingfiles checkPendingFilesx yzy o  >A���� :0 pendingdropboxwaiverlistold pendingDropboxWaiverListOLDz {��{ o  AD���� 0 mylist myList��  ��  w o      ���� 0 isprocessing isProcessingu |}| r  MT~~ o  MP���� 0 mylist myList o      ���� :0 pendingdropboxwaiverlistold pendingDropboxWaiverListOLD} ��� Z  U�������� = UZ��� o  UX���� 0 isprocessing isProcessing� m  XY��
�� boovfals� k  ]��� ��� r  ]`��� m  ]^��
�� boovfals� o      ���� 0 ishappy isHappy� ��� r  ad��� m  ab��
�� boovtrue� o      ���� *0 forcestalefilecheck forceStaleFileCheck� ��� r  el��� b  ej��� o  ef���� 0 failedtests failedTests� m  fi�� ��� " d r o p b o x _ w a i v e r s ,  � o      ���� 0 failedtests failedTests� ���� Z  m�������� = mt��� o  mr���� 0 dev_mode  � m  rs��
�� boovtrue� I w~�����
�� .sysodlogaskr        TEXT� m  wz�� ��� 2 N O T   H A P P Y   d r o p b o x _ w a i v e r s��  ��  ��  ��  ��  ��  � ���� r  ����� m  ����
�� boovtrue� o      ���� 40 updatestalefiletimestamp updateStaleFileTimestamp��  ��  ` l ��������  �   not time to check   � ��� $   n o t   t i m e   t o   c h e c k] ��� l ����������  ��  ��  � ��� r  ����� o  ������ 0 mycount myCount� o      ���� 0 mydatavalue myDataValue� ��� r  ����� b  ����� b  ����� m  ���� ��� F { " n a m e " : " d r o p b o x _ w a i v e r s " , " v a l u e " : "� o  ������ 0 mydatavalue myDataValue� m  ���� ���  " }� o      ���� 0 nextitem nextItem� ��� r  ����� b  ����� b  ����� o  ������ 0 	dataitems 	dataItems� m  ���� ���  ,� o  ������ 0 nextitem nextItem� o      ���� 0 	dataitems 	dataItems� ��� l ����������  ��  ��  � ��� r  ����� o  ������ 0 dropbox_waiver_delta  � o      ���� 0 mydatavalue myDataValue� ��� r  ����� b  ����� b  ����� m  ���� ��� P { " n a m e " : " d r o p b o x _ w a i v e r _ d e l t a " , " v a l u e " : "� o  ������ 0 mydatavalue myDataValue� m  ���� ���  " }� o      ���� 0 nextitem nextItem� ��� r  ����� b  ����� b  ����� o  ������ 0 	dataitems 	dataItems� m  ���� ���  ,� o  ������ 0 nextitem nextItem� o      ���� 0 	dataitems 	dataItems� ��� l ����������  ��  ��  � ��� l ��������  �   get age of oldest file   � ��� .   g e t   a g e   o f   o l d e s t   f i l e� ��� r  ����� I  ��������� (0 getageofoldestfile getAgeOfOldestFile� ��� o  ������ .0 incomingrawxmlwaivers incomingRawXmlWaivers� ���� m  ���� ���  . x m l��  ��  � o      ���� 0 mydatavalue myDataValue� ��� Z ��������� ?  ����� o  ������ 0 mydatavalue myDataValue� o  ������ 0 oldestfileage oldestFileAge� r  ����� o  ������ 0 mydatavalue myDataValue� o      ���� 0 oldestfileage oldestFileAge��  ��  � ��� r  ����� b  ����� b  ����� m  ���� ��� L { " n a m e " : " d r o p b o x _ w a i v e r _ a g e " , " v a l u e " : "� o  ������ 0 mydatavalue myDataValue� m  ��   �  " }� o      ���� 0 nextitem nextItem�  r  � b  � b  �	 o  � ���� 0 	dataitems 	dataItems	 m   

 �  , o  ���� 0 nextitem nextItem o      ���� 0 	dataitems 	dataItems  l 		��������  ��  ��    l 		����    get memory stats    �   g e t   m e m o r y   s t a t s  Q  	* r   I ����
�� .sysoexecTEXT���     TEXT m   � l t o p   - l   1   |   h e a d   - n   1 0   |   g r e p   P h y s M e m   |   s e d   ' s / ,   / n   / g '��   o      ���� 0 memory_stats   R      ����
�� .ascrerr ****      � **** o      ���� 
0 errmsg  ��   r  * b  & !  m  """ �## . E R R O R   R E A D I N G   M E M O R Y   -  ! o  "%���� 
0 errmsg   o      ���� 0 memory_stats   $%$ l ++��������  ��  ��  % &'& r  +2()( o  +.���� 0 memory_stats  ) o      ���� 0 mydatavalue myDataValue' *+* r  3@,-, b  3>./. b  3:010 m  3622 �33 B { " n a m e " : " m e m o r y _ s t a t u s " , " v a l u e " : "1 o  69���� 0 mydatavalue myDataValue/ m  :=44 �55  " }- o      ���� 0 nextitem nextItem+ 676 r  AJ898 b  AH:;: b  AF<=< o  AB���� 0 	dataitems 	dataItems= m  BE>> �??  ,; o  FG���� 0 nextitem nextItem9 o      �� 0 	dataitems 	dataItems7 @A@ l KK�~�}�|�~  �}  �|  A BCB l KK�{DE�{  D  get free disk stats   E �FF & g e t   f r e e   d i s k   s t a t sC GHG r  KXIJI c  KTKLK I  KP�z�y�x�z  0 getfreediskpct getFreeDiskPct�y  �x  L m  PS�w
�w 
longJ o      �v�v 0 freediskpct freeDiskPctH MNM r  YfOPO c  YbQRQ I  Y^�u�t�s�u (0 getfreediskspacegb getFreeDiskSpaceGB�t  �s  R m  ^a�r
�r 
longP o      �q�q 0 
freediskgb 
freeDiskGBN STS Z  g�UV�p�oU G  g|WXW l gnY�n�mY A  gnZ[Z o  gj�l�l 0 
freediskgb 
freeDiskGB[ m  jm�k�k �n  �m  X l qx\�j�i\ A  qx]^] o  qt�h�h 0 freediskpct freeDiskPct^ m  tw�g�g 
�j  �i  V k  �__ `a` r  �bcb m  ��f
�f boovfalsc o      �e�e 0 ishappy isHappya ded r  ��fgf b  ��hih o  ���d�d 0 failedtests failedTestsi m  ��jj �kk  d i s k _ f r e e _ g b ,  g o      �c�c 0 failedtests failedTestse l�bl Z  ��mn�a�`m = ��opo o  ���_�_ 0 dev_mode  p m  ���^
�^ boovtruen I ���]q�\
�] .sysodisAaleR        TEXTq m  ��rr �ss , N O T   H A P P Y   d i s k _ f r e e _ g b�\  �a  �`  �b  �p  �o  T tut l ���[�Z�Y�[  �Z  �Y  u vwv r  ��xyx o  ���X�X 0 
freediskgb 
freeDiskGBy o      �W�W 0 mydatavalue myDataValuew z{z r  ��|}| b  ��~~ b  ����� m  ���� ��� @ { " n a m e " : " d i s k _ f r e e _ g b " , " v a l u e " : "� o  ���V�V 0 mydatavalue myDataValue m  ���� ���  " }} o      �U�U 0 nextitem nextItem{ ��� r  ����� b  ����� b  ����� o  ���T�T 0 	dataitems 	dataItems� m  ���� ���  ,� o  ���S�S 0 nextitem nextItem� o      �R�R 0 	dataitems 	dataItems� ��� l ���Q�P�O�Q  �P  �O  � ��� r  ����� o  ���N�N 0 freediskpct freeDiskPct� o      �M�M 0 mydatavalue myDataValue� ��� r  ����� b  ����� b  ����� m  ���� ��� J { " n a m e " : " d i s k _ f r e e _ p e r c e n t " , " v a l u e " : "� o  ���L�L 0 mydatavalue myDataValue� m  ���� ���  " }� o      �K�K 0 nextitem nextItem� ��� r  ����� b  ����� b  ����� o  ���J�J 0 	dataitems 	dataItems� m  ���� ���  ,� o  ���I�I 0 nextitem nextItem� o      �H�H 0 	dataitems 	dataItems� ��� l ���G�F�E�G  �F  �E  � ��� l ���D���D  � !  get computer ID from pList   � ��� 6   g e t   c o m p u t e r   I D   f r o m   p L i s t� ��� Q  ����� r  ����� I  ���C��B�C 0 	readplist 	readPlist� ��� o  ���A�A 0 theplistpath thePListPath� ��� m  ���� ���  c o m p u t e r _ i d� ��@� m  ���� ���  m a c S t a d i u m�@  �B  � o      �?�? 0 mydatavalue myDataValue� R      �>��=
�> .ascrerr ****      � ****� o      �<�< 
0 errmsg  �=  � r  ��� b  ��� m  �� ��� 0 C o m p u t e r   I D   n o t   f o u n d   -  � o  
�;�; 
0 errmsg  � o      �:�: 0 mydatavalue myDataValue� ��� r  ��� b  ��� b  ��� m  �� ��� > { " n a m e " : " c o m p u t e r _ i d " , " v a l u e " : "� o  �9�9 0 mydatavalue myDataValue� m  �� ���  " }� o      �8�8 0 nextitem nextItem� ��� r  '��� b  %��� b  #��� o  �7�7 0 	dataitems 	dataItems� m  "�� ���  ,� o  #$�6�6 0 nextitem nextItem� o      �5�5 0 	dataitems 	dataItems� ��� l ((�4�3�2�4  �3  �2  � ��� l ((�1���1  �  get IP address   � ���  g e t   I P   a d d r e s s� ��� r  (5��� n  (1��� 1  -1�0
�0 
siip� l (-��/�.� e  (-�� I (-�-�,�+
�- .sysosigtsirr   ��� null�,  �+  �/  �.  � o      �*�* 0 mydatavalue myDataValue� ��� r  6C��� b  6A��� b  6=��� m  69�� ��� R { " n a m e " : " c o m p u t e r _ I P v 4 _ a d d r e s s " , " v a l u e " : "� o  9<�)�) 0 mydatavalue myDataValue� m  =@�� ���  " }� o      �(�( 0 nextitem nextItem� ��� r  DM��� b  DK   b  DI o  DE�'�' 0 	dataitems 	dataItems m  EH �  , o  IJ�&�& 0 nextitem nextItem� o      �%�% 0 	dataitems 	dataItems�  l NN�$�#�"�$  �#  �"   	 l NN�!� ��!  �   �  	 

 l NN����  �  �    r  NS o  NO�� 0 oldestfileage oldestFileAge o      �� 0 mydatavalue myDataValue  r  Ta b  T_ b  T[ m  TW � B { " n a m e " : " o l d e s t F i l e A g e " , " v a l u e " : " o  WZ�� 0 mydatavalue myDataValue m  [^ �  " } o      �� 0 nextitem nextItem  r  bk b  bi !  b  bg"#" o  bc�� 0 	dataitems 	dataItems# m  cf$$ �%%  ,! o  gh�� 0 nextitem nextItem o      �� 0 	dataitems 	dataItems &'& l ll����  �  �  ' ()( Z  l�*+��* ?  ls,-, o  lm�� 0 oldestfileage oldestFileAge- o  mr�� 00 oldestfileagethreshold oldestFileAgeThreshold+ k  v}.. /0/ l vv�12�  1   set isHappy to false   2 �33 *   s e t   i s H a p p y   t o   f a l s e0 4�4 r  v}565 b  v{787 o  vw�� 0 failedtests failedTests8 m  wz99 �::  o l d e s t F i l e A g e ,  6 o      �
�
 0 failedtests failedTests�  �  �  ) ;<; l ���	���	  �  �  < =>= Z  ��?@�A? = ��BCB o  ���� 0 ishappy isHappyC m  ���
� boovtrue@ k  ��DD EFE r  ��GHG m  ��II �JJ  U PH o      �� 0 mydatavalue myDataValueF K�K r  ��LML m  ���
� boovfalsM o      � �  *0 forcestalefilecheck forceStaleFileCheck�  �  A k  ��NN OPO r  ��QRQ m  ��SS �TT  D O W NR o      ���� 0 mydatavalue myDataValueP U��U Z  ��VW����V = ��XYX o  ������ 0 dev_mode  Y m  ����
�� boovtrueW I ����Z��
�� .sysodisAaleR        TEXTZ m  ��[[ �\\ " N O T   H A P P Y   O V E R A L L��  ��  ��  ��  > ]^] r  ��_`_ b  ��aba b  ��cdc m  ��ee �ff D { " n a m e " : " o v e r a l l _ s t a t u s " , " v a l u e " : "d o  ������ 0 mydatavalue myDataValueb m  ��gg �hh  " }` o      ���� 0 nextitem nextItem^ iji r  ��klk b  ��mnm b  ��opo o  ������ 0 	dataitems 	dataItemsp m  ��qq �rr  ,n o  ������ 0 nextitem nextIteml o      ���� 0 	dataitems 	dataItemsj sts l ����������  ��  ��  t uvu r  ��wxw o  ������ 0 failedtests failedTestsx o      ���� 0 mydatavalue myDataValuev yzy r  ��{|{ b  ��}~} b  ��� m  ���� ��� > { " n a m e " : " f a i l e d T e s t s " , " v a l u e " : "� o  ������ 0 mydatavalue myDataValue~ m  ���� ���  " }| o      ���� 0 nextitem nextItemz ��� r  ����� b  ����� b  ����� o  ������ 0 	dataitems 	dataItems� m  ���� ���  ,� o  ������ 0 nextitem nextItem� o      ���� 0 	dataitems 	dataItems� ��� l ����������  ��  ��  � ��� r  ����� I  ���������� 0 getepochtime getEpochTime��  ��  � o      ���� 0 mydatavalue myDataValue� ��� r  �	��� b  ����� b  ����� m  ���� ��� L { " n a m e " : " c u r r e n t _ e p o c h _ t i m e " , " v a l u e " : "� o  ������ 0 mydatavalue myDataValue� m  ���� ���  " }� o      ���� 0 nextitem nextItem� ��� r  		��� b  			��� b  		��� o  		���� 0 	dataitems 	dataItems� m  		�� ���  ,� o  		���� 0 nextitem nextItem� o      ���� 0 	dataitems 	dataItems� ��� l 		��������  ��  ��  � ��� r  		��� n  		��� 1  		��
�� 
siip� l 		������ e  		�� I 		������
�� .sysosigtsirr   ��� null��  ��  ��  ��  � o      ���� 0 mydatavalue myDataValue� ��� r  		'��� b  		%��� b  		!��� m  		�� ��� 0 { " n a m e " : " R F I D " , " v a l u e " : "� o  		 ���� 0 mydatavalue myDataValue� m  	!	$�� ���  " }� o      ���� 0 nextitem nextItem� ��� r  	(	1��� b  	(	/��� b  	(	-��� o  	(	)���� 0 	dataitems 	dataItems� m  	)	,�� ���  ,� o  	-	.���� 0 nextitem nextItem� o      ���� 0 	dataitems 	dataItems� ��� l 	2	2��������  ��  ��  � ��� Z  	2	E������� = 	2	9��� o  	2	7���� 0 dev_mode  � m  	7	8��
�� boovtrue� I 	<	A�����
�� .sysodlogaskr        TEXT� o  	<	=���� 0 	dataitems 	dataItems��  ��  ��  � ��� l 	F	F��������  ��  ��  � ��� I  	F	L������� "0 sendstatustobpc sendStatusToBPC� ���� o  	G	H���� 0 	dataitems 	dataItems��  ��  � ��� r  	M	T��� I  	M	R�������� 0 getepochtime getEpochTime��  ��  � o      ���� 80 laststatsgatheredtimestamp lastStatsGatheredTimestamp� ��� l 	U	U��������  ��  ��  � ��� Z  	U	f������� = 	U	X��� o  	U	V���� 40 updatestalefiletimestamp updateStaleFileTimestamp� m  	V	W��
�� boovtrue� r  	[	b��� I  	[	`�������� 0 getepochtime getEpochTime��  ��  � o      ���� :0 laststalefilechecktimestamp lastStaleFileCheckTimestamp��  ��  � ��� l 	g	g��������  ��  ��  � ��� Z  	g	�������� F  	g	x��� = 	g	j��� o  	g	h���� *0 recycleaftereffects recycleAfterEffects� m  	h	i��
�� boovtrue� = 	m	t��� o  	m	r���� 0 dev_mode  � m  	r	s��
�� boovfals� I  	{	��������� *0 recycleaftereffects recycleAfterEffects��  ��  ��  ��  � ��� l 	�	���������  ��  ��  � ���� l 	�	���������  ��  ��  ��  	� ��� l     ��������  ��  ��  � ��� i   � ���� I      ������� "0 sendstatustobpc sendStatusToBPC� ���� o      ���� 0 	dataitems 	dataItems��  ��  � k     ���    l     ����   [ U https://dev-api.bpcreates.com/stats/userDataCount?e=1000&type=FIRST+NAME&value=stacy    � �   h t t p s : / / d e v - a p i . b p c r e a t e s . c o m / s t a t s / u s e r D a t a C o u n t ? e = 1 0 0 0 & t y p e = F I R S T + N A M E & v a l u e = s t a c y  l     ��������  ��  ��    r     		
	 n      1    ��
�� 
siip l    ���� e      I    ������
�� .sysosigtsirr   ��� null��  ��  ��  ��  
 o      ���� 0 myip myIP  l  
 
��������  ��  ��    r   
  m   
  � 2 a p i . b p c r e a t e s . c o m / a p i / c u i o      ���� 0 
theurlbase 
theURLbase  r    $ I    ������ 0 	readplist 	readPlist  o    ���� 0 theplistpath thePListPath  m       �!!  d e v _ e n d p o i n t s "��" o    ���� 0 devendpoints devEndpoints��  ��   o      �� 0 devendpoints devEndpoints #$# r   % 2%&% c   % ,'(' o   % *�~�~ 0 devendpoints devEndpoints( m   * +�}
�} 
bool& o      �|�| 0 devendpoints devEndpoints$ )*) Z   3 J+,�{-+ =  3 :./. o   3 8�z�z 0 devendpoints devEndpoints/ m   8 9�y
�y boovtrue, r   = B010 b   = @232 m   = >44 �55  h t t p s : / / d e v -3 o   > ?�x�x 0 
theurlbase 
theURLbase1 o      �w�w 0 
theurlbase 
theURLbase�{  - r   E J676 b   E H898 m   E F:: �;;  h t t p s : / /9 o   F G�v�v 0 
theurlbase 
theURLbase7 o      �u�u 0 
theurlbase 
theURLbase* <=< l  K K�t�s�r�t  �s  �r  = >?> r   K V@A@ b   K TBCB b   K RDED b   K PFGF b   K NHIH m   K LJJ �KK0 { " a u t h " :   { " a g e n c y " :   " M a c S t a d i u m _ M o n i t o r i n g " , " a p i K e y " :   " 4 b 4 9 5 6 a d c 6 6 0 0 7 f f 9 9 0 f a 6 4 5 1 8 d 7 7 2 b 9 " } , " s u b m i s s i o n T y p e " :   " a d d " , " u i d L i s t " :   [ { " n a m e " :   " R F I D " , " v a l u e " :   "I o   L M�q�q 0 myip myIPG m   N OLL �MM x " } ] , " e v e n t C o d e " :   " 3 Q J V D " , " d i P a r a m F i l t e r " :   " r " , " d a t a I t e m s " :   [E o   P Q�p�p 0 	dataitems 	dataItemsC m   R SNN �OO  ] }A o      �o�o 0 
thepayload 
thePayload? PQP l  W W�n�m�l�n  �m  �l  Q RSR l  W W�k�j�i�k  �j  �i  S TUT r   W bVWV b   W `XYX b   W ^Z[Z b   W \\]\ m   W X^^ �__ � c u r l   - v   - H   " A c c e p t :   a p p l i c a t i o n / j s o n "   - H   " C o n t e n t - t y p e :   a p p l i c a t i o n / j s o n "   - X   P O S T   - d  ] n   X [`a` 1   Y [�h
�h 
strqa o   X Y�g�g 0 
thepayload 
thePayload[ m   \ ]bb �cc   Y o   ^ _�f�f 0 
theurlbase 
theURLbaseW o      �e�e 0 shscript  U ded l  c c�d�c�b�d  �c  �b  e fgf Z   c vhi�a�`h =  c jjkj o   c h�_�_ 0 dev_mode  k m   h i�^
�^ boovtruei I  m r�]l�\
�] .sysodlogaskr        TEXTl o   m n�[�[ 0 shscript  �\  �a  �`  g mnm l  w w�Z�Y�X�Z  �Y  �X  n opo Q   w �qrsq k   z �tt uvu r   z �wxw I  z �Wy�V
�W .sysoexecTEXT���     TEXTy o   z {�U�U 0 shscript  �V  x o      �T�T 0 	myoutcome 	myOutcomev z�Sz L   � �{{ o   � ��R�R 0 	myoutcome 	myOutcome�S  r R      �Q|�P
�Q .ascrerr ****      � ****| o      �O�O 
0 errmsg  �P  s L   � �}} o   � ��N�N 
0 errmsg  p ~�M~ l  � ��L�K�J�L  �K  �J  �M  � � l     �I�H�G�I  �H  �G  � ��� l     �F�E�D�F  �E  �D  � ��� l     �C�B�A�C  �B  �A  � ��� i   � ���� I      �@�?�>�@ 0 setvars setVars�?  �>  � k    J�� ��� Q    H���� k   >�� ��� r    	��� c    ��� J    �=�=  � m    �<
�< 
list� o      �;�; 00 pendingaewaiverlistold pendingAEWaiverListOLD� ��� r   
 ��� c   
 ��� J   
 �:�:  � m    �9
�9 
list� o      �8�8 .0 pendingamefilelistold pendingAMEFileListOLD� ��� r    ��� c    ��� J    �7�7  � m    �6
�6 
list� o      �5�5 :0 pendingdropboxwaiverlistold pendingDropboxWaiverListOLD� ��� l   �4�3�2�4  �3  �2  � ��� r    &��� I    $�1��0�1 0 	readplist 	readPlist� ��� o    �/�/ 0 theplistpath thePListPath� ��� m    �� ���  c o m p u t e r _ i d� ��.� m     �-
�- 
null�.  �0  � o      �,�, 0 computer_id  � ��� l  ' '�+�*�)�+  �*  �)  � ��� r   ' =��� I   ' 7�(��'�( 0 	readplist 	readPlist� ��� o   ( -�&�& 0 theplistpath thePListPath� ��� m   - .�� ���   c o m p u t e r U s e r N a m e� ��%� o   . 3�$�$ $0 computerusername computerUserName�%  �'  � o      �#�# $0 computerusername computerUserName� ��� l  > >�"�!� �"  �!  �   � ��� r   > V��� c   > P��� I   > N���� 0 	readplist 	readPlist� ��� o   ? D�� 0 theplistpath thePListPath� ��� m   D E�� ���  d e v _ m o d e� ��� o   E J�� 0 dev_mode  �  �  � m   N O�
� 
bool� o      �� 0 dev_mode  � ��� r   W o��� c   W i��� I   W g���� 0 	readplist 	readPlist� ��� o   X ]�� 0 theplistpath thePListPath� ��� m   ] ^�� ���  d e v _ e n d p o i n t s� ��� o   ^ c�� 0 devendpoints devEndpoints�  �  � m   g h�
� 
bool� o      �� 0 devendpoints devEndpoints� ��� l  p p����  �  �  � ��� r   p ���� c   p ~��� I   p |���� 0 	readplist 	readPlist� ��� o   q v�� 0 theplistpath thePListPath� ��� m   v w�� ���  c l e a n u p� ��� m   w x�

�
 boovtrue�  �  � m   | }�	
�	 
bool� o      �� (0 performfilecleanup performFileCleanup� ��� l  � �����  �  �  � ��� r   � ���� I   � ����� 0 	readplist 	readPlist� ��� o   � ��� 0 theplistpath thePListPath� ��� m   � ��� ���  s t a t i o n _ t y p e� ��� m   � �� 
�  
null�  �  � o      ���� 0 stationtype stationType� ��� l  � ���������  ��  ��  � ��� l  � �������  � # set this to false for testing   � �   : s e t   t h i s   t o   f a l s e   f o r   t e s t i n g�  l  � �����    set automated to true    � * s e t   a u t o m a t e d   t o   t r u e  r   � �	 c   � �

 I   � ������� 0 	readplist 	readPlist  o   � ����� 0 theplistpath thePListPath  m   � � �  a u t o m a t e d �� o   � ����� 0 	automated  ��  ��   m   � ���
�� 
bool	 o      ���� 0 	automated    l  � ���������  ��  ��    l  � �����    set loopDelaySecs to 10    � . s e t   l o o p D e l a y S e c s   t o   1 0  r   � � c   � �  I   � ���!���� 0 	readplist 	readPlist! "#" o   � ����� 0 theplistpath thePListPath# $%$ m   � �&& �''  l o o p _ d e l a y% (��( o   � ����� 0 loopdelaysecs loopDelaySecs��  ��    m   � ���
�� 
long o      ���� 0 loopdelaysecs loopDelaySecs )*) l  � ���������  ��  ��  * +,+ l  � ���-.��  -  set processDelaySecs to 5   . �// 2 s e t   p r o c e s s D e l a y S e c s   t o   5, 010 r   � �232 c   � �454 I   � ���6���� 0 	readplist 	readPlist6 787 o   � ����� 0 theplistpath thePListPath8 9:9 m   � �;; �<<  p r o c e s s _ d e l a y: =��= o   � ����� $0 processdelaysecs processDelaySecs��  ��  5 m   � ���
�� 
long3 o      ���� $0 processdelaysecs processDelaySecs1 >?> l  � ���������  ��  ��  ? @A@ l  � ���BC��  B I Cset waiver sequence, the interger to look for at end of waiver name   C �DD � s e t   w a i v e r   s e q u e n c e ,   t h e   i n t e r g e r   t o   l o o k   f o r   a t   e n d   o f   w a i v e r   n a m eA EFE r   �GHG c   � �IJI I   � ���K���� 0 	readplist 	readPlistK LML o   � ����� 0 theplistpath thePListPathM NON m   � �PP �QQ  w a i v e r _ s e q u e n c eO R��R o   � ����� $0 waivergrepstring waiverGrepString��  ��  J m   � ���
�� 
listH o      ���� $0 waivergrepstring waiverGrepStringF STS l ��������  ��  ��  T UVU l ��WX��  W 1 + set delay when checking AE output filesize   X �YY V   s e t   d e l a y   w h e n   c h e c k i n g   A E   o u t p u t   f i l e s i z eV Z[Z r  \]\ c  ^_^ I  ��`���� 0 	readplist 	readPlist` aba o  ���� 0 theplistpath thePListPathb cdc m  ee �ff $ f i l e s i z e C h e c k D e l a yd g��g o  ���� (0 filesizecheckdelay filesizeCheckDelay��  ��  _ m  ��
�� 
long] o      ���� (0 filesizecheckdelay filesizeCheckDelay[ hih l ��������  ��  ��  i jkj l ��lm��  l G A set minimum file size expected from AE output (before transcode)   m �nn �   s e t   m i n i m u m   f i l e   s i z e   e x p e c t e d   f r o m   A E   o u t p u t   ( b e f o r e   t r a n s c o d e )k opo r  ;qrq c  5sts I  1��u���� 0 	readplist 	readPlistu vwv o   %���� 0 theplistpath thePListPathw xyx m  %(zz �{{ : f i l e s i z e C h e c k _ m i n A E O u t p u t S i z ey |��| o  (-���� >0 filesizecheck_minaeoutputsize filesizeCheck_minAEOutputSize��  ��  t m  14��
�� 
longr o      ���� >0 filesizecheck_minaeoutputsize filesizeCheck_minAEOutputSizep }~} l <<��������  ��  ��  ~ � l <<��������  ��  ��  � ���� L  <>�� m  <=��
�� boovtrue��  � R      �����
�� .ascrerr ****      � ****� o      ���� 
0 errmsg  ��  � L  FH�� o  FG���� 
0 errmsg  � ���� l II��������  ��  ��  ��  � ��� l     ��������  ��  ��  � ��� l     ��������  ��  ��  � ��� l     ��������  ��  ��  � ��� i   � ���� I      �������� 0 setpaths setPaths��  ��  � k    ��� ��� Q    ����� k   ��� ��� r    ��� I    ������� 0 	readplist 	readPlist� ��� o    	���� 0 theplistpath thePListPath� ��� m   	 
�� ���  d r o p b o x _ p a t h� ���� m   
 �� ��� v / U s e r s / b l u e p i x e l / D r o p b o x / p r e i n g e s t i o n _ p r o c e s s e s / v i d e o b o o t h /��  ��  � o      ���� "0 dropboxrootpath dropboxRootPath� ��� Z    +������ =   ��� o    ���� 0 devendpoints devEndpoints� m    ��
�� boovtrue� r    #��� b    !��� b    ��� o    ���� "0 dropboxrootpath dropboxRootPath� m    �� ���  d e v _� o     ���� 0 stationtype stationType� o      ���� "0 dropboxrootpath dropboxRootPath��  � r   & +��� b   & )��� o   & '���� "0 dropboxrootpath dropboxRootPath� o   ' (���� 0 stationtype stationType� o      ���� "0 dropboxrootpath dropboxRootPath� ��� l  , ,�������  ��  �  � ��� r   , 1��� b   , /��� o   , -�� "0 dropboxrootpath dropboxRootPath� m   - .�� ���  / w a i v e r s /� o      �� .0 incomingrawxmlwaivers incomingRawXmlWaivers� ��� r   2 7��� b   2 5��� o   2 3�� "0 dropboxrootpath dropboxRootPath� m   3 4�� ���  / m e d i a /� o      �� :0 incomingrawmediafolderposix incomingRawMediaFolderPosix� ��� l  8 8����  �  �  � ��� r   8 C��� b   8 A��� b   8 ?��� m   8 9�� ���  / U s e r s /� o   9 >�� $0 computerusername computerUserName� m   ? @�� ��� : / D o c u m e n t s / p e n d i n g _ t r a n s c o d e /� o      �� :0 transcodependingfolderposix transcodePendingFolderPosix� ��� Q   D Z���� r   G O��� I   G M���� 0 folderexists folderExists� ��� o   H I�� :0 transcodependingfolderposix transcodePendingFolderPosix�  �  � o      �� 0 filetest fileTest� R      ���
� .ascrerr ****      � ****� o      �~�~ 
0 errmsg  �  � r   W Z��� m   W X�}
�} boovtrue� o      �|�| 0 filetest fileTest� ��� Z   [ r���{�z� =  [ ^��� o   [ \�y�y 0 filetest fileTest� m   \ ]�x
�x boovfals� I  a n�w��v
�w .sysoexecTEXT���     TEXT� b   a j��� m   a d�� ���  m k d i r  � n   d i��� 1   e i�u
�u 
strq� o   d e�t�t :0 transcodependingfolderposix transcodePendingFolderPosix�v  �{  �z  � ��� l  s s�s�r�q�s  �r  �q  � ��� r   s ���� b   s ���� b   s |��� m   s v�� ���  / U s e r s /� o   v {�p�p $0 computerusername computerUserName� m   | �� ��� 8 / D o c u m e n t s / m e d i a _ t r a n s c o d e d /� o      �o�o >0 transcodecompletedfolderposix transcodeCompletedFolderPosix� � � Q   � � r   � � I   � ��n�m�n 0 folderexists folderExists �l o   � ��k�k >0 transcodecompletedfolderposix transcodeCompletedFolderPosix�l  �m   o      �j�j 0 filetest fileTest R      �i�h
�i .ascrerr ****      � **** o      �g�g 
0 errmsg  �h   r   � �	
	 m   � ��f
�f boovtrue
 o      �e�e 0 filetest fileTest   Z   � ��d�c =  � � o   � ��b�b 0 filetest fileTest m   � ��a
�a boovfals I  � ��`�_
�` .sysoexecTEXT���     TEXT b   � � m   � � �  m k d i r   n   � � 1   � ��^
�^ 
strq o   � ��]�] >0 transcodecompletedfolderposix transcodeCompletedFolderPosix�_  �d  �c    l  � ��\�[�Z�\  �[  �Z    r   � � b   � � b   � � !  m   � �"" �##  / U s e r s /! o   � ��Y�Y $0 computerusername computerUserName m   � �$$ �%% 6 / D o c u m e n t s / w a i v e r s _ p e n d i n g / o      �X�X @0 pendingwaiversforaefolderposix pendingWaiversForAEFolderPosix &'& Q   � �()*( r   � �+,+ I   � ��W-�V�W 0 folderexists folderExists- .�U. o   � ��T�T @0 pendingwaiversforaefolderposix pendingWaiversForAEFolderPosix�U  �V  , o      �S�S 0 filetest fileTest) R      �R/�Q
�R .ascrerr ****      � ****/ o      �P�P 
0 errmsg  �Q  * r   � �010 m   � ��O
�O boovtrue1 o      �N�N 0 filetest fileTest' 232 Z   � �45�M�L4 =  � �676 o   � ��K�K 0 filetest fileTest7 m   � ��J
�J boovfals5 I  � ��I8�H
�I .sysoexecTEXT���     TEXT8 b   � �9:9 m   � �;; �<<  m k d i r  : n   � �=>= 1   � ��G
�G 
strq> o   � ��F�F @0 pendingwaiversforaefolderposix pendingWaiversForAEFolderPosix�H  �M  �L  3 ?@? l  � ��E�D�C�E  �D  �C  @ ABA r   �CDC b   �EFE o   � �B�B @0 pendingwaiversforaefolderposix pendingWaiversForAEFolderPosixF m   GG �HH $ c o m p l e t e d _ a r c h i v e /D o      �A�A %0 !waivers_pending_completed_archive  B IJI Q  	!KLMK r  NON I  �@P�?�@ 0 folderexists folderExistsP Q�>Q o  �=�= %0 !waivers_pending_completed_archive  �>  �?  O o      �<�< 0 filetest fileTestL R      �;R�:
�; .ascrerr ****      � ****R o      �9�9 
0 errmsg  �:  M r  !STS m  �8
�8 boovtrueT o      �7�7 0 filetest fileTestJ UVU Z  ";WX�6�5W = "%YZY o  "#�4�4 0 filetest fileTestZ m  #$�3
�3 boovfalsX I (7�2[�1
�2 .sysoexecTEXT���     TEXT[ b  (3\]\ m  (+^^ �__  m k d i r  ] n  +2`a` 1  .2�0
�0 
strqa o  +.�/�/ %0 !waivers_pending_completed_archive  �1  �6  �5  V bcb l <<�.�-�,�.  �-  �,  c ded r  <Mfgf b  <Ihih b  <Ejkj m  <?ll �mm  / U s e r s /k o  ?D�+�+ $0 computerusername computerUserNamei m  EHnn �oo , / D o c u m e n t s / m e d i a _ p o o l /g o      �*�* 0 
media_pool  e pqp Q  Nfrstr r  Q[uvu I  QY�)w�(�) 0 folderexists folderExistsw x�'x o  RU�&�& 0 
media_pool  �'  �(  v o      �%�% 0 filetest fileTests R      �$�#�"
�$ .ascrerr ****      � ****�#  �"  t r  cfyzy m  cd�!
�! boovtruez o      � �  0 filetest fileTestq {|{ Z  g�}~��} = gj� o  gh�� 0 filetest fileTest� m  hi�
� boovfals~ I m|���
� .sysoexecTEXT���     TEXT� b  mx��� m  mp�� ���  m k d i r  � n  pw��� 1  sw�
� 
strq� o  ps�� 0 
media_pool  �  �  �  | ��� l ������  �  �  � ��� r  ����� b  ����� b  ����� m  ���� ���  / U s e r s /� o  ���� $0 computerusername computerUserName� m  ���� ��� 8 / D o c u m e n t s / a m e _ w a t c h / O u t p u t /� o      �� 0 processed_media  � ��� Q  ������ r  ����� I  ������ 0 folderexists folderExists� ��� o  ���� 0 processed_media  �  �  � o      �� 0 filetest fileTest� R      ���
� .ascrerr ****      � ****�  �  � r  ����� m  ���

�
 boovtrue� o      �	�	 0 filetest fileTest� ��� Z  ������� = ����� o  ���� 0 filetest fileTest� m  ���
� boovfals� I �����
� .sysoexecTEXT���     TEXT� b  ����� m  ���� ���  m k d i r  � n  ����� 1  ���
� 
strq� o  ���� 0 processed_media  �  �  �  � ��� l ��� �����   ��  ��  � ��� r  ����� b  ����� b  ����� m  ���� ���  / U s e r s /� o  ������ $0 computerusername computerUserName� m  ���� ��� * / D o c u m e n t s / a e _ o u t p u t /� o      ���� 0 	ae_output  � ��� Q  ������ r  ����� I  ��������� 0 folderexists folderExists� ���� o  ������ 0 	ae_output  ��  ��  � o      ���� 0 filetest fileTest� R      ������
�� .ascrerr ****      � ****��  ��  � r  ����� m  ����
�� boovtrue� o      ���� 0 filetest fileTest� ��� Z  �
������� = ����� o  ������ 0 filetest fileTest� m  ����
�� boovfals� I ������
�� .sysoexecTEXT���     TEXT� b  ���� m  ���� ���  m k d i r  � n  ���� 1  ���
�� 
strq� o  ������ 0 	ae_output  ��  ��  ��  � ��� l ��������  ��  ��  � ��� r  ��� b  ��� b  ��� m  �� ���  / U s e r s /� o  ���� $0 computerusername computerUserName� m  �� ��� * / D o c u m e n t s / a m e _ w a t c h /� o      ���� 0 	ame_watch  � ��� Q  5���� r   *��� I   (������� 0 folderexists folderExists� ���� o  !$���� 0 	ame_watch  ��  ��  � o      ���� 0 filetest fileTest� R      ������
�� .ascrerr ****      � ****��  ��  � r  25��� m  23��
�� boovtrue� o      ���� 0 filetest fileTest� ��� Z  6O������� = 69��� o  67���� 0 filetest fileTest� m  78��
�� boovfals� I <K�����
�� .sysoexecTEXT���     TEXT� b  <G��� m  <?�� ���  m k d i r  � n  ?F��� 1  BF��
�� 
strq� o  ?B���� 0 	ame_watch  ��  ��  ��  � ��� l PP��������  ��  ��  � ��� r  PY��� b  PU   o  PQ���� "0 dropboxrootpath dropboxRootPath m  QT �  / i n g e s t o r /� o      ���� 0 spock_ingestor  �  Q  Zr r  ]g	
	 I  ]e������ 0 folderexists folderExists �� o  ^a���� 0 spock_ingestor  ��  ��  
 o      ���� 0 filetest fileTest R      ������
�� .ascrerr ****      � ****��  ��   r  or m  op��
�� boovtrue o      ���� 0 filetest fileTest  Z  s����� = sv o  st���� 0 filetest fileTest m  tu��
�� boovfals I y���
� .sysoexecTEXT���     TEXT b  y� m  y| �  m k d i r   n  |� 1  ��
� 
strq o  |�� 0 spock_ingestor  �  ��  ��    l ������  �  �   � L  �� m  ���
� boovtrue�  � R      � �
� .ascrerr ****      � ****  o      �� 
0 errmsg  �  � L  ��!! o  ���� 
0 errmsg  � "�" l ������  �  �  �  � #$# l     ����  �  �  $ %&% l     ����  �  �  & '(' l     �)*�  ) C =#############################################################   * �++ z # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #( ,-, l     �./�  . % ## --HELPER & UTILITY METHODS--   / �00 > # #   - - H E L P E R   &   U T I L I T Y   M E T H O D S - -- 121 l     �34�  3 % ## --HELPER & UTILITY METHODS--   4 �55 > # #   - - H E L P E R   &   U T I L I T Y   M E T H O D S - -2 676 l     �89�  8 C =#############################################################   9 �:: z # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #7 ;<; l     ����  �  �  < =>= i   � �?@? I      �A�� 0 read_waiver  A B�B o      �� 0 	thewaiver  �  �  @ k     �CC DED l     �FG�  F  get waiver details   G �HH $ g e t   w a i v e r   d e t a i l sE IJI r     
KLK I     �M�� 0 get_element  M NON o    �� 0 	thewaiver  O PQP m    RR �SS  J o bQ T�T m    UU �VV  i d�  �  L o      �� 0 	waiver_id  J WXW r    YZY I    �[�� 0 get_element  [ \]\ o    �� 0 	thewaiver  ] ^_^ m    `` �aa  J o b_ b�b m    cc �dd  c r e a t e d�  �  Z o      �� 0 waiver_created  X efe l   ����  �  �  f ghg r     iji I    �k�� 0 get_element  k lml o    �� 0 	thewaiver  m non m    pp �qq  J o bo r�r m    ss �tt  p r o g r a m�  �  j o      �� 0 waiver_program  h uvu r   ! +wxw I   ! )�y�� 0 get_element  y z{z o   " #�� 0 	thewaiver  { |}| m   # $~~ �  J o b} ��� m   $ %�� ���  a c t i v a t i o n�  �  x o      �� 0 waiver_activation  v ��� r   , 6��� I   , 4���� 0 get_element  � ��� o   - .�� 0 	thewaiver  � ��� m   . /�� ���  J o b� ��� m   / 0�� ���  t e a m�  �  � o      �� 0 waiver_team  � ��� r   7 A��� I   7 ?���� 0 get_element  � ��� o   8 9�~�~ 0 	thewaiver  � ��� m   9 :�� ���  J o b� ��}� m   : ;�� ��� 
 e v e n t�}  �  � o      �|�| 0 waiver_event  � ��� Q   B Z���� r   E O��� I   E M�{��z�{ 0 get_element  � ��� o   F G�y�y 0 	thewaiver  � ��� m   G H�� ���  J o b� ��x� m   H I�� ���  t o t a l _ r a w _ f i l e s�x  �z  � o      �w�w 0 waiver_total_raw_files  � R      �v�u�t
�v .ascrerr ****      � ****�u  �t  � r   W Z��� m   W X�s�s � o      �r�r 0 waiver_total_raw_files  � ��� l  [ [�q�p�o�q  �p  �o  � ��� l  [ [�n�m�l�n  �m  �l  � ��� r   [ }��� K   [ {�� �k���k 0 	waiver_id  � o   ^ _�j�j 0 	waiver_id  � �i���i 0 waiver_created  � o   b c�h�h 0 waiver_created  � �g���g 0 waiver_program  � o   f g�f�f 0 waiver_program  � �e���e 0 waiver_activation  � o   j k�d�d 0 waiver_activation  � �c���c 0 waiver_team  � o   n o�b�b 0 waiver_team  � �a���a 0 waiver_event  � o   r s�`�` 0 waiver_event  � �_��^�_ 0 waiver_total_raw_files  � o   v w�]�] 0 waiver_total_raw_files  �^  � o      �\�\ 0 waiver_data  � ��� l  ~ ~�[�Z�Y�[  �Z  �Y  � ��� l  ~ ~�X�W�V�X  �W  �V  � ��� L   ~ ��� o   ~ �U�U 0 waiver_data  � ��T� l  � ��S�R�Q�S  �R  �Q  �T  > ��� l     �P�O�N�P  �O  �N  � ��� i   � ���� I      �M��L�M 0 get_element  � ��� o      �K�K 0 	thewaiver  � ��� o      �J�J 0 node  � ��I� o      �H�H 0 element_name  �I  �L  � Q     =���� O    ��� k    �� ��� l   �G���G  �  get top level   � ���  g e t   t o p   l e v e l� ��� r    ��� n    ��� 5    �F��E
�F 
xmle� o    �D�D 0 node  
�E kfrmname� n    ��� 1    �C
�C 
pcnt� 4    �B�
�B 
xmlf� o   	 
�A�A 0 	thewaiver  � o      �@�@ 0 xmldata  � ��?� r    ��� n    ��� 1    �>
�> 
valL� n    ��� 5    �=��<
�= 
xmle� o    �;�; 0 element_name  
�< kfrmname� o    �:�: 0 xmldata  � o      �9
�9 
ret �?  � m    ���                                                                                  sevs  alis    P  Fusion                         BD ����System Events.app                                              ����            ����  
 cu             CoreServices  0/:System:Library:CoreServices:System Events.app/  $  S y s t e m   E v e n t s . a p p    F u s i o n  -System/Library/CoreServices/System Events.app   / ��  � R      �8��7
�8 .ascrerr ****      � ****� o      �6�6 
0 errmsg  �7  � k   & =�� ��� Z   & 9���5�4� =  & -   o   & +�3�3 0 dev_mode   m   + ,�2
�2 boovtrue� I  0 5�1�0
�1 .sysodisAaleR        TEXT o   0 1�/�/ 
0 errmsg  �0  �5  �4  � �. r   : = m   : ; �   o      �-
�- 
ret �.  � 	 l     �,�+�*�,  �+  �*  	 

 l     �)�(�'�)  �(  �'    i   � � I      �&�%�& 0 
fileexists 
fileExists �$ o      �#�# 0 thefile theFile�$  �%   l     O      Z    �" I   �!� 
�! .coredoexnull���     **** 4    �
� 
file o    �� 0 thefile theFile�    L     m    �
� boovtrue�"   L     m    �
� boovfals m     �                                                                                  sevs  alis    P  Fusion                         BD ����System Events.app                                              ����            ����  
 cu             CoreServices  0/:System:Library:CoreServices:System Events.app/  $  S y s t e m   E v e n t s . a p p    F u s i o n  -System/Library/CoreServices/System Events.app   / ��     (String) as Boolean    � (   ( S t r i n g )   a s   B o o l e a n  !  l     ����  �  �  ! "#" i   � �$%$ I      �&�� 0 folderexists folderExists& '�' o      �� 0 thefile theFile�  �  % l    ()*( O     +,+ Z    -.�/- I   �0�
� .coredoexnull���     ****0 4    �1
� 
cfol1 o    �� 0 thefile theFile�  . L    22 m    �
� boovtrue�  / L    33 m    �
� boovfals, m     44�                                                                                  sevs  alis    P  Fusion                         BD ����System Events.app                                              ����            ����  
 cu             CoreServices  0/:System:Library:CoreServices:System Events.app/  $  S y s t e m   E v e n t s . a p p    F u s i o n  -System/Library/CoreServices/System Events.app   / ��  )   (String) as Boolean   * �55 (   ( S t r i n g )   a s   B o o l e a n# 676 l     ����  �  �  7 898 i   � �:;: I      �
<�	�
 0 	changeext 	changeExt< =>= o      �� 0 filename  > ?�? o      �� 0 new_ext  �  �	  ; k      @@ ABA r     CDC \     	EFE l    G��G I    �H�
� .corecnte****       ****H n     IJI 2   �
� 
cha J o     � �  0 filename  �  �  �  F m    ���� D o      ���� 0 mylength  B KLK r    MNM b    OPO l   Q����Q c    RSR l   T����T n    UVU 7  ��WX
�� 
cha W m    ���� X o    ���� 0 mylength  V o    ���� 0 filename  ��  ��  S m    ��
�� 
TEXT��  ��  P o    ���� 0 new_ext  N o      ���� 0 	newstring  L Y��Y L     ZZ o    ���� 0 	newstring  ��  9 [\[ l     ��������  ��  ��  \ ]^] i   � �_`_ I      ��������  0 getfreediskpct getFreeDiskPct��  ��  ` k     aa bcb r     ded I     ��f���� 0 trimline trimLinef ghg n    iji o    ���� 0 capacity  j I    �������� 0 getdiskstats getDiskStats��  ��  h klk m    	mm �nn  %l o��o m   	 
���� ��  ��  e o      ���� 0 rawpct rawPctc pqp l   ��rs��  r . ( returns % used, so invert it for % free   s �tt P   r e t u r n s   %   u s e d ,   s o   i n v e r t   i t   f o r   %   f r e eq u��u L    vv \    wxw m    ���� dx o    ���� 0 rawpct rawPct��  ^ yzy l     ��������  ��  ��  z {|{ i   � �}~} I      �������� (0 getfreediskspacegb getFreeDiskSpaceGB��  ��  ~ L     	 n     ��� o    ���� 0 	available  � I     �������� 0 getdiskstats getDiskStats��  ��  | ��� l     ��������  ��  ��  � ��� i   � ���� I      �������� 0 getdiskstats getDiskStats��  ��  � k     ��� ��� r     ��� n    ��� 1    ��
�� 
txdl� 1     ��
�� 
ascr� o      ���� 0 old_delimts  � ��� Q    ������ k   	 ��� ��� l  	 	��������  ��  ��  � ��� r   	 ��� I  	 �����
�� .sysoexecTEXT���     TEXT� m   	 
�� ���  d f   - h l g��  � o      ���� 0 memory_stats  � ��� r    ��� n    ��� 4    ���
�� 
cpar� m    ���� � o    ���� 0 memory_stats  � o      ���� 0 	rawstring  � ��� l   ����  � ( " set freeDisk to item 1 of theList   � ��� D   s e t   f r e e D i s k   t o   i t e m   1   o f   t h e L i s t� ��� l   ����  � , & set diskCapacity to item 1 of theList   � ��� L   s e t   d i s k C a p a c i t y   t o   i t e m   1   o f   t h e L i s t� ��� l   ����  � 2 , set diskFreePercentage to item 2 of theList   � ��� X   s e t   d i s k F r e e P e r c e n t a g e   t o   i t e m   2   o f   t h e L i s t� ��� r    ��� m    �� ���   � n     ��� 1    �
� 
txdl� 1    �
� 
ascr� ��� r    #��� n    !��� 2   !�
� 
citm� o    �� 0 	rawstring  � o      �� 0 thelist theList� ��� r   $ )��� o   $ %�� 0 old_delimts  � n     ��� 1   & (�
� 
txdl� 1   % &�
� 
ascr� ��� l  * *����  �  �  � ��� r   * .��� J   * ,��  � o      �� 0 newlist newList� ��� X   / X���� k   ? S�� ��� l  ? ?����  �  display dialog i   � ���   d i s p l a y   d i a l o g   i� ��� Z   ? Q����� >  ? D��� l  ? B���� n   ? B��� m   @ B�
� 
ctxt� o   ? @�� 0 i  �  �  � m   B C�� ���  � s   G M��� l  G J���� n   G J��� m   H J�
� 
ctxt� o   G H�� 0 i  �  �  � l     ���� n      ���  ;   K L� o   J K�� 0 newlist newList�  �  �  �  � ��� l  R R����  �  �  �  � 0 i  � o   2 3�� 0 thelist theList� ��� l  Y Y����  �  �  � ��� r   Y ���� K   Y ��� ���� 0 diskname  � l  Z `���� n   Z `��� m   ^ `�
� 
ctxt� n   Z ^��� 4   [ ^��
� 
cobj� m   \ ]�� � o   Z [�� 0 newlist newList�  �  � ���� 0 
onegblocks 
OneGblocks� l  a g���� n   a g��� m   e g�
� 
ctxt� n   a e��� 4   b e� 
� 
cobj  m   c d�� � o   a b�� 0 newlist newList�  �  � �� 0 used   l  h n�� n   h n m   l n�
� 
ctxt n   h l 4   i l�
� 
cobj m   j k��  o   h i�� 0 newlist newList�  �   �	
� 0 	available  	 l  o w�� n   o w m   u w�
� 
ctxt n   o u 4   p u�~
�~ 
cobj m   q t�}�}  o   o p�|�| 0 newlist newList�  �  
 �{�z�{ 0 capacity   l  z ��y�x n   z � m   � ��w
�w 
ctxt n   z � 4   { ��v
�v 
cobj m   | �u�u  o   z {�t�t 0 newlist newList�y  �x  �z  � o      �s�s 0 	finallist 	finalList�  � R      �r�q
�r .ascrerr ****      � **** o      �p�p 
0 errmsg  �q  ��  �  l  � ��o�n�m�o  �n  �m    r   � � o   � ��l�l 0 old_delimts   n       1   � ��k
�k 
txdl  1   � ��j
�j 
ascr !"! l  � ��i�h�g�i  �h  �g  " #$# L   � �%% o   � ��f�f 0 	finallist 	finalList$ &'& l  � ��e�d�c�e  �d  �c  ' (�b( l  � ��a�`�_�a  �`  �_  �b  � )*) l     �^�]�\�^  �]  �\  * +,+ i   � �-.- I      �[/�Z�[ 0 
is_running  / 0�Y0 o      �X�X 0 appname appName�Y  �Z  . k     :11 232 O     454 r    676 l   	8�W�V8 n    	9:9 1    	�U
�U 
pnam: 2    �T
�T 
prcs�W  �V  7 o      �S�S "0 listofprocesses listOfProcesses5 m     ;;�                                                                                  sevs  alis    P  Fusion                         BD ����System Events.app                                              ����            ����  
 cu             CoreServices  0/:System:Library:CoreServices:System Events.app/  $  S y s t e m   E v e n t s . a p p    F u s i o n  -System/Library/CoreServices/System Events.app   / ��  3 <=< l   �R�Q�P�R  �Q  �P  = >?> r    @A@ m    �O
�O boovfalsA o      �N�N 0 appisrunning appIsRunning? BCB X    5D�MED Z   ! 0FG�L�KF E   ! $HIH o   ! "�J�J 0 thisitem thisItemI o   " #�I�I 0 appname appNameG k   ' ,JJ KLK r   ' *MNM m   ' (�H
�H boovtrueN o      �G�G 0 appisrunning appIsRunningL O�FO  S   + ,�F  �L  �K  �M 0 thisitem thisItemE o    �E�E "0 listofprocesses listOfProcessesC PQP L   6 8RR o   6 7�D�D 0 appisrunning appIsRunningQ S�CS l  9 9�B�A�@�B  �A  �@  �C  , TUT l     �?�>�=�?  �>  �=  U VWV i   � �XYX I      �<Z�;�< 0 	readplist 	readPlistZ [\[ o      �:�: 0 theplistpath thePListPath\ ]^] o      �9�9 0 plistvar plistVar^ _�8_ o      �7�7 0 defaultvalue defaultValue�8  �;  Y Q     2`ab` k    cc ded r    
fgf b    hih b    jkj m    ll �mm    - c   ' p r i n t  k o    �6�6 0 plistvar plistVari m    nn �oo  'g o      �5�5 0 	mycommand 	myCommande pqp r    rsr b    tut b    vwv m    xx �yy 2 / u s r / l i b e x e c / P l i s t B u d d y    w o    �4�4 0 theplistpath thePListPathu o    �3�3 0 	mycommand 	myCommands o      �2�2 0 myscript myScriptq z�1z I   �0{�/
�0 .sysoexecTEXT���     TEXT{ o    �.�. 0 myscript myScript�/  �1  a R      �-|�,
�- .ascrerr ****      � ****| o      �+�+ 
0 errmsg  �,  b k     2}} ~~ Z     /���*�)� =    #��� o     !�(�( 0 defaultvalue defaultValue� m   ! "�'
�' 
null� I  & +�&��%
�& .sysodisAaleR        TEXT� o   & '�$�$ 
0 errmsg  �%  �*  �)   ��#� L   0 2�� o   0 1�"�" 0 defaultvalue defaultValue�#  W ��� l     �!� ��!  �   �  � ��� l     ����  �  �  � ��� l     ����  �  �  � ��� l     ����  �  �  � ��� i   � ���� I      ���� 0 
writeplist 
writePlist� ��� o      �� 0 theplistpath thePListPath� ��� o      �� 0 plistvar plistVar� ��� o      �� 0 thevalue theValue�  �  � Q     #���� k    �� ��� r    ��� b    ��� b    
��� b    ��� b    ��� m    �� ���    - c   ' s e t  � o    �� 0 plistvar plistVar� m    �� ���   � o    	�� 0 thevalue theValue� m   
 �� ���  '� o      �� 0 	mycommand 	myCommand� ��� r    ��� b    ��� b    ��� m    �� ��� 2 / u s r / l i b e x e c / P l i s t B u d d y    � o    �� 0 theplistpath thePListPath� o    �
�
 0 	mycommand 	myCommand� o      �	�	 0 myscript myScript�  � R      ���
� .ascrerr ****      � ****� o      �� 
0 errmsg  �  � I   #���
� .sysodisAaleR        TEXT� o    �� 
0 errmsg  �  � ��� l     ��� �  �  �   � ��� l     ��������  ��  ��  � ��� i   � ���� I      �������� 0 getepochtime getEpochTime��  ��  � k     �� ��� r     	��� c     ��� l    ������ I    �����
�� .sysoexecTEXT���     TEXT� m     �� ��� \ p e r l   - e   ' u s e   T i m e : : H i R e s   q w ( t i m e ) ;   p r i n t   t i m e '��  ��  ��  � m    ��
�� 
TEXT� o      ���� 
0 mytime  � ��� r   
 ��� ]   
 ��� o   
 ���� 
0 mytime  � m    ����  ��� o      ���� 
0 mytime  � ���� L    �� I    ������� 0 number_to_string  � ���� o    ���� 
0 mytime  ��  ��  ��  � ��� l     ��������  ��  ��  � ��� i   � ���� I      ������� 0 number_to_string  � ���� o      ���� 0 this_number  ��  ��  � k     ��� ��� r     ��� c     ��� o     ���� 0 this_number  � m    ��
�� 
TEXT� o      ���� 0 this_number  � ���� Z    ������� E    	��� o    ���� 0 this_number  � m    �� ���  E +� k    ��� ��� r    ��� l   ������ I   �����
�� .sysooffslong    ��� null��  � ����
�� 
psof� m    �� ���  .� �����
�� 
psin� o    ���� 0 this_number  ��  ��  ��  � o      ���� 0 x  � ��� r    #��� l   !������ I   !�����
�� .sysooffslong    ��� null��  � �� 
�� 
psof  m     �  + ����
�� 
psin o    ���� 0 this_number  ��  ��  ��  � o      ���� 0 y  �  r   $ / l  $ -	����	 I  $ -����

�� .sysooffslong    ��� null��  
 ��
�� 
psof m   & ' �  E ����
�� 
psin o   ( )���� 0 this_number  ��  ��  ��   o      ���� 0 z    r   0 E c   0 C c   0 A n   0 ? 7  1 ?��
�� 
cha  l  5 ;���� \   5 ; o   6 7���� 0 y   l  7 :�� n   7 : !  1   8 :�
� 
leng! o   7 8�� 0 this_number  �  �  ��  ��   l 	 < >"��" m   < >�����  �   o   0 1�� 0 this_number   m   ? @�
� 
TEXT m   A B�
� 
nmbr l     #��# o      �� 0 decimal_adjust  �  �   $%$ Z   F c&'�(& >  F I)*) o   F G�� 0 x  * m   G H��  ' r   L ]+,+ c   L [-.- n   L Y/0/ 7  M Y�12
� 
cha 1 m   Q S�� 2 l  T X3��3 \   T X454 o   U V�� 0 x  5 m   V W�� �  �  0 o   L M�� 0 this_number  . m   Y Z�
� 
TEXT, l     6��6 o      �� 0 
first_part  �  �  �  ( r   ` c787 m   ` a99 �::  8 l     ;��; o      �� 0 
first_part  �  �  % <=< r   d w>?> c   d u@A@ n   d sBCB 7  e s�DE
� 
cha D l  i mF��F [   i mGHG o   j k�� 0 x  H m   k l�� �  �  E l  n rI��I \   n rJKJ o   o p�� 0 z  K m   p q�� �  �  C o   d e�� 0 this_number  A m   s t�
� 
TEXT? l     L��L o      �� 0 second_part  �  �  = MNM r   x {OPO l  x yQ��Q o   x y�� 0 
first_part  �  �  P l     R��R o      �� 0 converted_number  �  �  N STS Y   | �U�VW�U Q   � �XYZX r   � �[\[ b   � �]^] l 	 � �_��_ l  � �`��` o   � ��� 0 converted_number  �  �  �  �  ^ n   � �aba 4   � ��c
� 
cha c o   � ��� 0 i  b l  � �d��d o   � ��� 0 second_part  �  �  \ l     e��e o      �� 0 converted_number  �  �  Y R      �~�}�|
�~ .ascrerr ****      � ****�}  �|  Z r   � �fgf b   � �hih l  � �j�{�zj o   � ��y�y 0 converted_number  �{  �z  i m   � �kk �ll  0g l     m�x�wm o      �v�v 0 converted_number  �x  �w  � 0 i  V m    ��u�u W l  � �n�t�sn o   � ��r�r 0 decimal_adjust  �t  �s  �  T o�qo L   � �pp l  � �q�p�oq o   � ��n�n 0 converted_number  �p  �o  �q  ��  � L   � �rr o   � ��m�m 0 this_number  ��  � sts l     �l�k�j�l  �k  �j  t uvu i   � �wxw I      �iy�h�i 0 add_leading_zeros  y z{z o      �g�g 0 this_number  { |�f| o      �e�e 0 max_leading_zeros  �f  �h  x k     G}} ~~ r     ��� c     ��� l    ��d�c� a     ��� m     �b�b 
� o    �a�a 0 max_leading_zeros  �d  �c  � m    �`
�` 
long� l     ��_�^� o      �]�] 0 threshold_number  �_  �^   ��\� Z    G���[�� A   ��� o    	�Z�Z 0 this_number  � l  	 
��Y�X� o   	 
�W�W 0 threshold_number  �Y  �X  � k    @�� ��� r    ��� m    �� ���  � l     ��V�U� o      �T�T 0 leading_zeros  �V  �U  � ��� r    ��� l   ��S�R� n    ��� 1    �Q
�Q 
leng� l   ��P�O� c    ��� l   ��N�M� _    ��� o    �L�L 0 this_number  � m    �K�K �N  �M  � m    �J
�J 
TEXT�P  �O  �S  �R  � l     ��I�H� o      �G�G 0 digit_count  �I  �H  � ��� r    #��� \    !��� l   ��F�E� [    ��� o    �D�D 0 max_leading_zeros  � m    �C�C �F  �E  � o     �B�B 0 digit_count  � l     ��A�@� o      �?�? 0 character_count  �A  �@  � ��� U   $ 7��� r   + 2��� c   + 0��� l  + .��>�=� b   + .��� l  + ,��<�;� o   + ,�:�: 0 leading_zeros  �<  �;  � m   , -�� ���  0�>  �=  � m   . /�9
�9 
TEXT� l     ��8�7� o      �6�6 0 leading_zeros  �8  �7  � o   ' (�5�5 0 character_count  � ��4� L   8 @�� c   8 ?��� l  8 =��3�2� b   8 =��� o   8 9�1�1 0 leading_zeros  � l  9 <��0�/� c   9 <��� o   9 :�.�. 0 this_number  � m   : ;�-
�- 
ctxt�0  �/  �3  �2  � m   = >�,
�, 
TEXT�4  �[  � L   C G�� c   C F��� o   C D�+�+ 0 this_number  � m   D E�*
�* 
ctxt�\  v ��� l     �)�(�'�)  �(  �'  � ��� l     �&�%�$�&  �%  �$  � ��� i   � ���� I      �#��"�# 00 roundandtruncatenumber roundAndTruncateNumber� ��� o      �!�! 0 	thenumber 	theNumber� �� � o      �� .0 numberofdecimalplaces numberOfDecimalPlaces�   �"  � k     ��� ��� Z     ����� =    ��� o     �� .0 numberofdecimalplaces numberOfDecimalPlaces� m    ��  � k    �� ��� r    ��� [    	��� o    �� 0 	thenumber 	theNumber� m    �� ?�      � o      �� 0 	thenumber 	theNumber� ��� L    �� I    ���� 0 number_to_string  � ��� _    ��� o    �� 0 	thenumber 	theNumber� m    �� �  �  �  �  �  � ��� l   ����  �  �  � ��� r    ��� m    �� ���  5� o      �� $0 theroundingvalue theRoundingValue� ��� U    /��� r   % *��� b   % (��� m   % &�� ���  0� o   & '�� $0 theroundingvalue theRoundingValue� o      �� $0 theroundingvalue theRoundingValue� o   ! "�� .0 numberofdecimalplaces numberOfDecimalPlaces� ��� r   0 7   c   0 5 l  0 3��
 b   0 3 m   0 1 �  . o   1 2�	�	 $0 theroundingvalue theRoundingValue�  �
   m   3 4�
� 
nmbr o      �� $0 theroundingvalue theRoundingValue� 	
	 l  8 8����  �  �  
  r   8 = [   8 ; o   8 9�� 0 	thenumber 	theNumber o   9 :�� $0 theroundingvalue theRoundingValue o      �� 0 	thenumber 	theNumber  l  > >� �����   ��  ��    r   > A m   > ? �  1 o      ���� 0 themodvalue theModValue  U   B U r   K P b   K N  m   K L!! �""  0  o   L M���� 0 themodvalue theModValue o      ���� 0 themodvalue theModValue \   E H#$# o   E F���� .0 numberofdecimalplaces numberOfDecimalPlaces$ m   F G����  %&% r   V ]'(' c   V [)*) l  V Y+����+ b   V Y,-, m   V W.. �//  .- o   W X���� 0 themodvalue theModValue��  ��  * m   Y Z��
�� 
nmbr( o      ���� 0 themodvalue theModValue& 010 l  ^ ^��������  ��  ��  1 232 r   ^ e454 _   ^ c676 l  ^ a8����8 `   ^ a9:9 o   ^ _���� 0 	thenumber 	theNumber: m   _ `���� ��  ��  7 o   a b���� 0 themodvalue theModValue5 o      ���� 0 thesecondpart theSecondPart3 ;<; Z   f �=>����= A  f m?@? n   f kABA 1   i k��
�� 
lengB l  f iC����C c   f iDED o   f g���� 0 thesecondpart theSecondPartE m   g h��
�� 
ctxt��  ��  @ o   k l���� .0 numberofdecimalplaces numberOfDecimalPlaces> U   p �FGF r   } �HIH c   } �JKJ l  } �L����L b   } �MNM m   } ~OO �PP  0N o   ~ ���� 0 thesecondpart theSecondPart��  ��  K m   � ���
�� 
TEXTI o      ���� 0 thesecondpart theSecondPartG \   s zQRQ o   s t���� .0 numberofdecimalplaces numberOfDecimalPlacesR l  t yS����S l  t yT����T n   t yUVU 1   w y��
�� 
lengV l  t wW����W c   t wXYX o   t u���� 0 thesecondpart theSecondPartY m   u v��
�� 
ctxt��  ��  ��  ��  ��  ��  ��  ��  < Z[Z l  � ���������  ��  ��  [ \]\ r   � �^_^ _   � �`a` o   � ����� 0 	thenumber 	theNumbera m   � ����� _ o      ���� 0 thefirstpart theFirstPart] bcb r   � �ded I   � ���f���� 0 number_to_string  f g��g o   � ����� 0 thefirstpart theFirstPart��  ��  e o      ���� 0 thefirstpart theFirstPartc hih r   � �jkj l  � �l����l b   � �mnm b   � �opo o   � ����� 0 thefirstpart theFirstPartp m   � �qq �rr  .n o   � ����� 0 thesecondpart theSecondPart��  ��  k o      ���� 0 	thenumber 	theNumberi sts l  � ���������  ��  ��  t u��u L   � �vv o   � ��� 0 	thenumber 	theNumber��  � wxw l     ����  �  �  x yzy l     ����  �  �  z {|{ i   � �}~} I      ��� 0 trimline trimLine ��� o      �� 0 	this_text  � ��� o      �� 0 
trim_chars  � ��� o      �� 0 trim_indicator  �  �  ~ k     {�� ��� l     ����  � ' ! 0 = beginning, 1 = end, 2 = both   � ��� B   0   =   b e g i n n i n g ,   1   =   e n d ,   2   =   b o t h� ��� r     ��� l    ���� n     ��� 1    �
� 
leng� l    ���� o     �� 0 
trim_chars  �  �  �  �  � o      �� 0 x  � ��� l   ����  �   TRIM BEGINNING   � ���    T R I M   B E G I N N I N G� ��� Z    >����� E   ��� J    
�� ��� m    ��  � ��� m    �� �  � l  
 ���� o   
 �� 0 trim_indicator  �  �  � V    :��� Q    5���� r    +��� c    )��� n    '��� 7   '���
� 
cha � l   #���� [    #��� o     !�� 0 x  � m   ! "�� �  �  � m   $ &����� o    �� 0 	this_text  � m   ' (�
� 
TEXT� o      �� 0 	this_text  � R      ���
� .ascrerr ****      � ****�  �  � k   3 5�� ��� l  3 3����  � 8 2 the text contains nothing but the trim characters   � ��� d   t h e   t e x t   c o n t a i n s   n o t h i n g   b u t   t h e   t r i m   c h a r a c t e r s� ��� L   3 5�� m   3 4�� ���  �  � C   ��� o    �� 0 	this_text  � l   ���� o    �� 0 
trim_chars  �  �  �  �  � ��� l  ? ?������  �   TRIM ENDING   � ���    T R I M   E N D I N G� ��� Z   ? x������� E  ? E��� J   ? C�� ��� m   ? @���� � ���� m   @ A���� ��  � l  C D������ o   C D���� 0 trim_indicator  ��  ��  � V   H t��� Q   P o���� r   S e��� c   S c��� n   S a��� 7  T a����
�� 
cha � m   X Z���� � d   [ `�� l  \ _������ [   \ _��� o   \ ]���� 0 x  � m   ] ^���� ��  ��  � o   S T���� 0 	this_text  � m   a b�
� 
TEXT� o      �~�~ 0 	this_text  � R      �}�|�{
�} .ascrerr ****      � ****�|  �{  � k   m o�� ��� l  m m�z���z  � 8 2 the text contains nothing but the trim characters   � ��� d   t h e   t e x t   c o n t a i n s   n o t h i n g   b u t   t h e   t r i m   c h a r a c t e r s� ��y� L   m o�� m   m n�� ���  �y  � D   L O��� o   L M�x�x 0 	this_text  � l  M N��w�v� o   M N�u�u 0 
trim_chars  �w  �v  ��  ��  � ��t� L   y {�� o   y z�s�s 0 	this_text  �t  | ��� l     �r�q�p�r  �q  �p  � ��� l     �o�n�m�o  �n  �m  � ��� i   � ���� I      �l��k�l (0 getageofoldestfile getAgeOfOldestFile� ��� o      �j�j 0 filepath filePath� ��i� o      �h�h 0 fileextension fileExtension�i  �k  � k     �� ��� r     	� � l    �g�f I     �e�d�e 0 getoldestfile getOldestFile  o    �c�c 0 filepath filePath �b o    �a�a 0 fileextension fileExtension�b  �d  �g  �f    o      �`�` 
0 myfile  � �_ Z   
 �^	 >  
 

 o   
 �]�] 
0 myfile   m     �   L     I    �\�[�\ 0 getageoffile getAgeOfFile �Z b     o    �Y�Y 0 filepath filePath o    �X�X 
0 myfile  �Z  �[  �^  	 L     m    �W�W  �_  �  l     �V�U�T�V  �U  �T    i   � � I      �S�R�S 0 getageoffile getAgeOfFile �Q o      �P�P 
0 myfile  �Q  �R   k     "  l     �O �O   8 2 returns age based on modification time in seconds     �!! d   r e t u r n s   a g e   b a s e d   o n   m o d i f i c a t i o n   t i m e   i n   s e c o n d s "#" l     �N$%�N  $ � } set shscript2 to "echo $(($(date +%s) - $(stat -t %s -f %m -- /Users/Ben/documents/waivers_pending/" & getOldestFile & ")))"   % �&& �   s e t   s h s c r i p t 2   t o   " e c h o   $ ( ( $ ( d a t e   + % s )   -   $ ( s t a t   - t   % s   - f   % m   - -   / U s e r s / B e n / d o c u m e n t s / w a i v e r s _ p e n d i n g / "   &   g e t O l d e s t F i l e   &   " ) ) ) "# '(' r     )*) b     +,+ b     -.- m     // �00 X e c h o   $ ( ( $ ( d a t e   + % s )   -   $ ( s t a t   - t   % s   - f   % m   - -  . o    �M�M 
0 myfile  , m    11 �22  ) ) )* o      �L�L 0 shscript  ( 343 Q    5675 r    898 c    :;: l   <�K�J< I   �I=�H
�I .sysoexecTEXT���     TEXT= o    �G�G 0 shscript  �H  �K  �J  ; m    �F
�F 
long9 o      �E�E  0 fileageseconds fileAgeSeconds6 R      �D>�C
�D .ascrerr ****      � ****> o      �B�B 
0 errmsg  �C  7 r    ?@? m    �A
�A 
null@ o      �@�@  0 fileageseconds fileAgeSeconds4 A�?A L     "BB o     !�>�>  0 fileageseconds fileAgeSeconds�?   CDC l     �=�<�;�=  �<  �;  D EFE i   � �GHG I      �:I�9�: 0 getoldestfile getOldestFileI JKJ o      �8�8 0 filepath filePathK L�7L o      �6�6 0 fileextension fileExtension�7  �9  H k     &MM NON l     �5PQ�5  P c ] set shscript to "ls -tr /Users/Ben/documents/waivers_pending/ | grep '-e *.xml' | head -n 1"   Q �RR �   s e t   s h s c r i p t   t o   " l s   - t r   / U s e r s / B e n / d o c u m e n t s / w a i v e r s _ p e n d i n g /   |   g r e p   ' - e   * . x m l '   |   h e a d   - n   1 "O STS r     UVU b     WXW b     	YZY b     [\[ b     ]^] m     __ �``  l s   - t r  ^ n    aba 1    �4
�4 
strqb o    �3�3 0 filepath filePath\ m    cc �dd    |   g r e p   ' - e   *Z o    �2�2 0 fileextension fileExtensionX m   	 
ee �ff  '   |   h e a d   - n   1V o      �1�1 0 shscript  T ghg Q    #ijki r    lml I   �0n�/
�0 .sysoexecTEXT���     TEXTn o    �.�. 0 shscript  �/  m o      �-�- 0 
oldestfile 
oldestFilej R      �,o�+
�, .ascrerr ****      � ****o o      �*�* 
0 errmsg  �+  k r     #pqp m     !rr �ss  q o      �)�) 0 
oldestfile 
oldestFileh t�(t L   $ &uu o   $ %�'�' 0 
oldestfile 
oldestFile�(  F vwv l     �&�%�$�&  �%  �$  w xyx l     �#�"�!�#  �"  �!  y z{z i   � �|}| I      � ~��  60 launchaftereffectsproject launchAfterEffectsProject~ � o      �� $0 projectfileposix projectFilePOSIX�  �  } k     �� ��� l     ����  � ) # open file with default application   � ��� F   o p e n   f i l e   w i t h   d e f a u l t   a p p l i c a t i o n� ��� l     ����  �  �  � ��� r     ��� b     ��� m     �� ��� 
 o p e n  � n    ��� 1    �
� 
strq� o    �� $0 projectfileposix projectFilePOSIX� o      �� 0 shscript  � ��� Q    ���� I   ���
� .sysoexecTEXT���     TEXT� o    �� 0 shscript  �  � R      ���
� .ascrerr ****      � ****� o      �� 
0 errmsg  �  � l   ����  �  
 something   � ���    s o m e t h i n g� ��� l   ����  �  �  �  { ��� l     �
�	��
  �	  �  � ��� i   � ���� I      ���� 00 archiveaftereffectslog archiveAfterEffectsLog�  �  � k     c�� ��� r     ��� I     ���� 0 getepochtime getEpochTime�  �  � o      �� 0 	timestamp  � ��� l   � ���   � 1 +TODO improve to work on different user path   � ��� V T O D O   i m p r o v e   t o   w o r k   o n   d i f f e r e n t   u s e r   p a t h� ��� r    ��� b    ��� b    ��� m    	�� ��� ^ / U s e r s / b l u e p i x e l / D o c u m e n t s / w a i v e r s _ p e n d i n g / l o g /� I   	 �������� 0 getepochtime getEpochTime��  ��  � m    �� ���  . t x t� o      ���� "0 archivefilepath archiveFilePath� ��� r    !��� b    ��� b    ��� b    ��� m    �� ���  m v  � o    ���� *0 aftereffectslogfile afterEffectsLogFile� m    �� ���   � o    ���� "0 archivefilepath archiveFilePath� o      ���� 0 shscript  � ��� r   " -��� b   " +��� m   " #�� ���  t o u c h  � n   # *��� 1   ( *��
�� 
strq� o   # (���� *0 aftereffectslogfile afterEffectsLogFile� o      ���� 0 	shscript2  � ��� Q   . G���� I  1 6�����
�� .sysoexecTEXT���     TEXT� o   1 2���� 0 shscript  ��  � R      �����
�� .ascrerr ****      � ****� o      ���� 
0 errmsg  ��  � k   > G�� ��� I   > E������� $0 shownotification showNotification� ��� m   ? @�� ��� $ E r r o r   A r c h v i n g   L o g� ���� o   @ A���� 
0 errmsg  ��  ��  � ���� l  F F��������  ��  ��  ��  � ��� l  H H��������  ��  ��  � ��� Q   H a���� I  K P�����
�� .sysoexecTEXT���     TEXT� o   K L���� 0 	shscript2  ��  � R      �����
�� .ascrerr ****      � ****� o      ���� 
0 errmsg  ��  � k   X a�� ��� I   X _������� $0 shownotification showNotification� ��� m   Y Z�� ��� , E r r o r   C r e a t i n g   N e w   L o g� ���� o   Z [���� 
0 errmsg  ��  ��  � ���� l  ` `��������  ��  ��  ��  � ���� l  b b��������  ��  ��  ��  � ��� l     ��������  ��  ��  � ��� i   � ���� I      ������� 0 killapp killApp� ���� o      ���� "0 applicationname applicationName��  ��  � k     /�� � � r      b      m      �  k i l l a l l   n     1    ��
�� 
strq o    ���� "0 applicationname applicationName o      ���� 0 shscript    	
	 Q    ' k      I   ����
�� .sysoexecTEXT���     TEXT o    ���� 0 shscript  ��    r     m    ��
�� boovtrue o      ���� 0 	mysuccess 	mySuccess �� r     m     �   o      ���� 0 	mymessage 	myMessage��   R      ����
�� .ascrerr ****      � **** o      �� 
0 errmsg  ��   k     '  r     #  m     !�
� boovfals  o      �� 0 	mysuccess 	mySuccess !�! r   $ '"#" o   $ %�� 
0 errmsg  # o      �� 0 	mymessage 	myMessage�  
 $%$ l  ( (����  �  �  % &'& L   ( -(( J   ( ,)) *+* o   ( )�� 0 	mysuccess 	mySuccess+ ,�, o   ) *�� 0 	mymessage 	myMessage�  ' -�- l  . .����  �  �  �  � ./. l     ����  �  �  / 010 i   �232 I      ���� 20 checkaftereffectsstatus checkAfterEffectsStatus�  �  3 k     D44 565 l     ����  �  �  6 787 r     9:9 I     
�;�� 0 getageoffile getAgeOfFile; <�< o    �� *0 aftereffectslogfile afterEffectsLogFile�  �  : o      �� &0 logfileageseconds logFileAgeSeconds8 =>= l   ����  �  �  > ?@? Z    <AB�CA G    DED =   FGF o    �� &0 logfileageseconds logFileAgeSecondsG m    �
� 
nullE ?    HIH o    �� &0 logfileageseconds logFileAgeSecondsI l   J��J ]    KLK o    �� 00 expectedmaxprocesstime expectedMaxProcessTimeL m    �� �  �  B k   ! 2MM NON l  ! !�PQ�  P a [ AE is likely hung, kill it and relaunch project, but we will finish submitting stats first   Q �RR �   A E   i s   l i k e l y   h u n g ,   k i l l   i t   a n d   r e l a u n c h   p r o j e c t ,   b u t   w e   w i l l   f i n i s h   s u b m i t t i n g   s t a t s   f i r s tO STS l  ! !����  �  �  T UVU r   ! $WXW m   ! "�
� boovfalsX o      �� 0 	mysuccess 	mySuccessV YZY r   % ([\[ m   % &]] �^^ H p l e a s e   r e s t a r t   A f t e r   E f f e c t s   p r o j e c t\ o      �� 0 	mymessage 	myMessageZ _`_ I   ) 0�a�� $0 shownotification showNotificationa bcb m   * +dd �ee   A E   H a n g   D e t e c t e dc f�f m   + ,gg �hh < w i l l   t r y   t o   r e s t a r t   i t   f o r   y o u�  �  ` iji l  1 1����  �  �  j k�k l  1 1����  �  �  �  �  C k   5 <ll mnm l  5 5�op�  o   appears to be OK   p �qq "   a p p e a r s   t o   b e   O Kn rsr r   5 8tut m   5 6�
� boovtrueu o      �� 0 	mysuccess 	mySuccesss v�v r   9 <wxw m   9 :yy �zz  x o      �� 0 	mymessage 	myMessage�  @ {|{ l  = =���~�  �  �~  | }~} L   = B J   = A�� ��� o   = >�}�} 0 	mysuccess 	mySuccess� ��|� o   > ?�{�{ 0 	mymessage 	myMessage�|  ~ ��� l  C C�z�y�x�z  �y  �x  � ��w� l  C C�v�u�t�v  �u  �t  �w  1 ��� l     �s�r�q�s  �r  �q  � ��p� i  ��� I      �o�n�m�o *0 recycleaftereffects recycleAfterEffects�n  �m  � k     /�� ��� l     �l���l  � 6 0 AE is likely hung, kill it and relaunch project   � ��� `   A E   i s   l i k e l y   h u n g ,   k i l l   i t   a n d   r e l a u n c h   p r o j e c t� ��� I     �k��j�k $0 shownotification showNotification� ��� m    �� ��� " A E   N o n   R e s p o n s i v e� ��i� m    �� ��� 0 t r y i n g   a u t o m a t e d   r e s t a r t�i  �j  � ��� l   �h�g�f�h  �g  �f  � ��� I    �e��d�e 0 killapp killApp� ��c� m   	 
�� ���  A f t e r   E f f e c t s�c  �d  � ��� I   �b��a
�b .sysodelanull��� ��� nmbr� m    �`�` 
�a  � ��� l   �_�^�]�_  �^  �]  � ��� l   �\���\  � R L archive log file so we won't hit the error here again while AE is rebooting   � ��� �   a r c h i v e   l o g   f i l e   s o   w e   w o n ' t   h i t   t h e   e r r o r   h e r e   a g a i n   w h i l e   A E   i s   r e b o o t i n g� ��� I    �[�Z�Y�[ 00 archiveaftereffectslog archiveAfterEffectsLog�Z  �Y  � ��� l   �X�W�V�X  �W  �V  � ��� l   �U���U  � "  launch AE with project file   � ��� 8   l a u n c h   A E   w i t h   p r o j e c t   f i l e� ��� I    "�T��S�T $0 shownotification showNotification� ��� m    �� ���  R e s t a r t i n g   A E� ��R� m    �� ��� , t h i s   m a y   t a k e   a   m i n u t e�R  �S  � ��� I   # -�Q��P�Q 60 launchaftereffectsproject launchAfterEffectsProject� ��O� o   $ )�N�N 20 aftereffectsprojectfile afterEffectsProjectFile�O  �P  � ��M� l  . .�L�K�J�L  �K  �J  �M  �p       ?�I�   ����H�G�F�E�D V [�C�B�A�@�?�> z�=�����������������������������������������I  � =�<�;�:�9�8�7�6�5�4�3�2�1�0�/�.�-�,�+�*�)�(�'�&�%�$�#�"�!� ����������������������
�	��������� �< 0 theplistpath thePListPath�; *0 aftereffectslogfile afterEffectsLogFile�: 20 aftereffectsprojectfile afterEffectsProjectFile�9 00 incomingfileextensions incomingFileExtensions�8 80 rawtranscodefileextensions rawTranscodeFileExtensions�7 D0  bypassrawtranscodefileextensions  bypassRawTranscodeFileExtensions�6 60 statsintervalcheckminutes statsIntervalCheckMinutes�5 $0 processdelaysecs processDelaySecs�4 0 loopdelaysecs loopDelaySecs�3 0 	automated  �2 0 devendpoints devEndpoints�1 $0 computerusername computerUserName�0 $0 waivergrepstring waiverGrepString�/ >0 filesizecheck_minaeoutputsize filesizeCheck_minAEOutputSize�. (0 filesizecheckdelay filesizeCheckDelay�- (0 performfilecleanup performFileCleanup�, 0 dev_mode  �+ 00 oldestfileagethreshold oldestFileAgeThreshold�* 00 expectedmaxprocesstime expectedMaxProcessTime�) "0 applicationname applicationName�( $0 maxfilespercycle maxFilesPerCycle
�' .aevtoappnull  �   � ****
�& .miscidlenmbr    ��� null
�% .aevtquitnull��� ��� null�$ $0 shownotification showNotification�# "0 processonecycle processOneCycle�" &0 checkpendingfiles checkPendingFiles�! 0 filecleanup fileCleanup�  ,0 movefilestoingestion moveFilesToIngestion� :0 transcodeaftereffectsoutput transcodeAfterEffectsOutput� 0 handlewaivers handleWaivers� (0 checkrawfilestatus checkRawFileStatus� ,0 checkfilewritestatus checkFileWriteStatus� 0 gatherstats gatherStats� "0 sendstatustobpc sendStatusToBPC� 0 setvars setVars� 0 setpaths setPaths� 0 read_waiver  � 0 get_element  � 0 
fileexists 
fileExists� 0 folderexists folderExists� 0 	changeext 	changeExt�  0 getfreediskpct getFreeDiskPct� (0 getfreediskspacegb getFreeDiskSpaceGB� 0 getdiskstats getDiskStats� 0 
is_running  � 0 	readplist 	readPlist� 0 
writeplist 
writePlist� 0 getepochtime getEpochTime� 0 number_to_string  �
 0 add_leading_zeros  �	 00 roundandtruncatenumber roundAndTruncateNumber� 0 trimline trimLine� (0 getageofoldestfile getAgeOfOldestFile� 0 getageoffile getAgeOfFile� 0 getoldestfile getOldestFile� 60 launchaftereffectsproject launchAfterEffectsProject� 00 archiveaftereffectslog archiveAfterEffectsLog� 0 killapp killApp� 20 checkaftereffectsstatus checkAfterEffectsStatus�  *0 recycleaftereffects recycleAfterEffects� ����� �   ! % ) ,� ����� �   4 7� ����� �   ? B�H �G �F 

�E boovfals
�D boovfals�C ��@�B 
�A boovtrue
�@ boovfals�? �> ��= 
� �� ���������
�� .aevtoappnull  �   � ****��  ��  �  � �� ��� � ����� ���������������������

�� .misccurdldt    ��� null
�� .ascrcmnt****      � ****�� $0 shownotification showNotification�� 0 
fileexists 
fileExists
�� .sysodisAaleR        TEXT
�� .aevtquitnull��� ��� null�� 0 ishappy isHappy�� 0 setvars setVars�� 0 isready1 isReady1�� 0 setpaths setPaths�� 0 isready2 isReady2
�� 
bool�� 0 gatherstats gatherStats�� "0 processonecycle processOneCycle�� �*j  �%j O*��l+ O*b   k+ f  �b   %j O*j 	Y hOeE�O*j+ E�O*j+ E�O�e 	 �e �& *jk+ O*ek+ Y a �%a %�%j O*j  a %j OP� ����������
�� .miscidlenmbr    ��� null��  ��  �  � ����*,���� "0 processonecycle processOneCycle
�� .misccurdldt    ��� null
�� .ascrcmnt****      � ****�� "*fk+  O*j �%b  %�%j Ob  � ��5��������
�� .aevtquitnull��� ��� null��  ��  �  � ��=������
�� .misccurdldt    ��� null
�� .ascrcmnt****      � ****�� 0 gatherstats gatherStats
�� .aevtquitnull��� ��� null�� *j  �%j O*jk+ O)jd* � ��J���������� $0 shownotification showNotification�� ����� �  ������  0 subtitlestring subtitleString�� 0 messagestring messageString��  � ������  0 subtitlestring subtitleString�� 0 messagestring messageString� ������������
�� 
null
�� 
appr
�� 
subt�� 
�� .sysonotfnull��� ��� TEXT
�� .sysodelanull��� ��� nmbr�� ,�� ��b  �� Y *��b  � Okj OP� ��o���������� "0 processonecycle processOneCycle�� �� ��    ����  0 isinitialcycle isInitialCycle��  � ����  0 isinitialcycle isInitialCycle� ��y���������������������������
�� .misccurdldt    ��� null
�� .ascrcmnt****      � ****
�� 
null�� $0 shownotification showNotification�� 0 gatherstats gatherStats�� 0 handlewaivers handleWaivers
� .sysodelanull��� ��� nmbr� :0 transcodeaftereffectsoutput transcodeAfterEffectsOutput� ,0 movefilestoingestion moveFilesToIngestion� 0 filecleanup fileCleanup
� 
givu� 
� .sysodisAaleR        TEXT
� .aevtquitnull��� ��� null�� *j  �%j O*��l+ O*kk+ O*j+ O*j  �%b  %j Ob  j 	O*kk+ O*j+ 
O*j  �%b  %j Ob  j 	O*kk+ O*j+ O*j  �%b  %j Ob  j 	O*j+ O*j  �%b  %j Ob  j 	O*kk+ O*j  a %j O� *a a l+ Y *a a b  %a %l+ Ob  	f  &*j  a %j Oa a a l O*j Y hOP� �>���� &0 checkpendingfiles checkPendingFiles� ��   ��� 0 oldlist oldList� 0 newlist newList�   �������� 0 oldlist oldList� 0 newlist newList� 0 
filesadded 
filesAdded� 0 filesremoved filesRemoved� 0 isprocessing isProcessing� 0 i  � 0 n   ���~������
� 
list
� 
leng
� 
bool
� .ascrcmnt****      � ****
� 
cobj
� .corecnte****       ****
� 
TEXT� �jv�&E�Ojv�&E�OfE�O��,��,	 	��,j�& ���  �j OfY hO *k��-j kh ��/E�O�� 	��6FY h[OY��O *k��-j kh ��/E�O�� 	��6FY h[OY��O�%�&j O�%j O��,j	 	��,j �& fY hO��,j	 	��,j�& eY hOPY �j OeE�O�OP� �"���� 0 filecleanup fileCleanup�  �   ��� 0 shscript  � 
0 errmsg   0���
� .sysoexecTEXT���     TEXT� 
0 errmsg  �  � (b  e  �E�O 
�j W X  hOPY hOP� �K���� ,0 movefilestoingestion moveFilesToIngestion�  �   ������������ 0 myfolder  � 0 	pmyfolder  � 0 shscript  � 0 filelist  � 
0 errmsg  � 0 
cyclecount 
cycleCount� 0 i  � 
0 myfile  � 0 newfile  � 0 basefilename baseFilename� $0 intermediatelist intermediateList +���_�i�m����������~�}�|�{���z���y�x�w�v �u.�t24MRlp�� 0 processed_media  
� 
psxp
� .misccurdldt    ��� null
� .ascrcmnt****      � ****
� 
strq
� .sysoexecTEXT���     TEXT
� 
cpar
� 
list� 
0 errmsg  �  
� 
kocl
� 
cobj
�~ .corecnte****       ****
�} 
TEXT�|  B@�{ ,0 checkfilewritestatus checkFileWriteStatus�z 0 spock_ingestor  
�y .sysobeepnull��� ��� long
�x 
givu�w 
�v .sysodisAaleR        TEXT�u 0 	changeext 	changeExt�t 0 
media_pool  ���E�O��,E�O*j �%j O��,%�%E�O �j �-�&E�W !X  *j �%�%j O�j Ojv�&E�OjE�Ob�[�a l kh �b   Y hO��%a &E�O�E�O*�a km+ e a ��,%a %_ �,%�%E�O *j a %j O�j W :X  *j a %�%j O*j O*j O*j Oa �%a a l OO*�a &a l+  E�Oa !_ "�,%a #%�%a $%E�O �j �-�&E�W %X  *j a %%�%j Oa &j Ojv�&E�O M�[�a l kh a '_ "�,%a (%�%a &E�O 
�j W X  *j a )%�%j OP[OY��O�kE�OPY hOP[OY��O*j a *%j OP� �s��r�q	�p�s :0 transcodeaftereffectsoutput transcodeAfterEffectsOutput�r  �q   �o�n�m�l�k�j�i�h�g�f�e�d�c�b�a�`�o 0 myfolder  �n 0 completed_waivers  �m 0 shscript  �l 0 filelist  �k 
0 errmsg  �j 0 
cyclecount 
cycleCount�i 0 i  �h  0 outputfilename outputFilename�g *0 outputfilenameclean outputFilenameClean�f 0 	filesize1  �e 0 	filesize2  �d  0 filesize1clean filesize1Clean�c  0 filesize2clean filesize2Clean�b 0 skipthisfile skipThisFile�a &0 skipthisfileclean skipThisFileClean�` 
0 myfile  	 #�_�^��]�\�[��Z��Y��X�W�V�U�T��S�R�Q�PR�O�Nkw�M��L�����_ 0 	ae_output  �^ @0 pendingwaiversforaefolderposix pendingWaiversForAEFolderPosix
�] 
psxp
�\ 
TEXT
�[ .misccurdldt    ��� null
�Z .ascrcmnt****      � ****
�Y 
strq
�X .sysoexecTEXT���     TEXT
�W 
cpar
�V 
list�U 
0 errmsg  �T  
�S 
kocl
�R 
cobj
�Q .corecnte****       ****�P 0 	changeext 	changeExt
�O 
psxf
�N .rdwrgeofcomp       ****
�M .sysodelanull��� ��� nmbr
�L 
bool�p��E�O��%�,�&E�O*j �%j O��,%�%E�O �j �-�&E�W !X  *j �%j Oa j Ojv�&E�OjE�O5�[a a l kh *��&a l+ E�O*��&a l+ E�O�b   Y hOjE�OkE�OjE�OkE�OfE�OfE�O �hZ*j a %j O��%�&E�O*a �/j E�O*j a %�%j O�b   a j Okj Y hO*a �/j E�O*j a %�%j O�� 	 �b  a & *j a %j OY hO*j a  %���&%j O�b  
 	��a & a !j OeE�OY h[OY�/OP[OY��O*j a "%j OP� �K��J�I
�H�K 0 handlewaivers handleWaivers�J  �I  
 �G�F�E�D�C�B�A�@�?�>�=�<�;�G 60 incomingwaiverfolderposix incomingWaiverFolderPosix�F 0 shscript  �E  0 waiverfilelist waiverFileList�D 
0 errmsg  �C 0 
cyclecount 
cycleCount�B 0 outteri outterI�A 0 
waiverfile 
waiverFile�@ 0 mywaiver myWaiver�? 0 rawfileslist rawFilesList�> 0 movefileslist moveFilesList�= 0 myincrementer myIncrementer�< 0 	myrawfile 	myRawFile�; 0 success    �:��9�8�7�6�5�4�3�2�1J�0�/�.�-�,�+�*���)�(�'�&4:�%
�: .misccurdldt    ��� null
�9 .ascrcmnt****      � ****�8 .0 incomingrawxmlwaivers incomingRawXmlWaivers
�7 
strq
�6 .sysodlogaskr        TEXT
�5 .sysoexecTEXT���     TEXT
�4 
cpar
�3 
list�2 
0 errmsg  �1  
�0 
kocl
�/ 
cobj
�. .corecnte****       ****�- 0 read_waiver  �, 0 waiver_total_raw_files  
�+ 
null
�* 
bool�) 0 get_element  �( (0 checkrawfilestatus checkRawFileStatus�' @0 pendingwaiversforaefolderposix pendingWaiversForAEFolderPosix
�& 
TEXT
�% .sysodisAaleR        TEXT�HX*j  �%j O�E�O��,%�%b  %E�Ob  e  
�j Y hO �j �-�&E�W X  *j  �%j O�j Ojv�&E�OjE�O ��[��l kh �b   Y hO��%E�O*�k+ E�OjvE�OjvE�O�a ,a 	 �a ,ja & 5jE�O +�a ,Ekh�kE�O*�a a �%m+ E�O��6G[OY��Y ��6GO*�k+  Ta ��,%a %_ %�a &%E�OfE�O �j O�kE�OeE�W  X  *j  a %�%j Oa �%j Y hOP[OY�!OP� �$U�#�"�!�$ (0 checkrawfilestatus checkRawFileStatus�# � �    �� 0 rawfileslist rawFilesList�"   ������������������� 0 rawfileslist rawFilesList� "0 filereadystatus fileReadyStatus�  0 skiptranscoded skipTranscoded� 0 movefileslist moveFilesList� 0 i  � 0 
namelength  � 
0 myname  � 0 fileextension fileExtension� $0 mytranscodedfile myTranscodedFile� 0 	myrawfile 	myRawFile�  0 foundextension foundExtension� 0 shscript  � 0 success  � $0 filemove_success fileMove_success� 
0 errmsg  � 0 rawfilei rawFileI� 
0 myfile  � 0 pmyfile   'x���
�	����������#� 7;��������^��e����������������	
� .sysodlogaskr        TEXT
� 
kocl
�
 
cobj
�	 .corecnte****       ****
� 
TEXT
� 
cha � � >0 transcodecompletedfolderposix transcodeCompletedFolderPosix� :0 incomingrawmediafolderposix incomingRawMediaFolderPosix��� ,0 checkfilewritestatus checkFileWriteStatus
� 
strq�  :0 transcodependingfolderposix transcodePendingFolderPosix
�� .sysoexecTEXT���     TEXT�� 
0 errmsg  ��  
�� .misccurdldt    ��� null
�� .ascrcmnt****      � ****
�� .sysodisAaleR        TEXT
�� 
leng
�� 
bool
�� 
psxp�� 0 
media_pool  �!*b  e  �%j Y hOfE�OfE�OjvE�O�[��l kh ��&�-j E�O�[�\[Zk\Z��2�&E�O �b  [��l kh ��%�%�%�&E�O��%�%�%�&E�O�E�O*��km+  eE�O��%�&�6GOY hO*�jkm+  �fE�Ob  � ��,%a %_ %��%%E�Y #b  � a ��,%a %�%��%%E�Y hOfE�O �j OeE�OOPW $X  *j a %�%j Oa �%j OfE�OPY hOP[OY�.OP[OY��O�e 	 �a ,�a , a & � ��[��l kh b  e  a �%�&j Y hOa �%�%�&E^ O] a  ,E^ Oa !] �,%a "%_ #%�%E�OfE�Ob  e  
�j Y hO �j OeE�W &X  *j a $%�%j Oa %�%j OfE�OOP[OY�cO�OPY b  e  a &j Y hOfOPO�OP� ��	5�������� ,0 checkfilewritestatus checkFileWriteStatus�� ����   �������� 0 pmyfile  �� "0 expectedminsize expectedMinSize�� &0 checkdelayseconds checkDelaySeconds��   �������������� 0 pmyfile  �� "0 expectedminsize expectedMinSize�� &0 checkdelayseconds checkDelaySeconds��  0 thisfilestatus thisFileStatus�� 0 	filesize1  �� 0 	filesize2   ������������
�� 
null�� 0 
fileexists 
fileExists
�� 
psxf
�� .rdwrgeofcomp       ****
�� 
bool
�� .sysodelanull��� ��� nmbr�� lfE�OjE�OkE�O��  kE�Y hO*�k+  E*�/j E�O�k
 ���& fY hO�j O*�/j E�O��  eY fOPY fO�� ��	��������� 0 gatherstats gatherStats�� ����   ���� 0 intervalmins intervalMins��   ������������������������������������������������������������ 0 intervalmins intervalMins�� 0 
initialrun 
initialRun�� 0 timenow timeNow�� 0 	nextcheck 	nextCheck�� 40 updatestalefiletimestamp updateStaleFileTimestamp�� 0 oldestfileage oldestFileAge�� 0 	dataitems 	dataItems�� 0 failedtests failedTests�� *0 recycleaftereffects recycleAfterEffects�� 0 	firstitem 	firstItem�� 0 ae_test  �� 00 aftereffectsresponsive afterEffectsResponsive�� 0 ame_test  �� 0 nextitem nextItem�� 0 dropbox_test  �� 0 shscript  �� 0 mylist myList�� 
0 errmsg  �� 0 mycount myCount�� 0 oldcount oldCount�� 0 ae_waiver_delta  �� 0 isprocessing isProcessing�� 0 mydatavalue myDataValue�� 0 mythreshhold myThreshhold�� 0 ame_file_delta  �� 0 dropbox_waiver_delta  �� 0 memory_stats  �� 0 freediskpct freeDiskPct�� 0 
freediskgb 
freeDiskGB �����������������	�	�

��
��
3
<��
B
K������
x
�
�
�
�
�
�
�
�
�
�
�
�����#����������<����R����bl����������� ��/1;RV��r������������nv�������������)9C����������� 
"24>�����jr�����������������$9IS[egq������������� 0 getepochtime getEpochTime�� 80 laststatsgatheredtimestamp lastStatsGatheredTimestamp�� <��  ���� 0 number_to_string  �� :0 laststalefilechecktimestamp lastStaleFileCheckTimestamp�� *0 forcestalefilecheck forceStaleFileCheck�� 0 ishappy isHappy�� 0 
is_running  �� 0 
writeplist 
writePlist
�� .sysodlogaskr        TEXT�� 20 checkaftereffectsstatus checkAfterEffectsStatus
�� 
list
�� 
cobj�� @0 pendingwaiversforaefolderposix pendingWaiversForAEFolderPosix
�� 
strq
�� .sysoexecTEXT���     TEXT
�� 
cpar�� 
0 errmsg  ��  
�� .misccurdldt    ��� null
�� .ascrcmnt****      � ****
�� 
leng
�� 
null�� 0 	readplist 	readPlist
�� 
bool�� 00 pendingaewaiverlistold pendingAEWaiverListOLD�� &0 checkpendingfiles checkPendingFiles�� (0 getageofoldestfile getAgeOfOldestFile
�� .corecnte****       ****�� 0 	ame_watch  � .0 pendingamefilelistold pendingAMEFileListOLD� .0 incomingrawxmlwaivers incomingRawXmlWaivers� :0 pendingdropboxwaiverlistold pendingDropboxWaiverListOLD�  0 getfreediskpct getFreeDiskPct
� 
long� (0 getfreediskspacegb getFreeDiskSpaceGB� � 

� .sysodisAaleR        TEXT
� .sysosigtsirr   ��� null
� 
siip� "0 sendstatustobpc sendStatusToBPC� *0 recycleaftereffects recycleAfterEffects��	�fE�O�j (*j+  E�O*��� � k+ E�O�� hY hY *j+  E�O*j+  E�OfE�OeE�OeE�OfE�OjE�O�E�O�E�OfE�O�E�O*�k+ E�O*b   ��m+ O�f  0fE�O��%E�Ob  e  a j Y hOa E�OeE�OPY a E�O��%E�O*j+ a &E�O�a k/f  eE�O�a %E�Y hO*a k+ E�O*b   a �m+ O�f  ,fE�O�a %E�Ob  e  a j Y hOa E�Y a E�O�a %�%E�O*a k+ E�O*b   a  �m+ O�f  ,fE�O�a !%E�Ob  e  a "j Y hOa #E�Y a $E�O�a %%�%E�Oa &_ 'a (,%a )%E�O �j *a +-a &E^ W X , -*j .a /%] %j 0OjvE^ O] a 1,E^ O*b   a 2a 3m+ 4E^ O] ] E^ O*b   a 5] m+ O*b   a 6] m+ O*j+  E�O*�b  � k+ E�O��
 	�e a 7&
 	�e a 7& T*_ 8] l+ 9E^ O] E` 8O] f  *eE�OfE�O�a :%E�Ob  e  a ;j Y hY hOeE�OPY hO] E^ Oa <] %a =%E�O�a >%�%E�O] E^ Oa ?] %a @%E�O�a A%�%E�O*_ 'a Bl+ CE^ O] � 
] E�Y hOa D] %a E%E�O�a F%�%E�OjE^ Oa G_ 'a (,%a H%E�O -�j *a +-a &j IE^ O*b   a J] m+ OPW X , -*j .a K%] %j 0OjE^ O] ]  &fE�O�a L%E�Ob  e  a Mj Y hY hO] E^ Oa N] %a O%E�O�a P%�%E�Oa Q_ Ra (,%a S%E�O �j *a +-a &E^ W X , -*j .a T%] %j 0OjvE^ O] a 1,E^ O*b   a Ua 3m+ 4E^ O] ] E^ O*b   a V] m+ O*b   a W] m+ O*j+  E�O*�b  � k+ E�O��
 	�e a 7&
 	�e a 7& R*_ X] l+ 9E^ O] E` XO] f  *fE�OeE�O�a Y%E�Ob  e  a Zj Y hY hOeE�Y hO] E^ Oa [] %a \%E�O�a ]%�%E�O] E^ Oa ^] %a _%E�O�a `%�%E�O*_ Ra al+ CE^ O] � 
] E�Y hOa b] %a c%E�O�a d%�%E�Oa e_ fa (,%a g%E�O �j *a +-a &E^ W X , -*j .a h%] %j 0OjvE^ O] a 1,E^ O*b   a ia 3m+ 4E^ O] ] E^ O*b   a j] m+ O*b   a k] m+ O*j+  E�O*�b  � k+ E�O��
 	�e a 7&
 	�e a 7& R*_ l] l+ 9E^ O] E` lO] f  *fE�OeE�O�a m%E�Ob  e  a nj Y hY hOeE�Y hO] E^ Oa o] %a p%E�O�a q%�%E�O] E^ Oa r] %a s%E�O�a t%�%E�O*_ fa ul+ CE^ O] � 
] E�Y hOa v] %a w%E�O�a x%�%E�O a yj *E^ W X , -a z] %E^ O] E^ Oa {] %a |%E�O�a }%�%E�O*j+ ~a &E^ O*j+ �a &E^ O] a �
 ] a �a 7& &fE�O�a �%E�Ob  e  a �j �Y hY hO] E^ Oa �] %a �%E�O�a �%�%E�O] E^ Oa �] %a �%E�O�a �%�%E�O *b   a �a �m+ 4E^ W X , -a �] %E^ Oa �] %a �%E�O�a �%�%E�O*j �a �,E^ Oa �] %a �%E�O�a �%�%E�O�E^ Oa �] %a �%E�O�a �%�%E�O�b   �a �%E�Y hO�e  a �E^ OfE�Y a �E^ Ob  e  a �j �Y hOa �] %a �%E�O�a �%�%E�O�E^ Oa �] %a �%E�O�a �%�%E�O*j+  E^ Oa �] %a �%E�O�a �%�%E�O*j �a �,E^ Oa �] %a �%E�O�a �%�%E�Ob  e  
�j Y hO*�k+ �O*j+  E�O�e  *j+  E�Y hO�e 	 b  f a 7& 
*j+ �Y hOP� ������ "0 sendstatustobpc sendStatusToBPC� ��   �� 0 	dataitems 	dataItems�   �������� 0 	dataitems 	dataItems� 0 myip myIP� 0 
theurlbase 
theURLbase� 0 
thepayload 
thePayload� 0 shscript  � 0 	myoutcome 	myOutcome� 
0 errmsg   �� ��4:JLN^�b���~�}
� .sysosigtsirr   ��� null
� 
siip� 0 	readplist 	readPlist
� 
bool
� 
strq
� .sysodlogaskr        TEXT
� .sysoexecTEXT���     TEXT�~ 
0 errmsg  �}  � �*j  �,E�O�E�O*b   �b  
m+ Ec  
Ob  
�&Ec  
Ob  
e  
�%E�Y �%E�O�%�%�%�%E�O��,%�%�%E�Ob  e  
�j Y hO �j E�O�W 	X  �OP� �|��{�z�y�| 0 setvars setVars�{  �z   �x�x 
0 errmsg   �w�v�u�t��s�r�q���p����o&�n;Pez�m�l
�w 
list�v 00 pendingaewaiverlistold pendingAEWaiverListOLD�u .0 pendingamefilelistold pendingAMEFileListOLD�t :0 pendingdropboxwaiverlistold pendingDropboxWaiverListOLD
�s 
null�r 0 	readplist 	readPlist�q 0 computer_id  
�p 
bool�o 0 stationtype stationType
�n 
long�m 
0 errmsg  �l  �yK@jv�&E�Ojv�&E�Ojv�&E�O*b   ��m+ E�O*b   �b  m+ Ec  O*b   �b  m+ �&Ec  O*b   �b  
m+ �&Ec  
O*b   �em+ �&Ec  O*b   ��m+ E�O*b   �b  	m+ �&Ec  	O*b   a b  m+ a &Ec  O*b   a b  m+ a &Ec  O*b   a b  m+ �&Ec  O*b   a b  m+ a &Ec  O*b   a b  m+ a &Ec  OeW 	X  �OP� �k��j�i�h�k 0 setpaths setPaths�j  �i   �g�f�g 0 filetest fileTest�f 
0 errmsg   2���e�d��c��b��a���`�_�^�]��\�[���Z"$�Y;G�X^ln�W�V����U����T����S��R�e 0 	readplist 	readPlist�d "0 dropboxrootpath dropboxRootPath�c 0 stationtype stationType�b .0 incomingrawxmlwaivers incomingRawXmlWaivers�a :0 incomingrawmediafolderposix incomingRawMediaFolderPosix�` :0 transcodependingfolderposix transcodePendingFolderPosix�_ 0 folderexists folderExists�^ 
0 errmsg  �]  
�\ 
strq
�[ .sysoexecTEXT���     TEXT�Z >0 transcodecompletedfolderposix transcodeCompletedFolderPosix�Y @0 pendingwaiversforaefolderposix pendingWaiversForAEFolderPosix�X %0 !waivers_pending_completed_archive  �W 0 
media_pool  �V  �U 0 processed_media  �T 0 	ae_output  �S 0 	ame_watch  �R 0 spock_ingestor  �h��*b   ��m+ E�Ob  
e  ��%�%E�Y ��%E�O��%E�O��%E�O�b  %�%E�O *�k+ E�W 
X  eE�O�f  a �a ,%j Y hOa b  %a %E` O *_ k+ E�W 
X  eE�O�f  a _ a ,%j Y hOa b  %a %E` O *_ k+ E�W 
X  eE�O�f  a _ a ,%j Y hO_ a %E` O *_ k+ E�W 
X  eE�O�f  a _ a ,%j Y hOa b  %a %E`  O *_  k+ E�W 
X ! eE�O�f  a "_  a ,%j Y hOa #b  %a $%E` %O *_ %k+ E�W 
X ! eE�O�f  a &_ %a ,%j Y hOa 'b  %a (%E` )O *_ )k+ E�W 
X ! eE�O�f  a *_ )a ,%j Y hOa +b  %a ,%E` -O *_ -k+ E�W 
X ! eE�O�f  a ._ -a ,%j Y hO�a /%E` 0O *_ 0k+ E�W 
X ! eE�O�f  a 1_ 0a ,%j Y hOeW 	X  �OP� �Q@�P�O�N�Q 0 read_waiver  �P �M�M   �L�L 0 	thewaiver  �O   	�K�J�I�H�G�F�E�D�C�K 0 	thewaiver  �J 0 	waiver_id  �I 0 waiver_created  �H 0 waiver_program  �G 0 waiver_activation  �F 0 waiver_team  �E 0 waiver_event  �D 0 waiver_total_raw_files  �C 0 waiver_data   RU�B`cps~��������A�@�?�>�=�<�;�:�9�8�B 0 get_element  �A  �@  �? 0 	waiver_id  �> 0 waiver_created  �= 0 waiver_program  �< 0 waiver_activation  �; 0 waiver_team  �: 0 waiver_event  �9 0 waiver_total_raw_files  �8 �N �*���m+ E�O*���m+ E�O*���m+ E�O*���m+ E�O*���m+ E�O*���m+ E�O *���m+ E�W 
X  kE�Oa �a �a �a �a �a �a �a E�O�OP� �7��6�5 �4�7 0 get_element  �6 �3!�3 !  �2�1�0�2 0 	thewaiver  �1 0 node  �0 0 element_name  �5   �/�.�-�,�+�/ 0 	thewaiver  �. 0 node  �- 0 element_name  �, 0 xmldata  �+ 
0 errmsg    ��*�)�(�'�&�%�$�#�"
�* 
xmlf
�) 
pcnt
�( 
xmle
�' kfrmname
�& 
valL
�% 
ret �$ 
0 errmsg  �#  
�" .sysodisAaleR        TEXT�4 >  � *�/�,��0E�O���0�,E�UW X  b  e  
�j 	Y hO�E�� �!� �"#��! 0 
fileexists 
fileExists�  �$� $  �� 0 thefile theFile�  " �� 0 thefile theFile# ��
� 
file
� .coredoexnull���     ****� � *�/j  eY fU� �%��%&�� 0 folderexists folderExists� �'� '  �� 0 thefile theFile�  % �� 0 thefile theFile& 4��
� 
cfol
� .coredoexnull���     ****� � *�/j  eY fU� �;��()�� 0 	changeext 	changeExt� �*� *  �
�	�
 0 filename  �	 0 new_ext  �  ( ����� 0 filename  � 0 new_ext  � 0 mylength  � 0 	newstring  ) ����
� 
cha 
� .corecnte****       ****� 
� 
TEXT� !��-j �E�O�[�\[Zk\Z�2�&�%E�O�� � `����+,���   0 getfreediskpct getFreeDiskPct��  ��  + ���� 0 rawpct rawPct, ����m������ 0 getdiskstats getDiskStats�� 0 capacity  �� 0 trimline trimLine�� d�� **j+  �,�km+ E�O�� ��~����-.���� (0 getfreediskspacegb getFreeDiskSpaceGB��  ��  -  . ������ 0 getdiskstats getDiskStats�� 0 	available  �� 
*j+  �,E� �������/0���� 0 getdiskstats getDiskStats��  ��  / ������������������ 0 old_delimts  �� 0 memory_stats  �� 0 	rawstring  �� 0 thelist theList�� 0 newlist newList�� 0 i  �� 0 	finallist 	finalList�� 
0 errmsg  0 �����������������������������������������
�� 
ascr
�� 
txdl
�� .sysoexecTEXT���     TEXT
�� 
cpar
�� 
citm
�� 
kocl
�� 
cobj
�� .corecnte****       ****
�� 
ctxt�� 0 diskname  �� 0 
onegblocks 
OneGblocks�� 0 used  �� 0 	available  �� �� 0 capacity  �� �� 
�� 
0 errmsg  ��  �� ���,E�O ��j E�O��l/E�O���,FO��-E�O���,FOjvE�O (�[��l 	kh ��-� ��-�6GY hOP[OY��O��k/�-���l/�-��m/�-��a /�-a ��a /�-a E�W X  hO���,FO�OP� ��.����12���� 0 
is_running  �� ��3�� 3  ���� 0 appname appName��  1 ���������� 0 appname appName�� "0 listofprocesses listOfProcesses�� 0 appisrunning appIsRunning�� 0 thisitem thisItem2 ;����������
�� 
prcs
�� 
pnam
�� 
kocl
�� 
cobj
�� .corecnte****       ****�� ;� 	*�-�,E�UOfE�O #�[��l kh �� 
eE�OY h[OY��O�OP� ��Y����45���� 0 	readplist 	readPlist�� �6� 6  ���� 0 theplistpath thePListPath� 0 plistvar plistVar� 0 defaultvalue defaultValue��  4 ������� 0 theplistpath thePListPath� 0 plistvar plistVar� 0 defaultvalue defaultValue� 0 	mycommand 	myCommand� 0 myscript myScript� 
0 errmsg  5 lnx�����
� .sysoexecTEXT���     TEXT� 
0 errmsg  �  
� 
null
� .sysodisAaleR        TEXT�� 3 �%�%E�O�%�%E�O�j W X  ��  
�j Y hO�� ����78�� 0 
writeplist 
writePlist� �9� 9  ���� 0 theplistpath thePListPath� 0 plistvar plistVar� 0 thevalue theValue�  7 ������� 0 theplistpath thePListPath� 0 plistvar plistVar� 0 thevalue theValue� 0 	mycommand 	myCommand� 0 myscript myScript� 
0 errmsg  8 �������� 
0 errmsg  �  
� .sysodisAaleR        TEXT� $ �%�%�%�%E�O�%�%E�W X  �j � ����:;�� 0 getepochtime getEpochTime�  �  : �� 
0 mytime  ; �����
� .sysoexecTEXT���     TEXT
� 
TEXT�  ��� 0 number_to_string  � �j �&E�O�� E�O*�k+ � ����<=�� 0 number_to_string  � �>� >  �� 0 this_number  �  < 	���������� 0 this_number  � 0 x  � 0 y  � 0 z  � 0 decimal_adjust  � 0 
first_part  � 0 second_part  � 0 converted_number  � 0 i  = ����������9��~k
� 
TEXT
� 
psof
� 
psin� 
� .sysooffslong    ��� null
� 
cha 
� 
leng
� 
nmbr�  �~  � ���&E�O�� �*���� E�O*���� E�O*���� E�O�[�\[Z���,\Zi2�&�&E�O�j �[�\[Zk\Z�k2�&E�Y �E�O�[�\[Z�k\Z�k2�&E�O�E�O &k�kh  ���/%E�W X  ��%E�[OY��O�Y �� �}x�|�{?@�z�} 0 add_leading_zeros  �| �yA�y A  �x�w�x 0 this_number  �w 0 max_leading_zeros  �{  ? �v�u�t�s�r�q�v 0 this_number  �u 0 max_leading_zeros  �t 0 threshold_number  �s 0 leading_zeros  �r 0 digit_count  �q 0 character_count  @ �p�o��n�m��l�p 

�o 
long
�n 
TEXT
�m 
leng
�l 
ctxt�z H�$�&E�O�� 7�E�O�k"�&�,E�O�k�E�O �kh��%�&E�[OY��O���&%�&Y ��&� �k��j�iBC�h�k 00 roundandtruncatenumber roundAndTruncateNumber�j �gD�g D  �f�e�f 0 	thenumber 	theNumber�e .0 numberofdecimalplaces numberOfDecimalPlaces�i  B �d�c�b�a�`�_�d 0 	thenumber 	theNumber�c .0 numberofdecimalplaces numberOfDecimalPlaces�b $0 theroundingvalue theRoundingValue�a 0 themodvalue theModValue�` 0 thesecondpart theSecondPart�_ 0 thefirstpart theFirstPartC ��^���]!.�\�[O�Zq�^ 0 number_to_string  
�] 
nmbr
�\ 
ctxt
�[ 
leng
�Z 
TEXT�h ��j  ��E�O*�k"k+ Y hO�E�O �kh�%E�[OY��O�%�&E�O��E�O�E�O �kkh�%E�[OY��O�%�&E�O�k#�"E�O��&�,�  ���&�,kh�%�&E�[OY��Y hO�k"E�O*�k+ E�O��%�%E�O�� �Y~�X�WEF�V�Y 0 trimline trimLine�X �UG�U G  �T�S�R�T 0 	this_text  �S 0 
trim_chars  �R 0 trim_indicator  �W  E �Q�P�O�N�Q 0 	this_text  �P 0 
trim_chars  �O 0 trim_indicator  �N 0 x  F �M�L�K�J�I��
�M 
leng
�L 
cha 
�K 
TEXT�J  �I  �V |��,E�Ojllv� 0 *h�� �[�\[Z�k\Zi2�&E�W 	X  �[OY��Y hOkllv� 1 +h�� �[�\[Zk\Z�k'2�&E�W 	X  �[OY��Y hO�� �H��G�FHI�E�H (0 getageofoldestfile getAgeOfOldestFile�G �DJ�D J  �C�B�C 0 filepath filePath�B 0 fileextension fileExtension�F  H �A�@�?�A 0 filepath filePath�@ 0 fileextension fileExtension�? 
0 myfile  I �>�=�> 0 getoldestfile getOldestFile�= 0 getageoffile getAgeOfFile�E *��l+  E�O�� *��%k+ Y j� �<�;�:KL�9�< 0 getageoffile getAgeOfFile�; �8M�8 M  �7�7 
0 myfile  �:  K �6�5�4�3�6 
0 myfile  �5 0 shscript  �4  0 fileageseconds fileAgeSeconds�3 
0 errmsg  L /1�2�1�0�/�.
�2 .sysoexecTEXT���     TEXT
�1 
long�0 
0 errmsg  �/  
�. 
null�9 #�%�%E�O �j �&E�W 
X  �E�O�� �-H�,�+NO�*�- 0 getoldestfile getOldestFile�, �)P�) P  �(�'�( 0 filepath filePath�' 0 fileextension fileExtension�+  N �&�%�$�#�"�& 0 filepath filePath�% 0 fileextension fileExtension�$ 0 shscript  �# 0 
oldestfile 
oldestFile�" 
0 errmsg  O _�!ce� ��r
�! 
strq
�  .sysoexecTEXT���     TEXT� 
0 errmsg  �  �* '��,%�%�%�%E�O �j E�W 
X  �E�O�� �}��QR�� 60 launchaftereffectsproject launchAfterEffectsProject� �S� S  �� $0 projectfileposix projectFilePOSIX�  Q ���� $0 projectfileposix projectFilePOSIX� 0 shscript  � 
0 errmsg  R �����
� 
strq
� .sysoexecTEXT���     TEXT� 
0 errmsg  �  � ��,%E�O 
�j W X  hOP� ����TU�� 00 archiveaftereffectslog archiveAfterEffectsLog�  �  T ���
�	�� 0 	timestamp  � "0 archivefilepath archiveFilePath�
 0 shscript  �	 0 	shscript2  � 
0 errmsg  U �������������� 0 getepochtime getEpochTime
� 
strq
� .sysoexecTEXT���     TEXT� 
0 errmsg  �  � $0 shownotification showNotification� d*j+  E�O�*j+  %�%E�O�b  %�%�%E�O�b  �,%E�O 
�j W X  	*�l+ OPO 
�j W X  	*�l+ OPOP� ��� ��VW��� 0 killapp killApp�  ��X�� X  ���� "0 applicationname applicationName��  V ������������ "0 applicationname applicationName�� 0 shscript  �� 0 	mysuccess 	mySuccess�� 0 	mymessage 	myMessage�� 
0 errmsg  W ��������
�� 
strq
�� .sysoexecTEXT���     TEXT�� 
0 errmsg  ��  �� 0��,%E�O �j OeE�O�E�W X  fE�O�E�O��lvOP� ��3����YZ���� 20 checkaftereffectsstatus checkAfterEffectsStatus��  ��  Y �������� &0 logfileageseconds logFileAgeSeconds�� 0 	mysuccess 	mySuccess�� 0 	mymessage 	myMessageZ ������]dg��y�� 0 getageoffile getAgeOfFile
�� 
null
�� 
bool�� $0 shownotification showNotification�� E*b  k+  E�O�� 
 �b  l �& fE�O�E�O*��l+ OPY 	eE�O�E�O��lvOP� �������[\���� *0 recycleaftereffects recycleAfterEffects��  ��  [  \ ������������������� $0 shownotification showNotification�� 0 killapp killApp�� 

�� .sysodelanull��� ��� nmbr�� 00 archiveaftereffectslog archiveAfterEffectsLog�� 60 launchaftereffectsproject launchAfterEffectsProject�� 0*��l+ O*�k+ O�j O*j+ O*��l+ O*b  k+ 
OP ascr  ��ޭ