﻿//********************************************
//
// UpdateKeyColor.jsx
//
// developed for BenJohnson by Dan Ebberts
// debberts@comcast.net
//
//********************************************

/*
	Revision History

 	Ver. 	Description
	----	---------------------------------------------------------------

	v01	initial release

*/


function UpdateKeyColor(){
	
	var layerName = "customer";
	var samplePos = [1800,250];  // offset from upper left corner
	var sampleRadius = 20;
	
	if ((app.project.activeItem == null) || !(app.project.activeItem instanceof CompItem)){
		alert("No comp active.");
		return;
	}

	var myComp = app.project.activeItem;

	var myLayer = myComp.layer(layerName);
	
	// add temporary color control to layer

	var myColorControl = myLayer.property("Effects").addProperty("ADBE Color Control");
	var myColorProp = myColorControl.property("ADBE Color Control-0001");
	
	myColorProp.expression = "sampleImage(" + samplePos.toSource() + ",[" +  sampleRadius + "," + sampleRadius + "],false);";
	
	// harvest the result
	
	var myColor = myColorProp.value;
	
	// remove the color control
	
	myColorControl.remove();
	
	// apply the color to the Keylight efect
	
	myLayer.property("Effects").property("Keylight (1.2)").property("Screen Colour").setValue(myColor);

}
UpdateKeyColor();

