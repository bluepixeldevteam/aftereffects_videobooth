FasdUAS 1.101.10   ��   ��    k             l     ��  ��    H B multi-processor oriented Applescript for Pre-Ingestion processing     � 	 	 �   m u l t i - p r o c e s s o r   o r i e n t e d   A p p l e s c r i p t   f o r   P r e - I n g e s t i o n   p r o c e s s i n g   
  
 l     ��������  ��  ��        j     �� �� 0 theplistpath thePListPath  m        �   | / U s e r s / b l u e p i x e l / p r o c e s s _ c o n f i g / v i d e o b o o t h _ m a s t e r _ c o n f i g . p l i s t      j    �� �� *0 aftereffectslogfile afterEffectsLogFile  m       �   l / U s e r s / b l u e p i x e l / D o c u m e n t s / w a i v e r s _ p e n d i n g / l o g / l o g . t x t      j    �� �� 20 aftereffectsprojectfile afterEffectsProjectFile  m       �   � / U s e r s / b l u e p i x e l / g i t / a f t e r e f f e c t s _ v i d e o b o o t h / v i d e o b o o t h _ m a s t e r . a e p      j   	 �� �� 00 incomingfileextensions incomingFileExtensions  J   	         m   	 
 ! ! � " "  . j p g    # $ # m   
  % % � & & 
 . j p e g $  ' ( ' m     ) ) � * *  . p n g (  + , + m     - - � . .  . m p 4 ,  /�� / m     0 0 � 1 1  . m o v��     2 3 2 j    �� 4�� 80 rawtranscodefileextensions rawTranscodeFileExtensions 4 J     5 5  6�� 6 m     7 7 � 8 8  . m o v��   3  9 : 9 j    &�� ;�� D0  bypassrawtranscodefileextensions  bypassRawTranscodeFileExtensions ; J    % < <  = > = m     ? ? � @ @  . j p g >  A B A m     C C � D D 
 . j p e g B  E F E m     G G � H H  . p n g F  I�� I m    ! J J � K K  . m p 4��   :  L M L j   ' )�� N�� 60 statsintervalcheckminutes statsIntervalCheckMinutes N m   ' (����  M  O P O j   * ,�� Q�� $0 processdelaysecs processDelaySecs Q m   * +����  P  R S R j   - 1�� T�� 0 loopdelaysecs loopDelaySecs T m   - 0���� 
 S  U V U j   2 4�� W�� 0 	automated   W m   2 3��
�� boovfals V  X Y X j   5 7�� Z�� 0 devendpoints devEndpoints Z m   5 6��
�� boovfals Y  [ \ [ j   8 <�� ]�� $0 computerusername computerUserName ] m   8 ; ^ ^ � _ _  b l u e p i x e l \  ` a ` j   = A�� b�� $0 waivergrepstring waiverGrepString b m   = @ c c � d d  - e   . x m l a  e f e j   B F�� g�� >0 filesizecheck_minaeoutputsize filesizeCheck_minAEOutputSize g m   B E���� ��@ f  h i h j   G I�� j�� (0 filesizecheckdelay filesizeCheckDelay j m   G H����  i  k l k j   J L�� m�� (0 performfilecleanup performFileCleanup m m   J K��
�� boovtrue l  n o n j   M Q�� p�� 0 dev_mode   p m   M N��
�� boovfals o  q r q l      s t u s j   R X�� v�� 00 oldestfileagethreshold oldestFileAgeThreshold v m   R U����  t  seconds    u � w w  s e c o n d s r  x y x l      z { | z j   Y _�� }�� 00 expectedmaxprocesstime expectedMaxProcessTime } m   Y \���� � {  seconds    | � ~ ~  s e c o n d s y   �  j   ` f�� ��� "0 applicationname applicationName � m   ` c � � � � � $ V i d e o b o o t h   P r o c e s s �  � � � j   g m�� ��� $0 maxfilespercycle maxFilesPerCycle � m   g j���� 
 �  � � � l     ��������  ��  ��   �  � � � p   n n � � ������ 00 pendingaewaiverlistold pendingAEWaiverListOLD��   �  � � � p   n n � � ������ .0 pendingamefilelistold pendingAMEFileListOLD��   �  � � � p   n n � � ������ :0 pendingdropboxwaiverlistold pendingDropboxWaiverListOLD��   �  � � � l     ��������  ��  ��   �  � � � p   n n � � �� ��� "0 dropboxrootpath dropboxRootPath � �� ��� .0 incomingrawxmlwaivers incomingRawXmlWaivers � �� ��� :0 incomingrawmediafolderposix incomingRawMediaFolderPosix � ������ %0 !waivers_pending_completed_archive  ��   �  � � � p   n n � � ������ 0 stationtype stationType��   �  � � � p   n n � � �� ��� :0 transcodependingfolderposix transcodePendingFolderPosix � ������ >0 transcodecompletedfolderposix transcodeCompletedFolderPosix��   �  � � � p   n n � � �� ��� @0 pendingwaiversforaefolderposix pendingWaiversForAEFolderPosix � ������ 0 
media_pool  ��   �  � � � p   n n � � �� ��� 0 computer_id   � ������ 0 ishappy isHappy��   �  � � � p   n n � � �� ��� 0 processed_media   � �� ��� 0 	ae_output   � �� ��� 0 processed_images   � �� ��� 0 	ame_watch   � ������ 0 spock_ingestor  ��   �  � � � p   n n � � ������ 80 laststatsgatheredtimestamp lastStatsGatheredTimestamp��   �  � � � p   n n � � �� ��� :0 laststalefilechecktimestamp lastStaleFileCheckTimestamp � ������ *0 forcestalefilecheck forceStaleFileCheck��   �  � � � l     ��������  ��  ��   �  � � � i   n q � � � I     ������
�� .aevtoappnull  �   � ****��  ��   � k     � � �  � � � I    �� ���
�� .ascrcmnt****      � **** � b      � � � l     ����� � I    ������
�� .misccurdldt    ��� null��  ��  ��  ��   � m     � � � � �  L a u n c h i n g��   �  � � � I    �� ����� $0 shownotification showNotification �  � � � m     � � � � �  L a u n c h i n g �  ��� � m     � � � � � , c h e c k i n g   s y s t e m   s t a t u s��  ��   �  � � � l   ��������  ��  ��   �  � � � Z    8 � ����� � =     � � � I    �� ����� 0 
fileexists 
fileExists �  ��� � o    ���� 0 theplistpath thePListPath��  ��   � m    ��
�� boovfals � k   # 4 � �  � � � I  # .�� ���
�� .sysodisAaleR        TEXT � b   # * � � � m   # $ � � � � � P A b o r t i n g   b e c a u s e   c o n f i g   f i l e   n o t   f o u n d :   � o   $ )���� 0 theplistpath thePListPath��   �  ��� � I  / 4������
�� .aevtquitnull��� ��� null��  ��  ��  ��  ��   �  � � � l  9 9��������  ��  ��   �  � � � r   9 < � � � m   9 :��
�� boovtrue � o      ���� 0 ishappy isHappy �  � � � r   = D � � � I   = B�������� 0 setvars setVars��  ��   � o      ���� 0 isready1 isReady1 �  � � � r   E L � � � I   E J�������� 0 setpaths setPaths��  ��   � o      ���� 0 isready2 isReady2 �  � � � Z   M z � ��� � � F   M X � � � =  M P � � � o   M N���� 0 isready1 isReady1 � m   N O�
� boovtrue � =  S V � � � o   S T�~�~ 0 isready2 isReady2 � m   T U�}
�} boovtrue � k   [ h � �  � � � I   [ a�| ��{�| 0 gatherstats gatherStats �  �z  m   \ ]�y�y  �z  �{   � �x I   b h�w�v�w "0 processonecycle processOneCycle �u m   c d�t
�t boovtrue�u  �v  �x  ��   � I  k z�s�r
�s .sysodisAaleR        TEXT b   k v b   k t b   k p	
	 m   k n � & A b o r t i n g .   s e t V a r   =  
 o   n o�q�q 0 isready1 isReady1 m   p s �    s e t P a t h s   =   o   t u�p�p 0 isready2 isReady2�r   �  I  { ��o�n
�o .ascrcmnt****      � **** b   { � l  { ��m�l I  { ��k�j�i
�k .misccurdldt    ��� null�j  �i  �m  �l   m   � � � t D o n e   w i t h   i n i t i a l   c y c l e   -   w a i t i n g   3 0 s e c   f o r   o n I d l e   h a n d l e r�n   �h l  � ��g�f�e�g  �f  �e  �h   �  l     �d�c�b�d  �c  �b    i   r u I     �a�`�_
�a .miscidlenmbr    ��� null�`  �_   k     !   I     �^!�]�^ "0 processonecycle processOneCycle! "�\" m    �[
�[ boovfals�\  �]    #$# I   �Z%�Y
�Z .ascrcmnt****      � ****% b    &'& b    ()( b    *+* l   ,�X�W, I   �V�U�T
�V .misccurdldt    ��� null�U  �T  �X  �W  + m    -- �.. 2 D o n e   w i t h   c y c l e   -   w a i t i n g) o    �S�S 0 loopdelaysecs loopDelaySecs' m    // �00 
   s e c s�Y  $ 1�R1 L    !22 o     �Q�Q 0 loopdelaysecs loopDelaySecs�R   343 l     �P�O�N�P  �O  �N  4 565 i   v y787 I     �M�L�K
�M .aevtquitnull��� ��� null�L  �K  8 k     99 :;: I    �J<�I
�J .ascrcmnt****      � ****< b     =>= l    ?�H�G? I    �F�E�D
�F .misccurdldt    ��� null�E  �D  �H  �G  > m    @@ �AA  E x i t i n g   A p p�I  ; BCB I    �CD�B�C 0 gatherstats gatherStatsD E�AE m    �@�@  �A  �B  C F�?F M    GG I     �>�=�<
�> .aevtquitnull��� ��� null�=  �<  �?  6 HIH l     �;�:�9�;  �:  �9  I JKJ i   z }LML I      �8N�7�8 $0 shownotification showNotificationN OPO o      �6�6  0 subtitlestring subtitleStringP Q�5Q o      �4�4 0 messagestring messageString�5  �7  M k     +RR STS Z     #UV�3WU >    XYX o     �2�2 0 messagestring messageStringY m    �1
�1 
nullV I   �0Z[
�0 .sysonotfnull��� ��� TEXTZ o    �/�/ 0 messagestring messageString[ �.\]
�. 
appr\ o    �-�- "0 applicationname applicationName] �,^�+
�, 
subt^ o    �*�*  0 subtitlestring subtitleString�+  �3  W I   #�)�(_
�) .sysonotfnull��� ��� TEXT�(  _ �'`a
�' 
subt` o    �&�&  0 subtitlestring subtitleStringa �%b�$
�% 
apprb o    �#�# "0 applicationname applicationName�$  T cdc l  $ )efge I  $ )�"h�!
�" .sysodelanull��� ��� nmbrh m   $ %� �  �!  f ) #allow time for notification to fire   g �ii F a l l o w   t i m e   f o r   n o t i f i c a t i o n   t o   f i r ed j�j l  * *����  �  �  �  K klk l     ����  �  �  l mnm l     ����  �  �  n opo i   ~ �qrq I      �s�� "0 processonecycle processOneCycles t�t o      ��  0 isinitialcycle isInitialCycle�  �  r k    'uu vwv I    �x�
� .ascrcmnt****      � ****x b     yzy l    {��{ I    ���
� .misccurdldt    ��� null�  �  �  �  z m    || �}}  S t a r t i n g   C y c l e�  w ~~ I    �
��	�
 $0 shownotification showNotification� ��� m    �� ���  S t a r t i n g   C y c l e� ��� m    �
� 
null�  �	   ��� l   ����  �  �  � ��� I    ���� 0 gatherstats gatherStats� ��� m    � �  �  �  � ��� I     �������� 0 handlewaivers handleWaivers��  ��  � ��� I  ! 2�����
�� .ascrcmnt****      � ****� b   ! .��� b   ! (��� l  ! &������ I  ! &������
�� .misccurdldt    ��� null��  ��  ��  ��  � m   & '�� ��� > D o n e   c h e c k i n g   w a i v e r s .   w a i t i n g  � o   ( -���� $0 processdelaysecs processDelaySecs��  � ��� I  3 <�����
�� .sysodelanull��� ��� nmbr� o   3 8���� $0 processdelaysecs processDelaySecs��  � ��� l  = =��������  ��  ��  � ��� I   = C������� 0 gatherstats gatherStats� ���� m   > ?���� ��  ��  � ��� I   D I�������� :0 transcodeaftereffectsoutput transcodeAfterEffectsOutput��  ��  � ��� l  J J������  � : 4 TODO improve to have variable for clean file output   � ��� h   T O D O   i m p r o v e   t o   h a v e   v a r i a b l e   f o r   c l e a n   f i l e   o u t p u t� ��� I  J [�����
�� .ascrcmnt****      � ****� b   J W��� b   J Q��� l  J O������ I  J O������
�� .misccurdldt    ��� null��  ��  ��  ��  � m   O P�� ��� B d o n e   c h e c k i n g   A E   o u t p u t .   w a i t i n g  � o   Q V���� $0 processdelaysecs processDelaySecs��  � ��� I  \ e�����
�� .sysodelanull��� ��� nmbr� o   \ a���� $0 processdelaysecs processDelaySecs��  � ��� l  f f��������  ��  ��  � ��� I   f l������� 0 gatherstats gatherStats� ���� m   g h���� ��  ��  � ��� I   m r�������� ,0 movefilestoingestion moveFilesToIngestion��  ��  � ��� I   s x�������� .0 moveimagestoingestion moveImagesToIngestion��  ��  � ��� I  y ������
�� .ascrcmnt****      � ****� b   y ���� b   y ���� l  y ~������ I  y ~������
�� .misccurdldt    ��� null��  ��  ��  ��  � m   ~ �� ��� L d o n e   c h e c k i n g   e n c o d e r   o u t p u t .   w a i t i n g  � o   � ����� $0 processdelaysecs processDelaySecs��  � ��� I  � ������
�� .sysodelanull��� ��� nmbr� o   � ����� $0 processdelaysecs processDelaySecs��  � ��� l  � ���������  ��  ��  � ��� l  � �������  � 0 * clean out the raw .mov from encoder watch   � ��� T   c l e a n   o u t   t h e   r a w   . m o v   f r o m   e n c o d e r   w a t c h� ��� I   � ��������� 0 filecleanup fileCleanup��  ��  � ��� I  � ������
�� .ascrcmnt****      � ****� b   � ���� b   � ���� l  � ������� I  � �������
�� .misccurdldt    ��� null��  ��  ��  ��  � m   � ��� ��� R d o n e   c l e a n i n g   u p   e n c o d e r   s o u r c e .   w a i t i n g  � o   � ����� $0 processdelaysecs processDelaySecs��  � ��� I  � ������
�� .sysodelanull��� ��� nmbr� o   � ����� $0 processdelaysecs processDelaySecs��  � ��� l  � ���������  ��  ��  � ��� I   � �������� 0 gatherstats gatherStats� ���� m   � ����� ��  ��  � ��� I  � ������
�� .ascrcmnt****      � ****� b   � ���� l  � ������� I  � �������
�� .misccurdldt    ��� null��  ��  ��  ��  � m   � ��� ��� $ d o n e   s e n d i n g   s t a t s��  � ��� l  � ���������  ��  ��  � ��� Z   � ������� o   � �����  0 isinitialcycle isInitialCycle� I   � �������� $0 shownotification showNotification� ��� m   � ��� ��� , F i n i s h e d   I n i t i a l   C y c l e� ���� m   � ��� ��� B w a i t i n g   ~ 3 0 s e c   f o r   o n I d l e   h a n d l e r��  ��  ��  � I   � �������� $0 shownotification showNotification�    m   � � �  F i n i s h e d   C y c l e �� l  � ����� b   � � b   � �	 m   � �

 �  w a i t i n g  	 o   � ����� 0 loopdelaysecs loopDelaySecs m   � � � $ s e c   f o r   n e x t   c y c l e��  ��  ��  ��  �  l  � ���������  ��  ��    Z   �%���� =  � � o   � ����� 0 	automated   m   � ���
�� boovfals k   !  I  ����
�� .ascrcmnt****      � **** b   	 l  ���� I  ������
�� .misccurdldt    ��� null��  ��  ��  ��   m   � j D o n e   w i t h   c y c l e   -   e x i t i n g   b e c a u s e   a u t o m a t i o n   i s   f a l s e��     I ��!"
�� .sysodisAaleR        TEXT! m  ## �$$ j D o n e   w i t h   c y c l e   -   e x i t i n g   b e c a u s e   a u t o m a t i o n   i s   f a l s e" ��%��
�� 
givu% m  ���� ��    &��& I !������
�� .aevtquitnull��� ��� null��  ��  ��  ��  ��   '�' l &&�~�}�|�~  �}  �|  �  p ()( l     �{�z�y�{  �z  �y  ) *+* l     �x,-�x  , C =#############################################################   - �.. z # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #+ /0/ l     �w12�w  1 ) ### --DO NOT EDIT BELOW THIS LIINE--   2 �33 F # #   - - D O   N O T   E D I T   B E L O W   T H I S   L I I N E - -0 454 l     �v67�v  6 > 8## --THESE ARE ALL THE HANDLERS USED IN THE LOOP ABOVE--   7 �88 p # #   - - T H E S E   A R E   A L L   T H E   H A N D L E R S   U S E D   I N   T H E   L O O P   A B O V E - -5 9:9 l     �u;<�u  ; C =#############################################################   < �== z # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #: >?> l     �t�s�r�t  �s  �r  ? @A@ i   � �BCB I      �qD�p�q &0 checkpendingfiles checkPendingFilesD EFE o      �o�o 0 oldlist oldListF G�nG o      �m�m 0 newlist newList�n  �p  C k     �HH IJI r     KLK c     MNM J     �l�l  N m    �k
�k 
listL o      �j�j 0 
filesadded 
filesAddedJ OPO r    QRQ c    STS J    	�i�i  T m   	 
�h
�h 
listR o      �g�g 0 filesremoved filesRemovedP UVU r    WXW m    �f
�f boovfalsX o      �e�e 0 isprocessing isProcessingV YZY Z    �[\�d][ F    #^_^ l   `�c�b` @    aba n    cdc 1    �a
�a 
lengd o    �`�` 0 newlist newListb n    efe 1    �_
�_ 
lengf o    �^�^ 0 oldlist oldList�c  �b  _ l   !g�]�\g ?    !hih n    jkj 1    �[
�[ 
lengk o    �Z�Z 0 oldlist oldListi m     �Y�Y  �]  �\  \ k   & �ll mnm l  & &�X�W�V�X  �W  �V  n opo l  & &�Uqr�U  q   new/old is same and > 0   r �ss 0   n e w / o l d   i s   s a m e   a n d   >   0p tut Z   & 8vw�T�Sv =   & )xyx o   & '�R�R 0 newlist newListy o   ' (�Q�Q 0 oldlist oldListw k   , 4zz {|{ l  , ,�P}~�P  } &   error condition, not processing   ~ � @   e r r o r   c o n d i t i o n ,   n o t   p r o c e s s i n g| ��� I  , 1�O��N
�O .ascrcmnt****      � ****� m   , -�� ���  n o t   p r o c e s s i n g�N  � ��M� L   2 4�� m   2 3�L
�L boovfals�M  �T  �S  u ��� l  9 9�K�J�I�K  �J  �I  � ��� Y   9 d��H���G� k   I _�� ��� r   I O��� n   I M��� 4   J M�F�
�F 
cobj� o   K L�E�E 0 i  � o   I J�D�D 0 oldlist oldList� o      �C�C 0 n  � ��B� Z  P _���A�@� H   P T�� E  P S��� o   P Q�?�? 0 newlist newList� o   Q R�>�> 0 n  � r   W [��� o   W X�=�= 0 n  � n      ���  ;   Y Z� o   X Y�<�< 0 filesremoved filesRemoved�A  �@  �B  �H 0 i  � m   < =�;�; � I  = D�:��9
�: .corecnte****       ****� n   = @��� 2  > @�8
�8 
cobj� o   = >�7�7 0 oldlist oldList�9  �G  � ��� l  e e�6�5�4�6  �5  �4  � ��� Y   e ���3���2� k   u ��� ��� r   u {��� n   u y��� 4   v y�1�
�1 
cobj� o   w x�0�0 0 i  � o   u v�/�/ 0 newlist newList� o      �.�. 0 n  � ��-� Z  | ����,�+� H   | ��� E  | ��� o   | }�*�* 0 oldlist oldList� o   } ~�)�) 0 n  � r   � ���� o   � ��(�( 0 n  � n      ���  ;   � �� o   � ��'�' 0 
filesadded 
filesAdded�,  �+  �-  �3 0 i  � m   h i�&�& � I  i p�%��$
�% .corecnte****       ****� n   i l��� 2  j l�#
�# 
cobj� o   i j�"�" 0 newlist newList�$  �2  � ��� l  � ��!� ��!  �   �  � ��� I  � ����
� .ascrcmnt****      � ****� c   � ���� b   � ���� m   � ��� ���  f i l e s A d d e d :  � o   � ��� 0 
filesadded 
filesAdded� m   � ��
� 
TEXT�  � ��� I  � ����
� .ascrcmnt****      � ****� b   � ���� m   � ��� ���  f i l e s R e m o v e d :  � o   � ��� 0 filesremoved filesRemoved�  � ��� l  � �����  �  �  � ��� l  � �����  � ( " adding files but not removing any   � ��� D   a d d i n g   f i l e s   b u t   n o t   r e m o v i n g   a n y� ��� Z   � ������ F   � ���� l  � ����� ?   � ���� n   � ���� 1   � ��
� 
leng� o   � ��� 0 
filesadded 
filesAdded� m   � ���  �  �  � l  � ����� =   � ���� n   � ���� 1   � ��

�
 
leng� o   � ��	�	 0 filesremoved filesRemoved� m   � ���  �  �  � k   � ��� ��� l  � �����  � &   error condition, not processing   � ��� @   e r r o r   c o n d i t i o n ,   n o t   p r o c e s s i n g� ��� L   � ��� m   � ��
� boovfals�  �  �  � ��� l  � �����  �  �  � ��� l  � �����  � %  adding files AND removing them   � ��� >   a d d i n g   f i l e s   A N D   r e m o v i n g   t h e m� ��� Z   � ���� ��� F   � ���� l  � ������� @   � ���� n   � �   1   � ���
�� 
leng o   � ����� 0 
filesadded 
filesAdded� m   � �����  ��  ��  � l  � ����� ?   � � n   � � 1   � ���
�� 
leng o   � ����� 0 filesremoved filesRemoved m   � �����  ��  ��  � L   � � m   � ���
�� boovtrue�   ��  � �� l  � ���������  ��  ��  ��  �d  ] k   � �		 

 l  � �����   %  new/old are both 0 OR old is 0    � >   n e w / o l d   a r e   b o t h   0   O R   o l d   i s   0  l  � �����   A ; this is where we are when new files are added after a lull    � v   t h i s   i s   w h e r e   w e   a r e   w h e n   n e w   f i l e s   a r e   a d d e d   a f t e r   a   l u l l  I  � �����
�� .ascrcmnt****      � **** m   � � � 0 s k i p p i n g   l i s t   c o m p a r i s o n��   �� r   � � m   � ���
�� boovtrue o      ���� 0 isprocessing isProcessing��  Z  l  � ���������  ��  ��    L   � �   o   � ����� 0 isprocessing isProcessing !��! l  � ���������  ��  ��  ��  A "#" l     ��������  ��  ��  # $%$ i   � �&'& I      �������� 0 filecleanup fileCleanup��  ��  ' k     '(( )*) Z     %+,��-+ =    ./. o     ���� (0 performfilecleanup performFileCleanup/ m    ��
�� boovtrue, k   
 !00 121 r   
 343 m   
 55 �66 h r m   - r f   / U s e r s / b l u e p i x e l / D o c u m e n t s / a m e _ w a t c h / S o u r c e / *4 o      ���� 0 shscript  2 787 Q    9:��9 I   ��;��
�� .sysoexecTEXT���     TEXT; o    ���� 0 shscript  ��  : R      ��<��
�� .ascrerr ****      � ****< o      ���� 
0 errmsg  ��  ��  8 =>= l     ��������  ��  ��  > ?��? l     ��@A��  @   DO NOTHING   A �BB    D O   N O T H I N G��  ��  - l  $ $��CD��  C  
do nothing   D �EE  d o   n o t h i n g* FGF l  & &��������  ��  ��  G H��H l  & &��������  ��  ��  ��  % IJI l     ��������  ��  ��  J KLK l     ��������  ��  ��  L MNM i   � �OPO I      �������� ,0 movefilestoingestion moveFilesToIngestion��  ��  P k    �QQ RSR r     TUT o     ���� 0 processed_media  U o      ���� 0 myfolder  S VWV r    	XYX n    Z[Z 1    ��
�� 
psxp[ o    ���� 0 processed_media  Y o      ���� 0 	pmyfolder  W \]\ l  
 
��������  ��  ��  ] ^_^ I  
 ��`��
�� .ascrcmnt****      � ****` b   
 aba l  
 c����c I  
 ������
�� .misccurdldt    ��� null��  ��  ��  ��  b m    dd �ee   s t a r t i n g   u p l o a d s��  _ fgf r    hih b    jkj b    lml m    nn �oo  l s  m n    pqp 1    ��
�� 
strqq o    ���� 0 	pmyfolder  k m    rr �ss    |   g r e p   ' . m p 4 'i o      ���� 0 shscript  g tut l     ��������  ��  ��  u vwv Q     Pxyzx r   # .{|{ c   # ,}~} n   # *� 2  ( *��
�� 
cpar� l  # (������ I  # (�����
�� .sysoexecTEXT���     TEXT� o   # $���� 0 shscript  ��  ��  ��  ~ m   * +��
�� 
list| o      ���� 0 filelist  y R      �����
�� .ascrerr ****      � ****� o      ���� 
0 errmsg  ��  z k   6 P�� ��� I  6 C�����
�� .ascrcmnt****      � ****� b   6 ?��� b   6 =��� l  6 ;������ I  6 ;������
�� .misccurdldt    ��� null��  ��  ��  ��  � m   ; <�� ���  u p l o a d   e r r o r :  � o   = >���� 
0 errmsg  ��  � ��� I  D I�����
�� .ascrcmnt****      � ****� m   D E�� ��� z i f   e r r o r   1   i t   p r o b a b l y   m e a n s   t h e r e   a r e   n o   f i l e s   t o   u p l o a d   y e t��  � ���� r   J P��� c   J N��� J   J L����  � m   L M��
�� 
list� o      ���� 0 filelist  ��  w ��� l  Q Q��������  ��  ��  � ��� l  Q T���� r   Q T��� m   Q R����  � o      ���� 0 
cyclecount 
cycleCount� . (limit how many files we do on each cycle   � ��� P l i m i t   h o w   m a n y   f i l e s   w e   d o   o n   e a c h   c y c l e� ��� X   U������ k   g��� ��� Z   g v������� ?   g n��� o   g h���� 0 
cyclecount 
cycleCount� o   h m���� $0 maxfilespercycle maxFilesPerCycle�  S   q r��  ��  � ��� l  w w�������  ��  �  � ��� r   w ���� c   w ~��� b   w z��� o   w x�~�~ 0 myfolder  � o   x y�}�} 0 i  � m   z }�|
�| 
TEXT� o      �{�{ 
0 myfile  � ��� r   � ���� o   � ��z�z 0 i  � o      �y�y 0 newfile  � ��� l  � ��x�w�v�x  �w  �v  � ��� l  � ��u�t�s�u  �t  �s  � ��� Z   �����r�q� =  � ���� I   � ��p��o�p ,0 checkfilewritestatus checkFileWriteStatus� ��� o   � ��n�n 
0 myfile  � ��� m   � ��m�m  B@� ��l� m   � ��k�k �l  �o  � m   � ��j
�j boovtrue� k   ���� ��� l  � ��i���i  � , &upload to Dropbox folder for ingestion   � ��� L u p l o a d   t o   D r o p b o x   f o l d e r   f o r   i n g e s t i o n� ��� r   � ���� b   � ���� b   � ���� b   � ���� b   � ���� m   � ��� ���  m v  � n   � ���� 1   � ��h
�h 
strq� o   � ��g�g 
0 myfile  � m   � ��� ���   � n   � ���� 1   � ��f
�f 
strq� o   � ��e�e 0 spock_ingestor  � o   � ��d�d 0 newfile  � o      �c�c 0 shscript  � ��� l  � ��b�a�`�b  �a  �`  � ��� Q   � ����� k   � ��� ��� I  � ��_��^
�_ .ascrcmnt****      � ****� b   � ���� l  � ���]�\� I  � ��[�Z�Y
�[ .misccurdldt    ��� null�Z  �Y  �]  �\  � m   � ��� ���  s t a r t i n g   u p l o a d�^  � ��X� I  � ��W��V
�W .sysoexecTEXT���     TEXT� o   � ��U�U 0 shscript  �V  �X  � R      �T��S
�T .ascrerr ****      � ****� o      �R�R 
0 errmsg  �S  � k   � ��� ��� I  � ��Q��P
�Q .ascrcmnt****      � ****� b   � ���� b   � �� � l  � ��O�N I  � ��M�L�K
�M .misccurdldt    ��� null�L  �K  �O  �N    m   � � � ( e r r o r   i n   u p l o a d i n g :  � o   � ��J�J 
0 errmsg  �P  �  I  � ��I�H�G
�I .sysobeepnull��� ��� long�H  �G    I  � ��F�E�D
�F .sysobeepnull��� ��� long�E  �D   	 I  � ��C�B�A
�C .sysobeepnull��� ��� long�B  �A  	 

 I  � ��@
�@ .sysodisAaleR        TEXT b   � � m   � � � > A n   e r r o r   o c c u r e d   i n   u p l o a d i n g :   o   � ��?�? 
0 errmsg   �>�=
�> 
givu m   � ��<�< �=   �;  S   � ��;  �  l  � ��:�9�8�:  �9  �8    l  � ��7�7   6 0cleanup intermediate raw files to save HDD space    � ` c l e a n u p   i n t e r m e d i a t e   r a w   f i l e s   t o   s a v e   H D D   s p a c e  r   � I   �	�6�5�6 0 	changeext 	changeExt  !  c   �"#" o   � ��4�4 0 i  # m   ��3
�3 
TEXT! $�2$ m  %% �&&  �2  �5   o      �1�1 0 basefilename baseFilename '(' r  !)*) b  +,+ b  -.- b  /0/ b  121 m  33 �44  l s  2 n  565 1  �0
�0 
strq6 o  �/�/ 0 
media_pool  0 m  77 �88    |   g r e p   - e  . o  �.�. 0 basefilename baseFilename, m  99 �::  ** o      �-�- 0 shscript  ( ;<; Q  "V=>?= r  %0@A@ c  %.BCB n  %,DED 2 *,�,
�, 
cparE l %*F�+�*F I %*�)G�(
�) .sysoexecTEXT���     TEXTG o  %&�'�' 0 shscript  �(  �+  �*  C m  ,-�&
�& 
listA o      �%�% $0 intermediatelist intermediateList> R      �$H�#
�$ .ascrerr ****      � ****H o      �"�" 
0 errmsg  �#  ? k  8VII JKJ I 8G�!L� 
�! .ascrcmnt****      � ****L b  8CMNM b  8AOPO l 8=Q��Q I 8=���
� .misccurdldt    ��� null�  �  �  �  P m  =@RR �SS  u p l o a d   e r r o r :  N o  AB�� 
0 errmsg  �   K TUT I HO�V�
� .ascrcmnt****      � ****V m  HKWW �XX z i f   e r r o r   1   i t   p r o b a b l y   m e a n s   t h e r e   a r e   n o   f i l e s   t o   u p l o a d   y e t�  U Y�Y r  PVZ[Z c  PT\]\ J  PR��  ] m  RS�
� 
list[ o      �� $0 intermediatelist intermediateList�  < ^_^ l WW����  �  �  _ `a` X  W�b�cb k  i�dd efe r  i~ghg c  i|iji b  ixklk b  ivmnm b  iropo m  ilqq �rr  r m  p n  lqsts 1  oq�
� 
strqt o  lo�� 0 
media_pool  n m  ruuu �vv  /l o  vw�� 0 i  j m  x{�
� 
TEXTh o      �� 0 shscript  f wxw Q  �yz{y I ���
|�	
�
 .sysoexecTEXT���     TEXT| o  ���� 0 shscript  �	  z R      �}�
� .ascrerr ****      � ****} o      �� 
0 errmsg  �  { I ���~�
� .ascrcmnt****      � ****~ b  ��� b  ����� l ������ I ��� ����
�  .misccurdldt    ��� null��  ��  �  �  � m  ���� ��� 8 i n t e r m e d i a t e   c l e a n u p   e r r o r :  � o  ������ 
0 errmsg  �  x ���� l ����������  ��  ��  ��  � 0 i  c o  Z[���� $0 intermediatelist intermediateLista ��� l ����������  ��  ��  � ��� r  ����� [  ����� o  ������ 0 
cyclecount 
cycleCount� m  ������ � o      ���� 0 
cyclecount 
cycleCount� ��� l ����������  ��  ��  � ���� l ��������  �  fileReady end if   � ���   f i l e R e a d y   e n d   i f��  �r  �q  � ���� l ����������  ��  ��  ��  �� 0 i  � o   X Y���� 0 filelist  � ��� I �������
�� .ascrcmnt****      � ****� b  ����� l �������� I ��������
�� .misccurdldt    ��� null��  ��  ��  ��  � m  ���� ��� V d o n e   w i t h   u p l o a d i n g .   W a i t i n g   f o r   n e x t   c y c l e��  � ���� l ����������  ��  ��  ��  N ��� l     ��������  ��  ��  � ��� i   � ���� I      �������� .0 moveimagestoingestion moveImagesToIngestion��  ��  � k    ��� ��� r     ��� o     ���� 0 processed_images  � o      ���� 0 myfolder  � ��� r    	��� n    ��� 1    ��
�� 
psxp� o    ���� 0 myfolder  � o      ���� 0 	pmyfolder  � ��� l  
 
��������  ��  ��  � ��� I  
 �����
�� .ascrcmnt****      � ****� b   
 ��� l  
 ������ I  
 ������
�� .misccurdldt    ��� null��  ��  ��  ��  � m    �� ���   s t a r t i n g   u p l o a d s��  � ��� r    ��� b    ��� b    ��� m    �� ���  l s  � n    ��� 1    ��
�� 
strq� o    ���� 0 	pmyfolder  � m    �� ���    |   g r e p   ' . p n g '� o      ���� 0 shscript  � ��� l     ��������  ��  ��  � ��� Q     P���� r   # .��� c   # ,��� n   # *��� 2  ( *��
�� 
cpar� l  # (������ I  # (�����
�� .sysoexecTEXT���     TEXT� o   # $���� 0 shscript  ��  ��  ��  � m   * +��
�� 
list� o      ���� 0 filelist  � R      �����
�� .ascrerr ****      � ****� o      ���� 
0 errmsg  ��  � k   6 P�� ��� I  6 C�����
�� .ascrcmnt****      � ****� b   6 ?��� b   6 =��� l  6 ;������ I  6 ;������
�� .misccurdldt    ��� null��  ��  ��  ��  � m   ; <�� ���  u p l o a d   e r r o r :  � o   = >���� 
0 errmsg  ��  � ��� I  D I�����
�� .ascrcmnt****      � ****� m   D E�� ��� z i f   e r r o r   1   i t   p r o b a b l y   m e a n s   t h e r e   a r e   n o   f i l e s   t o   u p l o a d   y e t��  � ���� r   J P��� c   J N��� J   J L����  � m   L M��
�� 
list� o      ���� 0 filelist  ��  � ��� l  Q Q��������  ��  ��  � ��� l  Q T���� r   Q T��� m   Q R����  � o      ���� 0 
cyclecount 
cycleCount� . (limit how many files we do on each cycle   � ��� P l i m i t   h o w   m a n y   f i l e s   w e   d o   o n   e a c h   c y c l e� ��� X   U������ k   g��� ��� Z   g v������� ?   g n   o   g h���� 0 
cyclecount 
cycleCount o   h m���� $0 maxfilespercycle maxFilesPerCycle�  S   q r��  ��  �  l  w w��������  ��  ��    r   w � c   w ~	 b   w z

 o   w x���� 0 myfolder   o   x y���� 0 i  	 m   z }��
�� 
TEXT o      ���� 
0 myfile    r   � � I   � ������� 0 	changeext 	changeExt  c   � � o   � ����� 0 i   m   � ���
�� 
TEXT �� m   � � �  . j p g��  ��   o      ���� 0 newfile    l  � ���������  ��  ��    Z   ������ =  � � I   � ��� ���� ,0 checkfilewritestatus checkFileWriteStatus  !"! o   � ����� 
0 myfile  " #$# m   � �����   �P$ %��% m   � ����� ��  ��   m   � ���
�� boovtrue k   ��&& '(' l  � ���)*��  ) 7 1 transcode to JPG in output path, remove original   * �++ b   t r a n s c o d e   t o   J P G   i n   o u t p u t   p a t h ,   r e m o v e   o r i g i n a l( ,-, l  � ���������  ��  ��  - ./. l  � ��������  ��  �  / 010 r   � �232 b   � �454 b   � �676 b   � �898 b   � �:;: b   � �<=< b   � �>?> m   � �@@ �AA P s i p s   - s   f o r m a t   j p e g   - s   f o r m a t O p t i o n s   8 0  ? n   � �BCB 1   � ��~
�~ 
strqC o   � ��}�} 
0 myfile  = m   � �DD �EE    - - o u t  ; n   � �FGF 1   � ��|
�| 
strqG l  � �H�{�zH b   � �IJI o   � ��y�y 0 spock_ingestor  J o   � ��x�x 0 newfile  �{  �z  9 m   � �KK �LL 
 ;   r m  7 n   � �MNM 1   � ��w
�w 
strqN o   � ��v�v 
0 myfile  5 m   � �OO �PP  3 o      �u�u 0 shscript  1 QRQ l  � ��t�s�r�t  �s  �r  R STS Q   �UVWU k   � �XX YZY I  � ��q[�p
�q .ascrcmnt****      � ****[ b   � �\]\ l  � �^�o�n^ I  � ��m�l�k
�m .misccurdldt    ��� null�l  �k  �o  �n  ] m   � �__ �``  s t a r t i n g   u p l o a d�p  Z a�ja I  � ��ib�h
�i .sysoexecTEXT���     TEXTb o   � ��g�g 0 shscript  �h  �j  V R      �fc�e
�f .ascrerr ****      � ****c o      �d�d 
0 errmsg  �e  W k   �dd efe I  � ��cg�b
�c .ascrcmnt****      � ****g b   � �hih b   � �jkj l  � �l�a�`l I  � ��_�^�]
�_ .misccurdldt    ��� null�^  �]  �a  �`  k m   � �mm �nn ( e r r o r   i n   u p l o a d i n g :  i o   � ��\�\ 
0 errmsg  �b  f opo I  � ��[�Z�Y
�[ .sysobeepnull��� ��� long�Z  �Y  p qrq I  � ��X�W�V
�X .sysobeepnull��� ��� long�W  �V  r sts I  ��U�T�S
�U .sysobeepnull��� ��� long�T  �S  t uvu I �Rwx
�R .sysodisAaleR        TEXTw b  yzy m  {{ �|| > A n   e r r o r   o c c u r e d   i n   u p l o a d i n g :  z o  �Q�Q 
0 errmsg  x �P}�O
�P 
givu} m  
�N�N �O  v ~�M~  S  �M  T � l �L�K�J�L  �K  �J  � ��� l �I���I  � 6 0cleanup intermediate raw files to save HDD space   � ��� ` c l e a n u p   i n t e r m e d i a t e   r a w   f i l e s   t o   s a v e   H D D   s p a c e� ��� r  #��� I  !�H��G�H 0 	changeext 	changeExt� ��� c  ��� o  �F�F 0 i  � m  �E
�E 
TEXT� ��D� m  �� ���  �D  �G  � o      �C�C 0 basefilename baseFilename� ��� r  $9��� b  $7��� b  $3��� b  $1��� b  $-��� m  $'�� ���  l s  � n  ',��� 1  *,�B
�B 
strq� o  '*�A�A 0 
media_pool  � m  -0�� ���    |   g r e p   - e  � o  12�@�@ 0 basefilename baseFilename� m  36�� ���  *� o      �?�? 0 shscript  � ��� Q  :n���� r  =H��� c  =F��� n  =D��� 2 BD�>
�> 
cpar� l =B��=�<� I =B�;��:
�; .sysoexecTEXT���     TEXT� o  =>�9�9 0 shscript  �:  �=  �<  � m  DE�8
�8 
list� o      �7�7 $0 intermediatelist intermediateList� R      �6��5
�6 .ascrerr ****      � ****� o      �4�4 
0 errmsg  �5  � k  Pn�� ��� I P_�3��2
�3 .ascrcmnt****      � ****� b  P[��� b  PY��� l PU��1�0� I PU�/�.�-
�/ .misccurdldt    ��� null�.  �-  �1  �0  � m  UX�� ���  u p l o a d   e r r o r :  � o  YZ�,�, 
0 errmsg  �2  � ��� I `g�+��*
�+ .ascrcmnt****      � ****� m  `c�� ��� z i f   e r r o r   1   i t   p r o b a b l y   m e a n s   t h e r e   a r e   n o   f i l e s   t o   u p l o a d   y e t�*  � ��)� r  hn��� c  hl��� J  hj�(�(  � m  jk�'
�' 
list� o      �&�& $0 intermediatelist intermediateList�)  � ��� l oo�%�$�#�%  �$  �#  � ��� X  o���"�� k  ���� ��� r  ����� c  ����� b  ����� b  ����� b  ����� m  ���� ���  r m  � n  ����� 1  ���!
�! 
strq� o  ��� �  0 
media_pool  � m  ���� ���  /� o  ���� 0 i  � m  ���
� 
TEXT� o      �� 0 shscript  � ��� Q  ������ I �����
� .sysoexecTEXT���     TEXT� o  ���� 0 shscript  �  � R      ���
� .ascrerr ****      � ****� o      �� 
0 errmsg  �  � I �����
� .ascrcmnt****      � ****� b  ����� b  ����� l ������ I �����
� .misccurdldt    ��� null�  �  �  �  � m  ���� ��� 8 i n t e r m e d i a t e   c l e a n u p   e r r o r :  � o  ���� 
0 errmsg  �  � ��� l ������  �  �  �  �" 0 i  � o  rs�
�
 $0 intermediatelist intermediateList� ��� l ���	���	  �  �  � ��� r  ����� [  ����� o  ���� 0 
cyclecount 
cycleCount� m  ���� � o      �� 0 
cyclecount 
cycleCount� ��� l ������  �  �  � �� � l ��������  �  fileReady end if   � ���   f i l e R e a d y   e n d   i f�   ��  ��    ��  l ����������  ��  ��  ��  �� 0 i  � o   X Y���� 0 filelist  �  I ������
�� .ascrcmnt****      � **** b  �� l ������ I ��������
�� .misccurdldt    ��� null��  ��  ��  ��   m  �� � b d o n e   w i t h   i m a g e   u p l o a d i n g .   W a i t i n g   f o r   n e x t   c y c l e��   	��	 l ����������  ��  ��  ��  � 

 l     ��������  ��  ��    l     ��������  ��  ��    i   � � I      �������� :0 transcodeaftereffectsoutput transcodeAfterEffectsOutput��  ��   k    ~  l      ����   7 1 
TODO improve to take varaible for clean files 
    � b   
 T O D O   i m p r o v e   t o   t a k e   v a r a i b l e   f o r   c l e a n   f i l e s   
  r      o     ���� 0 	ae_output   o      ���� 0 myfolder    r     c     !  n    	"#" 1    	��
�� 
psxp# l   $����$ b    %&% o    ���� @0 pendingwaiversforaefolderposix pendingWaiversForAEFolderPosix& m    '' �((  c o m p l e t e /��  ��  ! m   	 
��
�� 
TEXT o      ���� 0 completed_waivers   )*) l   ��������  ��  ��  * +,+ I   ��-��
�� .ascrcmnt****      � ****- b    ./. l   0����0 I   ������
�� .misccurdldt    ��� null��  ��  ��  ��  / m    11 �22 & s t a r t i n g   t r a n s c o d e s��  , 343 r    #565 b    !787 b    9:9 m    ;; �<<  l s  : n    =>= 1    ��
�� 
strq> o    ���� 0 completed_waivers  8 m     ?? �@@    |   g r e p   - e   . x m l6 o      ���� 0 shscript  4 ABA l  $ $��������  ��  ��  B CDC Q   $ TEFGE r   ' 2HIH c   ' 0JKJ n   ' .LML 2  , .��
�� 
cparM l  ' ,N����N I  ' ,��O��
�� .sysoexecTEXT���     TEXTO o   ' (���� 0 shscript  ��  ��  ��  K m   . /��
�� 
listI o      ���� 0 filelist  F R      ��P��
�� .ascrerr ****      � ****P o      ���� 
0 errmsg  ��  G k   : TQQ RSR I  : E��T��
�� .ascrcmnt****      � ****T b   : AUVU l  : ?W����W I  : ?������
�� .misccurdldt    ��� null��  ��  ��  ��  V o   ? @���� 
0 errmsg  ��  S XYX I  F M��Z��
�� .ascrcmnt****      � ****Z m   F I[[ �\\ > p r o b a b l y   b e c a u s e   n o   w a i v e r s   y e t��  Y ]��] r   N T^_^ c   N R`a` J   N P����  a m   P Q��
�� 
list_ o      ���� 0 filelist  ��  D bcb l  U U��������  ��  ��  c ded l  U Xfghf r   U Xiji m   U V����  j o      ���� 0 
cyclecount 
cycleCountg . (limit how many files we do on each cycle   h �kk P l i m i t   h o w   m a n y   f i l e s   w e   d o   o n   e a c h   c y c l ee lml X   Ynn��on k   mipp qrq l  m m��������  ��  ��  r sts r   m zuvu I   m x��w���� 0 	changeext 	changeExtw xyx l  n qz����z c   n q{|{ o   n o���� 0 i  | m   o p��
�� 
TEXT��  ��  y }��} m   q t~~ �  . m o v��  ��  v o      ����  0 outputfilename outputFilenamet ��� r   { ���� I   { �������� 0 	changeext 	changeExt� ��� l  | ������ c   | ��� o   | }���� 0 i  � m   } ~��
�� 
TEXT��  ��  � ���� m    ��� ���  _ c l e a n . m o v��  ��  � o      ���� *0 outputfilenameclean outputFilenameClean� ��� l  � ���������  ��  ��  � ��� Z   � �������� ?   � ���� o   � ����� 0 
cyclecount 
cycleCount� o   � ����� $0 maxfilespercycle maxFilesPerCycle�  S   � ���  ��  � ��� l  � ���������  ��  ��  � ��� l  � �������  �  check for finished file   � ��� . c h e c k   f o r   f i n i s h e d   f i l e� ��� r   � ���� m   � �����  � o      ���� 0 	filesize1  � ��� r   � ���� m   � ����� � o      ���� 0 	filesize2  � ��� r   � ���� m   � �����  � o      ����  0 filesize1clean filesize1Clean� ��� r   � ���� m   � ����� � o      ����  0 filesize2clean filesize2Clean� ��� l  � ���������  ��  ��  � ��� r   � ���� m   � ���
�� boovfals� o      ���� 0 skipthisfile skipThisFile� ��� r   � ���� m   � ���
�� boovfals� o      ���� &0 skipthisfileclean skipThisFileClean� ��� r   � ���� m   � ���
�� boovfals� o      �� 0 
fileexists 
fileExists� ��� l  � ��~�}�|�~  �}  �|  � ��� T   ���� k   ���� ��� l  � ��{�z�y�{  �z  �y  � ��� I  � ��x��w
�x .ascrcmnt****      � ****� b   � ���� l  � ���v�u� I  � ��t�s�r
�t .misccurdldt    ��� null�s  �r  �v  �u  � m   � ��� ��� $ c h e c k i n g   f i l e   s i z e�w  � ��� r   � ���� c   � ���� b   � ���� o   � ��q�q 0 myfolder  � o   � ��p�p  0 outputfilename outputFilename� m   � ��o
�o 
TEXT� o      �n�n 
0 myfile  � ��� Z   � ����m�� I   � ��l��k�l 0 
fileexists 
fileExists� ��j� o   � ��i�i 
0 myfile  �j  �k  � k   � ��� ��� r   � ���� l  � ���h�g� I  � ��f��e
�f .rdwrgeofcomp       ****� 4   � ��d�
�d 
psxf� o   � ��c�c 
0 myfile  �e  �h  �g  � o      �b�b 0 	filesize1  � ��a� r   � ���� m   � ��`
�` boovtrue� o      �_�_ 0 
fileexists 
fileExists�a  �m  � r   � ���� m   � ��^�^  � o      �]�] 0 	filesize1  � ��� I  ��\��[
�\ .ascrcmnt****      � ****� b   ���� b   � ���� l  � ���Z�Y� I  � ��X�W�V
�X .misccurdldt    ��� null�W  �V  �Z  �Y  � m   � ��� ���  f i l e   s i z e   =  � o   � �U�U 0 	filesize1  �[  � ��� Z  !���T�S� ?  ��� o  �R�R 0 	filesize1  � o  �Q�Q >0 filesizecheck_minaeoutputsize filesizeCheck_minAEOutputSize� k  �� ��� I �P��O
�P .ascrcmnt****      � ****� m  �� ��� * f i l e s i z e 1   >   c h e c k s i z e�O  � ��N� I �M��L
�M .sysodelanull��� ��� nmbr� m  �K�K �L  �N  �T  �S  � ��� Z  "8 �J�I  o  "#�H�H 0 
fileexists 
fileExists r  &4 l &2�G�F I &2�E�D
�E .rdwrgeofcomp       **** 4  &.�C
�C 
psxf o  *-�B�B 
0 myfile  �D  �G  �F   o      �A�A 0 	filesize2  �J  �I  �  I 9H�@	�?
�@ .ascrcmnt****      � ****	 b  9D

 b  9B l 9>�>�= I 9>�<�;�:
�< .misccurdldt    ��� null�;  �:  �>  �=   m  >A �  f i l e   s i z e   =   o  BC�9�9 0 	filesize2  �?    l II�8�7�6�8  �7  �6    l II�5�5   % , filesizeCheck_minAEOutputSize    � > ,   f i l e s i z e C h e c k _ m i n A E O u t p u t S i z e  Z  Ip�4�3 F  IZ l IL�2�1 =  IL  o  IJ�0�0 0 	filesize1    o  JK�/�/ 0 	filesize2  �2  �1   l OV!�.�-! ?  OV"#" o  OP�,�, 0 	filesize2  # o  PU�+�+ >0 filesizecheck_minaeoutputsize filesizeCheck_minAEOutputSize�.  �-   k  ]l$$ %&% I ]j�*'�)
�* .ascrcmnt****      � ****' b  ]f()( l ]b*�(�'* I ]b�&�%�$
�& .misccurdldt    ��� null�%  �$  �(  �'  ) m  be++ �,, B f i l e   s i z e s   m a t c h   -   e x i t i n g   r e p e a t�)  & -�#-  S  kl�#  �4  �3   ./. I q��"0�!
�" .ascrcmnt****      � ****0 b  q�121 b  qz343 l qv5� �5 I qv���
� .misccurdldt    ��� null�  �  �   �  4 m  vy66 �77 4 f i l e   n o t   d o n e   -   d i f f e r e n c e2 l z8��8 c  z9:9 l z};��; \  z}<=< o  z{�� 0 	filesize2  = o  {|�� 0 	filesize1  �  �  : m  }~�
� 
TEXT�  �  �!  / >?> l ������  �  �  ? @�@ Z  ��AB��A G  ��CDC A  ��EFE o  ���� 0 	filesize1  F o  ���� >0 filesizecheck_minaeoutputsize filesizeCheck_minAEOutputSizeD l ��G��G A  ��HIH o  ���
�
 0 	filesize1  I o  ���	�	 0 	filesize2  �  �  B k  ��JJ KLK I ���M�
� .ascrcmnt****      � ****M m  ��NN �OO \ f i l e s i z e 1   <   c h e c k s i z e   o r   f i l e s i z e 1   <   f i l e s i z e 2�  L PQP l ���RS�  R %  still rendering, skip this one   S �TT >   s t i l l   r e n d e r i n g ,   s k i p   t h i s   o n eQ UVU r  ��WXW m  ���
� boovtrueX o      �� 0 skipthisfile skipThisFileV Y�Y  S  ���  �  �  �  � Z[Z l ����� �  �  �   [ \]\ l ����^_��  ^ X ROLD this was used on Netflix where 2 outputs where needed, one keyed and one clean   _ �`` � O L D   t h i s   w a s   u s e d   o n   N e t f l i x   w h e r e   2   o u t p u t s   w h e r e   n e e d e d ,   o n e   k e y e d   a n d   o n e   c l e a n] aba l  ����cd��  c60
		repeat			log (current date) & "checking clean file size"			set myfile to myfolder & outputFilenameClean as string			set filesize1Clean to (get eof of POSIX file myfile)			log (current date) & "Clean file size = " & filesize1Clean			if filesize1Clean > filesizeCheck_minAEOutputSize then				log "filesize1Clean > checksize"				delay 1			end if			set filesize2Clean to (get eof of POSIX file myfile)			log (current date) & "Clean file size = " & filesize2Clean						--, filesizeCheck_minAEOutputSize			if (filesize1Clean = filesize2Clean) and (filesize2 > filesizeCheck_minAEOutputSize) then				log (current date) & "file sizes match - exiting repeat"				exit repeat			end if			log (current date) & "Clean file not done - difference" & ((filesize2Clean - filesize1Clean) as string)						if filesize1Clean < filesizeCheck_minAEOutputSize or (filesize1Clean < filesize2Clean) then				log "filesize1Clean < checksize or filesize1Clean < filesize2Clean"				-- still rendering, skip this one				set skipThisFileClean to true				exit repeat			end if		end repeat						if skipThisFile is false and skipThisFileClean is false then			--move AE output file to AME watch folder			-- move XML waiver to archive						set shscript1 to "mv " & quoted form of myfolder & outputFilename & " /" & quoted form of ame_watch & outputFilename			set shscript2 to "mv " & quoted form of myfolder & outputFilenameClean & " /" & quoted form of ame_watch & outputFilenameClean			set shscript3 to "mv " & quoted form of completed_waivers & i & " " & quoted form of waivers_pending_completed_archive & i			if dev_mode is true then				display dialog shscript			end if			try				log (current date) & "moving i to AME watch folder "				do shell script shscript1				do shell script shscript2				do shell script shscript3				set cycleCount to cycleCount + 1							on error errmsg				log (current date) & "error in uploading: " & errmsg				beep				beep				beep				display alert "An error occured in uploading: " & errmsg giving up after 30				exit repeat			end try					end if
		   d �ee` 
 	 	 r e p e a t  	 	 	 l o g   ( c u r r e n t   d a t e )   &   " c h e c k i n g   c l e a n   f i l e   s i z e "  	 	 	 s e t   m y f i l e   t o   m y f o l d e r   &   o u t p u t F i l e n a m e C l e a n   a s   s t r i n g  	 	 	 s e t   f i l e s i z e 1 C l e a n   t o   ( g e t   e o f   o f   P O S I X   f i l e   m y f i l e )  	 	 	 l o g   ( c u r r e n t   d a t e )   &   " C l e a n   f i l e   s i z e   =   "   &   f i l e s i z e 1 C l e a n  	 	 	 i f   f i l e s i z e 1 C l e a n   >   f i l e s i z e C h e c k _ m i n A E O u t p u t S i z e   t h e n  	 	 	 	 l o g   " f i l e s i z e 1 C l e a n   >   c h e c k s i z e "  	 	 	 	 d e l a y   1  	 	 	 e n d   i f  	 	 	 s e t   f i l e s i z e 2 C l e a n   t o   ( g e t   e o f   o f   P O S I X   f i l e   m y f i l e )  	 	 	 l o g   ( c u r r e n t   d a t e )   &   " C l e a n   f i l e   s i z e   =   "   &   f i l e s i z e 2 C l e a n  	 	 	  	 	 	 - - ,   f i l e s i z e C h e c k _ m i n A E O u t p u t S i z e  	 	 	 i f   ( f i l e s i z e 1 C l e a n   =   f i l e s i z e 2 C l e a n )   a n d   ( f i l e s i z e 2   >   f i l e s i z e C h e c k _ m i n A E O u t p u t S i z e )   t h e n  	 	 	 	 l o g   ( c u r r e n t   d a t e )   &   " f i l e   s i z e s   m a t c h   -   e x i t i n g   r e p e a t "  	 	 	 	 e x i t   r e p e a t  	 	 	 e n d   i f  	 	 	 l o g   ( c u r r e n t   d a t e )   &   " C l e a n   f i l e   n o t   d o n e   -   d i f f e r e n c e "   &   ( ( f i l e s i z e 2 C l e a n   -   f i l e s i z e 1 C l e a n )   a s   s t r i n g )  	 	 	  	 	 	 i f   f i l e s i z e 1 C l e a n   <   f i l e s i z e C h e c k _ m i n A E O u t p u t S i z e   o r   ( f i l e s i z e 1 C l e a n   <   f i l e s i z e 2 C l e a n )   t h e n  	 	 	 	 l o g   " f i l e s i z e 1 C l e a n   <   c h e c k s i z e   o r   f i l e s i z e 1 C l e a n   <   f i l e s i z e 2 C l e a n "  	 	 	 	 - -   s t i l l   r e n d e r i n g ,   s k i p   t h i s   o n e  	 	 	 	 s e t   s k i p T h i s F i l e C l e a n   t o   t r u e  	 	 	 	 e x i t   r e p e a t  	 	 	 e n d   i f  	 	 e n d   r e p e a t  	 	  	 	  	 	 i f   s k i p T h i s F i l e   i s   f a l s e   a n d   s k i p T h i s F i l e C l e a n   i s   f a l s e   t h e n  	 	 	 - - m o v e   A E   o u t p u t   f i l e   t o   A M E   w a t c h   f o l d e r  	 	 	 - -   m o v e   X M L   w a i v e r   t o   a r c h i v e  	 	 	  	 	 	 s e t   s h s c r i p t 1   t o   " m v   "   &   q u o t e d   f o r m   o f   m y f o l d e r   &   o u t p u t F i l e n a m e   &   "   / "   &   q u o t e d   f o r m   o f   a m e _ w a t c h   &   o u t p u t F i l e n a m e  	 	 	 s e t   s h s c r i p t 2   t o   " m v   "   &   q u o t e d   f o r m   o f   m y f o l d e r   &   o u t p u t F i l e n a m e C l e a n   &   "   / "   &   q u o t e d   f o r m   o f   a m e _ w a t c h   &   o u t p u t F i l e n a m e C l e a n  	 	 	 s e t   s h s c r i p t 3   t o   " m v   "   &   q u o t e d   f o r m   o f   c o m p l e t e d _ w a i v e r s   &   i   &   "   "   &   q u o t e d   f o r m   o f   w a i v e r s _ p e n d i n g _ c o m p l e t e d _ a r c h i v e   &   i  	 	 	 i f   d e v _ m o d e   i s   t r u e   t h e n  	 	 	 	 d i s p l a y   d i a l o g   s h s c r i p t  	 	 	 e n d   i f  	 	 	 t r y  	 	 	 	 l o g   ( c u r r e n t   d a t e )   &   " m o v i n g   i   t o   A M E   w a t c h   f o l d e r   "  	 	 	 	 d o   s h e l l   s c r i p t   s h s c r i p t 1  	 	 	 	 d o   s h e l l   s c r i p t   s h s c r i p t 2  	 	 	 	 d o   s h e l l   s c r i p t   s h s c r i p t 3  	 	 	 	 s e t   c y c l e C o u n t   t o   c y c l e C o u n t   +   1  	 	 	 	  	 	 	 o n   e r r o r   e r r m s g  	 	 	 	 l o g   ( c u r r e n t   d a t e )   &   " e r r o r   i n   u p l o a d i n g :   "   &   e r r m s g  	 	 	 	 b e e p  	 	 	 	 b e e p  	 	 	 	 b e e p  	 	 	 	 d i s p l a y   a l e r t   " A n   e r r o r   o c c u r e d   i n   u p l o a d i n g :   "   &   e r r m s g   g i v i n g   u p   a f t e r   3 0  	 	 	 	 e x i t   r e p e a t  	 	 	 e n d   t r y  	 	 	  	 	 e n d   i f 
 	 	b fgf l ����������  ��  ��  g hih Z  �gjk����j = ��lml o  ������ 0 skipthisfile skipThisFilem m  ����
�� boovfalsk k  �cnn opo l ����qr��  q - 'move AE output file to AME watch folder   r �ss N m o v e   A E   o u t p u t   f i l e   t o   A M E   w a t c h   f o l d e rp tut l ����vw��  v !  move XML waiver to archive   w �xx 6   m o v e   X M L   w a i v e r   t o   a r c h i v eu yzy l ����������  ��  ��  z {|{ r  ��}~} b  ��� b  ����� b  ����� b  ����� b  ����� m  ���� ���  m v  � n  ����� 1  ����
�� 
strq� o  ������ 0 myfolder  � o  ������  0 outputfilename outputFilename� m  ���� ���    /� n  ����� 1  ����
�� 
strq� o  ������ 0 	ame_watch  � o  ������  0 outputfilename outputFilename~ o      ���� 0 	shscript1  | ��� r  ����� b  ����� b  ����� b  ����� b  ����� b  ����� m  ���� ���  m v  � n  ����� 1  ����
�� 
strq� o  ������ 0 completed_waivers  � o  ������ 0 i  � m  ���� ���   � n  ����� 1  ����
�� 
strq� o  ������ %0 !waivers_pending_completed_archive  � o  ������ 0 i  � o      ���� 0 	shscript3  � ��� Z  ��������� = ����� o  ������ 0 dev_mode  � m  ����
�� boovtrue� I �������
�� .sysodlogaskr        TEXT� o  ������ 0 shscript  ��  ��  ��  � ��� Q  �a���� k  &�� ��� I �����
�� .ascrcmnt****      � ****� b  
��� l ������ I ������
�� .misccurdldt    ��� null��  ��  ��  ��  � m  	�� ��� : m o v i n g   i   t o   A M E   w a t c h   f o l d e r  ��  � ��� I �����
�� .sysoexecTEXT���     TEXT� o  ���� 0 	shscript1  ��  � ��� I �����
�� .sysoexecTEXT���     TEXT� o  ���� 0 	shscript3  ��  � ��� r  $��� [  "��� o   ���� 0 
cyclecount 
cycleCount� m   !���� � o      ���� 0 
cyclecount 
cycleCount� ���� l %%��������  ��  ��  ��  � R      �����
�� .ascrerr ****      � ****� o      ���� 
0 errmsg  ��  � k  .a�� ��� I .=�����
�� .ascrcmnt****      � ****� b  .9��� b  .7��� l .3������ I .3������
�� .misccurdldt    ��� null��  ��  ��  ��  � m  36�� ��� ( e r r o r   i n   u p l o a d i n g :  � o  78���� 
0 errmsg  ��  � ��� I >C������
�� .sysobeepnull��� ��� long��  ��  � ��� I DI������
�� .sysobeepnull��� ��� long��  ��  � ��� I JO������
�� .sysobeepnull��� ��� long��  ��  � ��� I P_����
�� .sysodisAaleR        TEXT� b  PU��� m  PS�� ��� > A n   e r r o r   o c c u r e d   i n   u p l o a d i n g :  � o  ST���� 
0 errmsg  � �����
�� 
givu� m  X[���� ��  � ����  S  `a��  � ���� l bb��������  ��  ��  ��  ��  ��  i ��� l hh��������  ��  ��  � ���� l hh��������  ��  ��  ��  �� 0 i  o o   \ ]���� 0 filelist  m ��� I o|�����
�� .ascrcmnt****      � ****� b  ox��� l ot������ I ot������
�� .misccurdldt    ��� null��  ��  ��  ��  � m  tw�� ��� * d o n e   w i t h   t r a n s c o d i n g��  � ���� l }}��������  ��  ��  ��   ��� l     ��������  ��  ��  � ��� l     ��������  ��  ��  � ��� l     ��������  ��  ��  � ��� l     ��������  ��  ��  � ��� i   � ���� I      �������� 0 handlewaivers handleWaivers��  ��  � k    W�� � � I    ����
�� .ascrcmnt****      � **** b      l    ���� I    ������
�� .misccurdldt    ��� null��  ��  ��  ��   m     �   c h e c k i n g   w a i v e r s��     l   �������  ��  �   	
	 r     o    �~�~ .0 incomingrawxmlwaivers incomingRawXmlWaivers o      �}�} 60 incomingwaiverfolderposix incomingWaiverFolderPosix
  l   �|�{�z�|  �{  �z    l   �y�y    assemble greps	    �  a s s e m b l e   g r e p s 	  r     b     b     b     m     �  l s   n     !  1    �x
�x 
strq! o    �w�w 60 incomingwaiverfolderposix incomingWaiverFolderPosix m    "" �##    |   g r e p   o    �v�v $0 waivergrepstring waiverGrepString o      �u�u 0 shscript   $%$ l     �t�s�r�t  �s  �r  % &'& Z     3()�q�p( =    '*+* o     %�o�o 0 dev_mode  + m   % &�n
�n boovtrue) I  * /�m,�l
�m .sysodlogaskr        TEXT, o   * +�k�k 0 shscript  �l  �q  �p  ' -.- l  4 4�j�i�h�j  �i  �h  . /0/ l  4 4�g12�g  1  get list of waivers   2 �33 & g e t   l i s t   o f   w a i v e r s0 454 l  4 4�f67�f  6 2 ,grep -e Added -e Changed -e Fixed -e Deleted   7 �88 X g r e p   - e   A d d e d   - e   C h a n g e d   - e   F i x e d   - e   D e l e t e d5 9:9 Q   4 b;<=; r   7 B>?> c   7 @@A@ n   7 >BCB 2  < >�e
�e 
cparC l  7 <D�d�cD I  7 <�bE�a
�b .sysoexecTEXT���     TEXTE o   7 8�`�` 0 shscript  �a  �d  �c  A m   > ?�_
�_ 
list? o      �^�^  0 waiverfilelist waiverFileList< R      �]F�\
�] .ascrerr ****      � ****F o      �[�[ 
0 errmsg  �\  = k   J bGG HIH I  J U�ZJ�Y
�Z .ascrcmnt****      � ****J b   J QKLK l  J OM�X�WM I  J O�V�U�T
�V .misccurdldt    ��� null�U  �T  �X  �W  L o   O P�S�S 
0 errmsg  �Y  I NON I  V [�RP�Q
�R .ascrcmnt****      � ****P m   V WQQ �RR > p r o b a b l y   b e c a u s e   n o   w a i v e r s   y e t�Q  O S�PS r   \ bTUT c   \ `VWV J   \ ^�O�O  W m   ^ _�N
�N 
listU o      �M�M  0 waiverfilelist waiverFileList�P  : XYX l  c c�L�K�J�L  �K  �J  Y Z[Z l  c c�I\]�I  \ N H look for incoming source files and move them and waiver into production   ] �^^ �   l o o k   f o r   i n c o m i n g   s o u r c e   f i l e s   a n d   m o v e   t h e m   a n d   w a i v e r   i n t o   p r o d u c t i o n[ _`_ l  c fabca r   c fded m   c d�H�H  e o      �G�G 0 
cyclecount 
cycleCountb . (limit how many files we do on each cycle   c �ff P l i m i t   h o w   m a n y   f i l e s   w e   d o   o n   e a c h   c y c l e` ghg l  g g�F�E�D�F  �E  �D  h iji X   gUk�Clk k   wPmm non Z   w �pq�B�Ap ?   w ~rsr o   w x�@�@ 0 
cyclecount 
cycleCounts o   x }�?�? $0 maxfilespercycle maxFilesPerCycleq  S   � ��B  �A  o tut l  � ��>�=�<�>  �=  �<  u vwv l  � ��;xy�;  x G ANeed to read the waiver and see if it contains multiple raw files   y �zz � N e e d   t o   r e a d   t h e   w a i v e r   a n d   s e e   i f   i t   c o n t a i n s   m u l t i p l e   r a w   f i l e sw {|{ l  � ��:}~�:  } - ' we also have access here to PATE info    ~ � N   w e   a l s o   h a v e   a c c e s s   h e r e   t o   P A T E   i n f o  | ��� r   � ���� b   � ���� o   � ��9�9 60 incomingwaiverfolderposix incomingWaiverFolderPosix� o   � ��8�8 0 outteri outterI� o      �7�7 0 
waiverfile 
waiverFile� ��� r   � ���� I   � ��6��5�6 0 read_waiver  � ��4� o   � ��3�3 0 
waiverfile 
waiverFile�4  �5  � o      �2�2 0 mywaiver myWaiver� ��� l  � ��1�0�/�1  �0  �/  � ��� r   � ���� J   � ��.�.  � o      �-�- 0 rawfileslist rawFilesList� ��� r   � ���� J   � ��,�,  � o      �+�+ 0 movefileslist moveFilesList� ��� l  � ��*�)�(�*  �)  �(  � ��� Z   � ����'�� F   � ���� l  � ���&�%� >  � ���� n   � ���� o   � ��$�$ 0 waiver_total_raw_files  � o   � ��#�# 0 mywaiver myWaiver� m   � ��"
�" 
null�&  �%  � l  � ���!� � ?   � ���� n   � ���� o   � ��� 0 waiver_total_raw_files  � o   � ��� 0 mywaiver myWaiver� m   � ���  �!  �   � k   � ��� ��� r   � ���� m   � ���  � o      �� 0 myincrementer myIncrementer� ��� U   � ���� k   � ��� ��� r   � ���� [   � ���� o   � ��� 0 myincrementer myIncrementer� m   � ��� � o      �� 0 myincrementer myIncrementer� ��� l  � �����  �    does this need try/catch?   � ��� 4   d o e s   t h i s   n e e d   t r y / c a t c h ?� ��� r   � ���� I   � ����� 0 get_element  � ��� o   � ��� 0 
waiverfile 
waiverFile� ��� m   � ��� ���  J o b� ��� l  � ����� b   � ���� m   � ��� ���  r a w _ v i d e o _� o   � ��� 0 myincrementer myIncrementer�  �  �  �  � o      �� 0 	myrawfile 	myRawFile� ��� s   � ���� o   � ��� 0 	myrawfile 	myRawFile� l     ���
� n      ���  ;   � �� o   � ��	�	 0 rawfileslist rawFilesList�  �
  �  � l  � ����� n   � ���� o   � ��� 0 waiver_total_raw_files  � o   � ��� 0 mywaiver myWaiver�  �  �  �'  � k   � ��� ��� l  � �����  � O I no raw files defined in waiver, infer raw filename from waiver file name   � ��� �   n o   r a w   f i l e s   d e f i n e d   i n   w a i v e r ,   i n f e r   r a w   f i l e n a m e   f r o m   w a i v e r   f i l e   n a m e� ��� l  � �����  � O I just look for single raw file, we will strip .xml extension in next step   � ��� �   j u s t   l o o k   f o r   s i n g l e   r a w   f i l e ,   w e   w i l l   s t r i p   . x m l   e x t e n s i o n   i n   n e x t   s t e p� ��� s   � ���� o   � ��� 0 outteri outterI� l     �� ��� n      ���  ;   � �� o   � ����� 0 rawfileslist rawFilesList�   ��  �  � ��� l  � ���������  ��  ��  � ��� l  � ���������  ��  ��  � ��� l  � �������  � 2 , see if all media files are ready to process   � ��� X   s e e   i f   a l l   m e d i a   f i l e s   a r e   r e a d y   t o   p r o c e s s� ��� Z   �N������ I   � �������� (0 checkrawfilestatus checkRawFileStatus� ���� o   � ����� 0 rawfileslist rawFilesList��  ��  � k   �J�� ��� l  � �������  � 3 - files are ready, move waiver into production   � ��� Z   f i l e s   a r e   r e a d y ,   m o v e   w a i v e r   i n t o   p r o d u c t i o n� ��� r   �	 		  b   �			 b   �
			 b   �			 b   �				 m   � �	
	
 �		  m v  		 n   �			 1   ���
�� 
strq	 o   � ����� 0 
waiverfile 
waiverFile	 m  		 �		   	 o  	���� @0 pendingwaiversforaefolderposix pendingWaiversForAEFolderPosix	 l 
	����	 c  
			 o  
���� 0 outteri outterI	 m  ��
�� 
TEXT��  ��  	 o      ���� 0 shscript  � 			 l ��		��  	  display dialog shscript   	 �		 . d i s p l a y   d i a l o g   s h s c r i p t	 			 l ��������  ��  ��  	 			 r  			 m  ��
�� boovfals	 o      ���� 0 success  	 	��	 Q  J		 	!	 k  )	"	" 	#	$	# I ��	%��
�� .sysoexecTEXT���     TEXT	% o  ���� 0 shscript  ��  	$ 	&	'	& r   %	(	)	( [   #	*	+	* o   !���� 0 
cyclecount 
cycleCount	+ m  !"���� 	) o      ���� 0 
cyclecount 
cycleCount	' 	,	-	, l &&��������  ��  ��  	- 	.��	. r  &)	/	0	/ m  &'��
�� boovtrue	0 o      ���� 0 success  ��  	  R      ��	1��
�� .ascrerr ****      � ****	1 o      ���� 
0 errmsg  ��  	! k  1J	2	2 	3	4	3 I 1@��	5��
�� .ascrcmnt****      � ****	5 b  1<	6	7	6 b  1:	8	9	8 l 16	:����	: I 16������
�� .misccurdldt    ��� null��  ��  ��  ��  	9 m  69	;	; �	<	< ` e r r o r   i n   m o v i n g   w a i v e r   f i l e   t o   w a i v e r s _ p e n d i n g :  	7 o  :;���� 
0 errmsg  ��  	4 	=��	= I AJ��	>��
�� .sysodisAaleR        TEXT	> b  AF	?	@	? m  AD	A	A �	B	B ` e r r o r   i n   m o v i n g   w a i v e r   f i l e   t o   w a i v e r s _ p e n d i n g :  	@ o  DE���� 
0 errmsg  ��  ��  ��  ��  � l MM��	C	D��  	C 8 2 waiver not ready, we'll check again on next cycle   	D �	E	E d   w a i v e r   n o t   r e a d y ,   w e ' l l   c h e c k   a g a i n   o n   n e x t   c y c l e� 	F	G	F l OO��������  ��  ��  	G 	H	I	H l OO��������  ��  ��  	I 	J��	J l OO��	K	L��  	K  outterI   	L �	M	M  o u t t e r I��  �C 0 outteri outterIl o   j k����  0 waiverfilelist waiverFileListj 	N��	N l VV��������  ��  ��  ��  � 	O	P	O l     ��������  ��  ��  	P 	Q	R	Q l     ��������  ��  ��  	R 	S	T	S i   � �	U	V	U I      ��	W���� (0 checkrawfilestatus checkRawFileStatus	W 	X��	X o      ���� 0 rawfileslist rawFilesList��  ��  	V k    )	Y	Y 	Z	[	Z l     ��	\	]��  	\ * $ check if files are done transcoding   	] �	^	^ H   c h e c k   i f   f i l e s   a r e   d o n e   t r a n s c o d i n g	[ 	_	`	_ l     ��	a	b��  	a E ? if they are not found, check for them in incoming media folder   	b �	c	c ~   i f   t h e y   a r e   n o t   f o u n d ,   c h e c k   f o r   t h e m   i n   i n c o m i n g   m e d i a   f o l d e r	` 	d	e	d l     ��	f	g��  	f 5 / returns true if all files are done transcoding   	g �	h	h ^   r e t u r n s   t r u e   i f   a l l   f i l e s   a r e   d o n e   t r a n s c o d i n g	e 	i	j	i l     ��������  ��  ��  	j 	k	l	k l     ��	m	n��  	m b \ if first file is not found in transcoded, just move on to checking raw path for all of them   	n �	o	o �   i f   f i r s t   f i l e   i s   n o t   f o u n d   i n   t r a n s c o d e d ,   j u s t   m o v e   o n   t o   c h e c k i n g   r a w   p a t h   f o r   a l l   o f   t h e m	l 	p	q	p Z     	r	s����	r =    	t	u	t o     ���� 0 dev_mode  	u m    ��
�� boovtrue	s I  
 ��	v��
�� .sysodlogaskr        TEXT	v b   
 	w	x	w m   
 	y	y �	z	z  r a w F i l e s L i s t   =  	x o    ���� 0 rawfileslist rawFilesList��  ��  ��  	q 	{	|	{ r    	}	~	} m    ��
�� boovfals	~ o      ���� "0 filereadystatus fileReadyStatus	| 		�	 r    	�	�	� m    ��
�� boovfals	� o      ����  0 skiptranscoded skipTranscoded	� 	�	�	� r    "	�	�	� J     ����  	� o      ���� 0 movefileslist moveFilesList	� 	�	�	� l  # #��������  ��  ��  	� 	�	�	� l  # #��������  ��  ��  	� 	�	�	� X   #=	���	�	� k   38	�	� 	�	�	� r   3 >	�	�	� I  3 <��	���
�� .corecnte****       ****	� n   3 8	�	�	� 2  6 8��
�� 
cha 	� l  3 6	�����	� c   3 6	�	�	� o   3 4���� 0 i  	� m   4 5��
�� 
TEXT��  ��  ��  	� o      ���� 0 
namelength  	� 	�	�	� r   ? P	�	�	� c   ? N	�	�	� l  ? L	�����	� n   ? L	�	�	� 7 @ L��	�	�
�� 
cha 	� m   D F���� 	� l  G K	�����	� \   G K	�	�	� o   H I���� 0 
namelength  	� m   I J���� ��  ��  	� o   ? @���� 0 i  ��  ��  	� m   L M��
�� 
TEXT	� o      ���� 
0 myname  	� 	�	�	� l  Q Q����~��  �  �~  	� 	�	�	� l  Q Q�}�|�{�}  �|  �{  	� 	�	�	� l  Q Q�z	�	��z  	� 8 2check for transcoded media for any valid extension   	� �	�	� d c h e c k   f o r   t r a n s c o d e d   m e d i a   f o r   a n y   v a l i d   e x t e n s i o n	� 	�	�	� X   Q6	��y	�	� k   e1	�	� 	�	�	� r   e p	�	�	� c   e n	�	�	� b   e l	�	�	� b   e j	�	�	� b   e h	�	�	� m   e f	�	� �	�	�  /	� o   f g�x�x >0 transcodecompletedfolderposix transcodeCompletedFolderPosix	� o   h i�w�w 
0 myname  	� o   j k�v�v 0 fileextension fileExtension	� m   l m�u
�u 
TEXT	� o      �t�t $0 mytranscodedfile myTranscodedFile	� 	�	�	� r   q |	�	�	� c   q z	�	�	� b   q x	�	�	� b   q v	�	�	� b   q t	�	�	� m   q r	�	� �	�	�  /	� o   r s�s�s :0 incomingrawmediafolderposix incomingRawMediaFolderPosix	� o   t u�r�r 
0 myname  	� o   v w�q�q 0 fileextension fileExtension	� m   x y�p
�p 
TEXT	� o      �o�o 0 	myrawfile 	myRawFile	� 	�	�	� r   } �	�	�	� o   } ~�n�n 0 fileextension fileExtension	� o      �m�m  0 foundextension foundExtension	� 	�	�	� l  � ��l�k�j�l  �k  �j  	� 	�	�	� Z   � �	�	��i�h	� I   � ��g	��f�g ,0 checkfilewritestatus checkFileWriteStatus	� 	�	�	� o   � ��e�e $0 mytranscodedfile myTranscodedFile	� 	�	�	� m   � ��d�d�	� 	��c	� m   � ��b�b �c  �f  	� k   � �	�	� 	�	�	� r   � �	�	�	� m   � ��a
�a boovtrue	� o      �`�` "0 filereadystatus fileReadyStatus	� 	�	�	� s   � �	�	�	� c   � �	�	�	� l  � �	��_�^	� b   � �	�	�	� o   � ��]�] 
0 myname  	� o   � ��\�\  0 foundextension foundExtension�_  �^  	� m   � ��[
�[ 
TEXT	� l     	��Z�Y	� n      	�	�	�  ;   � �	� o   � ��X�X 0 movefileslist moveFilesList�Z  �Y  	� 	��W	�  S   � ��W  �i  �h  	� 	�	�	� l  � ��V�U�T�V  �U  �T  	� 	�	�	� l  � ��S	�	��S  	� 4 . file not found in transcoded, check it in raw   	� �	�	� \   f i l e   n o t   f o u n d   i n   t r a n s c o d e d ,   c h e c k   i t   i n   r a w	� 	�	�	� Z   �/	�	��R�Q	� I   � ��P	��O�P ,0 checkfilewritestatus checkFileWriteStatus	� 
 

  o   � ��N�N 0 	myrawfile 	myRawFile
 


 m   � ��M�M  
 
�L
 m   � ��K�K �L  �O  	� k   �+

 


 r   � �

	
 m   � ��J
�J boovfals
	 o      �I�I "0 filereadystatus fileReadyStatus
 




 l  � ��H

�H  
 ( "move raw file to Transcoding chain   
 �

 D m o v e   r a w   f i l e   t o   T r a n s c o d i n g   c h a i n
 


 Z   � �


�G
 E   � �


 o   � ��F�F 80 rawtranscodefileextensions rawTranscodeFileExtensions
 o   � ��E�E  0 foundextension foundExtension
 r   � �


 b   � �


 b   � �


 b   � �


 b   � �


 m   � �
 
  �
!
!  m v  
 n   � �
"
#
" 1   � ��D
�D 
strq
# o   � ��C�C 0 	myrawfile 	myRawFile
 m   � �
$
$ �
%
%   
 o   � ��B�B :0 transcodependingfolderposix transcodePendingFolderPosix
 l  � �
&�A�@
& b   � �
'
(
' o   � ��?�? 
0 myname  
( o   � ��>�>  0 foundextension foundExtension�A  �@  
 o      �=�= 0 shscript  
 
)
*
) E   � �
+
,
+ o   � ��<�< D0  bypassrawtranscodefileextensions  bypassRawTranscodeFileExtensions
, o   � ��;�;  0 foundextension foundExtension
* 
-�:
- r   � �
.
/
. b   � �
0
1
0 b   � �
2
3
2 b   � �
4
5
4 b   � �
6
7
6 m   � �
8
8 �
9
9  m v  
7 n   � �
:
;
: 1   � ��9
�9 
strq
; o   � ��8�8 0 	myrawfile 	myRawFile
5 m   � �
<
< �
=
=   
3 o   � ��7�7 >0 transcodecompletedfolderposix transcodeCompletedFolderPosix
1 l  � �
>�6�5
> b   � �
?
@
? o   � ��4�4 
0 myname  
@ o   � ��3�3  0 foundextension foundExtension�6  �5  
/ o      �2�2 0 shscript  �:  �G  
 
A
B
A r   � �
C
D
C m   � ��1
�1 boovfals
D o      �0�0 0 success  
B 
E
F
E Q   �)
G
H
I
G k   �
J
J 
K
L
K I  � ��/
M�.
�/ .sysoexecTEXT���     TEXT
M o   � ��-�- 0 shscript  �.  
L 
N
O
N r   � 
P
Q
P m   � ��,
�, boovtrue
Q o      �+�+ $0 filemove_success fileMove_success
O 
R
S
R  S  
S 
T�*
T l �)�(�'�)  �(  �'  �*  
H R      �&
U�%
�& .ascrerr ****      � ****
U o      �$�$ 
0 errmsg  �%  
I k  )
V
V 
W
X
W I �#
Y�"
�# .ascrcmnt****      � ****
Y b  
Z
[
Z b  
\
]
\ l 
^�!� 
^ I ���
� .misccurdldt    ��� null�  �  �!  �   
] m  
_
_ �
`
` ` e r r o r   i n   m o v i n g   m e d i a   f i l e   t o   m e d i a _ t r a n s c o d e d :  
[ o  �� 
0 errmsg  �"  
X 
a
b
a I %�
c�
� .sysodisAaleR        TEXT
c b  !
d
e
d m  
f
f �
g
g ` e r r o r   i n   m o v i n g   m e d i a   f i l e   t o   m e d i a _ t r a n s c o d e d :  
e o   �� 
0 errmsg  �  
b 
h�
h r  &)
i
j
i m  &'�
� boovfals
j o      �� $0 filemove_success fileMove_success�  
F 
k
l
k l **����  �  �  
l 
m
n
m l **����  �  �  
n 
o�
o l **�
p
q�  
p  exists rawFile end if   
q �
r
r * e x i s t s   r a w F i l e   e n d   i f�  �R  �Q  	� 
s�
s l 00�
t
u�  
t  file extensions repeat   
u �
v
v , f i l e   e x t e n s i o n s   r e p e a t�  �y 0 fileextension fileExtension	� o   T Y�� 00 incomingfileextensions incomingFileExtensions	� 
w
x
w l 77�
�	��
  �	  �  
x 
y
z
y l 77����  �  �  
z 
{�
{ l 77�
|
}�  
|  outter repeat in filelist   
} �
~
~ 2 o u t t e r   r e p e a t   i n   f i l e l i s t�  �� 0 i  	� o   & '�� 0 rawfileslist rawFilesList	� 

�
 l >>�� ���  �   ��  
� 
�
�
� l >>��
�
���  
� U Oif all files are found in transcoded, check their status and move to media_pool   
� �
�
� � i f   a l l   f i l e s   a r e   f o u n d   i n   t r a n s c o d e d ,   c h e c k   t h e i r   s t a t u s   a n d   m o v e   t o   m e d i a _ p o o l
� 
�
�
� Z  >$
�
���
�
� F  >S
�
�
� = >A
�
�
� o  >?���� "0 filereadystatus fileReadyStatus
� m  ?@��
�� boovtrue
� l DO
�����
� =  DO
�
�
� n  DI
�
�
� 1  EI��
�� 
leng
� o  DE���� 0 movefileslist moveFilesList
� n  IN
�
�
� 1  JN��
�� 
leng
� o  IJ���� 0 rawfileslist rawFilesList��  ��  
� k  V
�
� 
�
�
� X  V
���
�
� k  f�
�
� 
�
�
� Z  f
�
�����
� = fm
�
�
� o  fk���� 0 dev_mode  
� m  kl��
�� boovtrue
� I p{��
���
�� .sysodlogaskr        TEXT
� c  pw
�
�
� b  pu
�
�
� m  ps
�
� �
�
�   m o v e F i l e s L i s t   =  
� o  st���� 0 movefileslist moveFilesList
� m  uv��
�� 
TEXT��  ��  ��  
� 
�
�
� r  ��
�
�
� c  ��
�
�
� b  ��
�
�
� b  ��
�
�
� m  ��
�
� �
�
�  /
� o  ������ >0 transcodecompletedfolderposix transcodeCompletedFolderPosix
� o  ������ 0 rawfilei rawFileI
� m  ����
�� 
TEXT
� o      ���� 
0 myfile  
� 
�
�
� l ����������  ��  ��  
� 
�
�
� r  ��
�
�
� n  ��
�
�
� 1  ����
�� 
psxp
� o  ������ 
0 myfile  
� o      ���� 0 pmyfile  
� 
�
�
� l ����
�
���  
� F @TODO refactor this to use incoming_media_ext from config file!!!   
� �
�
� � T O D O   r e f a c t o r   t h i s   t o   u s e   i n c o m i n g _ m e d i a _ e x t   f r o m   c o n f i g   f i l e ! ! !
� 
�
�
� r  ��
�
�
� b  ��
�
�
� b  ��
�
�
� b  ��
�
�
� b  ��
�
�
� m  ��
�
� �
�
�  m v  
� n  ��
�
�
� 1  ����
�� 
strq
� o  ������ 0 pmyfile  
� m  ��
�
� �
�
�   
� o  ������ 0 
media_pool  
� l ��
�����
� o  ������ 0 rawfilei rawFileI��  ��  
� o      ���� 0 shscript  
� 
�
�
� r  ��
�
�
� m  ����
�� boovfals
� o      ���� 0 success  
� 
�
�
� Z  ��
�
�����
� = ��
�
�
� o  ������ 0 dev_mode  
� m  ����
�� boovtrue
� I ����
���
�� .sysodlogaskr        TEXT
� o  ������ 0 shscript  ��  ��  ��  
� 
�
�
� Q  ��
�
�
�
� k  ��
�
� 
�
�
� I ����
���
�� .sysoexecTEXT���     TEXT
� o  ������ 0 shscript  ��  
� 
���
� r  ��
�
�
� m  ����
�� boovtrue
� o      ���� "0 filereadystatus fileReadyStatus��  
� R      ��
���
�� .ascrerr ****      � ****
� o      ���� 
0 errmsg  ��  
� k  ��
�
� 
�
�
� I ����
���
�� .ascrcmnt****      � ****
� b  ��
�
�
� b  ��
�
�
� l ��
�����
� I ��������
�� .misccurdldt    ��� null��  ��  ��  ��  
� m  ��
�
� �
�
� T e r r o r   i n   m o v i n g   m e d i a   f i l e   t o   m e d i a _ p o o l :  
� o  ������ 
0 errmsg  ��  
� 
�
�
� I ����
���
�� .sysodisAaleR        TEXT
� b  ��
�
�
� m  ��
�
� �
�
� T e r r o r   i n   m o v i n g   m e d i a   f i l e   t o   m e d i a _ p o o l :  
� o  ������ 
0 errmsg  ��  
� 
� 
� r  �� m  ����
�� boovfals o      ���� "0 filereadystatus fileReadyStatus  ��  S  ����  
� �� l ����������  ��  ��  ��  �� 0 rawfilei rawFileI
� o  YZ���� 0 movefileslist moveFilesList
�  L   o  ���� "0 filereadystatus fileReadyStatus �� l ��������  ��  ��  ��  ��  
� k  
$		 

 l 

����   3 - files not ready or move to media_pool failed    � Z   f i l e s   n o t   r e a d y   o r   m o v e   t o   m e d i a _ p o o l   f a i l e d  Z  
���� = 
 o  
���� 0 dev_mode   m  ��
�� boovtrue I ����
�� .sysodlogaskr        TEXT m   � . N o t   a l l   f i l e s   a r e   r e a d y��  ��  ��    L   " m   !��
�� boovfals �� l ##����    fileReadyStatus    �  f i l e R e a d y S t a t u s��  
�   l %%��������  ��  ��    !"! L  %'## o  %&���� "0 filereadystatus fileReadyStatus" $��$ l ((��������  ��  ��  ��  	T %&% l     ��������  ��  ��  & '(' l     ��������  ��  ��  ( )*) l     ��������  ��  ��  * +,+ i   � �-.- I      ��/���� ,0 checkfilewritestatus checkFileWriteStatus/ 010 o      ���� 0 pmyfile  1 232 o      ���� "0 expectedminsize expectedMinSize3 4��4 o      ���� &0 checkdelayseconds checkDelaySeconds��  ��  . k     k55 676 r     898 m     ��
�� boovfals9 o      ����  0 thisfilestatus thisFileStatus7 :;: l   ��������  ��  ��  ; <=< r    >?> m    ����  ? o      ���� 0 	filesize1  = @A@ r    BCB m    	���� C o      ���� 0 	filesize2  A DED Z   FG����F =   HIH o    ���� "0 expectedminsize expectedMinSizeI m    ��
�� 
nullG r    JKJ m    ���� K o      ���� "0 expectedminsize expectedMinSize��  ��  E LML l   ��~�}�  �~  �}  M NON Z    hPQ�|RP I     �{S�z�{ 0 
fileexists 
fileExistsS T�yT o    �x�x 0 pmyfile  �y  �z  Q k   # cUU VWV r   # -XYX l  # +Z�w�vZ I  # +�u[�t
�u .rdwrgeofcomp       ****[ 4   # '�s\
�s 
psxf\ o   % &�r�r 0 pmyfile  �t  �w  �v  Y o      �q�q 0 	filesize1  W ]^] Z  . B_`�p�o_ l  . 9a�n�ma G   . 9bcb l  . 1d�l�kd A   . 1efe o   . /�j�j 0 	filesize1  f m   / 0�i�i �l  �k  c l  4 7g�h�gg A   4 7hih o   4 5�f�f 0 	filesize1  i o   5 6�e�e "0 expectedminsize expectedMinSize�h  �g  �n  �m  ` L   < >jj m   < =�d
�d boovfals�p  �o  ^ klk l  C C�c�b�a�c  �b  �a  l mnm I  C H�`o�_
�` .sysodelanull��� ��� nmbro o   C D�^�^ &0 checkdelayseconds checkDelaySeconds�_  n pqp r   I Srsr l  I Qt�]�\t I  I Q�[u�Z
�[ .rdwrgeofcomp       ****u 4   I M�Yv
�Y 
psxfv o   K L�X�X 0 pmyfile  �Z  �]  �\  s o      �W�W 0 	filesize2  q wxw Z   T ayz�V{y =   T W|}| o   T U�U�U 0 	filesize1  } o   U V�T�T 0 	filesize2  z L   Z \~~ m   Z [�S
�S boovtrue�V  { L   _ a m   _ `�R
�R boovfalsx ��Q� l  b b�P�O�N�P  �O  �N  �Q  �|  R k   f h�� ��� l  f f�M���M  �   file does not exist   � ��� (   f i l e   d o e s   n o t   e x i s t� ��L� L   f h�� m   f g�K
�K boovfals�L  O ��� l  i i�J�I�H�J  �I  �H  � ��� l  i i�G�F�E�G  �F  �E  � ��D� L   i k�� o   i j�C�C  0 thisfilestatus thisFileStatus�D  , ��� l     �B�A�@�B  �A  �@  � ��� i   � ���� I      �?��>�? 0 gatherstats gatherStats� ��=� o      �<�< 0 intervalmins intervalMins�=  �>  � k    	��� ��� l     �;���;  � &   only send stats every X minutes   � ��� @   o n l y   s e n d   s t a t s   e v e r y   X   m i n u t e s� ��� r     ��� m     �:
�: boovfals� o      �9�9 0 
initialrun 
initialRun� ��� Z    G���8�� ?    ��� o    �7�7 0 intervalmins intervalMins� m    �6�6  � k   
 -�� ��� r   
 ��� I   
 �5�4�3�5 0 getepochtime getEpochTime�4  �3  � o      �2�2 0 timenow timeNow� ��� r     ��� I    �1��0�1 0 number_to_string  � ��/� [    ��� o    �.�. 80 laststatsgatheredtimestamp lastStatsGatheredTimestamp� l   ��-�,� ]    ��� ]    ��� o    �+�+ 0 intervalmins intervalMins� m    �*�* <� m    �)�)  ���-  �,  �/  �0  � o      �(�( 0 	nextcheck 	nextCheck� ��'� Z   ! -���&�%� ?   ! $��� o   ! "�$�$ 0 	nextcheck 	nextCheck� o   " #�#�# 0 timenow timeNow� L   ' )�"�"  �&  �%  �'  �8  � k   0 G�� ��� l  0 0�!���!  � A ;making initial run, need to init lastStatsGatheredTimestamp   � ��� v m a k i n g   i n i t i a l   r u n ,   n e e d   t o   i n i t   l a s t S t a t s G a t h e r e d T i m e s t a m p� ��� r   0 7��� I   0 5� ���  0 getepochtime getEpochTime�  �  � o      �� 80 laststatsgatheredtimestamp lastStatsGatheredTimestamp� ��� r   8 ?��� I   8 =���� 0 getepochtime getEpochTime�  �  � o      �� :0 laststalefilechecktimestamp lastStaleFileCheckTimestamp� ��� r   @ C��� m   @ A�
� boovfals� o      �� *0 forcestalefilecheck forceStaleFileCheck� ��� r   D G��� m   D E�
� boovtrue� o      �� 0 
initialrun 
initialRun�  � ��� l  H H����  �  �  � ��� l  H H����  � %  set var for our overall health   � ��� >   s e t   v a r   f o r   o u r   o v e r a l l   h e a l t h� ��� r   H K��� m   H I�
� boovtrue� o      �� 0 ishappy isHappy� ��� r   L O��� m   L M�
� boovfals� o      �� 40 updatestalefiletimestamp updateStaleFileTimestamp� ��� r   P S��� m   P Q��  � o      �
�
 0 oldestfileage oldestFileAge� ��� r   T W��� m   T U�� ���  � o      �	�	 0 	dataitems 	dataItems� ��� r   X [��� m   X Y�� ���  � o      �� 0 failedtests failedTests� ��� r   \ _��� m   \ ]�
� boovfals� o      �� *0 recycleaftereffects recycleAfterEffects� ��� l  ` `����  �  �  � ��� l  ` `��� �  �  �   � ��� r   ` c��� m   ` a�� �   @ { " n a m e " : " t e s t i n g " , " v a l u e " : " f o o " }� o      ���� 0 	firstitem 	firstItem�  l  d d��������  ��  ��    l  d d����     check if AE is running    � .   c h e c k   i f   A E   i s   r u n n i n g 	 r   d l

 I   d j������ 0 
is_running   �� m   e f �  A f t e r   E f f e c t s��  ��   o      ���� 0 ae_test  	  I   m y������ 0 
writeplist 
writePlist  o   n s���� 0 theplistpath thePListPath  m   s t �  a e _ t e s t �� o   t u���� 0 ae_test  ��  ��    Z   z ��� =  z }  o   z {���� 0 ae_test    m   { |��
�� boovfals k   � �!! "#" r   � �$%$ m   � ���
�� boovfals% o      ���� 0 ishappy isHappy# &'& r   � �()( b   � �*+* o   � ����� 0 failedtests failedTests+ m   � �,, �--  a e _ s t a t u s ,  ) o      ���� 0 failedtests failedTests' ./. Z   � �01����0 =  � �232 o   � ����� 0 dev_mode  3 m   � ���
�� boovtrue1 I  � ���4��
�� .sysodlogaskr        TEXT4 m   � �55 �66 & N O T   H A P P Y   a e _ s t a t u s��  ��  ��  / 787 r   � �9:9 m   � �;; �<< F { " n a m e " : " a e _ s t a t u s " , " v a l u e " : " D O W N " }: o      ���� 0 	firstitem 	firstItem8 =>= r   � �?@? m   � ���
�� boovtrue@ o      ���� *0 recycleaftereffects recycleAfterEffects> A��A l  � ���������  ��  ��  ��  ��   r   � �BCB m   � �DD �EE B { " n a m e " : " a e _ s t a t u s " , " v a l u e " : " U P " }C o      ���� 0 	firstitem 	firstItem FGF r   � �HIH b   � �JKJ o   � ����� 0 	dataitems 	dataItemsK o   � ����� 0 	firstitem 	firstItemI o      ���� 0 	dataitems 	dataItemsG LML l  � ���������  ��  ��  M NON l  � ���PQ��  P   check if AE is hung up   Q �RR .   c h e c k   i f   A E   i s   h u n g   u pO STS r   � �UVU c   � �WXW I   � ��������� 20 checkaftereffectsstatus checkAfterEffectsStatus��  ��  X m   � ���
�� 
listV o      ���� 00 aftereffectsresponsive afterEffectsResponsiveT YZY Z   � �[\����[ =  � �]^] n   � �_`_ 4   � ���a
�� 
cobja m   � ����� ` o   � ����� 00 aftereffectsresponsive afterEffectsResponsive^ m   � ���
�� boovfals\ k   � �bb cdc l  � ���ef��  e 1 + recycle it after we send the status report   f �gg V   r e c y c l e   i t   a f t e r   w e   s e n d   t h e   s t a t u s   r e p o r td hih r   � �jkj m   � ���
�� boovtruek o      ���� *0 recycleaftereffects recycleAfterEffectsi l��l r   � �mnm b   � �opo o   � ����� 0 failedtests failedTestsp m   � �qq �rr 4 r e s t a r t i n g _ a f t e r _ e f f e c t s ,  n o      ���� 0 failedtests failedTests��  ��  ��  Z sts l  � ���������  ��  ��  t uvu l  � ���������  ��  ��  v wxw l  � ���yz��  y ( " check if Media Encoder is running   z �{{ D   c h e c k   i f   M e d i a   E n c o d e r   i s   r u n n i n gx |}| r   � �~~ I   � �������� 0 
is_running  � ���� m   � ��� ��� & A d o b e   M e d i a   E n c o d e r��  ��   o      ���� 0 ame_test  } ��� I   � �������� 0 
writeplist 
writePlist� ��� o   � ����� 0 theplistpath thePListPath� ��� m   � ��� ���  a m e _ t e s t� ���� o   � ����� 0 ame_test  ��  ��  � ��� Z   �0������ =  � ���� o   � ����� 0 ame_test  � m   � ���
�� boovfals� k  (�� ��� r  ��� m  ��
�� boovfals� o      ���� 0 ishappy isHappy� ��� r  ��� b  
��� o  ���� 0 failedtests failedTests� m  	�� ���  a m e _ s t a t u s ,  � o      ���� 0 failedtests failedTests� ��� Z  "������� = ��� o  ���� 0 dev_mode  � m  ��
�� boovtrue� I �����
�� .sysodlogaskr        TEXT� m  �� ��� ( N O T   H A P P Y   a m e _ s t a t u s��  ��  ��  � ���� r  #(��� m  #&�� ��� H { " n a m e " : " a m e _ s t a t u s " , " v a l u e " : " D O W N " }� o      ���� 0 nextitem nextItem��  ��  � r  +0��� m  +.�� ��� D { " n a m e " : " a m e _ s t a t u s " , " v a l u e " : " U P " }� o      ���� 0 nextitem nextItem� ��� r  1:��� b  18��� b  16��� o  12���� 0 	dataitems 	dataItems� m  25�� ���  ,� o  67���� 0 nextitem nextItem� o      ���� 0 	dataitems 	dataItems� ��� l ;;��������  ��  ��  � ��� l ;;������  � "  check if Dropbox is running   � ��� 8   c h e c k   i f   D r o p b o x   i s   r u n n i n g� ��� r  ;E��� I  ;C������� 0 
is_running  � ���� m  <?�� ���  D r o p b o x��  ��  � o      ���� 0 dropbox_test  � ��� I  FT������� 0 
writeplist 
writePlist� ��� o  GL���� 0 theplistpath thePListPath� ��� m  LO�� ���  d r o p b o x _ t e s t� ���� o  OP���� 0 dropbox_test  ��  ��  � ��� Z  U������� = UX��� o  UV���� 0 dropbox_test  � m  VW��
�� boovfals� k  [��� ��� r  [^��� m  [\��
�� boovfals� o      ���� 0 ishappy isHappy� ��� r  _f��� b  _d��� o  _`���� 0 failedtests failedTests� m  `c�� ���   d r o p b o x _ s t a t u s ,  � o      ���� 0 failedtests failedTests� ��� Z  g|������� = gn��� o  gl���� 0 dev_mode  � m  lm��
�� boovtrue� I qx�����
�� .sysodlogaskr        TEXT� m  qt�� ��� 0 N O T   H A P P Y   d r o p b o x _ s t a t u s��  ��  ��  � ���� r  }���� m  }��� ��� P { " n a m e " : " d r o p b o x _ s t a t u s " , " v a l u e " : " D O W N " }� o      ���� 0 nextitem nextItem��  ��  � r  ����� m  ���� ��� L { " n a m e " : " d r o p b o x _ s t a t u s " , " v a l u e " : " U P " }� o      ���� 0 nextitem nextItem� ��� r  ��� � b  �� b  �� o  ������ 0 	dataitems 	dataItems m  �� �  , o  ������ 0 nextitem nextItem  o      ���� 0 	dataitems 	dataItems�  l ���������  ��  �   	
	 l ���~�}�|�~  �}  �|  
  l ���{�{   &  check pending waivers (AE queue)    � @ c h e c k   p e n d i n g   w a i v e r s   ( A E   q u e u e )  r  �� b  �� b  �� m  �� �  l s   n  �� 1  ���z
�z 
strq o  ���y�y @0 pendingwaiversforaefolderposix pendingWaiversForAEFolderPosix m  �� �    |   g r e p   ' . x m l ' o      �x�x 0 shscript    Q  �� !"  r  ��#$# c  ��%&% n  ��'(' 2 ���w
�w 
cpar( l ��)�v�u) I ���t*�s
�t .sysoexecTEXT���     TEXT* o  ���r�r 0 shscript  �s  �v  �u  & m  ���q
�q 
list$ o      �p�p 0 mylist myList! R      �o+�n
�o .ascrerr ****      � ****+ o      �m�m 
0 errmsg  �n  " k  ��,, -.- I ���l/�k
�l .ascrcmnt****      � ****/ b  ��010 b  ��232 l ��4�j�i4 I ���h�g�f
�h .misccurdldt    ��� null�g  �f  �j  �i  3 m  ��55 �66 2 p r o b a b l y   n o   w a i v e r s   y e t :  1 o  ���e�e 
0 errmsg  �k  . 7�d7 r  ��898 J  ���c�c  9 o      �b�b 0 mylist myList�d   :;: l ���a�`�_�a  �`  �_  ; <=< r  ��>?> n  ��@A@ 1  ���^
�^ 
lengA o  ���]�] 0 mylist myList? o      �\�\ 0 mycount myCount= BCB r  ��DED I  ���[F�Z�[ 0 	readplist 	readPlistF GHG o  ���Y�Y 0 theplistpath thePListPathH IJI m  ��KK �LL   a e _ w a i v e r _ c o u n t 1J M�XM m  ���W
�W 
null�X  �Z  E o      �V�V 0 oldcount oldCountC NON r  �PQP \  �RSR o  � �U�U 0 mycount myCountS o   �T�T 0 oldcount oldCountQ o      �S�S 0 ae_waiver_delta  O TUT I  	�RV�Q�R 0 
writeplist 
writePlistV WXW o  
�P�P 0 theplistpath thePListPathX YZY m  [[ �\\  a e _ w a i v e r _ d e l t aZ ]�O] o  �N�N 0 ae_waiver_delta  �O  �Q  U ^_^ I  *�M`�L�M 0 
writeplist 
writePlist` aba o   �K�K 0 theplistpath thePListPathb cdc m   #ee �ff   a e _ w a i v e r _ c o u n t 1d g�Jg o  #&�I�I 0 mycount myCount�J  �L  _ hih l ++�H�G�F�H  �G  �F  i jkj l ++�Elm�E  l   see if files are moving	   m �nn 2   s e e   i f   f i l e s   a r e   m o v i n g 	k opo r  +2qrq I  +0�D�C�B�D 0 getepochtime getEpochTime�C  �B  r o      �A�A 0 timenow timeNowp sts r  3Cuvu I  3A�@w�?�@ 0 number_to_string  w x�>x [  4=yzy o  45�=�= :0 laststalefilechecktimestamp lastStaleFileCheckTimestampz l 5<{�<�;{ ]  5<|}| o  5:�:�: 00 expectedmaxprocesstime expectedMaxProcessTime} m  :;�9�9  ���<  �;  �>  �?  v o      �8�8 0 	nextcheck 	nextCheckt ~~ Z  D����7�� G  D[��� G  DQ��� l DG��6�5� A  DG��� o  DE�4�4 0 	nextcheck 	nextCheck� o  EF�3�3 0 timenow timeNow�6  �5  � l JM��2�1� = JM��� o  JK�0�0 0 
initialrun 
initialRun� m  KL�/
�/ boovtrue�2  �1  � l TW��.�-� = TW��� o  TU�,�, *0 forcestalefilecheck forceStaleFileCheck� m  UV�+
�+ boovtrue�.  �-  � k  ^��� ��� l ^^�*���*  �  check files   � ���  c h e c k   f i l e s� ��� r  ^m��� I  ^i�)��(�) &0 checkpendingfiles checkPendingFiles� ��� o  _b�'�' 00 pendingaewaiverlistold pendingAEWaiverListOLD� ��&� o  be�%�% 0 mylist myList�&  �(  � o      �$�$ 0 isprocessing isProcessing� ��� r  nu��� o  nq�#�# 0 mylist myList� o      �"�" 00 pendingaewaiverlistold pendingAEWaiverListOLD� ��� Z  v����!�� = v{��� o  vy� �  0 isprocessing isProcessing� m  yz�
� boovfals� k  ~��� ��� r  ~���� m  ~�
� boovtrue� o      �� *0 forcestalefilecheck forceStaleFileCheck� ��� r  ����� m  ���
� boovfals� o      �� 0 ishappy isHappy� ��� r  ����� b  ����� o  ���� 0 failedtests failedTests� m  ���� ��� ( a e _ p e n d i n g _ w a i v e r s ,  � o      �� 0 failedtests failedTests� ��� Z  ������� = ����� o  ���� 0 dev_mode  � m  ���
� boovtrue� I �����
� .sysodlogaskr        TEXT� m  ���� ��� 8 N O T   H A P P Y   a e _ p e n d i n g _ w a i v e r s�  �  �  �  �!  � l ������  �  isProcessing is true			   � ��� . i s P r o c e s s i n g   i s   t r u e 	 	 	� ��� r  ����� m  ���
� boovtrue� o      �� 40 updatestalefiletimestamp updateStaleFileTimestamp� ��� l ������  �  �  �  �7  � l ���
���
  �   not time to check yet   � ��� ,   n o t   t i m e   t o   c h e c k   y e t ��� l ���	���	  �  �  � ��� l ������  �  �  � ��� r  ����� o  ���� 0 mycount myCount� o      �� 0 mydatavalue myDataValue� ��� r  ����� b  ����� b  ����� m  ���� ��� L { " n a m e " : " a e _ p e n d i n g _ w a i v e r s " , " v a l u e " : "� o  ���� 0 mydatavalue myDataValue� m  ���� ���  " }� o      � �  0 nextitem nextItem� ��� r  ����� b  ����� b  ����� o  ������ 0 	dataitems 	dataItems� m  ���� ���  ,� o  ������ 0 nextitem nextItem� o      ���� 0 	dataitems 	dataItems� ��� l ����������  ��  ��  � ��� r  ����� o  ������ 0 ae_waiver_delta  � o      ���� 0 mydatavalue myDataValue� ��� r  ����� b  ����� b  ����� m  ���� ��� F { " n a m e " : " a e _ w a i v e r _ d e l t a " , " v a l u e " : "� o  ������ 0 mydatavalue myDataValue� m  ���� ���  " }� o      ���� 0 nextitem nextItem� ��� r  ��� � b  �� b  �� o  ������ 0 	dataitems 	dataItems m  �� �  , o  ������ 0 nextitem nextItem  o      ���� 0 	dataitems 	dataItems�  l ����������  ��  ��   	
	 l ������     get age of oldest file    � .   g e t   a g e   o f   o l d e s t   f i l e
  r  � I  �������� (0 getageofoldestfile getAgeOfOldestFile  o  ������ @0 pendingwaiversforaefolderposix pendingWaiversForAEFolderPosix �� m  �� �  . x m l��  ��   o      ���� 0 mydatavalue myDataValue  Z ���� ?   o  ���� 0 mydatavalue myDataValue o  ���� 0 oldestfileage oldestFileAge r  
 o  
���� 0 mydatavalue myDataValue o      ���� 0 oldestfileage oldestFileAge��  ��    !  r  !"#" b  $%$ b  &'& m  (( �)) B { " n a m e " : " a e _ w a i v e r _ a g e " , " v a l u e " : "' o  ���� 0 mydatavalue myDataValue% m  ** �++  " }# o      ���� 0 nextitem nextItem! ,-, r  "+./. b  ")010 b  "'232 o  "#���� 0 	dataitems 	dataItems3 m  #&44 �55  ,1 o  '(���� 0 nextitem nextItem/ o      ���� 0 	dataitems 	dataItems- 676 l ,,��������  ��  ��  7 898 l ,,��������  ��  ��  9 :;: l ,,��<=��  <  check for errored waivers   = �>> 2 c h e c k   f o r   e r r o r e d   w a i v e r s; ?@? r  ,1ABA m  ,-����  B o      ���� 0 mythreshhold myThreshhold@ CDC r  2CEFE b  2AGHG b  2=IJI m  25KK �LL  l s  J n  5<MNM 1  8<��
�� 
strqN o  58���� @0 pendingwaiversforaefolderposix pendingWaiversForAEFolderPosixH m  =@OO �PP ( e r r o r /   |   g r e p   ' . x m l 'F o      ���� 0 shscript  D QRQ l DD��������  ��  ��  R STS Q  D�UVWU k  GoXX YZY r  G\[\[ I GX��]��
�� .corecnte****       ****] c  GT^_^ n  GP`a` 2 LP��
�� 
cpara l GLb����b I GL��c��
�� .sysoexecTEXT���     TEXTc o  GH���� 0 shscript  ��  ��  ��  _ m  PS��
�� 
list��  \ o      ���� 0 mycount myCountZ ded I  ]m��f���� 0 
writeplist 
writePlistf ghg o  ^c���� 0 theplistpath thePListPathh iji m  cfkk �ll $ a e _ e r r o r e d _ w a i v e r sj m��m o  fi���� 0 mycount myCount��  ��  e n��n l nn��������  ��  ��  ��  V R      ��o��
�� .ascrerr ****      � ****o o      ���� 
0 errmsg  ��  W k  w�pp qrq I w���s��
�� .ascrcmnt****      � ****s b  w�tut b  w�vwv l w|x����x I w|������
�� .misccurdldt    ��� null��  ��  ��  ��  w m  |yy �zz 2 p r o b a b l y   n o   w a i v e r s   y e t :  u o  ������ 
0 errmsg  ��  r {��{ r  ��|}| m  ������  } o      ���� 0 mycount myCount��  T ~~ l ����������  ��  ��   ��� Z  ��������� ?  ����� o  ������ 0 mycount myCount� o  ������ 0 mythreshhold myThreshhold� k  ���� ��� r  ����� m  ����
�� boovfals� o      ���� 0 ishappy isHappy� ��� r  ����� b  ����� o  ������ 0 failedtests failedTests� m  ���� ��� " a e _ e r r o r _ w a i v e r ,  � o      ���� 0 failedtests failedTests� ��� l ����������  ��  ��  � ���� Z  ��������� = ����� o  ������ 0 dev_mode  � m  ����
�� boovtrue� I �������
�� .sysodlogaskr        TEXT� m  ���� ��� 2 N O T   H A P P Y   a e _ e r r o r _ w a i v e r��  ��  ��  ��  ��  ��  � ��� r  ����� o  ������ 0 mycount myCount� o      ���� 0 mydatavalue myDataValue� ��� r  ����� b  ����� b  ����� m  ���� ��� F { " n a m e " : " a e _ e r r o r _ w a i v e r " , " v a l u e " : "� o  ������ 0 mydatavalue myDataValue� m  ���� ���  " }� o      ���� 0 nextitem nextItem� ��� r  ����� b  ����� b  ����� o  ������ 0 	dataitems 	dataItems� m  ���� ���  ,� o  ������ 0 nextitem nextItem� o      ���� 0 	dataitems 	dataItems� ��� l ����������  ��  ��  � ��� l ����������  ��  ��  � ��� l ��������  � ) #check pending AME files (AME queue)   � ��� F c h e c k   p e n d i n g   A M E   f i l e s   ( A M E   q u e u e )� ��� r  ����� b  ����� b  ����� m  ���� ���  l s  � n  ����� 1  ����
�� 
strq� o  ������ 0 	ame_watch  � m  ���� ���    |   g r e p   ' . m o v '� o      ���� 0 shscript  � ��� Q  �%���� r  ���� c  ���� n  ����� 2 ����
�� 
cpar� l �������� I �������
�� .sysoexecTEXT���     TEXT� o  ������ 0 shscript  ��  ��  ��  � m  � ��
�� 
list� o      ���� 0 mylist myList� R      ����
�� .ascrerr ****      � ****� o      �~�~ 
0 errmsg  �  � k  %�� ��� I �}��|
�} .ascrcmnt****      � ****� b  ��� b  ��� l ��{�z� I �y�x�w
�y .misccurdldt    ��� null�x  �w  �{  �z  � m  �� ��� 2 p r o b a b l y   n o   w a i v e r s   y e t :  � o  �v�v 
0 errmsg  �|  � ��u� r  %��� J  !�t�t  � o      �s�s 0 mylist myList�u  � ��� l &&�r�q�p�r  �q  �p  � ��� r  &1��� n  &-��� 1  )-�o
�o 
leng� o  &)�n�n 0 mylist myList� o      �m�m 0 mycount myCount� ��� r  2F��� I  2B�l��k�l 0 	readplist 	readPlist� ��� o  38�j�j 0 theplistpath thePListPath� ��� m  8;�� ���  a m e _ c o u n t _ 1� ��i� m  ;>�h
�h 
null�i  �k  � o      �g�g 0 oldcount oldCount� ��� r  GR   \  GN o  GJ�f�f 0 mycount myCount o  JM�e�e 0 oldcount oldCount o      �d�d 0 ame_file_delta  �  I  Sc�c�b�c 0 
writeplist 
writePlist  o  TY�a�a 0 theplistpath thePListPath 	
	 m  Y\ �  a m e _ q u e u e _ d e l t a
 �` o  \_�_�_ 0 ame_file_delta  �`  �b    I  dt�^�]�^ 0 
writeplist 
writePlist  o  ej�\�\ 0 theplistpath thePListPath  m  jm �  a m e _ c o u n t _ 1 �[ o  mp�Z�Z 0 mycount myCount�[  �]    l uu�Y�X�W�Y  �X  �W    l uu�V�V     see if files are moving	    � 2   s e e   i f   f i l e s   a r e   m o v i n g 	   r  u|!"! I  uz�U�T�S�U 0 getepochtime getEpochTime�T  �S  " o      �R�R 0 timenow timeNow  #$# r  }�%&% I  }��Q'�P�Q 0 number_to_string  ' (�O( [  ~�)*) o  ~�N�N :0 laststalefilechecktimestamp lastStaleFileCheckTimestamp* l �+�M�L+ ]  �,-, o  ��K�K 00 expectedmaxprocesstime expectedMaxProcessTime- m  ���J�J  ���M  �L  �O  �P  & o      �I�I 0 	nextcheck 	nextCheck$ ./. Z  ��01�H20 G  ��343 G  ��565 l ��7�G�F7 A  ��898 o  ���E�E 0 	nextcheck 	nextCheck9 o  ���D�D 0 timenow timeNow�G  �F  6 l ��:�C�B: = ��;<; o  ���A�A 0 
initialrun 
initialRun< m  ���@
�@ boovtrue�C  �B  4 l ��=�?�>= = ��>?> o  ���=�= *0 forcestalefilecheck forceStaleFileCheck? m  ���<
�< boovtrue�?  �>  1 k  ��@@ ABA l ���;CD�;  C  check files   D �EE  c h e c k   f i l e sB FGF r  ��HIH I  ���:J�9�: &0 checkpendingfiles checkPendingFilesJ KLK o  ���8�8 .0 pendingamefilelistold pendingAMEFileListOLDL M�7M o  ���6�6 0 mylist myList�7  �9  I o      �5�5 0 isprocessing isProcessingG NON r  ��PQP o  ���4�4 0 mylist myListQ o      �3�3 .0 pendingamefilelistold pendingAMEFileListOLDO RSR Z  ��TU�2�1T = ��VWV o  ���0�0 0 isprocessing isProcessingW m  ���/
�/ boovfalsU k  ��XX YZY r  ��[\[ m  ���.
�. boovfals\ o      �-�- 0 ishappy isHappyZ ]^] r  ��_`_ m  ���,
�, boovtrue` o      �+�+ *0 forcestalefilecheck forceStaleFileCheck^ aba r  ��cdc b  ��efe o  ���*�* 0 failedtests failedTestsf m  ��gg �hh & a m e _ p e n d i n g _ f i l e s ,  d o      �)�) 0 failedtests failedTestsb i�(i Z  ��jk�'�&j = ��lml o  ���%�% 0 dev_mode  m m  ���$
�$ boovtruek I ���#n�"
�# .sysodlogaskr        TEXTn m  ��oo �pp 6 N O T   H A P P Y   a m e _ p e n d i n g _ f i l e s�"  �'  �&  �(  �2  �1  S q�!q r  ��rsr m  ��� 
�  boovtrues o      �� 40 updatestalefiletimestamp updateStaleFileTimestamp�!  �H  2 l ���tu�  t   not time to check   u �vv $   n o t   t i m e   t o   c h e c k/ wxw l ������  �  �  x yzy r  �{|{ o  ���� 0 mycount myCount| o      �� 0 mydatavalue myDataValuez }~} r  � b  ��� b  	��� m  �� ��� J { " n a m e " : " a m e _ p e n d i n g _ f i l e s " , " v a l u e " : "� o  �� 0 mydatavalue myDataValue� m  	�� ���  " }� o      �� 0 nextitem nextItem~ ��� r  ��� b  ��� b  ��� o  �� 0 	dataitems 	dataItems� m  �� ���  ,� o  �� 0 nextitem nextItem� o      �� 0 	dataitems 	dataItems� ��� l ����  �  �  � ��� r  !��� o  �� 0 ame_file_delta  � o      �� 0 mydatavalue myDataValue� ��� r  "/��� b  "-��� b  ")��� m  "%�� ��� J { " n a m e " : " a m e _ p e n d i n g _ d e l t a " , " v a l u e " : "� o  %(�� 0 mydatavalue myDataValue� m  ),�� ���  " }� o      �� 0 nextitem nextItem� ��� r  09��� b  07��� b  05��� o  01�� 0 	dataitems 	dataItems� m  14�� ���  ,� o  56�� 0 nextitem nextItem� o      �
�
 0 	dataitems 	dataItems� ��� l ::�	���	  �  �  � ��� l ::����  �   get age of oldest file   � ��� .   g e t   a g e   o f   o l d e s t   f i l e� ��� r  :I��� I  :E���� (0 getageofoldestfile getAgeOfOldestFile� ��� o  ;>�� 0 	ame_watch  � ��� m  >A�� ���  . m o v�  �  � o      �� 0 mydatavalue myDataValue� ��� Z J[��� ��� ?  JO��� o  JM���� 0 mydatavalue myDataValue� o  MN���� 0 oldestfileage oldestFileAge� r  RW��� o  RU���� 0 mydatavalue myDataValue� o      ���� 0 oldestfileage oldestFileAge�   ��  � ��� r  \i��� b  \g��� b  \c��� m  \_�� ��� @ { " n a m e " : " a m e _ f i l e _ a g e " , " v a l u e " : "� o  _b���� 0 mydatavalue myDataValue� m  cf�� ���  " }� o      ���� 0 nextitem nextItem� ��� r  js��� b  jq��� b  jo��� o  jk���� 0 	dataitems 	dataItems� m  kn�� ���  ,� o  op���� 0 nextitem nextItem� o      ���� 0 	dataitems 	dataItems� ��� l tt��������  ��  ��  � ��� l tt��������  ��  ��  � ��� l tt������  � $ check pending incoming waivers   � ��� < c h e c k   p e n d i n g   i n c o m i n g   w a i v e r s� ��� r  t���� b  t���� b  t��� m  tw�� ���  l s  � n  w~��� 1  z~��
�� 
strq� o  wz���� .0 incomingrawxmlwaivers incomingRawXmlWaivers� m  ��� ���    |   g r e p   ' . x m l '� o      ���� 0 shscript  � ��� Q  ������ r  ����� c  ����� n  ����� 2 ����
�� 
cpar� l �� ����  I ������
�� .sysoexecTEXT���     TEXT o  ������ 0 shscript  ��  ��  ��  � m  ����
�� 
list� o      ���� 0 mylist myList� R      ����
�� .ascrerr ****      � **** o      ���� 
0 errmsg  ��  � k  ��  I ������
�� .ascrcmnt****      � **** b  �� b  ��	
	 l ������ I ��������
�� .misccurdldt    ��� null��  ��  ��  ��  
 m  �� � 2 p r o b a b l y   n o   w a i v e r s   y e t :   o  ������ 
0 errmsg  ��   �� r  �� J  ������   o      ���� 0 mylist myList��  �  l ����������  ��  ��    r  �� n  �� 1  ����
�� 
leng o  ������ 0 mylist myList o      ���� 0 mycount myCount  r  �� I  �������� 0 	readplist 	readPlist  o  ������ 0 theplistpath thePListPath  !  m  ��"" �## ( d r o p b o x _ w a i v e r _ c o u n t! $��$ m  ����
�� 
null��  ��   o      ���� 0 oldcount oldCount %&% r  ��'(' \  ��)*) o  ������ 0 mycount myCount* o  ������ 0 oldcount oldCount( o      ���� 0 dropbox_waiver_delta  & +,+ I  ����-���� 0 
writeplist 
writePlist- ./. o  ������ 0 theplistpath thePListPath/ 010 m  ��22 �33 ( d r o p b o x _ w a i v e r _ d e l t a1 4��4 o  ������ 0 dropbox_waiver_delta  ��  ��  , 565 I  �	��7���� 0 
writeplist 
writePlist7 898 o  ������ 0 theplistpath thePListPath9 :;: m  �<< �== ( d r o p b o x _ w a i v e r _ c o u n t; >��> o  ���� 0 mycount myCount��  ��  6 ?@? l 

��������  ��  ��  @ ABA l 

��CD��  C   see if files are moving	   D �EE 2   s e e   i f   f i l e s   a r e   m o v i n g 	B FGF r  
HIH I  
�������� 0 getepochtime getEpochTime��  ��  I o      ���� 0 timenow timeNowG JKJ r  "LML I   ��N���� 0 number_to_string  N O��O [  PQP o  ���� :0 laststalefilechecktimestamp lastStaleFileCheckTimestampQ l R����R ]  STS o  ���� 00 expectedmaxprocesstime expectedMaxProcessTimeT m  ����  ����  ��  ��  ��  M o      ���� 0 	nextcheck 	nextCheckK UVU Z  #�WX��YW G  #:Z[Z G  #0\]\ l #&^����^ A  #&_`_ o  #$���� 0 	nextcheck 	nextCheck` o  $%���� 0 timenow timeNow��  ��  ] l ),a����a = ),bcb o  )*���� 0 
initialrun 
initialRunc m  *+��
�� boovtrue��  ��  [ l 36d����d = 36efe o  34���� *0 forcestalefilecheck forceStaleFileCheckf m  45��
�� boovtrue��  ��  X k  =�gg hih l ==��jk��  j  check files   k �ll  c h e c k   f i l e si mnm r  =Lopo I  =H��q���� &0 checkpendingfiles checkPendingFilesq rsr o  >A���� :0 pendingdropboxwaiverlistold pendingDropboxWaiverListOLDs t��t o  AD���� 0 mylist myList��  ��  p o      ���� 0 isprocessing isProcessingn uvu r  MTwxw o  MP���� 0 mylist myListx o      ���� :0 pendingdropboxwaiverlistold pendingDropboxWaiverListOLDv yzy Z  U�{|����{ = UZ}~} o  UX���� 0 isprocessing isProcessing~ m  XY��
�� boovfals| k  ]� ��� r  ]`��� m  ]^��
�� boovfals� o      ���� 0 ishappy isHappy� ��� r  ad��� m  ab��
�� boovtrue� o      ���� *0 forcestalefilecheck forceStaleFileCheck� ��� r  el��� b  ej��� o  ef���� 0 failedtests failedTests� m  fi�� ��� " d r o p b o x _ w a i v e r s ,  � o      ���� 0 failedtests failedTests� ���� Z  m�������� = mt��� o  mr���� 0 dev_mode  � m  rs��
�� boovtrue� I w~�����
�� .sysodlogaskr        TEXT� m  wz�� ��� 2 N O T   H A P P Y   d r o p b o x _ w a i v e r s��  ��  ��  ��  ��  ��  z ���� r  ����� m  ����
�� boovtrue� o      ���� 40 updatestalefiletimestamp updateStaleFileTimestamp��  ��  Y l ��������  �   not time to check   � ��� $   n o t   t i m e   t o   c h e c kV ��� l ������~��  �  �~  � ��� r  ����� o  ���}�} 0 mycount myCount� o      �|�| 0 mydatavalue myDataValue� ��� r  ����� b  ����� b  ����� m  ���� ��� F { " n a m e " : " d r o p b o x _ w a i v e r s " , " v a l u e " : "� o  ���{�{ 0 mydatavalue myDataValue� m  ���� ���  " }� o      �z�z 0 nextitem nextItem� ��� r  ����� b  ����� b  ����� o  ���y�y 0 	dataitems 	dataItems� m  ���� ���  ,� o  ���x�x 0 nextitem nextItem� o      �w�w 0 	dataitems 	dataItems� ��� l ���v�u�t�v  �u  �t  � ��� r  ����� o  ���s�s 0 dropbox_waiver_delta  � o      �r�r 0 mydatavalue myDataValue� ��� r  ����� b  ����� b  ����� m  ���� ��� P { " n a m e " : " d r o p b o x _ w a i v e r _ d e l t a " , " v a l u e " : "� o  ���q�q 0 mydatavalue myDataValue� m  ���� ���  " }� o      �p�p 0 nextitem nextItem� ��� r  ����� b  ����� b  ����� o  ���o�o 0 	dataitems 	dataItems� m  ���� ���  ,� o  ���n�n 0 nextitem nextItem� o      �m�m 0 	dataitems 	dataItems� ��� l ���l�k�j�l  �k  �j  � ��� l ���i���i  �   get age of oldest file   � ��� .   g e t   a g e   o f   o l d e s t   f i l e� ��� r  ����� I  ���h��g�h (0 getageofoldestfile getAgeOfOldestFile� ��� o  ���f�f .0 incomingrawxmlwaivers incomingRawXmlWaivers� ��e� m  ���� ���  . x m l�e  �g  � o      �d�d 0 mydatavalue myDataValue� ��� Z �����c�b� ?  ����� o  ���a�a 0 mydatavalue myDataValue� o  ���`�` 0 oldestfileage oldestFileAge� r  ����� o  ���_�_ 0 mydatavalue myDataValue� o      �^�^ 0 oldestfileage oldestFileAge�c  �b  � ��� r  ����� b  ����� b  ����� m  ���� ��� L { " n a m e " : " d r o p b o x _ w a i v e r _ a g e " , " v a l u e " : "� o  ���]�] 0 mydatavalue myDataValue� m  ���� ���  " }� o      �\�\ 0 nextitem nextItem� ��� r  ���� b  �� � b  � o  � �[�[ 0 	dataitems 	dataItems m    �  ,  o  �Z�Z 0 nextitem nextItem� o      �Y�Y 0 	dataitems 	dataItems�  l 		�X�W�V�X  �W  �V    l 		�U	
�U  	  get memory stats   
 �   g e t   m e m o r y   s t a t s  Q  	* r   I �T�S
�T .sysoexecTEXT���     TEXT m   � l t o p   - l   1   |   h e a d   - n   1 0   |   g r e p   P h y s M e m   |   s e d   ' s / ,   / n   / g '�S   o      �R�R 0 memory_stats   R      �Q�P
�Q .ascrerr ****      � **** o      �O�O 
0 errmsg  �P   r  * b  & m  " � . E R R O R   R E A D I N G   M E M O R Y   -   o  "%�N�N 
0 errmsg   o      �M�M 0 memory_stats    l ++�L�K�J�L  �K  �J     r  +2!"! o  +.�I�I 0 memory_stats  " o      �H�H 0 mydatavalue myDataValue  #$# r  3@%&% b  3>'(' b  3:)*) m  36++ �,, B { " n a m e " : " m e m o r y _ s t a t u s " , " v a l u e " : "* o  69�G�G 0 mydatavalue myDataValue( m  :=-- �..  " }& o      �F�F 0 nextitem nextItem$ /0/ r  AJ121 b  AH343 b  AF565 o  AB�E�E 0 	dataitems 	dataItems6 m  BE77 �88  ,4 o  FG�D�D 0 nextitem nextItem2 o      �C�C 0 	dataitems 	dataItems0 9:9 l KK�B�A�@�B  �A  �@  : ;<; l KK�?=>�?  =  get free disk stats   > �?? & g e t   f r e e   d i s k   s t a t s< @A@ r  KXBCB c  KTDED I  KP�>�=�<�>  0 getfreediskpct getFreeDiskPct�=  �<  E m  PS�;
�; 
longC o      �:�: 0 freediskpct freeDiskPctA FGF r  YfHIH c  YbJKJ I  Y^�9�8�7�9 (0 getfreediskspacegb getFreeDiskSpaceGB�8  �7  K m  ^a�6
�6 
longI o      �5�5 0 
freediskgb 
freeDiskGBG LML Z  g�NO�4�3N G  g|PQP l gnR�2�1R A  gnSTS o  gj�0�0 0 
freediskgb 
freeDiskGBT m  jm�/�/ �2  �1  Q l qxU�.�-U A  qxVWV o  qt�,�, 0 freediskpct freeDiskPctW m  tw�+�+ 
�.  �-  O k  �XX YZY r  �[\[ m  ��*
�* boovfals\ o      �)�) 0 ishappy isHappyZ ]^] r  ��_`_ b  ��aba o  ���(�( 0 failedtests failedTestsb m  ��cc �dd  d i s k _ f r e e _ g b ,  ` o      �'�' 0 failedtests failedTests^ e�&e Z  ��fg�%�$f = ��hih o  ���#�# 0 dev_mode  i m  ���"
�" boovtrueg I ���!j� 
�! .sysodisAaleR        TEXTj m  ��kk �ll , N O T   H A P P Y   d i s k _ f r e e _ g b�   �%  �$  �&  �4  �3  M mnm l ������  �  �  n opo r  ��qrq o  ���� 0 
freediskgb 
freeDiskGBr o      �� 0 mydatavalue myDataValuep sts r  ��uvu b  ��wxw b  ��yzy m  ��{{ �|| @ { " n a m e " : " d i s k _ f r e e _ g b " , " v a l u e " : "z o  ���� 0 mydatavalue myDataValuex m  ��}} �~~  " }v o      �� 0 nextitem nextItemt � r  ����� b  ����� b  ����� o  ���� 0 	dataitems 	dataItems� m  ���� ���  ,� o  ���� 0 nextitem nextItem� o      �� 0 	dataitems 	dataItems� ��� l ������  �  �  � ��� r  ����� o  ���� 0 freediskpct freeDiskPct� o      �� 0 mydatavalue myDataValue� ��� r  ����� b  ����� b  ����� m  ���� ��� J { " n a m e " : " d i s k _ f r e e _ p e r c e n t " , " v a l u e " : "� o  ���� 0 mydatavalue myDataValue� m  ���� ���  " }� o      �� 0 nextitem nextItem� ��� r  ����� b  ����� b  ����� o  ���� 0 	dataitems 	dataItems� m  ���� ���  ,� o  ���� 0 nextitem nextItem� o      �� 0 	dataitems 	dataItems� ��� l ����
�	�  �
  �	  � ��� l ������  � !  get computer ID from pList   � ��� 6   g e t   c o m p u t e r   I D   f r o m   p L i s t� ��� Q  ����� r  ����� I  ������ 0 	readplist 	readPlist� ��� o  ���� 0 theplistpath thePListPath� ��� m  ���� ���  c o m p u t e r _ i d� ��� m  ���� ���  m a c S t a d i u m�  �  � o      �� 0 mydatavalue myDataValue� R      ���
� .ascrerr ****      � ****� o      � �  
0 errmsg  �  � r  ��� b  ��� m  �� ��� 0 C o m p u t e r   I D   n o t   f o u n d   -  � o  
���� 
0 errmsg  � o      ���� 0 mydatavalue myDataValue� ��� r  ��� b  ��� b  ��� m  �� ��� > { " n a m e " : " c o m p u t e r _ i d " , " v a l u e " : "� o  ���� 0 mydatavalue myDataValue� m  �� ���  " }� o      ���� 0 nextitem nextItem� ��� r  '��� b  %��� b  #��� o  ���� 0 	dataitems 	dataItems� m  "�� ���  ,� o  #$���� 0 nextitem nextItem� o      ���� 0 	dataitems 	dataItems� ��� l ((��������  ��  ��  � ��� l ((������  �  get IP address   � ���  g e t   I P   a d d r e s s� ��� r  (5��� n  (1��� 1  -1��
�� 
siip� l (-������ e  (-�� I (-������
�� .sysosigtsirr   ��� null��  ��  ��  ��  � o      ���� 0 mydatavalue myDataValue� ��� r  6C��� b  6A��� b  6=��� m  69�� ��� R { " n a m e " : " c o m p u t e r _ I P v 4 _ a d d r e s s " , " v a l u e " : "� o  9<���� 0 mydatavalue myDataValue� m  =@�� ���  " }� o      ���� 0 nextitem nextItem� ��� r  DM��� b  DK��� b  DI��� o  DE���� 0 	dataitems 	dataItems� m  EH�� ���  ,� o  IJ���� 0 nextitem nextItem� o      ���� 0 	dataitems 	dataItems� � � l NN��������  ��  ��     l NN��������  ��  ��    l NN��������  ��  ��    r  NS o  NO���� 0 oldestfileage oldestFileAge o      ���� 0 mydatavalue myDataValue 	
	 r  Ta b  T_ b  T[ m  TW � B { " n a m e " : " o l d e s t F i l e A g e " , " v a l u e " : " o  WZ���� 0 mydatavalue myDataValue m  [^ �  " } o      ���� 0 nextitem nextItem
  r  bk b  bi b  bg o  bc���� 0 	dataitems 	dataItems m  cf �  , o  gh���� 0 nextitem nextItem o      ���� 0 	dataitems 	dataItems   l ll��������  ��  ��    !"! Z  l�#$����# ?  ls%&% o  lm���� 0 oldestfileage oldestFileAge& o  mr���� 00 oldestfileagethreshold oldestFileAgeThreshold$ k  v}'' ()( l vv��*+��  *   set isHappy to false   + �,, *   s e t   i s H a p p y   t o   f a l s e) -��- r  v}./. b  v{010 o  vw���� 0 failedtests failedTests1 m  wz22 �33  o l d e s t F i l e A g e ,  / o      ���� 0 failedtests failedTests��  ��  ��  " 454 l ����������  ��  ��  5 676 Z  ��89��:8 = ��;<; o  ������ 0 ishappy isHappy< m  ����
�� boovtrue9 k  ��== >?> r  ��@A@ m  ��BB �CC  U PA o      ���� 0 mydatavalue myDataValue? D��D r  ��EFE m  ����
�� boovfalsF o      ���� *0 forcestalefilecheck forceStaleFileCheck��  ��  : k  ��GG HIH r  ��JKJ m  ��LL �MM  D O W NK o      ���� 0 mydatavalue myDataValueI N��N Z  ��OP����O = ��QRQ o  ������ 0 dev_mode  R m  ����
�� boovtrueP I ����S��
�� .sysodisAaleR        TEXTS m  ��TT �UU " N O T   H A P P Y   O V E R A L L��  ��  ��  ��  7 VWV r  ��XYX b  ��Z[Z b  ��\]\ m  ��^^ �__ D { " n a m e " : " o v e r a l l _ s t a t u s " , " v a l u e " : "] o  ������ 0 mydatavalue myDataValue[ m  ��`` �aa  " }Y o      ���� 0 nextitem nextItemW bcb r  ��ded b  ��fgf b  ��hih o  ������ 0 	dataitems 	dataItemsi m  ��jj �kk  ,g o  ������ 0 nextitem nextIteme o      ���� 0 	dataitems 	dataItemsc lml l ����������  ��  ��  m non r  ��pqp o  ������ 0 failedtests failedTestsq o      ���� 0 mydatavalue myDataValueo rsr r  ��tut b  ��vwv b  ��xyx m  ��zz �{{ > { " n a m e " : " f a i l e d T e s t s " , " v a l u e " : "y o  ������ 0 mydatavalue myDataValuew m  ��|| �}}  " }u o      ���� 0 nextitem nextItems ~~ r  ����� b  ����� b  ����� o  ������ 0 	dataitems 	dataItems� m  ���� ���  ,� o  ������ 0 nextitem nextItem� o      ���� 0 	dataitems 	dataItems ��� l ����������  ��  ��  � ��� r  ����� I  ���������� 0 getepochtime getEpochTime��  ��  � o      ���� 0 mydatavalue myDataValue� ��� r  �	��� b  ����� b  ����� m  ���� ��� L { " n a m e " : " c u r r e n t _ e p o c h _ t i m e " , " v a l u e " : "� o  ������ 0 mydatavalue myDataValue� m  ���� ���  " }� o      ���� 0 nextitem nextItem� ��� r  		��� b  			��� b  		��� o  		���� 0 	dataitems 	dataItems� m  		�� ���  ,� o  		���� 0 nextitem nextItem� o      ���� 0 	dataitems 	dataItems� ��� l 		��������  ��  ��  � ��� r  		��� n  		��� 1  		��
�� 
siip� l 		������ e  		�� I 		������
�� .sysosigtsirr   ��� null��  ��  ��  ��  � o      ���� 0 mydatavalue myDataValue� ��� r  		'��� b  		%��� b  		!��� m  		�� ��� 0 { " n a m e " : " R F I D " , " v a l u e " : "� o  		 ���� 0 mydatavalue myDataValue� m  	!	$�� ���  " }� o      ���� 0 nextitem nextItem� ��� r  	(	1��� b  	(	/��� b  	(	-��� o  	(	)���� 0 	dataitems 	dataItems� m  	)	,�� ���  ,� o  	-	.���� 0 nextitem nextItem� o      ���� 0 	dataitems 	dataItems� ��� l 	2	2�������  ��  �  � ��� Z  	2	E����� = 	2	9��� o  	2	7�� 0 dev_mode  � m  	7	8�
� boovtrue� I 	<	A���
� .sysodlogaskr        TEXT� o  	<	=�� 0 	dataitems 	dataItems�  �  �  � ��� l 	F	F����  �  �  � ��� I  	F	L���� "0 sendstatustobpc sendStatusToBPC� ��� o  	G	H�� 0 	dataitems 	dataItems�  �  � ��� r  	M	T��� I  	M	R���~� 0 getepochtime getEpochTime�  �~  � o      �}�} 80 laststatsgatheredtimestamp lastStatsGatheredTimestamp� ��� l 	U	U�|�{�z�|  �{  �z  � ��� Z  	U	f���y�x� = 	U	X��� o  	U	V�w�w 40 updatestalefiletimestamp updateStaleFileTimestamp� m  	V	W�v
�v boovtrue� r  	[	b��� I  	[	`�u�t�s�u 0 getepochtime getEpochTime�t  �s  � o      �r�r :0 laststalefilechecktimestamp lastStaleFileCheckTimestamp�y  �x  � ��� l 	g	g�q�p�o�q  �p  �o  � ��� Z  	g	����n�m� F  	g	x��� = 	g	j��� o  	g	h�l�l *0 recycleaftereffects recycleAfterEffects� m  	h	i�k
�k boovtrue� = 	m	t��� o  	m	r�j�j 0 dev_mode  � m  	r	s�i
�i boovfals� I  	{	��h�g�f�h *0 recycleaftereffects recycleAfterEffects�g  �f  �n  �m  � ��� l 	�	��e�d�c�e  �d  �c  � ��b� l 	�	��a�`�_�a  �`  �_  �b  � ��� l     �^�]�\�^  �]  �\  � ��� i   � ���� I      �[��Z�[ "0 sendstatustobpc sendStatusToBPC� ��Y� o      �X�X 0 	dataitems 	dataItems�Y  �Z  � k     ��� ��� l     �W���W  � [ U https://dev-api.bpcreates.com/stats/userDataCount?e=1000&type=FIRST+NAME&value=stacy   � ��� �   h t t p s : / / d e v - a p i . b p c r e a t e s . c o m / s t a t s / u s e r D a t a C o u n t ? e = 1 0 0 0 & t y p e = F I R S T + N A M E & v a l u e = s t a c y� ��� l     �V�U�T�V  �U  �T  �    r     	 n      1    �S
�S 
siip l    �R�Q e      I    �P�O�N
�P .sysosigtsirr   ��� null�O  �N  �R  �Q   o      �M�M 0 myip myIP 	 l  
 
�L�K�J�L  �K  �J  	 

 r   
  m   
  � 2 a p i . b p c r e a t e s . c o m / a p i / c u i o      �I�I 0 
theurlbase 
theURLbase  r    $ I    �H�G�H 0 	readplist 	readPlist  o    �F�F 0 theplistpath thePListPath  m     �  d e v _ e n d p o i n t s �E o    �D�D 0 devendpoints devEndpoints�E  �G   o      �C�C 0 devendpoints devEndpoints  r   % 2 c   % , !  o   % *�B�B 0 devendpoints devEndpoints! m   * +�A
�A 
bool o      �@�@ 0 devendpoints devEndpoints "#" Z   3 J$%�?&$ =  3 :'(' o   3 8�>�> 0 devendpoints devEndpoints( m   8 9�=
�= boovtrue% r   = B)*) b   = @+,+ m   = >-- �..  h t t p s : / / d e v -, o   > ?�<�< 0 
theurlbase 
theURLbase* o      �;�; 0 
theurlbase 
theURLbase�?  & r   E J/0/ b   E H121 m   E F33 �44  h t t p s : / /2 o   F G�:�: 0 
theurlbase 
theURLbase0 o      �9�9 0 
theurlbase 
theURLbase# 565 l  K K�8�7�6�8  �7  �6  6 787 r   K V9:9 b   K T;<; b   K R=>= b   K P?@? b   K NABA m   K LCC �DD0 { " a u t h " :   { " a g e n c y " :   " M a c S t a d i u m _ M o n i t o r i n g " , " a p i K e y " :   " 4 b 4 9 5 6 a d c 6 6 0 0 7 f f 9 9 0 f a 6 4 5 1 8 d 7 7 2 b 9 " } , " s u b m i s s i o n T y p e " :   " a d d " , " u i d L i s t " :   [ { " n a m e " :   " R F I D " , " v a l u e " :   "B o   L M�5�5 0 myip myIP@ m   N OEE �FF x " } ] , " e v e n t C o d e " :   " 3 Q J V D " , " d i P a r a m F i l t e r " :   " r " , " d a t a I t e m s " :   [> o   P Q�4�4 0 	dataitems 	dataItems< m   R SGG �HH  ] }: o      �3�3 0 
thepayload 
thePayload8 IJI l  W W�2�1�0�2  �1  �0  J KLK l  W W�/�.�-�/  �.  �-  L MNM r   W bOPO b   W `QRQ b   W ^STS b   W \UVU m   W XWW �XX � c u r l   - v   - H   " A c c e p t :   a p p l i c a t i o n / j s o n "   - H   " C o n t e n t - t y p e :   a p p l i c a t i o n / j s o n "   - X   P O S T   - d  V n   X [YZY 1   Y [�,
�, 
strqZ o   X Y�+�+ 0 
thepayload 
thePayloadT m   \ ][[ �\\   R o   ^ _�*�* 0 
theurlbase 
theURLbaseP o      �)�) 0 shscript  N ]^] l  c c�(�'�&�(  �'  �&  ^ _`_ Z   c vab�%�$a =  c jcdc o   c h�#�# 0 dev_mode  d m   h i�"
�" boovtrueb I  m r�!e� 
�! .sysodlogaskr        TEXTe o   m n�� 0 shscript  �   �%  �$  ` fgf l  w w����  �  �  g hih Q   w �jklj k   z �mm non r   z �pqp I  z �r�
� .sysoexecTEXT���     TEXTr o   z {�� 0 shscript  �  q o      �� 0 	myoutcome 	myOutcomeo s�s L   � �tt o   � ��� 0 	myoutcome 	myOutcome�  k R      �u�
� .ascrerr ****      � ****u o      �� 
0 errmsg  �  l L   � �vv o   � ��� 
0 errmsg  i w�w l  � �����  �  �  �  � xyx l     ����  �  �  y z{z i   � �|}| I      �
�	��
 0 setvars setVars�	  �  } k    J~~ � Q    H���� k   >�� ��� r    	��� c    ��� J    ��  � m    �
� 
list� o      �� 00 pendingaewaiverlistold pendingAEWaiverListOLD� ��� r   
 ��� c   
 ��� J   
 ��  � m    �
� 
list� o      �� .0 pendingamefilelistold pendingAMEFileListOLD� ��� r    ��� c    ��� J    ��  � m    � 
�  
list� o      ���� :0 pendingdropboxwaiverlistold pendingDropboxWaiverListOLD� ��� l   ��������  ��  ��  � ��� r    &��� I    $������� 0 	readplist 	readPlist� ��� o    ���� 0 theplistpath thePListPath� ��� m    �� ���  c o m p u t e r _ i d� ���� m     ��
�� 
null��  ��  � o      ���� 0 computer_id  � ��� l  ' '��������  ��  ��  � ��� r   ' =��� I   ' 7������� 0 	readplist 	readPlist� ��� o   ( -���� 0 theplistpath thePListPath� ��� m   - .�� ���   c o m p u t e r U s e r N a m e� ���� o   . 3���� $0 computerusername computerUserName��  ��  � o      ���� $0 computerusername computerUserName� ��� l  > >��������  ��  ��  � ��� r   > V��� c   > P��� I   > N������� 0 	readplist 	readPlist� ��� o   ? D���� 0 theplistpath thePListPath� ��� m   D E�� ���  d e v _ m o d e� ���� o   E J���� 0 dev_mode  ��  ��  � m   N O��
�� 
bool� o      ���� 0 dev_mode  � ��� r   W o��� c   W i��� I   W g������� 0 	readplist 	readPlist� ��� o   X ]���� 0 theplistpath thePListPath� ��� m   ] ^�� ���  d e v _ e n d p o i n t s� ���� o   ^ c���� 0 devendpoints devEndpoints��  ��  � m   g h��
�� 
bool� o      ���� 0 devendpoints devEndpoints� ��� l  p p��������  ��  ��  � ��� r   p ���� c   p ~��� I   p |������� 0 	readplist 	readPlist� ��� o   q v���� 0 theplistpath thePListPath� ��� m   v w�� ���  c l e a n u p� ���� m   w x��
�� boovtrue��  ��  � m   | }��
�� 
bool� o      ���� (0 performfilecleanup performFileCleanup� ��� l  � ���������  ��  ��  � ��� r   � ���� I   � �������� 0 	readplist 	readPlist� ��� o   � ����� 0 theplistpath thePListPath� ��� m   � ��� ���  s t a t i o n _ t y p e� ���� m   � ���
�� 
null��  ��  � o      ���� 0 stationtype stationType� ��� l  � ���������  ��  ��  � ��� l  � �������  � # set this to false for testing   � ��� : s e t   t h i s   t o   f a l s e   f o r   t e s t i n g� ��� l  � �������  �  set automated to true   � ��� * s e t   a u t o m a t e d   t o   t r u e� ��� r   � ���� c   � �� � I   � ������� 0 	readplist 	readPlist  o   � ����� 0 theplistpath thePListPath  m   � � �  a u t o m a t e d �� o   � ��� 0 	automated  ��  ��    m   � ��
� 
bool� o      �� 0 	automated  � 	
	 l  � �����  �  �  
  l  � ���    set loopDelaySecs to 10    � . s e t   l o o p D e l a y S e c s   t o   1 0  r   � � c   � � I   � ���� 0 	readplist 	readPlist  o   � ��� 0 theplistpath thePListPath  m   � � �  l o o p _ d e l a y � o   � ��� 0 loopdelaysecs loopDelaySecs�  �   m   � ��
� 
long o      �� 0 loopdelaysecs loopDelaySecs  l  � �����  �  �    !  l  � ��"#�  "  set processDelaySecs to 5   # �$$ 2 s e t   p r o c e s s D e l a y S e c s   t o   5! %&% r   � �'(' c   � �)*) I   � ��+�� 0 	readplist 	readPlist+ ,-, o   � ��� 0 theplistpath thePListPath- ./. m   � �00 �11  p r o c e s s _ d e l a y/ 2�2 o   � ��� $0 processdelaysecs processDelaySecs�  �  * m   � ��
� 
long( o      �� $0 processdelaysecs processDelaySecs& 343 l  � �����  �  �  4 565 l  � ��78�  7 I Cset waiver sequence, the interger to look for at end of waiver name   8 �99 � s e t   w a i v e r   s e q u e n c e ,   t h e   i n t e r g e r   t o   l o o k   f o r   a t   e n d   o f   w a i v e r   n a m e6 :;: r   �<=< c   � �>?> I   � ��@�� 0 	readplist 	readPlist@ ABA o   � ��� 0 theplistpath thePListPathB CDC m   � �EE �FF  w a i v e r _ s e q u e n c eD G�G o   � ��� $0 waivergrepstring waiverGrepString�  �  ? m   � ��
� 
list= o      �� $0 waivergrepstring waiverGrepString; HIH l ����  �  �  I JKJ l �LM�  L 1 + set delay when checking AE output filesize   M �NN V   s e t   d e l a y   w h e n   c h e c k i n g   A E   o u t p u t   f i l e s i z eK OPO r  QRQ c  STS I  �U�� 0 	readplist 	readPlistU VWV o  �� 0 theplistpath thePListPathW XYX m  ZZ �[[ $ f i l e s i z e C h e c k D e l a yY \�\ o  �� (0 filesizecheckdelay filesizeCheckDelay�  �  T m  �
� 
longR o      �� (0 filesizecheckdelay filesizeCheckDelayP ]^] l ����  �  �  ^ _`_ l �ab�  a G A set minimum file size expected from AE output (before transcode)   b �cc �   s e t   m i n i m u m   f i l e   s i z e   e x p e c t e d   f r o m   A E   o u t p u t   ( b e f o r e   t r a n s c o d e )` ded r  ;fgf c  5hih I  1�j�� 0 	readplist 	readPlistj klk o   %�� 0 theplistpath thePListPathl mnm m  %(oo �pp : f i l e s i z e C h e c k _ m i n A E O u t p u t S i z en q�q o  (-�� >0 filesizecheck_minaeoutputsize filesizeCheck_minAEOutputSize�  �  i m  14�
� 
longg o      �� >0 filesizecheck_minaeoutputsize filesizeCheck_minAEOutputSizee rsr l <<����  �  �  s tut l <<����  �  �  u v�v L  <>ww m  <=�~
�~ boovtrue�  � R      �}x�|
�} .ascrerr ****      � ****x o      �{�{ 
0 errmsg  �|  � L  FHyy o  FG�z�z 
0 errmsg  � z�yz l II�x�w�v�x  �w  �v  �y  { {|{ l     �u�t�s�u  �t  �s  | }~} i   � �� I      �r�q�p�r 0 setpaths setPaths�q  �p  � k    ��� ��� Q    ����� k   ��� ��� r    ��� I    �o��n�o 0 	readplist 	readPlist� ��� o    	�m�m 0 theplistpath thePListPath� ��� m   	 
�� ���  d r o p b o x _ p a t h� ��l� m   
 �� ��� v / U s e r s / b l u e p i x e l / D r o p b o x / p r e i n g e s t i o n _ p r o c e s s e s / v i d e o b o o t h /�l  �n  � o      �k�k "0 dropboxrootpath dropboxRootPath� ��� Z    +���j�� =   ��� o    �i�i 0 devendpoints devEndpoints� m    �h
�h boovtrue� r    #��� b    !��� b    ��� o    �g�g "0 dropboxrootpath dropboxRootPath� m    �� ���  d e v _� o     �f�f 0 stationtype stationType� o      �e�e "0 dropboxrootpath dropboxRootPath�j  � r   & +��� b   & )��� o   & '�d�d "0 dropboxrootpath dropboxRootPath� o   ' (�c�c 0 stationtype stationType� o      �b�b "0 dropboxrootpath dropboxRootPath� ��� l  , ,�a�`�_�a  �`  �_  � ��� r   , 1��� b   , /��� o   , -�^�^ "0 dropboxrootpath dropboxRootPath� m   - .�� ���  / w a i v e r s /� o      �]�] .0 incomingrawxmlwaivers incomingRawXmlWaivers� ��� r   2 7��� b   2 5��� o   2 3�\�\ "0 dropboxrootpath dropboxRootPath� m   3 4�� ���  / m e d i a /� o      �[�[ :0 incomingrawmediafolderposix incomingRawMediaFolderPosix� ��� l  8 8�Z�Y�X�Z  �Y  �X  � ��� r   8 C��� b   8 A��� b   8 ?��� m   8 9�� ���  / U s e r s /� o   9 >�W�W $0 computerusername computerUserName� m   ? @�� ��� : / D o c u m e n t s / p e n d i n g _ t r a n s c o d e /� o      �V�V :0 transcodependingfolderposix transcodePendingFolderPosix� ��� Q   D Z���� r   G O��� I   G M�U��T�U 0 folderexists folderExists� ��S� o   H I�R�R :0 transcodependingfolderposix transcodePendingFolderPosix�S  �T  � o      �Q�Q 0 filetest fileTest� R      �P��O
�P .ascrerr ****      � ****� o      �N�N 
0 errmsg  �O  � r   W Z��� m   W X�M
�M boovtrue� o      �L�L 0 filetest fileTest� ��� Z   [ r���K�J� =  [ ^��� o   [ \�I�I 0 filetest fileTest� m   \ ]�H
�H boovfals� I  a n�G��F
�G .sysoexecTEXT���     TEXT� b   a j��� m   a d�� ���  m k d i r  � n   d i��� 1   e i�E
�E 
strq� o   d e�D�D :0 transcodependingfolderposix transcodePendingFolderPosix�F  �K  �J  � ��� l  s s�C�B�A�C  �B  �A  � ��� r   s ���� b   s ���� b   s |��� m   s v�� ���  / U s e r s /� o   v {�@�@ $0 computerusername computerUserName� m   | �� ��� 8 / D o c u m e n t s / m e d i a _ t r a n s c o d e d /� o      �?�? >0 transcodecompletedfolderposix transcodeCompletedFolderPosix� ��� Q   � ����� r   � ���� I   � ��>��=�> 0 folderexists folderExists� ��<� o   � ��;�; >0 transcodecompletedfolderposix transcodeCompletedFolderPosix�<  �=  � o      �:�: 0 filetest fileTest� R      �9��8
�9 .ascrerr ****      � ****� o      �7�7 
0 errmsg  �8  � r   � ���� m   � ��6
�6 boovtrue� o      �5�5 0 filetest fileTest� ��� Z   � ����4�3� =  � �   o   � ��2�2 0 filetest fileTest m   � ��1
�1 boovfals� I  � ��0�/
�0 .sysoexecTEXT���     TEXT b   � � m   � � �  m k d i r   n   � � 1   � ��.
�. 
strq o   � ��-�- >0 transcodecompletedfolderposix transcodeCompletedFolderPosix�/  �4  �3  � 	
	 l  � ��,�+�*�,  �+  �*  
  r   � � b   � � b   � � m   � � �  / U s e r s / o   � ��)�) $0 computerusername computerUserName m   � � � 6 / D o c u m e n t s / w a i v e r s _ p e n d i n g / o      �(�( @0 pendingwaiversforaefolderposix pendingWaiversForAEFolderPosix  Q   � � r   � � I   � ��'�&�' 0 folderexists folderExists �% o   � ��$�$ @0 pendingwaiversforaefolderposix pendingWaiversForAEFolderPosix�%  �&   o      �#�# 0 filetest fileTest R      �" �!
�" .ascrerr ****      � ****  o      � �  
0 errmsg  �!   r   � �!"! m   � ��
� boovtrue" o      �� 0 filetest fileTest #$# Z   � �%&��% =  � �'(' o   � ��� 0 filetest fileTest( m   � ��
� boovfals& I  � ��)�
� .sysoexecTEXT���     TEXT) b   � �*+* m   � �,, �--  m k d i r  + n   � �./. 1   � ��
� 
strq/ o   � ��� @0 pendingwaiversforaefolderposix pendingWaiversForAEFolderPosix�  �  �  $ 010 l  � �����  �  �  1 232 r   �454 b   �676 o   � �� @0 pendingwaiversforaefolderposix pendingWaiversForAEFolderPosix7 m   88 �99 $ c o m p l e t e d _ a r c h i v e /5 o      �� %0 !waivers_pending_completed_archive  3 :;: Q  	!<=>< r  ?@? I  �A�� 0 folderexists folderExistsA B�B o  �� %0 !waivers_pending_completed_archive  �  �  @ o      �� 0 filetest fileTest= R      �C�

� .ascrerr ****      � ****C o      �	�	 
0 errmsg  �
  > r  !DED m  �
� boovtrueE o      �� 0 filetest fileTest; FGF Z  ";HI��H = "%JKJ o  "#�� 0 filetest fileTestK m  #$�
� boovfalsI I (7�L�
� .sysoexecTEXT���     TEXTL b  (3MNM m  (+OO �PP  m k d i r  N n  +2QRQ 1  .2� 
�  
strqR o  +.���� %0 !waivers_pending_completed_archive  �  �  �  G STS l <<��������  ��  ��  T UVU r  <MWXW b  <IYZY b  <E[\[ m  <?]] �^^  / U s e r s /\ o  ?D���� $0 computerusername computerUserNameZ m  EH__ �`` , / D o c u m e n t s / m e d i a _ p o o l /X o      ���� 0 
media_pool  V aba Q  Nfcdec r  Q[fgf I  QY��h���� 0 folderexists folderExistsh i��i o  RU���� 0 
media_pool  ��  ��  g o      ���� 0 filetest fileTestd R      ������
�� .ascrerr ****      � ****��  ��  e r  cfjkj m  cd��
�� boovtruek o      ���� 0 filetest fileTestb lml Z  g�no����n = gjpqp o  gh���� 0 filetest fileTestq m  hi��
�� boovfalso I m|��r��
�� .sysoexecTEXT���     TEXTr b  mxsts m  mpuu �vv  m k d i r  t n  pwwxw 1  sw��
�� 
strqx o  ps���� 0 
media_pool  ��  ��  ��  m yzy l ����������  ��  ��  z {|{ r  ��}~} b  ��� b  ����� m  ���� ���  / U s e r s /� o  ������ $0 computerusername computerUserName� m  ���� ��� 8 / D o c u m e n t s / a m e _ w a t c h / O u t p u t /~ o      ���� 0 processed_media  | ��� Q  ������ r  ����� I  ��������� 0 folderexists folderExists� ���� o  ������ 0 processed_media  ��  ��  � o      ���� 0 filetest fileTest� R      ������
�� .ascrerr ****      � ****��  ��  � r  ����� m  ����
�� boovtrue� o      ���� 0 filetest fileTest� ��� Z  ��������� = ����� o  ������ 0 filetest fileTest� m  ����
�� boovfals� I �������
�� .sysoexecTEXT���     TEXT� b  ����� m  ���� ���  m k d i r  � n  ����� 1  ����
�� 
strq� o  ������ 0 processed_media  ��  ��  ��  � ��� l ����������  ��  ��  � ��� r  ����� b  ����� b  ����� m  ���� ���  / U s e r s /� o  ������ $0 computerusername computerUserName� m  ���� ��� * / D o c u m e n t s / a e _ o u t p u t /� o      ���� 0 	ae_output  � ��� Q  ������ r  ����� I  ��������� 0 folderexists folderExists� ���� o  ������ 0 	ae_output  ��  ��  � o      ���� 0 filetest fileTest� R      ������
�� .ascrerr ****      � ****��  ��  � r  ����� m  ����
�� boovtrue� o      ���� 0 filetest fileTest� ��� Z  �
������� = ����� o  ���� 0 filetest fileTest� m  ���
� boovfals� I ����
� .sysoexecTEXT���     TEXT� b  ���� m  ���� ���  m k d i r  � n  ���� 1  ��
� 
strq� o  ���� 0 	ae_output  �  ��  ��  � ��� l ����  �  �  � ��� r  ��� b  ��� b  ��� m  �� ���  / U s e r s /� o  �� $0 computerusername computerUserName� m  �� ��� 6 / D o c u m e n t s / a e _ i m a g e _ o u t p u t /� o      �� 0 processed_images  � ��� Q  5���� r   *��� I   (���� 0 folderexists folderExists� ��� o  !$�� 0 processed_images  �  �  � o      �� 0 filetest fileTest� R      ���
� .ascrerr ****      � ****�  �  � r  25��� m  23�
� boovtrue� o      �� 0 filetest fileTest� ��� Z  6O����� = 69��� o  67�� 0 filetest fileTest� m  78�
� boovfals� I <K���
� .sysoexecTEXT���     TEXT� b  <G��� m  <?�� ���  m k d i r  � n  ?F��� 1  BF�
� 
strq� o  ?B�� 0 processed_images  �  �  �  � ��� l PP����  �  �  � ��� r  Pa��� b  P]��� b  PY��� m  PS�� ���  / U s e r s /� o  SX�� $0 computerusername computerUserName� m  Y\�� ��� * / D o c u m e n t s / a m e _ w a t c h /� o      �� 0 	ame_watch  � ��� Q  bz���� r  eo��� I  em� �� 0 folderexists folderExists  � o  fi�� 0 	ame_watch  �  �  � o      �� 0 filetest fileTest� R      ���
� .ascrerr ****      � ****�  �  � r  wz m  wx�
� boovtrue o      �� 0 filetest fileTest�  Z  {��� = {~	 o  {|�� 0 filetest fileTest	 m  |}�
� boovfals I ���
�
� .sysoexecTEXT���     TEXT
 b  �� m  �� �  m k d i r   n  �� 1  ���
� 
strq o  ���� 0 	ame_watch  �  �  �    l ������  �  �    r  �� b  �� o  ���� "0 dropboxrootpath dropboxRootPath m  �� �  / i n g e s t o r / o      �� 0 spock_ingestor    Q  �� r  �� !  I  ���"�� 0 folderexists folderExists" #�# o  ���� 0 spock_ingestor  �  �  ! o      �� 0 filetest fileTest R      ���
� .ascrerr ****      � ****�  �   r  ��$%$ m  ���~
�~ boovtrue% o      �}�} 0 filetest fileTest &'& Z  ��()�|�{( = ��*+* o  ���z�z 0 filetest fileTest+ m  ���y
�y boovfals) I ���x,�w
�x .sysoexecTEXT���     TEXT, b  ��-.- m  ��// �00  m k d i r  . n  ��121 1  ���v
�v 
strq2 o  ���u�u 0 spock_ingestor  �w  �|  �{  ' 343 l ���t�s�r�t  �s  �r  4 5�q5 L  ��66 m  ���p
�p boovtrue�q  � R      �o7�n
�o .ascrerr ****      � ****7 o      �m�m 
0 errmsg  �n  � L  ��88 o  ���l�l 
0 errmsg  � 9�k9 l ���j�i�h�j  �i  �h  �k  ~ :;: l     �g�f�e�g  �f  �e  ; <=< l     �d�c�b�d  �c  �b  = >?> l     �a@A�a  @ C =#############################################################   A �BB z # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #? CDC l     �`EF�`  E % ## --HELPER & UTILITY METHODS--   F �GG > # #   - - H E L P E R   &   U T I L I T Y   M E T H O D S - -D HIH l     �_JK�_  J % ## --HELPER & UTILITY METHODS--   K �LL > # #   - - H E L P E R   &   U T I L I T Y   M E T H O D S - -I MNM l     �^OP�^  O C =#############################################################   P �QQ z # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #N RSR l     �]�\�[�]  �\  �[  S TUT i   � �VWV I      �ZX�Y�Z 0 read_waiver  X Y�XY o      �W�W 0 	thewaiver  �X  �Y  W k     �ZZ [\[ l     �V]^�V  ]  get waiver details   ^ �__ $ g e t   w a i v e r   d e t a i l s\ `a` r     
bcb I     �Ud�T�U 0 get_element  d efe o    �S�S 0 	thewaiver  f ghg m    ii �jj  J o bh k�Rk m    ll �mm  i d�R  �T  c o      �Q�Q 0 	waiver_id  a non r    pqp I    �Pr�O�P 0 get_element  r sts o    �N�N 0 	thewaiver  t uvu m    ww �xx  J o bv y�My m    zz �{{  c r e a t e d�M  �O  q o      �L�L 0 waiver_created  o |}| l   �K�J�I�K  �J  �I  } ~~ r     ��� I    �H��G�H 0 get_element  � ��� o    �F�F 0 	thewaiver  � ��� m    �� ���  J o b� ��E� m    �� ���  p r o g r a m�E  �G  � o      �D�D 0 waiver_program   ��� r   ! +��� I   ! )�C��B�C 0 get_element  � ��� o   " #�A�A 0 	thewaiver  � ��� m   # $�� ���  J o b� ��@� m   $ %�� ���  a c t i v a t i o n�@  �B  � o      �?�? 0 waiver_activation  � ��� r   , 6��� I   , 4�>��=�> 0 get_element  � ��� o   - .�<�< 0 	thewaiver  � ��� m   . /�� ���  J o b� ��;� m   / 0�� ���  t e a m�;  �=  � o      �:�: 0 waiver_team  � ��� r   7 A��� I   7 ?�9��8�9 0 get_element  � ��� o   8 9�7�7 0 	thewaiver  � ��� m   9 :�� ���  J o b� ��6� m   : ;�� ��� 
 e v e n t�6  �8  � o      �5�5 0 waiver_event  � ��� Q   B Z���� r   E O��� I   E M�4��3�4 0 get_element  � ��� o   F G�2�2 0 	thewaiver  � ��� m   G H�� ���  J o b� ��1� m   H I�� ���  t o t a l _ r a w _ f i l e s�1  �3  � o      �0�0 0 waiver_total_raw_files  � R      �/�.�-
�/ .ascrerr ****      � ****�.  �-  � r   W Z��� m   W X�,�, � o      �+�+ 0 waiver_total_raw_files  � ��� l  [ [�*�)�(�*  �)  �(  � ��� l  [ [�'�&�%�'  �&  �%  � ��� r   [ }��� K   [ {�� �$���$ 0 	waiver_id  � o   ^ _�#�# 0 	waiver_id  � �"���" 0 waiver_created  � o   b c�!�! 0 waiver_created  � � ���  0 waiver_program  � o   f g�� 0 waiver_program  � ���� 0 waiver_activation  � o   j k�� 0 waiver_activation  � ���� 0 waiver_team  � o   n o�� 0 waiver_team  � ���� 0 waiver_event  � o   r s�� 0 waiver_event  � ���� 0 waiver_total_raw_files  � o   v w�� 0 waiver_total_raw_files  �  � o      �� 0 waiver_data  � ��� l  ~ ~����  �  �  � ��� l  ~ ~����  �  �  � ��� L   ~ ��� o   ~ �� 0 waiver_data  � ��� l  � ����
�  �  �
  �  U ��� l     �	���	  �  �  � ��� i   � ���� I      ���� 0 get_element  � ��� o      �� 0 	thewaiver  � ��� o      �� 0 node  � ��� o      �� 0 element_name  �  �  � Q     =���� O    ��� k    �� ��� l   � ���   �  get top level   � ���  g e t   t o p   l e v e l� ��� r       n     5    ����
�� 
xmle o    ���� 0 node  
�� kfrmname n     1    ��
�� 
pcnt 4    ��
�� 
xmlf o   	 
���� 0 	thewaiver   o      ���� 0 xmldata  � �� r    	
	 n     1    ��
�� 
valL n     5    ����
�� 
xmle o    ���� 0 element_name  
�� kfrmname o    ���� 0 xmldata  
 o      ��
�� 
ret ��  � m    �                                                                                  sevs  alis    \  Macintosh HD                   BD ����System Events.app                                              ����            ����  
 cu             CoreServices  0/:System:Library:CoreServices:System Events.app/  $  S y s t e m   E v e n t s . a p p    M a c i n t o s h   H D  -System/Library/CoreServices/System Events.app   / ��  � R      ����
�� .ascrerr ****      � **** o      ���� 
0 errmsg  ��  � k   & =  Z   & 9���� =  & - o   & +���� 0 dev_mode   m   + ,��
�� boovtrue I  0 5����
�� .sysodisAaleR        TEXT o   0 1���� 
0 errmsg  ��  ��  ��   �� r   : = m   : ; �   o      ��
�� 
ret ��  �   l     ��������  ��  ��    !"! l     ��������  ��  ��  " #$# i   � �%&% I      ��'���� 0 
fileexists 
fileExists' (��( o      ���� 0 thefile theFile��  ��  & l    )*+) O     ,-, Z    ./��0. I   ��1��
�� .coredoexnull���     ****1 4    ��2
�� 
file2 o    ���� 0 thefile theFile��  / L    33 m    ��
�� boovtrue��  0 L    44 m    ��
�� boovfals- m     55�                                                                                  sevs  alis    \  Macintosh HD                   BD ����System Events.app                                              ����            ����  
 cu             CoreServices  0/:System:Library:CoreServices:System Events.app/  $  S y s t e m   E v e n t s . a p p    M a c i n t o s h   H D  -System/Library/CoreServices/System Events.app   / ��  *   (String) as Boolean   + �66 (   ( S t r i n g )   a s   B o o l e a n$ 787 l     ��������  ��  ��  8 9:9 i   � �;<; I      ��=���� 0 folderexists folderExists= >��> o      ���� 0 thefile theFile��  ��  < l    ?@A? O     BCB Z    DE��FD I   ��G��
�� .coredoexnull���     ****G 4    ��H
�� 
cfolH o    ���� 0 thefile theFile��  E L    II m    ��
�� boovtrue��  F L    JJ m    ��
�� boovfalsC m     KK�                                                                                  sevs  alis    \  Macintosh HD                   BD ����System Events.app                                              ����            ����  
 cu             CoreServices  0/:System:Library:CoreServices:System Events.app/  $  S y s t e m   E v e n t s . a p p    M a c i n t o s h   H D  -System/Library/CoreServices/System Events.app   / ��  @   (String) as Boolean   A �LL (   ( S t r i n g )   a s   B o o l e a n: MNM l     ��������  ��  ��  N OPO i   � �QRQ I      ��S���� 0 	changeext 	changeExtS TUT o      ���� 0 filename  U V��V o      �� 0 new_ext  ��  ��  R k      WW XYX r     Z[Z \     	\]\ l    ^��^ I    �_�
� .corecnte****       ****_ n     `a` 2   �
� 
cha a o     �� 0 filename  �  �  �  ] m    �� [ o      �� 0 mylength  Y bcb r    ded b    fgf l   h��h c    iji l   k��k n    lml 7  �no
� 
cha n m    �� o o    �� 0 mylength  m o    �� 0 filename  �  �  j m    �
� 
TEXT�  �  g o    �� 0 new_ext  e o      �� 0 	newstring  c p�p L     qq o    �� 0 	newstring  �  P rsr l     ����  �  �  s tut i   � �vwv I      ����  0 getfreediskpct getFreeDiskPct�  �  w k     xx yzy r     {|{ I     �}�� 0 trimline trimLine} ~~ n    ��� o    �� 0 capacity  � I    ���� 0 getdiskstats getDiskStats�  �   ��� m    	�� ���  %� ��� m   	 
�� �  �  | o      �� 0 rawpct rawPctz ��� l   ����  � . ( returns % used, so invert it for % free   � ��� P   r e t u r n s   %   u s e d ,   s o   i n v e r t   i t   f o r   %   f r e e� ��� L    �� \    ��� m    �� d� o    �� 0 rawpct rawPct�  u ��� l     ����  �  �  � ��� i   � ���� I      ���� (0 getfreediskspacegb getFreeDiskSpaceGB�  �  � L     	�� n     ��� o    �� 0 	available  � I     ���� 0 getdiskstats getDiskStats�  �  � ��� l     ����  �  �  � ��� i   � ���� I      ���� 0 getdiskstats getDiskStats�  �  � k     ��� ��� r     ��� n    ��� 1    �
� 
txdl� 1     �
� 
ascr� o      �� 0 old_delimts  � ��� Q    ����� k   	 ��� ��� r   	 ��� I  	 ���
� .sysoexecTEXT���     TEXT� m   	 
�� ���  d f   - h l g�  � o      �� 0 memory_stats  � ��� r    ��� n    ��� 4    ��
� 
cpar� m    �~�~ � o    �}�} 0 memory_stats  � o      �|�| 0 	rawstring  � ��� l   �{���{  � ( " set freeDisk to item 1 of theList   � ��� D   s e t   f r e e D i s k   t o   i t e m   1   o f   t h e L i s t� ��� l   �z���z  � , & set diskCapacity to item 1 of theList   � ��� L   s e t   d i s k C a p a c i t y   t o   i t e m   1   o f   t h e L i s t� ��� l   �y���y  � 2 , set diskFreePercentage to item 2 of theList   � ��� X   s e t   d i s k F r e e P e r c e n t a g e   t o   i t e m   2   o f   t h e L i s t� ��� r    ��� m    �� ���   � n     ��� 1    �x
�x 
txdl� 1    �w
�w 
ascr� ��� r    #��� n    !��� 2   !�v
�v 
citm� o    �u�u 0 	rawstring  � o      �t�t 0 thelist theList� ��� r   $ )��� o   $ %�s�s 0 old_delimts  � n     ��� 1   & (�r
�r 
txdl� 1   % &�q
�q 
ascr� ��� l  * *�p�o�n�p  �o  �n  � ��� r   * .��� J   * ,�m�m  � o      �l�l 0 newlist newList� ��� X   / X��k�� k   ? S�� ��� l  ? ?�j���j  �  display dialog i   � ���   d i s p l a y   d i a l o g   i� ��� Z   ? Q���i�h� >  ? D��� l  ? B��g�f� n   ? B��� m   @ B�e
�e 
ctxt� o   ? @�d�d 0 i  �g  �f  � m   B C�� ���  � s   G M��� l  G J��c�b� n   G J��� m   H J�a
�a 
ctxt� o   G H�`�` 0 i  �c  �b  � l     ��_�^� n      ���  ;   K L� o   J K�]�] 0 newlist newList�_  �^  �i  �h  � ��\� l  R R�[�Z�Y�[  �Z  �Y  �\  �k 0 i  � o   2 3�X�X 0 thelist theList�    l  Y Y�W�V�U�W  �V  �U   �T r   Y � K   Y � �S�S 0 diskname   l  Z `�R�Q n   Z `	
	 m   ^ `�P
�P 
ctxt
 n   Z ^ 4   [ ^�O
�O 
cobj m   \ ]�N�N  o   Z [�M�M 0 newlist newList�R  �Q   �L�L 0 
onegblocks 
OneGblocks l  a g�K�J n   a g m   e g�I
�I 
ctxt n   a e 4   b e�H
�H 
cobj m   c d�G�G  o   a b�F�F 0 newlist newList�K  �J   �E�E 0 used   l  h n�D�C n   h n m   l n�B
�B 
ctxt n   h l 4   i l�A
�A 
cobj m   j k�@�@  o   h i�?�? 0 newlist newList�D  �C   �>�> 0 	available   l  o w �=�<  n   o w!"! m   u w�;
�; 
ctxt" n   o u#$# 4   p u�:%
�: 
cobj% m   q t�9�9 $ o   o p�8�8 0 newlist newList�=  �<   �7&�6�7 0 capacity  & l  z �'�5�4' n   z �()( m   � ��3
�3 
ctxt) n   z �*+* 4   { ��2,
�2 
cobj, m   | �1�1 + o   z {�0�0 0 newlist newList�5  �4  �6   o      �/�/ 0 	finallist 	finalList�T  � R      �.-�-
�. .ascrerr ****      � ****- o      �,�, 
0 errmsg  �-  �  � ./. l  � ��+�*�)�+  �*  �)  / 010 r   � �232 o   � ��(�( 0 old_delimts  3 n     454 1   � ��'
�' 
txdl5 1   � ��&
�& 
ascr1 676 l  � ��%�$�#�%  �$  �#  7 898 L   � �:: o   � ��"�" 0 	finallist 	finalList9 ;�!; l  � �� ���   �  �  �!  � <=< l     ����  �  �  = >?> i   � �@A@ I      �B�� 0 
is_running  B C�C o      �� 0 appname appName�  �  A k     :DD EFE O     GHG r    IJI l   	K��K n    	LML 1    	�
� 
pnamM 2    �
� 
prcs�  �  J o      �� "0 listofprocesses listOfProcessesH m     NN�                                                                                  sevs  alis    \  Macintosh HD                   BD ����System Events.app                                              ����            ����  
 cu             CoreServices  0/:System:Library:CoreServices:System Events.app/  $  S y s t e m   E v e n t s . a p p    M a c i n t o s h   H D  -System/Library/CoreServices/System Events.app   / ��  F OPO l   ����  �  �  P QRQ r    STS m    �
� boovfalsT o      �� 0 appisrunning appIsRunningR UVU X    5W�XW Z   ! 0YZ��
Y E   ! $[\[ o   ! "�	�	 0 thisitem thisItem\ o   " #�� 0 appname appNameZ k   ' ,]] ^_^ r   ' *`a` m   ' (�
� boovtruea o      �� 0 appisrunning appIsRunning_ b�b  S   + ,�  �  �
  � 0 thisitem thisItemX o    �� "0 listofprocesses listOfProcessesV cdc L   6 8ee o   6 7�� 0 appisrunning appIsRunningd f�f l  9 9�� ���  �   ��  �  ? ghg l     ��������  ��  ��  h iji i   � �klk I      ��m���� 0 	readplist 	readPlistm non o      ���� 0 theplistpath thePListPatho pqp o      ���� 0 plistvar plistVarq r��r o      ���� 0 defaultvalue defaultValue��  ��  l Q     2stus k    vv wxw r    
yzy b    {|{ b    }~} m     ���    - c   ' p r i n t  ~ o    ���� 0 plistvar plistVar| m    �� ���  'z o      ���� 0 	mycommand 	myCommandx ��� r    ��� b    ��� b    ��� m    �� ��� 2 / u s r / l i b e x e c / P l i s t B u d d y    � o    ���� 0 theplistpath thePListPath� o    ���� 0 	mycommand 	myCommand� o      ���� 0 myscript myScript� ���� I   �����
�� .sysoexecTEXT���     TEXT� o    ���� 0 myscript myScript��  ��  t R      �����
�� .ascrerr ****      � ****� o      ���� 
0 errmsg  ��  u k     2�� ��� Z     /������� =    #��� o     !���� 0 defaultvalue defaultValue� m   ! "��
�� 
null� I  & +�����
�� .sysodisAaleR        TEXT� o   & '���� 
0 errmsg  ��  ��  ��  � ���� L   0 2�� o   0 1���� 0 defaultvalue defaultValue��  j ��� l     ��������  ��  ��  � ��� i   � ���� I      ������� 0 
writeplist 
writePlist� ��� o      ���� 0 theplistpath thePListPath� ��� o      ���� 0 plistvar plistVar� ���� o      ���� 0 thevalue theValue��  ��  � Q     #���� k    �� ��� r    ��� b    ��� b    
��� b    ��� b    ��� m    �� ���    - c   ' s e t  � o    ���� 0 plistvar plistVar� m    �� ���   � o    	���� 0 thevalue theValue� m   
 �� ���  '� o      ���� 0 	mycommand 	myCommand� ���� r    ��� b    ��� b    ��� m    �� ��� 2 / u s r / l i b e x e c / P l i s t B u d d y    � o    ���� 0 theplistpath thePListPath� o    ���� 0 	mycommand 	myCommand� o      ���� 0 myscript myScript��  � R      �����
�� .ascrerr ****      � ****� o      ���� 
0 errmsg  ��  � I   #�����
�� .sysodisAaleR        TEXT� o    ���� 
0 errmsg  ��  � ��� l     ��������  ��  ��  � ��� l     ��������  ��  ��  � ��� i   � ���� I      �������� 0 getepochtime getEpochTime��  ��  � k     �� ��� r     	��� c     ��� l    ������ I    ���
� .sysoexecTEXT���     TEXT� m     �� ��� \ p e r l   - e   ' u s e   T i m e : : H i R e s   q w ( t i m e ) ;   p r i n t   t i m e '�  ��  ��  � m    �
� 
TEXT� o      �� 
0 mytime  � ��� r   
 ��� ]   
 ��� o   
 �� 
0 mytime  � m    ��  ��� o      �� 
0 mytime  � ��� L    �� I    ���� 0 number_to_string  � ��� o    �� 
0 mytime  �  �  �  � ��� l     ����  �  �  � ��� i   � ���� I      ���� 0 number_to_string  � ��� o      �� 0 this_number  �  �  � k     ��� ��� r     ��� c     ��� o     �� 0 this_number  � m    �
� 
TEXT� o      �� 0 this_number  � ��� Z    ������ E    	��� o    �� 0 this_number  � m    �� ���  E +� k    ��� ��� r    ��� l    ��  I   ��
� .sysooffslong    ��� null�   �
� 
psof m     �  . ��
� 
psin o    �� 0 this_number  �  �  �  � o      �� 0 x  �  r    #	
	 l   !�� I   !��
� .sysooffslong    ��� null�   �
� 
psof m     �  + ��
� 
psin o    �� 0 this_number  �  �  �  
 o      �� 0 y    r   $ / l  $ -�� I  $ -��
� .sysooffslong    ��� null�   �
� 
psof m   & ' �  E ����
�� 
psin o   ( )���� 0 this_number  ��  �  �   o      ���� 0 z    r   0 E  c   0 C!"! c   0 A#$# n   0 ?%&% 7  1 ?��'(
�� 
cha ' l  5 ;)����) \   5 ;*+* o   6 7���� 0 y  + l  7 :,����, n   7 :-.- 1   8 :��
�� 
leng. o   7 8���� 0 this_number  ��  ��  ��  ��  ( l 	 < >/����/ m   < >��������  ��  & o   0 1���� 0 this_number  $ m   ? @�
� 
TEXT" m   A B�~
�~ 
nmbr  l     0�}�|0 o      �{�{ 0 decimal_adjust  �}  �|   121 Z   F c34�z53 >  F I676 o   F G�y�y 0 x  7 m   G H�x�x  4 r   L ]898 c   L [:;: n   L Y<=< 7  M Y�w>?
�w 
cha > m   Q S�v�v ? l  T X@�u�t@ \   T XABA o   U V�s�s 0 x  B m   V W�r�r �u  �t  = o   L M�q�q 0 this_number  ; m   Y Z�p
�p 
TEXT9 l     C�o�nC o      �m�m 0 
first_part  �o  �n  �z  5 r   ` cDED m   ` aFF �GG  E l     H�l�kH o      �j�j 0 
first_part  �l  �k  2 IJI r   d wKLK c   d uMNM n   d sOPO 7  e s�iQR
�i 
cha Q l  i mS�h�gS [   i mTUT o   j k�f�f 0 x  U m   k l�e�e �h  �g  R l  n rV�d�cV \   n rWXW o   o p�b�b 0 z  X m   p q�a�a �d  �c  P o   d e�`�` 0 this_number  N m   s t�_
�_ 
TEXTL l     Y�^�]Y o      �\�\ 0 second_part  �^  �]  J Z[Z r   x {\]\ l  x y^�[�Z^ o   x y�Y�Y 0 
first_part  �[  �Z  ] l     _�X�W_ o      �V�V 0 converted_number  �X  �W  [ `a` Y   | �b�Ucd�Tb Q   � �efge r   � �hih b   � �jkj l 	 � �l�S�Rl l  � �m�Q�Pm o   � ��O�O 0 converted_number  �Q  �P  �S  �R  k n   � �non 4   � ��Np
�N 
cha p o   � ��M�M 0 i  o l  � �q�L�Kq o   � ��J�J 0 second_part  �L  �K  i l     r�I�Hr o      �G�G 0 converted_number  �I  �H  f R      �F�E�D
�F .ascrerr ****      � ****�E  �D  g r   � �sts b   � �uvu l  � �w�C�Bw o   � ��A�A 0 converted_number  �C  �B  v m   � �xx �yy  0t l     z�@�?z o      �>�> 0 converted_number  �@  �?  �U 0 i  c m    ��=�= d l  � �{�<�;{ o   � ��:�: 0 decimal_adjust  �<  �;  �T  a |�9| L   � �}} l  � �~�8�7~ o   � ��6�6 0 converted_number  �8  �7  �9  �  � L   � � o   � ��5�5 0 this_number  �  � ��� l     �4�3�2�4  �3  �2  � ��� i   � ���� I      �1��0�1 0 add_leading_zeros  � ��� o      �/�/ 0 this_number  � ��.� o      �-�- 0 max_leading_zeros  �.  �0  � k     G�� ��� r     ��� c     ��� l    ��,�+� a     ��� m     �*�* 
� o    �)�) 0 max_leading_zeros  �,  �+  � m    �(
�( 
long� l     ��'�&� o      �%�% 0 threshold_number  �'  �&  � ��$� Z    G���#�� A   ��� o    	�"�" 0 this_number  � l  	 
��!� � o   	 
�� 0 threshold_number  �!  �   � k    @�� ��� r    ��� m    �� ���  � l     ���� o      �� 0 leading_zeros  �  �  � ��� r    ��� l   ���� n    ��� 1    �
� 
leng� l   ���� c    ��� l   ���� _    ��� o    �� 0 this_number  � m    �� �  �  � m    �
� 
TEXT�  �  �  �  � l     ���� o      �� 0 digit_count  �  �  � ��� r    #��� \    !��� l   ���� [    ��� o    �� 0 max_leading_zeros  � m    �� �  �  � o     �
�
 0 digit_count  � l     ��	�� o      �� 0 character_count  �	  �  � ��� U   $ 7��� r   + 2��� c   + 0��� l  + .���� b   + .��� l  + ,���� o   + ,�� 0 leading_zeros  �  �  � m   , -�� ���  0�  �  � m   . /�
� 
TEXT� l     �� ��� o      ���� 0 leading_zeros  �   ��  � o   ' (���� 0 character_count  � ���� L   8 @�� c   8 ?��� l  8 =������ b   8 =��� o   8 9���� 0 leading_zeros  � l  9 <������ c   9 <��� o   9 :���� 0 this_number  � m   : ;��
�� 
ctxt��  ��  ��  ��  � m   = >��
�� 
TEXT��  �#  � L   C G�� c   C F��� o   C D���� 0 this_number  � m   D E��
�� 
ctxt�$  � ��� l     ��������  ��  ��  � ��� i   � ���� I      ������� 00 roundandtruncatenumber roundAndTruncateNumber� ��� o      ���� 0 	thenumber 	theNumber� ���� o      ���� .0 numberofdecimalplaces numberOfDecimalPlaces��  ��  � k     ��� ��� Z     ������� =    ��� o     ���� .0 numberofdecimalplaces numberOfDecimalPlaces� m    ����  � k    �� ��� r    ��� [    	��� o    ���� 0 	thenumber 	theNumber� m    �� ?�      � o      ���� 0 	thenumber 	theNumber� ���� L    �� I    ������� 0 number_to_string  � ���� _    ��� o    ���� 0 	thenumber 	theNumber� m    ���� ��  ��  ��  ��  ��  � ��� l   ��������  ��  ��  � ��� r    ��� m    �� ���  5� o      ���� $0 theroundingvalue theRoundingValue� � � U    / r   % * b   % ( m   % & �  0 o   & '���� $0 theroundingvalue theRoundingValue o      ���� $0 theroundingvalue theRoundingValue o   ! "���� .0 numberofdecimalplaces numberOfDecimalPlaces  	
	 r   0 7 c   0 5 l  0 3���� b   0 3 m   0 1 �  . o   1 2���� $0 theroundingvalue theRoundingValue��  ��   m   3 4��
�� 
nmbr o      ���� $0 theroundingvalue theRoundingValue
  l  8 8��������  ��  ��    r   8 = [   8 ; o   8 9���� 0 	thenumber 	theNumber o   9 :���� $0 theroundingvalue theRoundingValue o      ���� 0 	thenumber 	theNumber  l  > >��������  ��  ��    r   > A !  m   > ?"" �##  1! o      ���� 0 themodvalue theModValue $%$ U   B U&'& r   K P()( b   K N*+* m   K L,, �--  0+ o   L M���� 0 themodvalue theModValue) o      ���� 0 themodvalue theModValue' \   E H./. o   E F���� .0 numberofdecimalplaces numberOfDecimalPlaces/ m   F G���� % 010 r   V ]232 c   V [454 l  V Y6����6 b   V Y787 m   V W99 �::  .8 o   W X���� 0 themodvalue theModValue��  ��  5 m   Y Z��
�� 
nmbr3 o      �� 0 themodvalue theModValue1 ;<; l  ^ ^����  �  �  < =>= r   ^ e?@? _   ^ cABA l  ^ aC��C `   ^ aDED o   ^ _�� 0 	thenumber 	theNumberE m   _ `�� �  �  B o   a b�� 0 themodvalue theModValue@ o      �� 0 thesecondpart theSecondPart> FGF Z   f �HI��H A  f mJKJ n   f kLML 1   i k�
� 
lengM l  f iN��N c   f iOPO o   f g�� 0 thesecondpart theSecondPartP m   g h�
� 
ctxt�  �  K o   k l�� .0 numberofdecimalplaces numberOfDecimalPlacesI U   p �QRQ r   } �STS c   } �UVU l  } �W��W b   } �XYX m   } ~ZZ �[[  0Y o   ~ �� 0 thesecondpart theSecondPart�  �  V m   � ��
� 
TEXTT o      �� 0 thesecondpart theSecondPartR \   s z\]\ o   s t�� .0 numberofdecimalplaces numberOfDecimalPlaces] l  t y^��^ l  t y_��_ n   t y`a` 1   w y�
� 
lenga l  t wb��b c   t wcdc o   t u�� 0 thesecondpart theSecondPartd m   u v�
� 
ctxt�  �  �  �  �  �  �  �  G efe l  � �����  �  �  f ghg r   � �iji _   � �klk o   � ��� 0 	thenumber 	theNumberl m   � ��� j o      �� 0 thefirstpart theFirstParth mnm r   � �opo I   � ��q�� 0 number_to_string  q r�r o   � ��� 0 thefirstpart theFirstPart�  �  p o      �� 0 thefirstpart theFirstPartn sts r   � �uvu l  � �w��w b   � �xyx b   � �z{z o   � ��� 0 thefirstpart theFirstPart{ m   � �|| �}}  .y o   � ��� 0 thesecondpart theSecondPart�  �  v o      �� 0 	thenumber 	theNumbert ~~ l  � �����  �  �   ��� L   � ��� o   � ��� 0 	thenumber 	theNumber�  � ��� l     ����  �  �  � ��� i   � ���� I      ���� 0 trimline trimLine� ��� o      �� 0 	this_text  � ��� o      �� 0 
trim_chars  � ��� o      �� 0 trim_indicator  �  �  � k     {�� ��� l     ����  � ' ! 0 = beginning, 1 = end, 2 = both   � ��� B   0   =   b e g i n n i n g ,   1   =   e n d ,   2   =   b o t h� ��� r     ��� l    ���~� n     ��� 1    �}
�} 
leng� l    ��|�{� o     �z�z 0 
trim_chars  �|  �{  �  �~  � o      �y�y 0 x  � ��� l   �x���x  �   TRIM BEGINNING   � ���    T R I M   B E G I N N I N G� ��� Z    >���w�v� E   ��� J    
�� ��� m    �u�u  � ��t� m    �s�s �t  � l  
 ��r�q� o   
 �p�p 0 trim_indicator  �r  �q  � V    :��� Q    5���� r    +��� c    )��� n    '��� 7   '�o��
�o 
cha � l   #��n�m� [    #��� o     !�l�l 0 x  � m   ! "�k�k �n  �m  � m   $ &�j�j��� o    �i�i 0 	this_text  � m   ' (�h
�h 
TEXT� o      �g�g 0 	this_text  � R      �f�e�d
�f .ascrerr ****      � ****�e  �d  � k   3 5�� ��� l  3 3�c���c  � 8 2 the text contains nothing but the trim characters   � ��� d   t h e   t e x t   c o n t a i n s   n o t h i n g   b u t   t h e   t r i m   c h a r a c t e r s� ��b� L   3 5�� m   3 4�� ���  �b  � C   ��� o    �a�a 0 	this_text  � l   ��`�_� o    �^�^ 0 
trim_chars  �`  �_  �w  �v  � ��� l  ? ?�]���]  �   TRIM ENDING   � ���    T R I M   E N D I N G� ��� Z   ? x���\�[� E  ? E��� J   ? C�� ��� m   ? @�Z�Z � ��Y� m   @ A�X�X �Y  � l  C D��W�V� o   C D�U�U 0 trim_indicator  �W  �V  � V   H t��� Q   P o���� r   S e��� c   S c��� n   S a��� 7  T a�T��
�T 
cha � m   X Z�S�S � d   [ `�� l  \ _��R�Q� [   \ _��� o   \ ]�P�P 0 x  � m   ] ^�O�O �R  �Q  � o   S T�N�N 0 	this_text  � m   a b�M
�M 
TEXT� o      �L�L 0 	this_text  � R      �K�J�I
�K .ascrerr ****      � ****�J  �I  � k   m o�� ��� l  m m�H���H  � 8 2 the text contains nothing but the trim characters   � ��� d   t h e   t e x t   c o n t a i n s   n o t h i n g   b u t   t h e   t r i m   c h a r a c t e r s� ��G� L   m o�� m   m n�� ���  �G  � D   L O��� o   L M�F�F 0 	this_text  � l  M N��E�D� o   M N�C�C 0 
trim_chars  �E  �D  �\  �[  � ��B� L   y {�� o   y z�A�A 0 	this_text  �B  � ��� l     �@�?�>�@  �?  �>  � ��� i   � ���� I      �=��<�= (0 getageofoldestfile getAgeOfOldestFile�    o      �;�; 0 filepath filePath �: o      �9�9 0 fileextension fileExtension�:  �<  � k       r     	 l    �8�7 I     �6	�5�6 0 getoldestfile getOldestFile	 

 o    �4�4 0 filepath filePath �3 o    �2�2 0 fileextension fileExtension�3  �5  �8  �7   o      �1�1 
0 myfile   �0 Z   
 �/ >  
  o   
 �.�. 
0 myfile   m     �   L     I    �-�,�- 0 getageoffile getAgeOfFile �+ b     o    �*�* 0 filepath filePath o    �)�) 
0 myfile  �+  �,  �/   L     m    �(�(  �0  �  l     �'�&�%�'  �&  �%    i   � �  I      �$!�#�$ 0 getageoffile getAgeOfFile! "�"" o      �!�! 
0 myfile  �"  �#    k     "## $%$ l     � &'�   & 8 2 returns age based on modification time in seconds   ' �(( d   r e t u r n s   a g e   b a s e d   o n   m o d i f i c a t i o n   t i m e   i n   s e c o n d s% )*) l     �+,�  + � } set shscript2 to "echo $(($(date +%s) - $(stat -t %s -f %m -- /Users/Ben/documents/waivers_pending/" & getOldestFile & ")))"   , �-- �   s e t   s h s c r i p t 2   t o   " e c h o   $ ( ( $ ( d a t e   + % s )   -   $ ( s t a t   - t   % s   - f   % m   - -   / U s e r s / B e n / d o c u m e n t s / w a i v e r s _ p e n d i n g / "   &   g e t O l d e s t F i l e   &   " ) ) ) "* ./. r     010 b     232 b     454 m     66 �77 X e c h o   $ ( ( $ ( d a t e   + % s )   -   $ ( s t a t   - t   % s   - f   % m   - -  5 o    �� 
0 myfile  3 m    88 �99  ) ) )1 o      �� 0 shscript  / :;: Q    <=>< r    ?@? c    ABA l   C��C I   �D�
� .sysoexecTEXT���     TEXTD o    �� 0 shscript  �  �  �  B m    �
� 
long@ o      ��  0 fileageseconds fileAgeSeconds= R      �E�
� .ascrerr ****      � ****E o      �� 
0 errmsg  �  > r    FGF m    �
� 
nullG o      ��  0 fileageseconds fileAgeSeconds; H�H L     "II o     !��  0 fileageseconds fileAgeSeconds�   JKJ l     ����  �  �  K LML i   � �NON I      �P�
� 0 getoldestfile getOldestFileP QRQ o      �	�	 0 filepath filePathR S�S o      �� 0 fileextension fileExtension�  �
  O k     &TT UVU l     �WX�  W c ] set shscript to "ls -tr /Users/Ben/documents/waivers_pending/ | grep '-e *.xml' | head -n 1"   X �YY �   s e t   s h s c r i p t   t o   " l s   - t r   / U s e r s / B e n / d o c u m e n t s / w a i v e r s _ p e n d i n g /   |   g r e p   ' - e   * . x m l '   |   h e a d   - n   1 "V Z[Z r     \]\ b     ^_^ b     	`a` b     bcb b     ded m     ff �gg  l s   - t r  e n    hih 1    �
� 
strqi o    �� 0 filepath filePathc m    jj �kk    |   g r e p   ' - e   *a o    �� 0 fileextension fileExtension_ m   	 
ll �mm  '   |   h e a d   - n   1] o      �� 0 shscript  [ non Q    #pqrp r    sts I   �u� 
� .sysoexecTEXT���     TEXTu o    ���� 0 shscript  �   t o      ���� 0 
oldestfile 
oldestFileq R      ��v��
�� .ascrerr ****      � ****v o      ���� 
0 errmsg  ��  r r     #wxw m     !yy �zz  x o      ���� 0 
oldestfile 
oldestFileo {��{ L   $ &|| o   $ %���� 0 
oldestfile 
oldestFile��  M }~} l     ��������  ��  ��  ~ � i   ���� I      ������� 60 launchaftereffectsproject launchAfterEffectsProject� ���� o      ���� $0 projectfileposix projectFilePOSIX��  ��  � k     �� ��� l     ������  � ) # open file with default application   � ��� F   o p e n   f i l e   w i t h   d e f a u l t   a p p l i c a t i o n� ��� l     ��������  ��  ��  � ��� r     ��� b     ��� m     �� ��� 
 o p e n  � n    ��� 1    ��
�� 
strq� o    ���� $0 projectfileposix projectFilePOSIX� o      ���� 0 shscript  � ��� Q    ���� I   �����
�� .sysoexecTEXT���     TEXT� o    ���� 0 shscript  ��  � R      �����
�� .ascrerr ****      � ****� o      ���� 
0 errmsg  ��  � l   ������  �  
 something   � ���    s o m e t h i n g� ���� l   ��������  ��  ��  ��  � ��� l     ��������  ��  ��  � ��� i  ��� I      �������� 00 archiveaftereffectslog archiveAfterEffectsLog��  ��  � k     c�� ��� r     ��� I     �������� 0 getepochtime getEpochTime��  ��  � o      ���� 0 	timestamp  � ��� l   ������  � 1 +TODO improve to work on different user path   � ��� V T O D O   i m p r o v e   t o   w o r k   o n   d i f f e r e n t   u s e r   p a t h� ��� r    ��� b    ��� b    ��� m    	�� ��� ^ / U s e r s / b l u e p i x e l / D o c u m e n t s / w a i v e r s _ p e n d i n g / l o g /� I   	 �������� 0 getepochtime getEpochTime��  ��  � m    �� ���  . t x t� o      ���� "0 archivefilepath archiveFilePath� ��� r    !��� b    ��� b    ��� b    ��� m    �� ���  m v  � o    ���� *0 aftereffectslogfile afterEffectsLogFile� m    �� ���   � o    ���� "0 archivefilepath archiveFilePath� o      ���� 0 shscript  � ��� r   " -��� b   " +��� m   " #�� ���  t o u c h  � n   # *��� 1   ( *��
�� 
strq� o   # (���� *0 aftereffectslogfile afterEffectsLogFile� o      ���� 0 	shscript2  � ��� Q   . G���� I  1 6�����
�� .sysoexecTEXT���     TEXT� o   1 2���� 0 shscript  ��  � R      �����
�� .ascrerr ****      � ****� o      ���� 
0 errmsg  ��  � k   > G�� ��� I   > E������� $0 shownotification showNotification� ��� m   ? @�� ��� $ E r r o r   A r c h v i n g   L o g� ���� o   @ A���� 
0 errmsg  ��  ��  � ��� l  F F����  �  �  �  � ��� l  H H����  �  �  � ��� Q   H a���� I  K P���
� .sysoexecTEXT���     TEXT� o   K L�� 0 	shscript2  �  � R      ���
� .ascrerr ****      � ****� o      �� 
0 errmsg  �  � k   X a�� ��� I   X _���� $0 shownotification showNotification� ��� m   Y Z�� ��� , E r r o r   C r e a t i n g   N e w   L o g� ��� o   Z [�� 
0 errmsg  �  �  � ��� l  ` `����  �  �  �  � ��� l  b b����  �  �  �  � ��� l     ����  �  �  � ��� i  	� � I      ��� 0 killapp killApp � o      �� "0 applicationname applicationName�  �    k     /  r      b     	 m     

 �  k i l l a l l  	 n     1    �
� 
strq o    �� "0 applicationname applicationName o      �� 0 shscript    Q    ' k      I   ��
� .sysoexecTEXT���     TEXT o    �� 0 shscript  �    r     m    �
� boovtrue o      �� 0 	mysuccess 	mySuccess � r     m     �   o      �� 0 	mymessage 	myMessage�   R      � �
� .ascrerr ****      � ****  o      �� 
0 errmsg  �   k     '!! "#" r     #$%$ m     !�
� boovfals% o      �� 0 	mysuccess 	mySuccess# &�& r   $ ''(' o   $ %�� 
0 errmsg  ( o      �� 0 	mymessage 	myMessage�   )*) l  ( (����  �  �  * +,+ L   ( --- J   ( ,.. /0/ o   ( )�� 0 	mysuccess 	mySuccess0 1�1 o   ) *�� 0 	mymessage 	myMessage�  , 2�2 l  . .����  �  �  �  � 343 l     ����  �  �  4 565 i  
787 I      ���~� 20 checkaftereffectsstatus checkAfterEffectsStatus�  �~  8 k     @99 :;: r     <=< I     
�}>�|�} 0 getageoffile getAgeOfFile> ?�{? o    �z�z *0 aftereffectslogfile afterEffectsLogFile�{  �|  = o      �y�y &0 logfileageseconds logFileAgeSeconds; @A@ l   �x�w�v�x  �w  �v  A BCB Z    :DE�uFD G    GHG =   IJI o    �t�t &0 logfileageseconds logFileAgeSecondsJ m    �s
�s 
nullH ?    KLK o    �r�r &0 logfileageseconds logFileAgeSecondsL l   M�q�pM ]    NON o    �o�o 00 expectedmaxprocesstime expectedMaxProcessTimeO m    �n�n �q  �p  E k   ! 0PP QRQ l  ! !�mST�m  S b \ AE is likely hung, kill it and relaunch project, but we will finish submitting stats first	   T �UU �   A E   i s   l i k e l y   h u n g ,   k i l l   i t   a n d   r e l a u n c h   p r o j e c t ,   b u t   w e   w i l l   f i n i s h   s u b m i t t i n g   s t a t s   f i r s t 	R VWV r   ! $XYX m   ! "�l
�l boovfalsY o      �k�k 0 	mysuccess 	mySuccessW Z[Z r   % (\]\ m   % &^^ �__ H p l e a s e   r e s t a r t   A f t e r   E f f e c t s   p r o j e c t] o      �j�j 0 	mymessage 	myMessage[ `�i` I   ) 0�ha�g�h $0 shownotification showNotificationa bcb m   * +dd �ee   A E   H a n g   D e t e c t e dc f�ff m   + ,gg �hh < w i l l   t r y   t o   r e s t a r t   i t   f o r   y o u�f  �g  �i  �u  F k   3 :ii jkj l  3 3�elm�e  l   appears to be OK   m �nn "   a p p e a r s   t o   b e   O Kk opo r   3 6qrq m   3 4�d
�d boovtruer o      �c�c 0 	mysuccess 	mySuccessp s�bs r   7 :tut m   7 8vv �ww  u o      �a�a 0 	mymessage 	myMessage�b  C xyx l  ; ;�`�_�^�`  �_  �^  y z�]z L   ; @{{ J   ; ?|| }~} o   ; <�\�\ 0 	mysuccess 	mySuccess~ �[ o   < =�Z�Z 0 	mymessage 	myMessage�[  �]  6 ��� l     �Y�X�W�Y  �X  �W  � ��V� i  ��� I      �U�T�S�U *0 recycleaftereffects recycleAfterEffects�T  �S  � k     /�� ��� l     �R���R  � 6 0 AE is likely hung, kill it and relaunch project   � ��� `   A E   i s   l i k e l y   h u n g ,   k i l l   i t   a n d   r e l a u n c h   p r o j e c t� ��� I     �Q��P�Q $0 shownotification showNotification� ��� m    �� ��� " A E   N o n   R e s p o n s i v e� ��O� m    �� ��� 0 t r y i n g   a u t o m a t e d   r e s t a r t�O  �P  � ��� l   �N�M�L�N  �M  �L  � ��� I    �K��J�K 0 killapp killApp� ��I� m   	 
�� ���  A f t e r   E f f e c t s�I  �J  � ��� I   �H��G
�H .sysodelanull��� ��� nmbr� m    �F�F 
�G  � ��� l   �E�D�C�E  �D  �C  � ��� l   �B���B  � R L archive log file so we won't hit the error here again while AE is rebooting   � ��� �   a r c h i v e   l o g   f i l e   s o   w e   w o n ' t   h i t   t h e   e r r o r   h e r e   a g a i n   w h i l e   A E   i s   r e b o o t i n g� ��� I    �A�@�?�A 00 archiveaftereffectslog archiveAfterEffectsLog�@  �?  � ��� l   �>�=�<�>  �=  �<  � ��� l   �;���;  � "  launch AE with project file   � ��� 8   l a u n c h   A E   w i t h   p r o j e c t   f i l e� ��� I    "�:��9�: $0 shownotification showNotification� ��� m    �� ���  R e s t a r t i n g   A E� ��8� m    �� ��� , t h i s   m a y   t a k e   a   m i n u t e�8  �9  � ��� I   # -�7��6�7 60 launchaftereffectsproject launchAfterEffectsProject� ��5� o   $ )�4�4 20 aftereffectsprojectfile afterEffectsProjectFile�5  �6  � ��3� l  . .�2�1�0�2  �1  �0  �3  �V       @�/�   ����.�-�,�+�* ^ c�)�(�'�&�%�$ ��#������������������������������������������/  � >�"�!� ����������������������
�	��������� �������������������������������������������������������" 0 theplistpath thePListPath�! *0 aftereffectslogfile afterEffectsLogFile�  20 aftereffectsprojectfile afterEffectsProjectFile� 00 incomingfileextensions incomingFileExtensions� 80 rawtranscodefileextensions rawTranscodeFileExtensions� D0  bypassrawtranscodefileextensions  bypassRawTranscodeFileExtensions� 60 statsintervalcheckminutes statsIntervalCheckMinutes� $0 processdelaysecs processDelaySecs� 0 loopdelaysecs loopDelaySecs� 0 	automated  � 0 devendpoints devEndpoints� $0 computerusername computerUserName� $0 waivergrepstring waiverGrepString� >0 filesizecheck_minaeoutputsize filesizeCheck_minAEOutputSize� (0 filesizecheckdelay filesizeCheckDelay� (0 performfilecleanup performFileCleanup� 0 dev_mode  � 00 oldestfileagethreshold oldestFileAgeThreshold� 00 expectedmaxprocesstime expectedMaxProcessTime� "0 applicationname applicationName� $0 maxfilespercycle maxFilesPerCycle
� .aevtoappnull  �   � ****
� .miscidlenmbr    ��� null
� .aevtquitnull��� ��� null�
 $0 shownotification showNotification�	 "0 processonecycle processOneCycle� &0 checkpendingfiles checkPendingFiles� 0 filecleanup fileCleanup� ,0 movefilestoingestion moveFilesToIngestion� .0 moveimagestoingestion moveImagesToIngestion� :0 transcodeaftereffectsoutput transcodeAfterEffectsOutput� 0 handlewaivers handleWaivers� (0 checkrawfilestatus checkRawFileStatus� ,0 checkfilewritestatus checkFileWriteStatus�  0 gatherstats gatherStats�� "0 sendstatustobpc sendStatusToBPC�� 0 setvars setVars�� 0 setpaths setPaths�� 0 read_waiver  �� 0 get_element  �� 0 
fileexists 
fileExists�� 0 folderexists folderExists�� 0 	changeext 	changeExt��  0 getfreediskpct getFreeDiskPct�� (0 getfreediskspacegb getFreeDiskSpaceGB�� 0 getdiskstats getDiskStats�� 0 
is_running  �� 0 	readplist 	readPlist�� 0 
writeplist 
writePlist�� 0 getepochtime getEpochTime�� 0 number_to_string  �� 0 add_leading_zeros  �� 00 roundandtruncatenumber roundAndTruncateNumber�� 0 trimline trimLine�� (0 getageofoldestfile getAgeOfOldestFile�� 0 getageoffile getAgeOfFile�� 0 getoldestfile getOldestFile�� 60 launchaftereffectsproject launchAfterEffectsProject�� 00 archiveaftereffectslog archiveAfterEffectsLog�� 0 killapp killApp�� 20 checkaftereffectsstatus checkAfterEffectsStatus�� *0 recycleaftereffects recycleAfterEffects� ����� �   ! % ) - 0� ����� �   7� ����� �   ? C G J�. �- �, 

�+ boovfals
�* boovfals�) ��@�( 
�' boovtrue
�& boovfals�% �$ ��# 
� �� ���������
�� .aevtoappnull  �   � ****��  ��  �  � �� ��� � ����� ���������������������
�� .misccurdldt    ��� null
�� .ascrcmnt****      � ****�� $0 shownotification showNotification�� 0 
fileexists 
fileExists
�� .sysodisAaleR        TEXT
�� .aevtquitnull��� ��� null�� 0 ishappy isHappy�� 0 setvars setVars�� 0 isready1 isReady1�� 0 setpaths setPaths�� 0 isready2 isReady2
�� 
bool�� 0 gatherstats gatherStats�� "0 processonecycle processOneCycle�� �*j  �%j O*��l+ O*b   k+ f  �b   %j O*j 	Y hOeE�O*j+ E�O*j+ E�O�e 	 �e �& *jk+ O*ek+ Y a �%a %�%j O*j  a %j OP� ����������
�� .miscidlenmbr    ��� null��  ��  �  � ����-/���� "0 processonecycle processOneCycle
�� .misccurdldt    ��� null
�� .ascrcmnt****      � ****�� "*fk+  O*j �%b  %�%j Ob  � ��8��������
�� .aevtquitnull��� ��� null��  ��  �  � ��@������
�� .misccurdldt    ��� null
�� .ascrcmnt****      � ****�� 0 gatherstats gatherStats
�� .aevtquitnull��� ��� null�� *j  �%j O*jk+ O)jd* � ��M���������� $0 shownotification showNotification�� ����� �  ������  0 subtitlestring subtitleString�� 0 messagestring messageString��  � ������  0 subtitlestring subtitleString�� 0 messagestring messageString� ������������
�� 
null
�� 
appr
�� 
subt�� 
�� .sysonotfnull��� ��� TEXT
�� .sysodelanull��� ��� nmbr�� ,�� ��b  �� Y *��b  � Okj OP� ��r���������� "0 processonecycle processOneCycle�� ����� �  ����  0 isinitialcycle isInitialCycle��  � ����  0 isinitialcycle isInitialCycle� ��|����������������������������
#����
�� .misccurdldt    ��� null
�� .ascrcmnt****      � ****
�� 
null�� $0 shownotification showNotification�� 0 gatherstats gatherStats�� 0 handlewaivers handleWaivers
�� .sysodelanull��� ��� nmbr�� :0 transcodeaftereffectsoutput transcodeAfterEffectsOutput�� ,0 movefilestoingestion moveFilesToIngestion�� .0 moveimagestoingestion moveImagesToIngestion�� 0 filecleanup fileCleanup
� 
givu� 
� .sysodisAaleR        TEXT
� .aevtquitnull��� ��� null��(*j  �%j O*��l+ O*kk+ O*j+ O*j  �%b  %j Ob  j 	O*kk+ O*j+ 
O*j  �%b  %j Ob  j 	O*kk+ O*j+ O*j+ O*j  �%b  %j Ob  j 	O*j+ O*j  a %b  %j Ob  j 	O*kk+ O*j  a %j O� *a a l+ Y *a a b  %a %l+ Ob  	f  &*j  a %j Oa a a l O*j Y hOP� �C������ &0 checkpendingfiles checkPendingFiles� ��� �  ��� 0 oldlist oldList� 0 newlist newList�  � �������� 0 oldlist oldList� 0 newlist newList� 0 
filesadded 
filesAdded� 0 filesremoved filesRemoved� 0 isprocessing isProcessing� 0 i  � 0 n  � ����������
� 
list
� 
leng
� 
bool
� .ascrcmnt****      � ****
� 
cobj
� .corecnte****       ****
� 
TEXT� �jv�&E�Ojv�&E�OfE�O��,��,	 	��,j�& ���  �j OfY hO *k��-j kh ��/E�O�� 	��6FY h[OY��O *k��-j kh ��/E�O�� 	��6FY h[OY��O�%�&j O�%j O��,j	 	��,j �& fY hO��,j	 	��,j�& eY hOPY �j OeE�O�OP� �'������ 0 filecleanup fileCleanup�  �  � ��� 0 shscript  � 
0 errmsg  � 5���~
� .sysoexecTEXT���     TEXT� 
0 errmsg  �~  � (b  e  �E�O 
�j W X  hOPY hOP� �}P�|�{ �z�} ,0 movefilestoingestion moveFilesToIngestion�|  �{    �y�x�w�v�u�t�s�r�q�p�o�y 0 myfolder  �x 0 	pmyfolder  �w 0 shscript  �v 0 filelist  �u 
0 errmsg  �t 0 
cyclecount 
cycleCount�s 0 i  �r 
0 myfile  �q 0 newfile  �p 0 basefilename baseFilename�o $0 intermediatelist intermediateList +�n�m�ld�kn�jr�i�h�g�f�e���d�c�b�a�`�_���^��]�\�[�Z%�Y3�X79RWqu���n 0 processed_media  
�m 
psxp
�l .misccurdldt    ��� null
�k .ascrcmnt****      � ****
�j 
strq
�i .sysoexecTEXT���     TEXT
�h 
cpar
�g 
list�f 
0 errmsg  �e  
�d 
kocl
�c 
cobj
�b .corecnte****       ****
�a 
TEXT�`  B@�_ ,0 checkfilewritestatus checkFileWriteStatus�^ 0 spock_ingestor  
�] .sysobeepnull��� ��� long
�\ 
givu�[ 
�Z .sysodisAaleR        TEXT�Y 0 	changeext 	changeExt�X 0 
media_pool  �z��E�O��,E�O*j �%j O��,%�%E�O �j �-�&E�W !X  *j �%�%j O�j Ojv�&E�OjE�Ob�[�a l kh �b   Y hO��%a &E�O�E�O*�a km+ e a ��,%a %_ �,%�%E�O *j a %j O�j W :X  *j a %�%j O*j O*j O*j Oa �%a a l OO*�a &a l+  E�Oa !_ "�,%a #%�%a $%E�O �j �-�&E�W %X  *j a %%�%j Oa &j Ojv�&E�O M�[�a l kh a '_ "�,%a (%�%a &E�O 
�j W X  *j a )%�%j OP[OY��O�kE�OPY hOP[OY��O*j a *%j OP� �W��V�U�T�W .0 moveimagestoingestion moveImagesToIngestion�V  �U   �S�R�Q�P�O�N�M�L�K�J�I�S 0 myfolder  �R 0 	pmyfolder  �Q 0 shscript  �P 0 filelist  �O 
0 errmsg  �N 0 
cyclecount 
cycleCount�M 0 i  �L 
0 myfile  �K 0 newfile  �J 0 basefilename baseFilename�I $0 intermediatelist intermediateList .�H�G�F��E��D��C�B�A�@�?���>�=�<�;�:�9�8@D�7KO_m�6{�5�4�3���2��������H 0 processed_images  
�G 
psxp
�F .misccurdldt    ��� null
�E .ascrcmnt****      � ****
�D 
strq
�C .sysoexecTEXT���     TEXT
�B 
cpar
�A 
list�@ 
0 errmsg  �?  
�> 
kocl
�= 
cobj
�< .corecnte****       ****
�; 
TEXT�: 0 	changeext 	changeExt�9   �P�8 ,0 checkfilewritestatus checkFileWriteStatus�7 0 spock_ingestor  
�6 .sysobeepnull��� ��� long
�5 
givu�4 
�3 .sysodisAaleR        TEXT�2 0 
media_pool  �T��E�O��,E�O*j �%j O��,%�%E�O �j �-�&E�W !X  *j �%�%j O�j Ojv�&E�OjE�Oz�[�a l kh �b   Y hO��%a &E�O*�a &a l+ E�O*�a km+ e *a ��,%a %_ �%�,%a %��,%a %E�O *j a %j O�j W :X  *j a %�%j O*j O*j O*j Oa �%a  a !l "OO*�a &a #l+ E�Oa $_ %�,%a &%�%a '%E�O �j �-�&E�W %X  *j a (%�%j Oa )j Ojv�&E�O M�[�a l kh a *_ %�,%a +%�%a &E�O 
�j W X  *j a ,%�%j OP[OY��O�kE�OPY hOP[OY��O*j a -%j OP� �1�0�/�.�1 :0 transcodeaftereffectsoutput transcodeAfterEffectsOutput�0  �/   �-�,�+�*�)�(�'�&�%�$�#�"�!� ������- 0 myfolder  �, 0 completed_waivers  �+ 0 shscript  �* 0 filelist  �) 
0 errmsg  �( 0 
cyclecount 
cycleCount�' 0 i  �&  0 outputfilename outputFilename�% *0 outputfilenameclean outputFilenameClean�$ 0 	filesize1  �# 0 	filesize2  �"  0 filesize1clean filesize1Clean�!  0 filesize2clean filesize2Clean�  0 skipthisfile skipThisFile� &0 skipthisfileclean skipThisFileClean� 0 
fileexists 
fileExists� 
0 myfile  � 0 	shscript1  � 0 	shscript3   2��'���1�;�?�����[���~����
�	�����+6N������������� ���� 0 	ae_output  � @0 pendingwaiversforaefolderposix pendingWaiversForAEFolderPosix
� 
psxp
� 
TEXT
� .misccurdldt    ��� null
� .ascrcmnt****      � ****
� 
strq
� .sysoexecTEXT���     TEXT
� 
cpar
� 
list� 
0 errmsg  �  
� 
kocl
� 
cobj
� .corecnte****       ****� 0 	changeext 	changeExt�
 0 
fileexists 
fileExists
�	 
psxf
� .rdwrgeofcomp       ****
� .sysodelanull��� ��� nmbr
� 
bool� 0 	ame_watch  � %0 !waivers_pending_completed_archive  
� .sysodlogaskr        TEXT
� .sysobeepnull��� ��� long
� 
givu�  
�� .sysodisAaleR        TEXT�.�E�O��%�,�&E�O*j �%j O��,%�%E�O �j �-�&E�W !X  *j �%j Oa j Ojv�&E�OjE�O�[a a l kh *��&a l+ E�O*��&a l+ E�O�b   Y hOjE�OkE�OjE�OkE�OfE�OfE�OfE�O �hZ*j a %j O��%�&E^ O*] k+  *a ] /j E�OeE�Y jE�O*j a %�%j O�b   a j Okj Y hO� *a ] /j E�Y hO*j a %�%j O�� 	 �b  a & *j a  %j OY hO*j a !%���&%j O�b  
 	��a & a "j OeE�OY h[OY�O�f  �a #��,%�%a $%_ %�,%�%E^ Oa &��,%�%a '%_ (�,%�%E^ Ob  e  
�j )Y hO **j a *%j O] j O] j O�kE�OPW :X  *j a +%�%j O*j ,O*j ,O*j ,Oa -�%a .a /l 0OOPY hOP[OY��O*j a 1%j OP� ����������� 0 handlewaivers handleWaivers��  ��   ���������������������������� 60 incomingwaiverfolderposix incomingWaiverFolderPosix�� 0 shscript  ��  0 waiverfilelist waiverFileList�� 
0 errmsg  �� 0 
cyclecount 
cycleCount�� 0 outteri outterI�� 0 
waiverfile 
waiverFile�� 0 mywaiver myWaiver�� 0 rawfileslist rawFilesList�� 0 movefileslist moveFilesList�� 0 myincrementer myIncrementer�� 0 	myrawfile 	myRawFile�� 0 success    ��������"������������Q��������������������	
	����	;	A��
�� .misccurdldt    ��� null
�� .ascrcmnt****      � ****�� .0 incomingrawxmlwaivers incomingRawXmlWaivers
�� 
strq
�� .sysodlogaskr        TEXT
�� .sysoexecTEXT���     TEXT
�� 
cpar
�� 
list�� 
0 errmsg  ��  
�� 
kocl
�� 
cobj
�� .corecnte****       ****�� 0 read_waiver  �� 0 waiver_total_raw_files  
�� 
null
�� 
bool�� 0 get_element  �� (0 checkrawfilestatus checkRawFileStatus�� @0 pendingwaiversforaefolderposix pendingWaiversForAEFolderPosix
�� 
TEXT
�� .sysodisAaleR        TEXT��X*j  �%j O�E�O��,%�%b  %E�Ob  e  
�j Y hO �j �-�&E�W X  *j  �%j O�j Ojv�&E�OjE�O ��[��l kh �b   Y hO��%E�O*�k+ E�OjvE�OjvE�O�a ,a 	 �a ,ja & 5jE�O +�a ,Ekh�kE�O*�a a �%m+ E�O��6G[OY��Y ��6GO*�k+  Ta ��,%a %_ %�a &%E�OfE�O �j O�kE�OeE�W  X  *j  a %�%j Oa �%j Y hOP[OY�!OP� ��	V����	���� (0 checkrawfilestatus checkRawFileStatus�� ��
�� 
  ���� 0 rawfileslist rawFilesList��   �������������������������������������� 0 rawfileslist rawFilesList�� "0 filereadystatus fileReadyStatus��  0 skiptranscoded skipTranscoded�� 0 movefileslist moveFilesList�� 0 i  �� 0 
namelength  �� 
0 myname  �� 0 fileextension fileExtension�� $0 mytranscodedfile myTranscodedFile�� 0 	myrawfile 	myRawFile��  0 foundextension foundExtension�� 0 shscript  �� 0 success  �� $0 filemove_success fileMove_success�� 
0 errmsg  �� 0 rawfilei rawFileI�� 
0 myfile  �� 0 pmyfile  	 '	y�������	��	����
 �
$�
8
<����
_�
f���
�
��
�
��
�
�
� .sysodlogaskr        TEXT
� 
kocl
� 
cobj
� .corecnte****       ****
� 
TEXT
� 
cha � � >0 transcodecompletedfolderposix transcodeCompletedFolderPosix� :0 incomingrawmediafolderposix incomingRawMediaFolderPosix��� ,0 checkfilewritestatus checkFileWriteStatus
� 
strq� :0 transcodependingfolderposix transcodePendingFolderPosix
� .sysoexecTEXT���     TEXT� 
0 errmsg  �  
� .misccurdldt    ��� null
� .ascrcmnt****      � ****
� .sysodisAaleR        TEXT
� 
leng
� 
bool
� 
psxp� 0 
media_pool  ��*b  e  �%j Y hOfE�OfE�OjvE�O�[��l kh ��&�-j E�O�[�\[Zk\Z��2�&E�O �b  [��l kh ��%�%�%�&E�O��%�%�%�&E�O�E�O*��km+  eE�O��%�&�6GOY hO*�jkm+  �fE�Ob  � ��,%a %_ %��%%E�Y #b  � a ��,%a %�%��%%E�Y hOfE�O �j OeE�OOPW $X  *j a %�%j Oa �%j OfE�OPY hOP[OY�.OP[OY��O�e 	 �a ,�a , a & � ��[��l kh b  e  a �%�&j Y hOa �%�%�&E^ O] a  ,E^ Oa !] �,%a "%_ #%�%E�OfE�Ob  e  
�j Y hO �j OeE�W &X  *j a $%�%j Oa %�%j OfE�OOP[OY�cO�OPY b  e  a &j Y hOfOPO�OP� �.���� ,0 checkfilewritestatus checkFileWriteStatus� ��   ���� 0 pmyfile  � "0 expectedminsize expectedMinSize� &0 checkdelayseconds checkDelaySeconds�   ������� 0 pmyfile  � "0 expectedminsize expectedMinSize� &0 checkdelayseconds checkDelaySeconds�  0 thisfilestatus thisFileStatus� 0 	filesize1  � 0 	filesize2   ������
� 
null� 0 
fileexists 
fileExists
� 
psxf
� .rdwrgeofcomp       ****
� 
bool
� .sysodelanull��� ��� nmbr� lfE�OjE�OkE�O��  kE�Y hO*�k+  E*�/j E�O�k
 ���& fY hO�j O*�/j E�O��  eY fOPY fO�� ������ 0 gatherstats gatherStats� ��   �� 0 intervalmins intervalMins�   �����������������~�}�|�{�z�y�x�w�v�u�t�s�r� 0 intervalmins intervalMins� 0 
initialrun 
initialRun� 0 timenow timeNow� 0 	nextcheck 	nextCheck� 40 updatestalefiletimestamp updateStaleFileTimestamp� 0 oldestfileage oldestFileAge� 0 	dataitems 	dataItems� 0 failedtests failedTests� *0 recycleaftereffects recycleAfterEffects� 0 	firstitem 	firstItem� 0 ae_test  � 00 aftereffectsresponsive afterEffectsResponsive� 0 ame_test  � 0 nextitem nextItem� 0 dropbox_test  � 0 shscript  �~ 0 mylist myList�} 
0 errmsg  �| 0 mycount myCount�{ 0 oldcount oldCount�z 0 ae_waiver_delta  �y 0 isprocessing isProcessing�x 0 mydatavalue myDataValue�w 0 mythreshhold myThreshhold�v 0 ame_file_delta  �u 0 dropbox_waiver_delta  �t 0 memory_stats  �s 0 freediskpct freeDiskPct�r 0 
freediskgb 
freeDiskGB ��q�p�o�n�m�l�k�j����i�h,5�g;D�f�e�dq��������������c�b�a�`�_�^�]5�\�[K�Z�Y[e�X�W�V��������U(*4KO�Tky�������S����Rgo������������Q�"2<�P�����������+-7�O�N�M�L�Kck�J{}�����������I�H���2BLT^`jz|��������G�F�q 0 getepochtime getEpochTime�p 80 laststatsgatheredtimestamp lastStatsGatheredTimestamp�o <�n  ���m 0 number_to_string  �l :0 laststalefilechecktimestamp lastStaleFileCheckTimestamp�k *0 forcestalefilecheck forceStaleFileCheck�j 0 ishappy isHappy�i 0 
is_running  �h 0 
writeplist 
writePlist
�g .sysodlogaskr        TEXT�f 20 checkaftereffectsstatus checkAfterEffectsStatus
�e 
list
�d 
cobj�c @0 pendingwaiversforaefolderposix pendingWaiversForAEFolderPosix
�b 
strq
�a .sysoexecTEXT���     TEXT
�` 
cpar�_ 
0 errmsg  �^  
�] .misccurdldt    ��� null
�\ .ascrcmnt****      � ****
�[ 
leng
�Z 
null�Y 0 	readplist 	readPlist
�X 
bool�W 00 pendingaewaiverlistold pendingAEWaiverListOLD�V &0 checkpendingfiles checkPendingFiles�U (0 getageofoldestfile getAgeOfOldestFile
�T .corecnte****       ****�S 0 	ame_watch  �R .0 pendingamefilelistold pendingAMEFileListOLD�Q .0 incomingrawxmlwaivers incomingRawXmlWaivers�P :0 pendingdropboxwaiverlistold pendingDropboxWaiverListOLD�O  0 getfreediskpct getFreeDiskPct
�N 
long�M (0 getfreediskspacegb getFreeDiskSpaceGB�L �K 

�J .sysodisAaleR        TEXT
�I .sysosigtsirr   ��� null
�H 
siip�G "0 sendstatustobpc sendStatusToBPC�F *0 recycleaftereffects recycleAfterEffects�	�fE�O�j (*j+  E�O*��� � k+ E�O�� hY hY *j+  E�O*j+  E�OfE�OeE�OeE�OfE�OjE�O�E�O�E�OfE�O�E�O*�k+ E�O*b   ��m+ O�f  0fE�O��%E�Ob  e  a j Y hOa E�OeE�OPY a E�O��%E�O*j+ a &E�O�a k/f  eE�O�a %E�Y hO*a k+ E�O*b   a �m+ O�f  ,fE�O�a %E�Ob  e  a j Y hOa E�Y a E�O�a %�%E�O*a k+ E�O*b   a  �m+ O�f  ,fE�O�a !%E�Ob  e  a "j Y hOa #E�Y a $E�O�a %%�%E�Oa &_ 'a (,%a )%E�O �j *a +-a &E^ W X , -*j .a /%] %j 0OjvE^ O] a 1,E^ O*b   a 2a 3m+ 4E^ O] ] E^ O*b   a 5] m+ O*b   a 6] m+ O*j+  E�O*�b  � k+ E�O��
 	�e a 7&
 	�e a 7& T*_ 8] l+ 9E^ O] E` 8O] f  *eE�OfE�O�a :%E�Ob  e  a ;j Y hY hOeE�OPY hO] E^ Oa <] %a =%E�O�a >%�%E�O] E^ Oa ?] %a @%E�O�a A%�%E�O*_ 'a Bl+ CE^ O] � 
] E�Y hOa D] %a E%E�O�a F%�%E�OjE^ Oa G_ 'a (,%a H%E�O -�j *a +-a &j IE^ O*b   a J] m+ OPW X , -*j .a K%] %j 0OjE^ O] ]  &fE�O�a L%E�Ob  e  a Mj Y hY hO] E^ Oa N] %a O%E�O�a P%�%E�Oa Q_ Ra (,%a S%E�O �j *a +-a &E^ W X , -*j .a T%] %j 0OjvE^ O] a 1,E^ O*b   a Ua 3m+ 4E^ O] ] E^ O*b   a V] m+ O*b   a W] m+ O*j+  E�O*�b  � k+ E�O��
 	�e a 7&
 	�e a 7& R*_ X] l+ 9E^ O] E` XO] f  *fE�OeE�O�a Y%E�Ob  e  a Zj Y hY hOeE�Y hO] E^ Oa [] %a \%E�O�a ]%�%E�O] E^ Oa ^] %a _%E�O�a `%�%E�O*_ Ra al+ CE^ O] � 
] E�Y hOa b] %a c%E�O�a d%�%E�Oa e_ fa (,%a g%E�O �j *a +-a &E^ W X , -*j .a h%] %j 0OjvE^ O] a 1,E^ O*b   a ia 3m+ 4E^ O] ] E^ O*b   a j] m+ O*b   a k] m+ O*j+  E�O*�b  � k+ E�O��
 	�e a 7&
 	�e a 7& R*_ l] l+ 9E^ O] E` lO] f  *fE�OeE�O�a m%E�Ob  e  a nj Y hY hOeE�Y hO] E^ Oa o] %a p%E�O�a q%�%E�O] E^ Oa r] %a s%E�O�a t%�%E�O*_ fa ul+ CE^ O] � 
] E�Y hOa v] %a w%E�O�a x%�%E�O a yj *E^ W X , -a z] %E^ O] E^ Oa {] %a |%E�O�a }%�%E�O*j+ ~a &E^ O*j+ �a &E^ O] a �
 ] a �a 7& &fE�O�a �%E�Ob  e  a �j �Y hY hO] E^ Oa �] %a �%E�O�a �%�%E�O] E^ Oa �] %a �%E�O�a �%�%E�O *b   a �a �m+ 4E^ W X , -a �] %E^ Oa �] %a �%E�O�a �%�%E�O*j �a �,E^ Oa �] %a �%E�O�a �%�%E�O�E^ Oa �] %a �%E�O�a �%�%E�O�b   �a �%E�Y hO�e  a �E^ OfE�Y a �E^ Ob  e  a �j �Y hOa �] %a �%E�O�a �%�%E�O�E^ Oa �] %a �%E�O�a �%�%E�O*j+  E^ Oa �] %a �%E�O�a �%�%E�O*j �a �,E^ Oa �] %a �%E�O�a �%�%E�Ob  e  
�j Y hO*�k+ �O*j+  E�O�e  *j+  E�Y hO�e 	 b  f a 7& 
*j+ �Y hOP� �E��D�C�B�E "0 sendstatustobpc sendStatusToBPC�D �A�A   �@�@ 0 	dataitems 	dataItems�C   �?�>�=�<�;�:�9�? 0 	dataitems 	dataItems�> 0 myip myIP�= 0 
theurlbase 
theURLbase�< 0 
thepayload 
thePayload�; 0 shscript  �: 0 	myoutcome 	myOutcome�9 
0 errmsg   �8�7�6�5-3CEGW�4[�3�2�1�0
�8 .sysosigtsirr   ��� null
�7 
siip�6 0 	readplist 	readPlist
�5 
bool
�4 
strq
�3 .sysodlogaskr        TEXT
�2 .sysoexecTEXT���     TEXT�1 
0 errmsg  �0  �B �*j  �,E�O�E�O*b   �b  
m+ Ec  
Ob  
�&Ec  
Ob  
e  
�%E�Y �%E�O�%�%�%�%E�O��,%�%�%E�Ob  e  
�j Y hO �j E�O�W 	X  �OP� �/}�.�-�,�/ 0 setvars setVars�.  �-   �+�+ 
0 errmsg   �*�)�(�'��&�%�$���#����"�!0EZo� �
�* 
list�) 00 pendingaewaiverlistold pendingAEWaiverListOLD�( .0 pendingamefilelistold pendingAMEFileListOLD�' :0 pendingdropboxwaiverlistold pendingDropboxWaiverListOLD
�& 
null�% 0 	readplist 	readPlist�$ 0 computer_id  
�# 
bool�" 0 stationtype stationType
�! 
long�  
0 errmsg  �  �,K@jv�&E�Ojv�&E�Ojv�&E�O*b   ��m+ E�O*b   �b  m+ Ec  O*b   �b  m+ �&Ec  O*b   �b  
m+ �&Ec  
O*b   �em+ �&Ec  O*b   ��m+ E�O*b   �b  	m+ �&Ec  	O*b   a b  m+ a &Ec  O*b   a b  m+ a &Ec  O*b   a b  m+ �&Ec  O*b   a b  m+ a &Ec  O*b   a b  m+ a &Ec  OeW 	X  �OP� ������ 0 setpaths setPaths�  �   ��� 0 filetest fileTest� 
0 errmsg   6�����������������������,8�O]_�
�	u����������������/� 0 	readplist 	readPlist� "0 dropboxrootpath dropboxRootPath� 0 stationtype stationType� .0 incomingrawxmlwaivers incomingRawXmlWaivers� :0 incomingrawmediafolderposix incomingRawMediaFolderPosix� :0 transcodependingfolderposix transcodePendingFolderPosix� 0 folderexists folderExists� 
0 errmsg  �  
� 
strq
� .sysoexecTEXT���     TEXT� >0 transcodecompletedfolderposix transcodeCompletedFolderPosix� @0 pendingwaiversforaefolderposix pendingWaiversForAEFolderPosix� %0 !waivers_pending_completed_archive  �
 0 
media_pool  �	  � 0 processed_media  � 0 	ae_output  � 0 processed_images  � 0 	ame_watch  � 0 spock_ingestor  ���*b   ��m+ E�Ob  
e  ��%�%E�Y ��%E�O��%E�O��%E�O�b  %�%E�O *�k+ E�W 
X  eE�O�f  a �a ,%j Y hOa b  %a %E` O *_ k+ E�W 
X  eE�O�f  a _ a ,%j Y hOa b  %a %E` O *_ k+ E�W 
X  eE�O�f  a _ a ,%j Y hO_ a %E` O *_ k+ E�W 
X  eE�O�f  a _ a ,%j Y hOa b  %a %E`  O *_  k+ E�W 
X ! eE�O�f  a "_  a ,%j Y hOa #b  %a $%E` %O *_ %k+ E�W 
X ! eE�O�f  a &_ %a ,%j Y hOa 'b  %a (%E` )O *_ )k+ E�W 
X ! eE�O�f  a *_ )a ,%j Y hOa +b  %a ,%E` -O *_ -k+ E�W 
X ! eE�O�f  a ._ -a ,%j Y hOa /b  %a 0%E` 1O *_ 1k+ E�W 
X ! eE�O�f  a 2_ 1a ,%j Y hO�a 3%E` 4O *_ 4k+ E�W 
X ! eE�O�f  a 5_ 4a ,%j Y hOeW 	X  �OP� �W��� � 0 read_waiver  � ����   ���� 0 	thewaiver  �   	�������������������� 0 	thewaiver  �� 0 	waiver_id  �� 0 waiver_created  �� 0 waiver_program  �� 0 waiver_activation  �� 0 waiver_team  �� 0 waiver_event  �� 0 waiver_total_raw_files  �� 0 waiver_data   il��wz�������������������������������� 0 get_element  ��  ��  �� 0 	waiver_id  �� 0 waiver_created  �� 0 waiver_program  �� 0 waiver_activation  �� 0 waiver_team  �� 0 waiver_event  �� 0 waiver_total_raw_files  �� �  �*���m+ E�O*���m+ E�O*���m+ E�O*���m+ E�O*���m+ E�O*���m+ E�O *���m+ E�W 
X  kE�Oa �a �a �a �a �a �a �a E�O�OP� ����������� 0 get_element  �� ����   �������� 0 	thewaiver  �� 0 node  �� 0 element_name  ��   ������������ 0 	thewaiver  �� 0 node  �� 0 element_name  �� 0 xmldata  �� 
0 errmsg   ������������������
�� 
xmlf
�� 
pcnt
�� 
xmle
�� kfrmname
�� 
valL
�� 
ret �� 
0 errmsg  ��  
�� .sysodisAaleR        TEXT�� >  � *�/�,��0E�O���0�,E�UW X  b  e  
�j 	Y hO�E�� ��&�������� 0 
fileexists 
fileExists�� �� ��    ���� 0 thefile theFile��   ���� 0 thefile theFile 5����
�� 
file
�� .coredoexnull���     ****�� � *�/j  eY fU� ��<����!"���� 0 folderexists folderExists�� ��#�� #  ���� 0 thefile theFile��  ! ���� 0 thefile theFile" K����
�� 
cfol
�� .coredoexnull���     ****�� � *�/j  eY fU� ��R���$%��� 0 	changeext 	changeExt�� �&� &  ��� 0 filename  � 0 new_ext  �  $ ����� 0 filename  � 0 new_ext  � 0 mylength  � 0 	newstring  % ����
� 
cha 
� .corecnte****       ****� 
� 
TEXT� !��-j �E�O�[�\[Zk\Z�2�&�%E�O�� �w��'(��  0 getfreediskpct getFreeDiskPct�  �  ' �� 0 rawpct rawPct( ������ 0 getdiskstats getDiskStats� 0 capacity  � 0 trimline trimLine� d� **j+  �,�km+ E�O�� ����)*�� (0 getfreediskspacegb getFreeDiskSpaceGB�  �  )  * ��� 0 getdiskstats getDiskStats� 0 	available  � 
*j+  �,E� ����+,�� 0 getdiskstats getDiskStats�  �  + ��������� 0 old_delimts  � 0 memory_stats  � 0 	rawstring  � 0 thelist theList� 0 newlist newList� 0 i  � 0 	finallist 	finalList� 
0 errmsg  , ����������������������
� 
ascr
� 
txdl
� .sysoexecTEXT���     TEXT
� 
cpar
� 
citm
� 
kocl
� 
cobj
� .corecnte****       ****
� 
ctxt� 0 diskname  � 0 
onegblocks 
OneGblocks� 0 used  � 0 	available  � � 0 capacity  � � 
� 
0 errmsg  �  � ���,E�O ��j E�O��l/E�O���,FO��-E�O���,FOjvE�O (�[��l 	kh ��-� ��-�6GY hOP[OY��O��k/�-���l/�-��m/�-��a /�-a ��a /�-a E�W X  hO���,FO�OP� �A��-.�� 0 
is_running  � �/� /  �� 0 appname appName�  - �~�}�|�{�~ 0 appname appName�} "0 listofprocesses listOfProcesses�| 0 appisrunning appIsRunning�{ 0 thisitem thisItem. N�z�y�x�w�v
�z 
prcs
�y 
pnam
�x 
kocl
�w 
cobj
�v .corecnte****       ****� ;� 	*�-�,E�UOfE�O #�[��l kh �� 
eE�OY h[OY��O�OP� �ul�t�s01�r�u 0 	readplist 	readPlist�t �q2�q 2  �p�o�n�p 0 theplistpath thePListPath�o 0 plistvar plistVar�n 0 defaultvalue defaultValue�s  0 �m�l�k�j�i�h�m 0 theplistpath thePListPath�l 0 plistvar plistVar�k 0 defaultvalue defaultValue�j 0 	mycommand 	myCommand�i 0 myscript myScript�h 
0 errmsg  1 ���g�f�e�d�c
�g .sysoexecTEXT���     TEXT�f 
0 errmsg  �e  
�d 
null
�c .sysodisAaleR        TEXT�r 3 �%�%E�O�%�%E�O�j W X  ��  
�j Y hO�� �b��a�`34�_�b 0 
writeplist 
writePlist�a �^5�^ 5  �]�\�[�] 0 theplistpath thePListPath�\ 0 plistvar plistVar�[ 0 thevalue theValue�`  3 �Z�Y�X�W�V�U�Z 0 theplistpath thePListPath�Y 0 plistvar plistVar�X 0 thevalue theValue�W 0 	mycommand 	myCommand�V 0 myscript myScript�U 
0 errmsg  4 �����T�S�R�T 
0 errmsg  �S  
�R .sysodisAaleR        TEXT�_ $ �%�%�%�%E�O�%�%E�W X  �j � �Q��P�O67�N�Q 0 getepochtime getEpochTime�P  �O  6 �M�M 
0 mytime  7 ��L�K�J�I
�L .sysoexecTEXT���     TEXT
�K 
TEXT�J  ���I 0 number_to_string  �N �j �&E�O�� E�O*�k+ � �H��G�F89�E�H 0 number_to_string  �G �D:�D :  �C�C 0 this_number  �F  8 	�B�A�@�?�>�=�<�;�:�B 0 this_number  �A 0 x  �@ 0 y  �? 0 z  �> 0 decimal_adjust  �= 0 
first_part  �< 0 second_part  �; 0 converted_number  �: 0 i  9 �9��8�7�6�5�4�3�2F�1�0x
�9 
TEXT
�8 
psof
�7 
psin�6 
�5 .sysooffslong    ��� null
�4 
cha 
�3 
leng
�2 
nmbr�1  �0  �E ���&E�O�� �*���� E�O*���� E�O*���� E�O�[�\[Z���,\Zi2�&�&E�O�j �[�\[Zk\Z�k2�&E�Y �E�O�[�\[Z�k\Z�k2�&E�O�E�O &k�kh  ���/%E�W X  ��%E�[OY��O�Y �� �/��.�-;<�,�/ 0 add_leading_zeros  �. �+=�+ =  �*�)�* 0 this_number  �) 0 max_leading_zeros  �-  ; �(�'�&�%�$�#�( 0 this_number  �' 0 max_leading_zeros  �& 0 threshold_number  �% 0 leading_zeros  �$ 0 digit_count  �# 0 character_count  < �"�!�� ����" 

�! 
long
�  
TEXT
� 
leng
� 
ctxt�, H�$�&E�O�� 7�E�O�k"�&�,E�O�k�E�O �kh��%�&E�[OY��O���&%�&Y ��&� ����>?�� 00 roundandtruncatenumber roundAndTruncateNumber� �@� @  ��� 0 	thenumber 	theNumber� .0 numberofdecimalplaces numberOfDecimalPlaces�  > ������� 0 	thenumber 	theNumber� .0 numberofdecimalplaces numberOfDecimalPlaces� $0 theroundingvalue theRoundingValue� 0 themodvalue theModValue� 0 thesecondpart theSecondPart� 0 thefirstpart theFirstPart? ����",9��Z�|� 0 number_to_string  
� 
nmbr
� 
ctxt
� 
leng
� 
TEXT� ��j  ��E�O*�k"k+ Y hO�E�O �kh�%E�[OY��O�%�&E�O��E�O�E�O �kkh�%E�[OY��O�%�&E�O�k#�"E�O��&�,�  ���&�,kh�%�&E�[OY��Y hO�k"E�O*�k+ E�O��%�%E�O�� ���
�	AB�� 0 trimline trimLine�
 �C� C  ���� 0 	this_text  � 0 
trim_chars  � 0 trim_indicator  �	  A ���� � 0 	this_text  � 0 
trim_chars  � 0 trim_indicator  �  0 x  B ������������
�� 
leng
�� 
cha 
�� 
TEXT��  ��  � |��,E�Ojllv� 0 *h�� �[�\[Z�k\Zi2�&E�W 	X  �[OY��Y hOkllv� 1 +h�� �[�\[Zk\Z�k'2�&E�W 	X  �[OY��Y hO�� �������DE���� (0 getageofoldestfile getAgeOfOldestFile�� ��F�� F  ������ 0 filepath filePath�� 0 fileextension fileExtension��  D �������� 0 filepath filePath�� 0 fileextension fileExtension�� 
0 myfile  E ������ 0 getoldestfile getOldestFile�� 0 getageoffile getAgeOfFile�� *��l+  E�O�� *��%k+ Y j� �� ����GH���� 0 getageoffile getAgeOfFile�� ��I�� I  ���� 
0 myfile  ��  G ���������� 
0 myfile  �� 0 shscript  ��  0 fileageseconds fileAgeSeconds�� 
0 errmsg  H 68����������
�� .sysoexecTEXT���     TEXT
�� 
long�� 
0 errmsg  ��  
�� 
null�� #�%�%E�O �j �&E�W 
X  �E�O�� ��O����JK���� 0 getoldestfile getOldestFile�� ��L�� L  ������ 0 filepath filePath�� 0 fileextension fileExtension��  J ������������ 0 filepath filePath�� 0 fileextension fileExtension�� 0 shscript  �� 0 
oldestfile 
oldestFile�� 
0 errmsg  K f��jl������y
�� 
strq
�� .sysoexecTEXT���     TEXT�� 
0 errmsg  ��  �� '��,%�%�%�%E�O �j E�W 
X  �E�O�� �������MN���� 60 launchaftereffectsproject launchAfterEffectsProject�� ��O�� O  ���� $0 projectfileposix projectFilePOSIX��  M �������� $0 projectfileposix projectFilePOSIX�� 0 shscript  �� 
0 errmsg  N ���������
�� 
strq
�� .sysoexecTEXT���     TEXT�� 
0 errmsg  ��  �� ��,%E�O 
�j W X  hOP� �������PQ��� 00 archiveaftereffectslog archiveAfterEffectsLog��  ��  P ������ 0 	timestamp  � "0 archivefilepath archiveFilePath� 0 shscript  � 0 	shscript2  � 
0 errmsg  Q �������������� 0 getepochtime getEpochTime
� 
strq
� .sysoexecTEXT���     TEXT� 
0 errmsg  �  � $0 shownotification showNotification� d*j+  E�O�*j+  %�%E�O�b  %�%�%E�O�b  �,%E�O 
�j W X  	*�l+ OPO 
�j W X  	*�l+ OPOP� � ��RS�� 0 killapp killApp� �T� T  �� "0 applicationname applicationName�  R ������ "0 applicationname applicationName� 0 shscript  � 0 	mysuccess 	mySuccess� 0 	mymessage 	myMessage� 
0 errmsg  S 
����
� 
strq
� .sysoexecTEXT���     TEXT� 
0 errmsg  �  � 0��,%E�O �j OeE�O�E�W X  fE�O�E�O��lvOP� �8��UV�� 20 checkaftereffectsstatus checkAfterEffectsStatus�  �  U ���� &0 logfileageseconds logFileAgeSeconds� 0 	mysuccess 	mySuccess� 0 	mymessage 	myMessageV ���^dg�v� 0 getageoffile getAgeOfFile
� 
null
� 
bool� $0 shownotification showNotification� A*b  k+  E�O�� 
 �b  l �& fE�O�E�O*��l+ Y 	eE�O�E�O��lv� ����WX�� *0 recycleaftereffects recycleAfterEffects�  �  W  X ������������ $0 shownotification showNotification� 0 killapp killApp� 

� .sysodelanull��� ��� nmbr� 00 archiveaftereffectslog archiveAfterEffectsLog� 60 launchaftereffectsproject launchAfterEffectsProject� 0*��l+ O*�k+ O�j O*j+ O*��l+ O*b  k+ 
OPascr  ��ޭ