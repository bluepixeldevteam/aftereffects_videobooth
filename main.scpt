FasdUAS 1.101.10   ��   ��    k             l     ��  ��    H B multi-processor oriented Applescript for Pre-Ingestion processing     � 	 	 �   m u l t i - p r o c e s s o r   o r i e n t e d   A p p l e s c r i p t   f o r   P r e - I n g e s t i o n   p r o c e s s i n g   
  
 l     ��������  ��  ��        j     �� �� 0 theplistpath thePListPath  m        �   | / U s e r s / b l u e p i x e l / p r o c e s s _ c o n f i g / v i d e o b o o t h _ m a s t e r _ c o n f i g . p l i s t      j    �� �� *0 aftereffectslogfile afterEffectsLogFile  m       �   l / U s e r s / b l u e p i x e l / D o c u m e n t s / w a i v e r s _ p e n d i n g / l o g / l o g . t x t      j    �� �� 20 aftereffectsprojectfile afterEffectsProjectFile  m       �   � / U s e r s / b l u e p i x e l / g i t / a f t e r e f f e c t s _ v i d e o b o o t h / v i d e o b o o t h _ m a s t e r . a e p      j   	 �� �� 00 incomingfileextensions incomingFileExtensions  J   	         m   	 
 ! ! � " "  . j p g    # $ # m   
  % % � & &  . p n g $  ' ( ' m     ) ) � * *  . m p 4 (  +�� + m     , , � - -  . m o v��     . / . j    �� 0�� 80 rawtranscodefileextensions rawTranscodeFileExtensions 0 J     1 1  2�� 2 m     3 3 � 4 4  . m o v��   /  5 6 5 j    �� 7�� D0  bypassrawtranscodefileextensions  bypassRawTranscodeFileExtensions 7 J     8 8  9 : 9 m     ; ; � < <  . j p g :  = > = m     ? ? � @ @  . p n g >  A�� A m     B B � C C  . m p 4��   6  D E D j    !�� F�� 60 statsintervalcheckminutes statsIntervalCheckMinutes F m     ����  E  G H G j   " $�� I�� $0 processdelaysecs processDelaySecs I m   " #����  H  J K J j   % )�� L�� 0 loopdelaysecs loopDelaySecs L m   % (���� 
 K  M N M j   * ,�� O�� 0 	automated   O m   * +��
�� boovfals N  P Q P j   - /�� R�� 0 devendpoints devEndpoints R m   - .��
�� boovfals Q  S T S j   0 4�� U�� $0 computerusername computerUserName U m   0 3 V V � W W  b l u e p i x e l T  X Y X j   5 9�� Z�� $0 waivergrepstring waiverGrepString Z m   5 8 [ [ � \ \  - e   . x m l Y  ] ^ ] j   : >�� _�� >0 filesizecheck_minaeoutputsize filesizeCheck_minAEOutputSize _ m   : =���� ��@ ^  ` a ` j   ? A�� b�� (0 filesizecheckdelay filesizeCheckDelay b m   ? @����  a  c d c j   B D�� e�� (0 performfilecleanup performFileCleanup e m   B C��
�� boovtrue d  f g f j   E I�� h�� 0 dev_mode   h m   E F��
�� boovfals g  i j i l      k l m k j   J P�� n�� 00 oldestfileagethreshold oldestFileAgeThreshold n m   J M����  l  seconds    m � o o  s e c o n d s j  p q p l      r s t r j   Q W�� u�� 00 expectedmaxprocesstime expectedMaxProcessTime u m   Q T���� � s  seconds    t � v v  s e c o n d s q  w x w j   X ^�� y�� "0 applicationname applicationName y m   X [ z z � { { $ V i d e o b o o t h   P r o c e s s x  | } | l     ��������  ��  ��   }  ~  ~ j   _ e�� ��� $0 maxfilespercycle maxFilesPerCycle � m   _ b���� 
   � � � l     ��������  ��  ��   �  � � � l     ��������  ��  ��   �  � � � p   f f � � ������ 00 pendingaewaiverlistold pendingAEWaiverListOLD��   �  � � � p   f f � � ������ .0 pendingamefilelistold pendingAMEFileListOLD��   �  � � � p   f f � � ������ :0 pendingdropboxwaiverlistold pendingDropboxWaiverListOLD��   �  � � � l     ��������  ��  ��   �  � � � p   f f � � �� ��� "0 dropboxrootpath dropboxRootPath � �� ��� .0 incomingrawxmlwaivers incomingRawXmlWaivers � �� ��� :0 incomingrawmediafolderposix incomingRawMediaFolderPosix � ������ %0 !waivers_pending_completed_archive  ��   �  � � � p   f f � � ������ 0 stationtype stationType��   �  � � � p   f f � � �� ��� :0 transcodependingfolderposix transcodePendingFolderPosix � ������ >0 transcodecompletedfolderposix transcodeCompletedFolderPosix��   �  � � � p   f f � � �� ��� @0 pendingwaiversforaefolderposix pendingWaiversForAEFolderPosix � ������ 0 
media_pool  ��   �  � � � p   f f � � �� ��� 0 computer_id   � ������ 0 ishappy isHappy��   �  � � � p   f f � � �� ��� 0 processed_media   � �� ��� 0 	ae_output   � �� ��� 0 	ame_watch   � ������ 0 spock_ingestor  ��   �  � � � p   f f � � ������ 80 laststatsgatheredtimestamp lastStatsGatheredTimestamp��   �  � � � p   f f � � �� ��� :0 laststalefilechecktimestamp lastStaleFileCheckTimestamp � ������ *0 forcestalefilecheck forceStaleFileCheck��   �  � � � l     ��������  ��  ��   �  � � � l     ��������  ��  ��   �  � � � i   f i � � � I     ������
�� .aevtoappnull  �   � ****��  ��   � k     � � �  � � � I    �� ���
�� .ascrcmnt****      � **** � b      � � � l     ����� � I    ������
�� .misccurdldt    ��� null��  ��  ��  ��   � m     � � � � �  L a u n c h i n g��   �  � � � I    �� ����� $0 shownotification showNotification �  � � � m     � � � � �  L a u n c h i n g �  ��� � m     � � � � � , c h e c k i n g   s y s t e m   s t a t u s��  ��   �  � � � l   ��������  ��  ��   �  � � � Z    8 � ����� � =     � � � I    �� ����� 0 
fileexists 
fileExists �  ��� � o    ���� 0 theplistpath thePListPath��  ��   � m    ��
�� boovfals � k   # 4 � �  � � � I  # .�� ���
�� .sysodisAaleR        TEXT � b   # * � � � m   # $ � � � � � P A b o r t i n g   b e c a u s e   c o n f i g   f i l e   n o t   f o u n d :   � o   $ )���� 0 theplistpath thePListPath��   �  ��� � I  / 4������
�� .aevtquitnull��� ��� null��  ��  ��  ��  ��   �  � � � l  9 9��������  ��  ��   �  � � � r   9 < � � � m   9 :��
�� boovtrue � o      ���� 0 ishappy isHappy �  � � � r   = D � � � I   = B������� 0 setvars setVars��  �   � o      �~�~ 0 isready1 isReady1 �  � � � r   E L � � � I   E J�}�|�{�} 0 setpaths setPaths�|  �{   � o      �z�z 0 isready2 isReady2 �  � � � Z   M z � ��y � � F   M X � � � =  M P � � � o   M N�x�x 0 isready1 isReady1 � m   N O�w
�w boovtrue � =  S V � � � o   S T�v�v 0 isready2 isReady2 � m   T U�u
�u boovtrue � k   [ h � �  � � � I   [ a�t ��s�t 0 gatherstats gatherStats �  ��r � m   \ ]�q�q  �r  �s   �  ��p � I   b h�o ��n�o "0 processonecycle processOneCycle �  �m  m   c d�l
�l boovtrue�m  �n  �p  �y   � I  k z�k�j
�k .sysodisAaleR        TEXT b   k v b   k t b   k p m   k n �		 & A b o r t i n g .   s e t V a r   =   o   n o�i�i 0 isready1 isReady1 m   p s

 �    s e t P a t h s   =   o   t u�h�h 0 isready2 isReady2�j   �  I  { ��g�f
�g .ascrcmnt****      � **** b   { � l  { ��e�d I  { ��c�b�a
�c .misccurdldt    ��� null�b  �a  �e  �d   m   � � � t D o n e   w i t h   i n i t i a l   c y c l e   -   w a i t i n g   3 0 s e c   f o r   o n I d l e   h a n d l e r�f   �` l  � ��_�^�]�_  �^  �]  �`   �  l     �\�[�Z�\  �[  �Z    i   j m I     �Y�X�W
�Y .miscidlenmbr    ��� null�X  �W   k     !  I     �V�U�V "0 processonecycle processOneCycle �T m    �S
�S boovfals�T  �U    !  I   �R"�Q
�R .ascrcmnt****      � ****" b    #$# b    %&% b    '(' l   )�P�O) I   �N�M�L
�N .misccurdldt    ��� null�M  �L  �P  �O  ( m    ** �++ 2 D o n e   w i t h   c y c l e   -   w a i t i n g& o    �K�K 0 loopdelaysecs loopDelaySecs$ m    ,, �-- 
   s e c s�Q  ! .�J. L    !// o     �I�I 0 loopdelaysecs loopDelaySecs�J   010 l     �H�G�F�H  �G  �F  1 232 i   n q454 I     �E�D�C
�E .aevtquitnull��� ��� null�D  �C  5 k     66 787 I    �B9�A
�B .ascrcmnt****      � ****9 b     :;: l    <�@�?< I    �>�=�<
�> .misccurdldt    ��� null�=  �<  �@  �?  ; m    == �>>  E x i t i n g   A p p�A  8 ?@? I    �;A�:�; 0 gatherstats gatherStatsA B�9B m    �8�8  �9  �:  @ C�7C M    DD I     �6�5�4
�6 .aevtquitnull��� ��� null�5  �4  �7  3 EFE l     �3�2�1�3  �2  �1  F GHG i   r uIJI I      �0K�/�0 $0 shownotification showNotificationK LML o      �.�.  0 subtitlestring subtitleStringM N�-N o      �,�, 0 messagestring messageString�-  �/  J k     +OO PQP Z     #RS�+TR >    UVU o     �*�* 0 messagestring messageStringV m    �)
�) 
nullS I   �(WX
�( .sysonotfnull��� ��� TEXTW o    �'�' 0 messagestring messageStringX �&YZ
�& 
apprY o    �%�% "0 applicationname applicationNameZ �$[�#
�$ 
subt[ o    �"�"  0 subtitlestring subtitleString�#  �+  T I   #�!� \
�! .sysonotfnull��� ��� TEXT�   \ �]^
� 
subt] o    ��  0 subtitlestring subtitleString^ �_�
� 
appr_ o    �� "0 applicationname applicationName�  Q `a` l  $ )bcdb I  $ )�e�
� .sysodelanull��� ��� nmbre m   $ %�� �  c ) #allow time for notification to fire   d �ff F a l l o w   t i m e   f o r   n o t i f i c a t i o n   t o   f i r ea g�g l  * *����  �  �  �  H hih l     ����  �  �  i jkj l     ����  �  �  k lml i   v ynon I      �p�� "0 processonecycle processOneCyclep q�q o      �
�
  0 isinitialcycle isInitialCycle�  �  o k    rr sts I    �	u�
�	 .ascrcmnt****      � ****u b     vwv l    x��x I    ���
� .misccurdldt    ��� null�  �  �  �  w m    yy �zz  S t a r t i n g   C y c l e�  t {|{ I    �}�� $0 shownotification showNotification} ~~ m    �� ���  S t a r t i n g   C y c l e �� � m    ��
�� 
null�   �  | ��� l   ��������  ��  ��  � ��� I    ������� 0 gatherstats gatherStats� ���� m    ���� ��  ��  � ��� I     �������� 0 handlewaivers handleWaivers��  ��  � ��� I  ! 2�����
�� .ascrcmnt****      � ****� b   ! .��� b   ! (��� l  ! &������ I  ! &������
�� .misccurdldt    ��� null��  ��  ��  ��  � m   & '�� ��� > D o n e   c h e c k i n g   w a i v e r s .   w a i t i n g  � o   ( -���� $0 processdelaysecs processDelaySecs��  � ��� I  3 <�����
�� .sysodelanull��� ��� nmbr� o   3 8���� $0 processdelaysecs processDelaySecs��  � ��� l  = =��������  ��  ��  � ��� I   = C������� 0 gatherstats gatherStats� ���� m   > ?���� ��  ��  � ��� I   D I�������� :0 transcodeaftereffectsoutput transcodeAfterEffectsOutput��  ��  � ��� l  J J������  � : 4 TODO improve to have variable for clean file output   � ��� h   T O D O   i m p r o v e   t o   h a v e   v a r i a b l e   f o r   c l e a n   f i l e   o u t p u t� ��� I  J [�����
�� .ascrcmnt****      � ****� b   J W��� b   J Q��� l  J O������ I  J O������
�� .misccurdldt    ��� null��  ��  ��  ��  � m   O P�� ��� B d o n e   c h e c k i n g   A E   o u t p u t .   w a i t i n g  � o   Q V���� $0 processdelaysecs processDelaySecs��  � ��� I  \ e�����
�� .sysodelanull��� ��� nmbr� o   \ a���� $0 processdelaysecs processDelaySecs��  � ��� l  f f��������  ��  ��  � ��� I   f l������� 0 gatherstats gatherStats� ���� m   g h���� ��  ��  � ��� I   m r�������� ,0 movefilestoingestion moveFilesToIngestion��  ��  � ��� I  s ������
�� .ascrcmnt****      � ****� b   s ���� b   s z��� l  s x������ I  s x������
�� .misccurdldt    ��� null��  ��  ��  ��  � m   x y�� ��� L d o n e   c h e c k i n g   e n c o d e r   o u t p u t .   w a i t i n g  � o   z ���� $0 processdelaysecs processDelaySecs��  � ��� I  � ������
�� .sysodelanull��� ��� nmbr� o   � ����� $0 processdelaysecs processDelaySecs��  � ��� l  � ���������  ��  ��  � ��� l  � �������  � 0 * clean out the raw .mov from encoder watch   � ��� T   c l e a n   o u t   t h e   r a w   . m o v   f r o m   e n c o d e r   w a t c h� ��� I   � ��������� 0 filecleanup fileCleanup��  ��  � ��� I  � ������
�� .ascrcmnt****      � ****� b   � ���� b   � ���� l  � ������� I  � �������
�� .misccurdldt    ��� null��  ��  ��  ��  � m   � ��� ��� R d o n e   c l e a n i n g   u p   e n c o d e r   s o u r c e .   w a i t i n g  � o   � ����� $0 processdelaysecs processDelaySecs��  � ��� I  � ������
�� .sysodelanull��� ��� nmbr� o   � ����� $0 processdelaysecs processDelaySecs��  � ��� l  � ���������  ��  ��  � ��� I   � �������� 0 gatherstats gatherStats� ���� m   � ����� ��  ��  � ��� I  � ������
�� .ascrcmnt****      � ****� b   � ���� l  � ������� I  � �������
�� .misccurdldt    ��� null��  ��  ��  ��  � m   � ��� ��� $ d o n e   s e n d i n g   s t a t s��  � ��� l  � ���������  ��  ��  � ��� Z   � ������� o   � �����  0 isinitialcycle isInitialCycle� I   � �������� $0 shownotification showNotification� ��� m   � ��� ��� , F i n i s h e d   I n i t i a l   C y c l e� ���� m   � ��� ��� B w a i t i n g   ~ 3 0 s e c   f o r   o n I d l e   h a n d l e r��  ��  ��  � I   � �������� $0 shownotification showNotification� ��� m   � ��� ���  F i n i s h e d   C y c l e� ���� l  � � ����  b   � � b   � � m   � � �  w a i t i n g   o   � ����� 0 loopdelaysecs loopDelaySecs m   � � � $ s e c   f o r   n e x t   c y c l e��  ��  ��  ��  � 	
	 l  � ���������  ��  ��  
  Z   ����� =  � � o   � ����� 0 	automated   m   � ���
�� boovfals k   �  I  �����
�� .ascrcmnt****      � **** b   � l  � ����� I  � �������
�� .misccurdldt    ��� null��  ��  ��  ��   m   �  � j D o n e   w i t h   c y c l e   -   e x i t i n g   b e c a u s e   a u t o m a t i o n   i s   f a l s e��    I ��
�� .sysodisAaleR        TEXT m  	 � j D o n e   w i t h   c y c l e   -   e x i t i n g   b e c a u s e   a u t o m a t i o n   i s   f a l s e �� ��
�� 
givu  m  �� ��   !�~! I �}�|�{
�} .aevtquitnull��� ��� null�|  �{  �~  ��  ��   "�z" l �y�x�w�y  �x  �w  �z  m #$# l     �v�u�t�v  �u  �t  $ %&% l     �s'(�s  ' C =#############################################################   ( �)) z # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #& *+* l     �r,-�r  , ) ### --DO NOT EDIT BELOW THIS LIINE--   - �.. F # #   - - D O   N O T   E D I T   B E L O W   T H I S   L I I N E - -+ /0/ l     �q12�q  1 > 8## --THESE ARE ALL THE HANDLERS USED IN THE LOOP ABOVE--   2 �33 p # #   - - T H E S E   A R E   A L L   T H E   H A N D L E R S   U S E D   I N   T H E   L O O P   A B O V E - -0 454 l     �p67�p  6 C =#############################################################   7 �88 z # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #5 9:9 l     �o�n�m�o  �n  �m  : ;<; i   z }=>= I      �l?�k�l &0 checkpendingfiles checkPendingFiles? @A@ o      �j�j 0 oldlist oldListA B�iB o      �h�h 0 newlist newList�i  �k  > k     �CC DED r     FGF c     HIH J     �g�g  I m    �f
�f 
listG o      �e�e 0 
filesadded 
filesAddedE JKJ r    LML c    NON J    	�d�d  O m   	 
�c
�c 
listM o      �b�b 0 filesremoved filesRemovedK PQP r    RSR m    �a
�a boovfalsS o      �`�` 0 isprocessing isProcessingQ TUT Z    �VW�_XV F    #YZY l   [�^�][ @    \]\ n    ^_^ 1    �\
�\ 
leng_ o    �[�[ 0 newlist newList] n    `a` 1    �Z
�Z 
lenga o    �Y�Y 0 oldlist oldList�^  �]  Z l   !b�X�Wb ?    !cdc n    efe 1    �V
�V 
lengf o    �U�U 0 oldlist oldListd m     �T�T  �X  �W  W k   & �gg hih l  & &�S�R�Q�S  �R  �Q  i jkj l  & &�Plm�P  l   new/old is same and > 0   m �nn 0   n e w / o l d   i s   s a m e   a n d   >   0k opo Z   & 8qr�O�Nq =   & )sts o   & '�M�M 0 newlist newListt o   ' (�L�L 0 oldlist oldListr k   , 4uu vwv l  , ,�Kxy�K  x &   error condition, not processing   y �zz @   e r r o r   c o n d i t i o n ,   n o t   p r o c e s s i n gw {|{ I  , 1�J}�I
�J .ascrcmnt****      � ****} m   , -~~ �  n o t   p r o c e s s i n g�I  | ��H� L   2 4�� m   2 3�G
�G boovfals�H  �O  �N  p ��� l  9 9�F�E�D�F  �E  �D  � ��� Y   9 d��C���B� k   I _�� ��� r   I O��� n   I M��� 4   J M�A�
�A 
cobj� o   K L�@�@ 0 i  � o   I J�?�? 0 oldlist oldList� o      �>�> 0 n  � ��=� Z  P _���<�;� H   P T�� E  P S��� o   P Q�:�: 0 newlist newList� o   Q R�9�9 0 n  � r   W [��� o   W X�8�8 0 n  � n      ���  ;   Y Z� o   X Y�7�7 0 filesremoved filesRemoved�<  �;  �=  �C 0 i  � m   < =�6�6 � I  = D�5��4
�5 .corecnte****       ****� n   = @��� 2  > @�3
�3 
cobj� o   = >�2�2 0 oldlist oldList�4  �B  � ��� l  e e�1�0�/�1  �0  �/  � ��� Y   e ���.���-� k   u ��� ��� r   u {��� n   u y��� 4   v y�,�
�, 
cobj� o   w x�+�+ 0 i  � o   u v�*�* 0 newlist newList� o      �)�) 0 n  � ��(� Z  | ����'�&� H   | ��� E  | ��� o   | }�%�% 0 oldlist oldList� o   } ~�$�$ 0 n  � r   � ���� o   � ��#�# 0 n  � n      ���  ;   � �� o   � ��"�" 0 
filesadded 
filesAdded�'  �&  �(  �. 0 i  � m   h i�!�! � I  i p� ��
�  .corecnte****       ****� n   i l��� 2  j l�
� 
cobj� o   i j�� 0 newlist newList�  �-  � ��� l  � �����  �  �  � ��� I  � ����
� .ascrcmnt****      � ****� c   � ���� b   � ���� m   � ��� ���  f i l e s A d d e d :  � o   � ��� 0 
filesadded 
filesAdded� m   � ��
� 
TEXT�  � ��� I  � ����
� .ascrcmnt****      � ****� b   � ���� m   � ��� ���  f i l e s R e m o v e d :  � o   � ��� 0 filesremoved filesRemoved�  � ��� l  � �����  �  �  � ��� l  � �����  � ( " adding files but not removing any   � ��� D   a d d i n g   f i l e s   b u t   n o t   r e m o v i n g   a n y� ��� Z   � ������ F   � ���� l  � ����� ?   � ���� n   � ���� 1   � ��

�
 
leng� o   � ��	�	 0 
filesadded 
filesAdded� m   � ���  �  �  � l  � ����� =   � ���� n   � ���� 1   � ��
� 
leng� o   � ��� 0 filesremoved filesRemoved� m   � ���  �  �  � k   � ��� ��� l  � �����  � &   error condition, not processing   � ��� @   e r r o r   c o n d i t i o n ,   n o t   p r o c e s s i n g� ��� L   � ��� m   � �� 
�  boovfals�  �  �  � ��� l  � ���������  ��  ��  � ��� l  � �������  � %  adding files AND removing them   � ��� >   a d d i n g   f i l e s   A N D   r e m o v i n g   t h e m� ��� Z   � �������� F   � ���� l  � ������� @   � ���� n   � ���� 1   � ���
�� 
leng� o   � ����� 0 
filesadded 
filesAdded� m   � �����  ��  ��  � l  � ������� ?   � ���� n   � �   1   � ���
�� 
leng o   � ����� 0 filesremoved filesRemoved� m   � �����  ��  ��  � L   � � m   � ���
�� boovtrue��  ��  � �� l  � ���������  ��  ��  ��  �_  X k   � �  l  � �����   %  new/old are both 0 OR old is 0    �		 >   n e w / o l d   a r e   b o t h   0   O R   o l d   i s   0 

 l  � �����   A ; this is where we are when new files are added after a lull    � v   t h i s   i s   w h e r e   w e   a r e   w h e n   n e w   f i l e s   a r e   a d d e d   a f t e r   a   l u l l  I  � �����
�� .ascrcmnt****      � **** m   � � � 0 s k i p p i n g   l i s t   c o m p a r i s o n��   �� r   � � m   � ���
�� boovtrue o      ���� 0 isprocessing isProcessing��  U  l  � ���������  ��  ��    L   � � o   � ����� 0 isprocessing isProcessing �� l  � ���������  ��  ��  ��  <  l     ��������  ��  ��     i   ~ �!"! I      �������� 0 filecleanup fileCleanup��  ��  " k     '## $%$ Z     %&'��(& =    )*) o     ���� (0 performfilecleanup performFileCleanup* m    ��
�� boovtrue' k   
 !++ ,-, r   
 ./. m   
 00 �11 h r m   - r f   / U s e r s / b l u e p i x e l / D o c u m e n t s / a m e _ w a t c h / S o u r c e / */ o      ���� 0 shscript  - 232 Q    45��4 I   ��6��
�� .sysoexecTEXT���     TEXT6 o    ���� 0 shscript  ��  5 R      ��7��
�� .ascrerr ****      � ****7 o      ���� 
0 errmsg  ��  ��  3 898 l     ��������  ��  ��  9 :��: l     ��;<��  ;   DO NOTHING   < �==    D O   N O T H I N G��  ��  ( l  $ $��>?��  >  
do nothing   ? �@@  d o   n o t h i n g% ABA l  & &��������  ��  ��  B C��C l  & &��������  ��  ��  ��    DED l     ��������  ��  ��  E FGF l     ��������  ��  ��  G HIH i   � �JKJ I      �������� ,0 movefilestoingestion moveFilesToIngestion��  ��  K k    �LL MNM r     OPO o     ���� 0 processed_media  P o      ���� 0 myfolder  N QRQ r    	STS n    UVU 1    ��
�� 
psxpV o    ���� 0 processed_media  T o      ���� 0 	pmyfolder  R WXW l  
 
��������  ��  ��  X YZY I  
 ��[��
�� .ascrcmnt****      � ****[ b   
 \]\ l  
 ^����^ I  
 ������
�� .misccurdldt    ��� null��  ��  ��  ��  ] m    __ �``   s t a r t i n g   u p l o a d s��  Z aba r    cdc b    efe b    ghg m    ii �jj  l s  h n    klk 1    ��
�� 
strql o    ���� 0 	pmyfolder  f m    mm �nn    |   g r e p   ' . m p 4 'd o      ���� 0 shscript  b opo l     ��������  ��  ��  p qrq Q     Pstus r   # .vwv c   # ,xyx n   # *z{z 2  ( *��
�� 
cpar{ l  # (|����| I  # (��}��
�� .sysoexecTEXT���     TEXT} o   # $���� 0 shscript  ��  ��  ��  y m   * +��
�� 
listw o      ���� 0 filelist  t R      ��~��
�� .ascrerr ****      � ****~ o      ���� 
0 errmsg  ��  u k   6 P ��� I  6 C�����
�� .ascrcmnt****      � ****� b   6 ?��� b   6 =��� l  6 ;������ I  6 ;������
�� .misccurdldt    ��� null��  ��  ��  ��  � m   ; <�� ���  u p l o a d   e r r o r :  � o   = >���� 
0 errmsg  ��  � ��� I  D I�����
�� .ascrcmnt****      � ****� m   D E�� ��� z i f   e r r o r   1   i t   p r o b a b l y   m e a n s   t h e r e   a r e   n o   f i l e s   t o   u p l o a d   y e t��  � ���� r   J P��� c   J N��� J   J L����  � m   L M��
�� 
list� o      ���� 0 filelist  ��  r ��� l  Q Q��������  ��  ��  � ��� l  Q T���� r   Q T��� m   Q R����  � o      ���� 0 
cyclecount 
cycleCount� . (limit how many files we do on each cycle   � ��� P l i m i t   h o w   m a n y   f i l e s   w e   d o   o n   e a c h   c y c l e� ��� X   U������ k   g��� ��� Z   g v������ ?   g n��� o   g h�~�~ 0 
cyclecount 
cycleCount� o   h m�}�} $0 maxfilespercycle maxFilesPerCycle�  S   q r��  �  � ��� l  w w�|�{�z�|  �{  �z  � ��� r   w ���� c   w ~��� b   w z��� o   w x�y�y 0 myfolder  � o   x y�x�x 0 i  � m   z }�w
�w 
TEXT� o      �v�v 
0 myfile  � ��� r   � ���� o   � ��u�u 0 i  � o      �t�t 0 newfile  � ��� l  � ��s�r�q�s  �r  �q  � ��� l  � ��p�o�n�p  �o  �n  � ��� Z   �����m�l� =  � ���� I   � ��k��j�k ,0 checkfilewritestatus checkFileWriteStatus� ��� o   � ��i�i 
0 myfile  � ��� m   � ��h�h  B@� ��g� m   � ��f�f �g  �j  � m   � ��e
�e boovtrue� k   ���� ��� l  � ��d���d  � , &upload to Dropbox folder for ingestion   � ��� L u p l o a d   t o   D r o p b o x   f o l d e r   f o r   i n g e s t i o n� ��� r   � ���� b   � ���� b   � ���� b   � ���� b   � ���� m   � ��� ���  m v  � n   � ���� 1   � ��c
�c 
strq� o   � ��b�b 
0 myfile  � m   � ��� ���   � n   � ���� 1   � ��a
�a 
strq� o   � ��`�` 0 spock_ingestor  � o   � ��_�_ 0 newfile  � o      �^�^ 0 shscript  � ��� l  � ��]�\�[�]  �\  �[  � ��� Q   � ����� k   � ��� ��� I  � ��Z��Y
�Z .ascrcmnt****      � ****� b   � ���� l  � ���X�W� I  � ��V�U�T
�V .misccurdldt    ��� null�U  �T  �X  �W  � m   � ��� ���  s t a r t i n g   u p l o a d�Y  � ��S� I  � ��R��Q
�R .sysoexecTEXT���     TEXT� o   � ��P�P 0 shscript  �Q  �S  � R      �O��N
�O .ascrerr ****      � ****� o      �M�M 
0 errmsg  �N  � k   � ��� ��� I  � ��L��K
�L .ascrcmnt****      � ****� b   � ���� b   � ���� l  � ���J�I� I  � ��H�G�F
�H .misccurdldt    ��� null�G  �F  �J  �I  � m   � ��� ��� ( e r r o r   i n   u p l o a d i n g :  � o   � ��E�E 
0 errmsg  �K  � � � I  � ��D�C�B
�D .sysobeepnull��� ��� long�C  �B     I  � ��A�@�?
�A .sysobeepnull��� ��� long�@  �?    I  � ��>�=�<
�> .sysobeepnull��� ��� long�=  �<    I  � ��;
�; .sysodisAaleR        TEXT b   � �	
	 m   � � � > A n   e r r o r   o c c u r e d   i n   u p l o a d i n g :  
 o   � ��:�: 
0 errmsg   �9�8
�9 
givu m   � ��7�7 �8   �6  S   � ��6  �  l  � ��5�4�3�5  �4  �3    l  � ��2�2   6 0cleanup intermediate raw files to save HDD space    � ` c l e a n u p   i n t e r m e d i a t e   r a w   f i l e s   t o   s a v e   H D D   s p a c e  r   � I   �	�1�0�1 0 	changeext 	changeExt  c   � o   � ��/�/ 0 i   m   ��.
�. 
TEXT �- m     �!!  �-  �0   o      �,�, 0 basefilename baseFilename "#" r  !$%$ b  &'& b  ()( b  *+* b  ,-, m  .. �//  l s  - n  010 1  �+
�+ 
strq1 o  �*�* 0 
media_pool  + m  22 �33    |   g r e p   - e  ) o  �)�) 0 basefilename baseFilename' m  44 �55  *% o      �(�( 0 shscript  # 676 Q  "V89:8 r  %0;<; c  %.=>= n  %,?@? 2 *,�'
�' 
cpar@ l %*A�&�%A I %*�$B�#
�$ .sysoexecTEXT���     TEXTB o  %&�"�" 0 shscript  �#  �&  �%  > m  ,-�!
�! 
list< o      � �  $0 intermediatelist intermediateList9 R      �C�
� .ascrerr ****      � ****C o      �� 
0 errmsg  �  : k  8VDD EFE I 8G�G�
� .ascrcmnt****      � ****G b  8CHIH b  8AJKJ l 8=L��L I 8=���
� .misccurdldt    ��� null�  �  �  �  K m  =@MM �NN  u p l o a d   e r r o r :  I o  AB�� 
0 errmsg  �  F OPO I HO�Q�
� .ascrcmnt****      � ****Q m  HKRR �SS z i f   e r r o r   1   i t   p r o b a b l y   m e a n s   t h e r e   a r e   n o   f i l e s   t o   u p l o a d   y e t�  P T�T r  PVUVU c  PTWXW J  PR��  X m  RS�
� 
listV o      �� $0 intermediatelist intermediateList�  7 YZY l WW����  �  �  Z [\[ X  W�]�^] k  i�__ `a` r  i~bcb c  i|ded b  ixfgf b  ivhih b  irjkj m  illl �mm  r m  k n  lqnon 1  oq�

�
 
strqo o  lo�	�	 0 
media_pool  i m  rupp �qq  /g o  vw�� 0 i  e m  x{�
� 
TEXTc o      �� 0 shscript  a rsr Q  �tuvt I ���w�
� .sysoexecTEXT���     TEXTw o  ���� 0 shscript  �  u R      �x�
� .ascrerr ****      � ****x o      � �  
0 errmsg  �  v I ����y��
�� .ascrcmnt****      � ****y b  ��z{z b  ��|}| l ��~����~ I ��������
�� .misccurdldt    ��� null��  ��  ��  ��  } m  �� ��� 8 i n t e r m e d i a t e   c l e a n u p   e r r o r :  { o  ������ 
0 errmsg  ��  s ���� l ����������  ��  ��  ��  � 0 i  ^ o  Z[���� $0 intermediatelist intermediateList\ ��� l ����������  ��  ��  � ��� r  ����� [  ����� o  ������ 0 
cyclecount 
cycleCount� m  ������ � o      ���� 0 
cyclecount 
cycleCount� ��� l ����������  ��  ��  � ���� l ��������  �  fileReady end if   � ���   f i l e R e a d y   e n d   i f��  �m  �l  � ���� l ����������  ��  ��  ��  �� 0 i  � o   X Y���� 0 filelist  � ��� I �������
�� .ascrcmnt****      � ****� b  ����� l �������� I ��������
�� .misccurdldt    ��� null��  ��  ��  ��  � m  ���� ��� V d o n e   w i t h   u p l o a d i n g .   W a i t i n g   f o r   n e x t   c y c l e��  � ���� l ����������  ��  ��  ��  I ��� l     ��������  ��  ��  � ��� l     ��������  ��  ��  � ��� i   � ���� I      �������� :0 transcodeaftereffectsoutput transcodeAfterEffectsOutput��  ��  � k    O�� ��� l      ������  � 7 1 
TODO improve to take varaible for clean files 
   � ��� b   
 T O D O   i m p r o v e   t o   t a k e   v a r a i b l e   f o r   c l e a n   f i l e s   
� ��� r     ��� o     ���� 0 	ae_output  � o      ���� 0 myfolder  � ��� r    ��� c    ��� n    	��� 1    	��
�� 
psxp� l   ������ b    ��� o    ���� @0 pendingwaiversforaefolderposix pendingWaiversForAEFolderPosix� m    �� ���  c o m p l e t e /��  ��  � m   	 
��
�� 
TEXT� o      ���� 0 completed_waivers  � ��� l   ��������  ��  ��  � ��� I   �����
�� .ascrcmnt****      � ****� b    ��� l   ������ I   ������
�� .misccurdldt    ��� null��  ��  ��  ��  � m    �� ��� & s t a r t i n g   t r a n s c o d e s��  � ��� r    #��� b    !��� b    ��� m    �� ���  l s  � n    ��� 1    ��
�� 
strq� o    ���� 0 completed_waivers  � m     �� ���    |   g r e p   - e   . x m l� o      ���� 0 shscript  � ��� l  $ $��������  ��  ��  � ��� Q   $ T���� r   ' 2��� c   ' 0��� n   ' .��� 2  , .��
�� 
cpar� l  ' ,������ I  ' ,�����
�� .sysoexecTEXT���     TEXT� o   ' (���� 0 shscript  ��  ��  ��  � m   . /��
�� 
list� o      ���� 0 filelist  � R      �����
�� .ascrerr ****      � ****� o      ���� 
0 errmsg  ��  � k   : T�� ��� I  : E�����
�� .ascrcmnt****      � ****� b   : A��� l  : ?������ I  : ?������
�� .misccurdldt    ��� null��  ��  ��  ��  � o   ? @���� 
0 errmsg  ��  � ��� I  F M�����
�� .ascrcmnt****      � ****� m   F I�� ��� > p r o b a b l y   b e c a u s e   n o   w a i v e r s   y e t��  � ���� r   N T��� c   N R��� J   N P����  � m   P Q��
�� 
list� o      ���� 0 filelist  ��  � ��� l  U U��������  ��  ��  � ��� l  U X���� r   U X��� m   U V����  � o      ���� 0 
cyclecount 
cycleCount� . (limit how many files we do on each cycle   � ��� P l i m i t   h o w   m a n y   f i l e s   w e   d o   o n   e a c h   c y c l e� ��� X   Y?����� k   m:    l  m m��������  ��  ��    r   m z I   m x������ 0 	changeext 	changeExt 	 l  n q
����
 c   n q o   n o���� 0 i   m   o p��
�� 
TEXT��  ��  	 �� m   q t �  . m o v��  ��   o      ����  0 outputfilename outputFilename  l  { {����   Q K commenting this out, was used for Netflix to make an alternate deliverable    � �   c o m m e n t i n g   t h i s   o u t ,   w a s   u s e d   f o r   N e t f l i x   t o   m a k e   a n   a l t e r n a t e   d e l i v e r a b l e  l  { {����   H B set outputFilenameClean to changeExt((i as string), "_clean.mov")    � �   s e t   o u t p u t F i l e n a m e C l e a n   t o   c h a n g e E x t ( ( i   a s   s t r i n g ) ,   " _ c l e a n . m o v " )  l  { {��������  ��  ��    Z   { ����� ?   { � !  o   { |���� 0 
cyclecount 
cycleCount! o   | ����� $0 maxfilespercycle maxFilesPerCycle  S   � ���  ��   "#" l  � ���������  ��  ��  # $%$ l  � ��&'�  &  check for finished file   ' �(( . c h e c k   f o r   f i n i s h e d   f i l e% )*) r   � �+,+ m   � ��~�~  , o      �}�} 0 	filesize1  * -.- r   � �/0/ m   � ��|�| 0 o      �{�{ 0 	filesize2  . 121 r   � �343 m   � ��z�z  4 o      �y�y  0 filesize1clean filesize1Clean2 565 r   � �787 m   � ��x�x 8 o      �w�w  0 filesize2clean filesize2Clean6 9:9 l  � ��v�u�t�v  �u  �t  : ;<; r   � �=>= m   � ��s
�s boovfals> o      �r�r 0 skipthisfile skipThisFile< ?@? r   � �ABA m   � ��q
�q boovfalsB o      �p�p &0 skipthisfileclean skipThisFileClean@ CDC l  � ��o�n�m�o  �n  �m  D EFE T   �zGG k   �uHH IJI I  � ��lK�k
�l .ascrcmnt****      � ****K b   � �LML l  � �N�j�iN I  � ��h�g�f
�h .misccurdldt    ��� null�g  �f  �j  �i  M m   � �OO �PP $ c h e c k i n g   f i l e   s i z e�k  J QRQ r   � �STS c   � �UVU b   � �WXW o   � ��e�e 0 myfolder  X o   � ��d�d  0 outputfilename outputFilenameV m   � ��c
�c 
TEXTT o      �b�b 
0 myfile  R YZY r   � �[\[ l  � �]�a�`] I  � ��_^�^
�_ .rdwrgeofcomp       ****^ 4   � ��]_
�] 
psxf_ o   � ��\�\ 
0 myfile  �^  �a  �`  \ o      �[�[ 0 	filesize1  Z `a` I  � ��Zb�Y
�Z .ascrcmnt****      � ****b b   � �cdc b   � �efe l  � �g�X�Wg I  � ��V�U�T
�V .misccurdldt    ��� null�U  �T  �X  �W  f m   � �hh �ii  f i l e   s i z e   =  d o   � ��S�S 0 	filesize1  �Y  a jkj Z   � �lm�R�Ql ?   � �non o   � ��P�P 0 	filesize1  o o   � ��O�O >0 filesizecheck_minaeoutputsize filesizeCheck_minAEOutputSizem k   � �pp qrq I  � ��Ns�M
�N .ascrcmnt****      � ****s m   � �tt �uu * f i l e s i z e 1   >   c h e c k s i z e�M  r v�Lv I  � ��Kw�J
�K .sysodelanull��� ��� nmbrw m   � ��I�I �J  �L  �R  �Q  k xyx r   �z{z l  �|�H�G| I  ��F}�E
�F .rdwrgeofcomp       ****} 4   � ��D~
�D 
psxf~ o   � ��C�C 
0 myfile  �E  �H  �G  { o      �B�B 0 	filesize2  y � I �A��@
�A .ascrcmnt****      � ****� b  ��� b  ��� l 	��?�>� I 	�=�<�;
�= .misccurdldt    ��� null�<  �;  �?  �>  � m  	�� ���  f i l e   s i z e   =  � o  �:�: 0 	filesize2  �@  � ��� l �9�8�7�9  �8  �7  � ��� l �6���6  � % , filesizeCheck_minAEOutputSize   � ��� > ,   f i l e s i z e C h e c k _ m i n A E O u t p u t S i z e� ��� Z  ;���5�4� F  %��� l ��3�2� =  ��� o  �1�1 0 	filesize1  � o  �0�0 0 	filesize2  �3  �2  � l !��/�.� ?  !��� o  �-�- 0 	filesize2  � o   �,�, >0 filesizecheck_minaeoutputsize filesizeCheck_minAEOutputSize�/  �.  � k  (7�� ��� I (5�+��*
�+ .ascrcmnt****      � ****� b  (1��� l (-��)�(� I (-�'�&�%
�' .misccurdldt    ��� null�&  �%  �)  �(  � m  -0�� ��� B f i l e   s i z e s   m a t c h   -   e x i t i n g   r e p e a t�*  � ��$�  S  67�$  �5  �4  � ��� I <O�#��"
�# .ascrcmnt****      � ****� b  <K��� b  <E��� l <A��!� � I <A���
� .misccurdldt    ��� null�  �  �!  �   � m  AD�� ��� 4 f i l e   n o t   d o n e   -   d i f f e r e n c e� l EJ���� c  EJ��� l EH���� \  EH��� o  EF�� 0 	filesize2  � o  FG�� 0 	filesize1  �  �  � m  HI�
� 
TEXT�  �  �"  � ��� l PP����  �  �  � ��� Z  Pu����� G  Pa��� A  PW��� o  PQ�� 0 	filesize1  � o  QV�� >0 filesizecheck_minaeoutputsize filesizeCheck_minAEOutputSize� l Z]���� A  Z]��� o  Z[�� 0 	filesize1  � o  [\�
�
 0 	filesize2  �  �  � k  dq�� ��� I dk�	��
�	 .ascrcmnt****      � ****� m  dg�� ��� \ f i l e s i z e 1   <   c h e c k s i z e   o r   f i l e s i z e 1   <   f i l e s i z e 2�  � ��� l ll����  � %  still rendering, skip this one   � ��� >   s t i l l   r e n d e r i n g ,   s k i p   t h i s   o n e� ��� r  lo��� m  lm�
� boovtrue� o      �� 0 skipthisfile skipThisFile� ���  S  pq�  �  �  �  F ��� l {{����  �  �  � ��� l {{� ���   � Q K commenting this out, was used for Netflix to make an alternate deliverable   � ��� �   c o m m e n t i n g   t h i s   o u t ,   w a s   u s e d   f o r   N e t f l i x   t o   m a k e   a n   a l t e r n a t e   d e l i v e r a b l e� ��� l  {{������  �B<
		repeat			log (current date) & "checking clean file size"			set myfile to myfolder & outputFilenameClean as string			set filesize1Clean to (get eof of POSIX file myfile)			log (current date) & "Clean file size = " & filesize1Clean			if filesize1Clean > filesizeCheck_minAEOutputSize then				log "filesize1Clean > checksize"				delay 1			end if			set filesize2Clean to (get eof of POSIX file myfile)			log (current date) & "Clean file size = " & filesize2Clean						--, filesizeCheck_minAEOutputSize			if (filesize1Clean = filesize2Clean) and (filesize2 > filesizeCheck_minAEOutputSize) then				log (current date) & "file sizes match - exiting repeat"				exit repeat			end if			log (current date) & "Clean file not done - difference" & ((filesize2Clean - filesize1Clean) as string)						if filesize1Clean < filesizeCheck_minAEOutputSize or (filesize1Clean < filesize2Clean) then				log "filesize1Clean < checksize or filesize1Clean < filesize2Clean"				-- still rendering, skip this one				set skipThisFileClean to true				exit repeat			end if		end repeat
		   � ���x 
 	 	 r e p e a t  	 	 	 l o g   ( c u r r e n t   d a t e )   &   " c h e c k i n g   c l e a n   f i l e   s i z e "  	 	 	 s e t   m y f i l e   t o   m y f o l d e r   &   o u t p u t F i l e n a m e C l e a n   a s   s t r i n g  	 	 	 s e t   f i l e s i z e 1 C l e a n   t o   ( g e t   e o f   o f   P O S I X   f i l e   m y f i l e )  	 	 	 l o g   ( c u r r e n t   d a t e )   &   " C l e a n   f i l e   s i z e   =   "   &   f i l e s i z e 1 C l e a n  	 	 	 i f   f i l e s i z e 1 C l e a n   >   f i l e s i z e C h e c k _ m i n A E O u t p u t S i z e   t h e n  	 	 	 	 l o g   " f i l e s i z e 1 C l e a n   >   c h e c k s i z e "  	 	 	 	 d e l a y   1  	 	 	 e n d   i f  	 	 	 s e t   f i l e s i z e 2 C l e a n   t o   ( g e t   e o f   o f   P O S I X   f i l e   m y f i l e )  	 	 	 l o g   ( c u r r e n t   d a t e )   &   " C l e a n   f i l e   s i z e   =   "   &   f i l e s i z e 2 C l e a n  	 	 	  	 	 	 - - ,   f i l e s i z e C h e c k _ m i n A E O u t p u t S i z e  	 	 	 i f   ( f i l e s i z e 1 C l e a n   =   f i l e s i z e 2 C l e a n )   a n d   ( f i l e s i z e 2   >   f i l e s i z e C h e c k _ m i n A E O u t p u t S i z e )   t h e n  	 	 	 	 l o g   ( c u r r e n t   d a t e )   &   " f i l e   s i z e s   m a t c h   -   e x i t i n g   r e p e a t "  	 	 	 	 e x i t   r e p e a t  	 	 	 e n d   i f  	 	 	 l o g   ( c u r r e n t   d a t e )   &   " C l e a n   f i l e   n o t   d o n e   -   d i f f e r e n c e "   &   ( ( f i l e s i z e 2 C l e a n   -   f i l e s i z e 1 C l e a n )   a s   s t r i n g )  	 	 	  	 	 	 i f   f i l e s i z e 1 C l e a n   <   f i l e s i z e C h e c k _ m i n A E O u t p u t S i z e   o r   ( f i l e s i z e 1 C l e a n   <   f i l e s i z e 2 C l e a n )   t h e n  	 	 	 	 l o g   " f i l e s i z e 1 C l e a n   <   c h e c k s i z e   o r   f i l e s i z e 1 C l e a n   <   f i l e s i z e 2 C l e a n "  	 	 	 	 - -   s t i l l   r e n d e r i n g ,   s k i p   t h i s   o n e  	 	 	 	 s e t   s k i p T h i s F i l e C l e a n   t o   t r u e  	 	 	 	 e x i t   r e p e a t  	 	 	 e n d   i f  	 	 e n d   r e p e a t 
 	 	� ��� l {{��������  ��  ��  � ��� l {{��������  ��  ��  � ��� Z  {8������� F  {���� = {~��� o  {|���� 0 skipthisfile skipThisFile� m  |}��
�� boovfals� = ����� o  ������ &0 skipthisfileclean skipThisFileClean� m  ����
�� boovfals� k  �4�� ��� l ��������  � - 'move AE output file to AME watch folder   � ��� N m o v e   A E   o u t p u t   f i l e   t o   A M E   w a t c h   f o l d e r� ��� l ��������  � !  move XML waiver to archive   � ��� 6   m o v e   X M L   w a i v e r   t o   a r c h i v e� ��� l ����������  ��  ��  � ��� r  ����� b  ����� b  ��� � b  �� b  �� b  �� m  �� �  m v   n  ��	
	 1  ����
�� 
strq
 o  ������ 0 myfolder   o  ������  0 outputfilename outputFilename m  �� �    /  n  �� 1  ����
�� 
strq o  ������ 0 	ame_watch  � o  ������  0 outputfilename outputFilename� o      ���� 0 	shscript1  �  l ������   Q K commenting this out, was used for Netflix to make an alternate deliverable    � �   c o m m e n t i n g   t h i s   o u t ,   w a s   u s e d   f o r   N e t f l i x   t o   m a k e   a n   a l t e r n a t e   d e l i v e r a b l e  l ������   �  set shscript2 to "mv " & quoted form of myfolder & outputFilenameClean & " /" & quoted form of ame_watch & outputFilenameClean    � �   s e t   s h s c r i p t 2   t o   " m v   "   &   q u o t e d   f o r m   o f   m y f o l d e r   &   o u t p u t F i l e n a m e C l e a n   &   "   / "   &   q u o t e d   f o r m   o f   a m e _ w a t c h   &   o u t p u t F i l e n a m e C l e a n  r  �� b  �� b  ��  b  ��!"! b  ��#$# b  ��%&% m  ��'' �((  m v  & n  ��)*) 1  ����
�� 
strq* o  ������ 0 completed_waivers  $ o  ������ 0 i  " m  ��++ �,,     n  ��-.- 1  ����
�� 
strq. o  ������ %0 !waivers_pending_completed_archive   o  ������ 0 i   o      ���� 0 	shscript3   /0/ Z  ��12����1 = ��343 o  ������ 0 dev_mode  4 m  ����
�� boovtrue2 I ����5��
�� .sysodlogaskr        TEXT5 o  ������ 0 shscript  ��  ��  ��  0 676 Q  �289:8 k  ��;; <=< I ����>��
�� .ascrcmnt****      � ****> b  ��?@? l ��A����A I ��������
�� .misccurdldt    ��� null��  ��  ��  ��  @ m  ��BB �CC : m o v i n g   i   t o   A M E   w a t c h   f o l d e r  ��  = DED I ����F��
�� .sysoexecTEXT���     TEXTF o  ������ 0 	shscript1  ��  E GHG l ����IJ��  I Q K commenting this out, was used for Netflix to make an alternate deliverable   J �KK �   c o m m e n t i n g   t h i s   o u t ,   w a s   u s e d   f o r   N e t f l i x   t o   m a k e   a n   a l t e r n a t e   d e l i v e r a b l eH LML l ����NO��  N    do shell script shscript2   O �PP 4   d o   s h e l l   s c r i p t   s h s c r i p t 2M QRQ I ����S��
�� .sysoexecTEXT���     TEXTS o  ������ 0 	shscript3  ��  R TUT r  ��VWV [  ��XYX o  ������ 0 
cyclecount 
cycleCountY m  ������ W o      ���� 0 
cyclecount 
cycleCountU Z��Z l ����������  ��  ��  ��  9 R      ��[��
�� .ascrerr ****      � ****[ o      ���� 
0 errmsg  ��  : k  �2\\ ]^] I ���_��
�� .ascrcmnt****      � ****_ b  �
`a` b  �bcb l �d����d I �������
�� .misccurdldt    ��� null��  ��  ��  ��  c m  ee �ff ( e r r o r   i n   u p l o a d i n g :  a o  	���� 
0 errmsg  ��  ^ ghg I ������
�� .sysobeepnull��� ��� long��  ��  h iji I ������
�� .sysobeepnull��� ��� long��  ��  j klk I  ������
�� .sysobeepnull��� ��� long��  ��  l mnm I !0��op
�� .sysodisAaleR        TEXTo b  !&qrq m  !$ss �tt > A n   e r r o r   o c c u r e d   i n   u p l o a d i n g :  r o  $%���� 
0 errmsg  p ��u��
�� 
givuu m  ),���� ��  n v��v  S  12��  7 w��w l 33��������  ��  ��  ��  ��  ��  � x��x l 99��������  ��  ��  ��  �� 0 i  � o   \ ]���� 0 filelist  � yzy I @M��{��
�� .ascrcmnt****      � ****{ b  @I|}| l @E~����~ I @E������
�� .misccurdldt    ��� null��  ��  ��  ��  } m  EH ��� * d o n e   w i t h   t r a n s c o d i n g��  z ���� l NN��������  ��  ��  ��  � ��� l     ��������  ��  ��  � ��� l     ��������  ��  ��  � ��� l     ��������  ��  ��  � ��� l     ��������  ��  ��  � ��� i   � ���� I      �������� 0 handlewaivers handleWaivers��  ��  � k    W�� ��� I    �����
�� .ascrcmnt****      � ****� b     ��� l    ������ I    ��~�}
� .misccurdldt    ��� null�~  �}  ��  ��  � m    �� ���   c h e c k i n g   w a i v e r s��  � ��� l   �|�{�z�|  �{  �z  � ��� r    ��� o    �y�y .0 incomingrawxmlwaivers incomingRawXmlWaivers� o      �x�x 60 incomingwaiverfolderposix incomingWaiverFolderPosix� ��� l   �w�v�u�w  �v  �u  � ��� l   �t���t  �  assemble greps	   � ���  a s s e m b l e   g r e p s 	� ��� r    ��� b    ��� b    ��� b    ��� m    �� ���  l s  � n    ��� 1    �s
�s 
strq� o    �r�r 60 incomingwaiverfolderposix incomingWaiverFolderPosix� m    �� ���    |   g r e p  � o    �q�q $0 waivergrepstring waiverGrepString� o      �p�p 0 shscript  � ��� l     �o�n�m�o  �n  �m  � ��� Z     3���l�k� =    '��� o     %�j�j 0 dev_mode  � m   % &�i
�i boovtrue� I  * /�h��g
�h .sysodlogaskr        TEXT� o   * +�f�f 0 shscript  �g  �l  �k  � ��� l  4 4�e�d�c�e  �d  �c  � ��� l  4 4�b���b  �  get list of waivers   � ��� & g e t   l i s t   o f   w a i v e r s� ��� l  4 4�a���a  � 2 ,grep -e Added -e Changed -e Fixed -e Deleted   � ��� X g r e p   - e   A d d e d   - e   C h a n g e d   - e   F i x e d   - e   D e l e t e d� ��� Q   4 b���� r   7 B��� c   7 @��� n   7 >��� 2  < >�`
�` 
cpar� l  7 <��_�^� I  7 <�]��\
�] .sysoexecTEXT���     TEXT� o   7 8�[�[ 0 shscript  �\  �_  �^  � m   > ?�Z
�Z 
list� o      �Y�Y  0 waiverfilelist waiverFileList� R      �X��W
�X .ascrerr ****      � ****� o      �V�V 
0 errmsg  �W  � k   J b�� ��� I  J U�U��T
�U .ascrcmnt****      � ****� b   J Q��� l  J O��S�R� I  J O�Q�P�O
�Q .misccurdldt    ��� null�P  �O  �S  �R  � o   O P�N�N 
0 errmsg  �T  � ��� I  V [�M��L
�M .ascrcmnt****      � ****� m   V W�� ��� > p r o b a b l y   b e c a u s e   n o   w a i v e r s   y e t�L  � ��K� r   \ b��� c   \ `��� J   \ ^�J�J  � m   ^ _�I
�I 
list� o      �H�H  0 waiverfilelist waiverFileList�K  � ��� l  c c�G�F�E�G  �F  �E  � ��� l  c c�D���D  � N H look for incoming source files and move them and waiver into production   � ��� �   l o o k   f o r   i n c o m i n g   s o u r c e   f i l e s   a n d   m o v e   t h e m   a n d   w a i v e r   i n t o   p r o d u c t i o n� ��� l  c f���� r   c f��� m   c d�C�C  � o      �B�B 0 
cyclecount 
cycleCount� . (limit how many files we do on each cycle   � ��� P l i m i t   h o w   m a n y   f i l e s   w e   d o   o n   e a c h   c y c l e� ��� l  g g�A�@�?�A  �@  �?  � ��� X   gU��>�� k   wP�� ��� Z   w � �=�<  ?   w ~ o   w x�;�; 0 
cyclecount 
cycleCount o   x }�:�: $0 maxfilespercycle maxFilesPerCycle  S   � ��=  �<  �  l  � ��9�8�7�9  �8  �7    l  � ��6	�6   G ANeed to read the waiver and see if it contains multiple raw files   	 �

 � N e e d   t o   r e a d   t h e   w a i v e r   a n d   s e e   i f   i t   c o n t a i n s   m u l t i p l e   r a w   f i l e s  l  � ��5�5   - ' we also have access here to PATE info     � N   w e   a l s o   h a v e   a c c e s s   h e r e   t o   P A T E   i n f o    r   � � b   � � o   � ��4�4 60 incomingwaiverfolderposix incomingWaiverFolderPosix o   � ��3�3 0 outteri outterI o      �2�2 0 
waiverfile 
waiverFile  r   � � I   � ��1�0�1 0 read_waiver   �/ o   � ��.�. 0 
waiverfile 
waiverFile�/  �0   o      �-�- 0 mywaiver myWaiver  l  � ��,�+�*�,  �+  �*    r   � � !  J   � ��)�)  ! o      �(�( 0 rawfileslist rawFilesList "#" r   � �$%$ J   � ��'�'  % o      �&�& 0 movefileslist moveFilesList# &'& l  � ��%�$�#�%  �$  �#  ' ()( Z   � �*+�",* F   � �-.- l  � �/�!� / >  � �010 n   � �232 o   � ��� 0 waiver_total_raw_files  3 o   � ��� 0 mywaiver myWaiver1 m   � ��
� 
null�!  �   . l  � �4��4 ?   � �565 n   � �787 o   � ��� 0 waiver_total_raw_files  8 o   � ��� 0 mywaiver myWaiver6 m   � ���  �  �  + k   � �99 :;: r   � �<=< m   � ���  = o      �� 0 myincrementer myIncrementer; >�> U   � �?@? k   � �AA BCB r   � �DED [   � �FGF o   � ��� 0 myincrementer myIncrementerG m   � ��� E o      �� 0 myincrementer myIncrementerC HIH l  � ��JK�  J    does this need try/catch?   K �LL 4   d o e s   t h i s   n e e d   t r y / c a t c h ?I MNM r   � �OPO I   � ��Q�� 0 get_element  Q RSR o   � ��� 0 
waiverfile 
waiverFileS TUT m   � �VV �WW  J o bU X�X l  � �Y��Y b   � �Z[Z m   � �\\ �]]  r a w _ v i d e o _[ o   � ��
�
 0 myincrementer myIncrementer�  �  �  �  P o      �	�	 0 	myrawfile 	myRawFileN ^�^ s   � �_`_ o   � ��� 0 	myrawfile 	myRawFile` l     a��a n      bcb  ;   � �c o   � ��� 0 rawfileslist rawFilesList�  �  �  @ l  � �d��d n   � �efe o   � ��� 0 waiver_total_raw_files  f o   � �� �  0 mywaiver myWaiver�  �  �  �"  , k   � �gg hih l  � ���jk��  j O I no raw files defined in waiver, infer raw filename from waiver file name   k �ll �   n o   r a w   f i l e s   d e f i n e d   i n   w a i v e r ,   i n f e r   r a w   f i l e n a m e   f r o m   w a i v e r   f i l e   n a m ei mnm l  � ���op��  o O I just look for single raw file, we will strip .xml extension in next step   p �qq �   j u s t   l o o k   f o r   s i n g l e   r a w   f i l e ,   w e   w i l l   s t r i p   . x m l   e x t e n s i o n   i n   n e x t   s t e pn r��r s   � �sts o   � ����� 0 outteri outterIt l     u����u n      vwv  ;   � �w o   � ����� 0 rawfileslist rawFilesList��  ��  ��  ) xyx l  � ���������  ��  ��  y z{z l  � ���������  ��  ��  { |}| l  � ���~��  ~ 2 , see if all media files are ready to process    ��� X   s e e   i f   a l l   m e d i a   f i l e s   a r e   r e a d y   t o   p r o c e s s} ��� Z   �N������ I   � �������� (0 checkrawfilestatus checkRawFileStatus� ���� o   � ����� 0 rawfileslist rawFilesList��  ��  � k   �J�� ��� l  � �������  � 3 - files are ready, move waiver into production   � ��� Z   f i l e s   a r e   r e a d y ,   m o v e   w a i v e r   i n t o   p r o d u c t i o n� ��� r   ���� b   ���� b   �
��� b   ���� b   ���� m   � ��� ���  m v  � n   ���� 1   ���
�� 
strq� o   � ����� 0 
waiverfile 
waiverFile� m  �� ���   � o  	���� @0 pendingwaiversforaefolderposix pendingWaiversForAEFolderPosix� l 
������ c  
��� o  
���� 0 outteri outterI� m  ��
�� 
TEXT��  ��  � o      ���� 0 shscript  � ��� l ������  �  display dialog shscript   � ��� . d i s p l a y   d i a l o g   s h s c r i p t� ��� l ��������  ��  ��  � ��� r  ��� m  ��
�� boovfals� o      ���� 0 success  � ���� Q  J���� k  )�� ��� I �����
�� .sysoexecTEXT���     TEXT� o  ���� 0 shscript  ��  � ��� r   %��� [   #��� o   !���� 0 
cyclecount 
cycleCount� m  !"���� � o      ���� 0 
cyclecount 
cycleCount� ��� l &&��������  ��  ��  � ���� r  &)��� m  &'��
�� boovtrue� o      ���� 0 success  ��  � R      �����
�� .ascrerr ****      � ****� o      ���� 
0 errmsg  ��  � k  1J�� ��� I 1@�����
�� .ascrcmnt****      � ****� b  1<��� b  1:��� l 16������ I 16������
�� .misccurdldt    ��� null��  ��  ��  ��  � m  69�� ��� ` e r r o r   i n   m o v i n g   w a i v e r   f i l e   t o   w a i v e r s _ p e n d i n g :  � o  :;���� 
0 errmsg  ��  � ���� I AJ�����
�� .sysodisAaleR        TEXT� b  AF��� m  AD�� ��� ` e r r o r   i n   m o v i n g   w a i v e r   f i l e   t o   w a i v e r s _ p e n d i n g :  � o  DE���� 
0 errmsg  ��  ��  ��  ��  � l MM������  � 8 2 waiver not ready, we'll check again on next cycle   � ��� d   w a i v e r   n o t   r e a d y ,   w e ' l l   c h e c k   a g a i n   o n   n e x t   c y c l e� ��� l OO��������  ��  ��  � ��� l OO��������  ��  ��  � ���� l OO������  �  outterI   � ���  o u t t e r I��  �> 0 outteri outterI� o   j k����  0 waiverfilelist waiverFileList� ��� l VV��������  ��  ��  � ��� l VV��������  ��  ��  � ��� l VV��������  ��  ��  � ���� l VV��������  ��  ��  ��  � ��� l     ��������  ��  ��  � ��� l     ��������  ��  ��  � ��� i   � ���� I      ������� (0 checkrawfilestatus checkRawFileStatus� ���� o      ���� 0 rawfileslist rawFilesList��  ��  � k    )�� ��� l     ������  � * $ check if files are done transcoding   � ��� H   c h e c k   i f   f i l e s   a r e   d o n e   t r a n s c o d i n g� ��� l     ������  � E ? if they are not found, check for them in incoming media folder   � ��� ~   i f   t h e y   a r e   n o t   f o u n d ,   c h e c k   f o r   t h e m   i n   i n c o m i n g   m e d i a   f o l d e r� ��� l     ������  � 5 / returns true if all files are done transcoding   � ��� ^   r e t u r n s   t r u e   i f   a l l   f i l e s   a r e   d o n e   t r a n s c o d i n g� � � l     ��������  ��  ��     l     ����   b \ if first file is not found in transcoded, just move on to checking raw path for all of them    � �   i f   f i r s t   f i l e   i s   n o t   f o u n d   i n   t r a n s c o d e d ,   j u s t   m o v e   o n   t o   c h e c k i n g   r a w   p a t h   f o r   a l l   o f   t h e m  Z     	���� =    

 o     ���� 0 dev_mode   m    ��
�� boovtrue	 I  
 ����
�� .sysodlogaskr        TEXT b   
  m   
  �  r a w F i l e s L i s t   =   o    ���� 0 rawfileslist rawFilesList��  ��  ��    r     m    ��
�� boovfals o      ���� "0 filereadystatus fileReadyStatus  r     m    ��
�� boovfals o      ����  0 skiptranscoded skipTranscoded  r    " J     ����   o      ���� 0 movefileslist moveFilesList  l  # #��������  ��  ��     l  # #��������  ��  ��    !"! X   #=#��$# k   38%% &'& r   3 >()( I  3 <��*��
�� .corecnte****       ***** n   3 8+,+ 2  6 8��
�� 
cha , l  3 6-����- c   3 6./. o   3 4���� 0 i  / m   4 5�
� 
TEXT��  ��  ��  ) o      �~�~ 0 
namelength  ' 010 r   ? P232 c   ? N454 l  ? L6�}�|6 n   ? L787 7 @ L�{9:
�{ 
cha 9 m   D F�z�z : l  G K;�y�x; \   G K<=< o   H I�w�w 0 
namelength  = m   I J�v�v �y  �x  8 o   ? @�u�u 0 i  �}  �|  5 m   L M�t
�t 
TEXT3 o      �s�s 
0 myname  1 >?> l  Q Q�r�q�p�r  �q  �p  ? @A@ l  Q Q�o�n�m�o  �n  �m  A BCB l  Q Q�lDE�l  D 8 2check for transcoded media for any valid extension   E �FF d c h e c k   f o r   t r a n s c o d e d   m e d i a   f o r   a n y   v a l i d   e x t e n s i o nC GHG X   Q6I�kJI k   e1KK LML r   e pNON c   e nPQP b   e lRSR b   e jTUT b   e hVWV m   e fXX �YY  /W o   f g�j�j >0 transcodecompletedfolderposix transcodeCompletedFolderPosixU o   h i�i�i 
0 myname  S o   j k�h�h 0 fileextension fileExtensionQ m   l m�g
�g 
TEXTO o      �f�f $0 mytranscodedfile myTranscodedFileM Z[Z r   q |\]\ c   q z^_^ b   q x`a` b   q vbcb b   q tded m   q rff �gg  /e o   r s�e�e :0 incomingrawmediafolderposix incomingRawMediaFolderPosixc o   t u�d�d 
0 myname  a o   v w�c�c 0 fileextension fileExtension_ m   x y�b
�b 
TEXT] o      �a�a 0 	myrawfile 	myRawFile[ hih r   } �jkj o   } ~�`�` 0 fileextension fileExtensionk o      �_�_  0 foundextension foundExtensioni lml l  � ��^�]�\�^  �]  �\  m non Z   � �pq�[�Zp I   � ��Yr�X�Y ,0 checkfilewritestatus checkFileWriteStatusr sts o   � ��W�W $0 mytranscodedfile myTranscodedFilet uvu m   � ��V�V�v w�Uw m   � ��T�T �U  �X  q k   � �xx yzy r   � �{|{ m   � ��S
�S boovtrue| o      �R�R "0 filereadystatus fileReadyStatusz }~} s   � �� c   � ���� l  � ���Q�P� b   � ���� o   � ��O�O 
0 myname  � o   � ��N�N  0 foundextension foundExtension�Q  �P  � m   � ��M
�M 
TEXT� l     ��L�K� n      ���  ;   � �� o   � ��J�J 0 movefileslist moveFilesList�L  �K  ~ ��I�  S   � ��I  �[  �Z  o ��� l  � ��H�G�F�H  �G  �F  � ��� l  � ��E���E  � 4 . file not found in transcoded, check it in raw   � ��� \   f i l e   n o t   f o u n d   i n   t r a n s c o d e d ,   c h e c k   i t   i n   r a w� ��� Z   �/���D�C� I   � ��B��A�B ,0 checkfilewritestatus checkFileWriteStatus� ��� o   � ��@�@ 0 	myrawfile 	myRawFile� ��� m   � ��?�?  � ��>� m   � ��=�= �>  �A  � k   �+�� ��� r   � ���� m   � ��<
�< boovfals� o      �;�; "0 filereadystatus fileReadyStatus� ��� l  � ��:���:  � ( "move raw file to Transcoding chain   � ��� D m o v e   r a w   f i l e   t o   T r a n s c o d i n g   c h a i n� ��� Z   � �����9� E   � ���� o   � ��8�8 80 rawtranscodefileextensions rawTranscodeFileExtensions� o   � ��7�7  0 foundextension foundExtension� r   � ���� b   � ���� b   � ���� b   � ���� b   � ���� m   � ��� ���  m v  � n   � ���� 1   � ��6
�6 
strq� o   � ��5�5 0 	myrawfile 	myRawFile� m   � ��� ���   � o   � ��4�4 :0 transcodependingfolderposix transcodePendingFolderPosix� l  � ���3�2� b   � ���� o   � ��1�1 
0 myname  � o   � ��0�0  0 foundextension foundExtension�3  �2  � o      �/�/ 0 shscript  � ��� E   � ���� o   � ��.�. D0  bypassrawtranscodefileextensions  bypassRawTranscodeFileExtensions� o   � ��-�-  0 foundextension foundExtension� ��,� r   � ���� b   � ���� b   � ���� b   � ���� b   � ���� m   � ��� ���  m v  � n   � ���� 1   � ��+
�+ 
strq� o   � ��*�* 0 	myrawfile 	myRawFile� m   � ��� ���   � o   � ��)�) >0 transcodecompletedfolderposix transcodeCompletedFolderPosix� l  � ���(�'� b   � ���� o   � ��&�& 
0 myname  � o   � ��%�%  0 foundextension foundExtension�(  �'  � o      �$�$ 0 shscript  �,  �9  � ��� r   � ���� m   � ��#
�# boovfals� o      �"�" 0 success  � ��� Q   �)���� k   ��� ��� I  � ��!�� 
�! .sysoexecTEXT���     TEXT� o   � ��� 0 shscript  �   � ��� r   � ��� m   � ��
� boovtrue� o      �� $0 filemove_success fileMove_success� ���  S  � ��� l ����  �  �  �  � R      ���
� .ascrerr ****      � ****� o      �� 
0 errmsg  �  � k  )�� ��� I ���
� .ascrcmnt****      � ****� b  ��� b  ��� l ���� I ���
� .misccurdldt    ��� null�  �  �  �  � m  �� ��� ` e r r o r   i n   m o v i n g   m e d i a   f i l e   t o   m e d i a _ t r a n s c o d e d :  � o  �� 
0 errmsg  �  � ��� I %���
� .sysodisAaleR        TEXT� b  !��� m  �� ��� ` e r r o r   i n   m o v i n g   m e d i a   f i l e   t o   m e d i a _ t r a n s c o d e d :  � o   �� 
0 errmsg  �  � ��
� r  &)�	 � m  &'�	
�	 boovfals	  o      �� $0 filemove_success fileMove_success�
  � 			 l **����  �  �  	 			 l **����  �  �  	 	�	 l **� 		�   	  exists rawFile end if   	 �		 * e x i s t s   r a w F i l e   e n d   i f�  �D  �C  � 		��		 l 00��	
	��  	
  file extensions repeat   	 �		 , f i l e   e x t e n s i o n s   r e p e a t��  �k 0 fileextension fileExtensionJ o   T Y���� 00 incomingfileextensions incomingFileExtensionsH 			 l 77��������  ��  ��  	 			 l 77��������  ��  ��  	 	��	 l 77��		��  	  outter repeat in filelist   	 �		 2 o u t t e r   r e p e a t   i n   f i l e l i s t��  �� 0 i  $ o   & '���� 0 rawfileslist rawFilesList" 			 l >>��������  ��  ��  	 			 l >>��		��  	 U Oif all files are found in transcoded, check their status and move to media_pool   	 �		 � i f   a l l   f i l e s   a r e   f o u n d   i n   t r a n s c o d e d ,   c h e c k   t h e i r   s t a t u s   a n d   m o v e   t o   m e d i a _ p o o l	 			 Z  >$		��	 	 F  >S	!	"	! = >A	#	$	# o  >?���� "0 filereadystatus fileReadyStatus	$ m  ?@��
�� boovtrue	" l DO	%����	% =  DO	&	'	& n  DI	(	)	( 1  EI��
�� 
leng	) o  DE���� 0 movefileslist moveFilesList	' n  IN	*	+	* 1  JN��
�� 
leng	+ o  IJ���� 0 rawfileslist rawFilesList��  ��  	 k  V	,	, 	-	.	- X  V	/��	0	/ k  f�	1	1 	2	3	2 Z  f	4	5����	4 = fm	6	7	6 o  fk���� 0 dev_mode  	7 m  kl��
�� boovtrue	5 I p{��	8��
�� .sysodlogaskr        TEXT	8 c  pw	9	:	9 b  pu	;	<	; m  ps	=	= �	>	>   m o v e F i l e s L i s t   =  	< o  st���� 0 movefileslist moveFilesList	: m  uv��
�� 
TEXT��  ��  ��  	3 	?	@	? r  ��	A	B	A c  ��	C	D	C b  ��	E	F	E b  ��	G	H	G m  ��	I	I �	J	J  /	H o  ������ >0 transcodecompletedfolderposix transcodeCompletedFolderPosix	F o  ������ 0 rawfilei rawFileI	D m  ����
�� 
TEXT	B o      ���� 
0 myfile  	@ 	K	L	K l ����������  ��  ��  	L 	M	N	M r  ��	O	P	O n  ��	Q	R	Q 1  ����
�� 
psxp	R o  ������ 
0 myfile  	P o      ���� 0 pmyfile  	N 	S	T	S l ����	U	V��  	U F @TODO refactor this to use incoming_media_ext from config file!!!   	V �	W	W � T O D O   r e f a c t o r   t h i s   t o   u s e   i n c o m i n g _ m e d i a _ e x t   f r o m   c o n f i g   f i l e ! ! !	T 	X	Y	X r  ��	Z	[	Z b  ��	\	]	\ b  ��	^	_	^ b  ��	`	a	` b  ��	b	c	b m  ��	d	d �	e	e  m v  	c n  ��	f	g	f 1  ����
�� 
strq	g o  ������ 0 pmyfile  	a m  ��	h	h �	i	i   	_ o  ������ 0 
media_pool  	] l ��	j����	j o  ������ 0 rawfilei rawFileI��  ��  	[ o      ���� 0 shscript  	Y 	k	l	k r  ��	m	n	m m  ����
�� boovfals	n o      ���� 0 success  	l 	o	p	o Z  ��	q	r����	q = ��	s	t	s o  ������ 0 dev_mode  	t m  ����
�� boovtrue	r I ����	u��
�� .sysodlogaskr        TEXT	u o  ������ 0 shscript  ��  ��  ��  	p 	v	w	v Q  ��	x	y	z	x k  ��	{	{ 	|	}	| I ����	~��
�� .sysoexecTEXT���     TEXT	~ o  ������ 0 shscript  ��  	} 	��	 r  ��	�	�	� m  ����
�� boovtrue	� o      ���� "0 filereadystatus fileReadyStatus��  	y R      ��	���
�� .ascrerr ****      � ****	� o      ���� 
0 errmsg  ��  	z k  ��	�	� 	�	�	� I ����	���
�� .ascrcmnt****      � ****	� b  ��	�	�	� b  ��	�	�	� l ��	�����	� I ��������
�� .misccurdldt    ��� null��  ��  ��  ��  	� m  ��	�	� �	�	� T e r r o r   i n   m o v i n g   m e d i a   f i l e   t o   m e d i a _ p o o l :  	� o  ������ 
0 errmsg  ��  	� 	�	�	� I ����	���
�� .sysodisAaleR        TEXT	� b  ��	�	�	� m  ��	�	� �	�	� T e r r o r   i n   m o v i n g   m e d i a   f i l e   t o   m e d i a _ p o o l :  	� o  ������ 
0 errmsg  ��  	� 	�	�	� r  ��	�	�	� m  ����
�� boovfals	� o      ���� "0 filereadystatus fileReadyStatus	� 	���	�  S  ����  	w 	���	� l ����������  ��  ��  ��  �� 0 rawfilei rawFileI	0 o  YZ���� 0 movefileslist moveFilesList	. 	�	�	� L  	�	� o  ���� "0 filereadystatus fileReadyStatus	� 	���	� l ��������  ��  ��  ��  ��  	  k  
$	�	� 	�	�	� l 

��	�	���  	� 3 - files not ready or move to media_pool failed   	� �	�	� Z   f i l e s   n o t   r e a d y   o r   m o v e   t o   m e d i a _ p o o l   f a i l e d	� 	�	�	� Z  
	�	�����	� = 
	�	�	� o  
���� 0 dev_mode  	� m  ��
�� boovtrue	� I ��	���
�� .sysodlogaskr        TEXT	� m  	�	� �	�	� . N o t   a l l   f i l e s   a r e   r e a d y��  ��  ��  	� 	�	�	� L   "	�	� m   !��
�� boovfals	� 	���	� l ##��	�	���  	�  fileReadyStatus   	� �	�	�  f i l e R e a d y S t a t u s��  	 	�	�	� l %%��������  ��  ��  	� 	�	�	� l %%��������  ��  ��  	� 	�	�	� l %%��������  ��  ��  	� 	�	�	� L  %'	�	� o  %&���� "0 filereadystatus fileReadyStatus	� 	�	�	� l ((��������  ��  ��  	� 	�	�	� l ((��������  ��  ��  	� 	���	� l ((��������  ��  ��  ��  � 	�	�	� l     ��������  ��  ��  	� 	�	�	� l     ����~��  �  �~  	� 	�	�	� l     �}�|�{�}  �|  �{  	� 	�	�	� i   � �	�	�	� I      �z	��y�z ,0 checkfilewritestatus checkFileWriteStatus	� 	�	�	� o      �x�x 0 pmyfile  	� 	�	�	� o      �w�w "0 expectedminsize expectedMinSize	� 	��v	� o      �u�u &0 checkdelayseconds checkDelaySeconds�v  �y  	� k     k	�	� 	�	�	� r     	�	�	� m     �t
�t boovfals	� o      �s�s  0 thisfilestatus thisFileStatus	� 	�	�	� l   �r�q�p�r  �q  �p  	� 	�	�	� r    	�	�	� m    �o�o  	� o      �n�n 0 	filesize1  	� 	�	�	� r    	�	�	� m    	�m�m 	� o      �l�l 0 	filesize2  	� 	�	�	� Z   	�	��k�j	� =   	�	�	� o    �i�i "0 expectedminsize expectedMinSize	� m    �h
�h 
null	� r    	�	�	� m    �g�g 	� o      �f�f "0 expectedminsize expectedMinSize�k  �j  	� 	�	�	� l   �e�d�c�e  �d  �c  	� 	�	�	� Z    h	�	��b	�	� I     �a	��`�a 0 
fileexists 
fileExists	� 	��_	� o    �^�^ 0 pmyfile  �_  �`  	� k   # c	�	� 	�	�	� r   # -	�	�	� l  # +	��]�\	� I  # +�[	��Z
�[ .rdwrgeofcomp       ****	� 4   # '�Y	�
�Y 
psxf	� o   % &�X�X 0 pmyfile  �Z  �]  �\  	� o      �W�W 0 	filesize1  	� 	�	�	� Z  . B	�	��V�U	� l  . 9	��T�S	� G   . 9
 

  l  . 1
�R�Q
 A   . 1


 o   . /�P�P 0 	filesize1  
 m   / 0�O�O �R  �Q  
 l  4 7
�N�M
 A   4 7


 o   4 5�L�L 0 	filesize1  
 o   5 6�K�K "0 expectedminsize expectedMinSize�N  �M  �T  �S  	� L   < >

 m   < =�J
�J boovfals�V  �U  	� 
	


	 l  C C�I�H�G�I  �H  �G  

 


 I  C H�F
�E
�F .sysodelanull��� ��� nmbr
 o   C D�D�D &0 checkdelayseconds checkDelaySeconds�E  
 


 r   I S


 l  I Q
�C�B
 I  I Q�A
�@
�A .rdwrgeofcomp       ****
 4   I M�?

�? 
psxf
 o   K L�>�> 0 pmyfile  �@  �C  �B  
 o      �=�= 0 	filesize2  
 


 Z   T a

�<

 =   T W


 o   T U�;�; 0 	filesize1  
 o   U V�:�: 0 	filesize2  
 L   Z \

 m   Z [�9
�9 boovtrue�<  
 L   _ a

 m   _ `�8
�8 boovfals
 
�7
 l  b b�6�5�4�6  �5  �4  �7  �b  	� k   f h

 
 
!
  l  f f�3
"
#�3  
"   file does not exist   
# �
$
$ (   f i l e   d o e s   n o t   e x i s t
! 
%�2
% L   f h
&
& m   f g�1
�1 boovfals�2  	� 
'
(
' l  i i�0�/�.�0  �/  �.  
( 
)
*
) l  i i�-�,�+�-  �,  �+  
* 
+�*
+ L   i k
,
, o   i j�)�)  0 thisfilestatus thisFileStatus�*  	� 
-
.
- l     �(�'�&�(  �'  �&  
. 
/
0
/ i   � �
1
2
1 I      �%
3�$�% 0 gatherstats gatherStats
3 
4�#
4 o      �"�" 0 intervalmins intervalMins�#  �$  
2 k    	x
5
5 
6
7
6 l     �!
8
9�!  
8 &   only send stats every X minutes   
9 �
:
: @   o n l y   s e n d   s t a t s   e v e r y   X   m i n u t e s
7 
;
<
; r     
=
>
= m     � 
�  boovfals
> o      �� 0 
initialrun 
initialRun
< 
?
@
? Z    G
A
B�
C
A ?    
D
E
D o    �� 0 intervalmins intervalMins
E m    ��  
B k   
 -
F
F 
G
H
G r   
 
I
J
I I   
 ���� 0 getepochtime getEpochTime�  �  
J o      �� 0 timenow timeNow
H 
K
L
K r     
M
N
M I    �
O�� 0 number_to_string  
O 
P�
P [    
Q
R
Q o    �� 80 laststatsgatheredtimestamp lastStatsGatheredTimestamp
R l   
S��
S ]    
T
U
T ]    
V
W
V o    �� 0 intervalmins intervalMins
W m    �� <
U m    ��  ���  �  �  �  
N o      �� 0 	nextcheck 	nextCheck
L 
X�
X Z   ! -
Y
Z��
Y ?   ! $
[
\
[ o   ! "�
�
 0 	nextcheck 	nextCheck
\ o   " #�	�	 0 timenow timeNow
Z L   ' )��  �  �  �  �  
C k   0 G
]
] 
^
_
^ l  0 0�
`
a�  
` A ;making initial run, need to init lastStatsGatheredTimestamp   
a �
b
b v m a k i n g   i n i t i a l   r u n ,   n e e d   t o   i n i t   l a s t S t a t s G a t h e r e d T i m e s t a m p
_ 
c
d
c r   0 7
e
f
e I   0 5���� 0 getepochtime getEpochTime�  �  
f o      �� 80 laststatsgatheredtimestamp lastStatsGatheredTimestamp
d 
g
h
g r   8 ?
i
j
i I   8 =��� � 0 getepochtime getEpochTime�  �   
j o      ���� :0 laststalefilechecktimestamp lastStaleFileCheckTimestamp
h 
k
l
k r   @ C
m
n
m m   @ A��
�� boovfals
n o      ���� *0 forcestalefilecheck forceStaleFileCheck
l 
o��
o r   D G
p
q
p m   D E��
�� boovtrue
q o      ���� 0 
initialrun 
initialRun��  
@ 
r
s
r l  H H��������  ��  ��  
s 
t
u
t l  H H��
v
w��  
v %  set var for our overall health   
w �
x
x >   s e t   v a r   f o r   o u r   o v e r a l l   h e a l t h
u 
y
z
y r   H K
{
|
{ m   H I��
�� boovtrue
| o      ���� 0 ishappy isHappy
z 
}
~
} r   L O

�
 m   L M��
�� boovfals
� o      ���� 40 updatestalefiletimestamp updateStaleFileTimestamp
~ 
�
�
� r   P S
�
�
� m   P Q����  
� o      ���� 0 oldestfileage oldestFileAge
� 
�
�
� r   T W
�
�
� m   T U
�
� �
�
�  
� o      ���� 0 	dataitems 	dataItems
� 
�
�
� r   X [
�
�
� m   X Y
�
� �
�
�  
� o      ���� 0 failedtests failedTests
� 
�
�
� r   \ _
�
�
� m   \ ]��
�� boovfals
� o      ���� *0 recycleaftereffects recycleAfterEffects
� 
�
�
� l  ` `��������  ��  ��  
� 
�
�
� l  ` `��������  ��  ��  
� 
�
�
� r   ` c
�
�
� m   ` a
�
� �
�
� @ { " n a m e " : " t e s t i n g " , " v a l u e " : " f o o " }
� o      ���� 0 	firstitem 	firstItem
� 
�
�
� l  d d��������  ��  ��  
� 
�
�
� l  d d��
�
���  
�   check if AE is running   
� �
�
� .   c h e c k   i f   A E   i s   r u n n i n g
� 
�
�
� r   d l
�
�
� I   d j��
����� 0 
is_running  
� 
���
� m   e f
�
� �
�
�  A f t e r   E f f e c t s��  ��  
� o      ���� 0 ae_test  
� 
�
�
� I   m y��
����� 0 
writeplist 
writePlist
� 
�
�
� o   n s���� 0 theplistpath thePListPath
� 
�
�
� m   s t
�
� �
�
�  a e _ t e s t
� 
���
� o   t u���� 0 ae_test  ��  ��  
� 
�
�
� Z   z �
�
���
�
� =  z }
�
�
� o   z {���� 0 ae_test  
� m   { |��
�� boovfals
� k   � �
�
� 
�
�
� r   � �
�
�
� m   � ���
�� boovfals
� o      ���� 0 ishappy isHappy
� 
�
�
� r   � �
�
�
� b   � �
�
�
� o   � ����� 0 failedtests failedTests
� m   � �
�
� �
�
�  a e _ s t a t u s ,  
� o      ���� 0 failedtests failedTests
� 
�
�
� Z   � �
�
�����
� =  � �
�
�
� o   � ����� 0 dev_mode  
� m   � ���
�� boovtrue
� I  � ���
���
�� .sysodlogaskr        TEXT
� m   � �
�
� �
�
� & N O T   H A P P Y   a e _ s t a t u s��  ��  ��  
� 
�
�
� r   � �
�
�
� m   � �
�
� �
�
� F { " n a m e " : " a e _ s t a t u s " , " v a l u e " : " D O W N " }
� o      ���� 0 	firstitem 	firstItem
� 
�
�
� r   � �
�
�
� m   � ���
�� boovtrue
� o      ���� *0 recycleaftereffects recycleAfterEffects
� 
���
� l  � ���������  ��  ��  ��  ��  
� r   � �
�
�
� m   � �
�
� �
�
� B { " n a m e " : " a e _ s t a t u s " , " v a l u e " : " U P " }
� o      ���� 0 	firstitem 	firstItem
� 
�
�
� r   � �
�
�
� b   � �
�
�
� o   � ����� 0 	dataitems 	dataItems
� o   � ����� 0 	firstitem 	firstItem
� o      ���� 0 	dataitems 	dataItems
� 
�
�
� l  � ���������  ��  ��  
� 
�
�
� l  � ���
�
���  
�   check if AE is hung up   
� �
�
� .   c h e c k   i f   A E   i s   h u n g   u p
� 
�
�
� r   � �
�
�
� c   � �
�
�
� I   � ��������� 20 checkaftereffectsstatus checkAfterEffectsStatus��  ��  
� m   � ���
�� 
list
� o      ���� 00 aftereffectsresponsive afterEffectsResponsive
� 
�
�
� Z   � �
�
�����
� =  � �
�
�
� n   � �
�
�
� 4   � ���
�
�� 
cobj
� m   � ����� 
� o   � ����� 00 aftereffectsresponsive afterEffectsResponsive
� m   � ���
�� boovfals
� k   � �    l  � �����   1 + recycle it after we send the status report    � V   r e c y c l e   i t   a f t e r   w e   s e n d   t h e   s t a t u s   r e p o r t  r   � �	 m   � ���
�� boovtrue	 o      ���� *0 recycleaftereffects recycleAfterEffects 
��
 r   � � b   � � o   � ����� 0 failedtests failedTests m   � � � 4 r e s t a r t i n g _ a f t e r _ e f f e c t s ,   o      ���� 0 failedtests failedTests��  ��  ��  
�  l  � ���������  ��  ��    l  � ���������  ��  ��    l  � �����   ( " check if Media Encoder is running    � D   c h e c k   i f   M e d i a   E n c o d e r   i s   r u n n i n g  r   � � I   � ������� 0 
is_running   �� m   � �   �!! & A d o b e   M e d i a   E n c o d e r��  ��   o      ���� 0 ame_test   "#" I   � ���$���� 0 
writeplist 
writePlist$ %&% o   � ����� 0 theplistpath thePListPath& '(' m   � �)) �**  a m e _ t e s t( +��+ o   � ����� 0 ame_test  ��  ��  # ,-, Z   �0./��0. =  � �121 o   � ����� 0 ame_test  2 m   � ���
�� boovfals/ k  (33 454 r  676 m  ��
�� boovfals7 o      ���� 0 ishappy isHappy5 898 r  :;: b  
<=< o  ���� 0 failedtests failedTests= m  	>> �??  a m e _ s t a t u s ,  ; o      ���� 0 failedtests failedTests9 @A@ Z  "BC����B = DED o  ���� 0 dev_mode  E m  ��
�� boovtrueC I ��F��
�� .sysodlogaskr        TEXTF m  GG �HH ( N O T   H A P P Y   a m e _ s t a t u s��  ��  ��  A I��I r  #(JKJ m  #&LL �MM H { " n a m e " : " a m e _ s t a t u s " , " v a l u e " : " D O W N " }K o      ���� 0 nextitem nextItem��  ��  0 r  +0NON m  +.PP �QQ D { " n a m e " : " a m e _ s t a t u s " , " v a l u e " : " U P " }O o      ���� 0 nextitem nextItem- RSR r  1:TUT b  18VWV b  16XYX o  12���� 0 	dataitems 	dataItemsY m  25ZZ �[[  ,W o  67���� 0 nextitem nextItemU o      ���� 0 	dataitems 	dataItemsS \]\ l ;;��������  ��  ��  ] ^_^ l ;;��`a��  ` "  check if Dropbox is running   a �bb 8   c h e c k   i f   D r o p b o x   i s   r u n n i n g_ cdc r  ;Eefe I  ;C��g���� 0 
is_running  g h��h m  <?ii �jj  D r o p b o x��  ��  f o      ���� 0 dropbox_test  d klk I  FT�m�~� 0 
writeplist 
writePlistm non o  GL�}�} 0 theplistpath thePListPatho pqp m  LOrr �ss  d r o p b o x _ t e s tq t�|t o  OP�{�{ 0 dropbox_test  �|  �~  l uvu Z  U�wx�zyw = UXz{z o  UV�y�y 0 dropbox_test  { m  VW�x
�x boovfalsx k  [�|| }~} r  [^� m  [\�w
�w boovfals� o      �v�v 0 ishappy isHappy~ ��� r  _f��� b  _d��� o  _`�u�u 0 failedtests failedTests� m  `c�� ���   d r o p b o x _ s t a t u s ,  � o      �t�t 0 failedtests failedTests� ��� Z  g|���s�r� = gn��� o  gl�q�q 0 dev_mode  � m  lm�p
�p boovtrue� I qx�o��n
�o .sysodlogaskr        TEXT� m  qt�� ��� 0 N O T   H A P P Y   d r o p b o x _ s t a t u s�n  �s  �r  � ��m� r  }���� m  }��� ��� P { " n a m e " : " d r o p b o x _ s t a t u s " , " v a l u e " : " D O W N " }� o      �l�l 0 nextitem nextItem�m  �z  y r  ����� m  ���� ��� L { " n a m e " : " d r o p b o x _ s t a t u s " , " v a l u e " : " U P " }� o      �k�k 0 nextitem nextItemv ��� r  ����� b  ����� b  ����� o  ���j�j 0 	dataitems 	dataItems� m  ���� ���  ,� o  ���i�i 0 nextitem nextItem� o      �h�h 0 	dataitems 	dataItems� ��� l ���g�f�e�g  �f  �e  � ��� l ���d�c�b�d  �c  �b  � ��� l ���a���a  � &  check pending waivers (AE queue)   � ��� @ c h e c k   p e n d i n g   w a i v e r s   ( A E   q u e u e )� ��� r  ����� b  ����� b  ����� m  ���� ���  l s  � n  ����� 1  ���`
�` 
strq� o  ���_�_ @0 pendingwaiversforaefolderposix pendingWaiversForAEFolderPosix� m  ���� ���    |   g r e p   ' . x m l '� o      �^�^ 0 shscript  � ��� Q  ������ r  ����� c  ����� n  ����� 2 ���]
�] 
cpar� l ����\�[� I ���Z��Y
�Z .sysoexecTEXT���     TEXT� o  ���X�X 0 shscript  �Y  �\  �[  � m  ���W
�W 
list� o      �V�V 0 mylist myList� R      �U��T
�U .ascrerr ****      � ****� o      �S�S 
0 errmsg  �T  � k  ���� ��� I ���R��Q
�R .ascrcmnt****      � ****� b  ����� b  ����� l ����P�O� I ���N�M�L
�N .misccurdldt    ��� null�M  �L  �P  �O  � m  ���� ��� 2 p r o b a b l y   n o   w a i v e r s   y e t :  � o  ���K�K 
0 errmsg  �Q  � ��J� r  ����� J  ���I�I  � o      �H�H 0 mylist myList�J  � ��� l ���G�F�E�G  �F  �E  � ��� r  ����� n  ����� 1  ���D
�D 
leng� o  ���C�C 0 mylist myList� o      �B�B 0 mycount myCount� ��� r  ����� I  ���A��@�A 0 	readplist 	readPlist� ��� o  ���?�? 0 theplistpath thePListPath� ��� m  ���� ���   a e _ w a i v e r _ c o u n t 1� ��>� m  ���=
�= 
null�>  �@  � o      �<�< 0 oldcount oldCount� ��� r  ���� \  ���� o  � �;�; 0 mycount myCount� o   �:�: 0 oldcount oldCount� o      �9�9 0 ae_waiver_delta  � ��� I  	�8��7�8 0 
writeplist 
writePlist� ��� o  
�6�6 0 theplistpath thePListPath� ��� m  �� ���  a e _ w a i v e r _ d e l t a� ��5� o  �4�4 0 ae_waiver_delta  �5  �7  � ��� I  *�3��2�3 0 
writeplist 
writePlist� � � o   �1�1 0 theplistpath thePListPath   m   # �   a e _ w a i v e r _ c o u n t 1 �0 o  #&�/�/ 0 mycount myCount�0  �2  �  l ++�.�-�,�.  �-  �,   	 l ++�+
�+  
   see if files are moving	    � 2   s e e   i f   f i l e s   a r e   m o v i n g 		  r  +2 I  +0�*�)�(�* 0 getepochtime getEpochTime�)  �(   o      �'�' 0 timenow timeNow  r  3C I  3A�&�%�& 0 number_to_string   �$ [  4= o  45�#�# :0 laststalefilechecktimestamp lastStaleFileCheckTimestamp l 5<�"�! ]  5< o  5:� �  00 expectedmaxprocesstime expectedMaxProcessTime m  :;��  ���"  �!  �$  �%   o      �� 0 	nextcheck 	nextCheck  Z  D��  G  D[!"! G  DQ#$# l DG%��% A  DG&'& o  DE�� 0 	nextcheck 	nextCheck' o  EF�� 0 timenow timeNow�  �  $ l JM(��( = JM)*) o  JK�� 0 
initialrun 
initialRun* m  KL�
� boovtrue�  �  " l TW+��+ = TW,-, o  TU�� *0 forcestalefilecheck forceStaleFileCheck- m  UV�
� boovtrue�  �   k  ^�.. /0/ l ^^�12�  1  check files   2 �33  c h e c k   f i l e s0 454 r  ^m676 I  ^i�8�� &0 checkpendingfiles checkPendingFiles8 9:9 o  _b�� 00 pendingaewaiverlistold pendingAEWaiverListOLD: ;�; o  be�� 0 mylist myList�  �  7 o      �
�
 0 isprocessing isProcessing5 <=< r  nu>?> o  nq�	�	 0 mylist myList? o      �� 00 pendingaewaiverlistold pendingAEWaiverListOLD= @A@ Z  v�BC�DB = v{EFE o  vy�� 0 isprocessing isProcessingF m  yz�
� boovfalsC k  ~�GG HIH r  ~�JKJ m  ~�
� boovtrueK o      �� *0 forcestalefilecheck forceStaleFileCheckI LML r  ��NON m  ���
� boovfalsO o      �� 0 ishappy isHappyM PQP r  ��RSR b  ��TUT o  ��� �  0 failedtests failedTestsU m  ��VV �WW ( a e _ p e n d i n g _ w a i v e r s ,  S o      ���� 0 failedtests failedTestsQ X��X Z  ��YZ����Y = ��[\[ o  ������ 0 dev_mode  \ m  ����
�� boovtrueZ I ����]��
�� .sysodlogaskr        TEXT] m  ��^^ �__ 8 N O T   H A P P Y   a e _ p e n d i n g _ w a i v e r s��  ��  ��  ��  �  D l ����`a��  `  isProcessing is true			   a �bb . i s P r o c e s s i n g   i s   t r u e 	 	 	A cdc r  ��efe m  ����
�� boovtruef o      ���� 40 updatestalefiletimestamp updateStaleFileTimestampd g��g l ����������  ��  ��  ��  �    l ����hi��  h   not time to check yet   i �jj ,   n o t   t i m e   t o   c h e c k   y e t klk l ����������  ��  ��  l mnm l ����������  ��  ��  n opo r  ��qrq o  ������ 0 mycount myCountr o      ���� 0 mydatavalue myDataValuep sts r  ��uvu b  ��wxw b  ��yzy m  ��{{ �|| L { " n a m e " : " a e _ p e n d i n g _ w a i v e r s " , " v a l u e " : "z o  ������ 0 mydatavalue myDataValuex m  ��}} �~~  " }v o      ���� 0 nextitem nextItemt � r  ����� b  ����� b  ����� o  ������ 0 	dataitems 	dataItems� m  ���� ���  ,� o  ������ 0 nextitem nextItem� o      ���� 0 	dataitems 	dataItems� ��� l ����������  ��  ��  � ��� r  ����� o  ������ 0 ae_waiver_delta  � o      ���� 0 mydatavalue myDataValue� ��� r  ����� b  ����� b  ����� m  ���� ��� F { " n a m e " : " a e _ w a i v e r _ d e l t a " , " v a l u e " : "� o  ������ 0 mydatavalue myDataValue� m  ���� ���  " }� o      ���� 0 nextitem nextItem� ��� r  ����� b  ����� b  ����� o  ������ 0 	dataitems 	dataItems� m  ���� ���  ,� o  ������ 0 nextitem nextItem� o      ���� 0 	dataitems 	dataItems� ��� l ����������  ��  ��  � ��� l ��������  �   get age of oldest file   � ��� .   g e t   a g e   o f   o l d e s t   f i l e� ��� r  ���� I  ��������� (0 getageofoldestfile getAgeOfOldestFile� ��� o  ������ @0 pendingwaiversforaefolderposix pendingWaiversForAEFolderPosix� ���� m  ���� ���  . x m l��  ��  � o      ���� 0 mydatavalue myDataValue� ��� Z ������� ?  ��� o  ���� 0 mydatavalue myDataValue� o  ���� 0 oldestfileage oldestFileAge� r  
��� o  
���� 0 mydatavalue myDataValue� o      ���� 0 oldestfileage oldestFileAge��  ��  � ��� r  !��� b  ��� b  ��� m  �� ��� B { " n a m e " : " a e _ w a i v e r _ a g e " , " v a l u e " : "� o  ���� 0 mydatavalue myDataValue� m  �� ���  " }� o      ���� 0 nextitem nextItem� ��� r  "+��� b  ")��� b  "'��� o  "#���� 0 	dataitems 	dataItems� m  #&�� ���  ,� o  '(���� 0 nextitem nextItem� o      ���� 0 	dataitems 	dataItems� ��� l ,,��������  ��  ��  � ��� l ,,��������  ��  ��  � ��� l ,,������  �  check for errored waivers   � ��� 2 c h e c k   f o r   e r r o r e d   w a i v e r s� ��� r  ,1��� m  ,-����  � o      ���� 0 mythreshhold myThreshhold� ��� r  2C��� b  2A��� b  2=��� m  25�� ���  l s  � n  5<��� 1  8<��
�� 
strq� o  58���� @0 pendingwaiversforaefolderposix pendingWaiversForAEFolderPosix� m  =@�� ��� ( e r r o r /   |   g r e p   ' . x m l '� o      ���� 0 shscript  � ��� l DD��������  ��  ��  � ��� Q  D����� k  Go�� ��� r  G\��� I GX�����
�� .corecnte****       ****� c  GT��� n  GP��� 2 LP��
�� 
cpar� l GL ����  I GL����
�� .sysoexecTEXT���     TEXT o  GH���� 0 shscript  ��  ��  ��  � m  PS��
�� 
list��  � o      ���� 0 mycount myCount�  I  ]m������ 0 
writeplist 
writePlist  o  ^c���� 0 theplistpath thePListPath  m  cf		 �

 $ a e _ e r r o r e d _ w a i v e r s �� o  fi���� 0 mycount myCount��  ��   �� l nn��������  ��  ��  ��  � R      ����
�� .ascrerr ****      � **** o      ���� 
0 errmsg  ��  � k  w�  I w�����
�� .ascrcmnt****      � **** b  w� b  w� l w|���� I w|������
�� .misccurdldt    ��� null��  ��  ��  ��   m  | � 2 p r o b a b l y   n o   w a i v e r s   y e t :   o  ������ 
0 errmsg  ��   �� r  �� m  ������   o      ���� 0 mycount myCount��  �  l ����������  ��  ��    Z  �� !����  ?  ��"#" o  ������ 0 mycount myCount# o  ������ 0 mythreshhold myThreshhold! k  ��$$ %&% r  ��'(' m  ����
�� boovfals( o      ���� 0 ishappy isHappy& )*) r  ��+,+ b  ��-.- o  ������ 0 failedtests failedTests. m  ��// �00 " a e _ e r r o r _ w a i v e r ,  , o      ���� 0 failedtests failedTests* 121 l ����������  ��  ��  2 3��3 Z  ��45����4 = ��676 o  ������ 0 dev_mode  7 m  ����
�� boovtrue5 I ����8��
�� .sysodlogaskr        TEXT8 m  ��99 �:: 2 N O T   H A P P Y   a e _ e r r o r _ w a i v e r��  ��  ��  ��  ��  ��   ;<; r  ��=>= o  ���� 0 mycount myCount> o      �~�~ 0 mydatavalue myDataValue< ?@? r  ��ABA b  ��CDC b  ��EFE m  ��GG �HH F { " n a m e " : " a e _ e r r o r _ w a i v e r " , " v a l u e " : "F o  ���}�} 0 mydatavalue myDataValueD m  ��II �JJ  " }B o      �|�| 0 nextitem nextItem@ KLK r  ��MNM b  ��OPO b  ��QRQ o  ���{�{ 0 	dataitems 	dataItemsR m  ��SS �TT  ,P o  ���z�z 0 nextitem nextItemN o      �y�y 0 	dataitems 	dataItemsL UVU l ���x�w�v�x  �w  �v  V WXW l ���u�t�s�u  �t  �s  X YZY l ���r[\�r  [ ) #check pending AME files (AME queue)   \ �]] F c h e c k   p e n d i n g   A M E   f i l e s   ( A M E   q u e u e )Z ^_^ r  ��`a` b  ��bcb b  ��ded m  ��ff �gg  l s  e n  ��hih 1  ���q
�q 
strqi o  ���p�p 0 	ame_watch  c m  ��jj �kk    |   g r e p   ' . m o v 'a o      �o�o 0 shscript  _ lml Q  �%nopn r  �qrq c  �sts n  ��uvu 2 ���n
�n 
cparv l ��w�m�lw I ���kx�j
�k .sysoexecTEXT���     TEXTx o  ���i�i 0 shscript  �j  �m  �l  t m  � �h
�h 
listr o      �g�g 0 mylist myListo R      �fy�e
�f .ascrerr ****      � ****y o      �d�d 
0 errmsg  �e  p k  %zz {|{ I �c}�b
�c .ascrcmnt****      � ****} b  ~~ b  ��� l ��a�`� I �_�^�]
�_ .misccurdldt    ��� null�^  �]  �a  �`  � m  �� ��� 2 p r o b a b l y   n o   w a i v e r s   y e t :   o  �\�\ 
0 errmsg  �b  | ��[� r  %��� J  !�Z�Z  � o      �Y�Y 0 mylist myList�[  m ��� l &&�X�W�V�X  �W  �V  � ��� r  &1��� n  &-��� 1  )-�U
�U 
leng� o  &)�T�T 0 mylist myList� o      �S�S 0 mycount myCount� ��� r  2F��� I  2B�R��Q�R 0 	readplist 	readPlist� ��� o  38�P�P 0 theplistpath thePListPath� ��� m  8;�� ���  a m e _ c o u n t _ 1� ��O� m  ;>�N
�N 
null�O  �Q  � o      �M�M 0 oldcount oldCount� ��� r  GR��� \  GN��� o  GJ�L�L 0 mycount myCount� o  JM�K�K 0 oldcount oldCount� o      �J�J 0 ame_file_delta  � ��� I  Sc�I��H�I 0 
writeplist 
writePlist� ��� o  TY�G�G 0 theplistpath thePListPath� ��� m  Y\�� ���  a m e _ q u e u e _ d e l t a� ��F� o  \_�E�E 0 ame_file_delta  �F  �H  � ��� I  dt�D��C�D 0 
writeplist 
writePlist� ��� o  ej�B�B 0 theplistpath thePListPath� ��� m  jm�� ���  a m e _ c o u n t _ 1� ��A� o  mp�@�@ 0 mycount myCount�A  �C  � ��� l uu�?�>�=�?  �>  �=  � ��� l uu�<���<  �   see if files are moving	   � ��� 2   s e e   i f   f i l e s   a r e   m o v i n g 	� ��� r  u|��� I  uz�;�:�9�; 0 getepochtime getEpochTime�:  �9  � o      �8�8 0 timenow timeNow� ��� r  }���� I  }��7��6�7 0 number_to_string  � ��5� [  ~���� o  ~�4�4 :0 laststalefilechecktimestamp lastStaleFileCheckTimestamp� l ���3�2� ]  ���� o  ��1�1 00 expectedmaxprocesstime expectedMaxProcessTime� m  ���0�0  ���3  �2  �5  �6  � o      �/�/ 0 	nextcheck 	nextCheck� ��� Z  �����.�� G  ����� G  ����� l ����-�,� A  ����� o  ���+�+ 0 	nextcheck 	nextCheck� o  ���*�* 0 timenow timeNow�-  �,  � l ����)�(� = ����� o  ���'�' 0 
initialrun 
initialRun� m  ���&
�& boovtrue�)  �(  � l ����%�$� = ����� o  ���#�# *0 forcestalefilecheck forceStaleFileCheck� m  ���"
�" boovtrue�%  �$  � k  ���� ��� l ���!���!  �  check files   � ���  c h e c k   f i l e s� ��� r  ����� I  ��� ���  &0 checkpendingfiles checkPendingFiles� ��� o  ���� .0 pendingamefilelistold pendingAMEFileListOLD� ��� o  ���� 0 mylist myList�  �  � o      �� 0 isprocessing isProcessing� ��� r  ����� o  ���� 0 mylist myList� o      �� .0 pendingamefilelistold pendingAMEFileListOLD� ��� Z  ������� = ����� o  ���� 0 isprocessing isProcessing� m  ���
� boovfals� k  ���� ��� r  ����� m  ���
� boovfals� o      �� 0 ishappy isHappy� ��� r  ����� m  ���
� boovtrue� o      �� *0 forcestalefilecheck forceStaleFileCheck� � � r  �� b  �� o  ���� 0 failedtests failedTests m  �� � & a m e _ p e n d i n g _ f i l e s ,   o      �� 0 failedtests failedTests  � Z  ��	�� = ��

 o  ���� 0 dev_mode   m  ���

�
 boovtrue	 I ���	�
�	 .sysodlogaskr        TEXT m  �� � 6 N O T   H A P P Y   a m e _ p e n d i n g _ f i l e s�  �  �  �  �  �  � � r  �� m  ���
� boovtrue o      �� 40 updatestalefiletimestamp updateStaleFileTimestamp�  �.  � l ����     not time to check    � $   n o t   t i m e   t o   c h e c k�  l ������  �  �    r  � o  ��� �  0 mycount myCount o      ���� 0 mydatavalue myDataValue  r   b    b  	!"! m  ## �$$ J { " n a m e " : " a m e _ p e n d i n g _ f i l e s " , " v a l u e " : "" o  ���� 0 mydatavalue myDataValue  m  	%% �&&  " } o      ���� 0 nextitem nextItem '(' r  )*) b  +,+ b  -.- o  ���� 0 	dataitems 	dataItems. m  // �00  ,, o  ���� 0 nextitem nextItem* o      ���� 0 	dataitems 	dataItems( 121 l ��������  ��  ��  2 343 r  !565 o  ���� 0 ame_file_delta  6 o      ���� 0 mydatavalue myDataValue4 787 r  "/9:9 b  "-;<; b  ")=>= m  "%?? �@@ J { " n a m e " : " a m e _ p e n d i n g _ d e l t a " , " v a l u e " : "> o  %(���� 0 mydatavalue myDataValue< m  ),AA �BB  " }: o      ���� 0 nextitem nextItem8 CDC r  09EFE b  07GHG b  05IJI o  01���� 0 	dataitems 	dataItemsJ m  14KK �LL  ,H o  56���� 0 nextitem nextItemF o      ���� 0 	dataitems 	dataItemsD MNM l ::��������  ��  ��  N OPO l ::��QR��  Q   get age of oldest file   R �SS .   g e t   a g e   o f   o l d e s t   f i l eP TUT r  :IVWV I  :E��X���� (0 getageofoldestfile getAgeOfOldestFileX YZY o  ;>���� 0 	ame_watch  Z [��[ m  >A\\ �]]  . m o v��  ��  W o      ���� 0 mydatavalue myDataValueU ^_^ Z J[`a����` ?  JObcb o  JM���� 0 mydatavalue myDataValuec o  MN���� 0 oldestfileage oldestFileAgea r  RWded o  RU���� 0 mydatavalue myDataValuee o      ���� 0 oldestfileage oldestFileAge��  ��  _ fgf r  \ihih b  \gjkj b  \clml m  \_nn �oo @ { " n a m e " : " a m e _ f i l e _ a g e " , " v a l u e " : "m o  _b���� 0 mydatavalue myDataValuek m  cfpp �qq  " }i o      ���� 0 nextitem nextItemg rsr r  jstut b  jqvwv b  joxyx o  jk���� 0 	dataitems 	dataItemsy m  knzz �{{  ,w o  op���� 0 nextitem nextItemu o      ���� 0 	dataitems 	dataItemss |}| l tt��������  ��  ��  } ~~ l tt��������  ��  ��   ��� l tt������  � $ check pending incoming waivers   � ��� < c h e c k   p e n d i n g   i n c o m i n g   w a i v e r s� ��� r  t���� b  t���� b  t��� m  tw�� ���  l s  � n  w~��� 1  z~��
�� 
strq� o  wz���� .0 incomingrawxmlwaivers incomingRawXmlWaivers� m  ��� ���    |   g r e p   ' . x m l '� o      ���� 0 shscript  � ��� Q  ������ r  ����� c  ����� n  ����� 2 ����
�� 
cpar� l �������� I �������
�� .sysoexecTEXT���     TEXT� o  ������ 0 shscript  ��  ��  ��  � m  ����
�� 
list� o      ���� 0 mylist myList� R      �����
�� .ascrerr ****      � ****� o      ���� 
0 errmsg  ��  � k  ���� ��� I �������
�� .ascrcmnt****      � ****� b  ����� b  ����� l �������� I ��������
�� .misccurdldt    ��� null��  ��  ��  ��  � m  ���� ��� 2 p r o b a b l y   n o   w a i v e r s   y e t :  � o  ������ 
0 errmsg  ��  � ���� r  ����� J  ������  � o      ���� 0 mylist myList��  � ��� l ����������  ��  ��  � ��� r  ����� n  ����� 1  ����
�� 
leng� o  ������ 0 mylist myList� o      ���� 0 mycount myCount� ��� r  ����� I  ��������� 0 	readplist 	readPlist� ��� o  ������ 0 theplistpath thePListPath� ��� m  ���� ��� ( d r o p b o x _ w a i v e r _ c o u n t� ���� m  ����
�� 
null��  ��  � o      ���� 0 oldcount oldCount� ��� r  ����� \  ����� o  ������ 0 mycount myCount� o  ������ 0 oldcount oldCount� o      ���� 0 dropbox_waiver_delta  � ��� I  ��������� 0 
writeplist 
writePlist� ��� o  ������ 0 theplistpath thePListPath� ��� m  ���� ��� ( d r o p b o x _ w a i v e r _ d e l t a� ���� o  ������ 0 dropbox_waiver_delta  ��  ��  � ��� I  �	������� 0 
writeplist 
writePlist� ��� o  ������ 0 theplistpath thePListPath� ��� m  ��� ��� ( d r o p b o x _ w a i v e r _ c o u n t� ���� o  ���� 0 mycount myCount��  ��  � ��� l 

��������  ��  ��  � ��� l 

������  �   see if files are moving	   � ��� 2   s e e   i f   f i l e s   a r e   m o v i n g 	� ��� r  
��� I  
�������� 0 getepochtime getEpochTime��  ��  � o      ���� 0 timenow timeNow� ��� r  "��� I   ������� 0 number_to_string  � ���� [  ��� o  ���� :0 laststalefilechecktimestamp lastStaleFileCheckTimestamp� l ������ ]  ��� o  ���� 00 expectedmaxprocesstime expectedMaxProcessTime� m  ����  ����  ��  ��  ��  � o      ���� 0 	nextcheck 	nextCheck� ��� Z  #������� G  #:��� G  #0��� l #&������ A  #&��� o  #$���� 0 	nextcheck 	nextCheck� o  $%���� 0 timenow timeNow��  ��  � l ),������ = ),   o  )*���� 0 
initialrun 
initialRun m  *+��
�� boovtrue��  ��  � l 36���� = 36 o  34���� *0 forcestalefilecheck forceStaleFileCheck m  45��
�� boovtrue��  ��  � k  =�  l ==��	��    check files   	 �

  c h e c k   f i l e s  r  =L I  =H������ &0 checkpendingfiles checkPendingFiles  o  >A���� :0 pendingdropboxwaiverlistold pendingDropboxWaiverListOLD �� o  AD�� 0 mylist myList��  ��   o      �~�~ 0 isprocessing isProcessing  r  MT o  MP�}�} 0 mylist myList o      �|�| :0 pendingdropboxwaiverlistold pendingDropboxWaiverListOLD  Z  U��{�z = UZ o  UX�y�y 0 isprocessing isProcessing m  XY�x
�x boovfals k  ]�  r  ]` !  m  ]^�w
�w boovfals! o      �v�v 0 ishappy isHappy "#" r  ad$%$ m  ab�u
�u boovtrue% o      �t�t *0 forcestalefilecheck forceStaleFileCheck# &'& r  el()( b  ej*+* o  ef�s�s 0 failedtests failedTests+ m  fi,, �-- " d r o p b o x _ w a i v e r s ,  ) o      �r�r 0 failedtests failedTests' .�q. Z  m�/0�p�o/ = mt121 o  mr�n�n 0 dev_mode  2 m  rs�m
�m boovtrue0 I w~�l3�k
�l .sysodlogaskr        TEXT3 m  wz44 �55 2 N O T   H A P P Y   d r o p b o x _ w a i v e r s�k  �p  �o  �q  �{  �z   6�j6 r  ��787 m  ���i
�i boovtrue8 o      �h�h 40 updatestalefiletimestamp updateStaleFileTimestamp�j  ��  � l ���g9:�g  9   not time to check   : �;; $   n o t   t i m e   t o   c h e c k� <=< l ���f�e�d�f  �e  �d  = >?> r  ��@A@ o  ���c�c 0 mycount myCountA o      �b�b 0 mydatavalue myDataValue? BCB r  ��DED b  ��FGF b  ��HIH m  ��JJ �KK F { " n a m e " : " d r o p b o x _ w a i v e r s " , " v a l u e " : "I o  ���a�a 0 mydatavalue myDataValueG m  ��LL �MM  " }E o      �`�` 0 nextitem nextItemC NON r  ��PQP b  ��RSR b  ��TUT o  ���_�_ 0 	dataitems 	dataItemsU m  ��VV �WW  ,S o  ���^�^ 0 nextitem nextItemQ o      �]�] 0 	dataitems 	dataItemsO XYX l ���\�[�Z�\  �[  �Z  Y Z[Z r  ��\]\ o  ���Y�Y 0 dropbox_waiver_delta  ] o      �X�X 0 mydatavalue myDataValue[ ^_^ r  ��`a` b  ��bcb b  ��ded m  ��ff �gg P { " n a m e " : " d r o p b o x _ w a i v e r _ d e l t a " , " v a l u e " : "e o  ���W�W 0 mydatavalue myDataValuec m  ��hh �ii  " }a o      �V�V 0 nextitem nextItem_ jkj r  ��lml b  ��non b  ��pqp o  ���U�U 0 	dataitems 	dataItemsq m  ��rr �ss  ,o o  ���T�T 0 nextitem nextItemm o      �S�S 0 	dataitems 	dataItemsk tut l ���R�Q�P�R  �Q  �P  u vwv l ���Oxy�O  x   get age of oldest file   y �zz .   g e t   a g e   o f   o l d e s t   f i l ew {|{ r  ��}~} I  ���N�M�N (0 getageofoldestfile getAgeOfOldestFile ��� o  ���L�L .0 incomingrawxmlwaivers incomingRawXmlWaivers� ��K� m  ���� ���  . x m l�K  �M  ~ o      �J�J 0 mydatavalue myDataValue| ��� Z �����I�H� ?  ����� o  ���G�G 0 mydatavalue myDataValue� o  ���F�F 0 oldestfileage oldestFileAge� r  ����� o  ���E�E 0 mydatavalue myDataValue� o      �D�D 0 oldestfileage oldestFileAge�I  �H  � ��� r  ����� b  ����� b  ����� m  ���� ��� L { " n a m e " : " d r o p b o x _ w a i v e r _ a g e " , " v a l u e " : "� o  ���C�C 0 mydatavalue myDataValue� m  ���� ���  " }� o      �B�B 0 nextitem nextItem� ��� r  ���� b  ���� b  ���� o  � �A�A 0 	dataitems 	dataItems� m   �� ���  ,� o  �@�@ 0 nextitem nextItem� o      �?�? 0 	dataitems 	dataItems� ��� l 		�>�=�<�>  �=  �<  � ��� l 		�;���;  �  get memory stats   � ���   g e t   m e m o r y   s t a t s� ��� Q  	*���� r  ��� I �:��9
�: .sysoexecTEXT���     TEXT� m  �� ��� l t o p   - l   1   |   h e a d   - n   1 0   |   g r e p   P h y s M e m   |   s e d   ' s / ,   / n   / g '�9  � o      �8�8 0 memory_stats  � R      �7��6
�7 .ascrerr ****      � ****� o      �5�5 
0 errmsg  �6  � r  *��� b  &��� m  "�� ��� . E R R O R   R E A D I N G   M E M O R Y   -  � o  "%�4�4 
0 errmsg  � o      �3�3 0 memory_stats  � ��� l ++�2�1�0�2  �1  �0  � ��� r  +2��� o  +.�/�/ 0 memory_stats  � o      �.�. 0 mydatavalue myDataValue� ��� r  3@��� b  3>��� b  3:��� m  36�� ��� B { " n a m e " : " m e m o r y _ s t a t u s " , " v a l u e " : "� o  69�-�- 0 mydatavalue myDataValue� m  :=�� ���  " }� o      �,�, 0 nextitem nextItem� ��� r  AJ��� b  AH��� b  AF��� o  AB�+�+ 0 	dataitems 	dataItems� m  BE�� ���  ,� o  FG�*�* 0 nextitem nextItem� o      �)�) 0 	dataitems 	dataItems� ��� l KK�(�'�&�(  �'  �&  � ��� l KK�%���%  �  get free disk stats   � ��� & g e t   f r e e   d i s k   s t a t s� ��� r  KX��� c  KT��� I  KP�$�#�"�$  0 getfreediskpct getFreeDiskPct�#  �"  � m  PS�!
�! 
long� o      � �  0 freediskpct freeDiskPct� ��� r  Yf��� c  Yb��� I  Y^���� (0 getfreediskspacegb getFreeDiskSpaceGB�  �  � m  ^a�
� 
long� o      �� 0 
freediskgb 
freeDiskGB� ��� Z  g������ G  g|��� l gn���� A  gn��� o  gj�� 0 
freediskgb 
freeDiskGB� m  jm�� �  �  � l qx���� A  qx��� o  qt�� 0 freediskpct freeDiskPct� m  tw�� 
�  �  � k  ��� ��� r  ���� m  ��
� boovfals� o      �� 0 ishappy isHappy� ��� r  ����� b  ��� � o  ���� 0 failedtests failedTests  m  �� �  d i s k _ f r e e _ g b ,  � o      �� 0 failedtests failedTests� � Z  ����
 = �� o  ���	�	 0 dev_mode   m  ���
� boovtrue I ����
� .sysodisAaleR        TEXT m  ��		 �

 , N O T   H A P P Y   d i s k _ f r e e _ g b�  �  �
  �  �  �  �  l ������  �  �    r  �� o  ���� 0 
freediskgb 
freeDiskGB o      �� 0 mydatavalue myDataValue  r  �� b  �� b  �� m  �� � @ { " n a m e " : " d i s k _ f r e e _ g b " , " v a l u e " : " o  ��� �  0 mydatavalue myDataValue m  �� �  " } o      ���� 0 nextitem nextItem  r  ��  b  ��!"! b  ��#$# o  ������ 0 	dataitems 	dataItems$ m  ��%% �&&  ," o  ������ 0 nextitem nextItem  o      ���� 0 	dataitems 	dataItems '(' l ����������  ��  ��  ( )*) r  ��+,+ o  ������ 0 freediskpct freeDiskPct, o      ���� 0 mydatavalue myDataValue* -.- r  ��/0/ b  ��121 b  ��343 m  ��55 �66 J { " n a m e " : " d i s k _ f r e e _ p e r c e n t " , " v a l u e " : "4 o  ������ 0 mydatavalue myDataValue2 m  ��77 �88  " }0 o      ���� 0 nextitem nextItem. 9:9 r  ��;<; b  ��=>= b  ��?@? o  ������ 0 	dataitems 	dataItems@ m  ��AA �BB  ,> o  ������ 0 nextitem nextItem< o      ���� 0 	dataitems 	dataItems: CDC l ����������  ��  ��  D EFE l ����GH��  G !  get computer ID from pList   H �II 6   g e t   c o m p u t e r   I D   f r o m   p L i s tF JKJ Q  �LMNL r  ��OPO I  ����Q���� 0 	readplist 	readPlistQ RSR o  ������ 0 theplistpath thePListPathS TUT m  ��VV �WW  c o m p u t e r _ i dU X��X m  ��YY �ZZ  m a c S t a d i u m��  ��  P o      ���� 0 mydatavalue myDataValueM R      ��[��
�� .ascrerr ****      � ****[ o      ���� 
0 errmsg  ��  N r  \]\ b  ^_^ m  `` �aa 0 C o m p u t e r   I D   n o t   f o u n d   -  _ o  
���� 
0 errmsg  ] o      ���� 0 mydatavalue myDataValueK bcb r  ded b  fgf b  hih m  jj �kk > { " n a m e " : " c o m p u t e r _ i d " , " v a l u e " : "i o  ���� 0 mydatavalue myDataValueg m  ll �mm  " }e o      ���� 0 nextitem nextItemc non r  'pqp b  %rsr b  #tut o  ���� 0 	dataitems 	dataItemsu m  "vv �ww  ,s o  #$���� 0 nextitem nextItemq o      ���� 0 	dataitems 	dataItemso xyx l ((��������  ��  ��  y z{z l ((��|}��  |  get IP address   } �~~  g e t   I P   a d d r e s s{ � r  (5��� n  (1��� 1  -1��
�� 
siip� l (-������ e  (-�� I (-������
�� .sysosigtsirr   ��� null��  ��  ��  ��  � o      ���� 0 mydatavalue myDataValue� ��� r  6C��� b  6A��� b  6=��� m  69�� ��� R { " n a m e " : " c o m p u t e r _ I P v 4 _ a d d r e s s " , " v a l u e " : "� o  9<���� 0 mydatavalue myDataValue� m  =@�� ���  " }� o      ���� 0 nextitem nextItem� ��� r  DM��� b  DK��� b  DI��� o  DE���� 0 	dataitems 	dataItems� m  EH�� ���  ,� o  IJ���� 0 nextitem nextItem� o      ���� 0 	dataitems 	dataItems� ��� l NN��������  ��  ��  � ��� l NN��������  ��  ��  � ��� l NN��������  ��  ��  � ��� r  NS��� o  NO���� 0 oldestfileage oldestFileAge� o      ���� 0 mydatavalue myDataValue� ��� r  Ta��� b  T_��� b  T[��� m  TW�� ��� B { " n a m e " : " o l d e s t F i l e A g e " , " v a l u e " : "� o  WZ���� 0 mydatavalue myDataValue� m  [^�� ���  " }� o      ���� 0 nextitem nextItem� ��� r  bk��� b  bi��� b  bg��� o  bc���� 0 	dataitems 	dataItems� m  cf�� ���  ,� o  gh���� 0 nextitem nextItem� o      ���� 0 	dataitems 	dataItems� ��� l ll��������  ��  ��  � ��� Z  l�������� ?  ls��� o  lm���� 0 oldestfileage oldestFileAge� o  mr���� 00 oldestfileagethreshold oldestFileAgeThreshold� k  v}�� ��� l vv������  �   set isHappy to false   � ��� *   s e t   i s H a p p y   t o   f a l s e� ���� r  v}��� b  v{��� o  vw���� 0 failedtests failedTests� m  wz�� ���  o l d e s t F i l e A g e ,  � o      ���� 0 failedtests failedTests��  ��  ��  � ��� l ����������  ��  ��  � ��� Z  �������� = ����� o  ������ 0 ishappy isHappy� m  ����
�� boovtrue� k  ���� ��� r  ����� m  ���� ���  U P� o      ���� 0 mydatavalue myDataValue� ���� r  ����� m  ����
�� boovfals� o      ���� *0 forcestalefilecheck forceStaleFileCheck��  ��  � k  ���� ��� r  ����� m  ���� ���  D O W N� o      ���� 0 mydatavalue myDataValue� ���� Z  ��������� = ����� o  ������ 0 dev_mode  � m  ����
�� boovtrue� I �������
�� .sysodisAaleR        TEXT� m  ���� ��� " N O T   H A P P Y   O V E R A L L��  ��  ��  ��  � ��� r  ����� b  ����� b  ����� m  ���� ��� D { " n a m e " : " o v e r a l l _ s t a t u s " , " v a l u e " : "� o  ������ 0 mydatavalue myDataValue� m  ���� ���  " }� o      ���� 0 nextitem nextItem�    r  �� b  �� b  �� o  ������ 0 	dataitems 	dataItems m  �� �		  , o  ������ 0 nextitem nextItem o      ���� 0 	dataitems 	dataItems 

 l ����������  ��  ��    r  �� o  ������ 0 failedtests failedTests o      ���� 0 mydatavalue myDataValue  r  �� b  �� b  �� m  �� � > { " n a m e " : " f a i l e d T e s t s " , " v a l u e " : " o  ������ 0 mydatavalue myDataValue m  �� �  " } o      ���� 0 nextitem nextItem  r  �� b  �� !  b  ��"#" o  ������ 0 	dataitems 	dataItems# m  ��$$ �%%  ,! o  ������ 0 nextitem nextItem o      ���� 0 	dataitems 	dataItems &'& l ����������  ��  ��  ' ()( r  ��*+* I  ���������� 0 getepochtime getEpochTime��  ��  + o      ���� 0 mydatavalue myDataValue) ,-, r  �	./. b  ��010 b  ��232 m  ��44 �55 L { " n a m e " : " c u r r e n t _ e p o c h _ t i m e " , " v a l u e " : "3 o  ������ 0 mydatavalue myDataValue1 m  ��66 �77  " }/ o      ���� 0 nextitem nextItem- 898 r  		:;: b  			<=< b  		>?> o  		���� 0 	dataitems 	dataItems? m  		@@ �AA  ,= o  		���� 0 nextitem nextItem; o      ���� 0 	dataitems 	dataItems9 BCB l 		��������  ��  ��  C DED r  		FGF n  		HIH 1  		��
�� 
siipI l 		J����J e  		KK I 		����~
�� .sysosigtsirr   ��� null�  �~  ��  ��  G o      �}�} 0 mydatavalue myDataValueE LML r  		'NON b  		%PQP b  		!RSR m  		TT �UU 0 { " n a m e " : " R F I D " , " v a l u e " : "S o  		 �|�| 0 mydatavalue myDataValueQ m  	!	$VV �WW  " }O o      �{�{ 0 nextitem nextItemM XYX r  	(	1Z[Z b  	(	/\]\ b  	(	-^_^ o  	(	)�z�z 0 	dataitems 	dataItems_ m  	)	,`` �aa  ,] o  	-	.�y�y 0 nextitem nextItem[ o      �x�x 0 	dataitems 	dataItemsY bcb l 	2	2�w�v�u�w  �v  �u  c ded Z  	2	Efg�t�sf = 	2	9hih o  	2	7�r�r 0 dev_mode  i m  	7	8�q
�q boovtrueg I 	<	A�pj�o
�p .sysodlogaskr        TEXTj o  	<	=�n�n 0 	dataitems 	dataItems�o  �t  �s  e klk l 	F	F�m�l�k�m  �l  �k  l mnm I  	F	L�jo�i�j "0 sendstatustobpc sendStatusToBPCo p�hp o  	G	H�g�g 0 	dataitems 	dataItems�h  �i  n qrq r  	M	Tsts I  	M	R�f�e�d�f 0 getepochtime getEpochTime�e  �d  t o      �c�c 80 laststatsgatheredtimestamp lastStatsGatheredTimestampr uvu l 	U	U�b�a�`�b  �a  �`  v wxw Z  	U	fyz�_�^y = 	U	X{|{ o  	U	V�]�] 40 updatestalefiletimestamp updateStaleFileTimestamp| m  	V	W�\
�\ boovtruez r  	[	b}~} I  	[	`�[�Z�Y�[ 0 getepochtime getEpochTime�Z  �Y  ~ o      �X�X :0 laststalefilechecktimestamp lastStaleFileCheckTimestamp�_  �^  x � l 	g	g�W�V�U�W  �V  �U  � ��� Z  	g	v���T�S� = 	g	j��� o  	g	h�R�R *0 recycleaftereffects recycleAfterEffects� m  	h	i�Q
�Q boovtrue� I  	m	r�P�O�N�P *0 recycleaftereffects recycleAfterEffects�O  �N  �T  �S  � ��� l 	w	w�M�L�K�M  �L  �K  � ��J� l 	w	w�I�H�G�I  �H  �G  �J  
0 ��� l     �F�E�D�F  �E  �D  � ��� i   � ���� I      �C��B�C "0 sendstatustobpc sendStatusToBPC� ��A� o      �@�@ 0 	dataitems 	dataItems�A  �B  � k     ��� ��� l     �?���?  � [ U https://dev-api.bpcreates.com/stats/userDataCount?e=1000&type=FIRST+NAME&value=stacy   � ��� �   h t t p s : / / d e v - a p i . b p c r e a t e s . c o m / s t a t s / u s e r D a t a C o u n t ? e = 1 0 0 0 & t y p e = F I R S T + N A M E & v a l u e = s t a c y� ��� l     �>�=�<�>  �=  �<  � ��� r     	��� n     ��� 1    �;
�; 
siip� l    ��:�9� e     �� I    �8�7�6
�8 .sysosigtsirr   ��� null�7  �6  �:  �9  � o      �5�5 0 myip myIP� ��� l  
 
�4�3�2�4  �3  �2  � ��� r   
 ��� m   
 �� ��� 2 a p i . b p c r e a t e s . c o m / a p i / c u i� o      �1�1 0 
theurlbase 
theURLbase� ��� r    $��� I    �0��/�0 0 	readplist 	readPlist� ��� o    �.�. 0 theplistpath thePListPath� ��� m    �� ���  d e v _ e n d p o i n t s� ��-� o    �,�, 0 devendpoints devEndpoints�-  �/  � o      �+�+ 0 devendpoints devEndpoints� ��� r   % 2��� c   % ,��� o   % *�*�* 0 devendpoints devEndpoints� m   * +�)
�) 
bool� o      �(�( 0 devendpoints devEndpoints� ��� Z   3 J���'�� =  3 :��� o   3 8�&�& 0 devendpoints devEndpoints� m   8 9�%
�% boovtrue� r   = B��� b   = @��� m   = >�� ���  h t t p s : / / d e v -� o   > ?�$�$ 0 
theurlbase 
theURLbase� o      �#�# 0 
theurlbase 
theURLbase�'  � r   E J��� b   E H��� m   E F�� ���  h t t p s : / /� o   F G�"�" 0 
theurlbase 
theURLbase� o      �!�! 0 
theurlbase 
theURLbase� ��� l  K K� ���   �  �  � ��� r   K V��� b   K T��� b   K R��� b   K P��� b   K N��� m   K L�� ���0 { " a u t h " :   { " a g e n c y " :   " M a c S t a d i u m _ M o n i t o r i n g " , " a p i K e y " :   " 4 b 4 9 5 6 a d c 6 6 0 0 7 f f 9 9 0 f a 6 4 5 1 8 d 7 7 2 b 9 " } , " s u b m i s s i o n T y p e " :   " a d d " , " u i d L i s t " :   [ { " n a m e " :   " R F I D " , " v a l u e " :   "� o   L M�� 0 myip myIP� m   N O�� ��� x " } ] , " e v e n t C o d e " :   " 3 Q J V D " , " d i P a r a m F i l t e r " :   " r " , " d a t a I t e m s " :   [� o   P Q�� 0 	dataitems 	dataItems� m   R S�� ���  ] }� o      �� 0 
thepayload 
thePayload� ��� l  W W����  �  �  � ��� l  W W����  �  �  � ��� r   W b��� b   W `��� b   W ^��� b   W \��� m   W X�� ��� � c u r l   - v   - H   " A c c e p t :   a p p l i c a t i o n / j s o n "   - H   " C o n t e n t - t y p e :   a p p l i c a t i o n / j s o n "   - X   P O S T   - d  � n   X [��� 1   Y [�
� 
strq� o   X Y�� 0 
thepayload 
thePayload� m   \ ]�� ���   � o   ^ _�� 0 
theurlbase 
theURLbase� o      �� 0 shscript  � ��� l  c c����  �  �  � ��� Z   c v����� =  c j��� o   c h�� 0 dev_mode  � m   h i�

�
 boovtrue� I  m r�	��
�	 .sysodlogaskr        TEXT� o   m n�� 0 shscript  �  �  �  �    l  w w����  �  �    Q   w � k   z � 	 r   z �

 I  z ��
� .sysoexecTEXT���     TEXT o   z {�� 0 shscript  �   o      � �  0 	myoutcome 	myOutcome	 �� L   � � o   � ����� 0 	myoutcome 	myOutcome��   R      ����
�� .ascrerr ****      � **** o      ���� 
0 errmsg  ��   L   � � o   � ����� 
0 errmsg   �� l  � ���������  ��  ��  ��  �  l     ��������  ��  ��    l     ��������  ��  ��    l     ��������  ��  ��    i   � � I      �������� 0 setvars setVars��  ��   k    J  Q    H ! k   >"" #$# r    	%&% c    '(' J    ����  ( m    ��
�� 
list& o      ���� 00 pendingaewaiverlistold pendingAEWaiverListOLD$ )*) r   
 +,+ c   
 -.- J   
 ����  . m    ��
�� 
list, o      ���� .0 pendingamefilelistold pendingAMEFileListOLD* /0/ r    121 c    343 J    ����  4 m    ��
�� 
list2 o      ���� :0 pendingdropboxwaiverlistold pendingDropboxWaiverListOLD0 565 l   ��������  ��  ��  6 787 r    &9:9 I    $��;���� 0 	readplist 	readPlist; <=< o    ���� 0 theplistpath thePListPath= >?> m    @@ �AA  c o m p u t e r _ i d? B��B m     ��
�� 
null��  ��  : o      ���� 0 computer_id  8 CDC l  ' '��������  ��  ��  D EFE r   ' =GHG I   ' 7��I���� 0 	readplist 	readPlistI JKJ o   ( -���� 0 theplistpath thePListPathK LML m   - .NN �OO   c o m p u t e r U s e r N a m eM P��P o   . 3���� $0 computerusername computerUserName��  ��  H o      ���� $0 computerusername computerUserNameF QRQ l  > >��������  ��  ��  R STS r   > VUVU c   > PWXW I   > N��Y���� 0 	readplist 	readPlistY Z[Z o   ? D���� 0 theplistpath thePListPath[ \]\ m   D E^^ �__  d e v _ m o d e] `��` o   E J���� 0 dev_mode  ��  ��  X m   N O��
�� 
boolV o      ���� 0 dev_mode  T aba r   W ocdc c   W iefe I   W g��g���� 0 	readplist 	readPlistg hih o   X ]���� 0 theplistpath thePListPathi jkj m   ] ^ll �mm  d e v _ e n d p o i n t sk n��n o   ^ c���� 0 devendpoints devEndpoints��  ��  f m   g h��
�� 
boold o      ���� 0 devendpoints devEndpointsb opo l  p p��������  ��  ��  p qrq r   p �sts c   p ~uvu I   p |��w���� 0 	readplist 	readPlistw xyx o   q v���� 0 theplistpath thePListPathy z{z m   v w|| �}}  c l e a n u p{ ~��~ m   w x��
�� boovtrue��  ��  v m   | }��
�� 
boolt o      ���� (0 performfilecleanup performFileCleanupr � l  � ���������  ��  ��  � ��� r   � ���� I   � �������� 0 	readplist 	readPlist� ��� o   � ����� 0 theplistpath thePListPath� ��� m   � ��� ���  s t a t i o n _ t y p e� ���� m   � ���
�� 
null��  ��  � o      ���� 0 stationtype stationType� ��� l  � ���������  ��  ��  � ��� l  � �������  � # set this to false for testing   � ��� : s e t   t h i s   t o   f a l s e   f o r   t e s t i n g� ��� l  � �������  �  set automated to true   � ��� * s e t   a u t o m a t e d   t o   t r u e� ��� r   � ���� c   � ���� I   � �������� 0 	readplist 	readPlist� ��� o   � ����� 0 theplistpath thePListPath� ��� m   � ��� ���  a u t o m a t e d� ���� o   � ����� 0 	automated  ��  ��  � m   � ���
�� 
bool� o      ���� 0 	automated  � ��� l  � ���������  ��  ��  � ��� l  � �������  �  set loopDelaySecs to 10   � ��� . s e t   l o o p D e l a y S e c s   t o   1 0� ��� r   � ���� c   � ���� I   � �������� 0 	readplist 	readPlist� ��� o   � ����� 0 theplistpath thePListPath� ��� m   � ��� ���  l o o p _ d e l a y� ���� o   � ����� 0 loopdelaysecs loopDelaySecs��  ��  � m   � ���
�� 
long� o      ���� 0 loopdelaysecs loopDelaySecs� ��� l  � ���������  ��  ��  � ��� l  � �������  �  set processDelaySecs to 5   � ��� 2 s e t   p r o c e s s D e l a y S e c s   t o   5� ��� r   � ���� c   � ���� I   � ����� 0 	readplist 	readPlist� ��� o   � ��� 0 theplistpath thePListPath� ��� m   � ��� ���  p r o c e s s _ d e l a y� ��� o   � ��� $0 processdelaysecs processDelaySecs�  �  � m   � ��
� 
long� o      �� $0 processdelaysecs processDelaySecs� ��� l  � �����  �  �  � ��� l  � �����  � I Cset waiver sequence, the interger to look for at end of waiver name   � ��� � s e t   w a i v e r   s e q u e n c e ,   t h e   i n t e r g e r   t o   l o o k   f o r   a t   e n d   o f   w a i v e r   n a m e� ��� r   ���� c   � ���� I   � ����� 0 	readplist 	readPlist� ��� o   � ��� 0 theplistpath thePListPath� ��� m   � ��� ���  w a i v e r _ s e q u e n c e� ��� o   � ��� $0 waivergrepstring waiverGrepString�  �  � m   � ��
� 
list� o      �~�~ $0 waivergrepstring waiverGrepString� ��� l �}�|�{�}  �|  �{  � ��� l �z���z  � 1 + set delay when checking AE output filesize   � ��� V   s e t   d e l a y   w h e n   c h e c k i n g   A E   o u t p u t   f i l e s i z e� ��� r  ��� c  ��� I  �y��x�y 0 	readplist 	readPlist� ��� o  �w�w 0 theplistpath thePListPath� ��� m  �� ��� $ f i l e s i z e C h e c k D e l a y� ��v� o  �u�u (0 filesizecheckdelay filesizeCheckDelay�v  �x  � m  �t
�t 
long� o      �s�s (0 filesizecheckdelay filesizeCheckDelay� ��� l �r�q�p�r  �q  �p  � ��� l �o� �o  � G A set minimum file size expected from AE output (before transcode)     � �   s e t   m i n i m u m   f i l e   s i z e   e x p e c t e d   f r o m   A E   o u t p u t   ( b e f o r e   t r a n s c o d e )�  r  ; c  5 I  1�n�m�n 0 	readplist 	readPlist 	
	 o   %�l�l 0 theplistpath thePListPath
  m  %( � : f i l e s i z e C h e c k _ m i n A E O u t p u t S i z e �k o  (-�j�j >0 filesizecheck_minaeoutputsize filesizeCheck_minAEOutputSize�k  �m   m  14�i
�i 
long o      �h�h >0 filesizecheck_minaeoutputsize filesizeCheck_minAEOutputSize  l <<�g�f�e�g  �f  �e    l <<�d�c�b�d  �c  �b   �a L  <> m  <=�`
�` boovtrue�a    R      �_�^
�_ .ascrerr ****      � **** o      �]�] 
0 errmsg  �^  ! L  FH o  FG�\�\ 
0 errmsg   �[ l II�Z�Y�X�Z  �Y  �X  �[    l     �W�V�U�W  �V  �U    l     �T�S�R�T  �S  �R    l     �Q�P�O�Q  �P  �O     i   � �!"! I      �N�M�L�N 0 setpaths setPaths�M  �L  " k    �## $%$ Q    �&'(& k   �)) *+* r    ,-, I    �K.�J�K 0 	readplist 	readPlist. /0/ o    	�I�I 0 theplistpath thePListPath0 121 m   	 
33 �44  d r o p b o x _ p a t h2 5�H5 m   
 66 �77 v / U s e r s / b l u e p i x e l / D r o p b o x / p r e i n g e s t i o n _ p r o c e s s e s / v i d e o b o o t h /�H  �J  - o      �G�G "0 dropboxrootpath dropboxRootPath+ 898 Z    +:;�F<: =   =>= o    �E�E 0 devendpoints devEndpoints> m    �D
�D boovtrue; r    #?@? b    !ABA b    CDC o    �C�C "0 dropboxrootpath dropboxRootPathD m    EE �FF  d e v _B o     �B�B 0 stationtype stationType@ o      �A�A "0 dropboxrootpath dropboxRootPath�F  < r   & +GHG b   & )IJI o   & '�@�@ "0 dropboxrootpath dropboxRootPathJ o   ' (�?�? 0 stationtype stationTypeH o      �>�> "0 dropboxrootpath dropboxRootPath9 KLK l  , ,�=�<�;�=  �<  �;  L MNM r   , 1OPO b   , /QRQ o   , -�:�: "0 dropboxrootpath dropboxRootPathR m   - .SS �TT  / w a i v e r s /P o      �9�9 .0 incomingrawxmlwaivers incomingRawXmlWaiversN UVU r   2 7WXW b   2 5YZY o   2 3�8�8 "0 dropboxrootpath dropboxRootPathZ m   3 4[[ �\\  / m e d i a /X o      �7�7 :0 incomingrawmediafolderposix incomingRawMediaFolderPosixV ]^] l  8 8�6�5�4�6  �5  �4  ^ _`_ r   8 Caba b   8 Acdc b   8 ?efe m   8 9gg �hh  / U s e r s /f o   9 >�3�3 $0 computerusername computerUserNamed m   ? @ii �jj : / D o c u m e n t s / p e n d i n g _ t r a n s c o d e /b o      �2�2 :0 transcodependingfolderposix transcodePendingFolderPosix` klk Q   D Zmnom r   G Opqp I   G M�1r�0�1 0 folderexists folderExistsr s�/s o   H I�.�. :0 transcodependingfolderposix transcodePendingFolderPosix�/  �0  q o      �-�- 0 filetest fileTestn R      �,t�+
�, .ascrerr ****      � ****t o      �*�* 
0 errmsg  �+  o r   W Zuvu m   W X�)
�) boovtruev o      �(�( 0 filetest fileTestl wxw Z   [ ryz�'�&y =  [ ^{|{ o   [ \�%�% 0 filetest fileTest| m   \ ]�$
�$ boovfalsz I  a n�#}�"
�# .sysoexecTEXT���     TEXT} b   a j~~ m   a d�� ���  m k d i r   n   d i��� 1   e i�!
�! 
strq� o   d e� �  :0 transcodependingfolderposix transcodePendingFolderPosix�"  �'  �&  x ��� l  s s����  �  �  � ��� r   s ���� b   s ���� b   s |��� m   s v�� ���  / U s e r s /� o   v {�� $0 computerusername computerUserName� m   | �� ��� 8 / D o c u m e n t s / m e d i a _ t r a n s c o d e d /� o      �� >0 transcodecompletedfolderposix transcodeCompletedFolderPosix� ��� Q   � ����� r   � ���� I   � ����� 0 folderexists folderExists� ��� o   � ��� >0 transcodecompletedfolderposix transcodeCompletedFolderPosix�  �  � o      �� 0 filetest fileTest� R      ���
� .ascrerr ****      � ****� o      �� 
0 errmsg  �  � r   � ���� m   � ��
� boovtrue� o      �� 0 filetest fileTest� ��� Z   � ������ =  � ���� o   � ��� 0 filetest fileTest� m   � ��
� boovfals� I  � ����
� .sysoexecTEXT���     TEXT� b   � ���� m   � ��� ���  m k d i r  � n   � ���� 1   � ��

�
 
strq� o   � ��	�	 >0 transcodecompletedfolderposix transcodeCompletedFolderPosix�  �  �  � ��� l  � �����  �  �  � ��� r   � ���� b   � ���� b   � ���� m   � ��� ���  / U s e r s /� o   � ��� $0 computerusername computerUserName� m   � ��� ��� 6 / D o c u m e n t s / w a i v e r s _ p e n d i n g /� o      �� @0 pendingwaiversforaefolderposix pendingWaiversForAEFolderPosix� ��� Q   � ����� r   � ���� I   � ����� 0 folderexists folderExists� ��� o   � �� �  @0 pendingwaiversforaefolderposix pendingWaiversForAEFolderPosix�  �  � o      ���� 0 filetest fileTest� R      �����
�� .ascrerr ****      � ****� o      ���� 
0 errmsg  ��  � r   � ���� m   � ���
�� boovtrue� o      ���� 0 filetest fileTest� ��� Z   � �������� =  � ���� o   � ����� 0 filetest fileTest� m   � ���
�� boovfals� I  � ������
�� .sysoexecTEXT���     TEXT� b   � ���� m   � ��� ���  m k d i r  � n   � ���� 1   � ���
�� 
strq� o   � ����� @0 pendingwaiversforaefolderposix pendingWaiversForAEFolderPosix��  ��  ��  � ��� l  � ���������  ��  ��  � ��� r   ���� b   ���� o   � ���� @0 pendingwaiversforaefolderposix pendingWaiversForAEFolderPosix� m   �� ��� $ c o m p l e t e d _ a r c h i v e /� o      ���� %0 !waivers_pending_completed_archive  � ��� Q  	!���� r  ��� I  ������� 0 folderexists folderExists� ���� o  ���� %0 !waivers_pending_completed_archive  ��  ��  � o      ���� 0 filetest fileTest� R      �����
�� .ascrerr ****      � ****� o      ���� 
0 errmsg  ��  � r  !��� m  ��
�� boovtrue� o      ���� 0 filetest fileTest� ��� Z  ";������� = "%��� o  "#���� 0 filetest fileTest� m  #$��
�� boovfals� I (7�����
�� .sysoexecTEXT���     TEXT� b  (3��� m  (+�� ���  m k d i r  � n  +2��� 1  .2��
�� 
strq� o  +.���� %0 !waivers_pending_completed_archive  ��  ��  ��  � ��� l <<��������  ��  ��  � ��� r  <M��� b  <I��� b  <E��� m  <?�� �    / U s e r s /� o  ?D���� $0 computerusername computerUserName� m  EH � , / D o c u m e n t s / m e d i a _ p o o l /� o      ���� 0 
media_pool  �  Q  Nf r  Q[	 I  QY��
���� 0 folderexists folderExists
 �� o  RU���� 0 
media_pool  ��  ��  	 o      ���� 0 filetest fileTest R      ������
�� .ascrerr ****      � ****��  ��   r  cf m  cd��
�� boovtrue o      ���� 0 filetest fileTest  Z  g����� = gj o  gh���� 0 filetest fileTest m  hi��
�� boovfals I m|����
�� .sysoexecTEXT���     TEXT b  mx m  mp �  m k d i r   n  pw 1  sw��
�� 
strq o  ps���� 0 
media_pool  ��  ��  ��    l ����������  ��  ��    r  ��  b  ��!"! b  ��#$# m  ��%% �&&  / U s e r s /$ o  ������ $0 computerusername computerUserName" m  ��'' �(( 8 / D o c u m e n t s / a m e _ w a t c h / O u t p u t /  o      �� 0 processed_media   )*) Q  ��+,-+ r  ��./. I  ���0�� 0 folderexists folderExists0 1�1 o  ���� 0 processed_media  �  �  / o      �� 0 filetest fileTest, R      ���
� .ascrerr ****      � ****�  �  - r  ��232 m  ���
� boovtrue3 o      �� 0 filetest fileTest* 454 Z  ��67��6 = ��898 o  ���� 0 filetest fileTest9 m  ���
� boovfals7 I ���:�
� .sysoexecTEXT���     TEXT: b  ��;<; m  ��== �>>  m k d i r  < n  ��?@? 1  ���
� 
strq@ o  ���� 0 processed_media  �  �  �  5 ABA l ������  �  �  B CDC r  ��EFE b  ��GHG b  ��IJI m  ��KK �LL  / U s e r s /J o  ���� $0 computerusername computerUserNameH m  ��MM �NN * / D o c u m e n t s / a e _ o u t p u t /F o      �� 0 	ae_output  D OPO Q  ��QRSQ r  ��TUT I  ���V�� 0 folderexists folderExistsV W�W o  ���� 0 	ae_output  �  �  U o      �� 0 filetest fileTestR R      ���
� .ascrerr ****      � ****�  �  S r  ��XYX m  ���
� boovtrueY o      �� 0 filetest fileTestP Z[Z Z  �
\]��\ = ��^_^ o  ���� 0 filetest fileTest_ m  ���
� boovfals] I ��`�
� .sysoexecTEXT���     TEXT` b  �aba m  ��cc �dd  m k d i r  b n  �efe 1  ��
� 
strqf o  ���� 0 	ae_output  �  �  �  [ ghg l ����  �  �  h iji r  klk b  mnm b  opo m  qq �rr  / U s e r s /p o  �� $0 computerusername computerUserNamen m  ss �tt * / D o c u m e n t s / a m e _ w a t c h /l o      �� 0 	ame_watch  j uvu Q  5wxyw r   *z{z I   (�|�� 0 folderexists folderExists| }�} o  !$�� 0 	ame_watch  �  �  { o      �� 0 filetest fileTestx R      ���
� .ascrerr ****      � ****�  �  y r  25~~ m  23�
� boovtrue o      �� 0 filetest fileTestv ��� Z  6O����� = 69��� o  67�� 0 filetest fileTest� m  78�
� boovfals� I <K���
� .sysoexecTEXT���     TEXT� b  <G��� m  <?�� ���  m k d i r  � n  ?F��� 1  BF�
� 
strq� o  ?B�� 0 	ame_watch  �  �  �  � ��� l PP�~�}�|�~  �}  �|  � ��� r  PY��� b  PU��� o  PQ�{�{ "0 dropboxrootpath dropboxRootPath� m  QT�� ���  / i n g e s t o r /� o      �z�z 0 spock_ingestor  � ��� Q  Zr���� r  ]g��� I  ]e�y��x�y 0 folderexists folderExists� ��w� o  ^a�v�v 0 spock_ingestor  �w  �x  � o      �u�u 0 filetest fileTest� R      �t�s�r
�t .ascrerr ****      � ****�s  �r  � r  or��� m  op�q
�q boovtrue� o      �p�p 0 filetest fileTest� ��� Z  s����o�n� = sv��� o  st�m�m 0 filetest fileTest� m  tu�l
�l boovfals� I y��k��j
�k .sysoexecTEXT���     TEXT� b  y���� m  y|�� ���  m k d i r  � n  |���� 1  ��i
�i 
strq� o  |�h�h 0 spock_ingestor  �j  �o  �n  � ��� l ���g�f�e�g  �f  �e  � ��d� L  ���� m  ���c
�c boovtrue�d  ' R      �b��a
�b .ascrerr ****      � ****� o      �`�` 
0 errmsg  �a  ( L  ���� o  ���_�_ 
0 errmsg  % ��^� l ���]�\�[�]  �\  �[  �^    ��� l     �Z�Y�X�Z  �Y  �X  � ��� l     �W�V�U�W  �V  �U  � ��� l     �T���T  � C =#############################################################   � ��� z # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #� ��� l     �S���S  � % ## --HELPER & UTILITY METHODS--   � ��� > # #   - - H E L P E R   &   U T I L I T Y   M E T H O D S - -� ��� l     �R���R  � % ## --HELPER & UTILITY METHODS--   � ��� > # #   - - H E L P E R   &   U T I L I T Y   M E T H O D S - -� ��� l     �Q���Q  � C =#############################################################   � ��� z # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #� ��� l     �P�O�N�P  �O  �N  � ��� i   � ���� I      �M��L�M 0 read_waiver  � ��K� o      �J�J 0 	thewaiver  �K  �L  � k     r�� ��� l     �I���I  �  get waiver details   � ��� $ g e t   w a i v e r   d e t a i l s� ��� r     
��� I     �H��G�H 0 get_element  � ��� o    �F�F 0 	thewaiver  � ��� m    �� ���  J o b� ��E� m    �� ���  i d�E  �G  � o      �D�D 0 	waiver_id  � ��� r    ��� I    �C��B�C 0 get_element  � ��� o    �A�A 0 	thewaiver  � ��� m    �� ���  J o b� ��@� m    �� ���  c r e a t e d�@  �B  � o      �?�? 0 waiver_created  � ��� l   �>�=�<�>  �=  �<  � ��� r     ��� I    �;��:�; 0 get_element  � � � o    �9�9 0 	thewaiver     m     �  J o b �8 m     �  p r o g r a m�8  �:  � o      �7�7 0 waiver_program  � 	 r   ! +

 I   ! )�6�5�6 0 get_element    o   " #�4�4 0 	thewaiver    m   # $ �  J o b �3 m   $ % �  a c t i v a t i o n�3  �5   o      �2�2 0 waiver_activation  	  r   , 6 I   , 4�1�0�1 0 get_element    o   - .�/�/ 0 	thewaiver    m   . / �    J o b !�.! m   / 0"" �##  t e a m�.  �0   o      �-�- 0 waiver_team   $%$ r   7 A&'& I   7 ?�,(�+�, 0 get_element  ( )*) o   8 9�*�* 0 	thewaiver  * +,+ m   9 :-- �..  J o b, /�)/ m   : ;00 �11 
 e v e n t�)  �+  ' o      �(�( 0 waiver_event  % 232 r   B L454 I   B J�'6�&�' 0 get_element  6 787 o   C D�%�% 0 	thewaiver  8 9:9 m   D E;; �<<  J o b: =�$= m   E F>> �??  t o t a l _ r a w _ f i l e s�$  �&  5 o      �#�# 0 waiver_total_raw_files  3 @A@ l  M M�"�!� �"  �!  �   A BCB l  M M����  �  �  C DED r   M mFGF K   M kHH �IJ� 0 	waiver_id  I o   N O�� 0 	waiver_id  J �KL� 0 waiver_created  K o   R S�� 0 waiver_created  L �MN� 0 waiver_program  M o   V W�� 0 waiver_program  N �OP� 0 waiver_activation  O o   Z [�� 0 waiver_activation  P �QR� 0 waiver_team  Q o   ^ _�� 0 waiver_team  R �ST� 0 waiver_event  S o   b c�� 0 waiver_event  T �U�� 0 waiver_total_raw_files  U o   f g�� 0 waiver_total_raw_files  �  G o      �� 0 waiver_data  E VWV l  n n���
�  �  �
  W XYX l  n n�	���	  �  �  Y Z[Z L   n p\\ o   n o�� 0 waiver_data  [ ]�] l  q q����  �  �  �  � ^_^ l     �� ���  �   ��  _ `a` i   � �bcb I      ��d���� 0 get_element  d efe o      ���� 0 	thewaiver  f ghg o      ���� 0 node  h i��i o      ���� 0 element_name  ��  ��  c Q     =jklj O    mnm k    oo pqp l   ��rs��  r  get top level   s �tt  g e t   t o p   l e v e lq uvu r    wxw n    yzy 5    ��{��
�� 
xmle{ o    ���� 0 node  
�� kfrmnamez n    |}| 1    ��
�� 
pcnt} 4    ��~
�� 
xmlf~ o   	 
���� 0 	thewaiver  x o      ���� 0 xmldata  v �� r    ��� n    ��� 1    ��
�� 
valL� n    ��� 5    �����
�� 
xmle� o    ���� 0 element_name  
�� kfrmname� o    ���� 0 xmldata  � o      ��
�� 
ret ��  n m    ���                                                                                  sevs  alis    \  Macintosh HD                   BD ����System Events.app                                              ����            ����  
 cu             CoreServices  0/:System:Library:CoreServices:System Events.app/  $  S y s t e m   E v e n t s . a p p    M a c i n t o s h   H D  -System/Library/CoreServices/System Events.app   / ��  k R      �����
�� .ascrerr ****      � ****� o      ���� 
0 errmsg  ��  l k   & =�� ��� Z   & 9������� =  & -��� o   & +���� 0 dev_mode  � m   + ,��
�� boovtrue� I  0 5�����
�� .sysodisAaleR        TEXT� o   0 1���� 
0 errmsg  ��  ��  ��  � ���� r   : =��� m   : ;�� ���  � o      ��
�� 
ret ��  a ��� l     ��������  ��  ��  � ��� l     ��������  ��  ��  � ��� i   � ���� I      ������� 0 
fileexists 
fileExists� ���� o      ���� 0 thefile theFile��  ��  � l    ���� O     ��� Z    ������ I   �����
�� .coredoexnull���     ****� 4    ���
�� 
file� o    ���� 0 thefile theFile��  � L    �� m    ��
�� boovtrue��  � L    �� m    ��
�� boovfals� m     ���                                                                                  sevs  alis    \  Macintosh HD                   BD ����System Events.app                                              ����            ����  
 cu             CoreServices  0/:System:Library:CoreServices:System Events.app/  $  S y s t e m   E v e n t s . a p p    M a c i n t o s h   H D  -System/Library/CoreServices/System Events.app   / ��  �   (String) as Boolean   � ��� (   ( S t r i n g )   a s   B o o l e a n� ��� l     ��������  ��  ��  � ��� i   � ���� I      ������� 0 folderexists folderExists� ���� o      ���� 0 thefile theFile��  ��  � l    ���� O     ��� Z    ������ I   �����
�� .coredoexnull���     ****� 4    ���
�� 
cfol� o    ���� 0 thefile theFile��  � L    �� m    ��
�� boovtrue��  � L    �� m    �
� boovfals� m     ���                                                                                  sevs  alis    \  Macintosh HD                   BD ����System Events.app                                              ����            ����  
 cu             CoreServices  0/:System:Library:CoreServices:System Events.app/  $  S y s t e m   E v e n t s . a p p    M a c i n t o s h   H D  -System/Library/CoreServices/System Events.app   / ��  �   (String) as Boolean   � ��� (   ( S t r i n g )   a s   B o o l e a n� ��� l     ����  �  �  � ��� i   � ���� I      ���� 0 	changeext 	changeExt� ��� o      �� 0 filename  � ��� o      �� 0 new_ext  �  �  � k      �� ��� r     ��� \     	��� l    ���� I    ���
� .corecnte****       ****� n     ��� 2   �
� 
cha � o     �� 0 filename  �  �  �  � m    �� � o      �� 0 mylength  � ��� r    ��� b    ��� l   ���� c    ��� l   ���� n    ��� 7  ���
� 
cha � m    �� � o    �� 0 mylength  � o    �� 0 filename  �  �  � m    �
� 
TEXT�  �  � o    �� 0 new_ext  � o      �� 0 	newstring  � ��� L     �� o    �� 0 	newstring  �  � ��� l     ����  �  �  � ��� i   � ���� I      ����  0 getfreediskpct getFreeDiskPct�  �  � k     �� ��� r     ��� I     ���� 0 trimline trimLine� ��� n    ��� o    �� 0 capacity  � I    ���� 0 getdiskstats getDiskStats�  �  � ��� m    	�� ���  %� ��� m   	 
�� �  �  � o      �� 0 rawpct rawPct� ��� l   � �    . ( returns % used, so invert it for % free    � P   r e t u r n s   %   u s e d ,   s o   i n v e r t   i t   f o r   %   f r e e� � L     \     m    �� d o    �� 0 rawpct rawPct�  �  l     ����  �  �   	
	 i   � � I      ���� (0 getfreediskspacegb getFreeDiskSpaceGB�  �   L     	 n      o    �� 0 	available   I     ���� 0 getdiskstats getDiskStats�  �  
  l     ����  �  �    i   � � I      ���� 0 getdiskstats getDiskStats�  �   k     �  r      n     1    �~
�~ 
txdl 1     �}
�} 
ascr o      �|�| 0 old_delimts    Q    � �{ k   	 �!! "#" l  	 	�z�y�x�z  �y  �x  # $%$ r   	 &'& I  	 �w(�v
�w .sysoexecTEXT���     TEXT( m   	 
)) �**  d f   - h l g�v  ' o      �u�u 0 memory_stats  % +,+ r    -.- n    /0/ 4    �t1
�t 
cpar1 m    �s�s 0 o    �r�r 0 memory_stats  . o      �q�q 0 	rawstring  , 232 l   �p45�p  4 ( " set freeDisk to item 1 of theList   5 �66 D   s e t   f r e e D i s k   t o   i t e m   1   o f   t h e L i s t3 787 l   �o9:�o  9 , & set diskCapacity to item 1 of theList   : �;; L   s e t   d i s k C a p a c i t y   t o   i t e m   1   o f   t h e L i s t8 <=< l   �n>?�n  > 2 , set diskFreePercentage to item 2 of theList   ? �@@ X   s e t   d i s k F r e e P e r c e n t a g e   t o   i t e m   2   o f   t h e L i s t= ABA r    CDC m    EE �FF   D n     GHG 1    �m
�m 
txdlH 1    �l
�l 
ascrB IJI r    #KLK n    !MNM 2   !�k
�k 
citmN o    �j�j 0 	rawstring  L o      �i�i 0 thelist theListJ OPO r   $ )QRQ o   $ %�h�h 0 old_delimts  R n     STS 1   & (�g
�g 
txdlT 1   % &�f
�f 
ascrP UVU l  * *�e�d�c�e  �d  �c  V WXW r   * .YZY J   * ,�b�b  Z o      �a�a 0 newlist newListX [\[ X   / X]�`^] k   ? S__ `a` l  ? ?�_bc�_  b  display dialog i   c �dd   d i s p l a y   d i a l o g   ia efe Z   ? Qgh�^�]g >  ? Diji l  ? Bk�\�[k n   ? Blml m   @ B�Z
�Z 
ctxtm o   ? @�Y�Y 0 i  �\  �[  j m   B Cnn �oo  h s   G Mpqp l  G Jr�X�Wr n   G Jsts m   H J�V
�V 
ctxtt o   G H�U�U 0 i  �X  �W  q l     u�T�Su n      vwv  ;   K Lw o   J K�R�R 0 newlist newList�T  �S  �^  �]  f x�Qx l  R R�P�O�N�P  �O  �N  �Q  �` 0 i  ^ o   2 3�M�M 0 thelist theList\ yzy l  Y Y�L�K�J�L  �K  �J  z {�I{ r   Y �|}| K   Y �~~ �H��H 0 diskname   l  Z `��G�F� n   Z `��� m   ^ `�E
�E 
ctxt� n   Z ^��� 4   [ ^�D�
�D 
cobj� m   \ ]�C�C � o   Z [�B�B 0 newlist newList�G  �F  � �A���A 0 
onegblocks 
OneGblocks� l  a g��@�?� n   a g��� m   e g�>
�> 
ctxt� n   a e��� 4   b e�=�
�= 
cobj� m   c d�<�< � o   a b�;�; 0 newlist newList�@  �?  � �:���: 0 used  � l  h n��9�8� n   h n��� m   l n�7
�7 
ctxt� n   h l��� 4   i l�6�
�6 
cobj� m   j k�5�5 � o   h i�4�4 0 newlist newList�9  �8  � �3���3 0 	available  � l  o w��2�1� n   o w��� m   u w�0
�0 
ctxt� n   o u��� 4   p u�/�
�/ 
cobj� m   q t�.�. � o   o p�-�- 0 newlist newList�2  �1  � �,��+�, 0 capacity  � l  z ���*�)� n   z ���� m   � ��(
�( 
ctxt� n   z ���� 4   { ��'�
�' 
cobj� m   | �&�& � o   z {�%�% 0 newlist newList�*  �)  �+  } o      �$�$ 0 	finallist 	finalList�I    R      �#��"
�# .ascrerr ****      � ****� o      �!�! 
0 errmsg  �"  �{   ��� l  � �� ���   �  �  � ��� r   � ���� o   � ��� 0 old_delimts  � n     ��� 1   � ��
� 
txdl� 1   � ��
� 
ascr� ��� l  � �����  �  �  � ��� L   � ��� o   � ��� 0 	finallist 	finalList� ��� l  � �����  �  �  � ��� l  � �����  �  �  �   ��� l     ����  �  �  � ��� i   � ���� I      ���� 0 
is_running  � ��
� o      �	�	 0 appname appName�
  �  � k     :�� ��� O     ��� r    ��� l   	���� n    	��� 1    	�
� 
pnam� 2    �
� 
prcs�  �  � o      �� "0 listofprocesses listOfProcesses� m     ���                                                                                  sevs  alis    \  Macintosh HD                   BD ����System Events.app                                              ����            ����  
 cu             CoreServices  0/:System:Library:CoreServices:System Events.app/  $  S y s t e m   E v e n t s . a p p    M a c i n t o s h   H D  -System/Library/CoreServices/System Events.app   / ��  � ��� l   ����  �  �  � ��� r    ��� m    � 
�  boovfals� o      ���� 0 appisrunning appIsRunning� ��� X    5����� Z   ! 0������� E   ! $��� o   ! "���� 0 thisitem thisItem� o   " #���� 0 appname appName� k   ' ,�� ��� r   ' *��� m   ' (��
�� boovtrue� o      ���� 0 appisrunning appIsRunning� ����  S   + ,��  ��  ��  �� 0 thisitem thisItem� o    ���� "0 listofprocesses listOfProcesses� ��� L   6 8�� o   6 7���� 0 appisrunning appIsRunning� ���� l  9 9��������  ��  ��  ��  � ��� l     ��������  ��  ��  � ��� i   � ���� I      ������� 0 	readplist 	readPlist� ��� o      ���� 0 theplistpath thePListPath� ��� o      ���� 0 plistvar plistVar� ���� o      ���� 0 defaultvalue defaultValue��  ��  � Q     2���� k    �� ��� r    
��� b    ��� b    ��� m    �� ���    - c   ' p r i n t  � o    ���� 0 plistvar plistVar� m    �� ���  '� o      ���� 0 	mycommand 	myCommand� ��� r       b     b     m     � 2 / u s r / l i b e x e c / P l i s t B u d d y     o    ���� 0 theplistpath thePListPath o    ���� 0 	mycommand 	myCommand o      ���� 0 myscript myScript� �� I   ��	��
�� .sysoexecTEXT���     TEXT	 o    ���� 0 myscript myScript��  ��  � R      ��
��
�� .ascrerr ****      � ****
 o      ���� 
0 errmsg  ��  � k     2  Z     /���� =    # o     !���� 0 defaultvalue defaultValue m   ! "��
�� 
null I  & +����
�� .sysodisAaleR        TEXT o   & '���� 
0 errmsg  ��  ��  ��   �� L   0 2 o   0 1���� 0 defaultvalue defaultValue��  �  l     ��������  ��  ��    l     ��������  ��  ��    l     ��������  ��  ��    l     ��������  ��  ��    i   � �  I      ��!���� 0 
writeplist 
writePlist! "#" o      ���� 0 theplistpath thePListPath# $%$ o      ���� 0 plistvar plistVar% &��& o      ���� 0 thevalue theValue��  ��    Q     #'()' k    ** +,+ r    -.- b    /0/ b    
121 b    343 b    565 m    77 �88    - c   ' s e t  6 o    ���� 0 plistvar plistVar4 m    99 �::   2 o    	�� 0 thevalue theValue0 m   
 ;; �<<  '. o      �� 0 	mycommand 	myCommand, =�= r    >?> b    @A@ b    BCB m    DD �EE 2 / u s r / l i b e x e c / P l i s t B u d d y    C o    �� 0 theplistpath thePListPathA o    �� 0 	mycommand 	myCommand? o      �� 0 myscript myScript�  ( R      �F�
� .ascrerr ****      � ****F o      �� 
0 errmsg  �  ) I   #�G�
� .sysodisAaleR        TEXTG o    �� 
0 errmsg  �   HIH l     ����  �  �  I JKJ l     ����  �  �  K LML i   � �NON I      ���� 0 getepochtime getEpochTime�  �  O k     PP QRQ r     	STS c     UVU l    W��W I    �X�
� .sysoexecTEXT���     TEXTX m     YY �ZZ \ p e r l   - e   ' u s e   T i m e : : H i R e s   q w ( t i m e ) ;   p r i n t   t i m e '�  �  �  V m    �
� 
TEXTT o      �� 
0 mytime  R [\[ r   
 ]^] ]   
 _`_ o   
 �� 
0 mytime  ` m    ��  ��^ o      �� 
0 mytime  \ a�a L    bb I    �c�� 0 number_to_string  c d�d o    �� 
0 mytime  �  �  �  M efe l     ����  �  �  f ghg i   � �iji I      �k�� 0 number_to_string  k l�l o      �� 0 this_number  �  �  j k     �mm non r     pqp c     rsr o     �� 0 this_number  s m    �
� 
TEXTq o      �� 0 this_number  o t�t Z    �uv�wu E    	xyx o    �� 0 this_number  y m    zz �{{  E +v k    �|| }~} r    � l   ���� I   ���
� .sysooffslong    ��� null�  � ���
� 
psof� m    �� ���  .� ���
� 
psin� o    �� 0 this_number  �  �  �  � o      �� 0 x  ~ ��� r    #��� l   !���� I   !���
� .sysooffslong    ��� null�  � ���
� 
psof� m    �� ���  +� ���
� 
psin� o    �� 0 this_number  �  �  �  � o      �~�~ 0 y  � ��� r   $ /��� l  $ -��}�|� I  $ -�{�z�
�{ .sysooffslong    ��� null�z  � �y��
�y 
psof� m   & '�� ���  E� �x��w
�x 
psin� o   ( )�v�v 0 this_number  �w  �}  �|  � o      �u�u 0 z  � ��� r   0 E��� c   0 C��� c   0 A��� n   0 ?��� 7  1 ?�t��
�t 
cha � l  5 ;��s�r� \   5 ;��� o   6 7�q�q 0 y  � l  7 :��p�o� n   7 :��� 1   8 :�n
�n 
leng� o   7 8�m�m 0 this_number  �p  �o  �s  �r  � l 	 < >��l�k� m   < >�j�j���l  �k  � o   0 1�i�i 0 this_number  � m   ? @�h
�h 
TEXT� m   A B�g
�g 
nmbr� l     ��f�e� o      �d�d 0 decimal_adjust  �f  �e  � ��� Z   F c���c�� >  F I��� o   F G�b�b 0 x  � m   G H�a�a  � r   L ]��� c   L [��� n   L Y��� 7  M Y�`��
�` 
cha � m   Q S�_�_ � l  T X��^�]� \   T X��� o   U V�\�\ 0 x  � m   V W�[�[ �^  �]  � o   L M�Z�Z 0 this_number  � m   Y Z�Y
�Y 
TEXT� l     ��X�W� o      �V�V 0 
first_part  �X  �W  �c  � r   ` c��� m   ` a�� ���  � l     ��U�T� o      �S�S 0 
first_part  �U  �T  � ��� r   d w��� c   d u��� n   d s��� 7  e s�R��
�R 
cha � l  i m��Q�P� [   i m��� o   j k�O�O 0 x  � m   k l�N�N �Q  �P  � l  n r��M�L� \   n r��� o   o p�K�K 0 z  � m   p q�J�J �M  �L  � o   d e�I�I 0 this_number  � m   s t�H
�H 
TEXT� l     ��G�F� o      �E�E 0 second_part  �G  �F  � ��� r   x {��� l  x y��D�C� o   x y�B�B 0 
first_part  �D  �C  � l     ��A�@� o      �?�? 0 converted_number  �A  �@  � ��� Y   | ���>���=� Q   � ����� r   � ���� b   � ���� l 	 � ���<�;� l  � ���:�9� o   � ��8�8 0 converted_number  �:  �9  �<  �;  � n   � ���� 4   � ��7�
�7 
cha � o   � ��6�6 0 i  � l  � ���5�4� o   � ��3�3 0 second_part  �5  �4  � l     ��2�1� o      �0�0 0 converted_number  �2  �1  � R      �/�.�-
�/ .ascrerr ****      � ****�.  �-  � r   � ���� b   � ���� l  � ���,�+� o   � ��*�* 0 converted_number  �,  �+  � m   � ��� ���  0� l     ��)�(� o      �'�' 0 converted_number  �)  �(  �> 0 i  � m    ��&�& � l  � ���%�$� o   � ��#�# 0 decimal_adjust  �%  �$  �=  � ��"� L   � ��� l  � ���!� � o   � ��� 0 converted_number  �!  �   �"  �  w L   � �   o   � ��� 0 this_number  �  h  l     ����  �  �    i   � � I      ��� 0 add_leading_zeros   	 o      �� 0 this_number  	 
�
 o      �� 0 max_leading_zeros  �  �   k     G  r      c      l    �� a      m     �� 
 o    �� 0 max_leading_zeros  �  �   m    �
� 
long l     �� o      �� 0 threshold_number  �  �   � Z    G� A    o    	�� 0 this_number   l  	 
�
�	 o   	 
�� 0 threshold_number  �
  �	   k    @  r     !  m    "" �##  ! l     $��$ o      �� 0 leading_zeros  �  �   %&% r    '(' l   )��) n    *+* 1    �
� 
leng+ l   ,�� , c    -.- l   /����/ _    010 o    ���� 0 this_number  1 m    ���� ��  ��  . m    ��
�� 
TEXT�  �   �  �  ( l     2����2 o      ���� 0 digit_count  ��  ��  & 343 r    #565 \    !787 l   9����9 [    :;: o    ���� 0 max_leading_zeros  ; m    ���� ��  ��  8 o     ���� 0 digit_count  6 l     <����< o      ���� 0 character_count  ��  ��  4 =>= U   $ 7?@? r   + 2ABA c   + 0CDC l  + .E����E b   + .FGF l  + ,H����H o   + ,���� 0 leading_zeros  ��  ��  G m   , -II �JJ  0��  ��  D m   . /��
�� 
TEXTB l     K����K o      ���� 0 leading_zeros  ��  ��  @ o   ' (���� 0 character_count  > L��L L   8 @MM c   8 ?NON l  8 =P����P b   8 =QRQ o   8 9���� 0 leading_zeros  R l  9 <S����S c   9 <TUT o   9 :���� 0 this_number  U m   : ;��
�� 
ctxt��  ��  ��  ��  O m   = >��
�� 
TEXT��  �   L   C GVV c   C FWXW o   C D���� 0 this_number  X m   D E��
�� 
ctxt�   YZY l     ��������  ��  ��  Z [\[ l     ��������  ��  ��  \ ]^] i   � �_`_ I      ��a���� 00 roundandtruncatenumber roundAndTruncateNumbera bcb o      ���� 0 	thenumber 	theNumberc d��d o      ���� .0 numberofdecimalplaces numberOfDecimalPlaces��  ��  ` k     �ee fgf Z     hi����h =    jkj o     ���� .0 numberofdecimalplaces numberOfDecimalPlacesk m    ����  i k    ll mnm r    opo [    	qrq o    ���� 0 	thenumber 	theNumberr m    ss ?�      p o      ���� 0 	thenumber 	theNumbern t��t L    uu I    ��v���� 0 number_to_string  v w��w _    xyx o    ���� 0 	thenumber 	theNumbery m    ���� ��  ��  ��  ��  ��  g z{z l   ��������  ��  ��  { |}| r    ~~ m    �� ���  5 o      ���� $0 theroundingvalue theRoundingValue} ��� U    /��� r   % *��� b   % (��� m   % &�� ���  0� o   & '�� $0 theroundingvalue theRoundingValue� o      �� $0 theroundingvalue theRoundingValue� o   ! "�� .0 numberofdecimalplaces numberOfDecimalPlaces� ��� r   0 7��� c   0 5��� l  0 3���� b   0 3��� m   0 1�� ���  .� o   1 2�� $0 theroundingvalue theRoundingValue�  �  � m   3 4�
� 
nmbr� o      �� $0 theroundingvalue theRoundingValue� ��� l  8 8����  �  �  � ��� r   8 =��� [   8 ;��� o   8 9�� 0 	thenumber 	theNumber� o   9 :�� $0 theroundingvalue theRoundingValue� o      �� 0 	thenumber 	theNumber� ��� l  > >����  �  �  � ��� r   > A��� m   > ?�� ���  1� o      �� 0 themodvalue theModValue� ��� U   B U��� r   K P��� b   K N��� m   K L�� ���  0� o   L M�� 0 themodvalue theModValue� o      �� 0 themodvalue theModValue� \   E H��� o   E F�� .0 numberofdecimalplaces numberOfDecimalPlaces� m   F G�� � ��� r   V ]��� c   V [��� l  V Y���� b   V Y��� m   V W�� ���  .� o   W X�� 0 themodvalue theModValue�  �  � m   Y Z�
� 
nmbr� o      �� 0 themodvalue theModValue� ��� l  ^ ^����  �  �  � ��� r   ^ e��� _   ^ c��� l  ^ a���� `   ^ a��� o   ^ _�� 0 	thenumber 	theNumber� m   _ `�� �  �  � o   a b�� 0 themodvalue theModValue� o      �� 0 thesecondpart theSecondPart� ��� Z   f ������ A  f m��� n   f k��� 1   i k�
� 
leng� l  f i���� c   f i��� o   f g�� 0 thesecondpart theSecondPart� m   g h�
� 
ctxt�  �  � o   k l�� .0 numberofdecimalplaces numberOfDecimalPlaces� U   p ���� r   } ���� c   } ���� l  } ����� b   } ���� m   } ~�� ���  0� o   ~ �� 0 thesecondpart theSecondPart�  �  � m   � ��
� 
TEXT� o      ���� 0 thesecondpart theSecondPart� \   s z��� o   s t���� .0 numberofdecimalplaces numberOfDecimalPlaces� l  t y������ l  t y������ n   t y��� 1   w y��
�� 
leng� l  t w������ c   t w��� o   t u���� 0 thesecondpart theSecondPart� m   u v��
�� 
ctxt��  ��  ��  ��  ��  ��  �  �  � ��� l  � ���������  ��  ��  � ��� r   � ���� _   � ���� o   � ����� 0 	thenumber 	theNumber� m   � ����� � o      �� 0 thefirstpart theFirstPart� ��� r   � ���� I   � ��~��}�~ 0 number_to_string  � ��|� o   � ��{�{ 0 thefirstpart theFirstPart�|  �}  � o      �z�z 0 thefirstpart theFirstPart� ��� r   � ���� l  � ���y�x� b   � ���� b   � ���� o   � ��w�w 0 thefirstpart theFirstPart� m   � ��� �    .� o   � ��v�v 0 thesecondpart theSecondPart�y  �x  � o      �u�u 0 	thenumber 	theNumber�  l  � ��t�s�r�t  �s  �r   �q L   � � o   � ��p�p 0 	thenumber 	theNumber�q  ^  l     �o�n�m�o  �n  �m    l     �l�k�j�l  �k  �j   	
	 i   � � I      �i�h�i 0 trimline trimLine  o      �g�g 0 	this_text    o      �f�f 0 
trim_chars   �e o      �d�d 0 trim_indicator  �e  �h   k     {  l     �c�c   ' ! 0 = beginning, 1 = end, 2 = both    � B   0   =   b e g i n n i n g ,   1   =   e n d ,   2   =   b o t h  r      l    �b�a n      1    �`
�` 
leng l     �_�^  o     �]�] 0 
trim_chars  �_  �^  �b  �a   o      �\�\ 0 x   !"! l   �[#$�[  #   TRIM BEGINNING   $ �%%    T R I M   B E G I N N I N G" &'& Z    >()�Z�Y( E   *+* J    
,, -.- m    �X�X  . /�W/ m    �V�V �W  + l  
 0�U�T0 o   
 �S�S 0 trim_indicator  �U  �T  ) V    :121 Q    53453 r    +676 c    )898 n    ':;: 7   '�R<=
�R 
cha < l   #>�Q�P> [    #?@? o     !�O�O 0 x  @ m   ! "�N�N �Q  �P  = m   $ &�M�M��; o    �L�L 0 	this_text  9 m   ' (�K
�K 
TEXT7 o      �J�J 0 	this_text  4 R      �I�H�G
�I .ascrerr ****      � ****�H  �G  5 k   3 5AA BCB l  3 3�FDE�F  D 8 2 the text contains nothing but the trim characters   E �FF d   t h e   t e x t   c o n t a i n s   n o t h i n g   b u t   t h e   t r i m   c h a r a c t e r sC G�EG L   3 5HH m   3 4II �JJ  �E  2 C   KLK o    �D�D 0 	this_text  L l   M�C�BM o    �A�A 0 
trim_chars  �C  �B  �Z  �Y  ' NON l  ? ?�@PQ�@  P   TRIM ENDING   Q �RR    T R I M   E N D I N GO STS Z   ? xUV�?�>U E  ? EWXW J   ? CYY Z[Z m   ? @�=�= [ \�<\ m   @ A�;�; �<  X l  C D]�:�9] o   C D�8�8 0 trim_indicator  �:  �9  V V   H t^_^ Q   P o`ab` r   S ecdc c   S cefe n   S aghg 7  T a�7ij
�7 
cha i m   X Z�6�6 j d   [ `kk l  \ _l�5�4l [   \ _mnm o   \ ]�3�3 0 x  n m   ] ^�2�2 �5  �4  h o   S T�1�1 0 	this_text  f m   a b�0
�0 
TEXTd o      �/�/ 0 	this_text  a R      �.�-�,
�. .ascrerr ****      � ****�-  �,  b k   m ooo pqp l  m m�+rs�+  r 8 2 the text contains nothing but the trim characters   s �tt d   t h e   t e x t   c o n t a i n s   n o t h i n g   b u t   t h e   t r i m   c h a r a c t e r sq u�*u L   m ovv m   m nww �xx  �*  _ D   L Oyzy o   L M�)�) 0 	this_text  z l  M N{�(�'{ o   M N�&�& 0 
trim_chars  �(  �'  �?  �>  T |�%| L   y {}} o   y z�$�$ 0 	this_text  �%  
 ~~ l     �#�"�!�#  �"  �!   ��� l     � ���   �  �  � ��� i   � ���� I      ���� (0 getageofoldestfile getAgeOfOldestFile� ��� o      �� 0 filepath filePath� ��� o      �� 0 fileextension fileExtension�  �  � k     �� ��� r     	��� l    ���� I     ���� 0 getoldestfile getOldestFile� ��� o    �� 0 filepath filePath� ��� o    �� 0 fileextension fileExtension�  �  �  �  � o      �� 
0 myfile  � ��� Z   
 ����� >  
 ��� o   
 �� 
0 myfile  � m    �� ���  � L    �� I    ���� 0 getageoffile getAgeOfFile� ��� b    ��� o    �
�
 0 filepath filePath� o    �	�	 
0 myfile  �  �  �  � L    �� m    ��  �  � ��� l     ����  �  �  � ��� i   � ���� I      ���� 0 getageoffile getAgeOfFile� ��� o      �� 
0 myfile  �  �  � k     "�� ��� l     � ���   � 8 2 returns age based on modification time in seconds   � ��� d   r e t u r n s   a g e   b a s e d   o n   m o d i f i c a t i o n   t i m e   i n   s e c o n d s� ��� l     ������  � � } set shscript2 to "echo $(($(date +%s) - $(stat -t %s -f %m -- /Users/Ben/documents/waivers_pending/" & getOldestFile & ")))"   � ��� �   s e t   s h s c r i p t 2   t o   " e c h o   $ ( ( $ ( d a t e   + % s )   -   $ ( s t a t   - t   % s   - f   % m   - -   / U s e r s / B e n / d o c u m e n t s / w a i v e r s _ p e n d i n g / "   &   g e t O l d e s t F i l e   &   " ) ) ) "� ��� r     ��� b     ��� b     ��� m     �� ��� X e c h o   $ ( ( $ ( d a t e   + % s )   -   $ ( s t a t   - t   % s   - f   % m   - -  � o    ���� 
0 myfile  � m    �� ���  ) ) )� o      ���� 0 shscript  � ��� Q    ���� r    ��� c    ��� l   ������ I   �����
�� .sysoexecTEXT���     TEXT� o    ���� 0 shscript  ��  ��  ��  � m    ��
�� 
long� o      ����  0 fileageseconds fileAgeSeconds� R      �����
�� .ascrerr ****      � ****� o      ���� 
0 errmsg  ��  � r    ��� m    ��
�� 
null� o      ����  0 fileageseconds fileAgeSeconds� ���� L     "�� o     !����  0 fileageseconds fileAgeSeconds��  � ��� l     ��������  ��  ��  � ��� i   � ���� I      ������� 0 getoldestfile getOldestFile� ��� o      ���� 0 filepath filePath� ���� o      ���� 0 fileextension fileExtension��  ��  � k     &�� ��� l     ������  � c ] set shscript to "ls -tr /Users/Ben/documents/waivers_pending/ | grep '-e *.xml' | head -n 1"   � ��� �   s e t   s h s c r i p t   t o   " l s   - t r   / U s e r s / B e n / d o c u m e n t s / w a i v e r s _ p e n d i n g /   |   g r e p   ' - e   * . x m l '   |   h e a d   - n   1 "� ��� r     ��� b     ��� b     	��� b     ��� b     ��� m     �� ���  l s   - t r  � n    ��� 1    ��
�� 
strq� o    ���� 0 filepath filePath� m    �� ���    |   g r e p   ' - e   *� o    ���� 0 fileextension fileExtension� m   	 
�� ���  '   |   h e a d   - n   1� o      ���� 0 shscript  � ��� Q    #���� r    ��� I   �����
�� .sysoexecTEXT���     TEXT� o    ���� 0 shscript  ��  � o      ���� 0 
oldestfile 
oldestFile� R      �����
�� .ascrerr ****      � ****� o      ���� 
0 errmsg  ��  � r     #��� m     !   �  � o      ���� 0 
oldestfile 
oldestFile� �� L   $ & o   $ %���� 0 
oldestfile 
oldestFile��  �  l     ��������  ��  ��    l     ��������  ��  ��   	 i   � �

 I      ������ 60 launchaftereffectsproject launchAfterEffectsProject �� o      ���� $0 projectfileposix projectFilePOSIX��  ��   k       l     ����   ) # open file with default application    � F   o p e n   f i l e   w i t h   d e f a u l t   a p p l i c a t i o n  l     ��������  ��  ��    r      b      m      � 
 o p e n   n     1    ��
�� 
strq o    ���� $0 projectfileposix projectFilePOSIX o      ���� 0 shscript    !  Q    "#$" I   ��%��
�� .sysoexecTEXT���     TEXT% o    ���� 0 shscript  ��  # R      ��&��
�� .ascrerr ****      � ****& o      ���� 
0 errmsg  ��  $ l   ��'(��  '  
 something   ( �))    s o m e t h i n g! *�* l   ����  �  �  �  	 +,+ l     ����  �  �  , -.- i   � �/0/ I      ���� 00 archiveaftereffectslog archiveAfterEffectsLog�  �  0 k     c11 232 r     454 I     ���� 0 getepochtime getEpochTime�  �  5 o      �� 0 	timestamp  3 676 l   �89�  8 1 +TODO improve to work on different user path   9 �:: V T O D O   i m p r o v e   t o   w o r k   o n   d i f f e r e n t   u s e r   p a t h7 ;<; r    =>= b    ?@? b    ABA m    	CC �DD ^ / U s e r s / b l u e p i x e l / D o c u m e n t s / w a i v e r s _ p e n d i n g / l o g /B I   	 ���� 0 getepochtime getEpochTime�  �  @ m    EE �FF  . t x t> o      �� "0 archivefilepath archiveFilePath< GHG r    !IJI b    KLK b    MNM b    OPO m    QQ �RR  m v  P o    �� *0 aftereffectslogfile afterEffectsLogFileN m    SS �TT   L o    �� "0 archivefilepath archiveFilePathJ o      �� 0 shscript  H UVU r   " -WXW b   " +YZY m   " #[[ �\\  t o u c h  Z n   # *]^] 1   ( *�
� 
strq^ o   # (�� *0 aftereffectslogfile afterEffectsLogFileX o      �� 0 	shscript2  V _`_ Q   . Gabca I  1 6�d�
� .sysoexecTEXT���     TEXTd o   1 2�� 0 shscript  �  b R      �e�
� .ascrerr ****      � ****e o      �� 
0 errmsg  �  c k   > Gff ghg I   > E�i�� $0 shownotification showNotificationi jkj m   ? @ll �mm $ E r r o r   A r c h v i n g   L o gk n�n o   @ A�� 
0 errmsg  �  �  h o�o l  F F����  �  �  �  ` pqp l  H H����  �  �  q rsr Q   H atuvt I  K P�w�
� .sysoexecTEXT���     TEXTw o   K L�� 0 	shscript2  �  u R      �x�
� .ascrerr ****      � ****x o      �� 
0 errmsg  �  v k   X ayy z{z I   X _�|�� $0 shownotification showNotification| }~} m   Y Z ��� , E r r o r   C r e a t i n g   N e w   L o g~ ��� o   Z [�� 
0 errmsg  �  �  { ��� l  ` `����  �  �  �  s ��� l  b b����  �  �  �  . ��� l     ����  �  �  � ��� i   � ���� I      ���� 0 killapp killApp� ��~� o      �}�} "0 applicationname applicationName�~  �  � k     /�� ��� r     ��� b     ��� m     �� ���  k i l l a l l  � n    ��� 1    �|
�| 
strq� o    �{�{ "0 applicationname applicationName� o      �z�z 0 shscript  � ��� Q    '���� k    �� ��� I   �y��x
�y .sysoexecTEXT���     TEXT� o    �w�w 0 shscript  �x  � ��� r    ��� m    �v
�v boovtrue� o      �u�u 0 	mysuccess 	mySuccess� ��t� r    ��� m    �� ���  � o      �s�s 0 	mymessage 	myMessage�t  � R      �r��q
�r .ascrerr ****      � ****� o      �p�p 
0 errmsg  �q  � k     '�� ��� r     #��� m     !�o
�o boovfals� o      �n�n 0 	mysuccess 	mySuccess� ��m� r   $ '��� o   $ %�l�l 
0 errmsg  � o      �k�k 0 	mymessage 	myMessage�m  � ��� l  ( (�j�i�h�j  �i  �h  � ��� L   ( -�� J   ( ,�� ��� o   ( )�g�g 0 	mysuccess 	mySuccess� ��f� o   ) *�e�e 0 	mymessage 	myMessage�f  � ��d� l  . .�c�b�a�c  �b  �a  �d  � ��� l     �`�_�^�`  �_  �^  � ��� i   ���� I      �]�\�[�] 20 checkaftereffectsstatus checkAfterEffectsStatus�\  �[  � k     D�� ��� l     �Z�Y�X�Z  �Y  �X  � ��� r     ��� I     
�W��V�W 0 getageoffile getAgeOfFile� ��U� o    �T�T *0 aftereffectslogfile afterEffectsLogFile�U  �V  � o      �S�S &0 logfileageseconds logFileAgeSeconds� ��� l   �R�Q�P�R  �Q  �P  � ��� Z    <���O�� G    ��� =   ��� o    �N�N &0 logfileageseconds logFileAgeSeconds� m    �M
�M 
null� ?    ��� o    �L�L &0 logfileageseconds logFileAgeSeconds� l   ��K�J� ]    ��� o    �I�I 00 expectedmaxprocesstime expectedMaxProcessTime� m    �H�H �K  �J  � k   ! 2�� ��� l  ! !�G���G  � a [ AE is likely hung, kill it and relaunch project, but we will finish submitting stats first   � ��� �   A E   i s   l i k e l y   h u n g ,   k i l l   i t   a n d   r e l a u n c h   p r o j e c t ,   b u t   w e   w i l l   f i n i s h   s u b m i t t i n g   s t a t s   f i r s t� ��� l  ! !�F�E�D�F  �E  �D  � ��� r   ! $��� m   ! "�C
�C boovfals� o      �B�B 0 	mysuccess 	mySuccess� ��� r   % (��� m   % &�� ��� H p l e a s e   r e s t a r t   A f t e r   E f f e c t s   p r o j e c t� o      �A�A 0 	mymessage 	myMessage� ��� I   ) 0�@��?�@ $0 shownotification showNotification� ��� m   * +�� ���   A E   H a n g   D e t e c t e d� ��>� m   + ,�� ��� < w i l l   t r y   t o   r e s t a r t   i t   f o r   y o u�>  �?  � ��� l  1 1�=�<�;�=  �<  �;  � ��:� l  1 1�9�8�7�9  �8  �7  �:  �O  � k   5 <�� ��� l  5 5�6���6  �   appears to be OK   � ��� "   a p p e a r s   t o   b e   O K�    r   5 8 m   5 6�5
�5 boovtrue o      �4�4 0 	mysuccess 	mySuccess �3 r   9 < m   9 : �   o      �2�2 0 	mymessage 	myMessage�3  � 	
	 l  = =�1�0�/�1  �0  �/  
  L   = B J   = A  o   = >�.�. 0 	mysuccess 	mySuccess �- o   > ?�,�, 0 	mymessage 	myMessage�-    l  C C�+�*�)�+  �*  �)   �( l  C C�'�&�%�'  �&  �%  �(  �  l     �$�#�"�$  �#  �"   �! i   I      � ���  *0 recycleaftereffects recycleAfterEffects�  �   k     /  l     ��   6 0 AE is likely hung, kill it and relaunch project    � `   A E   i s   l i k e l y   h u n g ,   k i l l   i t   a n d   r e l a u n c h   p r o j e c t  !  I     �"�� $0 shownotification showNotification" #$# m    %% �&& " A E   N o n   R e s p o n s i v e$ '�' m    (( �)) 0 t r y i n g   a u t o m a t e d   r e s t a r t�  �  ! *+* l   ����  �  �  + ,-, I    �.�� 0 killapp killApp. /�/ m   	 
00 �11  A f t e r   E f f e c t s�  �  - 232 I   �4�
� .sysodelanull��� ��� nmbr4 m    �� 
�  3 565 l   ����  �  �  6 787 l   �9:�  9 R L archive log file so we won't hit the error here again while AE is rebooting   : �;; �   a r c h i v e   l o g   f i l e   s o   w e   w o n ' t   h i t   t h e   e r r o r   h e r e   a g a i n   w h i l e   A E   i s   r e b o o t i n g8 <=< I    ���
� 00 archiveaftereffectslog archiveAfterEffectsLog�  �
  = >?> l   �	���	  �  �  ? @A@ l   �BC�  B "  launch AE with project file   C �DD 8   l a u n c h   A E   w i t h   p r o j e c t   f i l eA EFE I    "�G�� $0 shownotification showNotificationG HIH m    JJ �KK  R e s t a r t i n g   A EI L�L m    MM �NN , t h i s   m a y   t a k e   a   m i n u t e�  �  F OPO I   # -�Q�� 60 launchaftereffectsproject launchAfterEffectsProjectQ R� R o   $ )���� 20 aftereffectsprojectfile afterEffectsProjectFile�   �  P S��S l  . .��������  ��  ��  ��  �!       ?��T   UVW���������� V [������������ z��XYZ[\]^_`abcdefghijklmnopqrstuvwxyz{|}~��  T =������������������������������������������������������������������������������������������������������������� 0 theplistpath thePListPath�� *0 aftereffectslogfile afterEffectsLogFile�� 20 aftereffectsprojectfile afterEffectsProjectFile�� 00 incomingfileextensions incomingFileExtensions�� 80 rawtranscodefileextensions rawTranscodeFileExtensions�� D0  bypassrawtranscodefileextensions  bypassRawTranscodeFileExtensions�� 60 statsintervalcheckminutes statsIntervalCheckMinutes�� $0 processdelaysecs processDelaySecs�� 0 loopdelaysecs loopDelaySecs�� 0 	automated  �� 0 devendpoints devEndpoints�� $0 computerusername computerUserName�� $0 waivergrepstring waiverGrepString�� >0 filesizecheck_minaeoutputsize filesizeCheck_minAEOutputSize�� (0 filesizecheckdelay filesizeCheckDelay�� (0 performfilecleanup performFileCleanup�� 0 dev_mode  �� 00 oldestfileagethreshold oldestFileAgeThreshold�� 00 expectedmaxprocesstime expectedMaxProcessTime�� "0 applicationname applicationName�� $0 maxfilespercycle maxFilesPerCycle
�� .aevtoappnull  �   � ****
�� .miscidlenmbr    ��� null
�� .aevtquitnull��� ��� null�� $0 shownotification showNotification�� "0 processonecycle processOneCycle�� &0 checkpendingfiles checkPendingFiles�� 0 filecleanup fileCleanup�� ,0 movefilestoingestion moveFilesToIngestion�� :0 transcodeaftereffectsoutput transcodeAfterEffectsOutput�� 0 handlewaivers handleWaivers�� (0 checkrawfilestatus checkRawFileStatus�� ,0 checkfilewritestatus checkFileWriteStatus�� 0 gatherstats gatherStats�� "0 sendstatustobpc sendStatusToBPC�� 0 setvars setVars�� 0 setpaths setPaths�� 0 read_waiver  �� 0 get_element  �� 0 
fileexists 
fileExists�� 0 folderexists folderExists�� 0 	changeext 	changeExt��  0 getfreediskpct getFreeDiskPct�� (0 getfreediskspacegb getFreeDiskSpaceGB�� 0 getdiskstats getDiskStats�� 0 
is_running  � 0 	readplist 	readPlist� 0 
writeplist 
writePlist� 0 getepochtime getEpochTime� 0 number_to_string  � 0 add_leading_zeros  � 00 roundandtruncatenumber roundAndTruncateNumber� 0 trimline trimLine� (0 getageofoldestfile getAgeOfOldestFile� 0 getageoffile getAgeOfFile� 0 getoldestfile getOldestFile� 60 launchaftereffectsproject launchAfterEffectsProject� 00 archiveaftereffectslog archiveAfterEffectsLog� 0 killapp killApp� 20 checkaftereffectsstatus checkAfterEffectsStatus� *0 recycleaftereffects recycleAfterEffectsU ��� �   ! % ) ,V ��� �   3W ��� �   ; ? B�� �� �� 

�� boovfals
�� boovfals�� ��@�� 
�� boovtrue
�� boovfals�� �� ��� 
X � ������
� .aevtoappnull  �   � ****�  �  �  � � �� � ��� �����������

� .misccurdldt    ��� null
� .ascrcmnt****      � ****� $0 shownotification showNotification� 0 
fileexists 
fileExists
� .sysodisAaleR        TEXT
� .aevtquitnull��� ��� null� 0 ishappy isHappy� 0 setvars setVars� 0 isready1 isReady1� 0 setpaths setPaths� 0 isready2 isReady2
� 
bool� 0 gatherstats gatherStats� "0 processonecycle processOneCycle� �*j  �%j O*��l+ O*b   k+ f  �b   %j O*j 	Y hOeE�O*j+ E�O*j+ E�O�e 	 �e �& *jk+ O*ek+ Y a �%a %�%j O*j  a %j OPY ������
� .miscidlenmbr    ��� null�  �  �  � ��*,�� "0 processonecycle processOneCycle
� .misccurdldt    ��� null
� .ascrcmnt****      � ****� "*fk+  O*j �%b  %�%j Ob  Z �5�����
� .aevtquitnull��� ��� null�  �  �  � �=���
� .misccurdldt    ��� null
� .ascrcmnt****      � ****� 0 gatherstats gatherStats
� .aevtquitnull��� ��� null� *j  �%j O*jk+ O)jd* [ �J������ $0 shownotification showNotification� ��� �  ���  0 subtitlestring subtitleString� 0 messagestring messageString�  � ���  0 subtitlestring subtitleString� 0 messagestring messageString� ������~
� 
null
� 
appr
� 
subt� 
� .sysonotfnull��� ��� TEXT
�~ .sysodelanull��� ��� nmbr� ,�� ��b  �� Y *��b  � Okj OP\ �}o�|�{���z�} "0 processonecycle processOneCycle�| �y��y �  �x�x  0 isinitialcycle isInitialCycle�{  � �w�w  0 isinitialcycle isInitialCycle� �vy�u��t�s�r�q��p�o��n��m������l�k�j�i
�v .misccurdldt    ��� null
�u .ascrcmnt****      � ****
�t 
null�s $0 shownotification showNotification�r 0 gatherstats gatherStats�q 0 handlewaivers handleWaivers
�p .sysodelanull��� ��� nmbr�o :0 transcodeaftereffectsoutput transcodeAfterEffectsOutput�n ,0 movefilestoingestion moveFilesToIngestion�m 0 filecleanup fileCleanup
�l 
givu�k 
�j .sysodisAaleR        TEXT
�i .aevtquitnull��� ��� null�z *j  �%j O*��l+ O*kk+ O*j+ O*j  �%b  %j Ob  j 	O*kk+ O*j+ 
O*j  �%b  %j Ob  j 	O*kk+ O*j+ O*j  �%b  %j Ob  j 	O*j+ O*j  �%b  %j Ob  j 	O*kk+ O*j  a %j O� *a a l+ Y *a a b  %a %l+ Ob  	f  &*j  a %j Oa a a l O*j Y hOP] �h>�g�f���e�h &0 checkpendingfiles checkPendingFiles�g �d��d �  �c�b�c 0 oldlist oldList�b 0 newlist newList�f  � �a�`�_�^�]�\�[�a 0 oldlist oldList�` 0 newlist newList�_ 0 
filesadded 
filesAdded�^ 0 filesremoved filesRemoved�] 0 isprocessing isProcessing�\ 0 i  �[ 0 n  � �Z�Y�X~�W�V�U��T�
�Z 
list
�Y 
leng
�X 
bool
�W .ascrcmnt****      � ****
�V 
cobj
�U .corecnte****       ****
�T 
TEXT�e �jv�&E�Ojv�&E�OfE�O��,��,	 	��,j�& ���  �j OfY hO *k��-j kh ��/E�O�� 	��6FY h[OY��O *k��-j kh ��/E�O�� 	��6FY h[OY��O�%�&j O�%j O��,j	 	��,j �& fY hO��,j	 	��,j�& eY hOPY �j OeE�O�OP^ �S"�R�Q���P�S 0 filecleanup fileCleanup�R  �Q  � �O�N�O 0 shscript  �N 
0 errmsg  � 0�M�L�K
�M .sysoexecTEXT���     TEXT�L 
0 errmsg  �K  �P (b  e  �E�O 
�j W X  hOPY hOP_ �JK�I�H���G�J ,0 movefilestoingestion moveFilesToIngestion�I  �H  � �F�E�D�C�B�A�@�?�>�=�<�F 0 myfolder  �E 0 	pmyfolder  �D 0 shscript  �C 0 filelist  �B 
0 errmsg  �A 0 
cyclecount 
cycleCount�@ 0 i  �? 
0 myfile  �> 0 newfile  �= 0 basefilename baseFilename�< $0 intermediatelist intermediateList� +�;�:�9_�8i�7m�6�5�4�3�2���1�0�/�.�-�,���+���*�)�(�' �&.�%24MRlp��; 0 processed_media  
�: 
psxp
�9 .misccurdldt    ��� null
�8 .ascrcmnt****      � ****
�7 
strq
�6 .sysoexecTEXT���     TEXT
�5 
cpar
�4 
list�3 
0 errmsg  �2  
�1 
kocl
�0 
cobj
�/ .corecnte****       ****
�. 
TEXT�-  B@�, ,0 checkfilewritestatus checkFileWriteStatus�+ 0 spock_ingestor  
�* .sysobeepnull��� ��� long
�) 
givu�( 
�' .sysodisAaleR        TEXT�& 0 	changeext 	changeExt�% 0 
media_pool  �G��E�O��,E�O*j �%j O��,%�%E�O �j �-�&E�W !X  *j �%�%j O�j Ojv�&E�OjE�Ob�[�a l kh �b   Y hO��%a &E�O�E�O*�a km+ e a ��,%a %_ �,%�%E�O *j a %j O�j W :X  *j a %�%j O*j O*j O*j Oa �%a a l OO*�a &a l+  E�Oa !_ "�,%a #%�%a $%E�O �j �-�&E�W %X  *j a %%�%j Oa &j Ojv�&E�O M�[�a l kh a '_ "�,%a (%�%a &E�O 
�j W X  *j a )%�%j OP[OY��O�kE�OPY hOP[OY��O*j a *%j OP` �$��#�"���!�$ :0 transcodeaftereffectsoutput transcodeAfterEffectsOutput�#  �"  � � �����������������  0 myfolder  � 0 completed_waivers  � 0 shscript  � 0 filelist  � 
0 errmsg  � 0 
cyclecount 
cycleCount� 0 i  �  0 outputfilename outputFilename� 0 	filesize1  � 0 	filesize2  �  0 filesize1clean filesize1Clean�  0 filesize2clean filesize2Clean� 0 skipthisfile skipThisFile� &0 skipthisfileclean skipThisFileClean� 
0 myfile  � 0 	shscript1  � 0 	shscript3  � 0��������
��	����������� O����ht����������'+����Be��s������� 0 	ae_output  � @0 pendingwaiversforaefolderposix pendingWaiversForAEFolderPosix
� 
psxp
� 
TEXT
� .misccurdldt    ��� null
�
 .ascrcmnt****      � ****
�	 
strq
� .sysoexecTEXT���     TEXT
� 
cpar
� 
list� 
0 errmsg  �  
� 
kocl
� 
cobj
� .corecnte****       ****�  0 	changeext 	changeExt
�� 
psxf
�� .rdwrgeofcomp       ****
�� .sysodelanull��� ��� nmbr
�� 
bool�� 0 	ame_watch  �� %0 !waivers_pending_completed_archive  
�� .sysodlogaskr        TEXT
�� .sysobeepnull��� ��� long
�� 
givu�� 
�� .sysodisAaleR        TEXT�!P�E�O��%�,�&E�O*j �%j O��,%�%E�O �j �-�&E�W !X  *j �%j Oa j Ojv�&E�OjE�O�[a a l kh *��&a l+ E�O�b   Y hOjE�OkE�OjE�OkE�OfE�OfE�O �hZ*j a %j O��%�&E�O*a �/j E�O*j a %�%j O�b   a j Okj Y hO*a �/j E�O*j a %�%j O�� 	 �b  a & *j a %j OY hO*j a %���&%j O�b  
 	��a & a  j OeE�OY h[OY�/O�f 	 	�f a & �a !��,%�%a "%_ #�,%�%E�Oa $��,%�%a %%_ &�,%�%E^ Ob  e  
�j 'Y hO (*j a (%j O�j O] j O�kE�OPW :X  *j a )%�%j O*j *O*j *O*j *Oa +�%a ,a -l .OOPY hOP[OY�-O*j a /%j OPa ������������� 0 handlewaivers handleWaivers��  ��  � ���������������������������� 60 incomingwaiverfolderposix incomingWaiverFolderPosix�� 0 shscript  ��  0 waiverfilelist waiverFileList�� 
0 errmsg  �� 0 
cyclecount 
cycleCount�� 0 outteri outterI�� 0 
waiverfile 
waiverFile�� 0 mywaiver myWaiver�� 0 rawfileslist rawFilesList�� 0 movefileslist moveFilesList�� 0 myincrementer myIncrementer�� 0 	myrawfile 	myRawFile�� 0 success  �  ��������������������������������������V\��������������
�� .misccurdldt    ��� null
�� .ascrcmnt****      � ****�� .0 incomingrawxmlwaivers incomingRawXmlWaivers
�� 
strq
�� .sysodlogaskr        TEXT
�� .sysoexecTEXT���     TEXT
�� 
cpar
�� 
list�� 
0 errmsg  ��  
�� 
kocl
�� 
cobj
�� .corecnte****       ****�� 0 read_waiver  �� 0 waiver_total_raw_files  
�� 
null
�� 
bool�� 0 get_element  �� (0 checkrawfilestatus checkRawFileStatus�� @0 pendingwaiversforaefolderposix pendingWaiversForAEFolderPosix
�� 
TEXT
�� .sysodisAaleR        TEXT��X*j  �%j O�E�O��,%�%b  %E�Ob  e  
�j Y hO �j �-�&E�W X  *j  �%j O�j Ojv�&E�OjE�O ��[��l kh �b   Y hO��%E�O*�k+ E�OjvE�OjvE�O�a ,a 	 �a ,ja & 5jE�O +�a ,Ekh�kE�O*�a a �%m+ E�O��6G[OY��Y ��6GO*�k+  Ta ��,%a %_ %�a &%E�OfE�O �j O�kE�OeE�W  X  *j  a %�%j Oa �%j Y hOP[OY�!OPb ������������� (0 checkrawfilestatus checkRawFileStatus�� ����� �  ���� 0 rawfileslist rawFilesList��  � �������������������������������������� 0 rawfileslist rawFilesList�� "0 filereadystatus fileReadyStatus��  0 skiptranscoded skipTranscoded�� 0 movefileslist moveFilesList�� 0 i  �� 0 
namelength  �� 
0 myname  �� 0 fileextension fileExtension�� $0 mytranscodedfile myTranscodedFile�� 0 	myrawfile 	myRawFile��  0 foundextension foundExtension�� 0 shscript  �� 0 success  �� $0 filemove_success fileMove_success�� 
0 errmsg  �� 0 rawfilei rawFileI�� 
0 myfile  �� 0 pmyfile  � '��������������X��f��������������������������������	=	I��	d	h�	�	�	�
�� .sysodlogaskr        TEXT
�� 
kocl
�� 
cobj
�� .corecnte****       ****
�� 
TEXT
�� 
cha �� �� >0 transcodecompletedfolderposix transcodeCompletedFolderPosix�� :0 incomingrawmediafolderposix incomingRawMediaFolderPosix����� ,0 checkfilewritestatus checkFileWriteStatus
�� 
strq�� :0 transcodependingfolderposix transcodePendingFolderPosix
�� .sysoexecTEXT���     TEXT�� 
0 errmsg  ��  
�� .misccurdldt    ��� null
�� .ascrcmnt****      � ****
�� .sysodisAaleR        TEXT
�� 
leng
�� 
bool
�� 
psxp� 0 
media_pool  ��*b  e  �%j Y hOfE�OfE�OjvE�O�[��l kh ��&�-j E�O�[�\[Zk\Z��2�&E�O �b  [��l kh ��%�%�%�&E�O��%�%�%�&E�O�E�O*��km+  eE�O��%�&�6GOY hO*�jkm+  �fE�Ob  � ��,%a %_ %��%%E�Y #b  � a ��,%a %�%��%%E�Y hOfE�O �j OeE�OOPW $X  *j a %�%j Oa �%j OfE�OPY hOP[OY�.OP[OY��O�e 	 �a ,�a , a & � ��[��l kh b  e  a �%�&j Y hOa �%�%�&E^ O] a  ,E^ Oa !] �,%a "%_ #%�%E�OfE�Ob  e  
�j Y hO �j OeE�W &X  *j a $%�%j Oa %�%j OfE�OOP[OY�cO�OPY b  e  a &j Y hOfOPO�OPc �	������� ,0 checkfilewritestatus checkFileWriteStatus� ��� �  ���� 0 pmyfile  � "0 expectedminsize expectedMinSize� &0 checkdelayseconds checkDelaySeconds�  � ������� 0 pmyfile  � "0 expectedminsize expectedMinSize� &0 checkdelayseconds checkDelaySeconds�  0 thisfilestatus thisFileStatus� 0 	filesize1  � 0 	filesize2  � ������
� 
null� 0 
fileexists 
fileExists
� 
psxf
� .rdwrgeofcomp       ****
� 
bool
� .sysodelanull��� ��� nmbr� lfE�OjE�OkE�O��  kE�Y hO*�k+  E*�/j E�O�k
 ���& fY hO�j O*�/j E�O��  eY fOPY fO�d �
2������ 0 gatherstats gatherStats� ��� �  �� 0 intervalmins intervalMins�  � �������~�}�|�{�z�y�x�w�v�u�t�s�r�q�p�o�n�m�l�k�j�i�h� 0 intervalmins intervalMins� 0 
initialrun 
initialRun� 0 timenow timeNow� 0 	nextcheck 	nextCheck� 40 updatestalefiletimestamp updateStaleFileTimestamp� 0 oldestfileage oldestFileAge�~ 0 	dataitems 	dataItems�} 0 failedtests failedTests�| *0 recycleaftereffects recycleAfterEffects�{ 0 	firstitem 	firstItem�z 0 ae_test  �y 00 aftereffectsresponsive afterEffectsResponsive�x 0 ame_test  �w 0 nextitem nextItem�v 0 dropbox_test  �u 0 shscript  �t 0 mylist myList�s 
0 errmsg  �r 0 mycount myCount�q 0 oldcount oldCount�p 0 ae_waiver_delta  �o 0 isprocessing isProcessing�n 0 mydatavalue myDataValue�m 0 mythreshhold myThreshhold�l 0 ame_file_delta  �k 0 dropbox_waiver_delta  �j 0 memory_stats  �i 0 freediskpct freeDiskPct�h 0 
freediskgb 
freeDiskGB� ��g�f�e�d�c�b�a�`
�
�
�
��_
��^
�
��]
�
��\�[�Z )>GLPZir�������Y�X��W�V�U�T�S��R�Q��P�O��N�M�LV^{}������K������J	/9GISf�Ij�����H#%/?AK\npz��G������F,4JLVfhr����������E�D�C�B�A	�@%57AVY`jlv�?�>������������$46@TV`�=�<�g 0 getepochtime getEpochTime�f 80 laststatsgatheredtimestamp lastStatsGatheredTimestamp�e <�d  ���c 0 number_to_string  �b :0 laststalefilechecktimestamp lastStaleFileCheckTimestamp�a *0 forcestalefilecheck forceStaleFileCheck�` 0 ishappy isHappy�_ 0 
is_running  �^ 0 
writeplist 
writePlist
�] .sysodlogaskr        TEXT�\ 20 checkaftereffectsstatus checkAfterEffectsStatus
�[ 
list
�Z 
cobj�Y @0 pendingwaiversforaefolderposix pendingWaiversForAEFolderPosix
�X 
strq
�W .sysoexecTEXT���     TEXT
�V 
cpar�U 
0 errmsg  �T  
�S .misccurdldt    ��� null
�R .ascrcmnt****      � ****
�Q 
leng
�P 
null�O 0 	readplist 	readPlist
�N 
bool�M 00 pendingaewaiverlistold pendingAEWaiverListOLD�L &0 checkpendingfiles checkPendingFiles�K (0 getageofoldestfile getAgeOfOldestFile
�J .corecnte****       ****�I 0 	ame_watch  �H .0 pendingamefilelistold pendingAMEFileListOLD�G .0 incomingrawxmlwaivers incomingRawXmlWaivers�F :0 pendingdropboxwaiverlistold pendingDropboxWaiverListOLD�E  0 getfreediskpct getFreeDiskPct
�D 
long�C (0 getfreediskspacegb getFreeDiskSpaceGB�B �A 

�@ .sysodisAaleR        TEXT
�? .sysosigtsirr   ��� null
�> 
siip�= "0 sendstatustobpc sendStatusToBPC�< *0 recycleaftereffects recycleAfterEffects�	yfE�O�j (*j+  E�O*��� � k+ E�O�� hY hY *j+  E�O*j+  E�OfE�OeE�OeE�OfE�OjE�O�E�O�E�OfE�O�E�O*�k+ E�O*b   ��m+ O�f  0fE�O��%E�Ob  e  a j Y hOa E�OeE�OPY a E�O��%E�O*j+ a &E�O�a k/f  eE�O�a %E�Y hO*a k+ E�O*b   a �m+ O�f  ,fE�O�a %E�Ob  e  a j Y hOa E�Y a E�O�a %�%E�O*a k+ E�O*b   a  �m+ O�f  ,fE�O�a !%E�Ob  e  a "j Y hOa #E�Y a $E�O�a %%�%E�Oa &_ 'a (,%a )%E�O �j *a +-a &E^ W X , -*j .a /%] %j 0OjvE^ O] a 1,E^ O*b   a 2a 3m+ 4E^ O] ] E^ O*b   a 5] m+ O*b   a 6] m+ O*j+  E�O*�b  � k+ E�O��
 	�e a 7&
 	�e a 7& T*_ 8] l+ 9E^ O] E` 8O] f  *eE�OfE�O�a :%E�Ob  e  a ;j Y hY hOeE�OPY hO] E^ Oa <] %a =%E�O�a >%�%E�O] E^ Oa ?] %a @%E�O�a A%�%E�O*_ 'a Bl+ CE^ O] � 
] E�Y hOa D] %a E%E�O�a F%�%E�OjE^ Oa G_ 'a (,%a H%E�O -�j *a +-a &j IE^ O*b   a J] m+ OPW X , -*j .a K%] %j 0OjE^ O] ]  &fE�O�a L%E�Ob  e  a Mj Y hY hO] E^ Oa N] %a O%E�O�a P%�%E�Oa Q_ Ra (,%a S%E�O �j *a +-a &E^ W X , -*j .a T%] %j 0OjvE^ O] a 1,E^ O*b   a Ua 3m+ 4E^ O] ] E^ O*b   a V] m+ O*b   a W] m+ O*j+  E�O*�b  � k+ E�O��
 	�e a 7&
 	�e a 7& R*_ X] l+ 9E^ O] E` XO] f  *fE�OeE�O�a Y%E�Ob  e  a Zj Y hY hOeE�Y hO] E^ Oa [] %a \%E�O�a ]%�%E�O] E^ Oa ^] %a _%E�O�a `%�%E�O*_ Ra al+ CE^ O] � 
] E�Y hOa b] %a c%E�O�a d%�%E�Oa e_ fa (,%a g%E�O �j *a +-a &E^ W X , -*j .a h%] %j 0OjvE^ O] a 1,E^ O*b   a ia 3m+ 4E^ O] ] E^ O*b   a j] m+ O*b   a k] m+ O*j+  E�O*�b  � k+ E�O��
 	�e a 7&
 	�e a 7& R*_ l] l+ 9E^ O] E` lO] f  *fE�OeE�O�a m%E�Ob  e  a nj Y hY hOeE�Y hO] E^ Oa o] %a p%E�O�a q%�%E�O] E^ Oa r] %a s%E�O�a t%�%E�O*_ fa ul+ CE^ O] � 
] E�Y hOa v] %a w%E�O�a x%�%E�O a yj *E^ W X , -a z] %E^ O] E^ Oa {] %a |%E�O�a }%�%E�O*j+ ~a &E^ O*j+ �a &E^ O] a �
 ] a �a 7& &fE�O�a �%E�Ob  e  a �j �Y hY hO] E^ Oa �] %a �%E�O�a �%�%E�O] E^ Oa �] %a �%E�O�a �%�%E�O *b   a �a �m+ 4E^ W X , -a �] %E^ Oa �] %a �%E�O�a �%�%E�O*j �a �,E^ Oa �] %a �%E�O�a �%�%E�O�E^ Oa �] %a �%E�O�a �%�%E�O�b   �a �%E�Y hO�e  a �E^ OfE�Y a �E^ Ob  e  a �j �Y hOa �] %a �%E�O�a �%�%E�O�E^ Oa �] %a �%E�O�a �%�%E�O*j+  E^ Oa �] %a �%E�O�a �%�%E�O*j �a �,E^ Oa �] %a �%E�O�a �%�%E�Ob  e  
�j Y hO*�k+ �O*j+  E�O�e  *j+  E�Y hO�e  
*j+ �Y hOPe �;��:�9���8�; "0 sendstatustobpc sendStatusToBPC�: �7��7 �  �6�6 0 	dataitems 	dataItems�9  � �5�4�3�2�1�0�/�5 0 	dataitems 	dataItems�4 0 myip myIP�3 0 
theurlbase 
theURLbase�2 0 
thepayload 
thePayload�1 0 shscript  �0 0 	myoutcome 	myOutcome�/ 
0 errmsg  � �.�-���,�+�������*��)�(�'�&
�. .sysosigtsirr   ��� null
�- 
siip�, 0 	readplist 	readPlist
�+ 
bool
�* 
strq
�) .sysodlogaskr        TEXT
�( .sysoexecTEXT���     TEXT�' 
0 errmsg  �&  �8 �*j  �,E�O�E�O*b   �b  
m+ Ec  
Ob  
�&Ec  
Ob  
e  
�%E�Y �%E�O�%�%�%�%E�O��,%�%�%E�Ob  e  
�j Y hO �j E�O�W 	X  �OPf �%�$�#���"�% 0 setvars setVars�$  �#  � �!�! 
0 errmsg  � � ���@���N^�l|����������
�  
list� 00 pendingaewaiverlistold pendingAEWaiverListOLD� .0 pendingamefilelistold pendingAMEFileListOLD� :0 pendingdropboxwaiverlistold pendingDropboxWaiverListOLD
� 
null� 0 	readplist 	readPlist� 0 computer_id  
� 
bool� 0 stationtype stationType
� 
long� 
0 errmsg  �  �"K@jv�&E�Ojv�&E�Ojv�&E�O*b   ��m+ E�O*b   �b  m+ Ec  O*b   �b  m+ �&Ec  O*b   �b  
m+ �&Ec  
O*b   �em+ �&Ec  O*b   ��m+ E�O*b   �b  	m+ �&Ec  	O*b   a b  m+ a &Ec  O*b   a b  m+ a &Ec  O*b   a b  m+ �&Ec  O*b   a b  m+ a &Ec  O*b   a b  m+ a &Ec  OeW 	X  �OPg �"������ 0 setpaths setPaths�  �  � ��� 0 filetest fileTest� 
0 errmsg  � 236��E�S�[�
gi�	������������������� ��%'��=KM��cqs�������� 0 	readplist 	readPlist� "0 dropboxrootpath dropboxRootPath� 0 stationtype stationType� .0 incomingrawxmlwaivers incomingRawXmlWaivers�
 :0 incomingrawmediafolderposix incomingRawMediaFolderPosix�	 :0 transcodependingfolderposix transcodePendingFolderPosix� 0 folderexists folderExists� 
0 errmsg  �  
� 
strq
� .sysoexecTEXT���     TEXT� >0 transcodecompletedfolderposix transcodeCompletedFolderPosix� @0 pendingwaiversforaefolderposix pendingWaiversForAEFolderPosix� %0 !waivers_pending_completed_archive  �  0 
media_pool  ��  �� 0 processed_media  �� 0 	ae_output  �� 0 	ame_watch  �� 0 spock_ingestor  ���*b   ��m+ E�Ob  
e  ��%�%E�Y ��%E�O��%E�O��%E�O�b  %�%E�O *�k+ E�W 
X  eE�O�f  a �a ,%j Y hOa b  %a %E` O *_ k+ E�W 
X  eE�O�f  a _ a ,%j Y hOa b  %a %E` O *_ k+ E�W 
X  eE�O�f  a _ a ,%j Y hO_ a %E` O *_ k+ E�W 
X  eE�O�f  a _ a ,%j Y hOa b  %a %E`  O *_  k+ E�W 
X ! eE�O�f  a "_  a ,%j Y hOa #b  %a $%E` %O *_ %k+ E�W 
X ! eE�O�f  a &_ %a ,%j Y hOa 'b  %a (%E` )O *_ )k+ E�W 
X ! eE�O�f  a *_ )a ,%j Y hOa +b  %a ,%E` -O *_ -k+ E�W 
X ! eE�O�f  a ._ -a ,%j Y hO�a /%E` 0O *_ 0k+ E�W 
X ! eE�O�f  a 1_ 0a ,%j Y hOeW 	X  �OPh ������������� 0 read_waiver  �� ����� �  ���� 0 	thewaiver  ��  � 	�������������������� 0 	thewaiver  �� 0 	waiver_id  �� 0 waiver_created  �� 0 waiver_program  �� 0 waiver_activation  �� 0 waiver_team  �� 0 waiver_event  �� 0 waiver_total_raw_files  �� 0 waiver_data  � ������"-0;>������������������ 0 get_element  �� 0 	waiver_id  �� 0 waiver_created  �� 0 waiver_program  �� 0 waiver_activation  �� 0 waiver_team  �� 0 waiver_event  �� 0 waiver_total_raw_files  �� �� s*���m+ E�O*���m+ E�O*���m+ E�O*���m+ E�O*���m+ E�O*���m+ E�O*���m+ E�O�a �a �a �a �a �a �a E�O�OPi ��c���������� 0 get_element  �� ����� �  �������� 0 	thewaiver  �� 0 node  �� 0 element_name  ��  � ������������ 0 	thewaiver  �� 0 node  �� 0 element_name  �� 0 xmldata  �� 
0 errmsg  � ��������������������
�� 
xmlf
�� 
pcnt
�� 
xmle
�� kfrmname
�� 
valL
�� 
ret �� 
0 errmsg  ��  
�� .sysodisAaleR        TEXT�� >  � *�/�,��0E�O���0�,E�UW X  b  e  
�j 	Y hO�E�j ������������� 0 
fileexists 
fileExists�� ����� �  ���� 0 thefile theFile��  � ���� 0 thefile theFile� �����
�� 
file
�� .coredoexnull���     ****�� � *�/j  eY fUk ������������� 0 folderexists folderExists�� ��� �  �� 0 thefile theFile��  � �� 0 thefile theFile� ���
� 
cfol
� .coredoexnull���     ****�� � *�/j  eY fUl �������� 0 	changeext 	changeExt� ��� �  ��� 0 filename  � 0 new_ext  �  � ����� 0 filename  � 0 new_ext  � 0 mylength  � 0 	newstring  � ����
� 
cha 
� .corecnte****       ****� 
� 
TEXT� !��-j �E�O�[�\[Zk\Z�2�&�%E�O�m ��������  0 getfreediskpct getFreeDiskPct�  �  � �� 0 rawpct rawPct� ������ 0 getdiskstats getDiskStats� 0 capacity  � 0 trimline trimLine� d� **j+  �,�km+ E�O�n ������� (0 getfreediskspacegb getFreeDiskSpaceGB�  �  �  � ��� 0 getdiskstats getDiskStats� 0 	available  � 
*j+  �,Eo ������� 0 getdiskstats getDiskStats�  �  � ��������� 0 old_delimts  � 0 memory_stats  � 0 	rawstring  � 0 thelist theList� 0 newlist newList� 0 i  � 0 	finallist 	finalList� 
0 errmsg  � ��)��E�����n����������~
� 
ascr
� 
txdl
� .sysoexecTEXT���     TEXT
� 
cpar
� 
citm
� 
kocl
� 
cobj
� .corecnte****       ****
� 
ctxt� 0 diskname  � 0 
onegblocks 
OneGblocks� 0 used  � 0 	available  � � 0 capacity  � � 
� 
0 errmsg  �~  � ���,E�O ��j E�O��l/E�O���,FO��-E�O���,FOjvE�O (�[��l 	kh ��-� ��-�6GY hOP[OY��O��k/�-���l/�-��m/�-��a /�-a ��a /�-a E�W X  hO���,FO�OPp �}��|�{���z�} 0 
is_running  �| �y��y �  �x�x 0 appname appName�{  � �w�v�u�t�w 0 appname appName�v "0 listofprocesses listOfProcesses�u 0 appisrunning appIsRunning�t 0 thisitem thisItem� ��s�r�q�p�o
�s 
prcs
�r 
pnam
�q 
kocl
�p 
cobj
�o .corecnte****       ****�z ;� 	*�-�,E�UOfE�O #�[��l kh �� 
eE�OY h[OY��O�OPq �n��m�l���k�n 0 	readplist 	readPlist�m �j��j �  �i�h�g�i 0 theplistpath thePListPath�h 0 plistvar plistVar�g 0 defaultvalue defaultValue�l  � �f�e�d�c�b�a�f 0 theplistpath thePListPath�e 0 plistvar plistVar�d 0 defaultvalue defaultValue�c 0 	mycommand 	myCommand�b 0 myscript myScript�a 
0 errmsg  � ���`�_�^�]�\
�` .sysoexecTEXT���     TEXT�_ 
0 errmsg  �^  
�] 
null
�\ .sysodisAaleR        TEXT�k 3 �%�%E�O�%�%E�O�j W X  ��  
�j Y hO�r �[ �Z�Y���X�[ 0 
writeplist 
writePlist�Z �W��W �  �V�U�T�V 0 theplistpath thePListPath�U 0 plistvar plistVar�T 0 thevalue theValue�Y  � �S�R�Q�P�O�N�S 0 theplistpath thePListPath�R 0 plistvar plistVar�Q 0 thevalue theValue�P 0 	mycommand 	myCommand�O 0 myscript myScript�N 
0 errmsg  � 79;D�M�L�K�M 
0 errmsg  �L  
�K .sysodisAaleR        TEXT�X $ �%�%�%�%E�O�%�%E�W X  �j s �JO�I�H���G�J 0 getepochtime getEpochTime�I  �H  � �F�F 
0 mytime  � Y�E�D�C�B
�E .sysoexecTEXT���     TEXT
�D 
TEXT�C  ���B 0 number_to_string  �G �j �&E�O�� E�O*�k+ t �Aj�@�?���>�A 0 number_to_string  �@ �=��= �  �<�< 0 this_number  �?  � 	�;�:�9�8�7�6�5�4�3�; 0 this_number  �: 0 x  �9 0 y  �8 0 z  �7 0 decimal_adjust  �6 0 
first_part  �5 0 second_part  �4 0 converted_number  �3 0 i  � �2z�1��0�/�.���-�,�+��*�)�
�2 
TEXT
�1 
psof
�0 
psin�/ 
�. .sysooffslong    ��� null
�- 
cha 
�, 
leng
�+ 
nmbr�*  �)  �> ���&E�O�� �*���� E�O*���� E�O*���� E�O�[�\[Z���,\Zi2�&�&E�O�j �[�\[Zk\Z�k2�&E�Y �E�O�[�\[Z�k\Z�k2�&E�O�E�O &k�kh  ���/%E�W X  ��%E�[OY��O�Y �u �(�'�&���%�( 0 add_leading_zeros  �' �$��$ �  �#�"�# 0 this_number  �" 0 max_leading_zeros  �&  � �!� �����! 0 this_number  �  0 max_leading_zeros  � 0 threshold_number  � 0 leading_zeros  � 0 digit_count  � 0 character_count  � ��"��I�� 

� 
long
� 
TEXT
� 
leng
� 
ctxt�% H�$�&E�O�� 7�E�O�k"�&�,E�O�k�E�O �kh��%�&E�[OY��O���&%�&Y ��&v �`������ 00 roundandtruncatenumber roundAndTruncateNumber� ��� �  ��� 0 	thenumber 	theNumber� .0 numberofdecimalplaces numberOfDecimalPlaces�  � ������
� 0 	thenumber 	theNumber� .0 numberofdecimalplaces numberOfDecimalPlaces� $0 theroundingvalue theRoundingValue� 0 themodvalue theModValue� 0 thesecondpart theSecondPart�
 0 thefirstpart theFirstPart� s�	�������������	 0 number_to_string  
� 
nmbr
� 
ctxt
� 
leng
� 
TEXT� ��j  ��E�O*�k"k+ Y hO�E�O �kh�%E�[OY��O�%�&E�O��E�O�E�O �kkh�%E�[OY��O�%�&E�O�k#�"E�O��&�,�  ���&�,kh�%�&E�[OY��Y hO�k"E�O*�k+ E�O��%�%E�O�w ������� 0 trimline trimLine� � ��  �  �������� 0 	this_text  �� 0 
trim_chars  �� 0 trim_indicator  �  � ���������� 0 	this_text  �� 0 
trim_chars  �� 0 trim_indicator  �� 0 x  � ����������Iw
�� 
leng
�� 
cha 
�� 
TEXT��  ��  � |��,E�Ojllv� 0 *h�� �[�\[Z�k\Zi2�&E�W 	X  �[OY��Y hOkllv� 1 +h�� �[�\[Zk\Z�k'2�&E�W 	X  �[OY��Y hO�x ������������� (0 getageofoldestfile getAgeOfOldestFile�� ����� �  ������ 0 filepath filePath�� 0 fileextension fileExtension��  � �������� 0 filepath filePath�� 0 fileextension fileExtension�� 
0 myfile  � ������� 0 getoldestfile getOldestFile�� 0 getageoffile getAgeOfFile�� *��l+  E�O�� *��%k+ Y jy ������������� 0 getageoffile getAgeOfFile�� ����� �  ���� 
0 myfile  ��  � ���������� 
0 myfile  �� 0 shscript  ��  0 fileageseconds fileAgeSeconds�� 
0 errmsg  � ������������
�� .sysoexecTEXT���     TEXT
�� 
long�� 
0 errmsg  ��  
�� 
null�� #�%�%E�O �j �&E�W 
X  �E�O�z ������������� 0 getoldestfile getOldestFile�� ����� �  ������ 0 filepath filePath�� 0 fileextension fileExtension��  � ������������ 0 filepath filePath�� 0 fileextension fileExtension�� 0 shscript  �� 0 
oldestfile 
oldestFile�� 
0 errmsg  � ����������� 
�� 
strq
�� .sysoexecTEXT���     TEXT�� 
0 errmsg  ��  �� '��,%�%�%�%E�O �j E�W 
X  �E�O�{ ������������ 60 launchaftereffectsproject launchAfterEffectsProject�� ����� �  ���� $0 projectfileposix projectFilePOSIX��  � �������� $0 projectfileposix projectFilePOSIX�� 0 shscript  �� 
0 errmsg  � ����
� 
strq
� .sysoexecTEXT���     TEXT� 
0 errmsg  �  �� ��,%E�O 
�j W X  hOP| �0������ 00 archiveaftereffectslog archiveAfterEffectsLog�  �  � ������ 0 	timestamp  � "0 archivefilepath archiveFilePath� 0 shscript  � 0 	shscript2  � 
0 errmsg  � �CEQS[����l�� 0 getepochtime getEpochTime
� 
strq
� .sysoexecTEXT���     TEXT� 
0 errmsg  �  � $0 shownotification showNotification� d*j+  E�O�*j+  %�%E�O�b  %�%�%E�O�b  �,%E�O 
�j W X  	*�l+ OPO 
�j W X  	*�l+ OPOP} �������� 0 killapp killApp� ��� �  �� "0 applicationname applicationName�  � ������ "0 applicationname applicationName� 0 shscript  � 0 	mysuccess 	mySuccess� 0 	mymessage 	myMessage� 
0 errmsg  � ������
� 
strq
� .sysoexecTEXT���     TEXT� 
0 errmsg  �  � 0��,%E�O �j OeE�O�E�W X  fE�O�E�O��lvOP~ �������� 20 checkaftereffectsstatus checkAfterEffectsStatus�  �  � ���� &0 logfileageseconds logFileAgeSeconds� 0 	mysuccess 	mySuccess� 0 	mymessage 	myMessage� �������� 0 getageoffile getAgeOfFile
� 
null
� 
bool� $0 shownotification showNotification� E*b  k+  E�O�� 
 �b  l �& fE�O�E�O*��l+ OPY 	eE�O�E�O��lvOP ������� *0 recycleaftereffects recycleAfterEffects�  �  �  � %(�0����JM�� $0 shownotification showNotification� 0 killapp killApp� 

� .sysodelanull��� ��� nmbr� 00 archiveaftereffectslog archiveAfterEffectsLog� 60 launchaftereffectsproject launchAfterEffectsProject� 0*��l+ O*�k+ O�j O*j+ O*��l+ O*b  k+ 
OP ascr  ��ޭ